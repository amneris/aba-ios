//
//  RegisterRepositoryTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import XCTest
import RxBlocking
import OHHTTPStubs

@testable import ABA

class RegisterRepositoryTest: XCTestCase {

    var registerRepo: RegisterRepository?

    override func setUp() {
        super.setUp()
        registerRepo = RegisterRepository()
    }

    override func tearDown() {
        super.tearDown()
    }

    // Tests: Registration with email

    func testRegister() {
        do {

            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()
            let user = try registerRepo?.registerWithEmail(name: "EmailGenerator", email: generatedEmail, password: "123456").toBlocking().single()

            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, generatedEmail)
            XCTAssertEqual(user?.name, "EmailGenerator")
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

    func testDuplicatedRegister() {

        let generatedEmail = TestUtils.generateEmail()

        // First registration
        do {
            try ShepherdTestHelper.forceIntegration()
            let user = try registerRepo?.registerWithEmail(name: "EmailGenerator", email: generatedEmail, password: "123456").toBlocking().single()

            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, generatedEmail)
            XCTAssertEqual(user?.name, "EmailGenerator")
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }

        // Second registration with the same email
        do {
            let _ = try registerRepo?.registerWithEmail(name: "EmailGenerator", email: generatedEmail, password: "123456").toBlocking().single()
        }
        catch let error {
            // corr
            if let repoError = error as? RegisterRepositoryError {
                if repoError != .emailAlreadyExists {
                    XCTFail()
                }
            }
            else {
                print("testRegister error: \(error)")
                XCTFail()
            }

            do {
                try ShepherdTestHelper.forceIntegration()
                let generatedEmail = TestUtils.generateEmail()
                let user = try registerRepo?.registerWithEmail(name: "EmailGenerator", email: generatedEmail, password: "123456").toBlocking().single()

                XCTAssertNotNil(user)
                XCTAssertNotNil(user?.idUser)
                XCTAssertEqual(user?.email, generatedEmail)
                XCTAssertEqual(user?.name, "EmailGenerator")
            }
            catch let error {
                print("testRegister error: \(error)")
                XCTFail()
            }
        }
    }

    func testNoIpRegister() {
        // Adding server failure for ip network call
        stub(condition: isPath("/api/abaEnglishApi/iprecognition")) { _ in
            return OHHTTPStubsResponse(data: Data(), statusCode: 500, headers: nil).responseTime(1)
        }

        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }

        do {
            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()
            let _ = try registerRepo?.registerWithEmail(name: "EmailGenerator", email: generatedEmail, password: "123456").toBlocking().single()
            XCTFail()
        }
        catch let error {
            if let publicIpError = error as? PublicIpRepositoryError, publicIpError == PublicIpRepositoryError.errorGettingIP{
                // Expected result
            }
            else {
                XCTFail()
            }
        }
    }

    // Tests: Registration with facebook

    func testFacebookRegister() {
        do {
            
            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()
            let generatedFbId = TestUtils.generateFacebookID()
            
            let user = try registerRepo?.registerWithFacebook(facebookId: generatedFbId, name: "EmailGenerator", surname: "EmailGenerator surname", email: generatedEmail, gender: "male", avatar: "23423").toBlocking().single()
            
            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, generatedEmail)
            XCTAssertEqual(user?.name, "EmailGenerator")
            XCTAssertEqual(user?.isNewUser, true)
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }
}
