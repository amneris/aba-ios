//
//  UserParserTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest
@testable import ABA

class UserFactoryTest: XCTestCase {

    override func setUp() {
        super.setUp()
        UserDataModelDAO.deleteAllUsers()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testParser() {
        do {
            // Getting a fake network user
            let rawJsonData = try TestHelper.getJsonDataFromFile("kLoginWithEmailSampleData")
            let networkUser = try NetworkUserParser.parseNetworkUser(rawJsonData)
            XCTAssertNotNil(networkUser)

            // Saving the networkuser
            UserDataModelDAO.saveUser(networkUser!)

            // Reading repo user
            let repoUser = UserDataModelDAO.getCurrentUser()
            XCTAssertNotNil(repoUser)

            // Checking mandatory properties
            XCTAssertEqual(networkUser!.email, repoUser?.email)
            XCTAssertEqual(networkUser!.idUser, repoUser?.idUser)
            XCTAssertEqual(networkUser!.name, repoUser?.name)
            XCTAssertEqual(networkUser!.token, repoUser?.token)
            XCTAssertEqual(networkUser!.type, repoUser?.type)

            // Checking other properties
            XCTAssertEqual(networkUser!.countryNameIso, repoUser?.countryNameIso)
            XCTAssertEqual(networkUser!.idSession, repoUser?.idSession)
            XCTAssertEqual(networkUser!.partnerID, repoUser?.partnerID)
            XCTAssertEqual(networkUser!.sourceID, repoUser?.sourceID)
            XCTAssertEqual(networkUser!.surnames, repoUser?.surnames)
            XCTAssertEqual(networkUser!.teacherId, repoUser?.teacherId)
            XCTAssertEqual(networkUser!.teacherImage, repoUser?.teacherImage)
            XCTAssertEqual(networkUser!.teacherName, repoUser?.teacherName)
            XCTAssertEqual(networkUser!.urlImage, repoUser?.urlImage)
            XCTAssertEqual(networkUser!.userLang, repoUser?.userLang)
            XCTAssertEqual(networkUser!.gender, repoUser?.gender)
            XCTAssertEqual(networkUser!.birthDate, repoUser?.birthDate)
            XCTAssertEqual(networkUser!.phone, repoUser?.phone)
            XCTAssertEqual(networkUser!.externalKey, repoUser?.externalKey)
            XCTAssertTrue(networkUser!.expirationDate?.timeIntervalSince1970 == repoUser?.expirationDate!.timeIntervalSince1970)
            XCTAssertTrue(networkUser!.lastLoginDate!.timeIntervalSince1970 == repoUser?.lastLoginDate!.timeIntervalSince1970)
        }
        catch {
            XCTFail()
        }
    }
}