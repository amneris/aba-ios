//
//  InterpretControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class InterpretControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteInterpretSection() {
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteInterpretSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let interpretController:InterpretController = InterpretController()
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        interpretController.setCompletedSection(randomUnit.sectionInterpret as ABAInterpret) { error, object in
            let interpret:ABAInterpret = object as! ABAInterpret
            
            XCTAssertEqual(interpret.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

    }
    
    func testGetPhrasesFromInterpretSection() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetPhrasesFromInterpretSection")
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        let interpretSection:ABAInterpret = LevelUnitController.getUnitWithID(randomUnit.idUnit).sectionInterpret
        
        XCTAssertGreaterThan(interpretSection.dialog.count, 0);
    }
    
    func testToGetAllAudiosIdForInterpretPhrases() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetPhrasesFromInterpretSection")
        let sectionController: SectionController = SectionController()
        let interpretController: InterpretController = InterpretController()

        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        let interpretSection:ABAInterpret = LevelUnitController.getUnitWithID(randomUnit.idUnit).sectionInterpret
        let interpretAudios = interpretController.getAllAudioIDs(forInterpretSection: interpretSection)
        
        XCTAssertTrue((interpretAudios?.count)!>0)
        XCTAssertEqual(interpretAudios?.count, Int(interpretController.getTotalElements(for: interpretSection)))
    }
    
    func testCompleteAllDialogsForInterpretSection() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }

        let expectation = self.expectation(description: "testGetPhrasesFromInterpretSection")
        let sectionController: SectionController = SectionController()
        let interpretController: InterpretController = InterpretController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        let interpretSection = randomUnit.sectionInterpret!
        for i in 0..<interpretSection.roles.count {
            let role = interpretSection.roles.object(at: i)
            var donePhrasesForRole = interpretController.getDonePhrases(for: role, forInterpretSection: interpretSection).count
            for j in 0..<role.interpretPhrases().count {
                let interpretPhrase = role.interpretPhrases().object(at: j) as! ABAInterpretPhrase
                interpretController.setPhraseDone(interpretPhrase, forInterpretSection: interpretSection, withCurrentRole: role, completionBlock: { (error, object) -> Void in
                    XCTAssertNil(error, "error should be nil")
                    donePhrasesForRole += 1;
                })
            }
            XCTAssertEqual(donePhrasesForRole, interpretController.getDonePhrases(for: role, forInterpretSection: interpretSection).count)
        }
        
        XCTAssertTrue(interpretController.isSectionCompleted(interpretSection))
        XCTAssertTrue(interpretController.allRolesAreCompleted(forInterpretSection: interpretSection))
        XCTAssertEqual(interpretController.getTotalElements(for: interpretSection), interpretController.getElementsCompleted(for: interpretSection))
        XCTAssertEqual(Int(interpretController.getProgressFor(interpretSection)), 100)
        XCTAssertEqual(interpretController.getPercentageFor(interpretSection), "100%")
        
        for i in 0..<interpretSection.roles.count {
            let role = interpretSection.roles.object(at: i)
            for j in 0..<role.interpretPhrases().count {
                let interpretPhrase = role.interpretPhrases().object(at: j) as! ABAInterpretPhrase
                XCTAssertEqual(interpretController.phrase(withID: interpretPhrase.audioFile, andPage: interpretPhrase.page, on: interpretSection).idPhrase, interpretPhrase.idPhrase)
                print(" idPhrase: " + interpretPhrase.idPhrase)
            }
            interpretController.eraseProgress(for: role, forInterpretSection: interpretSection, completionBlock: { (error, object) -> Void in
                XCTAssertNil(error, "error should be nil")
            })
        }
        
        XCTAssertFalse(interpretController.isSectionCompleted(interpretSection))
        XCTAssertFalse(interpretController.allRolesAreCompleted(forInterpretSection: interpretSection))
        XCTAssertNotEqual(interpretController.getTotalElements(for: interpretSection), interpretController.getElementsCompleted(for: interpretSection))
        XCTAssertNotEqual(Int(interpretController.getProgressFor(interpretSection)), 100)
        XCTAssertNotEqual(interpretController.getPercentageFor(interpretSection), "100%")
    }
}
