//
//  LoginModuleTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest
import RxBlocking
import OHHTTPStubs

@testable import ABA

class LoginRepositoryTest: XCTestCase {

    let email_studentES = "student+es@abaenglish.com"

    var loginRepo: LoginRepository?

    override func setUp() {
        super.setUp()
        loginRepo = LoginRepository()
        RealmTestHelper.deleteCurrentUser()
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: Login with user and password

    func testLogin() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            let user = try loginRepo?.login(email: email_studentES, password: "abaenglish").toBlocking().single()
            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, email_studentES)
        } catch {
            XCTFail()
        }
    }

    func testLoginAndStorage() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            let user = try loginRepo?.login(email: email_studentES, password: "abaenglish").toBlocking().single()
            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, email_studentES)

            if let userFromDB = UserDataModelDAO.getCurrentUser() {
                XCTAssertNotNil(userFromDB.idUser)
                XCTAssertEqual(userFromDB.email, email_studentES)
                XCTAssertEqual(userFromDB.name, user?.name)
            } else {
                XCTFail()
            }

        } catch {
            XCTFail()
        }
    }

    func testWrongPassword() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            let user = try loginRepo?.login(email: email_studentES, password: "wrong_password").toBlocking().single()
            if user != nil {
                XCTFail("Password should be wrong")
            }
        } catch LoginRepositoryError.wrongUserPassword {
            // Success
        }
        catch {
            XCTFail()
        }
    }

    func testWrongEmail() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            let user = try loginRepo?.login(email: "jesustestasdfasdf1123412323@abaenglish.com", password: "asdfasdfsdfsdf").toBlocking().single()
            if user != nil {
                XCTFail("Email should not exist")
            }
        } catch LoginRepositoryError.wrongUserPassword {
            // Success
        }
        catch {
            XCTFail()
        }
    }

    func testLogin_connection_error() {
        // Adding server failure
        stub(condition: isHost("api.int.aba.land")) { _ in
            return OHHTTPStubsResponse(error: NSError(domain: "fake domain error", code: 700, userInfo: nil)).responseTime(2)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }

        do {
            try ShepherdTestHelper.forceIntegration()
            
            let user = try loginRepo?.login(email: email_studentES, password: "abaenglish").toBlocking().single()
            if user != nil {
                XCTFail("The host should be fakedly down")
            }
        } catch let nserror {
            // Success
            print(nserror)
        }
    }

    // MARK: Login with token

    func testLoginWithToken() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            // 127d9b92a08061e5222722408bae93ea => jespejo@abaenglish.com token at ABA integration
            let user = try loginRepo?.login(token: "127d9b92a08061e5222722408bae93ea").toBlocking().single()
            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, "jespejo@abaenglish.com")
        } catch {
            XCTFail()
        }
    }
    
    func testLoginWithTokenNoConnection()  {
        // Adding server failure
        stub(condition: isHost("api.int.aba.land")) { _ in
            return OHHTTPStubsResponse(error: NSError(domain: "fake domain error", code: 700, userInfo: nil)).responseTime(1)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }
        
        do {
            try ShepherdTestHelper.forceIntegration()
            
            // 127d9b92a08061e5222722408bae93ea => jespejo@abaenglish.com token at ABA integration
            let user = try loginRepo?.login(token: "127d9b92a08061e5222722408bae93ea").toBlocking().single()
            if user != nil {
                XCTFail("The host should be fakedly down")
            }
        } catch let error {
            switch error {
            case LoginRepositoryError.deviceIsOffline:
                // Success
                break
            default:
                XCTFail()
            }
        }
    }
    
    func testLoginWithTokenServerDown()  {
        // Adding server failure
        stub(condition: isHost("api.int.aba.land")) { _ in
            return OHHTTPStubsResponse(data: Data(), statusCode: 500, headers: nil).responseTime(1)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }
        
        do {
            try ShepherdTestHelper.forceIntegration()
            
            // 127d9b92a08061e5222722408bae93ea => jespejo@abaenglish.com token at ABA integration
            let user = try loginRepo?.login(token: "127d9b92a08061e5222722408bae93ea").toBlocking().single()
            if user != nil {
                XCTFail("The host should be fakedly down")
            }
        } catch let error {
            switch error {
            case LoginRepositoryError.serverOffline:
                // Success
                break
            default:
                XCTFail()
            }
        }
    }
    
    func testLoginWithToken_InvalidToken()  {
        // Adding server failure
        stub(condition: isHost("api.int.aba.land")) { _ in
            return OHHTTPStubsResponse(data: Data(), statusCode: 420, headers: nil).responseTime(1)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }
        
        do {
            try ShepherdTestHelper.forceIntegration()
            
            // 127d9b92a08061e5222722408bae93ea => jespejo@abaenglish.com token at ABA integration
            let user = try loginRepo?.login(token: "127d9b92a08061e5222722408bae93ea").toBlocking().single()
            if user != nil {
                XCTFail("The host should be fakedly down")
            }
        } catch let error {
            switch error {
            case LoginRepositoryError.invalidToken:
                // Success
                break
            default:
                XCTFail()
            }
        }
    }
    
    func testLoginWithTokenFailure() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            // 2sdfsdfsdf => random token
            let user = try loginRepo?.login(token: "2sdfsdfsdf").toBlocking().single()
            if user != nil {
                XCTFail("The token should not be valid")
            }
        } catch let nserror {
            // Success
            print(nserror)
        }
    }
}
