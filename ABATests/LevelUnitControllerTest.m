//
//  LevelUnitControllerTest.m
//  ABA
//
//  Created by Oriol Vilaró on 4/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABARealmUser.h"
#import "ABARealmLevel.h"
#import "UserController.h"
#import "LevelUnitController.h"
#import "ABAUtilsHelper.h"
#import "DataController.h"
#import "APIManager.h"
#import "ABABaseTestCase.h"


@interface LevelUnitControllerTest : ABABaseTestCase

@end

@implementation LevelUnitControllerTest

- (void)setUp {
    [super setUp];
    
    [self logInTestUser];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testSaveUserData
{
    XCTAssertNotNil([UserController currentUser]);
    
    ABARealmUser *user = [UserController currentUser];
    
    NSString *newName = [NSString stringWithFormat:@"Test Name %u", arc4random_uniform(100)];
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    user.name = newName;
    [realm commitWriteTransaction];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    [UserController saveCurrentUser:user
                    completionBlock:^(NSError *error, id object) {
                        
                        XCTAssertNil(error);
                        
                        ABARealmUser *newUser = [UserController currentUser];
                        
                        XCTAssert([newUser.name isEqualToString:newName]);
                        
                        dispatch_semaphore_signal(semaphore);
                    }];
    
    while(dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

/** A currentLevel is mandatory */
- (void)testUserHaveCurrentLevel
{
    if ([UserController currentUser] != nil)
    {
        XCTAssertNotNil(UserController.currentUser.currentLevel);
    }
}

- (void)testChangeUserLevel
{
    __block ABARealmLevel *randomLevel = [ABAUtilsHelper getRandomLevel];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    [UserController setCurrentLevel:randomLevel
                    completionBlock:^(NSError *error, id object) {
                        XCTAssertNil(error);
                        
                        NSLog(@"%@ vs %@", [[UserController currentUser] currentLevel].idLevel, randomLevel.idLevel);
                        
                        XCTAssertTrue([[[UserController currentUser] currentLevel].idLevel isEqualToString:randomLevel.idLevel]);
                        dispatch_semaphore_signal(semaphore);
                    }];
    
    while(dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)testUnitCompleted
{
    __block ABAUnit *randomUnit = [ABAUtilsHelper getRandomUnit];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    [LevelUnitController setCompletedUnit:randomUnit
                          completionBlock:^(NSError *error, id object) {
                              XCTAssertNil(error);
                              
                              ABAUnit *checkedUnit = [LevelUnitController getUnitWithID:randomUnit.idUnit forLevel:randomUnit.level];
                              
                              XCTAssertTrue([checkedUnit.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                              
                              dispatch_semaphore_signal(semaphore);
                          }];
    
    while(dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)testLevelCompleted
{
    __block ABARealmLevel *randomLevel = [ABAUtilsHelper getRandomLevel];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    [LevelUnitController setCompletedLevel:randomLevel
                           completionBlock:^(NSError *error, id object) {
                               XCTAssertNil(error);
                               
                               ABARealmLevel *checkedLevel = [LevelUnitController getLevelWithID:randomLevel.idLevel];
                               
                               XCTAssertTrue([checkedLevel.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                               
                               dispatch_semaphore_signal(semaphore);
                           }];
    
    while(dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)testUserSetUnitProgress
{
    __block ABAUnit *randomUnit = [ABAUtilsHelper getRandomUnit];
    
    NSArray *progressKeys = @[ @"UnitId", @"AbaFilmPct", @"ExercisesPct", @"InterpretPct", @"SpeakPct", @"VocabularyPct", @"VideoClassPct", @"WritePct" ];
    NSArray *values = @[ randomUnit.idUnitString, @"100", @"100", @"100", @"100", @"100", @"100", @"100" ];
    NSDictionary *progressData = [NSDictionary dictionaryWithObjects:values forKeys:progressKeys];
    
    while ([[APIManager sharedManager] syncInProgress])
    {
        NSLog(@"waiting...");
    }
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [DataController syncUnitProgress:@[ progressData ]
                     completionBlock:^(NSError *error, id object) {
                         
                         XCTAssertNil(error);
                         
                         ABAUnit *updatedABAUnit = (ABAUnit *)object;
                         
                         NSLog(@"%d vs %d", [randomUnit.progress intValue], [updatedABAUnit.progress intValue]);
                         //                         NSLog(@"%@ vs %@", randomUnit.objectID, updatedABAUnit.objectID);
                         
                         XCTAssertTrue([updatedABAUnit.progress intValue] >= 87);
                         dispatch_semaphore_signal(semaphore);
                     }];
    
    while(dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

@end
