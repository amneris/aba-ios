//
//  MockSubscriptionProduct.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

class MockSelligentSubscriptionProduct: SelligentSubscriptionProduct {
    public var originalIdentifier: String?
    public var discountIdentifier: String?
    public var days: String?
    public var is2x1: String?
    
    init(originalIdentifier: String, discountIdentifier: String, days: String, is2x1: String) {
        self.originalIdentifier = originalIdentifier
        self.discountIdentifier = discountIdentifier
        self.days = days
        self.is2x1 = is2x1
    }
}

class MockSelligentProduct: SelligentProducts {
    var text: String?
    var products: [SelligentSubscriptionProduct]?
    
    init(text: String?, products: [SelligentSubscriptionProduct]? ) {
        self.text = text
        self.products = products
    }
}
