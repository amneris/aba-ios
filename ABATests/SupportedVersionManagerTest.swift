//
//  ABAVersionControlTests.swift
//  ABA
//
//  Created by Oriol Vilaró on 26/1/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class SupportedVersionManagerTest: ABABaseTestCase {
    
    let versionManager = SupportedVersionManager()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLastSupportedVersionVar() {
        let testVersionNumber = "0.0.1"
        
        versionManager.lastSupportedVersion = testVersionNumber
        
        XCTAssertEqual(UserDefaults.standard.object(forKey: kLastSupportedVersion) as? String, testVersionNumber,  "Must be equal")
        XCTAssertEqual(versionManager.lastSupportedVersion, testVersionNumber,  "Must be equal")
        
        UserDefaults.standard.removeObject(forKey: kLastSupportedVersion)
        UserDefaults.standard.synchronize()
        XCTAssertNotEqual(versionManager.lastSupportedVersion, testVersionNumber, "Must be not equal")
    }
    
    func testApiCall() {
        let expectation = self.expectation(description: "testApiCAll")
        let manager = APIManager.shared()
        
        manager?.getCurrentValidAppVersion{ error, object in
           
            XCTAssertNotNil(object, "object should not be nil")
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testCurrentVersionIsValid() {
        let expectation = self.expectation(description: "testCheckVersion")
        
        versionManager.isCurrentVersionValid { isValid in
            
            XCTAssertTrue(isValid)

            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testCheckVersionIntegrity() {
      
        XCTAssertTrue(versionManager.checkVersionIntegrity("2.2.0"))
        XCTAssertTrue(versionManager.checkVersionIntegrity("2.2"))
        XCTAssertTrue(versionManager.checkVersionIntegrity("2"))
        XCTAssertTrue(versionManager.checkVersionIntegrity("2.2.2.2"))
        XCTAssertTrue(versionManager.checkVersionIntegrity("22.22.23.24"))
        
        XCTAssertFalse(versionManager.checkVersionIntegrity("2."))
        XCTAssertFalse(versionManager.checkVersionIntegrity("2.2."))
        XCTAssertFalse(versionManager.checkVersionIntegrity(""))
        XCTAssertFalse(versionManager.checkVersionIntegrity("."))
        XCTAssertFalse(versionManager.checkVersionIntegrity("a.2"))
        XCTAssertFalse(versionManager.checkVersionIntegrity("a..2"))
        XCTAssertFalse(versionManager.checkVersionIntegrity("2a2"))
        XCTAssertFalse(versionManager.checkVersionIntegrity(""))
        
        XCTAssertFalse(versionManager.checkVersionIntegrity("2.2.2.2 "))
        XCTAssertFalse(versionManager.checkVersionIntegrity(" 2.2.2.2"))
        XCTAssertFalse(versionManager.checkVersionIntegrity(" 2.2.2.2 "))
        XCTAssertFalse(versionManager.checkVersionIntegrity(" 2.2.2. 2 "))
        XCTAssertFalse(versionManager.checkVersionIntegrity("2.2.2. 2"))
    }
    
    func testCheckCurrentVersionSupport() {

        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0", lastSupportedVersion: "0.9"))
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0", lastSupportedVersion: "1.0"))
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.0", lastSupportedVersion: "1.1"))
        
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: "0.9.9"))
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: "1.0.0"))
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: "1.0.1"))
        
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0.0", lastSupportedVersion: "0.9.9.9"))
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0.0", lastSupportedVersion: "1.0.0.0"))
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.0.0.0", lastSupportedVersion: "1.0.0.1"))
        
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: "1.0.0.0"))
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: "1.0.0.1"))
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: "1.0"))
        
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0", lastSupportedVersion: ""))
        
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0.100", lastSupportedVersion: "1.0.0.99"))
        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.1", lastSupportedVersion: "1.0.0.100"))
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.109.0", lastSupportedVersion: "1.110.0.0"))
        XCTAssertFalse(versionManager.checkCurrentVersionSupport("1.2.0", lastSupportedVersion: "1.109"))

        XCTAssertTrue(versionManager.checkCurrentVersionSupport("1.0.0.100", lastSupportedVersion: "unknown"))
    }

    func testParseSupportedVersionNumber() {

        let object = ["android": "2.2.2","ios": "2.2.1"]
        XCTAssertEqual(versionManager.parseSupportedVersionNumber(object), object["ios"])

        let object3 = ["android": "2.2.2"]
        XCTAssertEqual(versionManager.parseSupportedVersionNumber(object3), versionManager.lastSupportedVersion)

        let object4 = ["android"]
        XCTAssertEqual(versionManager.parseSupportedVersionNumber(object4), versionManager.lastSupportedVersion)
        
        let object5 = ["android": "2.2.2","ios": "2.2."]
        XCTAssertEqual(versionManager.parseSupportedVersionNumber(object5), versionManager.lastSupportedVersion)
    }
    
    func testCurrentVersion() {
        XCTAssertNotEqual(versionManager.currentVersion, kUnknownVersion)
    }
}
