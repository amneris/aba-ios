//
//  ItunesPlansInteractorTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import OHHTTPStubs
import RxBlocking

@testable import ABA

class ItunesPlansInteractorTest: XCTestCase {
    
    var plansInteractor: ItunesPlansInteractor!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testItunesCallWithoutDiscount()  {
        
        plansInteractor = ItunesPlansInteractor()
        
        let mockProduct = MockProductsFactory.createMockProductWithoutDiscounts()
        let expectation = self.expectation(description: "Retrieve plans from Apple")
        var viewmodel:PlansPageProducts? = nil
        
        plansInteractor.fetchInfoFromApple(selligentResponse: mockProduct).subscribe { event in
            switch event {
            case .next(let plansFromService):
                viewmodel = plansFromService
                expectation.fulfill()
            case .error(_):
                XCTFail()
            default:
                // do nothing
                break
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            print("testItunesCallWithoutDiscount error: \(error)")
        }
        
        XCTAssertNotNil(viewmodel)
        XCTAssertNotNil(viewmodel?.text)
        XCTAssertFalse(viewmodel!.text!.isEmpty)
        XCTAssertNotNil(viewmodel?.products)
        XCTAssertFalse(viewmodel?.products!.count == 0)
        XCTAssertTrue(viewmodel?.products!.count == 3)
        
        for plan in viewmodel!.products! {
            let index = viewmodel!.products!.index(where: { productAtIndex -> Bool in
                productAtIndex.days == plan.days
            })
            let mockAtIndex = mockProduct.products![index!]
            
            XCTAssertNotNil(plan.productWithDiscount)
            XCTAssertNotNil(plan.productWithOriginalPrice)
            
            XCTAssertEqual(mockAtIndex.originalIdentifier, plan.originalIdentifier)
            XCTAssertEqual(mockAtIndex.discountIdentifier, plan.discountIdentifier)
            XCTAssertEqual(Int(mockAtIndex.days!), plan.days)
            
            XCTAssertEqual(mockAtIndex.is2x1 == "1", plan.is2x1)
        }
        
    }
    
    func testItunesCallWithDiscount()  {
        
        plansInteractor = ItunesPlansInteractor()
        
        let mockProduct = MockProductsFactory.createMockProductWithoutDiscounts()
        let expectation = self.expectation(description: "Retrieve plans from Apple")
        var viewmodel:PlansPageProducts? = nil
        
        plansInteractor.fetchInfoFromApple(selligentResponse: mockProduct).subscribe { event in
            switch event {
            case .next(let plansFromService):
                viewmodel = plansFromService
                expectation.fulfill()
            case .error(_):
                XCTFail()
            default:
                // do nothing
                break
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            print("testItunesCallWithoutDiscount error: \(error)")
        }
        
        XCTAssertNotNil(viewmodel)
        XCTAssertNotNil(viewmodel?.text)
        XCTAssertFalse(viewmodel!.text!.isEmpty)
        XCTAssertNotNil(viewmodel?.products)
        XCTAssertFalse(viewmodel?.products!.count == 0)
        XCTAssertTrue(viewmodel?.products!.count == 3)
        
        for plan in viewmodel!.products! {
            let index = viewmodel!.products!.index(where: { productAtIndex -> Bool in
                productAtIndex.days == plan.days
            })
            let mockAtIndex = mockProduct.products![index!]
            
            XCTAssertNotNil(plan.productWithDiscount)
            XCTAssertNotNil(plan.productWithOriginalPrice)
            
            XCTAssertEqual(mockAtIndex.originalIdentifier, plan.originalIdentifier)
            XCTAssertEqual(mockAtIndex.discountIdentifier, plan.discountIdentifier)
            XCTAssertEqual(Int(mockAtIndex.days!), plan.days)
            
            XCTAssertEqual(mockAtIndex.is2x1 == "1", plan.is2x1)
        }
    }
    
    func testItunesCallWithoutText() {
        plansInteractor = ItunesPlansInteractor()
        
        let mockProduct = MockProductsFactory.createMockProductWithoutText()
        let expectation = self.expectation(description: "Retrieve plans from Apple")
        
        plansInteractor.fetchInfoFromApple(selligentResponse: mockProduct).subscribe { event in
            switch event {
            case .next(_):
                XCTFail()
            case .error(_):
                expectation.fulfill()
            case .completed:
                XCTFail()
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            print("testItunesCallWithoutDiscount error: \(error)")
        }
    }
    
    func testItunesCallWithoutProducts() {
        plansInteractor = ItunesPlansInteractor()
        
        let mockProduct = MockProductsFactory.createMockProductWithoutProducts()
        let expectation = self.expectation(description: "Retrieve plans from Apple")
        
        plansInteractor.fetchInfoFromApple(selligentResponse: mockProduct).subscribe { event in
            switch event {
            case .next(_):
                XCTFail()
            case .error(_):
                expectation.fulfill()
            case .completed:
                XCTFail()
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            print("testItunesCallWithoutDiscount error: \(error)")
        }
    }
}
