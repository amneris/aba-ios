//
//  FacebookUserParserTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class FacebookUserParserTest: XCTestCase {

    func testFullFacebookUser() {

        let id = TestUtils.generateFacebookID()
        let email = TestUtils.generateEmail()
        let firstName = "hola manolo"

        let lastName = "hola pepe"
        let gender = "male"
        let url = "path/to/image"

        let facebookUserdictionary: [String: AnyObject?] = ["id": id as Optional<AnyObject>, "email": email as Optional<AnyObject>, "first_name": firstName as Optional<AnyObject>, "last_name": lastName as Optional<AnyObject>, "gender": gender as Optional<AnyObject>]
        let kk =  ["url": url] as AnyObject
        let imageDataDictionary: [String: AnyObject?] = ["data":kk]

        do {
            let facebookUser = try FacebookUserParser.parseFacebookUser(facebookUserdictionary, imageDataResponse: imageDataDictionary)
            XCTAssertEqual(facebookUser.facebookId, id)
            XCTAssertEqual(facebookUser.email, email)
            XCTAssertEqual(facebookUser.firstName, firstName)
            XCTAssertEqual(facebookUser.lastName, lastName)
            XCTAssertEqual(facebookUser.gender, gender)
            XCTAssertEqual(facebookUser.imageUrl, url)
        }
        catch {
            XCTFail()
        }
    }

    func testEmptyParameters() {
        let id = TestUtils.generateFacebookID()
        let email = TestUtils.generateEmail()
        let firstName = "hola manolo"

        let lastName: String? = nil
        let gender: String? = nil

        let facebookUserdictionary: [String: AnyObject?] = ["id": id as Optional<AnyObject>, "email": email as Optional<AnyObject>, "first_name": firstName as Optional<AnyObject>, "last_name": lastName as Optional<AnyObject>, "gender": gender as Optional<AnyObject>]
        let imageDataDictionary: [String: AnyObject?] = ["data": nil, "pepe": nil]
        
        do {
            let facebookUser = try FacebookUserParser.parseFacebookUser(facebookUserdictionary, imageDataResponse: imageDataDictionary)
            XCTAssertEqual(facebookUser.facebookId, id)
            XCTAssertEqual(facebookUser.email, email)
            XCTAssertEqual(facebookUser.firstName, firstName)
            XCTAssertEqual(facebookUser.lastName, "")
            XCTAssertEqual(facebookUser.gender, "")
            XCTAssertEqual(facebookUser.imageUrl, "")
        }
        catch {
            XCTFail()
        }
    }
}
