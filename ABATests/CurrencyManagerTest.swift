//
//  CurrencyManagerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 2/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class CurrencyManagerTest: ABABaseTestCase {
    let testAmount: Double = 200.0
    let currencyCodes = [
        "USD", // USA
        "MXN", // Mexico
        "CAD", // Canada
        "GBP", // U.K
        "EUR" // European Union
    ]

    override func setUp() {
        super.setUp()
        logInTestUser();
    }

    override func tearDown() {
        super.tearDown()
    }

    func testCurrencyAPIManager() {
        CurrencyManager.updateCurrencies()
        let array = CurrencyCacheManager.loadCurrenciesFromCache()
        XCTAssertNotNil(array, "")
    }

    func testCurrencyConvertAmount() {

        let randomCurrencyCode: String = TestUtils.getRandomStringFromListOfString(self.currencyCodes)
        let convertedAmount = CurrencyManager.convertAmount(self.testAmount, toUSDollarFrom: randomCurrencyCode)

        if (randomCurrencyCode != "USD") {
            XCTAssertNotEqual(convertedAmount, testAmount, "must not be equal")
        } else {
            XCTAssertEqual(CurrencyManager.convertAmount(self.testAmount, toUSDollarFrom: randomCurrencyCode), self.testAmount, "must be equal")
        }
    }

    func testConvertEurToEur() {
        let convertedAmount = CurrencyManager.convertAmount(self.testAmount, toEurFrom: "EUR")
        XCTAssertEqual(convertedAmount, self.testAmount)
    }

    func testConvertToUSDtoEUR() {
        let amountEUR = CurrencyManager.convertAmount(self.testAmount, toEurFrom: "USD")
        let amountUSD = CurrencyManager.convertAmount(amountEUR, toUSDollarFrom: "EUR")
        
        XCTAssertEqual(amountUSD, self.testAmount)
    }
    
    func testConvertDirectVSIndirectConversion() {
        let amountCAD = CurrencyManager.convertAmount(self.testAmount, toEurFrom: "CAD")
        
        let amountUSD = CurrencyManager.convertAmount(self.testAmount, toUSDollarFrom: "CAD")
        let amountCAD2 = CurrencyManager.convertAmount(amountUSD, toEurFrom: "USD")
        
//        XCTAssertEqual(amountCAD2, amountCAD)
        XCTAssertTrue(Swift.abs(amountCAD2 - amountCAD) < 0.02)
    }
    
    func testRoundValues() {
        XCTAssertEqual(CurrencyManager.roundValue(2.001111), 2.00);
        XCTAssertEqual(CurrencyManager.roundValue(99.999999999), 100.00);
        XCTAssertEqual(CurrencyManager.roundValue(136.50000), 136.50);
        XCTAssertEqual(CurrencyManager.roundValue(198.91999999999999), 198.92);
        XCTAssertEqual(CurrencyManager.roundValue(139.3800048828125), 139.38);
    }
    
    func testNonExistingCurrency() {
        let amountEUR = CurrencyManager.convertAmount(self.testAmount, toEurFrom: "USDD")
        XCTAssertEqual(amountEUR, Double(NSNotFound))
    }
}
