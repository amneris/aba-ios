//
//  LoginInteractorTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 17/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest

@testable import ABA

class LoginInteractorTest: XCTestCase {

    var loginInteractor: LoginInteractor!
    let email_studentES = "student+es@abaenglish.com"

    override func setUp() {
        super.setUp()
        RealmTestHelper.deleteCurrentUser()
        loginInteractor = LoginInteractor(repository: LoginRepository())
    }

    override func tearDown() {
        super.tearDown()
    }

    func testSingleLogin() {
        do {
            let successUserDataModel = try loginInteractor.singleLogin(email: email_studentES, password: "abaenglish").toBlocking().single()
            if let safeuser = successUserDataModel {
                XCTAssertNotNil(safeuser.token)
            } else {
                XCTFail()
            }
        } catch {
            XCTFail()
        }
    }

    func testSingleLoginFailure() {
        do {
            let _ = try loginInteractor.singleLogin(email: email_studentES, password: "abaenglish23984293489234").toBlocking().single()
            XCTFail()
        } catch LoginRepositoryError.wrongUserPassword {
            // Correct
        } catch {
            XCTFail()
        }
    }

    func testWholeLoginProcedure() {
        do {
            let successUserDataModel = try loginInteractor.login(email: email_studentES, password: "abaenglish").toBlocking().single()
            if let safeuser = successUserDataModel {
                XCTAssertNotNil(safeuser.token)
                XCTAssertTrue(RealmTestHelper.isUserCreated())
                XCTAssertTrue(RealmTestHelper.areUnitsCreated())
                XCTAssertTrue(RealmTestHelper.areLevelsCreated())
            } else {
                XCTFail()
            }
        } catch {
            XCTFail()
        }
    }

    func testDownloadUnitsError() {
        do {
            let successUserDataModel = try loginInteractor.singleLogin(email: email_studentES, password: "abaenglish").toBlocking().single()
            if let safeuser = successUserDataModel {
                XCTAssertNotNil(safeuser.token)
                
                // Overwiting user token to break unit download
                safeuser.token = "asfasdfasdfasdfasdf"

                do {
                    _ = try loginInteractor.downloadUnits(safeuser as! UserDataModel).toBlocking().single()
                    XCTFail()
                } catch {
                    // Correct due to token is corrupted
                    
                    // The user model did not save to DB
                    XCTAssertFalse(RealmTestHelper.isUserCreated())
                    
                    // The units could not be downloaded
                    XCTAssertFalse(RealmTestHelper.areUnitsCreated())
                    
                    // The levels are in the DB
                    XCTAssertTrue(RealmTestHelper.areLevelsCreated())
                    return
                }

            } else {
                XCTFail()
            }
        } catch {
            XCTFail()
        }
    }

    // MARK: Login with token

    func testSingleLoginWithToken() {
        do {
            // 127d9b92a08061e5222722408bae93ea => jespejo@abaenglish.com token at ABA integration
            let user = try loginInteractor.singleLogin(token: "127d9b92a08061e5222722408bae93ea").toBlocking().single()
            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, "jespejo@abaenglish.com")
        } catch {
            XCTFail()
        }
    }
    
    func testSingleLoginWithTokenFailure() {
        do {
            // 2sdfsdfsdf => random token
            let user = try loginInteractor.singleLogin(token: "2sdfsdfsdf").toBlocking().single()
            if user != nil {
                XCTFail("The token should not be valid")
            }
        } catch let nserror {
            // Success
            XCTAssertFalse(RealmTestHelper.isUserCreated())
            XCTAssertFalse(RealmTestHelper.areUnitsCreated())
            XCTAssertFalse(RealmTestHelper.areLevelsCreated())
            print(nserror)
        }
    }
    
    func testLoginWithToken() {
        do {
            // 127d9b92a08061e5222722408bae93ea => jespejo@abaenglish.com token at ABA integration
            let user = try loginInteractor.login(token: "127d9b92a08061e5222722408bae93ea").toBlocking().single()
            XCTAssertNotNil(user)
            XCTAssertNotNil(user?.idUser)
            XCTAssertEqual(user?.email, "jespejo@abaenglish.com")
            
            XCTAssertTrue(RealmTestHelper.isUserCreated())
            XCTAssertTrue(RealmTestHelper.areUnitsCreated())
            XCTAssertTrue(RealmTestHelper.areLevelsCreated())
        } catch {
            XCTFail()
        }
    }

}
