//
//  MockPlansView.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

class MockPlansView: NSObject, PlansViewInput {
    
    var plansWithProductsRendered = false
    var errorRendered = false
    
    var callback: ((Void) -> Void) = { }
    

    func renderPlans(_ arrayOfPlans: PlansPageProducts!) {
        plansWithProductsRendered = true
        callback()
    }
    
    func renderError() {
        errorRendered = true
        callback()
    }
}
