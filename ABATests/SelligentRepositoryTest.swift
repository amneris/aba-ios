//
//  SelligentRepositoryTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/12/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import XCTest
import OHHTTPStubs
import RxBlocking

@testable import ABA

class SelligentRepositoryTest: XCTestCase {
    
    var sellientRepository: SelligentRepository?
    
    override func setUp() {
        super.setUp()
        sellientRepository = SelligentRepository()
    }
    
    func testGetProducts() {
        do {
            let productList = try sellientRepository?.selligentProducts(userid: "100").toBlocking().single()
            
            XCTAssertNotNil(productList)
            XCTAssertNotNil(productList?.text)
            XCTAssertNotNil(productList?.products)
            XCTAssertFalse((productList?.products?.isEmpty)!)
        }
        catch {
            XCTFail()
        }
    }
    
    func testGetProductsFailure() {
        
        stub(condition: isHost("mobile-selligent-apitools.proxy.abaenglish.com")) { _ in
            return OHHTTPStubsResponse(error: NSError(domain: "fake domain error", code: 700, userInfo: nil)).responseTime(2)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }
        
        do {
            let _ = try sellientRepository?.selligentProducts(userid: "").toBlocking().single()
            XCTFail() // trace should not reach this position
        }
        catch {
            // Success
        }
    }
}
