//
//  DataControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class DataControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
//    func testLoginTokenProcess() {
//
//        guard let token = UserController.currentUser().token else {
//            XCTFail("Token not fetched")
//            return
//        }
//        
//        guard let idUser = UserController.currentUser().idUser else {
//            XCTFail("idUser not fetched")
//            return
//        }
//        
//        let expectation = expectationWithDescription("testLoginTokenProcess")
//        
//        UserDataController.postLoginWithToken(token) { (error, object) -> Void in
//            
//            XCTAssertNil(error, "error should be nil")
//
//            XCTAssertEqual(idUser, UserController.currentUser().idUser, "Userid should be same")
//            
//            expectation.fulfill()
//        }
//        
//        waitForExpectationsWithTimeout(Double(WS_TIMEOUT)) { error in
//            if let error = error {
//                print("Error: \(error.localizedDescription)")
//            }
//        }
//    }
}
