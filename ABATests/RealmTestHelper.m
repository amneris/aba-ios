//
//  RealmHelper.m
//  ABA
//
//  Created by MBP13 Jesus on 17/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "RealmTestHelper.h"

#import "ABAEvaluation.h"
#import "ABAExercises.h"
#import "ABAExercisesGroup.h"
#import "ABAExercisesPhrase.h"
#import "ABAExercisesQuestion.h"
#import "ABAFilm.h"
#import "ABAInterpret.h"
#import "ABAInterpretRole.h"
#import "ABARealmLevel.h"
#import "ABARealmUser.h"
#import "ABASpeak.h"
#import "ABASpeakDialog.h"
#import "ABASpeakDialogPhrase.h"
#import "ABASpeakPhrase.h"
#import "ABAUnit.h"
#import "ABAVideoClass.h"
#import "ABAVocabulary.h"
#import "ABAVocabularyPhrase.h"
#import "ABAVocabularyPhrase.h"
#import "ABAVocabularyPhrase.h"
#import "ABAVocabularyPhrase.h"
#import "ABAVocabularyPhrase.h"
#import "ABAWrite.h"
#import "ABAWriteDialog.h"

@implementation RealmTestHelper

+ (void)deleteCurrentUser
{
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)
    NSError *error;

    [realm beginWriteTransaction];

    [realm deleteObjects:[ABARealmUser allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAEvaluation allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAEvaluationOption allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAEvaluationQuestion allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAExercises allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAExercisesGroup allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAExercisesQuestion allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAExercisesPhrase allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAFilm allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAInterpret allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAInterpretPhrase allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAInterpretRole allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAPhrase allObjectsInRealm:realm]];

    [realm deleteObjects:[ABASection allObjectsInRealm:realm]];

    [realm deleteObjects:[ABASpeak allObjectsInRealm:realm]];
    [realm deleteObjects:[ABASpeakDialog allObjectsInRealm:realm]];
    [realm deleteObjects:[ABASpeakDialogPhrase allObjectsInRealm:realm]];
    [realm deleteObjects:[ABASpeakPhrase allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAVideoClass allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAVocabulary allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAVocabularyPhrase allObjectsInRealm:realm]];

    [realm deleteObjects:[ABAWrite allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAWriteDialog allObjectsInRealm:realm]];

    [realm deleteObjects:[ABARole allObjectsInRealm:realm]];

    // Removing all units since unit´s text might be changed due to new user language
    [realm deleteObjects:[ABAUnit allObjectsInRealm:realm]];

    RLMResults *ABARealmLevels = [ABARealmLevel allObjectsInRealm:realm];

    for (ABARealmLevel *level in ABARealmLevels)
    {
        level.completed = [NSNumber numberWithInt:0];
        level.progress = [NSNumber numberWithInt:0];
    }
    
    [realm deleteObjects:ABARealmLevels];

    [realm commitWriteTransaction:&error];
}

+ (BOOL)isUserCreated
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults * usersArray = [ABARealmUser allObjectsInRealm:realm];
    return usersArray && usersArray.count == 1;
}

+ (BOOL)areUnitsCreated
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults * unitsArray = [ABAUnit allObjectsInRealm:realm];
    return unitsArray && unitsArray.count == 144;
}

+ (BOOL)areLevelsCreated
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    RLMResults * levelsArray = [ABARealmLevel allObjectsInRealm:realm];
    return levelsArray && levelsArray.count == 6;
}

@end
