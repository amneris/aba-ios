//
//  APIManagerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 2/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class APIManagerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testIpRecognition() {
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().getPublicIP { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testRegisterFacebook() {
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().postRegister(withFacebookID: TestUtils.generateFacebookID(), email: TestUtils.generateEmail(), name: "test", surnames: "facebook", gender: "male", avatar: "", completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testRegister() {
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().postRegister(withEmail: TestUtils.generateEmail(), password: "123456", name: "EmailGenerator", completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testLogin() {
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().postLogin(withEmail: TEST_USER_PREMIUM, password: TEST_PASSWORD_PREMIUM, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testRecoverPassword() {
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().postRecoverPassword(TEST_USER_NORMAL, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUpdateUserPassword() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let token = currentUser.token else {
            
            XCTFail("currentUser should have token")
            return
        }
        
        APIManager.shared().updatePasswordUser(withToken: token, password: TEST_PASSWORD_PREMIUM, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUpdateUserLevel() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let idUser = currentUser.idUser else {
            
            XCTFail("currentUser should have idUser")
            return
        }
        
        guard let randomLevel = ABAUtilsHelper.getRandomLevel() else {
            
            XCTFail("ABAUtilsHelper should return randomLevel")
            return
        }
        
        guard let idLevel = randomLevel.idLevel else {
            
            XCTFail("randomLevel should have idLevel")
            return
        }
        
        APIManager.shared().updateUserLevel(idLevel, idUser: idUser, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUnit() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let token = currentUser.token else {
            
            XCTFail("currentUser should have token")
            return
        }
        
        guard let userLang = currentUser.userLang else {
            
            XCTFail("currentUser should have userLang")
            return
        }
        
        APIManager.shared().getAllUnits(withToken: token, withLanguage: userLang, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testContent() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let userLang = currentUser.userLang else {
            
            XCTFail("currentUser should have userLang")
            return
        }
        
        guard let randomUnit = ABAUtilsHelper.getRandomUnit() else {
            
            XCTFail("ABAUtilsHelper should return randomUnit")
            return
        }
        
        guard let unitId = randomUnit.idUnitString else {
            
            XCTFail("randomUnit should have unitId")
            return
        }
        
        APIManager.shared().getSectionContent(withLocale: userLang, andUnitID: unitId, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testCourseSectionProgress() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let userId = currentUser.idUser else {
            
            XCTFail("currentUser should have userId")
            return
        }
        
        guard let userLang = currentUser.userLang else {
            
            XCTFail("currentUser should have userLang")
            return
        }
        
        APIManager.shared().getCourseProgress(withLocale: userLang, andUserID: userId, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUnitSectionProgress() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let userId = currentUser.idUser else {
            
            XCTFail("currentUser should have userId")
            return
        }
        
        guard let userLang = currentUser.userLang else {
            
            XCTFail("currentUser should have userLang")
            return
        }
        
        guard let randomUnit = ABAUtilsHelper.getRandomUnit() else {
            
            XCTFail("ABAUtilsHelper should return randomUnit")
            return
        }
        
        guard let unitId = randomUnit.idUnitString else {
            
            XCTFail("randomUnit should have unitId")
            return
        }

        APIManager.shared().getUnitProgress(withLocale: userLang, andUserID: userId, andUnitID: unitId, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testSectionListCompletedElements() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let userId = currentUser.idUser else {
            
            XCTFail("currentUser should have userId")
            return
        }
        
        guard let randomUnit = ABAUtilsHelper.getRandomUnit() else {
            
            XCTFail("ABAUtilsHelper should return randomUnit")
            return
        }
        
        APIManager.shared().getCompletedActions(fromUserID: userId, andUnit: randomUnit, withSectionID: nil, completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUnitListCompletedElements() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        guard let currentUser = UserController.currentUser() else {
            
            XCTFail("currentUser should exist")
            return
        }
        
        guard let userId = currentUser.idUser else {
            
            XCTFail("currentUser should have userId")
            return
        }
        
        guard let randomUnit = ABAUtilsHelper.getRandomUnit() else {
            
            XCTFail("ABAUtilsHelper should return randomUnit")
            return
        }
        
        APIManager.shared().getCompletedActions(fromUserID: userId, andUnit: randomUnit, withSectionID: "2", completionBlock: { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testProgressRegister() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        let progressController = ProgressController()
        
        progressController.sendProgressActions(withLimit: kProgressActionLimit) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")

            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testGetProducts() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().getSubscriptionProducts { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testFreeToPremium() {
        
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        let decimal = 60 as NSDecimalNumber
        
        let receipt = ["test": "cascas"] as [AnyHashable: Any]
        
        APIManager.shared().updateUserToPremium(withProductPrice: decimal, withCurrencyCode: "EUR", withCountryCode: "ES", withPediodDays: 6, withUserId: "4234235", withSessionId: nil, withReceipt: receipt, completionBlock: { (error) -> Void in
            let errorr = error as! NSError
//            XCTAssertEqual(error.code, 401, "error should be 401 unauthorized")
            XCTAssertEqual(errorr.code, 401, "error should be 401 unauthorized")
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testRequiredVersion() {
     
        self.logInTestUser()
        
        let expectation = self.expectation(description: "expectation")
        
        APIManager.shared().getCurrentValidAppVersion { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
