//
//  LanguageControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class LanguageControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        
        LanguageController.resetCurrentLanguage()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDefaultLanguageFromDefault() {
        
        guard let currentLanguage = LanguageController.getCurrentLanguage() else {
            XCTFail("should return current language")
            return
        }
        
        guard let supportedLanguages = LanguageController.getSupportedLanguages() as? Array<String> else {
            XCTFail("should return supported languages")
            return
        }
        
        XCTAssertTrue(supportedLanguages.contains(currentLanguage), "current language should exist")
    }
    
    func testSetLanguage() {
        
        guard let supportedLanguages = LanguageController.getSupportedLanguages() as? Array<String> else {
            XCTFail("should return supported languages")
            return
        }
        
        let randomLanguage = TestUtils.getRandomOfStringArray(supportedLanguages)
        LanguageController.setCurrentLanguage(randomLanguage)
        XCTAssertEqual(LanguageController.getCurrentLanguage(), randomLanguage, "should be equal")
    }
    
    func testForceAppLanguage() {
        
        guard let supportedLanguages = LanguageController.getSupportedLanguages() as? Array<String> else {
            XCTFail("should return supported languages")
            return
        }
        
        let randomLanguage = TestUtils.getRandomOfStringArray(supportedLanguages)
        
        UserDefaults.standard.set([randomLanguage, "en"], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        LanguageController.resetCurrentLanguage()
        
        XCTAssertEqual(LanguageController.getCurrentLanguage(), randomLanguage, "should be equal")
    }
}
