//
//  MockRegisterViewInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 19/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

@testable import ABA

class MockRegisterViewInput: RegisterViewInput {
    
    var registrationConfirmation: Bool = false
    var loadingIndicatorShown: Bool = false
    var loadingIndicartorHidden: Bool = false
    
    var emailRepeatedErrorShown: Bool = false
    var genericErrorShown: Bool = false
    var facebookErrorShown: Bool = false
    var permissionErrorShown: Bool = false
    
    var validationError: LoginParametersValidation? = nil
    
    var callback: ((Void) -> Void) = { }
    
    func renderRegistrationConfirmation() {
        registrationConfirmation = true
        callback()
    }
    
    func showLoadingIndicator() {
        loadingIndicatorShown = true
        callback()
    }
    
    func hideLoadingIndicator() {
        loadingIndicartorHidden = true
        callback()
    }
    
    func renderValidationError(_ error: LoginParametersValidation) {
        validationError = error
        callback()
    }
    
    func showEmailRepeatedError() {
        emailRepeatedErrorShown = true
        callback()
    }
    
    func showGenericError() {
        genericErrorShown = true
        callback()
    }
    
    func showFacebookError() {
        facebookErrorShown = true
        callback()
    }
    
    func showPermissionError(){
        permissionErrorShown = true
        callback()
    }
}
