//
//  LevelUnitControllerTest.m
//  ABA
//
//  Created by Oriol Vilaró on 11/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LevelUnitController.h"
#import "ABARealmLevel.h"
#import "UserController.h"
#import "ABAUtilsHelper.h"
#import "Resources.h"
#import "Utils.h"
#import "ABABaseTestCase.h"


@interface UnitTest : ABABaseTestCase

@end

@implementation UnitTest

- (void)setUp
{
    [super setUp];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [UserController loginUserWithEmail:TEST_USER_PREMIUM
                          withPassword:TEST_PASSWORD_PREMIUM
                       completionBlock:^(NSError *error, id object) {
                           XCTAssertNil(error);

                           dispatch_semaphore_signal(semaphore);
                       }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testHasLevels
{
    XCTAssertEqual([LevelUnitController getNumberOfLevels], ABA_DEFAULT_LEVEL_NUMBER);
}

- (void)testLevelsHaveAllUnits
{
    RLMResults *allLevels = [LevelUnitController getAllLevels];

    for (ABARealmLevel *level in allLevels)
    {
        XCTAssertEqual([level.units count], ABA_DEFAULT_UNITS_PER_LEVEL);
    }
}

- (void)testHasAllUnits
{
    RLMResults *levels = [LevelUnitController getAllLevels];

    int numberOfUnits = 0;

    for (ABARealmLevel *level in levels)
    {
        numberOfUnits += [level.units count];
    }

    XCTAssertEqual(numberOfUnits, ABA_DEFAULT_UNIT_NUMBER);
}

- (void)testCompleteUnit
{
    ABAUnit *unitToComplete = [ABAUtilsHelper getRandomUnit];

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [LevelUnitController setCompletedUnit:unitToComplete
                          completionBlock:^(NSError *error, id object) {
                              dispatch_semaphore_signal(semaphore);
                          }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }

    NSArray *completedUnits = [LevelUnitController getCompletedUnitsForLevel:unitToComplete.level];

    XCTAssertGreaterThan([completedUnits count], 0);
    
    BOOL unitFound = NO;
    for (ABAUnit *unit in completedUnits)
    {
        if ([unit.idUnit isEqualToNumber:unitToComplete.idUnit])
        {
            unitFound = YES;
        }
    }

    XCTAssertTrue(unitFound);

    XCTAssertTrue(unitToComplete.progress.intValue == 100);
}

@end
