//
//  UserControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class UserControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testCheckCredentials() {
        
        XCTAssertTrue(MDTUtils.isEmailValid(TEST_USER_PREMIUM))
        XCTAssertTrue(MDTUtils.isCorrectPassword(TEST_PASSWORD_PREMIUM))
        
        XCTAssertTrue(MDTUtils.isEmailValid(TEST_USER_NORMAL))
        XCTAssertTrue(MDTUtils.isCorrectPassword(TEST_PASSWORD_NORMAL))

        XCTAssertTrue(MDTUtils.isEmailValid(TEST_USER_REALM_MIGRATION))
        XCTAssertTrue(MDTUtils.isCorrectPassword(TEST_PASSWORD_REALM_MIGRATION))

        XCTAssertTrue(MDTUtils.isEmailValid(TEST_USER_REALM_MIGRATION_PREMIUM))
        XCTAssertTrue(MDTUtils.isCorrectPassword(TEST_PASSWORD_REALM_MIGRATION_PREMIUM))

        XCTAssertTrue(MDTUtils.isTextEmpty(""))

        XCTAssertFalse(MDTUtils.isEmailValid(""))
        XCTAssertFalse(MDTUtils.isEmailValid("test.test.com"))
        XCTAssertFalse(MDTUtils.isEmailValid("test@test."))
        XCTAssertFalse(MDTUtils.isEmailValid("test@.com"))
        XCTAssertFalse(MDTUtils.isEmailValid("test.com"))

        XCTAssertFalse(MDTUtils.isCorrectPassword(""))
        XCTAssertFalse(MDTUtils.isCorrectPassword("12345"))
    }
    
    func testUserCheckCredentials() {
        
        let expectation = self.expectation(description: "testUserCheckCredentials")
        
        UserController.checkUserPassword(withEmail: TEST_USER_PREMIUM, withPassword: TEST_PASSWORD_PREMIUM) { (error, success) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertTrue(success, "success should be true")

            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testLoginProcess() {
        
        let expectation = self.expectation(description: "testLoginProcess")
        
        UserController.loginUser(withEmail: TEST_USER_PREMIUM, withPassword: TEST_PASSWORD_PREMIUM) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            self.checkUserProperlyCreatedInDDBB()

            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testLoginWithFacebookProcess() {
        
        let expectation = self.expectation(description: "testLoginWithFacebookProcess")

        UserController.registerUser(withFacebookID: "1423011378021227", email: "jespejo@abaenglish.com", name: "Jesus Spiegel", surnames: "", gender: "male", avatar: "", completionBlock: { (error, object, isNewUser) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertFalse(isNewUser, "should be false")
            XCTAssertTrue(UserController.isUserLogged(), "user should be logged")

            self.checkUserProperlyCreatedInDDBB()
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testRegisterProcess() {
        
        let mail = TestUtils.generateEmail()
        
        let expectation = self.expectation(description: "testRegisterProcess")

        UserController.registerUser(withEmail: mail, withPassword: "123456", withName: "iOS Test User") { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertTrue(UserController.isUserLogged(), "user should be logged")
            
            self.checkUserProperlyCreatedInDDBB()

            expectation.fulfill()
        }
    
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    func testRegisterWithFacebookProcess() {
        
        let expectation = self.expectation(description: "testRegisterWithFacebookProcess")

        UserController.registerUser(withFacebookID: TestUtils.generateFacebookID(), email: TestUtils.generateEmail(), name: "test", surnames: "facebook", gender: "male", avatar: "") { (error, object, isNewUser) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertTrue(isNewUser, "should be false")
            XCTAssertTrue(UserController.isUserLogged(), "user should be logged")
            
            self.checkUserProperlyCreatedInDDBB()

            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUserPremium() {
        
        self.logInTestUser()
        
        let user = UserController.currentUser()
        
        XCTAssertNotNil(user, "User should exist")
        XCTAssertTrue(UserController.isUserPremium(), "user should be premium")
        
        self.checkUserProperlyCreatedInDDBB()
    }
    
    func testRecoverPassword() {
        
        let expectation = self.expectation(description: "testRecoverPassword")

        UserController.recoverPassword(TEST_USER_NORMAL) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func checkUserProperlyCreatedInDDBB() {

        XCTAssertNotNil(UserController.currentUser(), "should not be nil")
        
        UserController.logout { (error) -> Void in
            
            XCTAssertNil(error, "should be nil")
            XCTAssertNil(UserController.currentUser(), "should be nil")
        }
    }
}
