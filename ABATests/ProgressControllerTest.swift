//
//  ProgressControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class ProgressControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetCourseProgress() {
        
        let expectation = self.expectation(description: "testGetCourseProgress")
        
        let progressController = ProgressController.init()

        progressController.getCourseProgress { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testGetCompletedAction() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        guard let currentUser = UserController.currentUser() else {
            XCTFail("current user not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetCompletedAction")

        let progressController = ProgressController.init()

        progressController.getCompletedActions(from: currentUser, on: randomUnit) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testToSyncUnitsWithCompletedProgress() {
     
        let levels = LevelUnitController.getAllLevelsDescending()!
        var jsonArray = Array<Dictionary<String, String>>()
        
        for i:UInt in 0..<levels.count {
            let level = levels[i] as! ABARealmLevel
            
            for j in 0..<level.units.count {
                let unit = level.units.object(at: j)
                
                let progressKeys = ["IdUnit", "AbaFilmPct", "ExercisesPct", "InterpretPct", "SpeakPct", "VocabularyPct", "VideoClassPct", "WritePct", "AssessmentPct"]
                let values = [unit.idUnitString, "100", "100", "100", "100", "100", "100", "100", "100"]
                var jsonObject = Dictionary<String, String>()
                
                for k in 0..<progressKeys.count {
                    jsonObject[progressKeys[k]] = values[k]
                }
                
                jsonArray.append(jsonObject)
            }
        }
        
        let expectation = self.expectation(description: "testToSyncUnitsWithCompletedProgress")
        
        DataController.syncUnitProgress(jsonArray) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")
            
            self.checkIfAllUnitsAreCompleted()
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testUpdateProgressUnit() {
    
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        completeAllSections(for: randomUnit)
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let expectation = self.expectation(description: "testUpdateProgressUnit")
        
        // Update progress for Unit
        DataController.updateProgressUnit(randomUnit) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should be nil")
            XCTAssertNotNil(object, "object should not be nil")

            randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)

            XCTAssertEqual(Int(randomUnit.progress), 100)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
    }
    
    func checkIfAllUnitsAreCompleted() {
        
        let levels = LevelUnitController.getAllLevels()!
        var completedUnits = 0
        
        for i in 0..<levels.count {
            
            let level = levels.object(at: i) as! ABARealmLevel
            let levelsCompleted = LevelUnitController.getCompletedUnits(for: level)
            
            completedUnits += levelsCompleted!.count
        }
        
        XCTAssertEqual(completedUnits, 144, "should all be completed")
        
        let levelsDescending = LevelUnitController.getAllLevelsDescending()!
        
        for i in 0..<levelsDescending.count {
            let level = levelsDescending.object(at: i) as! ABARealmLevel
            
            XCTAssertEqual(LevelUnitController.getCompletedUnits(for: level).count, 24, "should be equal")
            XCTAssertEqual(LevelUnitController.getInompletedUnits(for: level).count, 0, "imcompleted units should be 0")
        }
        
    }
}
