//
//  ShepherdModuleTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class ShepherdModuleTest: XCTestCase {
    
    func testShepherIntegration() {
        do {
            try ShepherdTestHelper.forceIntegration()
            
            let shepherdUrl = (ABAShepherdEditor.shared().environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.classForCoder()) as? ABACoreShepherdEnvironment)!.abaSecureApiUrl
            if !(shepherdUrl?.contains("-int."))! {
                XCTFail()
            }
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

}
