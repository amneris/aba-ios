//
//  PlansInteractorTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import OHHTTPStubs
import RxBlocking

@testable import ABA

class SelligentPlansInteractorTest: XCTestCase {
    var plansInteractor: SelligentPlansInteractor!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSelligentCall() {
        
        plansInteractor = SelligentPlansInteractor(repository: SelligentRepository())
        
        do {
            let plans = try plansInteractor?.getProducts(userId: "").toBlocking().single()
            XCTAssertNotNil(plans)
            XCTAssertNotNil(plans?.text)
            XCTAssertNotNil(plans?.products)
            XCTAssertFalse(plans?.products?.count == 0)
        } catch _ {
            XCTFail()
        }
    }
    
    func testSelligentCallWithoutConnection() {
        
        plansInteractor = SelligentPlansInteractor(repository: SelligentRepository())
        
        stub(condition: isHost("mobile-selligent-apitools.proxy.abaenglish.com")) { _ in
            return OHHTTPStubsResponse(error: NSError(domain: "fake domain error", code: 700, userInfo: nil)).responseTime(2)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }
        
        do {
            let plans = try plansInteractor?.getProducts(userId: "").toBlocking().single()
            XCTAssertNil(plans)
            XCTFail() // trace should not reach this position
        } catch _ {
            // Success
        }
    }
}
