//
//  MockTracker.swift
//  ABA
//
//  Created by MBP13 Jesus on 19/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class MockTracker: RegisterTracker {
    
    var registrationWithEmailTracked = false
    var registrationWithFacebookTracked = false
    
    func trackRegister(_ email: String, name: String) {
        registrationWithEmailTracked = true
    }
    
    func trackAccessWithFacebook(_ email: String, name: String, newUser: Bool) {
        registrationWithFacebookTracked = false
    }
}
