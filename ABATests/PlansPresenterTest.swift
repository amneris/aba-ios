//
//  PlansPresenterTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import OHHTTPStubs
import RxBlocking

@testable import ABA

class PlansPresenterTest: XCTestCase {
    var plansPresenter: PlansPresenter!
    var mockView: MockPlansView!
    
    override func setUp() {
        super.setUp()
        
        mockView = MockPlansView()
        
        plansPresenter = PlansPresenter()
        plansPresenter.iTunesInteractor = ItunesPlansInteractor()
        plansPresenter.selligentInteractor = SelligentPlansInteractor(repository: SelligentRepository())
        plansPresenter.view = mockView
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGestPlans() {
        let expectation = self.expectation(description: "Retrieve plans from Selligent")
        mockView.callback = {
            expectation.fulfill()
        }
        
        plansPresenter.getPlans("")
        waitForExpectations(timeout: 25) { (error) in
            print("testGestPlans error: \(error)")
        }
        
        XCTAssertTrue(mockView.plansWithProductsRendered)
        XCTAssertFalse(mockView.errorRendered)
    }
    
    func testGetPlansWithoutConnection() {
        
        let expectation = self.expectation(description: "Retrieve plans from Selligent")
        mockView.callback = {
            expectation.fulfill()
        }
        
        stub(condition: isHost("mobile-selligent-apitools.proxy.abaenglish.com")) { _ in
            return OHHTTPStubsResponse(error: NSError(domain: "fake domain error", code: 700, userInfo: nil)).responseTime(2)
        }
        
        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }
        
        plansPresenter.getPlans("")
        waitForExpectations(timeout: 25) { (error) in
            print("testGestPlans error: \(error)")
        }
        
        XCTAssertFalse(mockView.plansWithProductsRendered)
        XCTAssertTrue(mockView.errorRendered)

    }
}
