//
//  ProgressActionTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class ProgressActionTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        
        logInTestUser();
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSaveProgressAction() {
        let expectation = self.expectation(description: "testGenerateProgressAction")

        DataController.saveProgressAction(for: nil, andUnit: nil, andPhrases: nil, errorText: nil, isListen: false, isHelp: false) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should not exist")
            XCTAssertNotNil(object, "object should exist")
            
            let progressActionsArray = object as! Array<ABARealmProgressAction>
            let progressAction = progressActionsArray[0] 
            let savedProgressAction = DataController.getPendingProgressActions().firstObject() as! ABARealmProgressAction
            
            XCTAssertNotNil(progressAction, "should exist")
            XCTAssertNotNil(savedProgressAction, "should exist")
            
            XCTAssertEqual(progressAction.timeStamp, savedProgressAction.timeStamp, "should be equal")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testCreateProgressAction() {
        let expectation = self.expectation(description: "testGenerateProgressAction")
        
        DataController.saveProgressAction(for: nil, andUnit: nil, andPhrases: nil, errorText: nil, isListen: false, isHelp: false) { (error, object) -> Void in
            
            XCTAssertNil(error, "error should not exist")
            XCTAssertNotNil(object, "object should exist")
            
            let progressActionsArray = object as! Array<ABARealmProgressAction>
            let progressAction = progressActionsArray[0]
            let dict = progressAction.toDictionary() as NSDictionary
            
            XCTAssertNotNil(dict, "should exist")
            
            XCTAssertEqual(progressAction.timeStamp, dict.object(forKey: "dateInit") as? String, "should be equal")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
