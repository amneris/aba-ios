//
//  ABASectionTest.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LevelUnitController.h"
#import "ABARealmUser.h"
#import "UserController.h"
#import "ABAFilm.h"
#import "ABAVideoClass.h"
#import "ABAWrite.h"
#import "ABAInterpret.h"
#import "ABASpeak.h"
#import "ABAVocabulary.h"
#import "ABAExercises.h"
#import "ABAEvaluation.h"
#import "ABARealmLevel.h"
#import "ABABaseTestCase.h"
#import "DataController.h"
#import "UserDataController.h"
#import "Utils.h"


@interface ABASectionTest : ABABaseTestCase

@end

@implementation ABASectionTest

- (void)setUp
{
    [super setUp];

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [UserController loginUserWithEmail:TEST_USER_PREMIUM
                          withPassword:TEST_PASSWORD_PREMIUM
                       completionBlock:^(NSError *error, id object) {
                           XCTAssertNil(error);

                           dispatch_semaphore_signal(semaphore);
                       }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
}

- (void)testABASpeakSamples
{
    // Downloading all the units for all the languages
    for (NSString *language in kSuportedLanguagesArray)
    {
        dispatch_semaphore_t semaphoreSection = dispatch_semaphore_create(0);
        [DataController getAllUnitsWithToken:[UserDataController getCurrentUser].token
                                withLanguage:language
                             completionBlock:^(NSError *error, id object) {
                                 if (error)
                                 {
                                     XCTFail(@"testABASpeakSamples Error: , %@", error);
                                 }
                                 else
                                 {
                                     dispatch_semaphore_signal(semaphoreSection);
                                 }
                                 NSLog(@"units for language %@ downloaded", language);
                             }];

        while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
        }
    }

    RLMResults *levels = [LevelUnitController getAllLevels];
    NSInteger numberOfUnitsPerLevel = 144 / levels.count;

    for (ABARealmLevel *level in levels)
    {
        XCTAssertTrue(level.units.count > numberOfUnitsPerLevel);
        XCTAssertTrue(level.units.count >= (numberOfUnitsPerLevel * levels.count));

        for (ABAUnit *unit in level.units)
        {
            XCTAssertNotNil(unit.sectionSpeak);
            [DataController getProgressForSection:unit.sectionSpeak];
        }
    }
}

- (void)testCompletedSection
{
    
    RLMResults *levels = [LevelUnitController getAllLevels];
    XCTAssertTrue(levels.count > 0);
    for (ABARealmLevel *level in [LevelUnitController getAllLevels])
    {
        XCTAssertTrue(level.units.count > 0);
        for (ABAUnit *randomUnit in level.units)
        {
            dispatch_semaphore_t semaphoreSection = dispatch_semaphore_create(0);
            
            XCTAssertNotNil(randomUnit.sectionFilm);
            [DataController setCompletedSection:randomUnit.sectionFilm
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionSpeak);
            [DataController setCompletedSection:randomUnit.sectionSpeak
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionWrite);
            [DataController setCompletedSection:randomUnit.sectionWrite
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionInterpret);
            [DataController setCompletedSection:randomUnit.sectionInterpret
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionVideoClass);
            [DataController setCompletedSection:randomUnit.sectionVideoClass
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionExercises);
            [DataController setCompletedSection:randomUnit.sectionExercises
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionVocabulary);
            [DataController setCompletedSection:randomUnit.sectionVocabulary
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
            
            XCTAssertNotNil(randomUnit.sectionEvaluation);
            [DataController setCompletedSection:randomUnit.sectionEvaluation
                                completionBlock:^(NSError *error, id object) {
                                    if (error)
                                    {
                                        XCTFail(@"testCompletedSection Error: , %@", error);
                                    }
                                    else
                                    {
                                        dispatch_semaphore_signal(semaphoreSection);
                                    }
                                }];

            while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
            {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
            }
        }
    }
}

@end
