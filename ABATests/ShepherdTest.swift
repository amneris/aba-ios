//
//  ShepherdTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 2/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class ShepherdTest: XCTestCase {
    let sharedShepherd = ABAShepherdEditor.shared()
    
    override func setUp() {
        super.setUp()
        sharedShepherd?.resetCurrentEnvironments()
    }
    
    override func tearDown() {
        super.tearDown()
        sharedShepherd?.resetCurrentEnvironments()
    }
    
    func testShepherdStartup() {
        XCTAssertNotNil(sharedShepherd)
    }
    
    func testShepherdDefaultEnvironment() {
        let environment = sharedShepherd?.environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.self)
        XCTAssertNotNil(environment)
        XCTAssertTrue((environment?.isDefault)!)
        XCTAssertEqual(environment?.environmentName, "Production")
    }
    
    func testShepherdOnProduction() {
        let config = ABACoreShepherdConfiguration()
        sharedShepherd?.registerConfiguration(config)
        
        for environment in config.environments {
            if (environment as AnyObject).environmentName == "Production" {
                sharedShepherd?.setcurrentEnvironment(environment as! ABAShepherdEnvironment, for: config)
                break
            }
        }
        
        let environment = sharedShepherd?.environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.self)
        XCTAssertNotNil(environment)
        XCTAssertEqual(environment?.environmentName, "Production")
    }
    
    func testShepherdNonDefaultEnvironment() {
        let config = ABACoreShepherdConfiguration()
        let environmentIntName = "Integration"
        sharedShepherd?.registerConfiguration(config)
        
        for environment in config.environments {
            if (environment as AnyObject).environmentName == environmentIntName {
                sharedShepherd?.setcurrentEnvironment(environment as! ABAShepherdEnvironment, for: config)
                break
            }
        }
        
        let environment = sharedShepherd?.environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.self)
        
        XCTAssertNotNil(environment)
        XCTAssertEqual(environment?.environmentName, environmentIntName)
        XCTAssertFalse((environment?.isDefault)!)
    }
    
    func testShepherdNonDefaultEnvironmentAndReset() {
        let config = ABACoreShepherdConfiguration()
        let environmentIntName = "Integration"
        sharedShepherd?.registerConfiguration(config)
        
        for environment in config.environments {
            if (environment as AnyObject).environmentName == environmentIntName {
                sharedShepherd?.setcurrentEnvironment(environment as! ABAShepherdEnvironment, for: config)
                break
            }
        }
        
        let environmentInt = sharedShepherd?.environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.self)
        
        XCTAssertNotNil(environmentInt)
        XCTAssertEqual(environmentInt?.environmentName, environmentIntName)
        XCTAssertFalse((environmentInt?.isDefault)!)
        sharedShepherd?.resetCurrentEnvironments()
        
        let environmentProd = sharedShepherd?.environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.self)
        XCTAssertNotNil(environmentProd)
        XCTAssertEqual(environmentProd?.environmentName, "Production")
        XCTAssertTrue((environmentProd?.isDefault)!)
    }
    
}
