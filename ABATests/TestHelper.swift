//
//  TestHelper.swift
//  ABA
//
//  Created by MBP13 Jesus on 09/06/16. Copied from Olek.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

enum JsonFileReaderException: Swift.Error {
    case fileNotFound
    case generic(message: String)
}

@objc class TestHelper: NSObject {
    
    static func getJsonDataFromFile(_ name: String) throws -> Data {
        let fileContents: String? = try getJsonFromFile(name)
        if let data = fileContents!.data(using: String.Encoding.utf8) {
            return data
        }

        throw JsonFileReaderException.generic(message: "Error conversion to NSData")
    }

    static func getJsonFromFile(_ name: String) throws -> String {

        guard let path = Bundle.main.path(forResource: name, ofType: "json") else {
            print("Error finding file")
            throw JsonFileReaderException.fileNotFound
        }

        let fileContents = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
        return fileContents
    }
}
