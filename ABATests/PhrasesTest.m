//
//  PhrasesTest.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhrasesController.h"
#import "SectionController.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "VocabularyController.h"
#import "ABAUnit.h"
#import "ABAVocabularyPhrase.h"
#import "ABAUtilsHelper.h"
#import "ABABaseTestCase.h"
#import "Utils.h"


@interface PhrasesTest : ABABaseTestCase

@end

@implementation PhrasesTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [UserController loginUserWithEmail:TEST_USER_PREMIUM
                          withPassword:TEST_PASSWORD_PREMIUM
                       completionBlock:^(NSError *error, id object) {
                           XCTAssertNil(error);

                           dispatch_semaphore_signal(semaphore);
                       }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
}

- (void)testMarkPhraseAsListened
{
    __block ABAUnit *randomUnit = [ABAUtilsHelper getRandomUnit];

    dispatch_semaphore_t semaphoreSection = dispatch_semaphore_create(0);

    SectionController *sectionController = [SectionController new];
    
    [sectionController getAllSectionsForUnit:randomUnit
                                      contentBlock:^(NSError *error, id object) {

                                          if (!error)
                                          {
                                              dispatch_semaphore_signal(semaphoreSection);
                                              randomUnit = [LevelUnitController getUnitWithID:randomUnit.idUnit];
                                          }
                                          else
                                          {
                                              XCTFail(@"getAllSectionsForUnit Error: , %@", error);
                                          }

                                      }];

    while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    VocabularyController *vocabularyController = [VocabularyController new];

    ABAVocabularyPhrase *phrase = [vocabularyController getRandomPhraseForVocabularySection:randomUnit.sectionVocabulary];

    PhrasesController *phrasesController = [PhrasesController new];
    
    [phrasesController setPhrasesListened:@[phrase]
                               forSection:(ABASection *)randomUnit.sectionVocabulary
                       sendProgressUpdate:YES
                          completionBlock:^(NSError *error, id object) {
                              
                              if (!error)
                              {
                                  XCTAssertTrue([phrase.listened isEqualToNumber:[NSNumber numberWithInt:1]]);
                                  
                                  dispatch_semaphore_signal(semaphore);
                              }
                              else
                              {
                                  XCTFail(@"markPhraseListenedForVocabularySection Error: , %@", error);
                              }
                          }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)testMarkPhraseAsCompared
{
    __block ABAUnit *randomUnit = [ABAUtilsHelper getRandomUnit];

    dispatch_semaphore_t semaphoreSection = dispatch_semaphore_create(0);

    SectionController *sectionController = [SectionController new];
    
    [sectionController getAllSectionsForUnit:randomUnit
                                      contentBlock:^(NSError *error, id object) {

                                          if (!error)
                                          {
                                              dispatch_semaphore_signal(semaphoreSection);
                                              randomUnit = [LevelUnitController getUnitWithID:randomUnit.idUnit];
                                          }
                                          else
                                          {
                                              XCTFail(@"getAllSectionsForUnit Error: , %@", error);
                                          }

                                      }];

    while (dispatch_semaphore_wait(semaphoreSection, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    //    }

    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    VocabularyController *vocabularyController = [VocabularyController new];
    
    ABAVocabularyPhrase *phrase = [vocabularyController getRandomPhraseForVocabularySection:randomUnit.sectionVocabulary];
    
    PhrasesController *phrasesController = [PhrasesController new];
    
    [phrasesController setPhrasesDone:@[phrase]
                           forSection:(ABASection *)randomUnit.sectionVocabulary
                   sendProgressUpdate:YES
                      completionBlock:^(NSError *error, id object) {
                          
                          if (!error)
                          {
                              XCTAssertTrue([phrase.done isEqualToNumber:[NSNumber numberWithInt:1]]);
                              
                              dispatch_semaphore_signal(semaphore);
                          }
                          else
                          {
                              XCTFail(@"markPhraseDoneForVocabularySection Error: , %@", error);
                          }
                      }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

@end
