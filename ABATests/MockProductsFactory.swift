//
//  MockProductsFactory.swift
//  ABA
//
//  Created by MBP13 Jesus on 16/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

class MockProductsFactory {
    
    class func createMockProductWithoutDiscounts() -> SelligentProducts {
        
        let mock1 = MockSelligentSubscriptionProduct(originalIdentifier: "1M_FULL_PRICE", discountIdentifier: "1M_FULL_PRICE", days: "30", is2x1: "0")
        let mock2 = MockSelligentSubscriptionProduct(originalIdentifier: "6M_FULL_PRICE", discountIdentifier: "6M_FULL_PRICE", days: "180", is2x1: "0")
        let mock3 = MockSelligentSubscriptionProduct(originalIdentifier: "12M_FULL_PRICE", discountIdentifier: "12M_FULL_PRICE", days: "360", is2x1: "0")
        
        let listOfMocks = [mock1, mock2, mock3]
        
        return MockSelligentProduct(text: "blablabala", products: listOfMocks)
    }
    
    class func createMockProductWithDiscounts() -> SelligentProducts {
        
        let mock1 = MockSelligentSubscriptionProduct(originalIdentifier: "1M_FULL_PRICE", discountIdentifier: "1M_FULL_PRICE", days: "30", is2x1: "0")
        let mock2 = MockSelligentSubscriptionProduct(originalIdentifier: "6M_FULL_PRICE", discountIdentifier: "6M_FIRST_DISCOUNT", days: "180", is2x1: "0")
        let mock3 = MockSelligentSubscriptionProduct(originalIdentifier: "12M_FULL_PRICE", discountIdentifier: "12M_FIRST_DISCOUNT", days: "360", is2x1: "0")
        
        let listOfMocks = [mock1, mock2, mock3]
        
        return MockSelligentProduct(text: "blablabala", products: listOfMocks)
    }
    
    class func createMockProductWithoutText() -> SelligentProducts {
        
        let mock1 = MockSelligentSubscriptionProduct(originalIdentifier: "1M_FULL_PRICE", discountIdentifier: "1M_FULL_PRICE", days: "30", is2x1: "0")
        let mock2 = MockSelligentSubscriptionProduct(originalIdentifier: "6M_FULL_PRICE", discountIdentifier: "6M_FIRST_DISCOUNT", days: "180", is2x1: "0")
        let mock3 = MockSelligentSubscriptionProduct(originalIdentifier: "12M_FULL_PRICE", discountIdentifier: "12M_FIRST_DISCOUNT", days: "360", is2x1: "0")
        
        let listOfMocks = [mock1, mock2, mock3]
        
        return MockSelligentProduct(text: nil, products: listOfMocks)
    }
    
    class func createMockProductWithoutProducts() -> SelligentProducts {
        return MockSelligentProduct(text: "blablablas", products: nil)
    }
}
