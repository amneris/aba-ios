//
//  EvaluationControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class EvaluationControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteEvaluationSection() {

        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteEvaluationSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)

        let evaluationController:EvaluationController = EvaluationController()
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        evaluationController.setCompletedSection(randomUnit.sectionEvaluation as ABAEvaluation) { error, object in
            let evaluation:ABAEvaluation = object as! ABAEvaluation
            
            XCTAssertEqual(evaluation.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
    
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testCompleteAllExercisesForEvaluationSection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteAllExercisesForEvaluationSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)

        let evaluationSection = randomUnit.sectionEvaluation as ABAEvaluation
        var evaluationQuestionAnswers = 0
        
        for i in 0..<evaluationSection.content.count {
            let evaluationQuestion = evaluationSection.content[i] 
            
            if let evaluationOption = TestUtils.getRandomOption(evaluationQuestion.options as! RLMArray<RLMObject>) {
                EvaluationController.evaluationQuestionAnswered(with: evaluationOption, completionBlock: { error, object in
                    XCTAssertNil(error, "error should be nil")
                    
                    evaluationQuestionAnswers += 1
                })
            }
        }
        
        XCTAssertEqual(evaluationQuestionAnswers, EvaluationController.getEvaluationAnswers(evaluationSection).count, "should be the same")
        XCTAssertEqual(EvaluationController.getEvaluationAnswers(evaluationSection).count, Int(evaluationSection.content.count), "should be the same")

        EvaluationController.reloadEvaluationAnswers(evaluationSection) { error, object in
            XCTAssertNil(error, "error should be nil")
            XCTAssertEqual(EvaluationController.getEvaluationAnswers(evaluationSection).count, 0, "should be the same")
        }
        
    }
}
