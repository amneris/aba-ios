//
//  RepoUserDAO.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest
import Unbox

@testable import ABA

class RepoUserDAOTest: XCTestCase {

    override func setUp() {
        super.setUp()
        UserDataModelDAO.deleteAllUsers()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testReadAndSaveUser() {
        let repoUserFromDB = UserDataModelDAO.getCurrentUser()
        XCTAssertNil(repoUserFromDB)

        let repoUser = UserDataModelTestFactory.createRepoUser()
        UserDataModelDAO.saveUser(repoUser)

        if let repoUserFromDB = UserDataModelDAO.getCurrentUser() {
            XCTAssertNotNil(repoUserFromDB)
            XCTAssertEqual(repoUserFromDB.email, repoUser.email)
        }
        else {
            XCTFail()
        }
    }

    func testDeleteUser() {
        let repoUser = UserDataModelTestFactory.createRepoUser()
        UserDataModelDAO.saveUser(repoUser)

        var repoUserFromDB = UserDataModelDAO.getCurrentUser()
        XCTAssertNotNil(repoUserFromDB)

        UserDataModelDAO.deleteAllUsers()
        repoUserFromDB = UserDataModelDAO.getCurrentUser()
        XCTAssertNil(repoUserFromDB)
    }
}

struct UserDataModelTestFactory {

    static let userTestDict = [
        "email": "pepe@pepe.com",
        "userId": "123",
        "name": "pepe",
        "token": "lskdjflaj9sdljsd",
        "userType": "2"
    ]
    
    static func createRepoUser() -> UserDataModel {
        let user = try! Unboxer.performCustomUnboxing(dictionary: userTestDict, closure: { unboxer -> UserDataModel? in
            return try! UserDataModel(unboxer: unboxer)
        })
        return user
    }
}
