//
//  ABABaseTestCase.m
//  ABA
//
//  Created by Oriol Vilaró on 20/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABABaseTestCase.h"
#import "UserController.h"
#import "Utils.h"
#import <Realm/Realm.h>
#import "SectionController.h"
#import "ABAUnit.h"
#import "LevelUnitController.h"

#import "ABAFilm.h"
#import "ABAVideoClass.h"
#import "ABAWrite.h"
#import "ABAInterpret.h"
#import "ABASpeak.h"
#import "ABAVocabulary.h"
#import "ABAExercises.h"
#import "ABAEvaluation.h"

#import "EvaluationController.h"
#import "SpeakController.h"
#import "WriteController.h"
#import "InterpretController.h"
#import "ExercisesController.h"
#import "VocabularyController.h"
#import "FilmController.h"

// Shepherd
#import "ABACoreShepherdEnvironment.h"
#import "ABACoreShepherdConfiguration.h"
#import "ABAShepherdEditor.h"


@implementation ABABaseTestCase

- (void)setUp
{
    [super setUp];
    
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.inMemoryIdentifier = self.name;
    [RLMRealmConfiguration setDefaultConfiguration:config];

    [self deleteFiles];
    
    ABAShepherdEditor *sharedShepherd = [ABAShepherdEditor shared];
    ABACoreShepherdConfiguration *shepherdConfig = [[ABACoreShepherdConfiguration alloc] init];
    [sharedShepherd registerConfiguration:shepherdConfig];
    
    for (id<ABAShepherdEnvironment> environment in shepherdConfig.environments)
    {
        if ([environment.environmentName isEqualToString:@"Integration"])
        {
            [sharedShepherd setcurrentEnvironment:environment forConfiguration:shepherdConfig];
            break;
        }
    }
}

- (void)tearDown
{
    [super tearDown];
    [self deleteFiles];
}

- (void)logInTestUser {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    [UserController loginUserWithEmail:TEST_USER_PREMIUM
                          withPassword:TEST_PASSWORD_PREMIUM
                       completionBlock:^(NSError *error, id object) {
                           
                           XCTAssertNil(error);
                           
                           dispatch_semaphore_signal(semaphore);
                       }];
    
    while(dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW)) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)deleteFiles
{
    // Delete Realm files
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
}

- (void)completeAllSectionsForUnit:(ABAUnit*)unit
{
    __block ABAUnit *randomUnit = unit;
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    SectionController *sectionController = [SectionController new];
    
    [sectionController getAllSectionsForUnit:randomUnit
                                contentBlock:^(NSError *error, id object) {
                                    
                                    if (!error)
                                    {
                                        dispatch_semaphore_signal(semaphore);
                                        randomUnit = [LevelUnitController getUnitWithID:randomUnit.idUnit];
                                    }
                                    else
                                    {
                                        XCTFail(@"getAllSectionsForUnit Error: , %@", error);
                                    }
                                    
                                }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    FilmController *filmController = [FilmController new];
    
    [filmController setCompletedSection:randomUnit.sectionFilm
                        completionBlock:^(NSError *error, id object) {
                            
                            if (!error)
                            {
                                ABAFilm *film = (ABAFilm *)object;
                                
                                XCTAssertTrue([film.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                
                                dispatch_semaphore_signal(semaphore);
                            }
                            else
                            {
                                XCTFail(@"sendCompletedForFilmSection Error: , %@", error);
                            }
                            
                        }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    SpeakController *speakController = [SpeakController new];
    
    [speakController sendAllPhrasesDoneForSpeakSection:randomUnit.sectionSpeak
                                       completionBlock:^(NSError *error, id object) {
                                           
                                           if (!error)
                                           {
                                               ABASpeak *speak = (ABASpeak *)object;
                                               
                                               XCTAssertTrue([speak.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                               
                                               dispatch_semaphore_signal(semaphore);
                                           }
                                           else
                                           {
                                               XCTFail(@"sendAllPhrasesDoneForSpeakSection Error: , %@", error);
                                           }
                                           
                                       }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    WriteController *writeController = [WriteController new];
    
    [writeController sendAllPhrasesDoneForWriteSection:randomUnit.sectionWrite
                                       completionBlock:^(NSError *error, id object) {
                                           if (!error)
                                           {
                                               ABAWrite *write = (ABAWrite *)object;
                                               
                                               XCTAssertTrue([write.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                               
                                               dispatch_semaphore_signal(semaphore);
                                           }
                                           else
                                           {
                                               XCTFail(@"sendAllPhrasesDoneForWriteSection Error: , %@", error);
                                           }
                                           
                                       }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    [filmController setCompletedSection:randomUnit.sectionVideoClass
                        completionBlock:^(NSError *error, id object) {
                            
                            if (!error)
                            {
                                ABAVideoClass *videoClass = (ABAVideoClass *)object;
                                
                                XCTAssertTrue([videoClass.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                
                                dispatch_semaphore_signal(semaphore);
                            }
                            else
                            {
                                XCTFail(@"sendCompletedForFilmSection Error: , %@", error);
                            }
                            
                        }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    VocabularyController *vocabularyController = [VocabularyController new];
    
    [vocabularyController sendAllPhrasesDoneForVocabularySection:randomUnit.sectionVocabulary
                                                 completionBlock:^(NSError *error, id object) {
                                                     if (!error)
                                                     {
                                                         ABAVocabulary *vocabulary = (ABAVocabulary *)object;
                                                         
                                                         XCTAssertTrue([vocabulary.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                                         
                                                         dispatch_semaphore_signal(semaphore);
                                                     }
                                                     else
                                                     {
                                                         XCTFail(@"sendAllPhrasesDoneForVocabularySection Error: , %@", error);
                                                         
                                                         dispatch_semaphore_signal(semaphore);
                                                         
                                                     }
                                                     
                                                 }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    InterpretController *interpretController = [InterpretController new];
    
    [interpretController sendAllPhrasesDoneForInterpretSection:randomUnit.sectionInterpret
                                               completionBlock:^(NSError *error, id object) {
                                                   if (!error)
                                                   {
                                                       ABAInterpret *interpret = (ABAInterpret *)object;
                                                       
                                                       XCTAssertTrue([interpret.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                                       
                                                       dispatch_semaphore_signal(semaphore);
                                                   }
                                                   else
                                                   {
                                                       XCTFail(@"sendAllPhrasesDoneForInterpretSection Error: , %@", error);
                                                       
                                                       dispatch_semaphore_signal(semaphore);
                                                   }
                                                   
                                               }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    semaphore = dispatch_semaphore_create(0);
    
    EvaluationController *evaluationController = [EvaluationController new];
    
    [evaluationController setCompletedSection:randomUnit.sectionEvaluation
                              completionBlock:^(NSError *error, id object) {
                                  
                                  if (!error)
                                  {
                                      ABAEvaluation *evaluation = (ABAEvaluation *)object;
                                      
                                      XCTAssertTrue([evaluation.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                      
                                      dispatch_semaphore_signal(semaphore);
                                  }
                                  else
                                  {
                                      XCTFail(@"sendCompletedForEvaluationSection Error: , %@", error);
                                  }
                                  
                              }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
    
    
    ExercisesController *exercicesController = [ExercisesController new];
    while ([exercicesController getCurrentABAExercisesQuestionForABAExercises:randomUnit.sectionExercises] != nil) {
        ABAExercisesQuestion *exercicesQuestion = [exercicesController getCurrentABAExercisesQuestionForABAExercises:randomUnit.sectionExercises];
        
        semaphore = dispatch_semaphore_create(0);

        [exercicesController setBlankPhrasesDoneForExercisesQuestion:exercicesQuestion forABAExercises:randomUnit.sectionExercises completionBlock:^(NSError *error, id object) {
            
            dispatch_semaphore_signal(semaphore);
            
        }];
        
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
        {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
        }

    }

    
    [exercicesController setCompletedSection:randomUnit.sectionExercises
                             completionBlock:^(NSError *error, id object) {
                                 
                                 if (!error)
                                 {
                                     ABAExercises *exercices = (ABAExercises *)object;
                                     
                                     XCTAssertTrue([exercices.completed isEqualToNumber:[NSNumber numberWithInt:1]]);
                                     
                                     dispatch_semaphore_signal(semaphore);
                                 }
                                 else
                                 {
                                     XCTFail(@"sendCompletedForExercicesSection Error: , %@", error);
                                 }
                                 
                             }];
    
    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

@end
