//
//  VideoClassControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class VideoClassControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteVideoClassSection() {
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteVideoClassSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let filmController: FilmController = FilmController()
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        filmController.setCompletedSection(randomUnit.sectionVideoClass) { error, object  in
            let videoClass:ABAVideoClass = object as! ABAVideoClass
            
            XCTAssertEqual(videoClass.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testDownloadSubtitlesForVideoClass() {
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testDownloadSubtitlesForVideoClass")
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let filmController: FilmController = FilmController()
        let videoClassSection: ABAVideoClass = randomUnit.sectionVideoClass
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        filmController.downloadSubtitlesVideo(videoClassSection, forSectionType:kABASectionTypes.abaVideoClassSectionType) { error, object in
            XCTAssertNil(error, "error should be nil")
            
            XCTAssertTrue(filmController.checkIfSubtitleExists(videoClassSection, forSectionType: kABASectionTypes.abaVideoClassSectionType, for: kABAVideoSubtitleType.abaSubtitleTypeEnglish), "Files not exist")
            XCTAssertTrue(filmController.checkIfSubtitleExists(videoClassSection, forSectionType: kABASectionTypes.abaVideoClassSectionType, for: kABAVideoSubtitleType.abaSubtitleTypeTranslate), "Files not exist")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
