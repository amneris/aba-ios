//
//  IpAddressRepositoryTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import XCTest
import OHHTTPStubs
import RxBlocking

@testable import ABA

class IpAddressRepositoryTest: XCTestCase {

    func testIpAddress() {
        do {
            let ip = try PublicIpRepository.publicIpAPICall().toBlocking().single()
            XCTAssertNotNil(ip)
            XCTAssertFalse(ip!.isEmpty)
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

    func testIpAddressFailure() {
        // Adding server failure for ip network call
        stub(condition: isPath("/api/abaEnglishApi/iprecognition")) { _ in
            return OHHTTPStubsResponse(data: Data(), statusCode: 500, headers: nil).responseTime(1)
        }

        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }

        do {
            let _ = try PublicIpRepository.publicIpAPICall().toBlocking().single()
            XCTFail()
        }
        catch let error {
            if let publicIpError = error as? PublicIpRepositoryError, publicIpError == PublicIpRepositoryError.errorGettingIP {
                // Expected result
            }
            else {
                XCTFail()
            }
        }
    }
}
