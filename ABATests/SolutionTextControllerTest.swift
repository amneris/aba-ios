//
//  SolutionTextControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class SolutionTextControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // Test para comprobar que el validador de soluciones de Escribe y Ejercicios cumple con todas las reglas y excepciones
    func testSolutionTextValidator() {
        
        let solutionTextController = SolutionTextController()
        
        var phrase = "I'm very happy tonight, tom"
        var correctphrase = "I am very happy tonight, Tom"
        XCTAssertFalse(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "I´m very happy tonight, Tom"
        correctphrase = "I'm very happy tonight, Tom"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "It is monday"
        correctphrase = "It´s Monday"
        XCTAssertFalse(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "It´s Monday"
        correctphrase = "It is Monday"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Today´s February and I like tomatoes in the street"
        correctphrase = "Today is February and I like tomatoes in the street"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Today´s february and I like tomatoes in the street"
        correctphrase = "Today is February and I like tomatoes in the street"
        XCTAssertFalse(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Today is February and i like tomatoes in the street"
        correctphrase = "Today´s February and I like tomatoes in the street"
        XCTAssertFalse(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Hello, is this a test phrase with question mark?"
        correctphrase = "Hello, is this a test phrase with question mark?"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Hello is this a test phrase with question mark"
        correctphrase = "Hello, is this a test phrase with question mark?"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "They´re happy!"
        correctphrase = "They are happy!"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Cote cafe cafes facades"
        correctphrase = "Côte café cafés façades"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
        
        phrase = "Côte café cafés façades"
        correctphrase = "Côte café cafés façades"
        XCTAssertTrue(solutionTextController.testCheckPhrase(phrase, withCorrectPhrase: correctphrase))
    }
}
