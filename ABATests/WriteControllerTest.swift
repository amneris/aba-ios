//
//  WriteControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class WriteControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteWriteSection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteWriteSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let writeController = WriteController()
        let writeSection = randomUnit.sectionWrite
        
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        writeController.setCompletedSection(writeSection) { (error, object) -> Void in
            
            let write = object as! ABAWrite
            
            XCTAssertEqual(write.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testGetPhrasesFromWriteSection() {

        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetPhrasesFromWriteSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        XCTAssertGreaterThan(randomUnit.sectionWrite.content.count, 0);
    }
    
    func testToGetAllAudiosIdForWritePhrases(){
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testToGetAllAudiosIdForWritePhrases")
        let sectionController: SectionController = SectionController()
        let writeController = WriteController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        let writeSection = LevelUnitController.getUnitWithID(randomUnit.idUnit).sectionWrite
        let writeAudios = writeController.getAllAudioIDs(forWriteSection: writeSection)
        
        XCTAssertTrue((writeAudios?.count)!>0)
        XCTAssertEqual(writeAudios?.count, Int(writeController.getTotalElements(for: writeSection)/2))
    }
    
    func testCompleteAllPhrasesFromWriteSection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteAllPhrasesFromWriteSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        let writeSection = randomUnit.sectionWrite
        
        let writeController = WriteController()
        let phrasesController = PhrasesController()
        
        let writePhrasesArray = writeController.getPhrasesDataSource(fromSection: writeSection)!
        
        for i in 0..<writePhrasesArray.count {
            
            let currentPhrase = writePhrasesArray[i] as! ABAPhrase
            
            phrasesController.setPhrasesDone([currentPhrase], for: writeSection, sendProgressUpdate: false, completionBlock: { (error, object) -> Void in
                
                XCTAssertTrue(currentPhrase.done.boolValue  && currentPhrase.listened.boolValue);
            })
            
            XCTAssertTrue(currentPhrase.done.boolValue  && currentPhrase.listened.boolValue);
        }
        
        XCTAssertTrue(writeController.isSectionCompleted(writeSection))
        XCTAssertEqual(writeController.getElementsCompleted(for: writeSection), writeController.getTotalElements(for: writeSection))
        XCTAssertEqual(Int(writeController.getProgressFor(writeSection)), 100)
        XCTAssertEqual(writeController.getPercentageFor(writeSection), "100%")
    }
}
