//
//  ABARealmUserTest.m
//  ABA
//
//  Created by Jaume Cornadó Panadés on 7/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserController.h"
#import "ABARealmUser.h"
#import "Utils.h"
#import "ABABaseTestCase.h"


@interface ABARealmUserTest : ABABaseTestCase

@end

@implementation ABARealmUserTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
}

- (void)testUserIsLogged
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [UserController loginUserWithEmail:TEST_USER_PREMIUM
                          withPassword:TEST_PASSWORD_PREMIUM
                       completionBlock:^(NSError *error, id object) {
                           XCTAssertNil(error);
                           XCTAssertTrue([UserController isUserLogged]);
                           XCTAssertTrue([[[UserController currentUser] email] isEqualToString:TEST_USER_PREMIUM]);
                           dispatch_semaphore_signal(semaphore);
                       }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)testLogout
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [UserController loginUserWithEmail:TEST_USER_PREMIUM
                          withPassword:TEST_PASSWORD_PREMIUM
                       completionBlock:^(NSError *error, id object) {
                           XCTAssertNil(error);
                           dispatch_semaphore_signal(semaphore);
                       }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }

    dispatch_semaphore_t semaphore2 = dispatch_semaphore_create(0);

    [UserController logoutWithBlock:^(NSError *error) {

        XCTAssertNil(error);
        XCTAssertNil([UserController currentUser]);
        dispatch_semaphore_signal(semaphore2);
    }];

    while (dispatch_semaphore_wait(semaphore2, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

- (void)testRegisterEmailExists
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

    [UserController registerUserWithEmail:TEST_USER_PREMIUM
                             withPassword:@"123456"
                                 withName:@"iOS Test User"
                          completionBlock:^(NSError *error, id object) {

                              XCTAssertNotNil(error);

                              XCTAssertTrue(error.code == CODE_EMAIL_EXIST_ERROR);

                              dispatch_semaphore_signal(semaphore);
                          }];

    while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:WS_TIMEOUT]];
    }
}

@end
