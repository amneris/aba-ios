//
//  FilmControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class FilmControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testCompleteAbaFilmSection() {
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteAbaFilmSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let filmController: FilmController = FilmController()
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        filmController.setCompletedSection(randomUnit.sectionFilm) { error, object  in
            let film:ABAFilm = object as! ABAFilm
            
            XCTAssertEqual(film.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testDownloadSubtitlesForABAFilm() {
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testDownloadSubtitlesForABAFilm")
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)

        let filmController: FilmController = FilmController()
        let filmSection: ABAFilm = randomUnit.sectionFilm
        let expectation2 = self.expectation(description: "semaphoreSection")

        filmController.downloadSubtitlesVideo(filmSection, forSectionType:kABASectionTypes.abaFilmSectionType) { error, object in
           XCTAssertNil(error, "error should be nil")
            
           XCTAssertTrue(filmController.checkIfSubtitleExists(filmSection, forSectionType: kABASectionTypes.abaFilmSectionType, for: kABAVideoSubtitleType.abaSubtitleTypeEnglish), "Files not exist")
           XCTAssertTrue(filmController.checkIfSubtitleExists(filmSection, forSectionType: kABASectionTypes.abaFilmSectionType, for: kABAVideoSubtitleType.abaSubtitleTypeTranslate), "Files not exist")
           expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}

