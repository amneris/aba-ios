//
//  DownloadControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class DownloadControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDownloadUnit() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        XCTAssertTrue(DownloadController.shouldDownloadOfflineMode(for: randomUnit), "Should not exist any downloaded data")
        
        let expectation = self.expectation(description: "testDownloadUnit")
        
        let downloadController = DownloadController(unit: randomUnit) { (error, progress, didFinish) -> Void in
            
            XCTAssertNil(error, "Error should be nil")
            
            if didFinish {
                
                XCTAssertFalse(DownloadController.shouldDownloadOfflineMode(for: randomUnit), "Should exist all downloaded data")
                
                DownloadController.removeDownloadedFiles()
                
                XCTAssertTrue(DownloadController.shouldDownloadOfflineMode(for: randomUnit), "Should not exist any downloaded data")

                expectation.fulfill()
            }

        }
        
        downloadController?.startDownload()
        
    
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

    
    }
}
