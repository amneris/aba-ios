//
//  SpeakControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class SpeakControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteSpeakSection() {
     
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }

        let expectation = self.expectation(description: "testCompleteSpeakSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)

        let speakController = SpeakController()
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        speakController.setCompletedSection(randomUnit.sectionSpeak) { error, object in
            let speak = object as! ABASpeak
            
            XCTAssertEqual(speak.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testGetPhrasesFromSpeakSection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetPhrasesFromSpeakSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        XCTAssertGreaterThan(randomUnit.sectionSpeak.content.count, 0);
    }

    func testCompleteAllPhrasesFromSpeakSection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteSpeakSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        let speakSection = randomUnit.sectionSpeak
        
        let speakController = SpeakController()
        let phrasesController = PhrasesController()
        
        while speakController.getCurrentPhrase(fromSection: speakSection) != nil {
            
            var currentPhrase = speakController.getCurrentPhrase(fromSection: speakSection)!
            
            if (currentPhrase.isKind(of: ABASpeakPhrase.self)) {
                if (currentPhrase.done == nil) && currentPhrase.subPhrases.count > 0 {
                    for i in 0..<currentPhrase.subPhrases.count {
                        let subPhrase = currentPhrase.subPhrases.object(at: i)
                        if (subPhrase.done == nil) && subPhrase.text != "" {
                            currentPhrase = subPhrase
                            break;
                        }
                    }
                }
            }
            
            XCTAssertEqual(speakController.phrase(withID: currentPhrase.idPhrase, andPage: currentPhrase.page, on: speakSection).idPhrase, currentPhrase.idPhrase)
            
            phrasesController.setPhrasesDone([currentPhrase], for: speakSection, sendProgressUpdate: false, completionBlock: { (error, object) -> Void in    
                XCTAssertTrue((currentPhrase.done.boolValue)  && (currentPhrase.listened.boolValue));
            })
            
            XCTAssertTrue((currentPhrase.done.boolValue) && (currentPhrase.listened.boolValue));
        }
        
        XCTAssertTrue(speakController.isSectionCompleted(speakSection))
        XCTAssertEqual(speakController.getElementsCompleted(for: speakSection), speakController.getTotalElements(for: speakSection))
        XCTAssertEqual(Int(speakController.getProgressFor(speakSection)), 100)
        XCTAssertEqual(speakController.getPercentageFor(speakSection), "100%")

    }
}
