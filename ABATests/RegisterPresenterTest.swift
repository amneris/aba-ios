//
//  RegiserPresenterTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 09/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import OHHTTPStubs

@testable import ABA

class RegisterPresenterTest: XCTestCase {

    var tracker = MockTracker()
    var registerPresenter: RegisterPresenter!
    var mockView: MockRegisterViewInput!

    override func setUp() {
        super.setUp()
        RealmTestHelper.deleteCurrentUser()

        mockView = MockRegisterViewInput()
        registerPresenter = RegisterPresenter(tracker: tracker)
        registerPresenter.interactor = RegisterInteractor(repository: RegisterRepository())
        registerPresenter.view = mockView
    }

    func testCredentialsValidation() {

        let emptyNameValidation = registerPresenter.parameterValidation("", email: "", pass: "pass")
        XCTAssertTrue(emptyNameValidation == .EmptyName)

        let nilNameValidation = registerPresenter.parameterValidation("    ", email: "", pass: "pass")
        XCTAssertTrue(nilNameValidation == .EmptyName)

        let emptyEmailValidation = registerPresenter.parameterValidation("pepe", email: "", pass: "pass")
        XCTAssertTrue(emptyEmailValidation == .EmptyEmail)

        let wrongEmailValidation1 = registerPresenter.parameterValidation("pepe", email: "jespejo", pass: "pass")
        XCTAssertEqual(wrongEmailValidation1, .WrongEmailFormat)

        let wrongEmailValidation2 = registerPresenter.parameterValidation("pepe", email: "jespejo@abaenglish", pass: "pass")
        XCTAssertEqual(wrongEmailValidation2, .WrongEmailFormat)

        let wrongEmailValidation3 = registerPresenter.parameterValidation("pepe", email: "jespejoabaenglish.com", pass: "pass")
        XCTAssertEqual(wrongEmailValidation3, .WrongEmailFormat)

        let emptyPasswordValidation = registerPresenter.parameterValidation("pepe", email: "jespejo@abaenglish.com", pass: "")
        XCTAssertTrue(emptyPasswordValidation == .EmptyPassword)

        let properCredentials = registerPresenter.parameterValidation("pepe", email: "jesp@abaenglish.com", pass: "asdfasdf")
        XCTAssertTrue(properCredentials == .Success)
    }

    func testRegister() {

        let expectation = self.expectation(description: "Perform registration")

        mockView.callback = {
            if self.mockView.emailRepeatedErrorShown || self.mockView.genericErrorShown || self.mockView.facebookErrorShown {
                XCTFail()
            }
            if self.mockView.registrationConfirmation && self.mockView.loadingIndicatorShown && self.mockView.loadingIndicartorHidden && self.tracker.registrationWithEmailTracked {
                expectation.fulfill()
            }
        }

        do {
            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()
            registerPresenter.registerUser(name: "pepe", email: generatedEmail, password: "123456")
            waitForExpectations(timeout: 25) { (error) in
                // TODO: Dispose?
                print("testRegister error: \(error)")
            }
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

    func testEmptyName() {

        let expectation = self.expectation(description: "Perform registration")

        mockView.callback = {

            if self.tracker.registrationWithEmailTracked {
                XCTFail()
            }

            if self.mockView.registrationConfirmation {
                XCTFail()
            }

            // Loading dialog should not appear since the register process must stop at validation point
            if (self.mockView.loadingIndicatorShown || self.mockView.loadingIndicartorHidden) {
                XCTFail()
            }

            if self.mockView.emailRepeatedErrorShown || self.mockView.genericErrorShown || self.mockView.facebookErrorShown {
                XCTFail()
            }

            // only error admited is .EmptyName
            if let error = self.mockView.validationError {
                if error == .EmptyName {
                    expectation.fulfill()
                }
                else {
                    XCTFail()
                }
            }
        }

        do {
            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()
            registerPresenter.registerUser(name: "   ", email: generatedEmail, password: "123456")
            waitForExpectations(timeout: 5) { (error) in
                // TODO: Dispose?
                print("testRegister error: \(error)")
            }
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

    func testRegistrationBlackListed() {
        // Adding server failure
        stub(condition: isHost("api.int.aba.land")) { _ in
            return OHHTTPStubsResponse(error: NSError(domain: "fake domain error", code: 700, userInfo: nil)).responseTime(2)
        }

        defer {
            // Cancelling server failure
            OHHTTPStubs.removeAllStubs()
        }

        let expectation = self.expectation(description: "Perform registration")
        mockView.callback = {

            if self.tracker.registrationWithEmailTracked {
                XCTFail()
            }

            if self.mockView.registrationConfirmation {
                XCTFail()
            }

            if let _ = self.mockView.validationError {
                // we should not get registration error proccess
                XCTFail()
            }

            if self.mockView.emailRepeatedErrorShown || self.mockView.facebookErrorShown {
                XCTFail()
            }

            if self.mockView.genericErrorShown {
                
                if !self.mockView.loadingIndicatorShown || !self.mockView.loadingIndicartorHidden {
                    XCTFail()
                }
                
                expectation.fulfill()
            }
        }

        do {
            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()
            registerPresenter.registerUser(name: "Register presenter test", email: generatedEmail, password: "123456")
            waitForExpectations(timeout: 5) { (error) in
                // TODO: Dispose?
                print("testRegister error: \(error)")
            }
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

    func testRegistrationWithSameMail() {

        do {
            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()

            let firstRegistrationExpectation = expectation(description: "Perform first registration")
            mockView.callback = {
                if self.mockView.validationError != nil || self.mockView.emailRepeatedErrorShown || self.mockView.genericErrorShown || self.mockView.facebookErrorShown {
                    XCTFail()
                }
                if self.mockView.registrationConfirmation && self.mockView.loadingIndicatorShown && self.mockView.loadingIndicartorHidden && self.tracker.registrationWithEmailTracked {
                    firstRegistrationExpectation.fulfill()
                }
            }

            registerPresenter.registerUser(name: "Register presenter test", email: generatedEmail, password: "123456")
            waitForExpectations(timeout: 25) { (error) in
                // TODO: Dispose?
                print("testRegister error: \(error)")
            }

            self.tracker.registrationWithEmailTracked = false
            let secondRegistrationExpectation = expectation(description: "Perform second registration")
            mockView.callback = {
                if self.mockView.genericErrorShown || self.mockView.facebookErrorShown {
                    XCTFail()
                }
                
                if self.mockView.emailRepeatedErrorShown {
                    secondRegistrationExpectation.fulfill()
                }
            }

            // Registering the user again
            registerPresenter.registerUser(name: "Register presenter test (again)", email: generatedEmail, password: "123456")
            waitForExpectations(timeout: 25) { (error) in
                // TODO: Dispose?
                print("testRegister error: \(error)")
            }
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }

    }
}
