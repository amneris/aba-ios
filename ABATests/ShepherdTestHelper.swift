//
//  ShepherdTestHelper.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

enum ShepherdTestHelperError: Swift.Error {
    var description: String { return "ShepherdTestHelperError.." }
    case abaConfigDoesNotExist
    case integrationEnvironmentDoesNotExist
}

class ShepherdTestHelper {
    
    class func forceIntegration() throws {
        
        // Ensuring aba core config exists
        if ABAShepherdEditor.shared().configurations.count == 0 {
            let abaCoreConfig = ABACoreShepherdConfiguration()
            ABAShepherdEditor.shared().registerConfiguration(abaCoreConfig)
        }
        
        // taking aba core from config list
        var abaCoreConfig: ABACoreShepherdConfiguration?
        for configuration in ABAShepherdEditor.shared().configurations {
            if let config = configuration as? ABACoreShepherdConfiguration {
                abaCoreConfig = config
            }
        }
        
        if abaCoreConfig == nil {
            throw ShepherdTestHelperError.abaConfigDoesNotExist
        }
        
        // searching Integration environment and setting it
        for enviroment in abaCoreConfig?.environments as! [ABAShepherdEnvironment] {
            if enviroment.environmentName == "Integration" {
                ABAShepherdEditor.shared().setcurrentEnvironment(enviroment, for: abaCoreConfig)
                return;
            }
        }
        
        throw ShepherdTestHelperError.integrationEnvironmentDoesNotExist
    }
}
