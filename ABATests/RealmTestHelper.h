//
//  RealmHelper.h
//  ABA
//
//  Created by MBP13 Jesus on 17/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RealmTestHelper : NSObject

+ (void)deleteCurrentUser;

+ (BOOL)isUserCreated;
+ (BOOL)areUnitsCreated;
+ (BOOL)areLevelsCreated;

@end
