//
//  TestUtils.swift
//  ABA
//
//  Created by Oriol Vilaró on 2/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class TestUtils {
    
    static func generateEmail() -> String {
        
        let mail = (self.randomStringWithLength(20) as String) + "@abaenglish.com"
        
        return mail
    }
    
    static func generateFacebookID() -> String {
        
        return self.randomNumericalStringWithLength(16) as String
    }
    
    
    static func getRandomPhrase(_ phraseArrayList: Array<ABAPhrase>) -> ABAPhrase {
        
        return ABAPhrase()
    }
    
    static func getRandomOfStringArray(_ stringArrayList: Array<String>) -> String {
        
        return stringArrayList[getRandomForInteger(stringArrayList.count-1)]
    }
    
    static func getRandomOption(_ optionsArrayList: RLMArray<RLMObject>) -> ABAEvaluationOption? {
        if (optionsArrayList.count > 0) {
            return optionsArrayList.object(at: UInt(getRandomForInteger(Int(optionsArrayList.count-1)))) as? ABAEvaluationOption
        }
        
        return nil
    }
    
    static func getRandomForInteger(_ integer: Int) -> Int {
        return Int(arc4random_uniform(UInt32(integer)))
    }
    
    static func getRandomStringFromListOfString(_ stringArrayList: Array<String>) -> String {
        let randomIndex = Int(arc4random_uniform(UInt32(stringArrayList.count)))
        return stringArrayList[randomIndex]
    }
    
    static func randomStringWithLength (_ len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0 ..< len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    static func randomNumericalStringWithLength (_ len : Int) -> NSString {
        
        let letters : NSString = "0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0 ..< len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
}
