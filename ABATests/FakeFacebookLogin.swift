//
//  FakeFacebookLogin.swift
//  ABA
//
//  Created by MBP13 Jesus on 19/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

@testable import ABA

class FakeFacebookLogin: FacebookInteractorInput {
    
    let generatedEmail = TestUtils.generateEmail()
    let generatedFacebookId = TestUtils.generateFacebookID()
    
    func loginWithFacebook() -> Observable<FacebookUser> {
        
        let facebookUser = FacebookUser(facebookId: generatedFacebookId, email: generatedEmail, firstName: "Test", lastName: "Another Test", gender: "male", imageUrl: "")
        
        return Observable.just(facebookUser)
    }
}