//
//  ABABaseTestCase.h
//  ABA
//
//  Created by Oriol Vilaró on 20/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <XCTest/XCTest.h>

@class ABAUnit;

@interface ABABaseTestCase : XCTestCase

- (void)logInTestUser;
- (void)completeAllSectionsForUnit:(ABAUnit*)unit;

@end
