//
//  ExerciseControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class ExerciseControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteExerciseSection() {
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteExerciseSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let exerciseController:ExercisesController = ExercisesController()
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        exerciseController.setCompletedSection(randomUnit.sectionExercises as ABAExercises) { error, object in
            let exercise:ABAExercises = object as! ABAExercises
            
            XCTAssertEqual(exercise.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

    }
    
    func testGetPhrasesFromExercisesSection() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteExerciseSection")
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        let testExercisesSection:ABAExercises = LevelUnitController.getUnitWithID(randomUnit.idUnit).sectionExercises
        let expectation2 = self.expectation(description: "semaphoreSection")
        
        XCTAssertGreaterThan(testExercisesSection.exercisesGroups.count, 0);
        expectation2.fulfill()
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }

    }
    
    func testCompleteAllPhrasesFromExercisesSection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteAllPhrasesFromExercisesSection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let exerciseController:ExercisesController = ExercisesController()
        let exerciseSection:ABAExercises = randomUnit.sectionExercises
        
        while exerciseController.getCurrentABAExercisesQuestion(for: exerciseSection) != nil {
            let exerciseQuestion = exerciseController.getCurrentABAExercisesQuestion(for: exerciseSection);
            exerciseController.setBlankPhrasesDoneFor(exerciseQuestion, for: exerciseSection, completionBlock: { error, object in
            
            })
        }
        
        XCTAssertTrue(exerciseController.isSectionCompleted(exerciseSection), "Section must be compleated")
        XCTAssertEqual(exerciseController.getTotalElements(for: exerciseSection), exerciseController.getElementsCompleted(for: exerciseSection), "compleated elements = total count")
        XCTAssertEqual(exerciseController.getProgressFor(exerciseSection), 100.0, "progress = 100%")
        XCTAssertEqual(exerciseController.getPercentageFor(exerciseSection), "100%", "percentage progress = 100%")
    }
}
