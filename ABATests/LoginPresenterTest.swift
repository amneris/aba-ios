//
//  LoginPresenterTest.swift
//  ABA
//
//  Created by MBP13 Jesus on 30/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class LoginPresenterTest: XCTestCase {

    var loginPresenter: LoginPresenter!
    var mockView: MockLoginViewInput!
    let exampleStudent = "jespejo@abaenglish.com"

    override func setUp() {
        super.setUp()
        RealmTestHelper.deleteCurrentUser()

        mockView = MockLoginViewInput()
        loginPresenter = LoginPresenter(tracker: nil)
        loginPresenter.interactor = LoginInteractor(repository: LoginRepository())
        loginPresenter.view = mockView
    }

    func testCredentialsValidation() {
        let emptyEmailValidation = loginPresenter.parameterValidation("", pass: "pass")
        XCTAssertTrue(emptyEmailValidation == .EmptyEmail)

        let wrongEmailValidation1 = loginPresenter.parameterValidation("jespejo", pass: "pass")
        XCTAssertTrue(wrongEmailValidation1 == LoginParametersValidation.WrongEmailFormat)

        let wrongEmailValidation2 = loginPresenter.parameterValidation("jespejo@abaenglish", pass: "pass")
        XCTAssertTrue(wrongEmailValidation2 == LoginParametersValidation.WrongEmailFormat)

        let wrongEmailValidation3 = loginPresenter.parameterValidation("jespejoabaenglish.com", pass: "pass")
        XCTAssertTrue(wrongEmailValidation3 == LoginParametersValidation.WrongEmailFormat)

        let emptyPasswordValidation = loginPresenter.parameterValidation("jespejo@abaenglish.com", pass: "")
        XCTAssertTrue(emptyPasswordValidation == .EmptyPassword)

        let properCredentials = loginPresenter.parameterValidation("jesp@abaenglish.com", pass: "asdfasdf")
        XCTAssertTrue(properCredentials == .Success)
    }

    func testLogin() {

        let expectation = self.expectation(description: "Perform login")

        mockView.callback = {
            if self.mockView.emailErrorShown || self.mockView.genericErrorShown || self.mockView.facebookErrorShown {
                XCTFail()
            }
            if self.mockView.loginConfirmation && self.mockView.loadingIndicatorShow && self.mockView.loadingIndicatorHide {
                expectation.fulfill()
            }
        }

        loginPresenter.login(withEmail: exampleStudent, andPassword: "qpalzm01")
        waitForExpectations(timeout: 25) { (error) in
            // TODO: Dispose?
        }
    }

    func testWrongLogin() {
        let expectation = self.expectation(description: "Perform login")

        mockView.callback = {
            if self.mockView.emailErrorShown && self.mockView.loadingIndicatorShow && self.mockView.loadingIndicatorHide {
                expectation.fulfill()
            }
        }

        loginPresenter.login(withEmail: exampleStudent, andPassword: "20394209348")
        waitForExpectations(timeout: 5) { (error) in
            // TODO: Dispose?
        }
    }

    func testEmailIncomplete1() {
        let expectation = self.expectation(description: "Perform login")

        mockView.callback = {
            if self.mockView.validationError == LoginParametersValidation.WrongEmailFormat // WrongEmailFormat
            {
                expectation.fulfill()
            }
        }

        loginPresenter.login(withEmail: "jespejo@abaenglish", andPassword: "qpalzm01")
        waitForExpectations(timeout: 2) { (error) in
            // TODO: Dispose?
        }
    }

}

class MockLoginViewInput: NSObject, LoginViewInput {

    var loginConfirmation: Bool = false
    var loadingIndicatorShow: Bool = false
    var loadingIndicatorHide: Bool = false
    var validationError: LoginParametersValidation? = nil

    var emailErrorShown: Bool = false
    var genericErrorShown: Bool = false
    var facebookErrorShown: Bool = false
    var languageError: Bool = false
    var permissionErrorShown: Bool = false

    var callback: ((Void) -> Void) = { }

    @objc func renderLoginConfirmation() {
        loginConfirmation = true
        callback()
    }

    @objc func renderValidationError(_ error: LoginParametersValidation) {
        validationError = error
        callback()
    }

    @objc func showLoadingIndicator() {
        loadingIndicatorShow = true
        callback()
    }

    @objc func hideLoadingIndicator() {
        loadingIndicatorHide = true
        callback()
    }

    func showEmailPasswordError() {
        emailErrorShown = true
        callback()
    }

    func showGenericError() {
        genericErrorShown = true
        callback()
    }

    func showFacebookError() {
        facebookErrorShown = true
        callback()
    }

    func showLanguageError() {
        languageError = true
        callback()
    }
    
    func showPermissionError(){
        permissionErrorShown = true
        callback()
    }
}
