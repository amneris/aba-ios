//
//  VocabularyControllerTest.swift
//  ABA
//
//  Created by Oriol Vilaró on 1/2/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@testable import ABA

class VocabularyControllerTest: ABABaseTestCase {
    
    override func setUp() {
        super.setUp()
        logInTestUser()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCompleteVocabularySection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }

        let expectation = self.expectation(description: "testCompleteVocabularySection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        
        let vocabularyController = VocabularyController()
        let vocabularySection = randomUnit.sectionVocabulary
        
        let expectation2 = self.expectation(description: "semaphoreSection")

        vocabularyController.setCompletedSection(vocabularySection) { (error, object) -> Void in
            
            let vocabulary = object as! ABAVocabulary

            XCTAssertEqual(vocabulary.completed, 1, "Completed must be true or 1")
            XCTAssertNil(error, "error should be nil")
            expectation2.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func testGetPhrasesFromVocabularySection() {
      
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetPhrasesFromVocabularySection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)

        XCTAssertGreaterThan(randomUnit.sectionVocabulary.content.count, 0);
    }
    
    func testToGetAllAudiosIdForVocabularyPhrases() {
        
        guard let randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testGetPhrasesFromInterpretSection")
        let sectionController: SectionController = SectionController()
        let vocabularyController = VocabularyController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            XCTAssertNil(error, "error should be nil")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        let vocabularySection = LevelUnitController.getUnitWithID(randomUnit.idUnit).sectionVocabulary
        let vocabularyAudios = vocabularyController.getAllAudioIDs(forVocabularySection: vocabularySection)
        
        XCTAssertTrue((vocabularyAudios?.count)!>0)
        XCTAssertEqual(vocabularyAudios?.count, Int(vocabularyController.getTotalElements(for: vocabularySection)/2))
    }
    
    func testCompleteAllPhrasesFromVocabularySection() {
        
        guard var randomUnit: ABAUnit = ABAUtilsHelper.getRandomUnit() else {
            XCTFail("Random unit not fetched")
            return
        }
        
        let expectation = self.expectation(description: "testCompleteAllPhrasesFromVocabularySection")
        
        let sectionController: SectionController = SectionController()
        
        sectionController.getAllSections(for: randomUnit) { error, object in
            
            XCTAssertNil(error, "error should be nil")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: Double(WS_TIMEOUT)) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        
        randomUnit = LevelUnitController.getUnitWithID(randomUnit.idUnit)
        let vocabularySection = randomUnit.sectionVocabulary!
        
        let vocabularyController = VocabularyController()
        let phrasesController = PhrasesController()
        
        for i in 0..<vocabularySection.content.count {
            
            let currentPhrase = vocabularySection.content.object(at: i)
            
            XCTAssertEqual(vocabularyController.phrase(withID: currentPhrase.audioFile, andPage: currentPhrase.page, on: vocabularySection).audioFile, currentPhrase.audioFile)

            phrasesController.setPhrasesDone([currentPhrase], for: vocabularySection, sendProgressUpdate: false, completionBlock: { (error, object) -> Void in
                
                XCTAssertTrue(currentPhrase.done.boolValue  && currentPhrase.listened.boolValue);
            })

            XCTAssertTrue(currentPhrase.done.boolValue  && currentPhrase.listened.boolValue);
        }

        XCTAssertTrue(vocabularyController.isSectionCompleted(vocabularySection))
        XCTAssertEqual(vocabularyController.getElementsCompleted(for: vocabularySection), vocabularyController.getTotalElements(for: vocabularySection))
        XCTAssertEqual(Int(vocabularyController.getProgressFor(vocabularySection)), 100)
        XCTAssertEqual(vocabularyController.getPercentageFor(vocabularySection), "100%")
    }
}
