//
//  RegisterInteractorPresenter.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

@testable import ABA

class RegisterInteractorTest: XCTestCase {

    var registerInteractor: RegisterInteractor!

    override func setUp() {
        super.setUp()
        RealmTestHelper.deleteCurrentUser()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testRegister() {
        do {
            registerInteractor = RegisterInteractor(repository: RegisterRepository())

            try ShepherdTestHelper.forceIntegration()
            let generatedEmail = TestUtils.generateEmail()

            let user = try registerInteractor.register(name: "Register Interactor Test", email: generatedEmail, password: "123456").toBlocking().single()

            if let safeuser = user {
                XCTAssertNotNil(safeuser)
                XCTAssertNotNil(safeuser.token)

                // Checking creation
                XCTAssertTrue(RealmTestHelper.isUserCreated())
                XCTAssertTrue(RealmTestHelper.areUnitsCreated())
                XCTAssertTrue(RealmTestHelper.areLevelsCreated())
            }
            else {
                XCTFail()
            }
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }

    func testRegisterWithFacebook() {
        do {
            registerInteractor = RegisterInteractor(repository: RegisterRepository())
            try ShepherdTestHelper.forceIntegration()

            let faceboUser = createRandomFacebookUser()
            let userRegisteredWithFacebook = try registerInteractor.registerWithFacebook(faceboUser).toBlocking().single()
            if userRegisteredWithFacebook != nil {

                XCTAssertEqual(userRegisteredWithFacebook?.isNewUser, true)

                // Checking creation
                XCTAssertTrue(RealmTestHelper.isUserCreated())
                XCTAssertTrue(RealmTestHelper.areUnitsCreated())
                XCTAssertTrue(RealmTestHelper.areLevelsCreated())
            }
            else {

                XCTFail()
            }

        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }
    
    func testRegisterAndLoginWithFacebook() {
        do {
            registerInteractor = RegisterInteractor(repository: RegisterRepository())
            try ShepherdTestHelper.forceIntegration()
            
            // 1. Registration
            let faceboUser = createRandomFacebookUser()
            let userRegisteredWithFacebook = try registerInteractor.registerWithFacebook(faceboUser).toBlocking().single()
            if userRegisteredWithFacebook != nil {
                
                XCTAssertEqual(userRegisteredWithFacebook?.isNewUser, true)
                
                // Checking creation
                XCTAssertTrue(RealmTestHelper.isUserCreated())
                XCTAssertTrue(RealmTestHelper.areUnitsCreated())
                XCTAssertTrue(RealmTestHelper.areLevelsCreated())
            }
            else {
                
                XCTFail()
            }
            
            // 2. Logout
            UserController.logout()
            
            // 3. Login
            let userLoggedInWithFacebook = try registerInteractor.registerWithFacebook(faceboUser).toBlocking().single()
            if userLoggedInWithFacebook != nil {
                XCTAssertEqual(userLoggedInWithFacebook?.isNewUser, false)
                
                // Checking creation
                XCTAssertTrue(RealmTestHelper.isUserCreated())
                XCTAssertTrue(RealmTestHelper.areUnitsCreated())
                XCTAssertTrue(RealmTestHelper.areLevelsCreated())
            }
            
        }
        catch let error {
            print("testRegister error: \(error)")
            XCTFail()
        }
    }
}

extension RegisterInteractorTest {
    
    func createRandomFacebookUser() -> FacebookUser {
        let facebookId = TestUtils.generateFacebookID()
        let email = TestUtils.generateEmail()
        let firstName = "hola manolo"
        
        let lastName = "hola pepe"
        let gender = "male"
        let imageUrl = "path/to/image"
        
        let facebookUser = FacebookUser(facebookId: facebookId, email: email, firstName: firstName, lastName: lastName, gender: gender, imageUrl: imageUrl)
        return facebookUser
    }
}