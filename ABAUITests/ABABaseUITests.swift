//
//  ABAUITestBase.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 14/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest

class ABABeseUITests: XCTestCase {
    let app = XCUIApplication()
    fileprivate var launched = false
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        launchIfNecessary()
//        let levelVC = app.navigationBars["LevelUnitView"]
//        let burger = levelVC.buttons["menuBurger"]
//        sleep(2)
//        if (burger.exists) {
//            burger.tap()
//            app.images["blankAvatar"].tap()
//            let  scrollView = app.scrollViews.elementBoundByIndex(0)
//            app.swipeUp()
//            scrollView.buttons["Log out"].tap()
//        }
    }
    
    fileprivate func launchIfNecessary() {
        if !launched {
            launched = true
            app.launchEnvironment = ["uitesting": "1"]
            app.launch()
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
}

extension XCUIElement {
    func clearAndEnterText(_ text: String) -> Void {
        
        guard let stringValue = self.value as? String else {
            XCTFail("Tried to clear and enter text into a non string value")
            return
        }
        
        self.tap()
        
        var deleteString: String = ""
        
        if (stringValue.characters.count > 0) {
            for _ in stringValue.characters {
                deleteString += XCUIKeyboardKeyDelete
            }
            
            self.typeText(deleteString)
        }
        
        self.typeText(text)
    }
}

