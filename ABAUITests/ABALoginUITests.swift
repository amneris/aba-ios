//
//  ABAUITests.swift
//  ABAUITests
//
//  Created by Oleksandr Gnatyshyn on 07/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest

class ABALoginUITests: ABABeseUITests {
    var emailError: String {
        get {
            let errorEmailList = [
                "gnatok@gmail",
                "gnatokgmail.com",
                "gnatok"
            ]
            
            let randomIndex = Int(arc4random_uniform(UInt32(errorEmailList.count)))
            return errorEmailList[randomIndex]
        }
    }
    
    var randomEmail: String {
        get {
            let randomIndex = Int(arc4random_uniform(UInt32(999)))
            return "testUI+\(randomIndex)@abaenglish.com"
        }
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLoggedSuccesfull() {
        app.buttons["startLoginButton"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        let emailField = elementsQuery.textFields["your email address"]
        let passField = elementsQuery.secureTextFields["your password"]
        let listVC = app.otherElements["levelListViewContainer"]
        XCTAssertFalse(listVC.exists)
        
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: listVC, handler: nil)
        
        emailField.clearAndEnterText("student+es@abaenglish.com")
        
        UIPasteboard.general.string = "abaenglish"
        passField.doubleTap()
        app.menuItems["Paste"].tap()
        
        elementsQuery.buttons["loginButton"].tap()
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssert(listVC.exists)
        
    }
    
    func testInvalidEmailShouldShowError() {
        app.buttons["startLoginButton"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        let emailField = elementsQuery.textFields["your email address"]
        let passField = elementsQuery.secureTextFields["your password"]
        let alertView = app.otherElements["alertView"]
        
        emailField.clearAndEnterText(self.emailError)
        
        UIPasteboard.general.string = "gnatok"
        passField.doubleTap()
        app.menuItems["Paste"].tap()
        
        elementsQuery.buttons["loginButton"].tap()
        XCTAssert(alertView.exists)
    }
    
    func testInvalidPasswordShouldShowError() {
        app.buttons["startLoginButton"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        let emailField = elementsQuery.textFields["your email address"]
        let passField = elementsQuery.secureTextFields["your password"]
        let alertView = app.otherElements["alertView"]
        XCTAssertFalse(alertView.exists)
        
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        
        emailField.clearAndEnterText("gnat86@gmail.com")
        
        UIPasteboard.general.string = "trololo"
        passField.doubleTap()
        app.menuItems["Paste"].tap()
        
        elementsQuery.buttons["loginButton"].tap()
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)
    }

    
    func testEmptyEmailShouldShowError() {
        app.buttons["startLoginButton"].tap()
        
        let elementsQuery = app.scrollViews.otherElements
        let emailField = elementsQuery.textFields["your email address"]
        let passField = elementsQuery.secureTextFields["your password"]
        let alertView = app.otherElements["alertView"]
        
        emailField.clearAndEnterText("")
        
        UIPasteboard.general.string = "gnatok"
        passField.doubleTap()
        app.menuItems["Paste"].tap()
        
        elementsQuery.buttons["loginButton"].tap()
        XCTAssert(alertView.exists)
    }
    
}

