//
//  ABAUITests.swift
//  ABAUITests
//
//  Created by Oleksandr Gnatyshyn on 07/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import XCTest

class ABARegisterUITests: ABABeseUITests {
    var emailError: String {
        get {
            let errorEmailList = [
                "gnatok@gmail",
                "gnatok@gmail..com",
                "gnatokgmail.com",
                "gnatok"
            ]
            
            let randomIndex = Int(arc4random_uniform(UInt32(errorEmailList.count)))
            return errorEmailList[randomIndex]
        }
    }
    
    var randomEmail: String {
        get {   
            let randomIndex = Int(arc4random_uniform(UInt32(999)))
            return "testUI+\(randomIndex)@abaenglish.com"
        }
    }
    
    override func setUp() {
        super.setUp()
        
        let tutorialnextstepbuttonButton = app.buttons["tutorialNextStepButton"]
        let registerWithEmailButton = app.buttons["registerWithMailButton"]
        
        app.buttons["startNowButton"].tap()
        
        for _ in 0..<3 {
            tutorialnextstepbuttonButton.tap()
            sleep(1)
        }
        
        registerWithEmailButton.tap()
        sleep(1)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testRegistrationWithEmailSuccessfull() {
        let nameField = app.textFields["your name"]
        let emailField = app.textFields["your email address"]
        let passField = app.secureTextFields["your password"]
        let levelSelectionList = app.otherElements["levelSelectionView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(levelSelectionList.exists)
        let exists = NSPredicate(format: "exists == true")
        
        nameField.typeText("UITester")
        registerwithEmailNextButton.tap()
        sleep(1)
        emailField.typeText(randomEmail)
        registerwithEmailNextButton.tap()
        sleep(1)
        passField.typeText("gnatok")
        
        expectation(for: exists, evaluatedWith: levelSelectionList, handler: nil)
        registerwithEmailNextButton.tap()
        
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssert(levelSelectionList.exists)
    }
    
    func testRegistrationWithWrongEmailWillShowError() {
        let nameField = app.textFields["your name"]
        let emailField = app.textFields["your email address"]
        let alertView = app.otherElements["alertView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(alertView.exists)
        let exists = NSPredicate(format: "exists == true")
        
        nameField.typeText("UITester")
        registerwithEmailNextButton.tap()
        sleep(1)
        emailField.typeText(emailError)
        registerwithEmailNextButton.tap()
        
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        registerwithEmailNextButton.tap()
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)
    }
    
    func testRegistrationWithEmptyEmailWillShowError() {
        let nameField = app.textFields["your name"]
        let emailField = app.textFields["your email address"]
        let alertView = app.otherElements["alertView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(alertView.exists)
        let exists = NSPredicate(format: "exists == true")
        
        nameField.typeText("UITester")
        registerwithEmailNextButton.tap()
        sleep(1)
        emailField.typeText("")
        registerwithEmailNextButton.tap()
        
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        registerwithEmailNextButton.tap()
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)
    }
    
    func testRegistrationWithEmptyUsernameWillShowError() {
        let nameField = app.textFields["your name"]
        let alertView = app.otherElements["alertView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(alertView.exists)
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        nameField.typeText("")
        registerwithEmailNextButton.tap()
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)
    }
    
    func testRegistrationWithWrongPasswordWillShowError() {
        let nameField = app.textFields["your name"]
        let emailField = app.textFields["your email address"]
        let passField = app.secureTextFields["your password"]
        let alertView = app.otherElements["alertView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(alertView.exists)
        let exists = NSPredicate(format: "exists == true")
        
        nameField.typeText("UITester")
        registerwithEmailNextButton.tap()
        sleep(1)
        emailField.typeText(randomEmail)
        registerwithEmailNextButton.tap()
        sleep(1)
        passField.typeText("123")
        
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        registerwithEmailNextButton.tap()
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)
    }
    
    func testRegistrationWithEmptyPasswordWillShowError() {
        let nameField = app.textFields["your name"]
        let emailField = app.textFields["your email address"]
        let passField = app.secureTextFields["your password"]
        let alertView = app.otherElements["alertView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(alertView.exists)
        let exists = NSPredicate(format: "exists == true")
        
        nameField.typeText("UITester")
        registerwithEmailNextButton.tap()
        sleep(1)
        emailField.typeText(randomEmail)
        registerwithEmailNextButton.tap()
        sleep(1)
        passField.typeText("")
        
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        registerwithEmailNextButton.tap()
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)
    }
    
    
    func testRegistrationWithAlreadyRegisteredEmailWillShowError() {
        let nameField = app.textFields["your name"]
        let emailField = app.textFields["your email address"]
        let alertView = app.otherElements["alertView"]
        let registerwithEmailNextButton = app.buttons["registerWithMailNextStepButton"]
        
        XCTAssertFalse(alertView.exists)
        let exists = NSPredicate(format: "exists == true")
        
        nameField.typeText("UITester")
        registerwithEmailNextButton.tap()
        sleep(1)
        emailField.typeText("gnat86@gmail.com")
        registerwithEmailNextButton.tap()
        
        expectation(for: exists, evaluatedWith: alertView, handler: nil)
        registerwithEmailNextButton.tap()
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssert(alertView.exists)}
    
}



