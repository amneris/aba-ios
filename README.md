[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=577e252eb24aba010070b6cf&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/577e252eb24aba010070b6cf/build/latest)

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to add Swift Lint ###

1) install swift linter

```
brew install swiftlint
```

2) Add a new `Run Script Phase` with:


```
if which swiftlint >/dev/null; then
  swiftlint
else
  echo "SwiftLint does not exist, download from https://github.com/realm/SwiftLint"
fi
```

## Phrase app ##

Copy resources are at www.phraseapp.com but they can be downloaded using a single line command. More info: `https://phraseapp.com/docs/developers/cli/` and `https://phraseapp.com/cli`. Configuration is already done: it's just a matter of using it.

Once you have downloaded the phraseapp CLI you can perform `phraseapp pull`. Make sure you commit the languages you need (there could be half translated languages that we do not want to be included in the app yet)

## Project compatibility ##

##### Xcode 8.0
##### Swift 2.3
##### iOS10
##### iOS9 deployment target

## Fastlane - Certificates and provisioning

Use the following commands to download all needed certificates, provisioning and singning keys to build the project into your device.

```
fastlane match development
fastlane match adhoc
fastlane match production
```

If there is a new device that was manually added into iTunes then:

```
fastlane match development --force
```

To add new devices to provisioning profile

```
fastlane run register_devices devices:"iphone 6"=<udid>
```


## Using Fastlane to renew APNS certificate

It will automatically renew the certificates if they are about to expire. Otherwise it does not renew anything.

For development:

```
pem --development -p "password"
```

For production:

```
pem -p "password"

```
## Fastlane - For building the app

To test:

```
fastlane test
```

To build a version and send it to Crashlytics beta:

```
fastlane beta
```

To send a version to the Appstore

```
fastlane appstore
```


## pod build ##

Before building make sure you have XCode version 8.0 with:

`xcversion select`

that should return:

```
Xcode 8.0
Build version 8A218a
```

Whenever the answer is different then perform:

`xcversion select 8.0`


Then you can:

```pod update```

or

```pod install```

## carthage build ##

To build using XCode 8 and Swift 2.3 compatibility

```carthage update --toolchain com.apple.dt.toolchain.Swift_2_3 --platform ios```


Regular carthage update

```carthage bootstrap --platform iOS```

## Reports ##

Visit the followin url to know the JAVA utitily to download the data

```
https://help.apple.com/itc/appsreporterguide/#/itc0f2481229
```

Do not forget to edit `Reporter.properties` with your information

Examples

```
java -jar Reporter.jar p=Reporter.properties Sales.getVendors

java -jar Reporter.jar p=Reporter.properties Sales.getAccounts

java -jar Reporter.jar p=Reporter.properties Sales.getReport 86104480, Subscription, Summary, Daily, 20161214

java -jar Reporter.jar p=Reporter.properties Sales.getReport 86104480, Sales, Summary, Daily, 20161214

java -jar Reporter.jar p=Reporter.properties Sales.getReport 86104480, SubscriptionEvent, Summary, Daily, 20161214
```

```
java -jar Reporter.jar p=Reporter.properties Finance.getVendorsAndRegions

java -jar Reporter.jar p=Reporter.properties Finance.getReport 86104480, EU, Financial, 2016, 1
```

### Apple fiscal years

`https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/wa/jumpTo?page=fiscalcalendar`

