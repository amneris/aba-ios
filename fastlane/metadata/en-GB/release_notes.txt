Hey ABA Students:

In this new version we have:

- Done a few small improvements to improve stability and performance
- Fixed commonly reported bugs

Go ahead and update!

We love your feedback so keep it coming. (And keep studying lots of English!)