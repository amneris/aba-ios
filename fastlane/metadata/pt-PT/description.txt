Você quer aprender inglês? O ABA English para iOS é muito mais do que uma app para estudar inglês. Descarregue-a e descubra o curso exclusivo com professor que você precisa para alcançar o seu objetivo: falar inglês!

A maneira mais fácil de aprender inglês 

Não somos apenas nós a dizê-lo...expertos linguísticos de importantes universidades do mundo concordam que é uma autêntica maravilha do m-learning.

Baseando-nos nos princípios do método natural, que consiste em aprender através de uma imersão total na língua, criamos um sistema para aprender inglês que simula o mesmo processo de aprendizagem que você experimentaria se viajasse para o estrangeiro para estudar inglês: primeiro você ouve e compreende, e depois começa a falar e escrever de forma espontânea e natural!


Uma academia de inglês na palma da sua mão: 

- 144 Vídeo-aulas: explicação eficaz e divertida de toda a gramática inglesa

- 6 Níveis de aprendizagem: de inglês para iniciantes até nível Business.

- Professor particular: o seu professor particular guiar-lhe-á no seu caminho para um melhor nível de inglês, com instruções e conselhos adaptados ao seu progresso no curso.


Descubra um método de aprendizagem exclusivo

Como funciona: primeiro você assiste os ABA Films, curtas-metragens filmadas em Londres e Nova Iorque; depois você estuda os diálogos, aprende a escrevê-los e posteriormente você dobra as personagens da história atuando com elas: você vai ter a impressão de estar vivendo uma autêntica experiência da vida real! A gramática, necessária para consolidar os conhecimentos, é tratada no final da unidade, juntamente com exercícios práticos. 

Como na vida real, você usará o seu dispositivo móvel para ouvir, falar, ler e escrever: o seu subconsciente gradualmente irá incorporando novo vocabulário e expressões...e você progredirá sem se dar conta!

NOTA:
Para os alunos Free e Premium do ABA English, a aplicação permite manter o progresso já realizado se for acessado com a mesma conta que o aluno usar para o Campus de ABA English.


Inclui as duas modalidades, a gratuita e a de pagamento (ABA Free e ABA Premium).

Com a modalidade gratuita (ABA Free) você tem toda a gramática inglesa em 144 vídeo-aulas explicadas pelos melhores professores da academia. Você também dispõe, de maneira gratuita, da primeira unidade completa de cada nível para provar a metodologia de aprendizagem baseada no visionamento de filmes.

Com a modalidade de pagamento (ABA Premium) você tem acesso a todo o conteúdo do curso completo, pode obter certificados por cada nível que você passar e também conta com o suporte dum professor particular que você poderá contatar através do Campus do ABA English para resolver qualquer dúvida linguística que você tiver. 

Existem três tipos de assinaturas para o ABA Premium:
Assinatura mensal por 24,99 €
Assinatura de seis meses por 124,99 €
Assinatura anual por 199,99 €

Todas as assinaturas são renovadas automaticamente, a menos que você desative esta opção antes da expiração da assinatura atual. 

Se você não desativar a renovação automática através do menu de configuração da sua conta do iTunes, será automaticamente cobrada a mesma quantidade durante as 24 horas anteriores ao vencimento da sua assinatura.

Se você desativar a renovação da sua assinatura, o acesso ao conteúdo do curso só irá terminar quando o período da sua assinatura atual expirar.


Sobre ABA English:

A ABA English é uma academia de inglês online com mais de 40 anos de experiência que utiliza uma metodologia de aprendizagem consolidada e validada por expertos linguísticos das maiores universidades do mundo.