Do you want to learn English? ABA English for iOS is much more than just an app for studying English. Download it and discover our unique course with a teacher to help you achieve your aim: speaking English!

The easiest way to learn English 

We’re not the only ones who say so...language experts from important universities all over the world agree that it’s an absolute m-learning gem.

Based on the principles of the natural method which consists of learning by full language immersion, we created a system for learning English which simulates the same learning process you would experience if you travelled abroad to study English: first you listen and comprehend, and then you start to speak and write, all this naturally and spontaneously!

An English academy in the palm of your hand:

- ABA Films: learn English by watching our exclusive short films shot in London and New York

- 144 video classes: English grammar explained in an effective and enjoyable way

- 6 levels: from English for Beginners to Business English

- Your private tutor: your private tutor will help you achieve a better level of English, with instructions and advice according to your progress in the course

- Assess your progress: you will be able to gauge your progress with an assessment at the end of each level

- Official certificates: with our English course you will be able to study and get an official ABA English certificate once you have completed a level

- Multi-device: with just one account you will be able to carry on learning English from your mobile, tablet or PC. You decide how and when!

Discover a unique learning method

This is how it works: first you watch the ABA Films, short films shot in London and New York, then you study the dialogues, learn how to write them and next  you take on the role of the characters from the script by acting with them. You feel like you are experiencing a real-life scenario! Grammar, required to consolidate your knowledge, is dealt with towards the end of the unit, together with practical exercises.

Just like in day-to-day life, you will use your mobile device to listen, speak, read and write: gradually you will subconsciously absorb new vocabulary and expressions and you will make progress without even realising it!

NOTE:
For ABA English’s Free and Premium students, the application allows you to keep track of the progress you have already made if you access it from the same account as the one you use for the ABA English Campus.

This is for both subscription types: the free one and the paying one (ABA Free and ABA Premium).

With a free subscription (ABA Free), you have access to English grammar in 144 video classes explained by the academy’s best teachers. The first complete unit for each level is also free so you can try out our learning method based on watching films.

With a paying subscription (ABA Premium), you have access to the complete course, you can get a certificate for each level you pass and you have the support of a private tutor who you will be able to contact through the ABA English Campus for any doubts concerning English that you may have. 

There are three types of ABA Premium subscriptions:
A monthly subscription for $29.99 
A six-monthly subscription for $149.99 
A yearly subscription for $239.99 

All subscriptions are automatically renewed unless you deactivate this option before your subscription expires.

If you don’t deactivate automatic renewals using the settings menu in your iTunes account, you will automatically be charged the same amount in the 24 hours prior to the termination of your subscription.

If you deactivate automatic renewals, your access to the course content will end when your current subscription expires.

About ABA English:

ABA English is an online language academy with over 40 years’ experience which allows you to learn thanks to a learning method which has been approved and perfected by language experts from important universities all over the world.