//
//  PurchaseHandler.m
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "PurchaseHandler.h"

#import "SubscriptionController.h"
#import "Utils.h"
#import <Crashlytics/Crashlytics.h>

@implementation PurchaseHandler

#pragma mark - Public methods

+ (PurchaseHandler *)sharedInstance
{
    static PurchaseHandler *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[PurchaseHandler alloc] init];
    });
    return _sharedInstance;
}

- (void)handlePurchaseStatus:(SubscriptionStatus)status
{
    [self notifyPurchaseStatus:status];
    [self showAlertWithPurchaseStatus:status];
}

- (void)notifyPurchaseStatus:(SubscriptionStatus)status
{
    if (status == SubscriptionStatusActivationProcessSucceed || status == SubscriptionStatusPurchaseAlreadyAssigned)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSubscriptionProcessCompletedOK object:self];
        CLS_LOG(@"Purchase: purchase completed");
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSubscriptionProcessCompletedFailed object:self];
        CLS_LOG(@"Purchase: purchase failed, already assigned");
    }
}

#pragma mark - Private methods

- (void)showAlertWithPurchaseStatus:(SubscriptionStatus)status
{
    switch (status) {
        case SubscriptionStatusActivationProcessSucceed:
        {
            // ALL GOOD
            [self displayAlertWithTitle:STRTRAD(@"alertSubscriptionOkTitle", @"Título de la alerta cuando se ha completado la suscripción con exito")
                             andMessage:STRTRAD(@"alertSubscriptionOkMessage", @"Mensaje de la alerta cuando se ha completado la suscripción con exito")
                                 forTag:1001];
            break;
        }
        case SubscriptionStatusPurchaseProcessFailed:
        {
            // Purchase could not be performed
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertSubscriptionKOMessage2", @"Error al cambiar a premium un usuario")
                                 forTag:1002];
            break;
        }
        case SubscriptionStatusActivationProcessFailed:
        {
            // Purchase could not be communicated to ABA server
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertSubscriptionKOMessage5", @"Ha habido un error al comunicar la compra a nuestros servidores. Puedes pulsar en 'Restaurar compras' más tarde desde la sección 'Tu cuenta'. Disculpa las molestias")
                                 forTag:1003];
     
            break;
        }
        case SubscriptionStatusTransactionVerificationFailed:
        {
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertSubscriptionKOMessage2", @"Error al cambiar a premium un usuario")
                                 forTag:1004];

            break;
        }
        case SubscriptionStatusPurchaseAlreadyAssigned:
        {
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertSubscriptionKOMessage3", @"Error al cambiar a premium un usuario. El receipt ya está asignado a otro usuario")
                                 forTag:1005];

            break;
        }
        case SubscriptionStatusRestoreNoTransactions:
        {
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertErrorNoAppleTransactions",@"el .4")
                                 forTag:1006];
            break;
        }
        case SubscriptionStatusRestoreProcessFailed:
        {
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertErrorFetchingAppleTransactions",@"Error al recuperar las compras del usuario desde Apple.")
                                 forTag:1007];
            break;
        }
        case SubscriptionStatusProductNotFound:
        {
            [self displayAlertWithTitle:nil
                             andMessage:STRTRAD(@"alertErrorFetchingAppleTransactions",@"Error al recuperar las compras del usuario desde Apple.")
                                 forTag:1008];
            break;
        }
        default:
            break;
    }
}

- (void) displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message forTag:(NSInteger)tag {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:STRTRAD(@"alertSubscriptionOkButton", @"OK")
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alert addAction:okAction];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert animated:YES completion:nil];
}

@end
