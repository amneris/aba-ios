//
//  ABAExercisesGroup.h
//  ABA
//
//  Created by Marc Güell Segarra on 11/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAExercisesQuestion.h"

@class ABAExercisesExample, ABAExercisesQuestion;

@interface ABAExercisesGroup : RLMObject

@property NSNumber<RLMBool>* completed;
@property NSString* title;
@property RLMArray<ABAExercisesQuestion *><ABAExercisesQuestion> *questions;

@end

RLM_ARRAY_TYPE(ABAExercisesGroup)