//
//  ExercicesViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ExercicesViewController.h"
#import "ExercisesController.h"
#import "ABAExercises.h"
#import "ABAExercisesGroup.h"
#import "ABAExercisesQuestion.h"
#import "ABAExercisesPhrase.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import "Resources.h"
#import "Definitions.h"
#import "ABAUnit.h"
#import "ABAAlert.h"
#import "ABAExercisesTextField.h"
#import "ABAExercisePhraseItem.h"
#import <QuartzCore/QuartzCore.h>
#import "AudioController.h"
#import "PhrasesController.h"
#import "SolutionTextController.h"
#import "SDVersion.h"
#import "ImageHelper.h"
#import "DataController.h"
#import "LegacyTrackingManager.h"
#import "DataController.h"
#import "PureLayout.h"
#import "APIManager.h"
#import "ABA-Swift.h"
#import <Crashlytics/Crashlytics.h>
@import SVProgressHUD;

#define PROPORTIONAL_WIDTH 0.85
#define TEXTFIELD_PADDING_RIGHT 5.0
#define ITEM_HEIGHT_BIG 40.0
#define LINE_HEIGHT_BIG 40.0
#define ITEM_HEIGHT 35.0
#define LINE_HEIGHT 35.0
#define PHRASEVIEW_PADDING 12.0
#define PHRASEVIEW_TOPMARGIN 0.0

@interface ExercicesViewController () <UITextFieldDelegate, AudioControllerDelegate>

@property ExercisesController *exercisesController;
@property PhrasesController *phrasesController;
@property AudioController *audioController;
@property SolutionTextController *solutionTextController;

@property kABAExercisesType currentType;
@property kABAExercisesStatus status;
@property UIView *correctAlertView;
@property ABAAlert *abaAlert;
@property BOOL firstTimePresented;

@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeightConstraint;

@property UILabel *titleLabel;
@property UIView *titleView;
@property UITapGestureRecognizer *singleTitleTap;

@property UIView *contentView;

@property UIView *exerciseView;
@property UIView *exercisePhraseView;

@property UIView *translationView;
@property UILabel *translationLabel;

@property UIView *infoView;
@property UIButton *infoButton;
@property NSTimer *infoViewTimer;
@property UITapGestureRecognizer *singleHelpTap;

@property (strong, nonatomic) UIView *keyboardView;
@property (strong, nonatomic) UIView *checkAnswerKeyboardView;
@property (strong, nonatomic) UIButton *checkAnswerKeyboardButton;

@property NSArray *elementsArray;

@property NSMutableArray *blankTextFields;
@property NSMutableArray *superiorElements; // For storing translation label

@property CGRect keyboardFrameBeginRect;

@property BOOL helpHasShown;
@property BOOL exerciseTitleHasShownForFirstTime;

@property BOOL readOnlyMode;

@property NSLayoutConstraint *contentViewBottomConstraint;

@end

@implementation ExercicesViewController
{
    // Constants
    CGFloat CONSTANT_fontSizeSmall;
    CGFloat CONSTANT_fontSizeNormal;
	CGFloat CONSTANT_fontSizeTranslation;
	CGFloat CONSTANT_fontSizeTitle;
    CGFloat CONSTANT_fontSizeBig;
    CGFloat CONSTANT_fontSizeLarge;
    CGFloat CONSTANT_buttonHeight;
    CGFloat CONSTANT_itemHeight;
    CGFloat CONSTANT_lineHeight;
	
	CGFloat CONSTANT_phraseViewPadding;

    CGFloat CONSTANT_contentViewOffset;
    CGFloat CONSTANT_keyboardHeight;
}
@dynamic section;

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if((self = [super initWithCoder:aDecoder]))
    {
        _exercisesController = [[ExercisesController alloc] init];
        _phrasesController = [[PhrasesController alloc] init];
        _solutionTextController = [[SolutionTextController alloc] init];
		_solutionTextController.strictContractedForms = NO;
        
        _audioController = [[AudioController alloc] init];
        _audioController.audioControllerDelegate = self;
        
        _blankTextFields = [[NSMutableArray alloc] init];
        _superiorElements = [[NSMutableArray alloc] init];
        _firstTimePresented = NO;
        
        _helpHasShown = NO;
        _exerciseTitleHasShownForFirstTime = NO;
		
		_readOnlyMode = NO;
		
        [self setupConstants];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupHeaderProgress];
    
    if ([SDVersion deviceSize] == Screen3Dot5inch ||
        [SDVersion deviceSize] == Screen4inch)
    {
        [_scrollView setScrollEnabled:YES];
    }
    
    [DataController saveProgressActionForSection:self.section andUnit:[self.section getUnitFromSection] andPhrases:nil errorText:nil isListen:NO isHelp: NO completionBlock:^(NSError *error, id object) {
        
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateStatus:kABAExercisesStatusInit];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"exercisessection"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self deleteBlankTextFields];
    
    _audioController.audioControllerDelegate = nil;
    
    if([[_audioController player] isPlaying])
    {
        [[_audioController player] stop];
    }
    
    if([[_audioController recorder] isRecording])
    {
        [_audioController stopRecording];
    }
    
    if(_abaAlert)
    {
        [_abaAlert dismiss];
    }
    
    if(_helpHasShown)
    {
        [_infoViewTimer invalidate];
        _infoViewTimer = nil;
        [_infoView removeFromSuperview];
    }
    
    [_correctAlertView removeFromSuperview];
    
    [self.view endEditing:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private methods

-(void)setupConstants
{
    if ([SDVersion deviceSize] == Screen3Dot5inch ||
        [SDVersion deviceSize] == Screen4inch)
    {
        CONSTANT_fontSizeSmall = 10.0f;
        CONSTANT_fontSizeNormal = 12.0f;
        CONSTANT_fontSizeBig = 14.0f;
        CONSTANT_fontSizeLarge = 20.0f;
        CONSTANT_buttonHeight = 35.0f;
        CONSTANT_itemHeight = ITEM_HEIGHT;
        CONSTANT_lineHeight = LINE_HEIGHT;
        CONSTANT_contentViewOffset = 44.0f;
		CONSTANT_fontSizeTranslation = 14.0f;
		CONSTANT_fontSizeTitle = 14.0f;
        
        CONSTANT_keyboardHeight = 216;
		CONSTANT_phraseViewPadding = PHRASEVIEW_PADDING;
    }
    else   // iPhone 4.7" & 5.5"
    {
        CONSTANT_fontSizeSmall = 14.0f;
        CONSTANT_fontSizeNormal = 16.0f;
        CONSTANT_fontSizeBig = 18.0f;
        CONSTANT_fontSizeLarge = 24.0f;
        CONSTANT_buttonHeight = 50.0f;
        CONSTANT_itemHeight = ITEM_HEIGHT_BIG;
        CONSTANT_lineHeight = LINE_HEIGHT_BIG;
        CONSTANT_contentViewOffset = 44.0f;
		CONSTANT_fontSizeTranslation = 16.0f;
		CONSTANT_fontSizeTitle = 16.0f;
        
        CONSTANT_keyboardHeight = 216;
		CONSTANT_phraseViewPadding = PHRASEVIEW_PADDING;
    }
    
    if(IS_IPAD)
    {
		CONSTANT_fontSizeSmall = 24.0f;
		CONSTANT_fontSizeNormal = 24.0f;
		CONSTANT_fontSizeBig = 24.0f;
		CONSTANT_fontSizeLarge = 28.0f;
		CONSTANT_buttonHeight = 75.0f;
		CONSTANT_itemHeight = ITEM_HEIGHT_BIG*1.1;
		CONSTANT_lineHeight = LINE_HEIGHT_BIG*1.1;
		CONSTANT_contentViewOffset = 44.0f;
		CONSTANT_fontSizeTranslation = 24.0f;
		CONSTANT_fontSizeTitle = 20.0f;

        CONSTANT_keyboardHeight = 264;
		CONSTANT_phraseViewPadding = PHRASEVIEW_PADDING*2;
    }
}

- (void)deleteBlankTextFields
{
    for (ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        blankTextField.delegate = nil;
//        [blankTextField.inputAccessoryView removeFromSuperview];
        blankTextField.inputAccessoryView = nil;
    }

    _blankTextFields = [[NSMutableArray alloc] init];
}

-(void)setupHeaderProgress
{
    [self addBackButtonWithTitle:STRTRAD(@"exercisesKey", @"Ejercicios") andSubtitle:[_exercisesController getPercentageForSection:self.section]];
}

- (void)updateFramesOnKeyboard:(NSNotification*)notification
{
    if(CGRectIsEmpty(_keyboardFrameBeginRect))
    {
        NSDictionary* keyboardInfo = [notification userInfo];
        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        _keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
        
        CGFloat yPosition = [UIScreen mainScreen].bounds.size.height-(_keyboardFrameBeginRect.size.height+CONSTANT_contentViewOffset)-_contentView.frame.size.height;
        
        if(yPosition < 0)
        {
            yPosition = 0;
        }
        
        CGRect newFrame = CGRectMake(_contentView.frame.origin.x, yPosition, _contentView.frame.size.width, _contentView.frame.size.height);
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^
                            {
                                _contentView.frame = newFrame;
                            }
                                            completion:NULL];
                       });
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

-(void)updateStatus: (kABAExercisesStatus)newStatus
{
    _status = newStatus;
    
    switch(_status)
    {
        case kABAExercisesStatusInit:
        {
            _currentQuestion = [_exercisesController getCurrentABAExercisesQuestionForABAExercises:self.section];
            
            [self trackCrashlyticsProperties];
            [[LegacyTrackingManager sharedManager] trackInteraction];
            
            _currentGroup = _currentQuestion.exercisesGroup;
			
			if(_currentQuestion == nil && _currentGroup == nil)
			{
				if([self.section.exercisesGroups count] > 0)
				{
					_readOnlyMode = YES;

					_currentGroup = [self.section.exercisesGroups objectAtIndex:0];
					_currentQuestion = [_currentGroup.questions objectAtIndex:0];
                    
                    [[LegacyTrackingManager sharedManager] trackInteraction];
				}
            }
            
            // We delete previous blank text fields stored
            [self deleteBlankTextFields];
            [self updateElements];
			
			if([self checkForQuestionIntegrity]) {
				[self updateStatus:kABAExercisesStatusMustShowExercise];
			}
			
            break;
        }
        case kABAExercisesStatusMustShowExercise:
        {
			[self setupViews];
            [self setupTranslationView];
            [self setupExercisePhraseView];
            [self setupExerciseView];
            [self setupCheckAnswerKeyboardButton];
            [self setupExerciseTitleLabel];
			
			if(_readOnlyMode)
			{
				[self fillAnswersInTextFields];
			}
			
#ifdef SHEPHERD_DEBUG
            if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
                [self fillAnswersInTextFields];
            }
#endif
            
			[self checkForBlankTextFieldFirstResponderWithDelay:0.75f];
			
            break;
        }
        case kABAExercisesStatusExerciseOK:
        {
			[self showCorrectAlert];
			
			[self setupHeaderProgress];
			
            break;
        }
        case kABAExercisesStatusExerciseKO:
        {
            _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"sectionExercisesKOKey", @"incorrecto") withCorrect:NO];
            [_abaAlert showWithOffset:44];

			_checkAnswerKeyboardButton.enabled = YES;
			
            break;
        }
        case kABAExercisesStatusMustSave:
		{
			
			SimpleBlock block = ^
			{
				// And we jump to the next question or next group
				NSUInteger indexCurrentQuestion = [_currentQuestion.exercisesGroup.questions
												   indexOfObject:_currentQuestion];
				
				indexCurrentQuestion++;
				
				if(indexCurrentQuestion <= [_currentQuestion.exercisesGroup.questions count] - 1)
				{
					_currentQuestion = [_currentQuestion.exercisesGroup.questions objectAtIndex:indexCurrentQuestion];
                    
                    [self trackCrashlyticsProperties];
                    [[LegacyTrackingManager sharedManager] trackInteraction];
                    
					_currentGroup = _currentQuestion.exercisesGroup;
					
					// We delete previous blank text fields stored
					[self deleteBlankTextFields];
					[self updateElements];

					if([self checkForQuestionIntegrity]) {
						[self updateStatus:kABAExercisesStatusMustShowNextExercise];
					}
					
				}
				else
				{
					[self updateStatus:kABAExercisesStatusMustJumpToNextGroup];
				}
			};
			

			if(_readOnlyMode)
			{
				block();
			}
			else
			{
				[self saveWithBlock:block];
			}
			
            break;
        }
        case kABAExercisesStatusMustShowNextExercise:
        {
            [self dropCurrentExercisePhraseAnimation];
			
			[self setupViews];
            [self setupTranslationView];
            [self setupExercisePhraseView];
            [self setupExerciseView];
            [self setupExerciseTitleLabel];

			if(_readOnlyMode)
			{
				[self fillAnswersInTextFields];
			}

            [self setupCheckAnswerKeyboardButton];

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2*NSEC_PER_SEC), dispatch_get_main_queue(), ^
                           {
                               [self fadeInContentViewAnimation];
                           });

            break;
        }
        case kABAExercisesStatusMustJumpToNextGroup:
        {
            _exerciseTitleHasShownForFirstTime = NO;
            
            NSUInteger indexCurrentGroup = [self.section.exercisesGroups
                                            indexOfObject:_currentQuestion.exercisesGroup];
            
            indexCurrentGroup++;
            
            if(indexCurrentGroup <= [self.section.exercisesGroups count] - 1)
            {
                ABAExercisesGroup *nextGroup = [self.section.exercisesGroups objectAtIndex:indexCurrentGroup];
                
                _currentQuestion = [nextGroup.questions objectAtIndex:0];
                
                [self trackCrashlyticsProperties];
                [[LegacyTrackingManager sharedManager] trackInteraction];
                
                _currentGroup = _currentQuestion.exercisesGroup;
                
                [self deleteBlankTextFields];
                [self updateElements];
				
				if([self checkForQuestionIntegrity]) {
					[self updateStatus:kABAExercisesStatusMustShowNextGroup];
				}

                [self updateStatus:kABAExercisesStatusMustShowNextGroup];
            }
            else        // No more groups, so section is done
            {
                [self updateStatus:kABAExercisesStatusDone];
            }

            break;
        }
        case kABAExercisesStatusMustShowNextGroup:
        {
            [self dropCurrentExercisePhraseAnimation];
            
            [self hideTitleLabelWithBlock:^
             {
                 [self setupExerciseTitleLabel];
                 
                 [self showTitleLabelWithBlock:^
                  {
					  [self setupViews];
                      [self setupTranslationView];
                      
                      [self setupExercisePhraseView];
                      [self setupExerciseView];
                      [self setupCheckAnswerKeyboardButton];
					  
					  if(_readOnlyMode)
					  {
						  [self fillAnswersInTextFields];
					  }

                      _status = kABAExercisesStatusMustShowExercise;
                      
                      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2*NSEC_PER_SEC), dispatch_get_main_queue(), ^
                                     {
                                         [self fadeInContentViewAnimation];
                                         [self.scrollView setContentOffset:CGPointZero animated:YES];

                                     });
                  }];
             }];

            break;
        }
        case kABAExercisesStatusDone:
        {
			if(!_readOnlyMode)
			{
				[self.delegate showAlertCompletedUnit:kABAExercisesSectionType];
			}
			
            [self.navigationController popViewControllerAnimated:YES];

            break;
        }
    }
}

-(void)checkResultButton
{
	_checkAnswerKeyboardButton.enabled = NO;
	
    if(_abaAlert)
    {
        [_abaAlert dismiss];
    }
    
    if(_helpHasShown)
    {
        [self hideHelpView];
    }

	if([self.view.subviews containsObject:_titleView] &&
	   [SDVersion deviceSize] == Screen3Dot5inch)
	{
		[self hideTitleLabel];
	}

	if(_readOnlyMode)
	{
		[self updateStatus:kABAExercisesStatusExerciseOK];
	}
	else
	{
        if([self checkAnswers])
        {
            _checkAnswerKeyboardButton.enabled = NO;
            
            [self updateStatus:kABAExercisesStatusExerciseOK];
        }
        else
        {
            if ([self isAllBlankCorrectOrNull])
            {
                _checkAnswerKeyboardButton.enabled = YES;

            }
            else
            {
                [self updateStatus:kABAExercisesStatusExerciseKO];
            }
        }
	}
}

- (BOOL)checkAnswers
{
    ABARealmUser *user = [UserController currentUser];

    BOOL correct = YES;
    for (ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        ABAPhrase *phrase = [[ABAPhrase alloc] init];
        phrase.audioFile = blankTextField.correctAnswer.idPhrase;
        phrase.page = blankTextField.correctAnswer.page;

        if (![self checkTextInputForBlankTextField:blankTextField])
        {
            [_phrasesController setPhraseKO:phrase forSection:self.section errorText:blankTextField.text completionBlock:^(NSError *error, id object){
            }];

            correct = NO;

            // Cooladata tracking
            [CoolaDataTrackingManager trackVerifiedText:user.idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaExercises exerciseId:blankTextField.correctAnswer.idPhrase isResultCorrect:NO writtenText:blankTextField.text];
        }
        else
        {
            [_phrasesController setPhrasesDone:@[ phrase ] forSection:self.section sendProgressUpdate:YES completionBlock:^(NSError *error, id object){
            }];

            // Cooladata tracking
            [CoolaDataTrackingManager trackVerifiedText:user.idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaExercises exerciseId:blankTextField.correctAnswer.idPhrase isResultCorrect:YES writtenText:blankTextField.text];
        }
    }

    return correct;
}

-(BOOL)isAllBlankCorrectOrNull
{
    for(ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        if (![blankTextField.text isEqualToString:@""])
        {
            if (![self checkTextInputForBlankTextField:blankTextField])
            {
                return NO;
            }
        }
    }

    ABAExercisesTextField *focusedTextField = [_blankTextFields objectAtIndex:[self getPositionFocusedTextField]];
    if (![focusedTextField.text isEqualToString:@""])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(NSUInteger)getPositionFocusedTextField
{
    NSUInteger i = 0;
    for(ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        if ([blankTextField isFirstResponder])
        {
            return i;
        }
        i++;
    }
    
    return i;
}


-(NSString *)lowercaseTextMinusFirstLetters:(NSString *)text
{
	NSArray *words = [text componentsSeparatedByString:@" "];
	NSMutableString *finalText = [[NSMutableString alloc] init];
	
	for(NSString *word in words)
	{
		[finalText appendString:[NSString stringWithFormat:@"%@ ",[self convertToLowercase:word]]];
	}
	
	return finalText;
}

-(NSString *)convertToLowercase:(NSString *)text
{
	if([text length] <= 0)
	{
		return @"";
	}
	
	NSString *firstChar = [text substringToIndex:1];
	
	NSString *restOfLetters = [text substringWithRange:NSMakeRange(1, [text length]-1)];
	
	return [NSString stringWithFormat:@"%@%@",firstChar,[restOfLetters lowercaseString]];
}

-(NSString *)capitalizeFirstLetter:(NSString *)text
{
	if([text length] <= 0)
	{
		return @"";
	}
	
	NSString *firstChar = [text substringToIndex:1];
	
	NSString *restOfLetters = [text substringWithRange:NSMakeRange(1, [text length]-1)];
	
	return [NSString stringWithFormat:@"%@%@",[firstChar uppercaseString],restOfLetters];
}

-(BOOL)checkTextInputForBlankTextField: (ABAExercisesTextField *)blankTextField
{
    // First we convert the text following some rules and for mantaining the same coherence (contracted forms, lower/uppercase, etc)
    NSMutableString *answerFormatted = [[_solutionTextController formatSolutionText:blankTextField.text] mutableCopy];
	NSMutableString *capitalizedAnswerFormatted = [NSMutableString stringWithString:[self capitalizeFirstLetter:answerFormatted]];
	
    // We get a dictionary in wich the key is the index of the word in the phrase. The value will be blank if the word is correct.
    NSDictionary *errorDict = [_solutionTextController checkSolutionText:capitalizedAnswerFormatted withCorrectSolutionText:blankTextField.correctAnswer.text];
    
    BOOL almostOneWordIsIncorrect = [_solutionTextController checkPhrase:capitalizedAnswerFormatted withErrorDict:errorDict];
    BOOL missingWords = [_solutionTextController areThereMissingWords:capitalizedAnswerFormatted withErrorDict:errorDict];
    
    // Get a formatted string with colors green and red (correct or wrong)
    NSMutableAttributedString *mutAttStr = [_solutionTextController getAttributedString:answerFormatted withErrorDict:errorDict];
    
    // If there is almost one word that is incorrect, we apply the attributed string created just before
    if(almostOneWordIsIncorrect)
    {
        [blankTextField setText:answerFormatted];
        [blankTextField setAttributedText:mutAttStr.copy];
        
        if(!missingWords)
        {
            [self selectTextForInput:blankTextField
							 atRange:[_solutionTextController getFirstIncorrectWordPosition:answerFormatted withErrorDict:errorDict]];
        }
    }
    else
    {
        if(_solutionTextController.mustChangeOriginalText)
        {
            [blankTextField setText:blankTextField.correctAnswer.text];
        }
        else
        {
            [blankTextField setText:blankTextField.text];
        }
        
        NSMutableAttributedString *mutAttStr1 = [[NSMutableAttributedString alloc] initWithString:blankTextField.text];
        
        [mutAttStr1 addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:CONSTANT_fontSizeNormal]
                           range:[blankTextField.text rangeOfString:blankTextField.text]];
        
        [mutAttStr1 addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_REG_MAIL_BUT
                           range:[blankTextField.text rangeOfString:blankTextField.text]];
        
        [blankTextField setAttributedText:mutAttStr1.copy];
    }
    
    return !almostOneWordIsIncorrect;
}

-(void)selectTextForInput:(UITextField *)input atRange:(NSRange)range
{
    UITextPosition *start = [input positionFromPosition:[input beginningOfDocument]
                                                 offset:range.location];
    UITextPosition *end = [input positionFromPosition:start
                                               offset:range.length];
    [input setSelectedTextRange:[input textRangeFromPosition:start toPosition:end]];
}

-(void)checkForBlankTextFieldFirstResponderWithDelay: (CGFloat)delay
{
    if([_blankTextFields count] > 0)
    {
        ABAExercisesTextField *blankTextField = [_blankTextFields objectAtIndex:0];
        [blankTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:delay];
    }
}

-(void)saveWithBlock: (SimpleBlock)block
{
    __weak typeof (self) weakSelf = self;
    
    [_exercisesController setBlankPhrasesDoneForExercisesQuestion:_currentQuestion
                                   forABAExercises:self.section
                                   completionBlock:^(NSError *error, id object)
     {
         if(!error)
         {
             weakSelf.section = (ABAExercises *)object;
             
             [weakSelf setupHeaderProgress];
             
             block();
         }
         else
         {
             [weakSelf showAlertWithText:STRTRAD(@"saveDataExercisesErrorKey", @"Imposible guardar el progreso")];
         }
     }];
}

-(void)updateElements
{
    NSArray *itemsArray = [_exercisesController getExercisesItemsForExerciseQuestion:_currentQuestion forExercisesSection:self.section];
    _elementsArray = [self getElementsWithItemsArray:itemsArray];
    _blankTextFields = [self getBlankTextFieldsForElementsArray:_elementsArray];
}

-(BOOL)checkForQuestionIntegrity {
	
	// If the _blankTextFields array is unset or void, must skip this question (mostly an incoherent example entry)
	if(_blankTextFields == nil || [_blankTextFields count] == 0) {
		
		BOOL mustShowNextGroup;
		
		do {
			NSUInteger indexCurrentQuestion = [_currentQuestion.exercisesGroup.questions
											   indexOfObject:_currentQuestion];
			
			indexCurrentQuestion++;

			if(indexCurrentQuestion <= [_currentQuestion.exercisesGroup.questions count] - 1) {
				_currentQuestion = [_currentQuestion.exercisesGroup.questions objectAtIndex:indexCurrentQuestion];
				_currentGroup = _currentQuestion.exercisesGroup;
				
				NSArray *itemsArray = [_exercisesController getExercisesItemsForExerciseQuestion:_currentQuestion forExercisesSection:self.section];
				_elementsArray = [self getElementsWithItemsArray:itemsArray];
				_blankTextFields = [self getBlankTextFieldsForElementsArray:_elementsArray];
				
				mustShowNextGroup = NO;
			}
			else {
				mustShowNextGroup = YES;
			}
		} while(_blankTextFields == nil || [_blankTextFields count] == 0);
		
		
		if(mustShowNextGroup)
		{
			[self updateStatus:kABAExercisesStatusMustJumpToNextGroup];
		}
		else
		{
			[self updateStatus:kABAExercisesStatusMustShowNextExercise];
		}
		
		return NO;
	}
	else {
		return YES;
	}
}

-(void)setupExerciseTitleLabel
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:CONSTANT_fontSizeTitle];
    gettingSizeLabel.text = _currentQuestion.exercisesGroup.title;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH, 9999);
    CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];

	_titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, size.height+(IS_IPAD?50.0f:30.0f))];
    _titleView.backgroundColor = [UIColor whiteColor];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 15.0f, size.width, size.height)];
    
    _titleLabel.numberOfLines = 0;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    [_titleLabel setText:_currentQuestion.exercisesGroup.title];
    [_titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:CONSTANT_fontSizeTitle]];
    [_titleLabel setTextColor:ABA_COLOR_DARKGREY];
    [_titleLabel layoutIfNeeded];
    
    if(([SDVersion deviceSize] != Screen3Dot5inch ||
       !_exerciseTitleHasShownForFirstTime) &&
	   _currentQuestion.exercisesGroup.title.length > 0)
    {
        _exerciseTitleHasShownForFirstTime = YES;
        
        [_titleView addSubview:_titleLabel];
        [self.view addSubview:_titleView];
        
        if ([SDVersion deviceSize] == Screen3Dot5inch)
        {
            CALayer *layer = _titleView.layer;
            layer.shadowOffset = CGSizeMake(1, 1);
            layer.shadowColor = [[UIColor lightGrayColor] CGColor];
            layer.shadowRadius = 10.0f;
            layer.shadowOpacity = 0.90f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            _singleTitleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTitleLabel)];
            [self.view addGestureRecognizer:_singleTitleTap];
            
        }
    }
}

-(void)hideTitleLabel
{
    [self.view removeGestureRecognizer:_singleTitleTap];
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        [_titleView setFrame:CGRectMake(_titleView.frame.origin.x, -_titleView.frame.size.height, _titleView.frame.size.width, _titleView.frame.size.height)];
    } completion:^(BOOL finished) {
        [_titleView removeFromSuperview];
    }];
}

-(void)setupViews {
	_contentView = [UIView newAutoLayoutView];
	_exerciseView = [UIView newAutoLayoutView];
	_translationView = [UIView newAutoLayoutView];
	_exercisePhraseView = [UIView newAutoLayoutView];

	[_contentView addSubview:_translationView];
	[_contentView addSubview:_exerciseView];
	[_exerciseView addSubview:_exercisePhraseView];

	[self.scrollContentView addSubview:_contentView];
}

-(void)setupExerciseView
{
    CGFloat alpha = 0.0f;
    
    if(!_firstTimePresented)
    {
        _firstTimePresented = YES;
        alpha = 1.0f;
    }
    
    _exerciseView.backgroundColor = ABA_COLOR_EXERC_BG;
    
    UIView *previousElement = nil;
    CGFloat contentViewHeight = 0.0f;
    
    if([_superiorElements count] > 0)
    {
        previousElement = [_superiorElements objectAtIndex:0];
        
        contentViewHeight += previousElement.frame.size.height;
    }

	[_exerciseView autoSetDimension:ALDimensionHeight toSize:(_exercisePhraseView.frame.size.height + (CONSTANT_phraseViewPadding*2))];
	[_exerciseView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.exerciseView.superview];
	[_exerciseView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
	[_exerciseView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:contentViewHeight];
	[_exerciseView layoutIfNeeded];
	
    UIView *topBorder = [UIView newAutoLayoutView];
	topBorder.backgroundColor = ABA_COLOR_EXERC_BORDER;
	[_exerciseView addSubview:topBorder];

	[topBorder autoSetDimension:ALDimensionHeight toSize:1.0];
	[topBorder autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_exerciseView];
	[topBorder autoPinEdgeToSuperviewEdge:ALEdgeTop];
	[topBorder autoPinEdgeToSuperviewEdge:ALEdgeLeft];
	[topBorder layoutIfNeeded];
	
	UIView *bottomBorder = [UIView newAutoLayoutView];
	bottomBorder.backgroundColor = ABA_COLOR_EXERC_BORDER;
	[_exerciseView addSubview:bottomBorder];

	[bottomBorder autoSetDimension:ALDimensionHeight toSize:1.0];
	[bottomBorder autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_exerciseView];
	[bottomBorder autoPinEdgeToSuperviewEdge:ALEdgeBottom];
	[bottomBorder autoPinEdgeToSuperviewEdge:ALEdgeLeft];
	[bottomBorder layoutIfNeeded];
	
	_infoButton = [UIButton newAutoLayoutView];
	[_exerciseView addSubview:_infoButton];
	
	[_infoButton autoSetDimension:ALDimensionWidth toSize:IS_IPAD?50.0f:22.0f];
	[_infoButton autoSetDimension:ALDimensionHeight toSize:IS_IPAD?50.0f:22.0f];
	[_infoButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:IS_IPAD?25.0f:5.0f];
	[_infoButton autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:IS_IPAD?15.0f:5.0f];
	[_infoButton layoutIfNeeded];
	
    _infoButton.backgroundColor = ABA_COLOR_DARKGREY4;
    _infoButton.layer.cornerRadius = _infoButton.frame.size.width/2;
    _infoButton.layer.masksToBounds = YES;
	
	UIImageView *buttonImage = [UIImageView newAutoLayoutView];
	[buttonImage setImage:[UIImage imageNamed:@"info"]];
	[_infoButton addSubview:buttonImage];
	
	[buttonImage autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_infoButton withMultiplier:0.30];
	[buttonImage autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_infoButton withMultiplier:0.50];
	[buttonImage autoCenterInSuperview];
	[buttonImage layoutIfNeeded];
	
    [_infoButton addTarget:self action:@selector(showInfoCorrectAnswer:) forControlEvents:UIControlEventTouchUpInside];
    _infoButton.adjustsImageWhenHighlighted = NO;
    _infoButton.adjustsImageWhenDisabled = NO;
	
	_contentView.alpha = alpha;
	contentViewHeight += _exerciseView.frame.size.height;

	[_contentView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
	_contentViewBottomConstraint = [_contentView autoPinEdgeToSuperviewEdge:ALEdgeBottom];
	[_contentView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_contentView.superview];
	[_contentView autoSetDimension:ALDimensionHeight toSize:contentViewHeight];
	[_contentView layoutIfNeeded];
}

-(void)setupExercisePhraseView
{
    CGFloat minHeightForExercisePhraseView = LINE_HEIGHT;
    CGFloat currentLineY = 0.0f;
    CGFloat currentLineX = 0.0f;
    NSUInteger numLines = 0;
	
    // We place the elements in _exercisePhraseView
    for(id object in _elementsArray)
    {
        UIView *objectView = (UIView *)object;
        
        if([_elementsArray indexOfObject:object] == 0)   // The first element is easy to place (0,0).
        {
            if([objectView isKindOfClass:[ABAExercisesTextField class]])
            {
                ABAExercisesTextField *blankTextField = (ABAExercisesTextField*)objectView;
                
                if(blankTextField.frame.size.width >
                   [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH)
                {
                    CGFloat maxWidth = [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH;
                    
                    blankTextField.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeNormal];;
                    
                    blankTextField.frame = CGRectMake(objectView.frame.origin.x, objectView.frame.origin.y, maxWidth, objectView.frame.size.height);
                }
            }

            [_exercisePhraseView addSubview:objectView];
        }
        else                                            // Posterior elements must know previous elements width and position.
        {
            NSUInteger previousElementIndex = [_elementsArray indexOfObject:object] - 1;
            id previousObject = [_elementsArray objectAtIndex:previousElementIndex];
            UIView *previousObjectView = (UIView *)previousObject;
            CGSize previousElementSize = [self getSizeForObject:[_elementsArray objectAtIndex:previousElementIndex]];
            CGPoint previousElementPosition = [self getPositionForObject:[_elementsArray objectAtIndex:previousElementIndex]];
            
            // Check if it's necessary to add a new line
            if((previousElementSize.width + previousElementPosition.x + objectView.frame.size.width) >=
               [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH)
            {
                minHeightForExercisePhraseView += LINE_HEIGHT;
                currentLineY += LINE_HEIGHT + 5.0f  ;
                currentLineX = 0.0f;
                numLines++;
                
                if([objectView isKindOfClass:[ABAExercisesTextField class]])
                {
                    ABAExercisesTextField *blankTextField = (ABAExercisesTextField*)objectView;

                    if(objectView.frame.size.width >
                       [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH)
                    {
                        CGFloat maxWidth = [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH;
                        blankTextField.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeNormal];;

                        objectView.frame = CGRectMake(objectView.frame.origin.x, objectView.frame.origin.y, maxWidth, objectView.frame.size.height);
                    }
                }
            }
            else
            {
                currentLineX = (previousElementSize.width) + previousElementPosition.x;
                
                // If the previous object is a textfield, we add a little separator
                if([previousObject isKindOfClass:[ABAExercisesTextField class]])
                {
                    currentLineX += TEXTFIELD_PADDING_RIGHT;
                }
                
                if([objectView isKindOfClass:[ABAExercisesTextField class]])
                {
                    ABAExercisesTextField *blankTextField = (ABAExercisesTextField*)objectView;

                    if(objectView.frame.size.width + previousObjectView.frame.origin.x >
                       [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH)
                    {
                        CGFloat maxWidth = [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH - previousObjectView.frame.origin.x;
                        blankTextField.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeNormal];

                        objectView.frame = CGRectMake(objectView.frame.origin.x, objectView.frame.origin.y, maxWidth, objectView.frame.size.height);
                    }
                }
            }
            
            objectView.frame = CGRectMake(currentLineX, currentLineY, objectView.frame.size.width, objectView.frame.size.height);
            
            [_exercisePhraseView addSubview:objectView];
        }
    }
    
    CGRect frame = CGRectMake(CONSTANT_phraseViewPadding, CONSTANT_phraseViewPadding, [UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH, minHeightForExercisePhraseView+numLines*5);
    
    _exercisePhraseView.frame = frame;
}

-(NSArray *)getElementsWithItemsArray: (NSArray *)itemsArray
{
    NSMutableArray *elementsArray = [[NSMutableArray alloc] init];

    // Here we create all the elements with corresponding size
    for(ABAExercisePhraseItem *item in itemsArray)
    {
        if(item.type == kABAExercisePhraseItemTypeNormal)
        {
            CGSize size = [self getSizeForLabelText:item.text];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, CONSTANT_itemHeight)];
            
            label.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeBig];
            label.text = item.text;
            label.textColor = ABA_COLOR_DARKGREY;
            
            [elementsArray insertObject:label atIndex:[itemsArray indexOfObject:item]];
        }
        else   // kABAExercisePhraseItemTypeBlank
        {
            CGSize size = [self getSizeForTextFieldText:item.text];
            
            ABAExercisesTextField *blankTextField = [[ABAExercisesTextField alloc] init];
            blankTextField.delegate = self;
            [blankTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

            blankTextField.correctAnswer = item;
            
            CGFloat textFieldWidth = size.width + 20.0f;
            
            blankTextField.frame = CGRectMake(0, 0, textFieldWidth, CONSTANT_itemHeight);
            [elementsArray insertObject:blankTextField atIndex:[itemsArray indexOfObject:item]];
        }
    }

    return elementsArray;
}

-(NSString *)getTranslationForCurrentStatus
{
    return _currentQuestion.exerciseTranslation;
}

-(NSMutableArray *)getBlankTextFieldsForElementsArray: (NSArray *)elementsArray
{
    NSMutableArray *array = [[NSMutableArray alloc] init];

    for(id object in elementsArray)
    {
        if([object isKindOfClass:[ABAExercisesTextField class]])
        {
            [array addObject:object];
        }
    }
    
    return array;
}

-(CGPoint)getPositionForObject: (id)object
{
    UIView *objectView = (UIView *)object;
    
    return objectView.frame.origin;
}

-(CGSize)getSizeForObject: (id)object
{
    UIView *objectView = (UIView *)object;
    
    return objectView.frame.size;
}

-(CGSize)getSizeForLabelText: (NSString *)text
{
    return [text sizeWithAttributes:
            @{NSFontAttributeName:
                  [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeBig]}];

}

-(CGSize)getSizeForTextFieldText: (NSString *)text
{
    return [text sizeWithAttributes:
            @{NSFontAttributeName:
                  [UIFont fontWithName:ABA_MUSEO_SANS_700 size:CONSTANT_fontSizeLarge]}];

}

- (void)setupCheckAnswerKeyboardButton
{
    _checkAnswerKeyboardButton = [UIButton newAutoLayoutView];
    
    _checkAnswerKeyboardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, CONSTANT_buttonHeight)];
    [_checkAnswerKeyboardView setBackgroundColor:ABA_COLOR_BLUE_WRITE_UNIT];
    [_checkAnswerKeyboardView addSubview:_checkAnswerKeyboardButton];

    [_checkAnswerKeyboardButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [_checkAnswerKeyboardButton autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [_checkAnswerKeyboardButton autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_checkAnswerKeyboardView];
    [_checkAnswerKeyboardButton autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_checkAnswerKeyboardView];
    [_checkAnswerKeyboardButton layoutIfNeeded];

    if (_readOnlyMode)
    {
         NSString *titleButton = [STRTRAD(@"nextResultExercisesKey", @"Siguiente") uppercaseString];
        [_checkAnswerKeyboardButton setTitle:titleButton forState:UIControlStateNormal];
    }
    else
    {
         NSString *titleButton = [STRTRAD(@"checkResultExercisesKey", @"Comprobar resultado") uppercaseString];
        [_checkAnswerKeyboardButton setTitle:titleButton forState:UIControlStateNormal];
    }

    _checkAnswerKeyboardButton.titleLabel.font = [UIFont fontWithName:ABA_RALEWAY_BOLD size:CONSTANT_fontSizeSmall];
    _checkAnswerKeyboardButton.titleLabel.textColor = [UIColor whiteColor];
    [_checkAnswerKeyboardButton addTarget:self action:@selector(checkResultButton) forControlEvents:UIControlEventTouchUpInside];

    for (ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        blankTextField.inputAccessoryView = _checkAnswerKeyboardView;
        [blankTextField reloadInputViews];
    }
}

- (void)fillAnswersInTextFields
{
    for (ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        blankTextField.text = blankTextField.correctAnswer.text;
        blankTextField.textColor = ABA_COLOR_REG_MAIL_BUT;
    }
}

-(void)setupTranslationView
{
    if([self getTranslationForCurrentStatus] != nil &&
       ![[self getTranslationForCurrentStatus] isEqualToString:@""])
    {
        _translationLabel = [UILabel newAutoLayoutView];
		_translationView.backgroundColor = [UIColor whiteColor];

		[_contentView addSubview:_translationView];
		[_translationView addSubview:_translationLabel];
	
		_translationLabel.numberOfLines = 0;
        _translationLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _translationLabel.text = [self getTranslationForCurrentStatus];

        UILabel *gettingSizeLabel = [[UILabel alloc] init];
        gettingSizeLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeTranslation];
        gettingSizeLabel.text = _translationLabel.text;
        gettingSizeLabel.numberOfLines = 0;
        gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
        CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*PROPORTIONAL_WIDTH, 9999);
        CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];

		[_translationView autoSetDimension:ALDimensionWidth toSize:IS_IPAD?MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height):[UIScreen mainScreen].bounds.size.width];
		[_translationView autoSetDimension:ALDimensionHeight toSize:size.height+(IS_IPAD?20.0f:10.0f)];
		[_translationView autoPinEdgeToSuperviewEdge:ALEdgeTop];
		[_translationView autoPinEdgeToSuperviewEdge:ALEdgeLeft];
		[_translationView layoutIfNeeded];
		
		[_translationLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_translationView withMultiplier:PROPORTIONAL_WIDTH];
		[_translationLabel autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_translationView];
		[_translationLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
		[_translationLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
		[_translationLabel layoutIfNeeded];
		
        _translationLabel.text = [self getTranslationForCurrentStatus];
        
        _translationLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeTranslation];
        _translationLabel.textColor = ABA_COLOR_DARKGREY;
		
        [_superiorElements insertObject:_translationView atIndex:0];
    }
}

-(void)showCorrectAlert
{
	CGFloat height;
	
	if([SDVersion deviceSize] == Screen3Dot5inch)
	{
		height = 40.0f;
	}
	else
	{
		height = _titleView.frame.size.height;
	}

    CGRect finalFrame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, height);
    
    _correctAlertView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, -44.0f, self.view.frame.size.width, height)];
    _correctAlertView.backgroundColor = ABA_COLOR_REG_MAIL_BUT;
    
    UIButton *speakerButton = [[UIButton alloc] initWithFrame:CGRectMake(_correctAlertView.frame.size.width - 50.0f, (_correctAlertView.frame.size.height/2)-11.0f, 22.0f, 22.0f)];
    [speakerButton setImage:[UIImage imageNamed:@"speakerIcon"] forState:UIControlStateNormal];
	
	UILabel *titleLabel = [UILabel newAutoLayoutView];
	[_correctAlertView addSubview:titleLabel];

	[titleLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_correctAlertView];
	[titleLabel autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_correctAlertView];
	[titleLabel autoCenterInSuperview];
	[titleLabel layoutIfNeeded];
	
    titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeNormal];
    titleLabel.textColor = [UIColor whiteColor];
	titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = STRTRAD(@"sectionExercisesOKKey", @"correcto");
    
    [_correctAlertView addSubview:speakerButton];
    [self.view addSubview:_correctAlertView];

    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _correctAlertView.frame = finalFrame;
                     } completion:^(BOOL finished)
    {
        [self playCorrectAnswer];
    }];
}

- (void)hideCorrectAlert
{
    [UIView animateWithDuration:0.2
        delay:0.0
        options:UIViewAnimationOptionCurveEaseOut
        animations:^{
            _correctAlertView.frame = CGRectMake(0.0f, -44.0f, self.view.frame.size.width, 44.0f);
        }
        completion:^(BOOL finished) {
            [_correctAlertView removeFromSuperview];

            [self updateStatus:kABAExercisesStatusMustSave];

        }];
}

-(void)showInfoCorrectAnswer: (id)sender
{
    NSMutableString *correctAnswersString = [[NSMutableString alloc] init];
    
    for(ABAExercisesTextField *blankTextField in _blankTextFields)
    {
        // We create a temporal ABAPhrase object for checking with PhraseController
        ABAPhrase *phrase = [[ABAPhrase alloc] init];
        phrase.text = blankTextField.correctAnswer.text;

        [correctAnswersString appendString:[_solutionTextController formatSolutionText:phrase.text]];
        
        if([_blankTextFields indexOfObject:blankTextField] != [_blankTextFields count] - 1)
        {
            [correctAnswersString appendString:@", "];
        }
    }
	
	//[self checkAnswers];
	
    [self toggleHelpViewWithText:correctAnswersString];
}

-(void)toggleHelpViewWithText: (NSString *)text
{
    if(_abaAlert)
    {
        [_abaAlert dismiss];
    }

    if(!_helpHasShown)
    {
        [self showHelpViewWithText:text];
    }
    else
    {
        [self hideHelpView];
    }
}

- (void)showHelpViewWithText:(NSString *)text
{
    ABAExercisesTextField *blankTextField = [_blankTextFields objectAtIndex:0];

    ABAPhrase *phrase = [[ABAPhrase alloc] init];
    phrase.audioFile = blankTextField.correctAnswer.audioFile;
    phrase.page = blankTextField.correctAnswer.page;

    [DataController saveProgressActionForSection:self.section andUnit:nil andPhrases:@[ phrase ] errorText:@"" isListen:NO isHelp:YES completionBlock:^(NSError *error, id object){

    }];

    _infoButton.enabled = NO;

    CGRect frame;
    CGRect labelFrame;

    if ([self getTranslationForCurrentStatus] != nil &&
        ![[self getTranslationForCurrentStatus] isEqualToString:@""])
    {
        frame = _translationView.frame;
        labelFrame = _translationLabel.frame;
    }
    else
    {
        frame = CGRectMake(0, _exerciseView.frame.origin.y - (IS_IPAD ? 50.0f : 30.0f), MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), (IS_IPAD ? 50.0f : 30.0f));
        labelFrame = CGRectMake(10.0f, 5.0f, MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height) - 20.0f, (IS_IPAD ? 40.0f : 20.0f));
    }

    // We create a screenshot and place it in the main view for animation purpose

    _infoView = [[UIView alloc] initWithFrame:frame];
    _infoView.backgroundColor = ABA_COLOR_DARKGREY4;

    _infoView.frame = CGRectMake(MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), _infoView.frame.origin.y, _infoView.frame.size.width, _infoView.frame.size.height);

    UILabel *infoLabel = [UILabel newAutoLayoutView];
    [_infoView addSubview:infoLabel];

    [infoLabel autoSetDimension:ALDimensionWidth toSize:labelFrame.size.width];
    [infoLabel autoSetDimension:ALDimensionHeight toSize:labelFrame.size.height];
    [infoLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
    [infoLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [infoLabel layoutIfNeeded];

    infoLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:CONSTANT_fontSizeTranslation];
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.text = text;

    [self.contentView addSubview:_infoView];

    [UIView animateWithDuration:0.3
        delay:0.0
        options:UIViewAnimationOptionCurveEaseIn
        animations:^{

            _infoButton.transform = CGAffineTransformMakeRotation(-3.141516 / 2);
            [_translationLabel setFrame:CGRectMake(-[UIScreen mainScreen].bounds.size.width, _translationLabel.frame.origin.y, _translationLabel.frame.size.width, _translationLabel.frame.size.height)];

            [_infoView setFrame:frame];
        }
        completion:^(BOOL finished) {
            _helpHasShown = YES;
            _infoButton.enabled = YES;

            _infoViewTimer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(hideHelpView) userInfo:nil repeats:NO];

            _singleHelpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHelpView)];
            [self.view addGestureRecognizer:_singleHelpTap];

        }];
    
    // Cooladata tracking
    [CoolaDataTrackingManager trackTextSuggestion:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaExercises exerciseId:blankTextField.correctAnswer.idPhrase];
}

-(void)hideHelpView
{
    [self.view removeGestureRecognizer:_singleHelpTap];
    _infoButton.enabled = NO;
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _infoButton.transform = CGAffineTransformMakeRotation(0);
                         
                         [_translationLabel setFrame:CGRectMake(10.0f, _translationLabel.frame.origin.y, _translationLabel.frame.size.width, _translationLabel.frame.size.height)];
						 
                         [_infoView setFrame:CGRectMake(MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height), _infoView.frame.origin.y, _infoView.frame.size.width, _infoView.frame.size.height)];
                         
                     } completion:^(BOOL finished)
     {
         [_infoViewTimer invalidate];
         _infoViewTimer = nil;
         
         _helpHasShown = NO;
         _infoButton.enabled = YES;
         
         [_infoView removeFromSuperview];
     }];
}

-(void)dropCurrentExercisePhraseAnimation
{
    // We create a screenshot and place it in the main view for animation purpose
    UIImageView *droppedView = [[UIImageView alloc] initWithFrame:_contentView.frame];
    droppedView.image = [ImageHelper imageWithView:self.contentView];
    [self.scrollContentView addSubview:droppedView];
    
    // We clear/delete previous views
    [_contentView removeFromSuperview];
    
    for(UIView *view in _superiorElements)
    {
        [view removeFromSuperview];
    }
	
    _superiorElements = [[NSMutableArray alloc] init];
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{

                         droppedView.layer.masksToBounds = NO;
                         droppedView.layer.shadowOffset = CGSizeMake(-1, 1);
                         droppedView.layer.shadowRadius = 0.2;
                         droppedView.layer.shadowOpacity = 0.1;

                         droppedView.transform = CGAffineTransformMakeRotation(70);
                         droppedView.center = CGPointMake(-[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
                     } completion:^(BOOL finished)
    {
        [droppedView removeFromSuperview];
                     }];
}

-(void)fadeInContentViewAnimation
{
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _contentView.alpha = 1.0f;
                     } completion:^(BOOL finished) {
                         _checkAnswerKeyboardButton.enabled = YES;
						 
						 [self checkForBlankTextFieldFirstResponderWithDelay:0.0f];
                     }];
}

-(void)showAlertWithText: (NSString *)text
{
    _abaAlert = [[ABAAlert alloc] initAlertWithText:text];
    [_abaAlert showWithOffset:44];
}

-(void)hideTitleLabelWithBlock: (SimpleBlock)block
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         _titleLabel.alpha = 0.0f;
                     } completion:^(BOOL finished)
     {
         block();
     }];
}

-(void)showTitleLabelWithBlock: (SimpleBlock)block
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _titleLabel.alpha = 1.0f;
                     } completion:^(BOOL finished)
     {
         block();
     }];
}

- (void)playCorrectAnswer
{
    ABAExercisesTextField *blankTextField = [_blankTextFields firstObject];
    if (blankTextField == nil)
    {
        [self audioFinished];
    }

    NSString *audioFile = [[blankTextField.correctAnswer.audioFile componentsSeparatedByString:@"_"] objectAtIndex:0];
    if (![_audioController checkIfAudioExists:audioFile andUnit:self.section.unit forRec:NO])
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    }

    [_audioController playAudioFile:audioFile withUnit:self.section.unit forRec:NO];
}

#pragma mark - AudioController delegate methods

-(void) audioStarted
{
    [SVProgressHUD dismiss];
}

- (void)audioFinished
{
    [self hideCorrectAlert];

    _checkAnswerKeyboardButton.enabled = YES;

    // Cooladata tracking
    ABAExercisesTextField *blankTextField = [_blankTextFields firstObject];
    if (blankTextField != nil)
    {
        [CoolaDataTrackingManager trackListenedToAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaExercises exerciseId:blankTextField.correctAnswer.idPhrase];
    }
    
#ifdef SHEPHERD_DEBUG
    if ([ABAShepherdAutomatorPlugin isAutomationEnabled])
    {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf fillAnswersInTextFields];
            [weakSelf checkResultButton];
        });
    }
#endif
}

-(void) audioDidGetError:(NSError *)error
{
	[SVProgressHUD dismiss];
	
	[self hideCorrectAlert];
	
	_checkAnswerKeyboardButton.enabled = YES;
	//
	//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3*NSEC_PER_SEC), dispatch_get_main_queue(), ^
	//                   {
	//                       [self hideCorrectAlert];
	//
	//                       [self showAlertWithText:error.localizedDescription];
	//				   });
}

#pragma mark - UITextFieldDelegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self checkResultButton];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField*)textField
{
    // hide shorcuts panel
    if ([textField respondsToSelector:@selector(inputAssistantItem)])
    {
        UITextInputAssistantItem* item = [textField inputAssistantItem];
        item.leadingBarButtonGroups = @[];
        item.trailingBarButtonGroups = @[];
    }
}

-(void)textFieldDidChange:(UITextField *)textField
{
    textField.textColor = ABA_COLOR_DARKGREY;
    
    if(_helpHasShown)
    {
        [self hideHelpView];
    }
    
    if([self.view.subviews containsObject:_titleView] &&
       [SDVersion deviceSize] == Screen3Dot5inch)
    {
        [self hideTitleLabel];
    }
}

#pragma mark - Keyboard notifications

- (void)keyboardWillToggle:(NSNotification *)notification
{
    NSDictionary* userInfo = [notification userInfo];
    NSTimeInterval duration;
    UIViewAnimationCurve animationCurve;
    CGRect startFrame;
    CGRect endFrame;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]    getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey]        getValue:&startFrame];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]          getValue:&endFrame];
    
    if (CGRectEqualToRect(startFrame, endFrame))
    {
        return;
    }
    
    NSInteger signCorrection = 1;
    if (startFrame.origin.y < 0 || startFrame.origin.x < 0 || endFrame.origin.y < 0 || endFrame.origin.x < 0)
        signCorrection = -1;
    
    CGFloat heightChange = (endFrame.origin.y - startFrame.origin.y) * signCorrection;
    
    // We ignore this notification because it appears to be a error (iPads with >=iOS9)
    if (IS_IPAD && abs((int)heightChange) == KB_CHANGE_HEIGHT_IGNORE)
    {
        return;
    }
    
    CGRect frame = _contentView.frame;
    frame.origin.y = frame.origin.y + heightChange;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:(animationCurve << 16)|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _contentViewBottomConstraint.constant += heightChange;
                         [_contentView layoutIfNeeded];
                     }
                     completion:NULL];
}

#pragma mark - Crashlytics properties traking

- (void)trackCrashlyticsProperties
{
    if (_currentQuestion.phrases.count != 0)
    {
        ABAExercisesPhrase *exercisePhrase = _currentQuestion.phrases[0];
        [[Crashlytics sharedInstance] setObjectValue:exercisePhrase.text forKey:@"Current phrase"];
    }
}

@end
