//
//  AppDelegate.h
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"
#import "ProgressController.h"
#import "UserController.h"
#import "ImageHelper.h"
#import <Crashlytics/Crashlytics.h>

#import "RootViewInput.h"
#import "RootViewOutput.h"

@class SupportedVersionManager;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CrashlyticsDelegate, RootViewInput>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ProgressController *progressController;
@property (strong, nonatomic) SupportedVersionManager *versionManager;

@property (strong, nonatomic) NSObject<RootViewOutput> *presenter;

- (void)syncProgress;

/**
 Push notification registration
 */
- (void)registerForRemoteNotification;

/**
 Push notification registration
 */
- (void)unregisterForRemoteNotification;

/**
 Method to reset the APNS configuration
 */
- (void)resetAPNSConfiguration;


@end

