//
//  LoginParameters.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

struct LoginParameters {

    let deviceId: String
    let sysOper: String
    let deviceName: String
    let email: String
    let password: String
    let appVersion: String
}

extension LoginParameters: ApiSignable {

    var orderedParams: [String] {
        return ["deviceId", "sysOper", "deviceName", "email", "password", "appVersion"]
    }

    func params() -> [String: String] {
        return ["deviceId": deviceId, "sysOper": sysOper, "deviceName": deviceName, "email": email, "password": password, "appVersion": appVersion]
    }
}