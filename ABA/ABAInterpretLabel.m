//
//  ABAInterpretLabel.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAInterpretLabel.h"
#import "ABAInterpretPhrase.h"
#import "Resources.h"
#import<CoreText/CoreText.h>
#import "Utils.h"

#define PADDING_LABEL 15

@implementation ABAInterpretLabel

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if((self = [super initWithCoder:aDecoder]))
    {
        [self setupView];
    }
    return self;
}

-(id) initWithFrame:(CGRect)frame
{
    if((self = [super initWithFrame:frame]))
    {
        [self setupView];
    }
    return self;
}

-(void)setupWithABAInterpretPhrase: (ABAInterpretPhrase *)phrase andStatus:(kABAInterpretRowStatus)status
{
    _phrase = phrase;
    _status   = status;
    
    [self setupText];
}

- (void)setupView
{
    self.edgeInsets = UIEdgeInsetsMake(PADDING_LABEL, PADDING_LABEL, PADDING_LABEL, PADDING_LABEL);
    self.backgroundColor = [UIColor whiteColor];
    
	self.layer.cornerRadius = IS_IPAD?5:3;
    self.layer.masksToBounds = YES;
}

- (void)setupText
{
    self.text = _phrase.text;

    [self setAttributedText:[self getTextAttributes]];
    
}

-(NSMutableAttributedString *)getTextAttributes
{
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:self.text];
    
    switch(_status)
    {
        case kABAInterpretRowStatusNormal:
        {
            [self setTextColor:ABA_COLOR_LIGHTGREY2];
            
            [mutAttStr addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                              range:[_phrase.text rangeOfString:_phrase.text]];
            
            break;
        }
        case kABAInterpretRowStatusCurrent:
        {
            [self setTextColor:ABA_COLOR_DARKGREY];
            
            [mutAttStr addAttribute:NSFontAttributeName
							  value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?18:16]
                              range:[_phrase.text rangeOfString:_phrase.text]];
            break;
        }
    }
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:5.0f];
    [mutAttStr addAttribute:NSParagraphStyleAttributeName
                      value:style
                      range:[_phrase.text rangeOfString:_phrase.text]];
    
    return [mutAttStr copy];
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    UIEdgeInsets insets = self.edgeInsets;
    CGRect rect = [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, insets)
                    limitedToNumberOfLines:numberOfLines];
    
    rect.origin.x    -= insets.left;
    rect.origin.y    -= insets.top;
    rect.size.width  += (insets.left + insets.right);
    rect.size.height += (insets.top + insets.bottom);
    
    return rect;
}

- (void)drawTextInRect:(CGRect)rect
{
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

@end