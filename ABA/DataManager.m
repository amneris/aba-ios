//
//  DataSource.m
//  ABA
//
//  Created by Oriol Vilaró on 7/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

#pragma mark Singleton Methods

+ (nonnull instancetype)sharedManager
{
    static DataManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)clearData
{
    _progressCache = nil;
}

@end
