//
//  RegisterViewOutput.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol RegisterViewOutput: FormValidator {

    // Actions
    func registerUser(name: String, email: String, password: String)
    func registerUserWithFacebook()
    func goToForgotPassword()
    func goToLogin()
}
