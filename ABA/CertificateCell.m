//
//  CertificateCell.m
//  ABA
//
//  Created by Jordi Mele on 10/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "CertificateCell.h"
#import "Resources.h"
#import "Utils.h"

@implementation CertificateCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
	[_certificateTitleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?24:16]];
    [_certificateTitleLabel setTextColor:ABA_COLOR_BLUE_MENU_UNIT];
    
    [_certificateSubTitleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?21:14]];
    [_certificateSubTitleLabel setTextColor:ABA_COLOR_INTRO_TITLE_BUT];
}

@end
