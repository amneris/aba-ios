//
//  LanguageController.h
//  ABA
//
//  Created by Oriol Vilaró on 13/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@interface LanguageController : NSObject

+ (NSString *)getCurrentLanguage;
+ (void)setCurrentLanguage:(NSString *)newLanguage;
+ (NSString *)getDeviceLanguageCodeKey;
+ (void)resetCurrentLanguage;
+ (NSArray *)getSupportedLanguages;

@end
