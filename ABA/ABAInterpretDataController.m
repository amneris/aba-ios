//
//  ABAInterpretDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAInterpretDataController.h"
#import <Realm/Realm.h>
#import "ABAUnit.h"
#import "ABAInterpret.h"

@implementation ABAInterpretDataController

+ (void)parseABAInterpretSection:(NSArray *)abaInterpretDataArray
                          onUnit:(ABAUnit *)unit
                 completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAInterpret *interpret = unit.sectionInterpret;
    
    __block NSMutableDictionary *rolesUrlImages = [[NSMutableDictionary alloc] init];
    __block NSMutableDictionary *rolesUrlBigImages = [[NSMutableDictionary alloc] init];
    
    for (ABARole *role in unit.roles)
    {
        [rolesUrlImages setObject:role.imageUrl forKey:role.name];
        [rolesUrlBigImages setObject:role.imageBigUrl forKey:role.name];
    }
    
    [realm transactionWithBlock:^{
        
        if (!interpret)
        {
            interpret = [[ABAInterpret alloc] init];
            unit.sectionInterpret = interpret;
        }
        
        NSMutableDictionary *rolesDict = [[NSMutableDictionary alloc] init];
        
        //
        //  We iterate in dialog array
        //
        for (NSDictionary *abaInterpretData in abaInterpretDataArray)
        {
            if ([rolesDict objectForKey:abaInterpretData[@"Role"]] == nil)
            {
                ABAInterpretRole *role = [[ABAInterpretRole alloc] init];
                role.name = abaInterpretData[@"Role"];
                role.completed = [NSNumber numberWithInt:0];
                
                if ([rolesUrlImages objectForKey:abaInterpretData[@"Role"]] != nil)
                {
                    role.imageUrl = [rolesUrlImages objectForKey:abaInterpretData[@"Role"]];
                }
                
                if ([rolesUrlBigImages objectForKey:abaInterpretData[@"Role"]] != nil)
                {
                    role.imageBigUrl = [rolesUrlBigImages objectForKey:abaInterpretData[@"Role"]];
                }
                
                [rolesDict setObject:role forKey:abaInterpretData[@"Role"]];
                
                [interpret.roles addObject:role];
            }
            
            if (abaInterpretData[@"dialog"] != nil &&
                [abaInterpretData[@"dialog"] isKindOfClass:[NSArray class]])
            {
                for (NSDictionary *dialogData in [abaInterpretData objectForKey:@"dialog"])
                {
                    // We create here phrase
                    ABAInterpretPhrase *phrase = [[ABAInterpretPhrase alloc] init];
                    
                    phrase.idPhrase = dialogData[@"audio"];
                    phrase.text = dialogData[@"text"];
                    phrase.audioFile = dialogData[@"audio"];
                    phrase.page = dialogData[@"page"];
                    phrase.role = [rolesDict objectForKey:abaInterpretData[@"Role"]];
                    
                    [interpret.dialog addObject:phrase];
                }
            }
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, interpret);
    }
    else
    {
        block(error, nil);
    }
}

@end
