//
//  ViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "MenuViewController.h"
#import "AppDelegate.h"
#import "ResideMenuViewController.h"
#import "LevelUnitViewController.h"
#import "CertificatesViewController.h"
#import "PlansViewController.h"
#import "ProfileViewController.h"
#import "UserController.h"
#import "ABANavigationController.h"
#import "Utils.h"
#import "LevelViewController.h"
#import "Resources.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import "AppDelegate.h"
#import "ABAAlert.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageHelper.h"
#import "LegacyTrackingManager.h"
#import <MessageUI/MessageUI.h>
#import "ABAAlert.h"
#import "HelpContactViewController.h"
#import "ABA-Swift.h"
@import SVProgressHUD;

#define kMenuCellId @"MenuCellId"

extern

@interface MenuViewController ()<UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>{

    NSArray *_dataSource;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titlePremium;
@property (weak, nonatomic) IBOutlet UILabel *subtitlePremium;
@property (weak, nonatomic) IBOutlet UIButton *buttonPremium;
@property (weak, nonatomic) IBOutlet UIView *premiumView;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userInfoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *usernameTopSpaceConstraint;

@property ABAAlert *abaAlert;

- (IBAction)logoutAction:(id)sender;

- (IBAction)premiumButtonAction:(id)sender;
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if(_abaAlert)
    {
        [_abaAlert dismiss];
    }
}

- (void)setupView{
    
    [self reloadMenu];
	
	if (IS_IPAD) { _usernameTopSpaceConstraint.constant = 22.0f; }
	
    // Make rounded button
    [_buttonPremium.layer setMasksToBounds:YES];
    [_buttonPremium.layer setCornerRadius:3.0f];
    
    if ([UserController isUserImage]) {
        
        _userImage.layer.cornerRadius = _userImage.frame.size.width/2;
        _userImage.contentMode = UIViewContentModeScaleAspectFill;

        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:UserController.currentUser.urlImage];
        NSURL *imageUrl = [NSURL URLWithString:urlString];
        
        if (imageUrl)
        {
            [_userImage sd_setImageWithURL:imageUrl];
        }
        else
        {
            [_userImage setImage:[UIImage imageNamed:@"blankAvatar"]];
        }
    }

	[_usernameLabel setFont:[UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?18:16]];
    
    NSString *name;
    if (UserController.currentUser.surnames == nil) {
        name = UserController.currentUser.name;
    }
    else {
        name = [NSString stringWithFormat:@"%@ %@", UserController.currentUser.name, UserController.currentUser.surnames];
    }
    
    _usernameLabel.text = name;
    [_userInfoLabel setFont:[UIFont fontWithName:ABA_RALEWAY_REGULAR size:IS_IPAD?16:14]];
    _userInfoLabel.text = STRTRAD(@"subtitleMenuNameKey", @"tu cuenta");
    
    
    // Localized strings for premium view
    _titlePremium.text = STRTRAD(@"titlePremiumKey", @"Pásate al plan premium");
	[_titlePremium setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:16]];

//    _subtitlePremium.text = STRTRAD(@"subtitlePremiumKey", @"30% descuento sólo 3 días");
    [_buttonPremium setTitle:STRTRAD(@"buttonPremiumKey", @"Ver precios") forState:UIControlStateDisabled];
    [_buttonPremium setTitle:STRTRAD(@"buttonPremiumKey", @"Ver precios") forState:UIControlStateNormal];
    [_buttonPremium setTitle:STRTRAD(@"buttonPremiumKey", @"Ver precios") forState:UIControlStateSelected];
	[_buttonPremium.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?17:15]];
    
    // single tap
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doSingleTap)];
    singleTap.numberOfTapsRequired = 1;
    [self.profileView addGestureRecognizer:singleTap];
	
    [self.tableView reloadData];
    
    [self checkAndLoadPlansDeepLink];
}

- (void)checkAndLoadPlansDeepLink
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kDeepLinkPlans])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDeepLinkPlans];
        [[NSUserDefaults standardUserDefaults] synchronize];

        if (![UserController isUserPremium])
        {
            [self premiumButtonAction:nil];
        }
    }
}

- (void)doSingleTap {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewProfileViewController"];
    
//#ifdef SHEPHERD_DEBUG
//    if (![ABAShepherdProfilePlugin isNewProfileView]) {
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//    }
//#endif

    ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
    [self.sideMenuViewController setContentViewController:nc animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

- (void)reloadMenu
{
    if ([UserController isUserPremium]) {
        
        // We fill datasource with menu section types
        
        _dataSource = @[[NSNumber numberWithUnsignedInteger:MenuUnits],
                        [NSNumber numberWithUnsignedInteger:MenuLevels],
                        [NSNumber numberWithUnsignedInteger:MenuCertificates],
                        [NSNumber numberWithUnsignedInteger:MenuHelp]];
        
        [_premiumView setHidden:YES];

    } else {

        // We fill datasource with menu section types
        
        _dataSource = @[[NSNumber numberWithUnsignedInteger:MenuUnits],
                        [NSNumber numberWithUnsignedInteger:MenuLevels],
                        [NSNumber numberWithUnsignedInteger:MenuCertificates],
                        [NSNumber numberWithUnsignedInteger:MenuPremium],
                        [NSNumber numberWithUnsignedInteger:MenuHelp]];

        [_premiumView setHidden:NO];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return IS_IPAD?60.0:44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMenuCellId];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kMenuCellId];
        [cell.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell.textLabel setFont:[UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?19:15]];
    }
    
    // We create localized strings

    MenuSection menuSection = [[_dataSource objectAtIndex:indexPath.item] unsignedIntegerValue];
    
    UIView *whiteLineSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, IS_IPAD?60.0:44.0 - 2, self.view.frame.size.width, 1)];
    [whiteLineSeparator setBackgroundColor:[UIColor whiteColor]];
    [whiteLineSeparator setAlpha:0.30];

    switch (menuSection) {
        case MenuUnits:{
            [cell.textLabel setText:[STRTRAD(@"unitsMenuItemKey", @"El curso") uppercaseString]];
            [cell.imageView setImage:[UIImage imageNamed:@"courseMenuIcon"]];

            [cell.contentView addSubview:whiteLineSeparator];

            break;
        }
        case MenuLevels:{
            [cell.textLabel setText:[STRTRAD(@"levelsMenuItemKey", @"Niveles") uppercaseString]];
            [cell.imageView setImage:[UIImage imageNamed:@"levelsMenuIcon"]];

            [cell.contentView addSubview:whiteLineSeparator];

            break;
        }
        case MenuCertificates:{
            [cell.textLabel setText:[STRTRAD(@"yourCertsMenuItemKey", @"Tus certificados") uppercaseString]];
            [cell.imageView setImage:[UIImage imageNamed:@"certificatesMenuIcon"]];

            [cell.contentView addSubview:whiteLineSeparator];
            
            break;
        }
        case MenuPremium:
            [cell.textLabel setText:[STRTRAD(@"programsMenuItemKey", @"Obtén acceso completo") uppercaseString]];
            [cell.imageView setImage:[UIImage imageNamed:@"premiumMenuIcon"]];

            [cell.contentView addSubview:whiteLineSeparator];

            break;
        case MenuHelp:{
            
            [cell.textLabel setText:[STRTRAD(@"helpMenuItemKey", @"Ayuda y contacto") uppercaseString]];
            [cell.imageView setImage:[UIImage imageNamed:@"helpMenuIcon"]];
            
            [cell.contentView addSubview:whiteLineSeparator];
            
            break;
        }
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    MenuSection menuSection = [[_dataSource objectAtIndex:indexPath.item] unsignedIntegerValue];
    
    switch (menuSection) {
        case MenuUnits:{
            
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
            ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
            [self.sideMenuViewController setContentViewController:nc animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
            ABARealmUser * user = [UserController currentUser];
            [CoolaDataTrackingManager trackEnteredCourseIndex:user.idUser levelId:user.currentLevel.idLevel];
            break;
        }
        case MenuLevels:{
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Start" bundle:[NSBundle mainBundle]];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"LevelViewController"];
            LevelViewController *levelVC = (LevelViewController *)vc;
            levelVC.presentedFromMenu = YES;
            [self.sideMenuViewController setContentViewController:levelVC animated:YES];
            [self.sideMenuViewController hideMenuViewController];

            break;
        }
        case MenuCertificates:{
            
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CertificatesViewController"];
            ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
            [self.sideMenuViewController setContentViewController:nc animated:YES];
            [self.sideMenuViewController hideMenuViewController];
  
            break;
        }
        case MenuPremium:{
            
            PlansViewController *vc = [PlansConfigurator createPlansViperStackWithType:kPlansViewControllerOriginMenu shouldShowBackButton:NO];
            ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
            [self.sideMenuViewController setContentViewController:nc animated:YES];
            [self.sideMenuViewController hideMenuViewController];

            break;
        }
        case MenuHelp:{
            
            UIViewController *vc = [[UIStoryboard storyboardWithName:@"Zendesk" bundle:nil] instantiateInitialViewController];
            ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
            [self.sideMenuViewController setContentViewController:nc animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
            ABARealmUser * user = [UserController currentUser];
            [CoolaDataTrackingManager trackOpenedHelp:user.idUser];
            
            break;
        }
        default:
            break;
    }
}

- (IBAction)logoutAction:(id)sender {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [UserController logoutWithBlock:^(NSError *error) {
        
         [SVProgressHUD dismiss];
         if(!error)
         {
             AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
             [appDelegate.presenter loadStartStoryboard];
         }
         else
         {
             ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"errorLogout", @"Error en hacer logout")];
             [abaAlert show];
         }
     }];
    
    AppDelegate * appDelegatec = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegatec resetAPNSConfiguration];
}

- (IBAction)premiumButtonAction:(id)sender {
    
    PlansViewController *vc = [PlansConfigurator createPlansViperStackWithType:kPlansViewControllerOriginBanner shouldShowBackButton:NO];
    ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
    [self.sideMenuViewController setContentViewController:nc animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        if(result == MFMailComposeResultSent) {
            _abaAlert = [[ABAAlert alloc] initAlertEmailWithText:STRTRAD(@"helpEmailOKKey", @"Hemos recibido tu mensaje, te contestaremos lo antes posible") withCorrect:YES];
            
            [_abaAlert showWithOffset:44];
        }
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	// We have to update the orientation of LevelUnitVC
	[[NSNotificationCenter defaultCenter] postNotificationName:kUpdateRotationFromMenu object:nil];
}

@end
