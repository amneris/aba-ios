//
//  RegisterViewInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol RegisterViewInput: class {

    func renderRegistrationConfirmation()

    func showLoadingIndicator()
    func hideLoadingIndicator()
    func renderValidationError(_ error: LoginParametersValidation)

    func showEmailRepeatedError()
    func showGenericError()
    func showFacebookError()
    func showPermissionError()
}
