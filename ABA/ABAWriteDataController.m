//
//  ABAWriteDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAWriteDataController.h"
#import <Realm/Realm.h>
#import "ABAUnit.h"
#import "ABAWrite.h"

@implementation ABAWriteDataController

+ (void)parseABAWriteSection:(NSArray *)abaWriteDataArray onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAWrite *write = unit.sectionWrite;
    
    [realm transactionWithBlock:^{
        
        if (!write)
        {
            write = [[ABAWrite alloc] init];
            unit.sectionWrite = write;
        }
        
        for (NSDictionary *abaWriteData in abaWriteDataArray)
        {
//            NSLog(@"PROCESSING: %@", abaWriteData);
            
            ABAWriteDialog *writeDialog = [[ABAWriteDialog alloc] init];
            
            writeDialog.role = [abaWriteData objectForKey:@"Role"];
            
            if ([abaWriteData objectForKey:@"dialog"] != nil && [[abaWriteData objectForKey:@"dialog"] isKindOfClass:[NSArray class]])
            {
                //Parse the dialog data
                for (NSDictionary *dialogData in [abaWriteData objectForKey:@"dialog"])
                {
                    ABAPhrase *phrase = [[ABAPhrase alloc] init];
                    phrase.idPhrase = [dialogData objectForKey:@"audio"];
                    phrase.audioFile = [dialogData objectForKey:@"audio"];
                    phrase.text = [dialogData objectForKey:@"text"];
                    phrase.page = [dialogData objectForKey:@"page"];
                    
                    [writeDialog.dialog addObject:phrase];
                }
            }
            [write.content addObject:writeDialog];
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, write);
    }
    else
    {
        block(error, nil);
    }
}

@end
