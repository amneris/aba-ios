//
//  ABAExercisesPhrase.h
//  ABA
//
//  Created by Marc Güell Segarra on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAPhrase.h"

@class ABAExercisesQuestion;

@interface ABAExercisesPhrase : ABAPhrase

@property NSString* blank;

- (ABAExercisesQuestion *)exercisesQuestion;

@end

RLM_ARRAY_TYPE(ABAExercisesPhrase)
