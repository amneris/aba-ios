//
// Created by Xabier Losada on 14/06/16.
// Copyright (c) 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoginViewInput <NSObject>

typedef NS_ENUM(NSInteger, LoginParametersValidation) {
    Success, // Validation succeeded
    EmptyName,
    EmptyEmail,
    WrongEmailFormat,
    EmptyPassword,
    WrongPasswordFormat
};

- (void)renderLoginConfirmation;
- (void)renderValidationError:(LoginParametersValidation)error;
- (void)showLoadingIndicator;
- (void)hideLoadingIndicator;

- (void)showEmailPasswordError;
- (void)showGenericError;
- (void)showFacebookError;
- (void)showLanguageError;
- (void)showPermissionError;

@end
