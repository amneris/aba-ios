//
//  NewProfileViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 13/12/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit
import BonMot
import Device

class NewProfileViewController: UIViewController {

    @IBOutlet weak var premiumBannerview: UIView!
    @IBOutlet weak var tableWrapperView: UIView!
    @IBOutlet weak var premiumActionLabel: UILabel!
    @IBOutlet weak var premiumActionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addMenuButton(
            withTitle: String.localize("menuKey", comment: ""),
            andSubtitle: String.localize("yourProfileKey", comment: "")
        )
        
        setupBannerView()
    }
    
    func setupBannerView() {
            
        let textStyle = StringStyle(
            .color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)),
            .font(UIFont(name: "Roboto-Bold", size: 16)!)
        )
        
        var attributedString: NSAttributedString
        if Device.size() == Size.screen4Inch {
            // in 4 inches screen we will ommit the crown since it takes a beautiful space in the screen and forces the other texts to be trimmed
            attributedString = String.localize("profile_premium_title", comment: "").uppercased().styled(with: textStyle)
        }
        else {
            let crownImage = #imageLiteral(resourceName: "crown")
            attributedString = NSAttributedString.composed(of: [ crownImage.styled(with: .baselineOffset(-3)), Tab.headIndent(12), String.localize("profile_premium_title", comment: "").uppercased().styled(with: textStyle) ])
        }
        
        if Device.size() == Size.screen4Inch {
            premiumActionButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        } else {
            premiumActionButton.contentEdgeInsets = UIEdgeInsetsMake(10, 17, 10, 17)
        }
        
        premiumActionButton.backgroundColor = UIColor(red: 255/255, green: 153/255, blue: 0/255, alpha: 1)
        premiumActionButton.setAttributedTitle(String.localize("profile_premium_button", comment: "").uppercased().styled(with: textStyle), for: .normal)
        premiumActionButton.layer.cornerRadius = 2.0
        premiumActionLabel.attributedText = attributedString
        
        premiumBannerview.isHidden =  UserController.isUserPremium()
    }
    
    @IBAction func goToPlansPage(_ sender: Any) {
        
        let plansController = PlansConfigurator.createPlansViperStack(type: .banner, shouldShowBackButton: true)
        navigationController?.pushViewController(plansController, animated: true)
    }
}
