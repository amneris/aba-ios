//
//  ABAViewTeacher.h
//  ABA
//
//  Created by Jordi Mele on 7/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABATeacherTipView;

@interface ABATeacherTipView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (id)initTeacherWithText:(NSString*)text
                withImage:(NSString*)image
               withShadow:(BOOL)shadow;

- (void)dismiss;

- (void)setAttributedText:(NSMutableAttributedString *)attributedText;

- (void)setConstraints;
- (void)setConstraintsAttributedText;

@end
