//
//  ChangePasswordViewController.m
//  ABA
//
//  Created by Jordi Mele on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import "ABAScrollView.h"
#import "Resources.h"
#import "SDVersion.h"
#import "ABAAlert.h"
#import "MDTUtils.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import "LegacyTrackingManager.h"
#import <Crashlytics/Crashlytics.h>
@import SVProgressHUD;

@interface ChangePasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet ABAScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UITextField *passwordOldTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTextView;
@property (weak, nonatomic) IBOutlet UITextField *passwordReNewTetField;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property ABAAlert *abaAlert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingField1Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *field1TopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingField1Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingField2Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingField2Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *field2TopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *field3TopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingField3Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingField3Constraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keylockIconTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *elementHeightConstraint;

- (IBAction)changePasswordButtonAction:(id)sender;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButtonNavBar];
    [self addTitleNavbar:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"changePasswordTitleKey", @"Cambiar contraseña")];
    
    
    if ([SDVersion deviceSize] == Screen3Dot5inch) {
        [_scrollView setScrollEnabled:NO];
    }
    else {
        [_scrollView setScrollEnabled:NO];
    }
    [self setupView];
    CLS_LOG(@"Change Password: Entering in change password screen");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if(_abaAlert)
    {
        [_abaAlert dismiss];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) setupView {
    
    _scrollView.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    
    _iconImage.layer.cornerRadius = _iconImage.frame.size.width/2;
    _iconImage.backgroundColor = ABA_COLOR_BLUE_MENU_UNIT;
    
    _passwordOldTextField.placeholder = STRTRAD(@"changePassword1Key", @"antigua contraseña");
    [self setupTextField:_passwordOldTextField];
    
    _passwordNewTextView.placeholder = STRTRAD(@"changePassword2Key", @"nueva contraseña");
    [self setupTextField:_passwordNewTextView];
    
    _passwordReNewTetField.placeholder = STRTRAD(@"changePassword3Key", @"repite la contraseña");
    [self setupTextField:_passwordReNewTetField];
    
    _changePasswordButton.backgroundColor = ABA_COLOR_BLUE;
    _changePasswordButton.layer.cornerRadius = 3.0;
    _changePasswordButton.layer.masksToBounds = YES;
    
    NSString *titleButton = [STRTRAD(@"profileChangePasswordKey", @"cambiar contraseña") uppercaseString];
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:titleButton];
    [mutAttStr addAttribute:NSFontAttributeName
					  value:[UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?18:14]
                      range:[titleButton rangeOfString:titleButton]];
    [mutAttStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:2.0]
                      range:NSMakeRange(0, [titleButton length])];
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:[titleButton rangeOfString:titleButton]];
    [_changePasswordButton setAttributedTitle:mutAttStr.copy forState:UIControlStateNormal];
	
	if(IS_IPAD) {
		_elementHeightConstraint.constant = _elementHeightConstraint.constant * 1.5;
		_leadingField1Constraint.constant = _leadingField1Constraint.constant * 4;
		_field1TopSpaceConstraint.constant = _field1TopSpaceConstraint.constant * 4;
		_trailingField1Constraint.constant = _trailingField1Constraint.constant * 4;
		_trailingField2Constraint.constant = _trailingField2Constraint.constant * 4;
		_leadingField2Constraint.constant = _leadingField2Constraint.constant * 4;
		_field2TopSpaceConstraint.constant = _field2TopSpaceConstraint.constant * 4;
		_field3TopSpaceConstraint.constant = _field3TopSpaceConstraint.constant * 4;
		_trailingField3Constraint.constant = _trailingField3Constraint.constant * 4;
		_leadingField3Constraint.constant = _leadingField3Constraint.constant * 4;
		_buttonTopSpaceConstraint.constant = _buttonTopSpaceConstraint.constant * 4;
		_buttonLeadingConstraint.constant = _buttonLeadingConstraint.constant * 4;
		_buttonTrailingConstraint.constant = _buttonTrailingConstraint.constant * 4;
		_keylockIconTopSpaceConstraint.constant = _keylockIconTopSpaceConstraint.constant * 3;
	}
}

-(void) setupTextField:(UITextField *)textField {
    
    [textField setBackgroundColor:[UIColor whiteColor]];
    
    textField.textColor = ABA_COLOR_REG_TEXTFIELD;
    textField.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16];
    
    [textField.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
    [textField.layer setBorderWidth:1];
    
    [textField.layer setCornerRadius:5.0f];
    textField.clipsToBounds = YES;
    
    //  Ponemos un pequeño margen interno en el textfield
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    [self correctTextField:textField];
}

-(void) correctTextField:(UITextField *)textField {
    
    [textField.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
    
}

-(void) errorTextField:(UITextField *)textField {
    
    [textField.layer setBorderColor:ABA_COLOR_RED_ERROR.CGColor];
    
}

-(BOOL)checkTextInput:(UITextField *)textField {
    
    switch (textField.tag) {
        case 100: {

            
            break;
        }
        case 101: {

            break;
        }
        case 102: {

            break;
        }
        default:
            break;
    }
    
    return YES;
}

- (IBAction)changePasswordButtonAction:(id)sender {
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [self.view endEditing:YES];
    
    __weak typeof(self) weakSelf = self;
        
    ABARealmUser *user = UserController.currentUser;
    [UserController checkUserPasswordWithEmail:user.email withPassword:_passwordOldTextField.text completionBlock:^(NSError *error, BOOL correct) {
        
        [SVProgressHUD dismiss];
        
        if(error) {
            if (error.code == CODE_CONNECTION_ERROR) {
                //Error de conexión
                ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:error.localizedDescription];
                [abaAlert show];
            }
            else {
                [weakSelf errorTextField:_passwordOldTextField];
                //Password incorrecto
                _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"changePassErrorOldPassword", @"la contraseña antigua no es correcta")];
                [_abaAlert showWithOffset:44];
            }
        }
        else {
            [weakSelf correctTextField:_passwordOldTextField];
            
            if(![MDTUtils isCorrectPassword:weakSelf.passwordNewTextView.text]) {
                [weakSelf errorTextField:weakSelf.passwordNewTextView];
                
                _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"regErrorNewPasswordMin", @"La nueva contraseña debe tener al menos 6 cifras")];
                [_abaAlert showWithOffset:44];
            }
            else {
                [weakSelf correctTextField:weakSelf.passwordNewTextView];
                
                if(![weakSelf.passwordNewTextView.text isEqualToString:weakSelf.passwordReNewTetField.text]) {
                    [weakSelf errorTextField:weakSelf.passwordReNewTetField];
                    
                    _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"changePassErrorEqualPassword", @"Ups, las constraseñan no coinciden")];
                    [_abaAlert showWithOffset:44];
                }
                else {
                    [weakSelf correctTextField:weakSelf.passwordReNewTetField];
                    // All fields correct : update password
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                    
                    [UserController updatePasswordUserWithToken:user.token withPassword:weakSelf.passwordReNewTetField.text completionBlock:^(NSError *error, id object) {
                        
                        [SVProgressHUD dismiss];
                        
                        if(!error) {
                            [weakSelf.navigationController popViewControllerAnimated:YES];
                        }
                        else {
                            
                            if (error.code == CODE_CONNECTION_ERROR) {
                                //Error de conexión
                                ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:error.localizedDescription];
                                [abaAlert show];
                            }else{

                                ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"errorUpdatePass", @"error en el update del password")];
                                [abaAlert show];
                            }
                        }
                        
                    }];
                    
                }
            }
            
        }
        
    }];
    
}


#pragma UITextFieldDelegate


-(void) textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    UITextField *nextTextField = (UITextField *)[self.view viewWithTag:(textField.tag + 1)];
    
    switch (textField.tag) {
        case 100: {
            [nextTextField becomeFirstResponder];
            break;
        }
        case 101: {
            [nextTextField becomeFirstResponder];
            break;
        }
        case 102: {
            [self changePasswordButtonAction:nil];
            break;
        }
        default:
            break;
    }
    
    return YES;
}

@end
