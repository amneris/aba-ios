//
//  ABARoscoController.m
//  ABA
//
//  Created by Oriol Vilaró on 14/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABARoscoController.h"

// common
#import "Resources.h"
#import "ABACustomSectionView.h"
#import "Utils.h"
#import "SectionProtocol.h"
#import "Definitions.h"
#import "Notifications.h"

// controllers, managers, etc
#import "ExperimentRunner.h"
#import "APIManager.h"
#import "SectionController.h"
#import "UserController.h"
#import "DownloadController.h"
#import "ProgressController.h"
#import "SectionStatusController.h"

// sections
#import "ABAFilm.h"
#import "ABASpeak.h"
#import "ABAWrite.h"
#import "ABAInterpret.h"
#import "ABAVideoClass.h"
#import "ABAExercises.h"
#import "ABAVocabulary.h"
#import "ABAEvaluation.h"

// third party
#import "DACircularProgressView.h"
#import "PureLayout.h"

@interface ABARoscoController () <ABACustomSectionViewDelegate>

// views
@property(strong, nonatomic) UIView *menuView;
@property(strong, nonatomic) UIView *centerBackgroundView;
@property(strong, nonatomic) DACircularProgressView *progressView;
@property(strong, nonatomic) UIView *centerView;
@property(strong, nonatomic) UILabel *titleCenterLabel;
@property(strong, nonatomic) UILabel *sectionCenterLabel;
@property(weak, nonatomic) UIView *containerView;

// sections views
@property ABACustomSectionView *sectionViewFilm;
@property ABACustomSectionView *sectionViewSpeak;
@property ABACustomSectionView *sectionViewWrite;
@property ABACustomSectionView *sectionViewInterpret;
@property ABACustomSectionView *sectionViewVideoClass;
@property ABACustomSectionView *sectionViewExercises;
@property ABACustomSectionView *sectionViewVocabulary;
@property ABACustomSectionView *sectionViewAssessment;

// current data
@property(nonatomic, strong) ABAUnit *unit;
@property float progressActual;

// controllers
@property(nonatomic, strong) SectionController *sectionController;
@property(nonatomic, strong) ProgressController *progressController;

// contraints
@property(strong, nonatomic) NSMutableArray *centerConstraints;

@end

@implementation ABARoscoController

#pragma mark - public methods

+ (ABARoscoController *)roscoControllerWithUnit:(ABAUnit *)unit containerView:(UIView *)containerView
{
    ABARoscoController *roscoController = [[ABARoscoController alloc] init];

    roscoController.unit = unit;
    roscoController.containerView = containerView;
    roscoController.sectionController = [[SectionController alloc] init];
    roscoController.progressController = [[ProgressController alloc] init];

    [roscoController createCentralViews];

    return roscoController;
}

- (void)loadSectionDataAnimated:(BOOL)animated
{
    [self loadSectionDataAnimated:animated withCompletionBlock:nil];
}

- (void)setupCenterView
{
    _centerBackgroundView.backgroundColor = ABA_COLOR_DARKGREY;
    _centerBackgroundView.layer.cornerRadius = _centerBackgroundView.frame.size.width / 2;

    _progressView.trackTintColor = ABA_COLOR_DARKGREY;
    _progressView.progressTintColor = [UIColor whiteColor];

    _centerView.backgroundColor = ABA_COLOR_BLUE_MENU_UNIT;
    _centerView.layer.cornerRadius = _centerView.frame.size.width / 2;
    
    _titleCenterLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_900 size:_centerView.frame.size.height*0.13f];
    _titleCenterLabel.textColor = ABA_COLOR_DARKBLUE;
    _titleCenterLabel.numberOfLines = 0;
    _titleCenterLabel.minimumScaleFactor = 0.5;
    _titleCenterLabel.textAlignment = NSTextAlignmentCenter;
    
    _sectionCenterLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:_sectionCenterLabel.frame.size.height * 0.5f];
    _sectionCenterLabel.textColor = [UIColor whiteColor];
    _sectionCenterLabel.numberOfLines = 1;
    _sectionCenterLabel.minimumScaleFactor = 0.5;
    _sectionCenterLabel.textAlignment = NSTextAlignmentCenter;
    
    [_sectionViewFilm updateViews];
    [_sectionViewSpeak updateViews];
    [_sectionViewWrite updateViews];
    [_sectionViewInterpret updateViews];
    [_sectionViewVideoClass updateViews];
    [_sectionViewExercises updateViews];
    [_sectionViewVocabulary updateViews];
    [_sectionViewAssessment updateViews];
}

- (void)udpateConstraintsIsLandscape:(BOOL)isLandscape shouldHideMenuView:(BOOL)shouldHideMenuView
{
    if (shouldHideMenuView)
    {
        [_menuView setAlpha:0.0f];
    }

    if (_centerConstraints == nil)
    {
        _centerConstraints = @[].mutableCopy;
    }

    if (_centerConstraints.count)
    {
        [_centerConstraints autoRemoveConstraints];
        [_centerConstraints removeAllObjects];
    }

    [_centerConstraints addObjectsFromArray:[_menuView autoCenterInSuperview]];
    [_centerConstraints addObject:[_menuView autoMatchDimension:ALDimensionWidth
                                                    toDimension:ALDimensionWidth
                                                         ofView:_containerView
                                                 withMultiplier:IS_IPAD ? isLandscape ? 0.6f : 0.8f : 1.0f]];
    [_centerConstraints addObject:[_menuView autoMatchDimension:ALDimensionWidth
                                                    toDimension:ALDimensionHeight
                                                         ofView:_menuView]];
    [_menuView layoutIfNeeded];
}

#pragma mark - private methods

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUnitViewProgressUpdatedNotification object:nil];
}

- (void)checkAndLoadSectionDeepLink
{
    NSString *sectionDeepLink = [[NSUserDefaults standardUserDefaults] objectForKey:kDeepLinkSection];
    if (sectionDeepLink)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDeepLinkSection];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *sectionNumber = [f numberFromString:sectionDeepLink];
        
        if (sectionNumber)
        {
            [self loadSection:(kABASectionTypes)[sectionNumber unsignedIntegerValue]];
        }
    }
}

#pragma mark - Central views methods

- (void)createCentralViews
{
    // menuView
    _menuView = [[UIView alloc] initForAutoLayout];
    [_menuView setBackgroundColor:[UIColor clearColor]];
    [_containerView addSubview:_menuView];
    BOOL isLandscape = NO;
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
    {
        isLandscape = YES;
    }
    [self udpateConstraintsIsLandscape:isLandscape shouldHideMenuView:YES];

    // centerBackgroundView
    _centerBackgroundView = [[UIView alloc] initForAutoLayout];
    [_menuView addSubview:_centerBackgroundView];
    [_centerBackgroundView autoCenterInSuperview];
    [_centerBackgroundView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_menuView withMultiplier:0.402f];
    [_centerBackgroundView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_menuView withMultiplier:0.402f];

    // progressView
    _progressView = [[DACircularProgressView alloc] initForAutoLayout];
    [_menuView addSubview:_progressView];
    [_progressView autoCenterInSuperview];
    [_progressView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_menuView withMultiplier:0.38f];
    [_progressView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_menuView withMultiplier:0.38f];

    // centerView
    _centerView = [[UIView alloc] initForAutoLayout];
    [_menuView addSubview:_centerView];
    [_centerView autoCenterInSuperview];
    [_centerView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_menuView withMultiplier:0.32f];
    [_centerView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_menuView withMultiplier:0.32f];

    // sectionCenterLabel
    _sectionCenterLabel = [[UILabel alloc] initForAutoLayout];
    [_centerView addSubview:_sectionCenterLabel];
    
    [_sectionCenterLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:_centerView withMultiplier:1.25f];
    
    [_sectionCenterLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:_centerView];
    [_sectionCenterLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_centerView withMultiplier:1 / 1.1f];
    [_sectionCenterLabel autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_centerView withMultiplier:1 / 4.5f];

    // titleCenterLabel
    _titleCenterLabel = [[UILabel alloc] initForAutoLayout];
    [_centerView addSubview:_titleCenterLabel];
    
    if ([_unit.completed boolValue] || (![UserController isUserPremium] && [_unit getIndex] != 1)) {
        [_titleCenterLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:_centerView];
    } else {
        [_titleCenterLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:_centerView withMultiplier:0.77f];
    }
    
    [_titleCenterLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:_centerView];
    [_titleCenterLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_centerView withMultiplier:1 / 1.05f];
    [_titleCenterLabel autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_centerView withMultiplier:1 / 3.0f];

    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [_centerView addGestureRecognizer:singleFingerTap];

    [_menuView layoutIfNeeded];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    kABASectionTypes currentSectionType = [ABAUnit getCurrentSectionForUnit:_unit];

    [self loadSection:currentSectionType];
}

- (void)updateCenterView
{
    kABASectionTypes currentSectionType = [ABAUnit getCurrentSectionForUnit:_unit];

    if ([UserController isUserPremium] || [_unit getIndex] == 1)
    {
        if ([_unit.completed boolValue])
        {
            _titleCenterLabel.text = [STRTRAD(@"unitMenuCenterPremComKey", @"¡HECHO!") uppercaseString];
            _sectionCenterLabel.hidden = YES;
            _progressActual = 1.0f;
        }
        else
        {
            _progressActual = [_unit.progress floatValue] / 100.0;

            if (currentSectionType == kABAFilmSectionType && _progressActual == 0)
            {
                _titleCenterLabel.text = [STRTRAD(@"unitMenuCenterPremEmKey", @"empieza") uppercaseString];
            }
            else
            {
                _titleCenterLabel.text = [STRTRAD(@"unitMenuCenterPremConKey", @"continua") uppercaseString];
            }
            [self updateCurrentSectionLabel];
        }
    }
    else
    {
        _titleCenterLabel.text = [STRTRAD(@"unitMenuCenterNoPremKey", @"desbloquea") uppercaseString];
        _sectionCenterLabel.hidden = YES;
        
        if ([_unit.sectionVideoClass.completed boolValue])
        {

            _progressActual = 0.125f;
        }
        else
        {
            _progressActual = 0.0f;
        }
    }
}

- (void)refreshProgressUnit
{
    NSLog(@"UNIT PROGRESS :%f", [_unit.progress floatValue] / 100.0);
    _progressActual = [_unit.progress floatValue] / 100.0;
    [_progressView setProgress:_progressActual animated:YES];
    [self updateCenterView];
    //    [self setTitleNavbarWithProgress:[_unit getPercentage]];

    [_delegate updateTitleWithProgress];
}

- (void)updateCurrentSectionLabel
{
    kABASectionTypes currentSectionType = [ABAUnit getCurrentSectionForUnit:_unit];

    switch (currentSectionType)
    {
    case kABAFilmSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle1Key", @"film") uppercaseString];
        break;
    }
    case kABASpeakSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle2Key", @"habla") uppercaseString];
        break;
    }
    case kABAWriteSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle3Key", @"escribe") uppercaseString];
        break;
    }
    case kABAInterpretSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle4Key", @"interpreta") uppercaseString];
        break;
    }
    case kABAVideoClassSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle5Key", @"videoclase") uppercaseString];
        break;
    }
    case kABAExercisesSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle6Key", @"ejercicios") uppercaseString];
        break;
    }
    case kABAVocabularySectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle7Key", @"vocabulario") uppercaseString];
        break;
    }
    case kABAAssessmentSectionType:
    {
        _sectionCenterLabel.text = [STRTRAD(@"unitMenuTitle8Key", @"evaluación") uppercaseString];
        break;
    }
    default:
        break;
    }
}

#pragma mark - sections view methods

- (void)createSections
{
    [UIView animateWithDuration:0.2
        animations:^{

            _sectionViewFilm.alpha = 0;
            _sectionViewSpeak.alpha = 0;
            _sectionViewWrite.alpha = 0;
            _sectionViewInterpret.alpha = 0;
            _sectionViewVideoClass.alpha = 0;
            _sectionViewExercises.alpha = 0;
            _sectionViewVocabulary.alpha = 0;
            _sectionViewAssessment.alpha = 0;
        }
        completion:^(BOOL finished) {

            [_sectionViewFilm removeFromSuperview];
            [_sectionViewSpeak removeFromSuperview];
            [_sectionViewWrite removeFromSuperview];
            [_sectionViewInterpret removeFromSuperview];
            [_sectionViewVideoClass removeFromSuperview];
            [_sectionViewExercises removeFromSuperview];
            [_sectionViewVocabulary removeFromSuperview];
            [_sectionViewAssessment removeFromSuperview];

            _sectionViewFilm = [[ABACustomSectionView alloc] initWithType:kABAFilmSectionType delegate:self view:_menuView];
            [_sectionViewFilm setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionFilm]];
            [_sectionViewFilm setAccessibilityLabel:@"abaFilmButton"];

            _sectionViewSpeak = [[ABACustomSectionView alloc] initWithType:kABASpeakSectionType delegate:self view:_menuView];
            [_sectionViewSpeak setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionSpeak]];
            [_sectionViewSpeak setAccessibilityLabel:@"speakButton"];

            _sectionViewWrite = [[ABACustomSectionView alloc] initWithType:kABAWriteSectionType delegate:self view:_menuView];
            [_sectionViewWrite setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionWrite]];
            [_sectionViewWrite setAccessibilityLabel:@"writeButton"];

            _sectionViewInterpret = [[ABACustomSectionView alloc] initWithType:kABAInterpretSectionType delegate:self view:_menuView];
            [_sectionViewInterpret setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionInterpret]];
            [_sectionViewInterpret setAccessibilityLabel:@"interpretButton"];

            _sectionViewVideoClass = [[ABACustomSectionView alloc] initWithType:kABAVideoClassSectionType delegate:self view:_menuView];
            [_sectionViewVideoClass setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionVideoClass]];
            [_sectionViewVideoClass setAccessibilityLabel:@"videoClassButton"];

            _sectionViewExercises = [[ABACustomSectionView alloc] initWithType:kABAExercisesSectionType delegate:self view:_menuView];
            [_sectionViewExercises setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionExercises]];
            [_sectionViewExercises setAccessibilityLabel:@"exercisesButton"];

            _sectionViewVocabulary = [[ABACustomSectionView alloc] initWithType:kABAVocabularySectionType delegate:self view:_menuView];
            [_sectionViewVocabulary setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionVocabulary]];
            [_sectionViewVocabulary setAccessibilityLabel:@"vocabularyButton"];

            _sectionViewAssessment = [[ABACustomSectionView alloc] initWithType:kABAAssessmentSectionType delegate:self view:_menuView];
            [_sectionViewAssessment setStyleOptions:[SectionStatusController getCreateStyleOptionsForSection:_unit.sectionEvaluation]];
            [_sectionViewAssessment setAccessibilityLabel:@"evaluationButton"];
        }];
}

- (void)updateSections
{
    [_sectionViewFilm setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionFilm]];
    [_sectionViewSpeak setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionSpeak]];
    [_sectionViewWrite setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionWrite]];
    [_sectionViewInterpret setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionInterpret]];
    [_sectionViewVideoClass setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionVideoClass]];
    [_sectionViewExercises setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionExercises]];
    [_sectionViewVocabulary setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionVocabulary]];
    [_sectionViewAssessment setStyleOptions:[SectionStatusController getUpdateStyleOptionsForSection:_unit.sectionEvaluation]];
}

- (void)loadSectionDataAnimated:(BOOL)animated withCompletionBlock:(SimpleBlock)completionBlock
{
    if (animated)
    {
        [_delegate showLoadingProgress];
    }

    __weak typeof(self) weakSelf = self;

    [_sectionController getAllSectionsForUnit:_unit
                                 contentBlock:^(NSError *error, ABAUnit *object) {

                                     if (!error)
                                     {
                                         [weakSelf.progressController getCompletedActionsFromUser:UserController.currentUser
                                                                                           onUnit:weakSelf.unit
                                                                                  completionBlock:^(NSError *error, id object) {
                                                                                      if (error)
                                                                                      {
                                                                                          NSLog(@"error: %@ %@", error, error.localizedDescription);
                                                                                          [_delegate hideLoadingProgress];
                                                                                      }
                                                                                      [weakSelf refresh:animated withCompletionBlock:completionBlock];
                                                                                  }];
                                     }
                                     else
                                     {
                                         NSLog(@"error: %@ %@", error, error.localizedDescription);
                                         [_delegate hideLoadingProgress];
                                         [weakSelf refresh:animated withCompletionBlock:completionBlock];
                                     }
                                 }];
}

- (void)refresh:(BOOL)animated withCompletionBlock:(SimpleBlock)completionBlock
{
    __weak typeof(self) weakSelf = self;

    dispatch_async(dispatch_get_main_queue(), ^{

        weakSelf.progressView.clockwiseProgress = 1;

        weakSelf.progressActual = [weakSelf.unit.progress floatValue] / 100.0;
        [weakSelf.progressView setProgress:weakSelf.progressActual animated:YES];
        [weakSelf.delegate updateTitleWithProgress];

        if (animated)
        {
            [weakSelf createSections];
        }
        else
        {
            [weakSelf updateSections];
        }

        [weakSelf.delegate hideLoadingProgress];

        [weakSelf setupCenterView];

        [UIView animateWithDuration:0.2f
            animations:^{

                [weakSelf.menuView setAlpha:1.0f];

            }
            completion:^(BOOL finished) {

                [weakSelf updateCenterView];

                [weakSelf checkAndLoadSectionDeepLink];

                if (completionBlock != nil)
                {
                    [weakSelf.delegate updateImages];
                    completionBlock();
                }
            }];
    });
};

#pragma ABACustomSectionViewDelegate

- (void)loadSection:(kABASectionTypes)type
{
    ABASection *section = [_unit getSectionForType:type];
    kABASectionActionStatus status = [SectionStatusController getStatusForSection:section];
    __weak typeof(self) weakSelf = self;
    
    switch (status)
    {
        case kABASectionActionStatusGranted:
        {
            if ([_unit shouldDownloadSectionData])
            {
                [self loadSectionDataAnimated:YES withCompletionBlock:^{

                              if ([_unit shouldDownloadSectionData])
                              {
                                  [weakSelf.delegate showConnectionAlert];
                              }
                              else
                              {
                                  [weakSelf.delegate didSelectSectionWithType:type];
                              }
                          }];
            }
            else
            {
                [_delegate didSelectSectionWithType:type];
            }
            
            break;
        }
        case kABASectionActionStatusOfflineWithoutData:
        {
            [_delegate showConnectionAlert];
            
            break;
        }
        case kABASectionActionStatusLockedReasonAssessment:
        {
            [_delegate showAssesmentLockAlert];
            
            break;
        }
        case kABASectionActionStatusLockedReasonUserNotPremium:
        {
            [_delegate showPlans];
            
            break;
        }
        case kABASectionActionStatusLockedReasonPreviusNotCompleted:
        {
            [_delegate showLockMessage];
            
            break;
        }
        default:
        {
            break;
        }
    }
}


@end
