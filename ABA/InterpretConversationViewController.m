//
//  InterpretConversationViewController.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "InterpretConversationViewController.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import "Resources.h"
#import "Definitions.h"
#import "ABAInterpret.h"
#import "ABAInterpretPhrase.h"
#import "ABAInterpretRole.h"
#import "ABAUnit.h"
#import "InterpretController.h"
#import "InterpretConversationCollectionViewCell.h"
#import "ABAInterpretLabel.h"
#import "ABAInterpretPauseView.h"
#import "AudioController.h"
#import "ABAAlert.h"
#import "InterpretViewController.h"
#import "ABAActionButtonView.h"
#import "ABAPulseViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageHelper.h"
#import "PhrasesController.h"
#import "LegacyTrackingManager.h"
#import "ABAControlView.h"
#import "PureLayout.h"
#import "ABA-Swift.h"
#import <Crashlytics/Crashlytics.h>

@interface InterpretConversationViewController ()  <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AudioControllerDelegate,  ABAInterpretPauseViewDelegate, ABAActionButtonViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (strong, nonatomic) ABAControlView *controlView;

@property InterpretController *interpretController;
@property AudioController *audioController;
@property UITapGestureRecognizer *pauseTapRecognizer;
@property UISwipeGestureRecognizer *hidePauseSwipeRecognizer;
@property ABAInterpretPauseView *pauseView;
@property ABAAlert *abaAlert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlViewHeightConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;

@property UIImageView *pauseButton;

@property UIView *timeRecordView;
@property UIView *timeRecordRedDot;
@property UILabel *timeRecordedLabel;
@property NSTimer *timeRecordedTimer;

@end

@implementation InterpretConversationViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        _interpretController = [[InterpretController alloc] init];
        
        [_collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
		
        _audioController = [[AudioController alloc] init];
        _audioController.audioControllerDelegate = self;
		
        _pauseTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePauseTap:)];
        [_pauseTapRecognizer setNumberOfTapsRequired:1];

        _hidePauseSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDownPauseView:)];
        [_hidePauseSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
        [_hidePauseSwipeRecognizer setNumberOfTouchesRequired:1];
    }
    return self;
}

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //
    // We load the background image
    //    
    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:_interpret.unit.filmImageInactiveUrl];
    NSURL *imageUrl = [NSURL URLWithString:urlString];
    
    [_backgroundImage sd_setImageWithURL:imageUrl];

    [self addBackButtonWithTitle:STRTRAD(@"interpretKey", @"Interpreta") andSubtitle:[_interpretController getPercentageForSection:_interpret]];
    
    [_pauseButton addGestureRecognizer:_pauseTapRecognizer];
    
    if(_type == kABAInterpretConversationTypeListenAll)
    {
		_controlView = [[ABAControlView alloc] initWithSectionType:kABAInterpretSectionType];
		[self.view addSubview:_controlView];
        [_controlView setHidden:YES];
		_controlView.actionButton.delegate = self;
		_audioController.pulseViewController = _controlView.actionButton.pulseViewController;

        _collectionViewHeightConstraint.constant = [UIScreen mainScreen].bounds.size.height - 44.0f;
        [_collectionView layoutIfNeeded];
    }
    else
    {
        [self setupPauseButton];
		
		_controlView = [[ABAControlView alloc] initWithSectionType:kABAInterpretSectionType];
		_controlView.audioController = _audioController;
		
		[self.view addSubview:_controlView];
		
		CGFloat controlHeight = MAX(self.view.frame.size.width, self.view.frame.size.height) * (IS_IPAD?0.17f:0.24f);
        CGFloat extraNavBarHeight = [[NSProcessInfo processInfo] operatingSystemVersion].majorVersion < 9 ? 0 : 44.0f;

		[_controlView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
		[_controlView autoSetDimension:ALDimensionHeight toSize:controlHeight];
		[_controlView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
		[_controlView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
		[_controlView setNeedsLayout];
		[_controlView setNeedsUpdateConstraints];
		[_controlView layoutIfNeeded];

        _collectionViewHeightConstraint.constant = self.view.frame.size.height - _controlView.frame.size.height - extraNavBarHeight;
        [_collectionView layoutIfNeeded];

		_controlView.actionButton.delegate = self;

		_audioController.pulseViewController = _controlView.actionButton.pulseViewController;

        [self setupControlsWithAutostart:NO];
    }
    
    [self updateContentInset];
    
    // Checking microphone status
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted)
     {
         if (!granted)
         {
             _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"noMicrophoneTextKey", @"Imposible grabar sonido :-(")];
             [_abaAlert showWithOffset:44 andDissmisAfter:2000]; // showing error 2000 seconds (trying to show the error permanently)
             self.controlView.alpha = 0.5;
             self.view.userInteractionEnabled = NO;
         }
     }];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationWillResignActiveNotification
                                               object: nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _audioController.audioControllerDelegate = nil;
    
    if ([[_audioController player] isPlaying])
    {
        [[_audioController player] stop];
    }
    
    if ([[_audioController recorder] isRecording])
    {
        [_audioController stopRecording];
    }
    
    if (_abaAlert)
    {
        [_abaAlert dismiss];
    }
    
    [_pauseButton removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self scrollToPhrase:_currentPhrase animated:YES];
    
    [[Crashlytics sharedInstance] setObjectValue:_currentPhrase.text forKey:@"Current phrase"];
    [[LegacyTrackingManager sharedManager] trackInteraction];
    
    if(_type == kABAInterpretConversationTypeListenAll)
    {
        [self setupControlsWithAutostart:YES];
    }
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"interpretsection"];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    dispatch_async(dispatch_get_main_queue(), ^{
					   [self.collectionView reloadData];
    });
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    _collectionViewHeightConstraint.constant =  self.view.frame.size.height - _controlView.frame.size.height;
    [_collectionView layoutIfNeeded];
    
    [self updateContentInset];
    
    [self scrollToPhrase:_currentPhrase animated:YES];
}

#pragma mark - Private methods

-(void)handleEnteredBackground
{
    if(![self.view.subviews containsObject:_pauseView]) {
        [self handlePauseTap:self];
    }
}

-(void)setupPauseButton
{
	_pauseButton = [UIImageView newAutoLayoutView];
	[self.navigationController.view addSubview:_pauseButton];

	[_pauseButton autoSetDimension:ALDimensionHeight toSize:30.0f];
	[_pauseButton autoSetDimension:ALDimensionWidth toSize:30.0f];
	[_pauseButton autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.navigationController.view withOffset:-10.0f];
	[_pauseButton autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.navigationController.view withOffset:7.0f];

    [_pauseButton setImage:[UIImage imageNamed:@"pauseCircleButton"]];
    _pauseButton.contentMode = UIViewContentModeScaleAspectFit;
    _pauseButton.userInteractionEnabled = YES;
    [_pauseButton addGestureRecognizer:_pauseTapRecognizer];
    
    [self.navigationController.view addSubview:_pauseButton];
}

-(void)listenButtonTapped
{
    _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
    
    [_controlView.actionButton removeButtonTapRecognizer];
    
    BOOL mustLoadRecordedFile = NO;
    
    if(_type == kABAInterpretConversationTypeListenAll &&
       [_currentRole isEqualToObject:_currentPhrase.role])
    {
        mustLoadRecordedFile = YES;
    }
	
    _audioType = kAudioControllerTypeAudio;
    
    [_audioController playAudioFile:_currentPhrase.audioFile withUnit: _interpret.unit forRec:mustLoadRecordedFile];
}

#pragma mark AudioController delegate methods

-(void) audioStarted
{
    if(_audioType == kAudioControllerTypeSoundEffectBeginRecord)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1*NSEC_PER_SEC), dispatch_get_main_queue(), ^
                       {
                           [self startRecordButtonTappedAfterSoundEffect];
                       });
    }
    else
    {
        [_controlView.actionButton startListeningAnimation];
    }
}

- (void)audioFinished
{
    if (_audioType != kAudioControllerTypeSoundEffectBeginRecord)
    {
        if (_type == kABAInterpretConversationTypeListenAll)
        {
            // If it's the last cell we have to go back
            if ([_interpret.dialog indexOfObject:_currentPhrase] == [_interpret.dialog count] - 1)
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [self next];
            }
        }
        else
        {
            [_controlView.actionButton stopListeningAnimation];

            //Marcamos la phrase como listened
            PhrasesController *phrasesController = [PhrasesController new];
            [phrasesController setPhrasesListened:@[ _currentPhrase ] forSection:_interpret sendProgressUpdate:NO completionBlock:^(NSError *error, id object){

            }];
            
            // Cooladata tracking
            [CoolaDataTrackingManager trackListenedToAudio:[UserController currentUser].idUser levelId:self.interpret.unit.level.idLevel unitId:self.interpret.unit.idUnitString sectionType:CooladataSectionTypeAbaInterpret exerciseId:_currentPhrase.idPhrase];

            if ([_interpret.dialog indexOfObject:_currentPhrase] == [_interpret.dialog count] - 1)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self.previousVC setSection:_interpret];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
            else
            {
                [self next];
            }
        }
    }
}

-(void) audioDidGetError:(NSError *)error
{
	[self audioFinished];
}

-(void)startRecordButtonTapped
{
    _audioType = kAudioControllerTypeSoundEffectBeginRecord;
    
    [_audioController playSound:@"beep.mp3"];
}

-(void)startRecordButtonTappedAfterSoundEffect
{
    // This will be executed when audioPlayer finishes playing previous sound effect
    [_controlView.actionButton removeButtonTapRecognizer];
    [_audioController recordAudioFile:_currentPhrase.audioFile withUnit: _interpret.unit];
}

-(void)stopRecordButtonTapped
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [_controlView.actionButton stopRecordingAnimation];
                       [_controlView hideRecordingTime];
                       [_controlView.actionButton removeButtonAnimations];
                       [_controlView.actionButton removeButtonTapRecognizer];
                       [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAInterpretSectionType];
                   });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1*NSEC_PER_SEC), dispatch_get_main_queue(), ^
                   {
                       [_audioController stopRecording];
                       _audioController.pulseViewController = nil;
                   });

}

#pragma mark AudioController delegate methods

-(void) recordingStarted
{
    [_controlView.actionButton switchToType:kABAActionButtonTypeRecord forSection:kABAInterpretSectionType];
    [_controlView showRecordingTime];
    [_controlView.actionButton startRecordingAnimation];
    [_controlView.actionButton addButtonTapRecognizer];
    
#ifdef SHEPHERD_DEBUG
    if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf stopRecordButtonTapped];
        });
    }
#endif
}

- (void)recordingFinished
{
    [_controlView hideRecordingTime];
    [_controlView.actionButton stopRecordingAnimation];

    _audioController.pulseViewController = nil;
    [_audioController.recorderPulseTimer invalidate];
    _audioController.recorderPulseTimer = nil;

    // Cooladata tracking
    [CoolaDataTrackingManager trackRecordedAudio:[UserController currentUser].idUser levelId:self.interpret.unit.level.idLevel unitId:self.interpret.unit.idUnitString sectionType:CooladataSectionTypeAbaInterpret exerciseId:_currentPhrase.idPhrase];

    [self setPhraseDoneAndNext];
}

-(void)setPhraseDoneAndNext
{
    __weak typeof(self) weakSelf = self;
    
    [_interpretController setPhraseDone:_currentPhrase
                    forInterpretSection:_interpret
                        withCurrentRole:[self getCurrentRoleForInterpret:weakSelf.interpret]
                        completionBlock:^(NSError *error, id object)
     {
         if(!error)
         {
             weakSelf.interpret = (ABAInterpret *)object;
             weakSelf.currentRole = [weakSelf getCurrentRoleForInterpret:weakSelf.interpret];
             
             if([weakSelf.currentRole.completed isEqualToNumber:[NSNumber numberWithInt:1]] &&
                [weakSelf.interpret.dialog indexOfObject:weakSelf.currentPhrase] == [weakSelf.interpret.dialog count] -1)
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^
                                {
                                    weakSelf.interpret = (ABAInterpret *)object;
                                    [weakSelf.previousVC setSection:(ABAInterpret *)object];
                                    [weakSelf.navigationController popViewControllerAnimated:YES];
                                });
             }
             else
             {
                 [weakSelf next];
                 [weakSelf addBackButtonWithTitle:STRTRAD(@"interpretKey", @"Interpreta") andSubtitle:[weakSelf.interpretController getPercentageForSection:weakSelf.interpret]];
             }
             NSLog(@"PROGRESS: %f - %f",
                   [weakSelf.interpretController getTotalElementsForSection:weakSelf.interpret],
                   [weakSelf.interpretController getElementsCompletedForSection:weakSelf.interpret]
                   );

         }
         else
         {
             [weakSelf showAlertWithText:STRTRAD(@"saveDataInterpretErrorKey",@"Imposible guardar el progreso :(")];
             [weakSelf.controlView.actionButton addButtonTapRecognizer];
         }
     }];
}

- (void)recordingDidGetError:(NSError *)error
{
	[self recordingFinished];
}


#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    NSInteger items = 0;
    
    if(view == _collectionView)
    {
        items = [_interpret.dialog count];
    }
    
    return items;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(cv == _collectionView)
    {
		NSString *cellId = [self getReuseIdentifierFromIndexPath:indexPath];
		
		InterpretConversationCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
		
		// I've just found a fucking UICollectionView & UITableView bug.
		// More info: http://stackoverflow.com/questions/19132908/auto-layout-constraints-issue-on-ios7-in-uitableviewcell
		cell.contentView.frame = cell.bounds;
		cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
		
		[cell setupWithABAInterpretPhrase:[_interpret.dialog objectAtIndex:indexPath.item]
							 withCellType:[self getObjectTypeFromIndexPath:indexPath]
								  andRole:_currentRole
						 andCurrentPhrase:_currentPhrase];
		
		[cell layoutSubviews];
		
		return cell;
    }
	
    return nil;
}

-(kABAInterpretRowType) getObjectTypeFromIndexPath:(NSIndexPath *)indexPath
{
    ABAInterpretPhrase *phrase = [_interpret.dialog objectAtIndex:indexPath.item];
    
    if([phrase.role isEqualToObject:_currentRole])
    {
        return kABAInterpretRecordRowType;
    }
    else
    {
        return kABAInterpretListenRowType;
    }
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    CGSize size = CGSizeMake(self.view.bounds.size.width, [self getCellHeightFromIndexPath: indexPath]);

    return size;
}

- (void)scrollToPhrase:  (ABAInterpretPhrase *)phrase
            animated: (BOOL)animated
{
    NSUInteger index;
    
    if(phrase != nil)
    {
       index = [_interpret.dialog indexOfObject:phrase];
    }
    else
    {
        index = [_interpret.dialog count] - 1;
    }
	
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]
                                               atScrollPosition:UICollectionViewScrollPositionBottom animated:animated];
                   });
}

- (void)updateContentInset
{
	CGFloat contentInsetTop = _collectionViewHeightConstraint.constant;
	
    NSIndexPath *indexPath = [self getIndexPathFromCurrentPhrase];
    
    for (int i=0;i<=indexPath.item;i++)
    {
        contentInsetTop -= [self getCellHeightFromIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
		
		if(contentInsetTop < 0)
		{
			contentInsetTop = 10.0f;
		}
    }
    
	_collectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0);
}

-(NSIndexPath *)getIndexPathFromCurrentPhrase
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:[_interpret.dialog indexOfObject:_currentPhrase] inSection:0];
    
    return indexPath;
}

-(NSString *)getReuseIdentifierFromIndexPath: (NSIndexPath *)indexPath
{
    switch([self getObjectTypeFromIndexPath:indexPath])
    {
        case kABAInterpretListenRowType:
        {
			return IS_IPAD?@"ListenInterpretCellId_iPad":@"ListenInterpretCellId";
            break;
        }
        case kABAInterpretRecordRowType:
        {
            return IS_IPAD?@"RecordInterpretCellId_iPad":@"RecordInterpretCellId";
            break;
        }
    }
}

-(CGFloat)getCellHeightFromIndexPath: (NSIndexPath *)indexPath
{
    CGFloat labelHeight = [self getLabelHeightFromIndexPath:indexPath];
    
	CGFloat defaultCellHeight = IS_IPAD?180.0f:120.0f;
    CGFloat topLabelSeparator = 18.0f;
    CGFloat bottomLabelSeparator = 32.0f;

    if(labelHeight > (defaultCellHeight - (topLabelSeparator + bottomLabelSeparator)))
    {
        return labelHeight + (bottomLabelSeparator + topLabelSeparator);
    }
    else
    {
        return defaultCellHeight;
    }
}

-(CGFloat)getLabelHeightFromIndexPath: (NSIndexPath *)indexPath
{
    ABAInterpretPhrase *phrase = [_interpret.dialog objectAtIndex:indexPath.item];
    
    CGFloat widthForLabel = self.view.frame.size.width - 100;
    
    CGRect labelFrame = CGRectMake(0, 0, widthForLabel, 40);
    
    ABAInterpretLabel *label = [[ABAInterpretLabel alloc] initWithFrame:labelFrame];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    label.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    label.autoresizesSubviews = YES;
    
    kABAInterpretRowStatus status;
    
    if([phrase isEqualToObject:_currentPhrase])
    {
        status = kABAInterpretRowStatusCurrent;
    }
    else
    {
        status = kABAInterpretRowStatusNormal;
    }
    
    [label setupWithABAInterpretPhrase:phrase andStatus:status];
    
    [label sizeToFit];
    
    return label.frame.size.height;
}

-(ABAInterpretRole *)getCurrentRoleForInterpret: (ABAInterpret *)interpret
{
    
    for(ABAInterpretRole *role in interpret.roles)
    {
        if([role isEqualToObject:_currentRole])
        {
            return role;
        }
    }
    
    return nil;
}


-(void)showAlertWithText: (NSString *)text
{
    if (_abaAlert)
    {
        [_abaAlert dismiss];
    }
    _abaAlert = [[ABAAlert alloc] initAlertWithText:text];
    [_abaAlert showWithOffset:44];
}


- (IBAction)handlePauseTap:(id)sender
{
    [_pauseButton removeGestureRecognizer:_pauseTapRecognizer];
    
    _audioController.audioControllerDelegate = nil;
    _audioController.pulseViewController = nil;
    
	if([[_audioController player] isPlaying])
	{
		[[_audioController player] stop];
	}
	
	if([[_audioController recorder] isRecording])
	{
		[_audioController stopRecording];
		[_controlView.actionButton stopRecordingAnimation];
        [_controlView hideRecordingTime];
	}
	
	[_controlView.actionButton removeButtonAnimations];
    [self showPauseView];
}

- (void)handleSwipeDownPauseView:(UISwipeGestureRecognizer *)swipe
{
    [self continuePauseViewAction];
}

-(void)continuePauseViewAction
{
    _audioController.audioControllerDelegate = self;
    
    __weak typeof (self) weakSelf = self;
    [self hidePauseViewWithCompletionBlock:^
     {
		 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
			 weakSelf.audioController.pulseViewController = weakSelf.controlView.actionButton.pulseViewController;
			 [weakSelf scrollToPhrase:weakSelf.currentPhrase animated:YES];
			 [weakSelf setupControlsWithAutostart:NO];
			 [weakSelf.controlView.actionButton addButtonTapRecognizer];
		 });
     }];
}

-(void)restartPauseViewAction
{
    _audioController.audioControllerDelegate = self;
    
    __weak typeof (self) weakSelf = self;
    [_interpretController eraseProgressForRole:_currentRole forInterpretSection:_interpret completionBlock:^(NSError *error, id object) {
         if(!error)
         {
             [weakSelf hidePauseViewWithCompletionBlock:^
              {
                  weakSelf.interpret = (ABAInterpret *)object;
                  weakSelf.currentRole = [weakSelf getCurrentRoleForInterpret:weakSelf.interpret];
                  weakSelf.currentPhrase = [weakSelf.interpretController getCurrentPhraseForRole:weakSelf.currentRole forInterpretSection:weakSelf.interpret];
				  
                  [weakSelf updateContentInset];

				  dispatch_async(dispatch_get_main_queue(), ^
								 {
									 [weakSelf.collectionView reloadData];
								 });
				  
                  // This is for smoothness
                  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                      weakSelf.audioController.pulseViewController = weakSelf.controlView.actionButton.pulseViewController;
                      [weakSelf scrollToPhrase:weakSelf.currentPhrase animated:YES];
                      [weakSelf setupControlsWithAutostart:NO];
					  [weakSelf.controlView.actionButton addButtonTapRecognizer];
                  });
              }];
         }
         else
         {
             [weakSelf showAlertWithText:STRTRAD(@"saveDataInterpretErrorKey",@"Imposible guardar el progreso :(")];
             weakSelf.audioController.pulseViewController = weakSelf.controlView.actionButton.pulseViewController;
         }
     }];
}

-(void)showPauseView
{
    CGRect initialFrame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 195.0f);
    CGRect finalFrame = CGRectMake(0, self.view.frame.size.height - 195.0f, self.view.frame.size.width, 195.0f);
    
    _pauseView = [[ABAInterpretPauseView alloc] initWithFrame:initialFrame];
    _pauseView.delegate = self;
    [_pauseView addGestureRecognizer:_hidePauseSwipeRecognizer];
    
    [self.view addSubview:_pauseView];
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^ {
                         [_pauseView setFrame:finalFrame];
                     } completion:NULL];
}

-(void)hidePauseViewWithCompletionBlock: (SimpleBlock)block
{
    CGRect finalFrame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 195.0f);
    
    __weak typeof(self) weakSelf = self;
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                         [_pauseView setFrame:finalFrame];
                     } completion:^(BOOL finished) {
                         [weakSelf.pauseView removeFromSuperview];
                         weakSelf.pauseView.delegate = nil;
                         [weakSelf.pauseButton addGestureRecognizer:weakSelf.pauseTapRecognizer];
                         [weakSelf.pauseView removeGestureRecognizer:weakSelf.hidePauseSwipeRecognizer];
                         
                         if(block != nil) {
                           block();
                         }
     }];
}

-(void)next
{
    NSUInteger nextIndex = [_interpret.dialog indexOfObject:_currentPhrase] + 1;
    _currentPhrase = [_interpret.dialog objectAtIndex:nextIndex];
    
    [[Crashlytics sharedInstance] setObjectValue:_currentPhrase.text forKey:@"Current phrase"];
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [self.collectionView reloadData];
                   });
    
    // This is for smoothness
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.8*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [self scrollToPhrase:_currentPhrase animated:YES];
    });
    
    // This is for smoothness
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.4*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [self setupControlsWithAutostart:YES];
    });

}

- (void)setupControlsWithAutostart: (BOOL)autostart
{
    switch(_type)
    {
        case kABAInterpretConversationTypeListenAll:
        {
            [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAInterpretSectionType];
            [self listenButtonTapped];

            break;
        }
        case kABAInterpretConversationTypeNormal:
        {
            // RECORD - your turn
            if([_currentRole isEqualToObject:_currentPhrase.role])
            {
                if(autostart)
                {
                    [self startRecordButtonTapped];
                }
                else
                {
                    [_controlView.actionButton switchToType:kABAActionButtonTypeRecord forSection:kABAInterpretSectionType];
					[_controlView.actionButton.actionLabel setText:[STRTRAD(@"startRecordingButtonKey", @"Grabar") uppercaseString]];
                }
            }
            // LISTEN - time to play
            else
            {
                [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAInterpretSectionType];
                
                if(autostart)
                {
                    [self listenButtonTapped];
                }
            }
            
            break;
        }
    }
}

@end
