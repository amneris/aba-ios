//
//  ABAPhrase.h
//  ABA
//
//  Created by Jordi Mele on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@class ABASpeak, ABASpeakDialog, ABAWriteDialog;

@interface ABAPhrase : RLMObject

@property NSString *audioFile;
@property NSNumber<RLMBool> *done;
@property NSString *idPhrase;
@property NSString *page;
@property NSString *text;
@property NSString *translation;
@property NSNumber<RLMBool> *listened;
@property NSDate *serverDate;

@end

RLM_ARRAY_TYPE(ABAPhrase)
