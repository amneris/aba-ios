//
//  InterpretCollectionViewCell.h
//  ABA
//
//  Created by Marc Güell Segarra on 2/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABAInterpretRole;

@protocol InterpretCollectionViewCellDelegate <NSObject>

- (void)retryRole: (ABAInterpretRole *)role;
- (void)listenRole: (ABAInterpretRole *)role;

@end

@interface InterpretCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id <InterpretCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *characterImage;
@property (weak, nonatomic) IBOutlet UILabel *chooseCharacterLabel;
@property (weak, nonatomic) IBOutlet UIImageView *doneCircle;
@property (weak, nonatomic) IBOutlet UILabel *retryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *retryButton;
@property (weak, nonatomic) IBOutlet UIImageView *listenButton;
@property (weak, nonatomic) IBOutlet UILabel *listenLabel;

-(void)setupWithABAInterpretRole: (ABAInterpretRole *)role;

@end
