//
//  ViewModel.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/03/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

struct UnitViewModel {
    let shouldShowPlansButton: Bool
    let shouldShowDownloadButton: Bool
    let shouldEnableDownloadButton: Bool
    let teacherText: String
    let sections: [SectionViewModel]?
}

struct SectionViewModel {
    let sectionType: kABASectionTypes
    let sectionStatus: kABASectionStyleOptions
    let sectionProgress: NSNumber?
}
