//
//  CooladataTrackingCommons.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/05/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

extension CoolaDataTrackingManager {

    @objc enum CooladataSectionType: Int {
        case abaFilm = 1
        case abaSpeak
        case abaWrite
        case abaInterpret
        case abaVideoClass
        case abaExercises
        case abaVocabulary
        case abaEvaluation
    }
    
    class func sectionTypeRepresentation(sectionType: CooladataSectionType) -> String {
        switch sectionType {
        case .abaFilm:
            return "1";
        case .abaSpeak:
            return "2";
        case .abaWrite:
            return "3";
        case .abaInterpret:
            return "4";
        case .abaVideoClass:
            return "5";
        case .abaExercises:
            return "6";
        case .abaVocabulary:
            return "7";
        case .abaEvaluation:
            return "8";
        }
    }
    
    class func createBasicStudyMap() -> [String: AnyObject] {
        let map = createBasicCooladataMap()
        // Add more properties to map type
        return map;
    }

    class func createBasicProfileMap() -> [String: AnyObject] {
        let map = createBasicCooladataMap()
        // Add more properties to map type
        return map;
    }

    class func createBasicNavigationMap() -> [String: AnyObject] {
        let map = createBasicCooladataMap()
        // Add more properties to map type
        return map;
    }

    class func createBasicOtherMap() -> [String: AnyObject] {
        let map = createBasicCooladataMap()
        // Add more properties to map type
        return map;
    }

    // MARK: Private methods

    fileprivate static let kIOSSite = "2"
    fileprivate class func createBasicCooladataMap() -> [String: AnyObject] {
        var mapWithBasicProperties = [String: AnyObject]()
        mapWithBasicProperties["event_timestamp_epoch"] = NSNumber(value: UInt64(Date().timeIntervalSince1970 * 1000) as UInt64) // with miliseconds
        mapWithBasicProperties["idSite"] = kIOSSite as AnyObject?
        return mapWithBasicProperties
    }
    
    
}
