//
//  RegisterFormViewController.swift
//  ABA
//
//  Created by MBP13 Jesus on 19/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
import SnapKit
import SkyFloatingLabelTextField
import Device
import SVProgressHUD

@objc
class RegisterFormViewController: UIViewController {
    fileprivate var backgroundImage: UIImage {
        get {
            if (self.traitCollection.horizontalSizeClass == .compact) {
                return UIImage(named: "register_page_landscape")!
            } else {
                return UIImage(named: "register_page")!
            }
        }
    }

    fileprivate let backgroundImageView = UIImageView()
    fileprivate let inputName = SkyFloatingLabelTextField()
    fileprivate let inputEmail = SkyFloatingLabelTextField()
    fileprivate let inputPassword = SkyFloatingLabelTextField()
    fileprivate let buttonRegister = UIButton()
    fileprivate let buttonRegisterFacebook = UIButton()
    fileprivate let buttonLogin = UIButton(type: .custom)
    fileprivate let imageViewAbaLogo = UIImageView(image: UIImage(named: "aba-logo"))
    fileprivate let buttonShowHidePassword = UIButton(type: .custom)

    // RX
    let disposableBag = DisposeBag()

    // VIPER
    var presenter: RegisterViewOutput?

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) will not been implemented")
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if Device.type() == .iPad {
            return .all
        }
        else {
            return .portrait
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        setupConstraints()

        let nameStream = inputName
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.inputName.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)

        let emailStream = inputEmail
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.inputEmail.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)

        let passwordStream = inputPassword
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.inputPassword.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)

        Observable
            .combineLatest(nameStream, emailStream, passwordStream) { [unowned self] (name, email, password) -> Bool in
                if let presenter = self.presenter {
//                    if let safeName = name, safeName.characters.count > 0 {
//                        presenter.validateName(safeName)
//                    }
//                    if let safeEmail = email, safeEmail.characters.count > 0 {
//                        presenter.validateEmail(safeEmail)
//                    }
//                    if let safePass = password, safePass.characters.count > 0 {
//                        presenter.validatePassword(safePass)
//                    }
                    return presenter.isNameValid(name) && presenter.isEmailValid(email) && presenter.isPasswordValid(password)
                } else {
                    return false
                }

        }.subscribe(onNext: { [unowned self] formIsComplete in
            self.updateRegisterButtonStyle(formIsComplete)
            
        }).addDisposableTo(disposableBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CLSLogv("Registratino form will appear", getVaList([]))
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        backgroundImageView.image = backgroundImage
    }

}

extension RegisterFormViewController: RegisterViewInput {
    
    func renderRegistrationConfirmation() {
        // Does nothing
    }
    
    func showLoadingIndicator() {
        showAbaLogo()
        SVProgressHUD.show(with: .black)
    }

    func hideLoadingIndicator() {
        SVProgressHUD.dismiss()
    }

    func renderValidationError(_ error: LoginParametersValidation) {
        switch error {
        case .EmptyName:
            self.inputName.errorMessage = String.localize("regErrorNameNil", comment: "")
        case .EmptyEmail:
            self.inputEmail.errorMessage = String.localize("regErrorEmailFormat2", comment: "")
        case .WrongEmailFormat:
            self.inputEmail.errorMessage = String.localize("regErrorEmailFormat2", comment: "")
        case .EmptyPassword:
            self.inputPassword.errorMessage = String.localize("regErrorPasswordMin", comment: "")
        case .WrongPasswordFormat:
            self.inputPassword.errorMessage = String.localize("regErrorPasswordMin", comment: "")
        default:
            print("renderValidationError: default")
        }
    }

    func showEmailRepeatedError() {
        let emailRepeatedAlert = UIAlertController(title: nil, message: String.localize("regErrorEmailExist", comment: ""), preferredStyle: .alert)
        emailRepeatedAlert.addAction(UIAlertAction(title: String.localize("regButtonForgot", comment: ""), style: .default, handler: { _ in self.presenter?.goToForgotPassword() }))
        emailRepeatedAlert.addAction(UIAlertAction(title: String.localize("funnelABtestLoginJoinTextButton", comment: ""), style: .default, handler: { _ in self.presenter?.goToLogin() }))
        emailRepeatedAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(emailRepeatedAlert, animated: true, completion: nil)
    }

    func showGenericError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("errorConnection", comment: ""))
        abaAlert?.show()
    }

    func showFacebookError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("errorRegisterFacebook", comment: ""))
        abaAlert?.show()
    }
    
    func showPermissionError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("regErrorFacebookEmailKey", comment: ""))
        abaAlert?.show()
    }
}

// Field validation
extension RegisterFormViewController {
    func updateRegisterButtonStyle(_ isActive: Bool) {
        if isActive {
            buttonRegister.setTitleColor(UIColor.white, for: UIControlState())
            buttonRegister.backgroundColor = RegisterFunnelConstants.colorAquaBlue
        } else {
            buttonRegister.setTitleColor(RegisterFunnelConstants.colorRegisterGrayText, for: UIControlState())
            buttonRegister.backgroundColor = RegisterFunnelConstants.colorRegisterGrayBackground
        }
    }
}

// View configuration and constrains

extension RegisterFormViewController {

    func configureViews() {

        // View heirarchy

        view.addSubview(backgroundImageView)
        view.addSubview(imageViewAbaLogo)
        view.addSubview(inputName)
        view.addSubview(inputEmail)
        view.addSubview(inputPassword)
        view.addSubview(buttonShowHidePassword)
        view.addSubview(buttonRegister)
        view.addSubview(buttonRegisterFacebook)
        view.addSubview(buttonLogin)

        // Style
        backgroundImageView.image = backgroundImage
        backgroundImageView.contentMode = .scaleAspectFill
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showAbaLogo)))

        inputName.placeholder = String.localize("funnelABtestRegisterFieldName", comment: "")
        inputName.title = String.localize("funnelABtestRegisterFieldName", comment: "")
        inputName.font = RegisterFunnelConstants.formFieldsFont
        inputName.tintColor = RegisterFunnelConstants.colorAquaBlue
        inputName.textColor = UIColor.white
        inputName.lineColor = RegisterFunnelConstants.colorFormGray
        inputName.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        inputName.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        inputName.autocorrectionType = .no
        inputName.clearButtonMode = .whileEditing
        inputName.keyboardType = UIKeyboardType.asciiCapable
        inputName.delegate = self
        inputName.returnKeyType = .next

        var clearButton = inputName.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white

        inputEmail.placeholder = String.localize("funnelABtestRegisterFieldEmail", comment: "")
        inputEmail.title = String.localize("funnelABtestRegisterFieldEmail", comment: "")
        inputEmail.font = RegisterFunnelConstants.formFieldsFont
        inputEmail.tintColor = RegisterFunnelConstants.colorAquaBlue
        inputEmail.textColor = UIColor.white
        
        inputEmail.lineColor = RegisterFunnelConstants.colorFormGray
        inputEmail.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        inputEmail.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        inputEmail.clearButtonMode = .whileEditing
        inputEmail.autocorrectionType = .no
        inputEmail.autocapitalizationType = .none
        inputEmail.keyboardType = UIKeyboardType.emailAddress
        inputEmail.delegate = self
        inputEmail.returnKeyType = .next

        clearButton = inputEmail.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white

        inputPassword.placeholder = String.localize("register_funnel_password_field", comment: "")
        inputPassword.title = String.localize("funnelABtestRegisterFieldPassword", comment: "")
        inputPassword.font = RegisterFunnelConstants.formFieldsFont
        inputPassword.tintColor = RegisterFunnelConstants.colorAquaBlue
        inputPassword.textColor = UIColor.white
        inputPassword.lineColor = RegisterFunnelConstants.colorFormGray
        inputPassword.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        inputPassword.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        inputPassword.autocorrectionType = .no
        inputPassword.keyboardType = UIKeyboardType.default
        inputPassword.isSecureTextEntry = true
        inputPassword.delegate = self
        inputPassword.returnKeyType = .join

        buttonShowHidePassword.setImage(UIImage(named: "show_password"), for: UIControlState())
        buttonShowHidePassword.addTarget(self, action: #selector(showHidePasswordAction), for: .touchUpInside)

        buttonRegister.setTitle(String.localize("funnelABtestRegisterButton", comment: ""), for: UIControlState())
        buttonRegister.layer.cornerRadius = RegisterFunnelConstants.cornerRadius
        buttonRegister.titleLabel?.font = RegisterFunnelConstants.formButtonsFont
        buttonRegister.addTarget(self, action: #selector(validateFormAndRegister), for: .touchUpInside)
        self.updateRegisterButtonStyle(false)

        buttonRegisterFacebook.setTitle(String.localize("funnelABtestRegisterWithFacebookButton", comment: ""), for: UIControlState())
        buttonRegisterFacebook.setTitleColor(UIColor.white, for: UIControlState())
        buttonRegisterFacebook.layer.cornerRadius = RegisterFunnelConstants.cornerRadius
        buttonRegisterFacebook.backgroundColor = RegisterFunnelConstants.colorRegisterBlueBackground
        buttonRegisterFacebook.titleLabel?.font = RegisterFunnelConstants.formButtonsFont
        buttonRegisterFacebook.addTarget(self, action: #selector(facebookRegistrationAction), for: .touchUpInside)

        let alreadyAUserString = String.localize("funnelABtestLoginTextButton", comment: "")
        let loginString = String.localize("funnelABtestLoginJoinTextButton", comment: "")
        let fullLoginString = alreadyAUserString + " " + loginString
        let fullLoginNSString = fullLoginString as NSString
        let attributedString = NSMutableAttributedString(string: fullLoginString)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: RegisterFunnelConstants.colorAquaBlue, range: fullLoginNSString.range(of: loginString))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: fullLoginNSString.range(of: alreadyAUserString))
        attributedString.addAttribute(NSFontAttributeName, value: RegisterFunnelConstants.bottomButtonFont!, range: fullLoginNSString.range(of: fullLoginString))

        buttonLogin.setTitle("", for: UIControlState())
        buttonLogin.contentMode = .center
        buttonLogin.titleLabel?.font = RegisterFunnelConstants.bottomButtonFont
        buttonLogin.setTitleColor(UIColor.white, for: UIControlState())
        buttonLogin.setAttributedTitle(attributedString, for: UIControlState())
        buttonLogin.addTarget(self, action: #selector(goToLogin), for: .touchUpInside)
    }

    // Method to update the form constains so that the form is aligned to the top of the screen
    // using Autolayouts
    func adjustFormToTop() {
        self.inputName.snp.remakeConstraints { (make) in
            if RegisterFunnelConstants.isIpadOrIpadScreenSize() {
                make.width.equalTo(RegisterFunnelConstants.formWidthForIpad)
                make.centerX.equalTo(self.view)
            }
            else {
                make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
                make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
            }

            make.top.equalTo(self.view).offset(RegisterFunnelConstants.formTopPadding)
        }
    }

    func adjustFormToLogo() {
        inputName.snp.remakeConstraints { (make) in
            if RegisterFunnelConstants.isIpadOrIpadScreenSize() {
                make.width.equalTo(RegisterFunnelConstants.formWidthForIpad)
                make.centerX.equalTo(self.view)
            }
            else {
                make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
                make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
            }

            make.top.equalTo(imageViewAbaLogo.snp.bottomMargin).offset(RegisterFunnelConstants.formTopPadding)
        }
    }

    // Layout
    func setupConstraints() {

        backgroundImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }

        imageViewAbaLogo.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 110, height: 94))
            make.top.equalTo(self.view).offset(RegisterFunnelConstants.logoPadding)
            make.centerX.equalTo(self.view)
        }

        // inputName constaints
        adjustFormToLogo()

        inputEmail.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputName.snp.left)
            make.right.equalTo(self.inputName.snp.right)
            make.top.equalTo(self.inputName).offset(RegisterFunnelConstants.formFieldsPadding)
        }

        inputPassword.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputName.snp.left)
            make.right.equalTo(self.inputName.snp.right)
            make.top.equalTo(self.inputEmail).offset(RegisterFunnelConstants.formFieldsPadding)
        }

        buttonShowHidePassword.snp.makeConstraints { (make) in
            make.right.equalTo(inputPassword)
            make.centerY.equalTo(inputPassword)
            make.size.equalTo(CGSize(width: 30.0, height: 30.0))
        }

        buttonRegister.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputName.snp.left)
            make.right.equalTo(self.inputName.snp.right)
            make.top.equalTo(self.inputPassword.snp.bottomMargin).offset(RegisterFunnelConstants.formButtonsTopPadding)
            make.height.equalTo(55)
        }

        buttonRegisterFacebook.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputName.snp.left)
            make.right.equalTo(self.inputName.snp.right)
            make.top.equalTo(self.buttonRegister.snp.bottomMargin).offset(RegisterFunnelConstants.formButtonsSeparation)
            make.height.equalTo(55)
        }

        buttonLogin.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-10)
            make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
            make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
        }
    }
}

extension RegisterFormViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        // Hidding aba logo
        hideAbaLogo()
        
        // validating form in filled inputs
        if let safeName = inputName.text, safeName.characters.count > 0 {
            presenter?.validateName(safeName)
        }
        if let safeEmail = inputEmail.text, safeEmail.characters.count > 0 {
            presenter?.validateEmail(safeEmail)
        }
        if let safePass = inputPassword.text, safePass.characters.count > 0 {
            presenter?.validatePassword(safePass)
        }
        
        
        return true
    }

    // moving along the form fields
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == inputName {
            _ = inputEmail.becomeFirstResponder()
        }
        else if textField == inputEmail {
            _ = inputPassword.becomeFirstResponder()
        }
        else if textField == inputPassword {
            validateFormAndRegister()
            return true
        }

        return false

    }
}

// Private methods
extension RegisterFormViewController {

    func hideAbaLogo() {
        // Hiding aba logo
        UIView.animate(withDuration: 0.5,
            animations: {
                self.imageViewAbaLogo.alpha = 0.0
            },
            // When finished, moving form to the top
            completion: { completed in
                if completed {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.adjustFormToTop()
                        self.view.layoutIfNeeded()
                    })
                }
        })
    }

    func showAbaLogo() {
        dismissKeyboard()
        UIView.animate(withDuration: 0.2,
            animations: {
                self.adjustFormToLogo()
                self.view.layoutIfNeeded()
            },
            // When finished, moving form to the top
            completion: { completed in
                if completed {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.imageViewAbaLogo.alpha = 1.0
                    })
                }
        })
    }

    func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }

    func validateFormAndRegister() {
        self.presenter?.registerUser(name: inputName.text!, email: inputEmail.text!, password: inputPassword.text!)
    }

    func facebookRegistrationAction() {
        self.presenter?.registerUserWithFacebook()
    }

    func showHidePasswordAction() {
        if inputPassword.isSecureTextEntry {
            inputPassword.isSecureTextEntry = false
            inputPassword.font = nil
            inputPassword.font = RegisterFunnelConstants.formFieldsFont
            buttonShowHidePassword.setImage(UIImage(named: "hide_password"), for: UIControlState())
        } else {
            inputPassword.isSecureTextEntry = true
            inputPassword.font = nil
            inputPassword.font = RegisterFunnelConstants.formFieldsFont
            buttonShowHidePassword.setImage(UIImage(named: "show_password"), for: UIControlState())
        }
    }

    func goToLogin() {
        self.presenter?.goToLogin()
    }
}
