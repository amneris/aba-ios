//
//  CurrencyManager.m
//  ABA
//
//  Created by MBP13 Jesus on 10/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "CurrencyManager.h"
#import "CurrencyAPIManager.h"
#import "CurrencyCacheManager.h"
#import "CurrencyPairModel.h"

@implementation CurrencyManager

#pragma mark - Public methods

+ (void)updateCurrencies
{
    [CurrencyAPIManager loadCurrencyWithCurrencyCodeArray:[self currenciesCodeArray] andTargetCurrency:[self targetCurrency]];
}

+ (double)convertAmount:(double)amountToConverse toUSDollarFrom:(NSString *)currencyCode
{
    CurrencyPairModel * currencyModel = [self currencyPairWithCurrencyCode:currencyCode];
    if (currencyModel)
    {
        return [self roundValue: amountToConverse / [currencyModel.currencyRate doubleValue]];
    }
    
    return NSNotFound;
}

+ (double)convertAmount:(double)amountToConverse toEurFrom:(NSString *)currencyCode
{
    CurrencyPairModel * currencyModelCurrencyToUSD = [self currencyPairWithCurrencyCode:currencyCode];
    CurrencyPairModel * currencyModelEURtoUSD = [self currencyPairWithCurrencyCode:@"EUR"];
    
    if (currencyModelCurrencyToUSD && currencyModelEURtoUSD)
    {
        double amountInUsd = amountToConverse / [currencyModelCurrencyToUSD.currencyRate doubleValue];
        double eurToUsdRate = [currencyModelEURtoUSD.currencyRate doubleValue];
        
        return [self roundValue: amountInUsd * eurToUsdRate];
    }
    
    return NSNotFound;
}

+ (double) roundValue:(double)value
{
    double divisor = pow(10.0, 2);
    return round(value * divisor) / divisor;
}

#pragma mark - Private methods

/**
 It defines the array of currencies that Apple supports
 */
+ (NSArray *)currenciesCodeArray
{
    return @[@"USD",  // USA
             @"MXN",  // Mexico
             @"CAD",  // Canada
             @"GBP",  // U.K
             @"EUR",  // European Union
             @"SEK",  // Sweden
             @"DKK",  // Denmark
             @"NOK",  // Norway
             @"CHF",  // Switzerland
             @"AUD",  // Australia
             @"NZD",  // New Zealand
             @"JPY",  // Japan
             @"CNY",  // China
             @"HKD",  // Hong Kong
             @"SGD",  // Singapore
             @"TWD",  // Taiwan
             @"INR",  // India
             @"IDR",  // Indonesia
             @"ILS",  // Israel
             @"RUB",  // Russia
             @"SAR",  // Saudi Arabia
             @"ZAR",  // South Africa
             @"TRY",  // Turkey
             @"AED",  // Emirates
             @"THB",  // Bath tailandes
             @"EGP",  // Egyptian Pound
             @"MYR",  // Malaysian Ringgit
             @"NGN",  // Nigerian Naira
             @"PKR",  // Pakistani Rupee
             @"PHP",  // Philippine Peso
             @"QAR",  // Qatari Riyal
             @"TZS",  // Tanzanian Shilling
             @"VND"  // Vietnamese Dong
             ];
}

+ (NSString *)targetCurrency
{
    return @"USD";
}

#pragma mark - Private methods

+ (NSArray *)arrayOfCurrencyModels
{
    return [CurrencyCacheManager loadCurrenciesFromCache];
}

+ (CurrencyPairModel *)currencyPairWithCurrencyCode:(NSString *)currencyCode
{
    NSArray * arrayOfCurrencyModel = [self arrayOfCurrencyModels];
    
    // Searching using predicate
    NSArray * arrayWithSingleCurrencyModel = [arrayOfCurrencyModel filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"latterCurrencyCode like %@", currencyCode]];
    
    CurrencyPairModel * currencyPairModelFromSearch;
    if (arrayWithSingleCurrencyModel.count > 0)
    {
        currencyPairModelFromSearch = [arrayWithSingleCurrencyModel objectAtIndex:0];
    }
    
    return currencyPairModelFromSearch;
}

@end
