//
//  ABAFilmDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAFilmDataController.h"
#import <Realm/Realm.h>
#import "ABAUnit.h"
#import "ABAFilm.h"

@implementation ABAFilmDataController

+ (void)createABAFilmWithDictionary:(NSDictionary *)abaFilmData onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAFilm *film = unit.sectionFilm;
    
    [realm transactionWithBlock:^{
        
        if (!film)
        {
            film = [[ABAFilm alloc] init];
            unit.sectionFilm = film;
        }
        
        film.englishSubtitles = [abaFilmData objectForKey:@"English Subtitles"];
        film.hdVideoURL = [abaFilmData objectForKey:@"SD Video URL"];
        film.previewImageURL = [abaFilmData objectForKey:@"Preview Image URL"];
        film.sdVideoURL = [abaFilmData objectForKey:@"Mobile SD Video URL"];
        film.translatedSubtitles = [abaFilmData objectForKey:@"Translated Subtitles"];
        
    } error:&error];
    
    if (!error)
    {
        block(nil, film);
    }
    else
    {
        block(error, nil);
    }
}

@end
