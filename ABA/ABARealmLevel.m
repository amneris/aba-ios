//
//  ABARealmLevel.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABARealmLevel.h"
#import "ABARealmCert.h"
#import "ABAUnit.h"
#import "ABARealmUser.h"
#import <UIKit/UIKit.h>

@implementation ABARealmLevel

- (NSString *)getPercentage
{
    NSString *percentageString = [NSString stringWithFormat:@"%ld%%", (long)[self.progress floatValue]];
    return percentageString;
}

- (float)getPercentageCertificateMaxProgress:(float)maxProgress
{
    CGFloat progressInt = [self.progress floatValue];
    CGFloat percentage;

    if (progressInt > 0)
    {
        percentage = (maxProgress / 100.0) * progressInt;
    }
    else
    {
        percentage = 0.0f;
    }

    return percentage;
}

- (ABAUnit *)getCurrentUnit
{
    RLMSortDescriptor *sortUnits = [RLMSortDescriptor sortDescriptorWithProperty:@"idUnit" ascending:YES];
    RLMResults *sortedUnits = [self.units sortedResultsUsingDescriptors:@[ sortUnits ]];

    for (ABAUnit *unit in sortedUnits)
    {
        if (![unit.completed boolValue])
        {
            return unit;
        }
    }
    return [self.units lastObject];
}

- (ABARealmCert *)cert
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"level == %@", self];
    RLMResults *results = [ABARealmCert objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}

@end
