//
//  InbentaSupportViewController.swift
//
//
//  Created by MBP13 Jesus on 10/02/17.
//
//

import Foundation
import UIKit
import SnapKit
import Device

@objc
class InbentaSupportViewController: UIViewController {
    
    var webView: UIWebView!
    var okButton: UIButton!
    var errorLabel: UILabel!
    var errorView: UIView!
    
    let abaDarkGray = UIColor(red: 67/255, green: 82/255, blue: 92/255, alpha: 1)
    
    let INBENTA_BASE_URL = "http://abaenglish-app-faqs.inbenta.com/"
    let language = ["fr","en","es","ru","pt","it"]
    let defaultLanguage = "en"
    
    var bottomConstraint: Constraint? = nil
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = UIColor.white
        self.addBackButton(withTitleBlue: String.localize("help_center_main_btn_knowledge_base", comment: "Centro de Ayuda"))
        
        webView = UIWebView()
        self.view.addSubview(webView);
        
        okButton = UIButton(type: UIButtonType.custom)
        okButton.setTitle("OK", for: UIControlState.normal)
        okButton.setTitleColor(RegisterFunnelConstants.colorAquaBlue, for: UIControlState.normal)
        okButton.addTarget(self, action: #selector(InbentaSupportViewController.okButtonAction), for: UIControlEvents.touchUpInside)
        
        errorLabel = UILabel()
        errorLabel.text = String.localize("getAllSectionsForUnitErrorKey", comment: "No connection")
        errorLabel.font = RegisterFunnelConstants.bottomButtonFont
        errorLabel.textColor = UIColor.white
        errorLabel.numberOfLines = 5
        
        errorView = UIView()
        errorView.addSubview(errorLabel)
        errorView.addSubview(okButton)
        errorView.layer.cornerRadius = 3.0
        errorView.clipsToBounds = true
        self.view.addSubview(errorView)
        
        okButton.backgroundColor = abaDarkGray
        errorLabel.backgroundColor = abaDarkGray
        errorView.backgroundColor = abaDarkGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setConstraints()
        
        let fullUrl = URL(string: constructURLString())
        if let safeurl = fullUrl {
            webView.loadRequest(URLRequest(url: safeurl))
            webView.delegate = self
        } else {
            showErrorMessage()
        }
    }
}

extension InbentaSupportViewController: UIWebViewDelegate {
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        webView.isHidden = true
        showErrorMessage()
    }
}

extension InbentaSupportViewController {
    
    func constructURLString() -> String {
        
        // get the email and the language of the user for Inbenta
        let email = UserController.currentUser().email
        let deviceVersion = UIDevice.current.systemVersion
        let deviceName = Device.version().rawValue
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"]
        
        var lang = UserController.currentUser().userLang
        if (lang == nil || !language.contains(lang!)) {
            lang = defaultLanguage
        }
        
        let urlBuilder = NSMutableString(string: INBENTA_BASE_URL)
        urlBuilder.append("\(lang!)/?") // Adding language
        
        // Adding email
        if let safeemail = email {
            urlBuilder.append("&user=\(safeemail)")
        }
        
        // Adding device version
        urlBuilder.append("&deviceVersion=\(deviceVersion)")
        
        // Adding device name
        urlBuilder.append("&deviceInfo=\(deviceName)")
        
        // Adding app version
        if let safeVersion = appVersion {
            urlBuilder.append("&appVersion=\(safeVersion)")
        }
        
        return String(urlBuilder)
    }
    
    func setConstraints() {
        
        webView.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(self.view)
            make.center.equalTo(self.view)
        }
        
        okButton.snp.makeConstraints{ (make) -> Void in
            make.width.height.equalTo(50)
            make.centerY.equalTo(errorView)
            make.right.equalTo(errorView).offset(-10)
        }
        
        errorLabel.snp.makeConstraints{ (make) -> Void in
            make.left.equalTo(errorView).offset(10)
            make.right.equalTo(okButton.snp.left).offset(-10)
            make.centerY.equalTo(errorView)
        }
        
        errorView.snp.makeConstraints{ (make) -> Void in
            make.height.equalTo(errorLabel.snp.height).offset(20)
            make.centerX.equalTo(self.view)
            self.bottomConstraint = make.bottom.equalTo(self.view.snp.bottom).offset(200).constraint
            
            if (Device.isPad()) {
                let w: Double = Double(UIScreen.main.bounds.width)
                let h: Double = Double(UIScreen.main.bounds.height)
                make.width.lessThanOrEqualTo(max(w, h) / 2)
            } else {
                make.width.equalTo(self.view).offset(-10)
            }
        }
    }
    
    func showErrorMessage() {
        
        self.errorView.snp.updateConstraints() { (make) in
            self.bottomConstraint?.update(offset: -15)
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.bottomConstraint?.update(offset: -5)
            UIView.animate(withDuration: 0.1, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
    }
    
    func okButtonAction() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
}
