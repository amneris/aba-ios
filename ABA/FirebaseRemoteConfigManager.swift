//
//  FirebaseRemoteConfigManager.swift
//  ABA
//
//  Created by MBP13 Jesus on 02/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

@objc
class FirebaseRemoteConfigManager : NSObject
{
    internal let remoteConfig:FIRRemoteConfig
    internal let REMOTE_CONFIG_EXPIRATION_TIME = 5.0 * 60 // 5 minutes (in seconds) for production
    
    static let SIX_MONTHS_TIER = "six_months_tier"
    static let SELLIGENT_PLANS_PAGE = "selligent_plans_page"
    static let UNIT_DETAIL_TYPE = "unit_detail_page_type"
    
    var tracker: MixpanelTrackingManager?
    
    override init()
    {
        #if RELEASE
            let developerEnabled = false;
        #else
            let developerEnabled = true;
        #endif
        
        self.remoteConfig = FIRRemoteConfig.remoteConfig()
        self.remoteConfig.configSettings = FIRRemoteConfigSettings(developerModeEnabled: developerEnabled)!
        self.remoteConfig.setDefaultsFromPlistFileName("RemoteConfigDefaults")
    }
    
    func updateConfig()
    {
        #if RELEASE
            let expirationTime = REMOTE_CONFIG_EXPIRATION_TIME
        #else
            let expirationTime = 0.0
        #endif
        
        self.remoteConfig.fetch(withExpirationDuration: expirationTime) { (status, error) in
            if status == .success
            {
                print("Config fetched!")
                self.remoteConfig.activateFetched()
                
                // Super properties
                self.trackSixMonthsTierVariation(shouldShow6Months: self.isSixMonthsTierActive())
                self.trackSelligentServiceVariation(shouldShowSelligentPlans: self.isSelligentPlanActive())
                self.trackUnitDetailVariation(variation: self.remoteConfig[FirebaseRemoteConfigManager.UNIT_DETAIL_TYPE].stringValue)
                
                // Sending the 'Update event' if the config changed
                if self.didConfigurationChanged()
                {
                    self.trackConfigurationChanged()
                    self.updateLastConfigurationHash()
                }
            }
            else
            {
                print("Config not fetched - Using cached files")
            }
        }
    }
    
    func isSixMonthsTierActive() -> Bool
    {
        return self.remoteConfig[FirebaseRemoteConfigManager.SIX_MONTHS_TIER].boolValue
    }
    
    func isSelligentPlanActive() -> Bool
    {
        return self.remoteConfig[FirebaseRemoteConfigManager.SELLIGENT_PLANS_PAGE].boolValue
    }
    
    func shouldShowListAtUnitDetail() -> Bool {
        let isList = self.remoteConfig[FirebaseRemoteConfigManager.UNIT_DETAIL_TYPE].stringValue == "list"
        return isList
    }
}

// Tracking
extension FirebaseRemoteConfigManager
{
    func trackConfigurationChanged()
    {
        tracker?.trackConfigurationChanged()
    }
    
    func trackSixMonthsTierVariation(shouldShow6Months: Bool)
    {
        let variation = shouldShow6Months ? "active" : "deactivated"
        tracker?.trackVariation(FirebaseRemoteConfigManager.SIX_MONTHS_TIER, value: variation)
    }
    
    func trackSelligentServiceVariation(shouldShowSelligentPlans: Bool) {
        let variation = shouldShowSelligentPlans ? "selligent" : "abawebapps"
        tracker?.trackVariation(FirebaseRemoteConfigManager.SELLIGENT_PLANS_PAGE, value: variation)
    }
    
    func trackUnitDetailVariation(variation: String?) {
        var stringToReport: String
        if let variation = variation {
            stringToReport = variation
        } else {
            stringToReport = "none"
        }
        
        tracker?.trackVariation(FirebaseRemoteConfigManager.UNIT_DETAIL_TYPE, value: stringToReport)
    }
}

// Private methods
extension FirebaseRemoteConfigManager
{
    private static let LAST_HASH_KEY = "LastHashKey"
    
    func didConfigurationChanged() -> Bool
    {
        let configurationHash = calculateConfigurationHash()
        let lastConfigurationHash = getLastConfigurationHash()
        
        // if current configuration is different from previous configuration
        if configurationHash == lastConfigurationHash {
            return false
        } else {
            return true
        }
    }
    
    func getLastConfigurationHash() -> String?
    {
        return UserDefaults.standard.string(forKey: FirebaseRemoteConfigManager.LAST_HASH_KEY)
    }
    
    func updateLastConfigurationHash()
    {
        UserDefaults.standard.set(calculateConfigurationHash(), forKey: FirebaseRemoteConfigManager.LAST_HASH_KEY)
    }
    
    func calculateConfigurationHash() -> String
    {
        // Strings with all the key and values
        let sixMonthsTier = "\(FirebaseRemoteConfigManager.SIX_MONTHS_TIER): \(self.isSixMonthsTierActive())"
        let selligentPlans = "\(FirebaseRemoteConfigManager.SELLIGENT_PLANS_PAGE): \(self.isSelligentPlanActive())"
        let unitDetailType = "\(FirebaseRemoteConfigManager.UNIT_DETAIL_TYPE): \(self.remoteConfig[FirebaseRemoteConfigManager.UNIT_DETAIL_TYPE].stringValue)"
        
        // Add more configuration values
        
        // concat every key and value
        let configuration = "\(sixMonthsTier)" + "\(selligentPlans)" + "\(unitDetailType)" // concat more config values
        
        // Calculating configuration hash
        let configurationHash = md5(configuration)
        
        return configurationHash
    }
    
    func md5(_ string: String) -> String
    {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string, CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest
        {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
}
