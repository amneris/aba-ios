//
//  CoolaDataModels.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 10/03/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Wrap

struct ABACooladataProgressActionEventProperties: WrapCustomizable {
    var idUser: String?
    var idSession: String?
    var platform: String = "mobile"
    var idUnit: String?
    var section: String?
    var idElem: String?
    var exercises: String?
    var page: String?
    var idSite: String?
    var progressABAFilm: Int?
    var progressSpeak: Int?
    var progressWrite: Int?
    var progressInterpret: Int?
    var progressVideoClass: Int?
    var progressExcercises: Int?
    var progressVocabulary: Int?
    var progressAssessment: Int?
    var progressTotalPercentExcludingEvaluation: Int?
    var progressTotalPercent: Int?
    var lastChange: String?
    var dateInit: String?
    var device: String?

    init() { }
    
    func keyForWrapping(propertyNamed propertyName: String) -> String? {
        switch propertyName {
            case "idUser": return "user_id"
            case "idSession": return "session_id"
            case "platform": return "platform"
            case "idUnit": return "unit_id"
            case "section": return "section"
            case "idElem": return "id_elem"
            case "exercises": return "exercises"
            case "page": return "page"
            case "idSite": return "idSite"
            case "progressABAFilm": return "progress_ABA_Film"
            case "progressSpeak": return "progress_Speak"
            case "progressWrite": return "progress_Write"
            case "progressInterpret": return "progress_Interpret"
            case "progressVideoClass": return "progress_Video_Class"
            case "progressExcercises": return "progress_Excercises"
            case "progressVocabulary": return "progress_Vocabulary"
            case "progressAssessment": return "progress_Assessment"
            case "progressTotalPercentExcludingEvaluation": return "progress_TotalPercentExcludingEvaluation"
            case "progressTotalPercent": return "progress_TotalPercent"
            case "lastChange": return "lastchange"
            case "dateInit": return "dateInit"
            case "device": return "device"
            default: return propertyName
        }
    }
    
}

struct ABACooladataProgressActionEvent {
    var name: String
    var properties: ABACooladataProgressActionEventProperties

    // Jesus: Commenting code since it's not in use anymore
    /*
    init(abaProgressAction: [String: AnyObject]) {
        if let _name = abaProgressAction["action"] {
            self.name = _name as! String
        } else {
            self.name = "UNKNOWN ACTION"
        }

        self.properties = ABACooladataEventParser.cooladataPropertiesFromAction(abaProgressAction)
    }
    */

    init(serverResponseJSON: [Dictionary<String, NSObject>], userId: String?) {
        self.name = "PROGRESS_UPDATE_TOTAL"
        self.properties = ABACooladataEventParser.cooladataPropertiesFromResponce(serverResponseJSON, userId: userId)
    }
}
