//
//  Dependencies.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/09/16.
//  Based on https://github.com/ReactiveX/RxSwift/blob/master/RxExample/RxExample/Examples/Dependencies.swift
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift


class Dependencies {

    let backgroundWorkScheduler: ImmediateSchedulerType
    let mainScheduler: SerialDispatchQueueScheduler
    
    init() {
        // Background scheduler
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 3
        operationQueue.qualityOfService = QualityOfService.userInitiated
        backgroundWorkScheduler = OperationQueueScheduler(operationQueue: operationQueue)
        
        // Main scheduler
        mainScheduler = MainScheduler.instance
    }
}
