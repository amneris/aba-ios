//
//  ProfileCustomTableViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 13/12/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit
import BonMot
import SnapKit
import SVProgressHUD
import RESideMenu

enum ProfileSectionType: Int {
    case userInfo = 0, subcriptionInfo, legalInfo
}

class ProfileCustomTableViewController: UITableViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameDescription: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailDescription: UILabel!
    @IBOutlet weak var langLabel: UILabel!
    @IBOutlet weak var langDescription: UILabel!
    @IBOutlet weak var changePasswordButton: ProfileCustomButton!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var studentTypeLabel: UILabel!
    @IBOutlet weak var studentTypeDescription: UILabel!
    @IBOutlet weak var subscriptionExpirationDate: UILabel!
    @IBOutlet weak var restorePurchasesButton: ProfileCustomButton!
    @IBOutlet weak var networkModeLabel: UILabel!
    @IBOutlet weak var delteDownloadsButton: ProfileCustomButton!
    @IBOutlet weak var deleteDownloadsDescriptionText: UILabel!
    @IBOutlet weak var restorePurchasesCell: UITableViewCell!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var mobileDataSwitch: UISwitch!
    
    final let CELL_SIZE: CGFloat = 52.0
    
    let user = UserController.currentUser()!
    
    
    // MARK: -
    // MARK: Computed properties
    
    var userEmail: String { return user.email }
    
    var userName: String {
        if let userNames = user.surnames {
            return "\(user.name!) \(userNames)"
        } else {
            return user.name!
        }
    }
    
    var notificationsEnabled: Bool {
        get {
            guard let apnsStatus = UserDefaults.standard.string(forKey: kApnsConfigurationStateKey) else { return true }
            return apnsStatus == kApnsEnabled
        }
        
        set(value) {
            let defs = UserDefaults.standard
            
            if(value == true) {
                defs.set(kApnsEnabled, forKey: kApnsConfigurationStateKey)
            } else {
                defs.set(kApnsDisabled, forKey: kApnsConfigurationStateKey)
            }
            
            defs.synchronize()
        }
    }
    
    var mobileDataDownloads: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kMobileDownloadsStateKey);
        }
        
        set(value) {
            let defs = UserDefaults.standard
            defs.set(value, forKey: kMobileDownloadsStateKey)
            defs.synchronize()
        }
    }
    
    let userType = UserController.isUserPremium() ? "Premium" : "Free"
    
    var userLanguage: String {
        switch LanguageController.getCurrentLanguage() {
        case "es":
            return String.localize("languageSpainKey", comment: "")
        case "en":
            return String.localize("languageEnglishKey", comment: "")
        case "it":
            return String.localize("languageItalianKey", comment: "")
        case "de":
            return String.localize("languageDeutchKey", comment: "")
        case "fr":
            return String.localize("languageFrenchKey", comment: "")
        case "ru":
            return String.localize("languageRussianKey", comment: "")
        case "pt":
            return String.localize("languagePortugueseKey", comment: "")
        default:
            return "unknown"
        }
    }
    
    
    // MARK: -
    // MARK: UITableViewController lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 40.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.cellLayoutMarginsFollowReadableWidth = false
        configureStrings()
        configureSwitchState()
        tableView.tableFooterView = footerView()
    }
    
    // MARK: -
    // MARK: IBAction
    
    @IBAction func setMobileDataState(_ sender: Any) {
        mobileDataDownloads = (sender as! UISwitch).isOn
    }
    
    @IBAction func restorePurcheses(_ sender: Any) {
        SVProgressHUD.show(with: .black)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: kSubscriptionProcessCompletedOK), object: nil, queue: nil, using: subscriptionNotificationCatch)
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: kSubscriptionProcessCompletedFailed), object: nil, queue: nil, using: subscriptionNotificationCatch)
        SubscriptionController.restorePurchases()
    }
    
    @IBAction func deleteDownloads(_ sender: Any) {
        let actionAlert = UIAlertController(title: nil, message: String.localize("profile_delete_downloads_alert_title", comment: ""), preferredStyle: .alert)
        let cancelBtn = UIAlertAction(title: String.localize("alertSubscriptionKOButton", comment: ""), style: .cancel, handler: nil)
        let yesBtn = UIAlertAction(title: String.localize("profile_delete_downloads_alert_ok_button", comment: ""), style: .destructive) { _ in
            FilesUtils.deleteCachedUnitFiles()
            self.configureStrings()
        }
        
        actionAlert.addAction(cancelBtn)
        actionAlert.addAction(yesBtn)
        self.present(actionAlert, animated: true, completion: nil)
    }
    
    @IBAction func setNotificationState(_ sender: Any) {
        let state = (sender as! UISwitch).isOn
        notificationsEnabled = state
        
        let app = UIApplication.shared.delegate as! AppDelegate
        
        if (state) {
            app.registerForRemoteNotification()
        } else {
            app.unregisterForRemoteNotification()
        }
        
        MixpanelTrackingManager.shared().trackNotificationConfigurationChange()
    }
    
    // MARK: -
    // MARK: private methods
    
    private func configureStrings() {
        nameLabel.attributedText = String.localize("profile_name_title", comment: "").uppercased().styled(with: ProfileStyle.labelStyle)
        emailLabel.attributedText = String.localize("profile_email_title", comment: "").uppercased().styled(with: ProfileStyle.labelStyle)
        langLabel.attributedText = String.localize("profile_language_title", comment: "").uppercased().styled(with: ProfileStyle.labelStyle)
        changePasswordButton.setTitle(String.localize("profile_change_password_title", comment: "").uppercased(), for: .normal)
        notificationLabel.attributedText = String.localize("profile_notification_title", comment: "").styled(with: ProfileStyle.labelStyle)
        studentTypeLabel.attributedText = String.localize("profile_subscription_type_title", comment: "").uppercased().styled(with: ProfileStyle.labelStyle)
        restorePurchasesButton.setTitle(String.localize("profile_restore_purchases_title", comment: "").uppercased(), for: .normal)
        networkModeLabel.attributedText = String.localize("profile_mobile_data_title", comment: "").styled(with: ProfileStyle.labelStyle)
        delteDownloadsButton.setTitle(String.localize("profile_delete_downloads_title", comment: "").uppercased(), for: .normal)
        
        // Megas to free text
        let megasToFree = Int(FilesUtils.calculateUnitFilesWeigth())
        let textWithMegasToShow = String.localize("profile_delete_downloads_description", comment: "").replacingOccurrences(of: "100", with: "\(megasToFree)")
        deleteDownloadsDescriptionText.attributedText = textWithMegasToShow.styled(with: ProfileStyle.descriptionStyle)
        
        nameDescription.attributedText = userName.styled(with: ProfileStyle.descriptionStyle)
        emailDescription.attributedText = userEmail.styled(with: ProfileStyle.descriptionStyle)
        langDescription.attributedText = userLanguage.styled(with: ProfileStyle.descriptionStyle)
        studentTypeDescription.attributedText = userType.styled(with: ProfileStyle.descriptionStyle)
        
        if UserController.isUserPremium() {
            // Reading/creating needed values
            let user = UserController.currentUser()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            
            if let expirationDate = user?.expirationDate
            {
                let formattedString = formatter.string(from: expirationDate)
                let date:String = String.localize("profileRegister2Key", comment: "") + ": " + formattedString
                subscriptionExpirationDate.attributedText = date.styled(with: ProfileStyle.subDescriptionStyle)
            }
            else
            {
                subscriptionExpirationDate.isHidden = true
            }
        }
        else
        {
            subscriptionExpirationDate.isHidden = true
        }
    }
    
    private func configureSwitchState() {
        notificationsSwitch.setOn(notificationsEnabled, animated: false)
        mobileDataSwitch.setOn(mobileDataDownloads, animated: false)
    }
    
    fileprivate func subscriptionNotificationCatch(notification: Notification) {
        defer {
            SVProgressHUD.dismiss()
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kSubscriptionProcessCompletedOK), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kSubscriptionProcessCompletedFailed), object: nil)
        }
        
        if (notification.name.rawValue == kSubscriptionProcessCompletedOK) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            RootRouter().loadMainStoryboard(appDelegate.window)
        }
    }
    
    fileprivate func footerView() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        let footerLabel = UILabel()
        
        let versionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let buildString = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
        
        let string1 = "\(String.localize("profile_version_text", comment: "")) \(versionString) Build \(buildString)"
        let string2 = "@ 2017 ABA English"
        
        footerLabel.numberOfLines = 0
        let attributedString = NSAttributedString.composed(of: [
            string1.styled(with: ProfileStyle.footerLabelStyle),
            Special.lineSeparator,
            string2.styled(with: ProfileStyle.footerLabelStyle)
            ])
        
        footerLabel.attributedText = attributedString
        footerView.addSubview(footerLabel)
        
        footerLabel.snp.makeConstraints { (make) in
            make.width.height.equalTo(footerView).inset(10)
        }
        
        return footerView
    }
    
    fileprivate func logOut() {
        SVProgressHUD.show(with: .black)
        
        UserController.logout { (error) in
            SVProgressHUD.dismiss()
            if(error == nil) {
                let app = UIApplication.shared.delegate as! AppDelegate
                app.presenter.loadStartStoryboard()
            } else {
                let alert = ABAAlert(alertWithText: String.localize("errorLogout", comment: ""))
                alert?.show()
            }
        }
    }
    
    fileprivate func showTermsOfUse() {
        let legalInfoVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "LegalInfoViewController") as! LegalInfoViewController
        legalInfoVC.text = String.localize("profileTemsDescriptionKey", comment: "")
        legalInfoVC.addBackButton(withTitleBlue: String.localize("profileTemsTitleKey", comment: ""))
        self.navigationController?.pushViewController(legalInfoVC, animated: true)
    }
    
    fileprivate func showLegalInfo() {
        let legalInfoVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "LegalInfoViewController") as! LegalInfoViewController
        legalInfoVC.text = String.localize("profilePolicyDescriptionKey", comment: "")
        legalInfoVC.addBackButton(withTitleBlue: String.localize("profilePolicyTitleKey", comment: ""))
        self.navigationController?.pushViewController(legalInfoVC, animated: true)
    }
}

// MARK: -

extension ProfileCustomTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if(indexPath.section == ProfileSectionType.legalInfo.rawValue) {
            switch indexPath.row {
            case 0:
                cell.textLabel?.attributedText = String.localize("profile_term_of_use_button", comment: "").styled(with: ProfileStyle.labelStyle)
            case 1:
                cell.textLabel?.attributedText = String.localize("profile_privacy_policy_button", comment: "").styled(with: ProfileStyle.labelStyle)
            case 2:
                cell.textLabel?.attributedText = String.localize("profile_logout_button", comment: "").styled(with: ProfileStyle.logoutButtonStyle)
            default: break
            }
        } else {
            cell.separatorInset = .zero
            cell.preservesSuperviewLayoutMargins = false
            cell.layoutMargins = .zero
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != ProfileSectionType.legalInfo.rawValue {
            let size = self.view.bounds.size
            let rightInset = size.width > size.height ? size.width : size.height
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, rightInset * 2)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == 2 {
            // TODO: Jesus - Remove this case once 3G/Wifi alerts are implemented
            return 0
        }
        else if indexPath.section == 1 && indexPath.row == 0 {
            if UserController.isUserPremium() {
                return CELL_SIZE * 1.2
            }
            else {
                return CELL_SIZE
            }
        }
        else {
            return CELL_SIZE
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 2
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == ProfileSectionType.legalInfo.rawValue) {
            switch indexPath.row {
            case 0: showTermsOfUse()
            case 1: showLegalInfo()
            case 2: logOut()
            default: break
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case ProfileSectionType.userInfo.rawValue:
            return String.localize("profile_card_title", comment: "")
        case ProfileSectionType.subcriptionInfo.rawValue:
            return String.localize("profile_subscription_title", comment: "")
        default:
            return " "
        }
    }
}
