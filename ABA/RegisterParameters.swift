//
//  RegisterParameters.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

struct RegistrationEmailParameters {

    let deviceId: String
    let sysOper: String
    let deviceName: String
    let email: String
    let password: String
    let languageEnvironment: String
    let name: String
    let surnames: String
    let ipMobile: String
    let idSourceList: String = "0"
    let appVersion: String
    
    var idPartner: String {
        get {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                return partneridIphone
            case .pad:
                return partneridIpad
            // case .Unspecified:
            default:
                return "Unspecified idPartner"
            }
        }
    }
    
    var deviceTypeSource: String {
        get {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                return "m"
            case .pad:
                return "t"
            // case .Unspecified:
            default:
                return "Unspecified device type"
            }
        }
    }
}

extension RegistrationEmailParameters: ApiSignable {

    var orderedParams: [String] {
        return [
            "deviceId",
            "sysOper",
            "deviceName",
            "email",
            "password",
            "langEnv",
            "name",
            "surnames",
            "ipMobile",
            "idPartner",
            "idSourceList",
            "deviceTypeSource",
            "appVersion"]
    }

    func params() -> [String: String] {
        return [
            "deviceId": deviceId,
            "sysOper": sysOper,
            "deviceName": deviceName,
            "email": email,
            "password": password,
            "langEnv":languageEnvironment,
            "name":name,
            "surnames":surnames,
            "ipMobile":ipMobile,
            "idPartner":idPartner,
            "idSourceList":idSourceList,
            "deviceTypeSource":deviceTypeSource,
            "appVersion": appVersion]
        
    }
}
