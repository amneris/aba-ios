//
//  DeepLinkController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 30/12/15.
//  Copyright © 2015 ABA English. All rights reserved.
//
// ////////////////////////////////////
// Example usages
// campusapp://course?unit=10&externalToken=9b99d1e59fe3f96be2d28d7a50707f9e
// campusapp://course?unit=10&section=2&externalToken=9b99d1e59fe3f96be2d28d7a50707f9e
// campusapp://purchase?externalToken=9b99d1e59fe3f96be2d28d7a50707f9e
// campusapp://autologin?externalToken=4849c0669bae3ef980fdc0d2ccf14867
// {"deeplink_data":"campusapp://course?unit=10&externalToken=9b99d1e59fe3f96be2d28d7a50707f9e"}
// {"deeplink_data":"campusapp://course?unit=10&section=2&externalToken=9b99d1e59fe3f96be2d28d7a50707f9e"}
// {"deeplink_data":"campusapp://purchase?externalToken=4849c0669bae3ef980fdc0d2ccf14867"}
// ////////////////////////////////////

import UIKit

private enum DeepLinkName {

    case scheme, course, plans, autologin, unknown

    func name() -> String {
        switch self {
        case .scheme:
            return "campusapp"
        case .course:
            return "course"
        case .plans:
            return "purchase"
        case .autologin:
            return "autologin"
        case .unknown:
            return "unknown"
        }
    }
}

class DeepLinkController: NSObject {

    private var url: NSURL?
    var token: String?
    var authority: String?
    var unit: String?
    var section: String?

    var plans: Bool {
        get {
            return self.url?.host == DeepLinkName.plans.name()
        }
    }

    var canParseDeepLink: Bool {
        get {
            return self.url?.scheme == DeepLinkName.scheme.name()
        }
    }

    static var storedToken: String? {
        get {
            return UserDefaults.standard.object(forKey: kDeepLinkToken) as? String
        }
        set {
            UserDefaults.standard.set(nil, forKey: kDeepLinkToken)
            UserDefaults.standard.synchronize()
        }
    }

    init(launchOptions: NSDictionary?) {
        super.init()

        guard let safeLaunchOptions = launchOptions else {
            defaultValues()
            return
        }

        let schemeUrl = safeLaunchOptions.object(forKey: UIApplicationLaunchOptionsKey.url) as? NSURL
        let pushNotificationInfo = safeLaunchOptions.object(forKey: UIApplicationLaunchOptionsKey.remoteNotification) as? NSDictionary
        let pushUrl = pushNotificationInfo?.object(forKey: "deeplink_data") as? NSURL

        if let schemeUrl = schemeUrl {
            print("DEBUG: didFinishLaunchingWithOptions url scheme")
            explodeURL(givenUrl: schemeUrl)
        }
        else if let pushUrl = pushUrl {
            print("DEBUG: didFinishLaunchingWithOptions push")
            explodeURL(givenUrl: pushUrl)
        }
    }

    init(url: NSURL?) {
        super.init()
        guard let safeUrl = url else {
            self.authority = ""
            self.unit = ""
            self.section = ""
            self.token = ""
            self.url = NSURL()
            return
        }
        explodeURL(givenUrl: safeUrl)
    }

    func saveDeepLink() {
        if let _ = self.unit {
            UserDefaults.standard.set(self.unit, forKey: kDeepLinkUnit)
            UserDefaults.standard.synchronize()

            if let _ = self.section {
                UserDefaults.standard.set(self.section, forKey: kDeepLinkSection)
                UserDefaults.standard.synchronize()
            } else {
                UserDefaults.standard.removeObject(forKey: kDeepLinkSection)
                UserDefaults.standard.synchronize()
            }
        } else {
            UserDefaults.standard.removeObject(forKey: kDeepLinkUnit)
            UserDefaults.standard.removeObject(forKey: kDeepLinkSection)
            UserDefaults.standard.synchronize()
        }

        UserDefaults.standard.set(self.plans, forKey: kDeepLinkPlans)
        UserDefaults.standard.synchronize()

        if let _ = self.token {
            UserDefaults.standard.set(self.token, forKey: kDeepLinkToken)
            UserDefaults.standard.synchronize()
        }
    }

    static func cleanDeeplinkingParameters() {
        UserDefaults.standard.removeObject(forKey: kDeepLinkUnit)
        UserDefaults.standard.removeObject(forKey: kDeepLinkSection)
        UserDefaults.standard.removeObject(forKey: kDeepLinkPlans)
        UserDefaults.standard.removeObject(forKey: kDeepLinkToken)
        UserDefaults.standard.synchronize()
    }

    // MARK: Private

    private func queryStringParameter(url: NSURL, key: String) -> String? {
        if let urlComponents = NSURLComponents(url: url as URL, resolvingAgainstBaseURL: false),
            let queryItems = (urlComponents.queryItems as [NSURLQueryItem]?) {
                return queryItems.filter({ (item) in item.name == key }).first?.value!
        }

        return nil
    }

    func explodeURL(givenUrl: NSURL) {
        self.url = givenUrl

        print("URL: \(givenUrl)")
        print("URL: \(givenUrl.scheme)")
        print("URL: \(givenUrl.host)")
        print("URL: \(givenUrl.query)")

        if let host = givenUrl.host {

            switch host {
            case DeepLinkName.course.name():
                self.authority = DeepLinkName.course.name()
                self.unit = self.queryStringParameter(url: givenUrl, key: "unit")
                self.section = self.queryStringParameter(url: givenUrl, key: "section")

            case DeepLinkName.plans.name():
                self.authority = DeepLinkName.plans.name()

            case DeepLinkName.autologin.name():
                self.authority = DeepLinkName.autologin.name()
            default:
                self.authority = DeepLinkName.unknown.name()
            }

            self.token = self.queryStringParameter(url: givenUrl, key: "externalToken")
        }
    }

    func defaultValues() {
        self.authority = ""
        self.unit = ""
        self.section = ""
        self.token = ""
        self.url = NSURL()
    }
}
