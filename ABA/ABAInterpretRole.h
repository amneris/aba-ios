//
//  ABAInterpretRole.h
//  ABA
//
//  Created by Marc Güell Segarra on 12/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABARole.h"
#import "ABAInterpretPhrase.h"

@class ABAInterpret;

@interface ABAInterpretRole : ABARole

@property NSNumber<RLMBool> * completed;
//@property (readonly) NSArray *interpretPhrases;

- (RLMResults *)interpretPhrases;

@end

RLM_ARRAY_TYPE(ABAInterpretRole)