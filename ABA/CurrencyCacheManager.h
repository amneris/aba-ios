//
//  CurrecyCacheManager.h
//  ABA
//
//  Created by MBP13 Jesus on 11/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyCacheManager : NSObject

+ (NSArray *)loadCurrenciesFromCache;
+ (void)storeCurrenciesInCache:(NSArray *)arrayOfCurrencies;

@end
