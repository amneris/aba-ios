//
//  NSArray+ABA.m
//  ABA
//
//  Created by Oriol Vilaró on 25/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "NSArray+ABA.h"
#import <Realm/Realm.h>

@implementation NSArray (ABA)

- (NSUInteger)indexOfRealmObject:(RLMObject *)anObject
{
    NSUInteger index = 0;
    for (RLMObject *cmp in self)
    {
        if ([cmp isEqualToObject:anObject])
        {
            return index;
        }
        index++;
    }
    return NSNotFound;
}

- (BOOL)containsRealmObject:(RLMObject *)anObject
{
    for (RLMObject *cmp in self)
    {
        if ([cmp isEqualToObject:anObject])
        {
            return YES;
        }
    }
    return NO;
}

@end
