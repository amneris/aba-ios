//
//  MixpanelTrackingManager.m
//  ABA
//
//  Created by Jesus Espejo on 19/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "MixpanelTrackingManager.h"

#import "TrackingManagerProtocol.h"

// Model
#import "ABAPlan+GenericTrackingManager.h"
#import "ABAPlan.h"
#import "ABARealmLevel.h"
#import "ABARealmUser+GenericTrackingManager.h"
#import "ABARealmUser.h"
#import "ABASection+GenericTrackingManager.h"
#import "ABASection+Methods.h"
#import "ABAUnit.h"
#import "SKProduct+GenericTrackingManager.h"

// Controller
#import "PlansViewController.h"
#import "UserController.h"

// Libraries in use
#import "APIManager.h"
#import "CurrencyManager.h"
#import "Mixpanel.h"
#import "Utils.h"

// Definitions
#define kIsSecondOrLaterSessionKey @"kIsSecondOrLaterSessionKey"
#define kUnknownUser @"<Unknown>"
#define kUnknownOriginalPrice @"Unknown original price"

@implementation MixpanelTrackingManager

+ (id)sharedManager
{
    if (isTargetTest)
    {
        return nil;
    }
    
    static MixpanelTrackingManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

#ifdef MIXPANEL_PRODUCTION

// Production key - ABA English Production
#define MIXPANEL_TOKEN @"7a0b3be8625e1b358121231a9e8f61d9"

#else

// Development key - ABA English Development
//#define MIXPANEL_TOKEN @"6d25373c64145e404cfd8bc0ea488dd0"
#define MIXPANEL_TOKEN @"6d25373c64145e404cfd8bc0ea488dd0"

#endif

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Mixpanel register
        Mixpanel *mixpanel = [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
        mixpanel.showNotificationOnActive = NO;
        mixpanel.enableLogging = NO;
        
        [self deviceSuperPropertiesForEvents];
    }
    
    return self;
}

#pragma mark - Public methods (Reset & configuration)

- (void)reset
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    // Flushing events
    [mixpanel flushWithCompletion:^{
        
        // reseting after flush
        [mixpanel reset];
        
        // After reseting we have to create a new identifier
        [self createABAUniqueIdentifier];
    }];
}

- (void)createABAUniqueIdentifier
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    NSString *abaUniqueId = [NSString stringWithFormat:@"%@-%@-%f", @"ABA", mixpanel.distinctId, [[NSDate date] timeIntervalSince1970]];
    [mixpanel identify:abaUniqueId];
    
    // Setting name to user
    // [[mixpanel people] set:@{@"$name" : kUnknownUser}];
}

#pragma mark - Public methods (Push notification)

- (void)registerToken:(NSData *)deviceToken
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people addPushDeviceToken:deviceToken];
}

- (void)trackPushNotificationIfExists:(NSDictionary *)launchOptions
{
    if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey])
    {
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel trackPushNotification:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]];
    }
}

- (void)trackPushNotification:(NSDictionary *)userInfo
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel trackPushNotification:userInfo];
}

#pragma mark - Public methods (Analytics/Login & Register)

- (void)trackFirstAppOpenOnce
{
    BOOL isSecondOrMoreSession = [[NSUserDefaults standardUserDefaults] boolForKey:kIsSecondOrLaterSessionKey];
    if (!isSecondOrMoreSession)
    {
        // Storing so the next time the method is called the event is not sent
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsSecondOrLaterSessionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Sending event
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"First open" properties:[self dictionaryWithTimestamp]];
    }
}

- (void)trackLoginEvent:(RecordType)type
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people set:[NSDictionary dictionaryWithObject:[self currentMixpanelFormattedDate] forKey:@"Last login date"]];
    
    // Event
    NSMutableDictionary *registerDictionary = [NSMutableDictionary new];
    [registerDictionary setObject:[self attributeForRecordType:type] forKey:@"Type"];
    [registerDictionary addEntriesFromDictionary:[self dictionaryWithTimestamp]];
    [mixpanel track:@"Login" properties:registerDictionary];
}

- (void)trackLoginSuperproperties
{
    ABARealmUser *currentUser = [UserController currentUser];
    
    if (currentUser)
    {
        // people
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel createAlias:currentUser.idUser forDistinctID:mixpanel.distinctId];
        [mixpanel identify:currentUser.idUser];
        [mixpanel.people set:[currentUser trackingProperties]];
        
        // Superproperties
        [mixpanel registerSuperProperties:[currentUser trackingSuperProperties]];
    }
}

- (void)trackLoginOfflineEvent
{
    // Event
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Login offline"];
}

- (void)trackRegister:(RecordType)type
{
    ABARealmUser *currentUser = [UserController currentUser];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    if (currentUser)
    {
        // Profile
        [mixpanel createAlias:currentUser.idUser forDistinctID:mixpanel.distinctId];
        [mixpanel identify:mixpanel.distinctId];
        
        // Uncomment when we do AB Testing with mixpanel in iOS
        // [mixpanel joinExperiments];
        
        [mixpanel.people set:[currentUser trackingProperties]];
        [mixpanel.people set:[NSDictionary dictionaryWithObject:[self currentMixpanelFormattedDate] forKey:@"Registration date"]];
        
        // Superproperties
        [mixpanel registerSuperProperties:[currentUser trackingSuperProperties]];
    }
    
    // Event
    NSMutableDictionary *registerDictionary = [NSMutableDictionary new];
    [registerDictionary setObject:[self attributeForRecordType:type] forKey:@"Type"];
    [registerDictionary addEntriesFromDictionary:[self dictionaryWithTimestamp]];
    [mixpanel track:@"Register" properties:registerDictionary];
}

#pragma mark - Public methods (Analytics/Events)

- (void)trackLevelChange:(ABARealmLevel *)fromLevel toLevel:(ABARealmLevel *)toLevel
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Level change" properties:[self dictionaryWithLevelChange:fromLevel toLevel:toLevel]];
}

- (void)trackPricingScreenArrivalWithOriginController:(PlansViewControllerOriginType)type
{
    NSString *attributeName = [self attributeForPlansViewControllerOriginType:type];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Subscription shown"
         properties:@{
                      @"Source" : attributeName
                      }];
}

- (void)trackPricingSelected:(ABAPlan *)plan
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Subscription selected" properties:[plan trackingProperties]];
}

- (void)trackPurchase:(SKPaymentTransaction *)transaction withPeriod:(NSInteger)period withProduct:(SKProduct *)product withOriginalPrice:(NSDecimalNumber *)originalPrice
{
    // Values to translate to USD
    double priceInCurrency = [product.price doubleValue];
    NSString *currencyCode = [product.priceLocale objectForKey:NSLocaleCurrencyCode];
    
    // Getting price in dollars from CurrencyManager
    double priceInDollars = [CurrencyManager convertAmount:priceInCurrency toUSDollarFrom:currencyCode];
    
    // Once having the price in dollars we can start reporting
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    // Checking whether the converted price was properly done
    if (priceInDollars != NSNotFound)
    {
        // Profile: Reporting if conversion was found
        [mixpanel.people trackCharge:[NSNumber numberWithDouble:priceInDollars]];
    }
    
    // Getting dictionary with main purchase properties
    NSMutableDictionary *purchaseDictionary = [NSMutableDictionary dictionaryWithDictionary:[product trackingProperties]];
    
    // Calculating the price in EUR
    double priceInEuros = [CurrencyManager convertAmount:priceInCurrency toEurFrom:currencyCode];
    if (priceInEuros != NSNotFound)
    {
        [purchaseDictionary setObject:[NSDecimalNumber numberWithDouble:priceInEuros] forKey:@"charged_price_eur"];
        if (originalPrice)
        {
            double originalPriceInEuros = [CurrencyManager convertAmount:originalPrice.doubleValue toEurFrom:currencyCode];
            [purchaseDictionary setObject:[NSDecimalNumber numberWithDouble:originalPriceInEuros] forKey:@"original_price_eur"];
        }
        else
        {
            [purchaseDictionary setObject:kUnknownOriginalPrice forKey:@"original_price_eur"];
        }
    }
    else
    {
        [purchaseDictionary setObject:kUnknownOriginalPrice forKey:@"charged_price_eur"];
    }
    
    // Event
    [mixpanel track:@"Subscription purchased" properties:purchaseDictionary];
}

- (void)trackActivatedPurchase
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Subscription activated"];
}

- (void)trackInfoButtonPushed:(PlanInfoType)infoType {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    NSDictionary * props = [NSDictionary dictionaryWithObject:[self attributeForPlanInfoType:infoType]
                                                       forKey:@"Type"];
    [mixpanel track:@"Plan info" properties:props];
}

#pragma mark - Notifications

- (void)trackNotificationConfigurationChange
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    // Event
    [mixpanel track:@"Notification configuration change"];
}

- (void)trackNotificationSuperproperties:(BOOL)enabled
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    // Tracking super properties
    NSString *attributeForNotification = [self attributeForNotificationStatus:enabled];
    [mixpanel registerSuperProperties:@{ @"Notification enabled" : attributeForNotification }];
    
    // People
    [mixpanel.people set:@{ @"Notification enabled" : attributeForNotification }];
}

- (void)trackSection:(ABASection *)section withSecondsOfStudyTime:(double)secondsOfStudy withInitialProgress:(NSNumber *)initialProgress withFinalProgress:(NSNumber *)finalProgress
{
    // Threshold:
    // - 30 secs study time, or
    // - progress difference
    if ([initialProgress floatValue] == [finalProgress floatValue] && secondsOfStudy < kSectionTimeThreshold)
    {
        return;
    }
    
    // Creating attribute disctionaries
    NSDictionary *attributesForSection = [section trackingProperties];
    NSDictionary *attributesForSectionProgress = [self dictionaryFromSecondsOfStudyTime:secondsOfStudy withInitialProgress:initialProgress withFinalProgress:finalProgress];
    
    // Merging dictionaries
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] init];
    [attributesToSend addEntriesFromDictionary:attributesForSection];
    [attributesToSend addEntriesFromDictionary:attributesForSectionProgress];
    
    // Event - sending previously created dictionaries
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Section" properties:attributesToSend];
    
    // Profile - sending hours of study and incrementing frequency of study
    double hoursOfStudy = secondsOfStudy / 3600.0; // From seconds to hours
    [mixpanel.people increment:@{ @"Hours of study" : [NSNumber numberWithDouble:hoursOfStudy],
                                  @"Study session count" : [NSNumber numberWithInteger:1] }];
}

#pragma mark - Configuration and AB testing

- (void)trackVariation:(nonnull NSString *)key value:(NSString *)value
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    
    // Tracking event property
    [mixpanel registerSuperProperties:[NSDictionary dictionaryWithObject:value forKey:key]];
    
    // Tracking user superproperty
    [mixpanel.people set:@{ key : value }];
}

- (void)trackConfigurationChanged
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Configuration Changed"];
}

#pragma mark - Downloads

- (void)trackDownloadStarted:(ABAUnit *)unit
{
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] init];
    if (unit && unit.idUnit)
    {
        [attributesToSend setObject:unit.idUnit forKey:@"Unitid"];
    }
    
    // Event
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Download started" properties:attributesToSend];
}

- (void)trackDownloadCancelled:(ABAUnit *)unit
{
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] init];
    if (unit && unit.idUnit)
    {
        [attributesToSend setObject:unit.idUnit forKey:@"Unitid"];
    }
    
    // Event
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Download cancelled" properties:attributesToSend];
}

- (void)trackDownloadError:(ABAUnit *)unit
{
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] init];
    if (unit && unit.idUnit)
    {
        [attributesToSend setObject:unit.idUnit forKey:@"Unitid"];
    }
    
    // Event
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Download error" properties:attributesToSend];
}

- (void)trackDownloadFinish:(ABAUnit *)unit
{
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] init];
    if (unit && unit.idUnit)
    {
        [attributesToSend setObject:unit.idUnit forKey:@"Unitid"];
    }
    
    // Event
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Download finished" properties:attributesToSend];
}

- (void)showMixpanelNotificationsAndSurveys
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel showNotification];
}

#pragma mark - Purchase error

- (void)trackSubscriptionErrorWithSubscriptionStatus:(SubscriptionStatus)status
{
    NSMutableDictionary *properties = [[NSMutableDictionary alloc] init];
    if (status)
    {
        switch (status)
        {
            case SubscriptionStatusActivationProcessFailed:
                [properties setObject:@"Subscription Status Activation Process Failed" forKey:@"Subscription status"];
                break;
            case SubscriptionStatusTransactionVerificationFailed:
                [properties setObject:@"Subscription Status Transaction Verification Failed" forKey:@"Subscription status"];
                break;
            case SubscriptionStatusPurchaseProcessFailed:
                [properties setObject:@"Subscription Status Purchase Process Failed" forKey:@"Subscription status"];
                break;
            case SubscriptionStatusRestoreProcessFailed:
                [properties setObject:@"Subscription Status Restore Process Failed" forKey:@"Subscription status"];
                break;
            case SubscriptionStatusPurchaseAlreadyAssigned:
                [properties setObject:@"Subscription Status Purchase Already Assigned" forKey:@"Subscription status"];
                break;
            case SubscriptionStatusRestoreNoTransactions:
                [properties setObject:@"Subscription Status Restore No Transactions" forKey:@"Subscription status"];
                break;
            case SubscriptionStatusProductNotFound:
                [properties setObject:@"Subscription Status Product Not Found" forKey:@"Subscription status"];
            default:
                break;
        }
    }
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Subscription attempt error" properties:properties];
}

#pragma mark - Registration funnel tracking

- (void)trackCurrentFunnelTypeSuperProperties
{
    NSDictionary *attributesToSend = [self dictionaryWithABTestingRegisterFunnel];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel registerSuperProperties:attributesToSend];
}

- (void)trackRegistrationFunnelWithRecordType:(RecordType)type withFunnelActionType:(RegistrationFunnelActionType)action
{
    [self trackCurrentFunnelTypeSuperProperties];
    
    NSMutableDictionary *mutDictionary = [[NSMutableDictionary alloc] initWithDictionary:[self dictionaryWithABTestingRegisterFunnel]];
    [mutDictionary setObject:[self attributeForRecordType:type] forKey:@"Type"];
    [mutDictionary setObject:(action == kRegistrationFunnelActionTypeRegister ? @"Register" :  @"Login") forKey:@"Funnel action"];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Registration Funnel ABTesting" properties:mutDictionary];
}

- (void)trackRegistrationFunnelShown
{
    [self trackCurrentFunnelTypeSuperProperties];
    
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] initWithDictionary:[self dictionaryWithABTestingRegisterFunnel]];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Registration form shown" properties:attributesToSend];
}

#pragma mark - Private methods

- (void)deviceSuperPropertiesForEvents
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    if (IS_IPAD)
    {
        [mixpanel registerSuperProperties:@{ @"Device type" : @"iPad" }];
    }
    else
    {
        [mixpanel registerSuperProperties:@{ @"Device type" : @"iPhone" }];
    }
}

@end
