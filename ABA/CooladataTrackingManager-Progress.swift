//
//  CooladataTrackingManager-Progress.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/05/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Wrap

extension CoolaDataTrackingManager {

    // MARK: Progress action tracking
    class func sendUpdatedProgress(_ progressUpdate: [Dictionary<String, NSObject>])
    {
        let userId = readUserProperties()["user_id"]
        
        let event = ABACooladataProgressActionEvent(serverResponseJSON: progressUpdate, userId: userId);
        self.trackProgressActionEvent(event)
    }

    // MARK: Private methods

    fileprivate class func trackProgressActionEvent(_ abaEvent: ABACooladataProgressActionEvent)
    {
        let name = abaEvent.name
        var properties: WrappedDictionary
        do {
            properties = try wrap(abaEvent.properties)
        }
        catch {
            properties = [:]
        }
        CoolaDataTracker.getInstance().trackEvent(name, properties: properties as [String : AnyObject])
    }
}
