//
//  SectionCell.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

class SectionCell: UITableViewCell {
    
    static let SECTION_CELL_REUSER_IDENTIFIER = "SectionCellReuseIdentifier"
    static let SECTION_CELL_HEIGHT: CGFloat = 80.0
    static let PROGRESS_BAR_OFFSET: CGFloat = 3.0
    
    let progressBarView: UIView = UIView(frame: CGRect.zero)
    let progressBar_1: UIProgressView = UIProgressView(frame: CGRect.zero)
    let progressBar_2: UIProgressView = UIProgressView(frame: CGRect.zero)
    let progressBar_3: UIProgressView = UIProgressView(frame: CGRect.zero)
    let progressBar_4: UIProgressView = UIProgressView(frame: CGRect.zero)
    
    let labelTitle: UILabel = UILabel(frame: CGRect.zero)
    let imageViewSection: UIImageView = UIImageView(frame: CGRect.zero)
    
    init() {
        super.init(style: .default, reuseIdentifier: SectionCell.SECTION_CELL_REUSER_IDENTIFIER)
        
        progressBarView.addSubview(progressBar_1)
        progressBarView.addSubview(progressBar_2)
        progressBarView.addSubview(progressBar_3)
        progressBarView.addSubview(progressBar_4)
        
        self.contentView.addSubview(progressBarView)
        self.contentView.addSubview(labelTitle)
        self.contentView.addSubview(imageViewSection)
        
        configureUI()
        configureLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// Public
extension SectionCell {
    
    func loadSection(sectionModel: SectionViewModel) {
        
//        print("----")
//        print("view frame: \(self.frame)")
//        print("imageViewSection frame: \(self.imageViewSection.frame)")
//        print("progressBar frame: \(self.progressBar.frame)")
//        print("labelTitle frame: \(self.labelTitle.frame)")
//        print("----")
        
//        print("progressBar 1 frame: \(self.progressBar_1.frame)")
//        print("progressBar 2 frame: \(self.progressBar_2.frame)")
//        print("progressBar 3 frame: \(self.progressBar_3.frame)")
//        print("progressBar 4 frame: \(self.progressBar_4.frame)")
        
        self.progressBar_1.progress = 0.0
        self.progressBar_2.progress = 0.0
        self.progressBar_3.progress = 0.0
        self.progressBar_4.progress = 0.0
        
        if let progress = sectionModel.sectionProgress?.floatValue {
            
            if progress > 75.0 {
                
                self.progressBar_1.progress = 1.0
                self.progressBar_2.progress = 1.0
                self.progressBar_3.progress = 1.0
                self.progressBar_4.progress = (progress - 75.0) / 25.0
            }
            else if progress > 50.0 {
                
                self.progressBar_1.progress = 1.0
                self.progressBar_2.progress = 1.0
                self.progressBar_3.progress = (progress - 50.0) / 25.0
            }
            else if progress > 25.0 {
                
                self.progressBar_1.progress = 1.0
                self.progressBar_2.progress = (progress - 25.0) / 25.0
            }
            else {
                
                self.progressBar_1.progress = progress / 25.0
            }
        }
        
        // Section icon and text
        switch sectionModel.sectionType {
        case kABASectionTypes.abaFilmSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_film")
            labelTitle.text = String.localize("unitMenuTitle1Key", comment: "film").uppercased()
            break
        case kABASectionTypes.abaSpeakSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_speak")
            labelTitle.text = String.localize("unitMenuTitle2Key", comment: "speak").uppercased()
            break
        case kABASectionTypes.abaWriteSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_write")
            labelTitle.text = String.localize("unitMenuTitle3Key", comment: "write").uppercased()
            break
        case kABASectionTypes.abaInterpretSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_interpret")
            labelTitle.text = String.localize("unitMenuTitle4Key", comment: "interpret").uppercased()
            break
        case kABASectionTypes.abaVideoClassSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_video")
            labelTitle.text = String.localize("unitMenuTitle5Key", comment: "video").uppercased()
            break
        case kABASectionTypes.abaExercisesSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_exercise")
            labelTitle.text = String.localize("unitMenuTitle6Key", comment: "exercise").uppercased()
            break
        case kABASectionTypes.abaVocabularySectionType:
            imageViewSection.image = UIImage(named: "icon_unit_vocabulary")
            labelTitle.text = String.localize("unitMenuTitle7Key", comment: "vocabulary").uppercased()
            break
        case kABASectionTypes.abaAssessmentSectionType:
            imageViewSection.image = UIImage(named: "icon_unit_evaluation")
            labelTitle.text = String.localize("unitMenuTitle8Key", comment: "evaluation").uppercased()
            break
        default:
            print("unknown section type")
            break
        }
        
        // Cell color depending on the section status
        switch sectionModel.sectionStatus {
        case kABASectionStyleOptions.abaSectionStyleBgColorDone:
            self.imageViewSection.backgroundColor = UnitDetailConstants.colorSectionDone
            self.progressBar_1.progressTintColor = UnitDetailConstants.colorSectionDone
            self.progressBar_2.progressTintColor = UnitDetailConstants.colorSectionDone
            self.progressBar_3.progressTintColor = UnitDetailConstants.colorSectionDone
            self.progressBar_4.progressTintColor = UnitDetailConstants.colorSectionDone
            self.accessoryType = .disclosureIndicator
            self.accessoryView = nil
            self.labelTitle.textColor = UIColor.white
            
        case kABASectionStyleOptions.abaSectionStyleBgNumberColorCurrent:
            self.imageViewSection.backgroundColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_1.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_2.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_3.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_4.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.accessoryType = .disclosureIndicator
            self.accessoryView = nil
            self.labelTitle.textColor = UIColor.white
            
        case kABASectionStyleOptions.abaSectionStyleLockEnabled:
            self.imageViewSection.backgroundColor = UnitDetailConstants.colorSectionDefault
            self.progressBar_1.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_2.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_3.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_4.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.accessoryType = .none
            self.accessoryView = UIImageView(image: #imageLiteral(resourceName: "icon_unit_locked"))
            self.labelTitle.textColor = UnitDetailConstants.colorDisabledDefault
            self.imageViewSection.image = self.imageViewSection.image?.imageWithColor(color: UnitDetailConstants.colorDisabledDefault)

        default:
            // case: kABASectionStyleOptions.kABASectionStyleDefault:
            // case kABASectionStyleOptions.abaSectionStyleAnimatedYes:
            // case kABASectionStyleOptions.abaSectionStyleAlphaEnabled:
            self.imageViewSection.backgroundColor = UnitDetailConstants.colorSectionDefault
            self.progressBar_1.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_2.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_3.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.progressBar_4.progressTintColor = UnitDetailConstants.colorSectionCurrent
            self.accessoryType = .disclosureIndicator
            self.accessoryView = nil
            self.labelTitle.textColor = UIColor.white
        }
        
        self.imageViewSection.layer.cornerRadius = SectionCell.SECTION_CELL_HEIGHT * 0.4
    }
}

internal extension SectionCell {
    
    func configureUI() {
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.clear
        
        // section image customization
        self.imageViewSection.backgroundColor = UnitDetailConstants.colorSectionDefault
        self.imageViewSection.contentMode = .center
        
        self.labelTitle.textColor = UIColor.white
        self.labelTitle.font = UIFont.boldSystemFont(ofSize: 16)
        
        self.progressBarView.backgroundColor = UIColor.clear
        self.progressBar_1.progressTintColor = UnitDetailConstants.colorSectionCurrent
        self.progressBar_1.trackTintColor = UnitDetailConstants.colorDefault
        self.progressBar_2.progressTintColor = UnitDetailConstants.colorSectionCurrent
        self.progressBar_2.trackTintColor = UnitDetailConstants.colorDefault
        self.progressBar_3.progressTintColor = UnitDetailConstants.colorSectionCurrent
        self.progressBar_3.trackTintColor = UnitDetailConstants.colorDefault
        self.progressBar_4.progressTintColor = UnitDetailConstants.colorSectionCurrent
        self.progressBar_4.trackTintColor = UnitDetailConstants.colorDefault
    }
    
    func configureLayout() {
        
        self.contentView.snp.makeConstraints { make in
            make.left.equalTo(self)
            make.right.equalTo(self).offset(-25)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
        }
        
        self.imageViewSection.snp.makeConstraints{ make in
            make.left.equalTo(self.contentView).offset(10)
            make.width.equalTo(SectionCell.SECTION_CELL_HEIGHT * 0.8)
            make.centerY.equalTo(self.contentView)
            make.height.equalTo(SectionCell.SECTION_CELL_HEIGHT * 0.8)
        }
        
        self.labelTitle.snp.makeConstraints { make in
            make.left.equalTo(self.imageViewSection.snp.right).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.bottom.equalTo(self.contentView.snp.centerY).offset(1)
        }
        
        self.progressBarView.snp.makeConstraints { make in
            make.left.equalTo(self.imageViewSection.snp.right).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.top.equalTo(self.labelTitle.snp.bottom).offset(5)
            make.height.equalTo(2)
        }
        
        self.progressBar_1.snp.makeConstraints { make in
            make.left.equalTo(self.progressBarView)
            make.top.equalTo(self.progressBarView)
            make.bottom.equalTo(self.progressBarView)
            make.width.equalTo(self.progressBarView).dividedBy(4).offset(-SectionCell.PROGRESS_BAR_OFFSET)
        }
        
        self.progressBar_2.snp.makeConstraints { make in
            make.left.equalTo(self.progressBar_1.snp.right).offset(SectionCell.PROGRESS_BAR_OFFSET)
            make.top.equalTo(self.progressBarView)
            make.bottom.equalTo(self.progressBarView)
            make.width.equalTo(self.progressBarView).dividedBy(4).offset(-SectionCell.PROGRESS_BAR_OFFSET)
        }

        self.progressBar_3.snp.makeConstraints { make in
            make.left.equalTo(self.progressBar_2.snp.right).offset(SectionCell.PROGRESS_BAR_OFFSET)
            make.top.equalTo(self.progressBarView)
            make.bottom.equalTo(self.progressBarView)
            make.width.equalTo(self.progressBarView).dividedBy(4).offset(-SectionCell.PROGRESS_BAR_OFFSET)
        }

        self.progressBar_4.snp.makeConstraints { make in
            make.left.equalTo(self.progressBar_3.snp.right).offset(SectionCell.PROGRESS_BAR_OFFSET)
            make.top.equalTo(self.progressBarView)
            make.bottom.equalTo(self.progressBarView)
            make.width.equalTo(self.progressBarView).dividedBy(4).offset(-SectionCell.PROGRESS_BAR_OFFSET)
        }
        
    }
}
