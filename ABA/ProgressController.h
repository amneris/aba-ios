//
//  ProgressController.h
//  ABA
//
//  Created by Jaume Cornadó Panadés on 10/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Blocks.h"
#import "Definitions.h"
#import <Foundation/Foundation.h>

@class ABARealmLevel;
@class ABARealmUser;
@class ABAUnit;
@class ABARealmProgressAction;

@interface ProgressController : NSObject

@property(nonatomic, strong) NSMutableArray *pendingActions;

/**
 Método para recuperar progreso del nivel cada vez que se cambia de nivel o se inicia la aplicaicón */
- (void)getCourseProgress:(CompletionBlock)block;

- (void)recalculateLevelProgress:(ABARealmLevel *)level completionBlock:(CompletionBlock)block;

/**
 Método para recuperar el progreso de cada unidad iniciada por el usuario. Este método se debe llamar al iniciar la aplicación para poder tener sincronizada la información al acceder al dashboard */
- (void)getUnitProgress:(ABAUnit *)unit completionBlock:(CompletionBlock)block;

/**
 Recupera todas las acciones completadas por el usuario, tanto correctas como incorrectas.
 Este método se debe llamar al acceder al contenido de una sección para sincronizar los datos con el servidor y el device local */
- (void)getCompletedActionsFromUser:(ABARealmUser *)user onUnit:(ABAUnit *)unit withSectionType:(kABASectionTypes)sectionType completionBlock:(CompletionBlock)block;

- (void)getCompletedActionsFromUser:(ABARealmUser *)user
                             onUnit:(ABAUnit *)unit
                    completionBlock:(CompletionBlock)block;

- (void)sendProgressActionsWithLimit:(int)limit withCompletionBlock:(CompletionBlock)block;

#pragma mark Static methods

+ (NSString *)getActionStringForSection:(ABASection *)section;

@end
