//
//  ABAEvaluationDataController.h
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Blocks.h"

@class ABAUnit, ABAEvaluationOption, ABAEvaluation;
@interface ABAEvaluationDataController : NSObject

+ (void)parseABAEvaluationSection:(NSArray *)abaEvaluationDataArray onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block;
+ (void)setQuestionEvaluationDone:(ABAEvaluationOption *)option completionBlock:(CompletionBlock)block;
+ (void)setAllQuestionEvaluationNone:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)block;
+ (void)setWrongQuestionEvaluationNone:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)block;

@end
