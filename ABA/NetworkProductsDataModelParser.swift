//
//  ProductsDataModelParser.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/12/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Wrap
import Unbox

struct NetworkProductsParser {
    
    static func parseProducts(_ productsDictionary: Any) throws -> NetworkProducts? {
        if let productsDictionary = productsDictionary as? UnboxableDictionary {
            let user:NetworkProducts = try unbox(dictionary: productsDictionary)
            return user
        }
        
        throw NetworkUserParserError.invalidData
    }
}
