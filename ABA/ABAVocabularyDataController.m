//
//  ABAVocabularyDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAVocabularyDataController.h"
#import <Realm/Realm.h>
#import "ABAUnit.h"
#import "ABAVocabulary.h"

@implementation ABAVocabularyDataController

+ (void)parseABAVocabularySection:(NSArray *)abaVocabularyDataArray onUnit: (ABAUnit*) unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAVocabulary *vocabulary = unit.sectionVocabulary;
    
    [realm transactionWithBlock:^{
        
        if (!vocabulary)
        {
            vocabulary = [[ABAVocabulary alloc] init];
            unit.sectionVocabulary = vocabulary;
        }
        
        for (NSDictionary *abaVocabularyData in abaVocabularyDataArray)
        {
            ABAVocabularyPhrase *phrase = [[ABAVocabularyPhrase alloc] init];
            phrase.audioFile = [abaVocabularyData objectForKey:@"audio"];
            phrase.text = [abaVocabularyData objectForKey:@"text"];
            phrase.translation = [[abaVocabularyData objectForKey:@"translation"] isEqual:[NSNull null]] ? @"" : [abaVocabularyData objectForKey:@"translation"];
            
            phrase.wordType = [abaVocabularyData objectForKey:@"wordType"];
            phrase.page = [abaVocabularyData objectForKey:@"page"];
            
            [vocabulary.content addObject:phrase];
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, vocabulary);
    }
    else
    {
        block(error, nil);
    }
}

@end
