//
//  ABAVideoClass.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAVideoClass.h"
#import "ABAUnit.h"


@implementation ABAVideoClass

-(BOOL)hasContent
{
    if (self.englishSubtitles && self.hdVideoURL && self.sdVideoURL)
    {
        return YES;
    }
    return NO;
}

- (ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionVideoClass == %@", self];
    RLMResults *results = [ABAUnit objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}


@end
