//
//  SectionStatusController.m
//  ABA
//
//  Created by Oriol Vilaró on 16/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "SectionStatusController.h"

#import "ABASection.h"
#import "ABAUnit.h"
#import "DownloadController.h"
#import "UserController.h"
#import "APIManager.h"

// Shepherd plugin
#import "ABAShepherdEvaluationPlugin.h"

// sections
#import "ABAFilm.h"
#import "ABASpeak.h"
#import "ABAWrite.h"
#import "ABAInterpret.h"
#import "ABAVideoClass.h"
#import "ABAExercises.h"
#import "ABAVocabulary.h"
#import "ABAEvaluation.h"

@implementation SectionStatusController

+ (kABASectionActionStatus)getStatusForSection:(ABASection *)section
{
    ABAUnit *unit = [section getUnitFromSection];
    kABASectionTypes sectionType = [section getSectionType];
    __block kABASectionActionStatus returnType = kABASectionActionStatusGranted;

    if ([UserController isUserPremium])
    {
        if ([DownloadController shouldDownloadOfflineModeForUnit:unit] && [[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusNotReachable)
        {
            returnType = kABASectionActionStatusOfflineWithoutData;
        }
#ifdef SHEPHERD_DEBUG
        else if ([ABAShepherdEvaluationPlugin forceUnlock] && sectionType == kABAAssessmentSectionType)
        {
            NSLog(@"Shepherd plugin - Unlocking Evaluation");
            
            returnType = kABASectionActionStatusGranted;
        }
#endif
        else if (!unit.isEvaluationSectionUnlocked && sectionType == kABAAssessmentSectionType)
        {
            returnType = kABASectionActionStatusLockedReasonAssessment;
        }
    }
    else // not premium
    {
        if (([unit getIndex] != 1 && sectionType != kABAVideoClassSectionType))
        {
            returnType = kABASectionActionStatusLockedReasonUserNotPremium;
        }
        else if ([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusNotReachable && [unit getIndex] == 1)
        {
            returnType = kABASectionActionStatusOfflineWithoutData;
        }
#ifdef SHEPHERD_DEBUG
        else if ([ABAShepherdEvaluationPlugin forceUnlock] && sectionType == kABAAssessmentSectionType)
        {
            NSLog(@"Shepherd plugin - Unlocking Evaluation");
            
            returnType = kABASectionActionStatusGranted;
        }
#endif
        else if (!unit.isEvaluationSectionUnlocked && sectionType == kABAAssessmentSectionType)
        {
            returnType = kABASectionActionStatusLockedReasonAssessment;
        }
    }

    return returnType;
}

+ (kABASectionStyleOptions)getCreateStyleOptionsForSection:(ABASection *)section
{
    return ([SectionStatusController getUpdateStyleOptionsForSection:section] | kABASectionStyleAnimatedYes);
}

+ (kABASectionStyleOptions)getUpdateStyleOptionsForSection:(ABASection *)section
{
    ABAUnit *unit = [section getUnitFromSection];
    kABASectionTypes currentSectionType = [ABAUnit getCurrentSectionForUnit:unit];
    kABASectionTypes sectionType = [section getSectionType];
    
    BOOL userIsPremium = [UserController isUserPremium];
    __block kABASectionStyleOptions returnType = kABASectionStyleDefault; // default options, pending status

    if (userIsPremium || [unit getIndex] == 1)
    {
        if (![unit isEvaluationSectionUnlocked] && sectionType == kABAAssessmentSectionType)
        {
            returnType = kABASectionStyleLockEnabled; // locked
        }
        else if ([section.completed boolValue] || [section.progress integerValue] == 100) // completed section
        {
            returnType = kABASectionStyleBgColorDone; // completed section
        }
        else if (currentSectionType == sectionType)
        {
            returnType = kABASectionStyleBgNumberColorCurrent; // actual section
        }
    }
    else // no premium
    {
        if (sectionType == kABAVideoClassSectionType) // videoclass
        {
            if ([section.completed boolValue] || [section.progress integerValue] == 100)
            {
                returnType = kABASectionStyleBgColorDone; // completed section
            }
            else
            {
                returnType = kABASectionStyleBgNumberColorCurrent; // actual section
            }
        }
        else
        {
            returnType = kABASectionStyleLockEnabled; // locked
        }
    }

    return returnType;
}

@end
