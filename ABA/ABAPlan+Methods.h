//
//  ABAPlan+Methods.h
//  ABA
//
//  Created by MBP13 Jesus on 07/01/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "ABAPlan.h"
#import <StoreKit/StoreKit.h>

@interface ABAPlan (Methods)

+ (NSString *)getSubscriptionStringFromDays:(NSInteger)days;
+ (NSString *)getSubscriptionPlanPriceInOneCharge:(SKProduct *)product forDays:(NSInteger)days;
+ (NSString *)getSubscriptionPlanPriceForMonth:(SKProduct *)product forDays:(NSInteger)days;
+ (NSInteger)getProductPeriod:(SKProduct *)product;
+ (NSString *)getProductNameForTrackingPurposes:(NSString *)productIdentifier;

@end
