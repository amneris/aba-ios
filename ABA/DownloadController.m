//
//  DownloadController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/8/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "DownloadController.h"
#import "Utils.h"
#import "ProgressController.h"
#import "AudioController.h"
#import "SectionController.h"
#import "FilmController.h"
#import "SpeakController.h"
#import "WriteController.h"
#import "InterpretController.h"
#import "ExercisesController.h"
#import "VocabularyController.h"
#import "DataController.h"
#import "ImageHelper.h"
#import "APIManager.h"
#import "ABAError.h"
#import "UserDataController.h"


// Definitions
#define kTotalNumberDownloadTasks 8.0f

@interface DownloadController ()

@property (nonatomic, copy) DownloadProgressBlock downloadProgressBlock;
@property (nonatomic, assign) BOOL shouldCancelDownload;
@property (nonatomic, strong) ABAUnit *currentUnit;
@property (nonatomic, assign) NSInteger completedTasks;

@property (nonatomic, strong) SectionController *sectionController;
@property (nonatomic, strong) ProgressController *progressController;
@property (nonatomic, strong) AudioController *audioController;
@property (nonatomic, strong) FilmController *filmController;
@property (nonatomic, strong) ImageHelper *imageHelper;

@end

@implementation DownloadController

+(DownloadController *)controllerWithUnit:(ABAUnit *)unit andDownloadProgressBlock:(DownloadProgressBlock)progressBlock{
    
    DownloadController *controller = [[DownloadController alloc] init];
    controller.downloadProgressBlock = progressBlock;
    controller.currentUnit = unit;
    controller.shouldCancelDownload = NO;
    
    controller.sectionController = [SectionController new];
    controller.progressController = [[ProgressController alloc] init];
    controller.audioController = [[AudioController alloc] init];
    controller.filmController = [[FilmController alloc] init];
    controller.imageHelper = [[ImageHelper alloc] init];
    
    return controller;
}

-(void)startDownload{
    
    NSLog(@"Starting offline download ...");
    [self downloadSectionData];
}

-(void)cancelDownload{
    
    NSLog(@"Cancelling offline download ...");

    _shouldCancelDownload = YES;
    
    [[[APIManager sharedManager] manager].operationQueue cancelAllOperations];
    [[[APIManager sharedManager] progressManager].operationQueue cancelAllOperations];
    [[[APIManager sharedManager] fileManager].operationQueue cancelAllOperations];
}

-(CGFloat)calculateProgress{
    
    return _completedTasks / kTotalNumberDownloadTasks * 100;
}

-(BOOL) checkDownloadResultsWithErrorOrNil:(NSError*)error{
    
    CGFloat currentProgress = [self calculateProgress];
    
    if (_shouldCancelDownload) { // check for cancel flag
        
        NSLog(@"Download cancelled ...");
        
        // download cancelled
        ABAError *error = [ABAError errorWithKey:STRTRAD(@"Download cancelled", @"Download cancelled")];
        _downloadProgressBlock(error, currentProgress, YES);
        
        return NO; // stop downloading
    }

    if (error) { // check for error
        
        NSLog(@"Error on downloading ...");

        // return error
        _downloadProgressBlock(error, currentProgress, NO);
        
        return NO; // stop downloading
    }

    _completedTasks+=1;
    
    if (_completedTasks==kTotalNumberDownloadTasks) { // check for number of tasks
     
        NSLog(@"Download finished successfully ...");

        // download finished successfully
        _downloadProgressBlock(nil, currentProgress, YES);

        return NO; // stop downloading
    }
    
    NSLog(@"continue downloading ...");
    
    // continue downloading
    _downloadProgressBlock(nil, currentProgress, NO);
    
    return YES;
}

#pragma mark - download tasks

-(void)downloadSectionData{
    
    NSLog(@"Starting download section data ...");
    
    __weak typeof(self) weakSelf = self;
    
    [_sectionController getAllSectionsForUnit:_currentUnit contentBlock:^(NSError *error, ABAUnit *object) {
       
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            [weakSelf downloadCompletedActions];
        }
    }];
}

-(void)downloadCompletedActions{
    
    NSLog(@"Starting download completed actions ...");
    
    __weak typeof(self) weakSelf = self;
    
    [_progressController getCompletedActionsFromUser:[UserDataController getCurrentUser] onUnit:_currentUnit completionBlock:^(NSError *error, id object) {
        
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            [weakSelf downloadAudios];
        }
    }];
}

-(void)downloadAudios{
    
    NSLog(@"Starting download audio files ...");
    
    __weak typeof(self) weakSelf = self;

    [_audioController downloadAudios:[weakSelf getAllAudiosForUnit:_currentUnit] withUnit:_currentUnit completionBlock:^(NSError *error) {
        
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {

            [weakSelf downloadVideoFilm];
        }
    }];
}

-(void)downloadVideoFilm{
    
    NSLog(@"Starting download video film ...");
    
    __weak typeof(self) weakSelf = self;

    [_filmController downloadVideo:(ABASection *)_currentUnit.sectionFilm forSectionType:kABAFilmSectionType completionBlock:^(NSError *error, id object) {
        
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            [weakSelf downloadSubtitlesFilm];
        }
    }];
}

-(void)downloadSubtitlesFilm{
    
    NSLog(@"Starting download subtitle film ...");
    
    __weak typeof(self) weakSelf = self;
    
    [_filmController downloadSubtitlesVideo:(ABASection *)_currentUnit.sectionFilm  forSectionType:kABAFilmSectionType completionBlock:^(NSError *error, id object) {
        
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            [weakSelf downloadVideoClass];
        }
    }];
}

-(void)downloadVideoClass{
    
    NSLog(@"Starting download video class ...");
    
    __weak typeof(self) weakSelf = self;
    
    [_filmController downloadVideo:(ABASection *)_currentUnit.sectionVideoClass forSectionType:kABAVideoClassSectionType completionBlock:^(NSError *error, id object) {
        
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            [weakSelf downloadSubtitlesVideoClass];
        }
    }];
}

-(void)downloadSubtitlesVideoClass{
    
    NSLog(@"Starting download subtitle video class ...");
    
    __weak typeof(self) weakSelf = self;
    
    [_filmController downloadSubtitlesVideo:(ABASection *)_currentUnit.sectionVideoClass forSectionType:kABAVideoClassSectionType completionBlock:^(NSError *error, id object) {
        
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            [weakSelf downloadImages];
        }
    }];
}

-(void)downloadImages{
   
    NSLog(@"Starting download images ...");
    
    __weak typeof(self) weakSelf = self;
    
    [_imageHelper downloadImagesForUnit:_currentUnit withBlock:^(NSError *error) {
    
        if ([weakSelf checkDownloadResultsWithErrorOrNil:error]) {
            
            // nothing to do here...
        }
    }];
}

-(NSArray *)getAllAudiosForUnit:(ABAUnit *)unit {
    
    NSMutableArray *allAudios = [NSMutableArray new];
    SpeakController *speakController = [[SpeakController alloc] init];
    WriteController *writeController = [[WriteController alloc] init];
    InterpretController *interpretController = [[InterpretController alloc] init];
    ExercisesController *exercisesController = [[ExercisesController alloc] init];
    VocabularyController *vocabularyController = [[VocabularyController alloc] init];
    
    [allAudios addObjectsFromArray:[speakController getAllAudioIDsForSpeakSection:unit.sectionSpeak]];
    [allAudios addObjectsFromArray:[writeController getAllAudioIDsForWriteSection:unit.sectionWrite]];
    [allAudios addObjectsFromArray:[interpretController getAllAudioIDsForInterpretSection:unit.sectionInterpret]];
    [allAudios addObjectsFromArray:[exercisesController getAllAudioIDsForExercisesSection:unit.sectionExercises]];
    [allAudios addObjectsFromArray:[vocabularyController getAllAudioIDsForVocabularySection:unit.sectionVocabulary]];
    
    // borramos audios repetidos
    NSArray *cleanedArray = [[NSSet setWithArray:allAudios] allObjects];
    
    return cleanedArray;
}

+ (BOOL)shouldDownloadOfflineModeForUnit:(ABAUnit *)unit
{
    if ([unit shouldDownloadSectionData])
    {
        return YES;
    }

    AudioController *audioController = [[AudioController alloc] init];
    SpeakController *speakController = [[SpeakController alloc] init];
    WriteController *writeController = [[WriteController alloc] init];
    InterpretController *interpretController = [[InterpretController alloc] init];
    ExercisesController *exercisesController = [[ExercisesController alloc] init];
    VocabularyController *vocabularyController = [[VocabularyController alloc] init];
    FilmController *filmController = [FilmController new];

    if (![audioController audiosDownloaded:[speakController getAllAudioIDsForSpeakSection:unit.sectionSpeak] withUnit:unit])
    {
        return YES;
    }
    if (![audioController audiosDownloaded:[writeController getAllAudioIDsForWriteSection:unit.sectionWrite] withUnit:unit])
    {
        return YES;
    }
    if (![audioController audiosDownloaded:[interpretController getAllAudioIDsForInterpretSection:unit.sectionInterpret] withUnit:unit])
    {
        return YES;
    }
    if (![audioController audiosDownloaded:[exercisesController getAllAudioIDsForExercisesSection:unit.sectionExercises] withUnit:unit])
    {
        return YES;
    }
    if (![audioController audiosDownloaded:[vocabularyController getAllAudioIDsForVocabularySection:unit.sectionVocabulary] withUnit:unit])
    {
        return YES;
    }
    if (![filmController videoAndSubtituleDownloaded:(ABASection *)unit.sectionFilm forSectionType:kABAFilmSectionType])
    {
        return YES;
    }
    if (![filmController videoAndSubtituleDownloaded:(ABASection *)unit.sectionVideoClass forSectionType:kABAVideoClassSectionType])
    {
        return YES;
    }

    return NO;
}

#pragma mark - Cleaning up

+ (void)removeDownloadedFiles
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    for (int unitIndex = 1; unitIndex <= 144; unitIndex++) {
        NSError * removeError = nil;
        NSString *unitFolder = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"unit%d", unitIndex]];
        [[NSFileManager defaultManager] removeItemAtPath:unitFolder error:&removeError];
        
        if (!removeError) {
            NSLog(@"Removing downloaded files for unit %d", unitIndex);
        }
    }
}

@end
