//
//  PlansViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TrackingDefinitions.h"
#import "PlansViewOutput.h"
#import "PlansViewInput.h"

@class ABAUnit;

@interface PlansViewController : UIViewController <PlansViewInput>

@property BOOL shouldShowBackButton;
@property PlansViewControllerOriginType originController;
@property (nonatomic, strong) id<PlansViewOutput> presenter;

@end
