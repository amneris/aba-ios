//
//  RegisterConfigurator.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

class RegisterConfigurator {

    class func createRegisterViperStack() -> RegisterFormViewController {
        let registerFormVC = RegisterFormViewController()
        configure(registerFormVC)

        return registerFormVC
    }
}

extension RegisterConfigurator {

    fileprivate class func configureModuleForViewInput(_ viewInput: UIViewController) {
        if let viewController = viewInput as? RegisterFormViewController {
            configure(viewController)
        }
        else {
            print("RegisterConfigurator: Could not configure")
        }
    }

    fileprivate class func configure(_ viewController: RegisterFormViewController) {
        let repository = RegisterRepository()
        let presenter = RegisterPresenter(tracker: Tracker.sharedInstance)
        let interactor = RegisterInteractor(repository: repository)
        let router = SessionRouter(vc: viewController)

        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.facebookInteractor = FacebookInteractor(tracker: LegacyTrackingManager.shared())
        viewController.presenter = presenter
    }

}
