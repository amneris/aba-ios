//
//  ChangeNameViewController.m
//  ABA
//
//  Created by Jordi Mele on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ChangeNameViewController.h"
#import "UIViewController+ABA.h"
#import "Utils.h"

@interface ChangeNameViewController ()

@end

@implementation ChangeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButtonNavBar];
    [self addTitleNavbar:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"changeNameTitleKey", @"Modificar nombre")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
