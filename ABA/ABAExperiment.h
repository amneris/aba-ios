//
//  ABAExperiment.h
//  ABA
//
//  Created by Oriol Vilaró on 5/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABAExperiment : NSObject <NSCoding>

@property NSString *experimentIdentifier;
@property NSString *experimentVariationIdentifier;
@property NSString *userExperimentVariationId;

@end
