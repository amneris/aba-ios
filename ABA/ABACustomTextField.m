//
//  ABACustomTextField.m
//  ABA
//
//  Created by Jordi Mele on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABACustomTextField.h"
#import "Resources.h"
#import "Utils.h"

@implementation ABACustomTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    self.textColor = ABA_COLOR_REG_TEXTFIELD;
    
    [self.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
    [self.layer setBorderWidth:1];
    
    
    if(IS_IPAD) {
        self.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:20];
        [self.layer setCornerRadius:6.0f];
    }
    else {
        self.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:16];
        [self.layer setCornerRadius:3.0f];
    }
    
    self.clipsToBounds = YES;
    
    //  Ponemos un pequeño margen interno en el textfield
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 0)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void) setNormalBorderColor {
    
    [self.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
    
}

-(void) setErrorBorderColor {
    
    [self.layer setBorderColor:ABA_COLOR_RED_ERROR.CGColor];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
