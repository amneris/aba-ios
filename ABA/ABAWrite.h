//
//  ABAWrite.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASection+Methods.h"
#import "ABAWriteDialog.h"

@class ABAUnit;

@interface ABAWrite : ABASection

@property RLMArray<ABAWriteDialog *><ABAWriteDialog> *content;
@property (readonly) ABAUnit *unit;

- (BOOL)hasContent;

@end
