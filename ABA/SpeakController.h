//
//  SpeakController.h
//  ABA
//
//  Created by Jordi Mele on 3/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"

@class ABASpeakDialog, ABASpeakPhrase, ABASpeak;

@interface SpeakController : SectionController

-(RLMArray*) getDialogDataSourceFromSpeak: (ABASpeak*) speak;

-(NSArray*) getPhrasesFromSpeak: (ABASpeak*) speak;

-(ABASpeakDialog*) getCurrentDialogFromSection: (ABASpeak*) speak;

-(ABASpeakPhrase*) getCurrentPhraseFromSection: (ABASpeak*) speak;

-(ABASpeakPhrase *) getCurrentSamplePhraseFromSection: (ABASpeak*) speak;

-(BOOL) isDialogCompleted: (ABASpeakDialog*) dialog ;

-(NSArray *) getArrayPhraseCurrenDialogFromSection:(ABASpeak *)speak;

-(CGFloat) getTotalPhraseCompletedDialogForSection:(ABASpeakDialog*) dialog;

-(NSArray *)getAllAudioIDsForSpeakSection: (ABASpeak *)speak;

-(void)sendAllPhrasesDoneForSpeakSection: (ABASpeak *)speak completionBlock:(CompletionBlock)block;

-(ABASpeakPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onSpeak: (ABASpeak*) speak;

@end
