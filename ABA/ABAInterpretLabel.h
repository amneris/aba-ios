//
//  ABAInterpretLabel.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@class ABAInterpretPhrase;

@interface ABAInterpretLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property (nonatomic, assign) ABAInterpretPhrase *phrase;
@property kABAInterpretRowStatus status;

-(void)setupWithABAInterpretPhrase: (ABAInterpretPhrase *)phrase andStatus:(kABAInterpretRowStatus)type;

@end
