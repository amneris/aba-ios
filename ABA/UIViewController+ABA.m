//
//  UIViewController+ABA.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "UIViewController+ABA.h"
#import "RESideMenu.h"
#import "Resources.h"
#import "Utils.h"
#import "PureLayout.h"

@implementation UIViewController (ABA)

- (void)addMenuButton {

    UIImage *image = [UIImage imageNamed:@"menuBurger"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(showMenu:)forControlEvents:UIControlEventTouchUpInside];

    [button setImage:image forState:UIControlStateNormal];

	button.frame = CGRectMake(0 ,0,IS_IPAD?50:26,IS_IPAD?44:19);

    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:button];

    self.navigationItem.leftBarButtonItem = menuButton;
}

- (void)addBackButtonNavBar {

    UIImage *image = [UIImage imageNamed:@"back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(doback:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,30,30);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];

    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)showMenu:(id)sender {
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)doback:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"showCompletedAlertIfNeeded" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addToolbarWithBackButtonAndTitle:(NSString *)title
{
    UIView *view = [UIView newAutoLayoutView];
    [view setBackgroundColor:ABA_COLOR_DARKGREY];
    [self.view addSubview:view];

    [view autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
    [view autoSetDimension:ALDimensionHeight toSize:44];
    [view autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view];
    [view autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];

    UIImage *image = [UIImage imageNamed:@"back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [view addSubview:button];
    [button addTarget:self action:@selector(doback:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];

    [button autoSetDimension:ALDimensionHeight toSize:30];
    [button autoSetDimension:ALDimensionWidth toSize:30];
    [button autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
    [button autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:7];

    UILabel* label = [UILabel newAutoLayoutView];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    label.font = [UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?16:14];
    [label setText:title];
    [label setTextAlignment:NSTextAlignmentCenter];
    [view addSubview:label];
    
    [label autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:view];
    [label autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:view];
    [label autoCenterInSuperview];
}

-(void)addToolbarWithTitle: (NSString *)title
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.tintColor = [UIColor whiteColor];
    toolbar.barTintColor = ABA_COLOR_DARKGREY;
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    label.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?16:14];
    [label setText:[title uppercaseString]];
    [label setTextAlignment:NSTextAlignmentCenter];
    UIBarButtonItem *labeltext= [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    NSArray *items = [NSArray arrayWithObjects:flexibleSpace, labeltext, flexibleSpace, nil];
    toolbar.items = items;
    
    toolbar.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:toolbar];
    
    [toolbar autoSetDimension:ALDimensionHeight toSize:44];
    [toolbar autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view];
    [toolbar autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    [toolbar autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
}

-(void)addMenuButtonWithTitle: (NSString *)title
                  andSubtitle: (NSString *)subtitle
{
    self.navigationItem.leftBarButtonItems = [self getItemsWithMenuButtonAndTitle:title andSubtitle:subtitle];
    self.navigationItem.title = @"";
}

-(void)addMenuButtonWithTitleBlue: (NSString *)title
{
    self.navigationItem.leftBarButtonItems = [self getItemsWithMenuButtonAndTitleBlue:title];
    self.navigationItem.title = @"";
}

-(void)addMenuButtonWithTitleWhite: (NSString *)title
{
    self.navigationItem.leftBarButtonItems = [self getItemsWithMenuButtonAndTitleWhite:title];
    self.navigationItem.title = @"";
}

-(void)addBackButtonWithTitleBlue: (NSString *)title
{
    self.navigationItem.leftBarButtonItems = [self getItemsWithBackButtonAndTitleBlue:title];
    self.navigationItem.title = @"";
}

-(void)addToolbarAndMenuButtonWithTitle: (NSString *)title
                            andSubtitle: (NSString *)subtitle
{
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.translatesAutoresizingMaskIntoConstraints = NO;

    toolbar.tintColor = [UIColor whiteColor];
    toolbar.barTintColor = ABA_COLOR_DARKGREY;
    
    toolbar.items = [self getItemsWithMenuButtonAndTitle:title andSubtitle:subtitle];;
    [self.view addSubview:toolbar];
    
    [toolbar autoSetDimension:ALDimensionHeight toSize:44];
    [toolbar autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.view];
    [toolbar autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
    [toolbar autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self.view];
}

-(NSArray *)getItemsWithMenuButtonAndTitle: (NSString *)title
                               andSubtitle: (NSString *)subtitle
{
    UIImage *image = [UIImage imageNamed:@"menuBurger"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(showMenu:)forControlEvents:UIControlEventTouchUpInside];
    [button setImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,IS_IPAD?50:26,IS_IPAD?44:19);
	
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 2;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    NSString *line0 = title;
    NSString *line1 = subtitle;
    NSString *str = [NSString stringWithFormat:@"%@\n%@", line0, line1];
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?14:12]
                      range:[str rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:ABA_COLOR_LIGHTBLUE
                      range:[str rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?14:12]
                      range:[str rangeOfString:line1]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:[str rangeOfString:line1]];
    
    [label setAttributedText:mutAttStr.copy];
    
    
    UIBarButtonItem *labeltext= [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:menuButton, labeltext, flexibleSpace, nil];
    
    return items;
}

-(void)addTitleNavbar:(NSString *)title andSubtitle:(NSString *)subtitle {
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 300, 10)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = ABA_COLOR_LIGHTBLUE;
    titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:12];
    titleLabel.text = title;
//    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 15)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?16:14];
    subTitleLabel.text = subtitle;
//    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, 44)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    self.navigationItem.titleView = twoLineTitleView;
}

-(void)addTitleNavbar:(NSString *)title andSubtitle:(NSString *)subtitle andProgress:(NSString *)progress
{
	UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height), 44)];
	self.navigationItem.titleView = twoLineTitleView;
	
//	    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 300, 10)];
	UILabel *titleLabel = [UILabel newAutoLayoutView];
	[twoLineTitleView addSubview:titleLabel];

	[titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
	[titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8.0f];
	[titleLabel autoSetDimension:ALDimensionWidth toSize:300.0f];
	[titleLabel autoSetDimension:ALDimensionHeight toSize:10.0f];

	titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = ABA_COLOR_LIGHTBLUE;
    titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:12];
	titleLabel.text = title;
	
//    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 15)];
	UILabel *subTitleLabel = [UILabel newAutoLayoutView];

	[twoLineTitleView addSubview:subTitleLabel];

	[subTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
	[subTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20.0f];
	[subTitleLabel autoSetDimension:ALDimensionWidth toSize:300.0f];
	[subTitleLabel autoSetDimension:ALDimensionHeight toSize:15.0f];

    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:14];
    subTitleLabel.text = subtitle;
	subTitleLabel.text = [NSString stringWithFormat:@"%@ %@", subtitle, progress];
    [self addBackButtonNavBar];
}

-(void)addTitleNavbarSection:(NSString *)title andSubtitle:(NSString *)subtitle {
	
	UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height), 44)];
	self.navigationItem.titleView = twoLineTitleView;

//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 300, 10)];
	UILabel *titleLabel = [UILabel newAutoLayoutView];
	[twoLineTitleView addSubview:titleLabel];

	[titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
	[titleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8.0f];
	[titleLabel autoSetDimension:ALDimensionWidth toSize:300.0f];
	[titleLabel autoSetDimension:ALDimensionHeight toSize:10.0f];

    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:14];
    titleLabel.text = title;
    
//    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 15)];
	UILabel *subTitleLabel = [UILabel newAutoLayoutView];
	[twoLineTitleView addSubview:subTitleLabel];

	[subTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10.0f];
	[subTitleLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20.0f];
	[subTitleLabel autoSetDimension:ALDimensionWidth toSize:300.0f];
	[subTitleLabel autoSetDimension:ALDimensionHeight toSize:15.0f];

    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = ABA_COLOR_LIGHTBLUE;
    subTitleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:16];
    subTitleLabel.text = subtitle;
}

-(NSArray *)getItemsWithMenuButtonAndTitleBlue: (NSString *)title
{
    UIImage *image = [UIImage imageNamed:@"menuBurger"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(showMenu:)forControlEvents:UIControlEventTouchUpInside];
    [button setImage:image forState:UIControlStateNormal];

    button.frame = CGRectMake(0 ,0,IS_IPAD?50:26,IS_IPAD?44:19);

    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    NSString *line0 = title;
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:line0];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?16:14]
                      range:[line0 rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:ABA_COLOR_LIGHTBLUE
                      range:[line0 rangeOfString:line0]];
    
    [label setAttributedText:mutAttStr.copy];
    
    
    UIBarButtonItem *labeltext= [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:menuButton, labeltext, flexibleSpace, nil];
    
    return items;
}

-(NSArray *)getItemsWithMenuButtonAndTitleWhite: (NSString *)title
{
    UIImage *image = [UIImage imageNamed:@"menuBurger"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(showMenu:)forControlEvents:UIControlEventTouchUpInside];
    [button setImage:image forState:UIControlStateNormal];

	button.frame = CGRectMake(0 ,0,IS_IPAD?50:26,IS_IPAD?44:19);

    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    NSString *line0 = title;
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:line0];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?16:14]
                      range:[line0 rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:[line0 rangeOfString:line0]];
    
    [label setAttributedText:mutAttStr.copy];
    
    
    UIBarButtonItem *labeltext= [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 20.0f; // or whatever you want

    NSArray *items = [NSArray arrayWithObjects:menuButton, fixedItem, labeltext, nil];
    
    return items;
}

-(NSArray *)getItemsWithBackButtonAndTitleBlue: (NSString *)title
{
    UIImage *image = [UIImage imageNamed:@"back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(doback:)forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,30,30);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    NSString *line0 = title;
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:line0];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?16:14]
                      range:[line0 rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:ABA_COLOR_LIGHTBLUE
                      range:[line0 rangeOfString:line0]];
    
    [label setAttributedText:mutAttStr.copy];
    
    
    UIBarButtonItem *labeltext= [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:backButton, labeltext, flexibleSpace, nil];
    
    return items;
}

-(void)addBackButtonWithTitle: (NSString *)title
                  andSubtitle: (NSString *)subtitle
{
    self.navigationItem.leftBarButtonItems = [self getItemsWithBackButtonAndTitle:title andSubtitle:subtitle withTarget:self withAction:@selector(doback:)];
    self.navigationItem.title = @"";
}

- (void)addBackButtonWithTitle:(NSString *)title andSubtitle:(NSString *)subtitle withTarget:(id)target withAction:(SEL)action
{
    self.navigationItem.leftBarButtonItems = [self getItemsWithBackButtonAndTitle:title andSubtitle:subtitle withTarget:target withAction:action];
    self.navigationItem.title = @"";
}

-(NSArray *)getItemsWithBackButtonAndTitle: (NSString *)title
                               andSubtitle: (NSString *)subtitle
                                withTarget:(id)target
                                withAction:(SEL)action
{
    UIImage *image = [UIImage imageNamed:@"back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,30,30);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 2;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentLeft];
    
    NSString *line0 = title;
    NSString *line1 = subtitle;
    NSString *str = ![line1 isEqualToString:@""] ? [NSString stringWithFormat:@"%@\n%@", line0, line1] : line0;
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:16]
                      range:[str rangeOfString:line1]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:ABA_COLOR_LIGHTBLUE
                      range:[str rangeOfString:line1]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:14]
                      range:[str rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:[str rangeOfString:line0]];
    
    [label setAttributedText:mutAttStr.copy];
    
    
    UIBarButtonItem *labeltext= [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *items = [NSArray arrayWithObjects:backButton, labeltext, flexibleSpace, nil];
    
    return items;
}



@end
