//
//  ListenInterpretCollectionViewCell.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "InterpretConversationCollectionViewCell.h"
#import "ABAInterpretPhrase.h"
#import "ABAInterpretRole.h"
#import "ABAInterpretLabel.h"
#import "ABAInterpret.h"
#import "InterpretController.h"
#import "ABAUnit.h"
#import "Utils.h"
#import "ImageHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define PADDING_LABEL 15

@implementation InterpretConversationCollectionViewCell

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if((self = [super initWithCoder:aDecoder]))
    {
    }
    return self;
}

-(void)setupWithABAInterpretPhrase: (ABAInterpretPhrase *)phrase
                      withCellType:(kABAInterpretRowType)type
                           andRole:(ABAInterpretRole *)role
                  andCurrentPhrase:(ABAInterpretPhrase *)currentPhrase
{
    _role = role;
    
    if([phrase.role isEqualToObject:_role])
    {
        _characterName.text = STRTRAD(@"interpretYouKey", @"Tu");
    }
    else
    {
        _characterName.text = phrase.role.name;
    }
    
    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:phrase.role.imageUrl];
    NSURL *imageUrl = [NSURL URLWithString:urlString];
    
    [_characterImage sd_setImageWithURL:imageUrl];
    
    CALayer *imageLayer = _characterImage.layer;
    [imageLayer setCornerRadius:(_characterImage.frame.size.width/2)];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    if([currentPhrase isEqualToObject:phrase])
    {
        _status = kABAInterpretRowStatusCurrent;
    }
    else
    {
        _status = kABAInterpretRowStatusNormal;
    }
    
    [_dialogLabel setupWithABAInterpretPhrase:phrase andStatus:_status];
}

@end
