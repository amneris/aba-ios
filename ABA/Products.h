//
//  Products.h
//  ABA
//
//  Created by MBP13 Jesus on 14/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

#import <StoreKit/StoreKit.h>

@protocol SelligentSubscriptionProduct
@property (nonatomic, readonly, copy) NSString * _Nullable originalIdentifier;
@property (nonatomic, readonly, copy) NSString * _Nullable discountIdentifier;
@property (nonatomic, readonly, copy) NSString * _Nullable days;
@property (nonatomic, readonly, copy) NSString * _Nullable is2x1;
@end

@protocol SelligentProducts
@property (nonatomic, readonly, copy) NSString * _Nullable text;
@property (nonatomic, readonly, copy) NSArray<id <SelligentSubscriptionProduct>> * _Nullable products;
@end

@protocol PlansPageSubscriptionProduct
@property (nonatomic, copy) NSString * _Nullable originalIdentifier;
@property (nonatomic, copy) NSString * _Nullable discountIdentifier;
@property (nonatomic, assign) NSInteger days;
@property (nonatomic, assign) BOOL is2x1;
@property (nonatomic, strong) SKProduct * _Nullable productWithDiscount;
@property (nonatomic, strong) SKProduct * _Nullable productWithOriginalPrice;
@end


@protocol PlansPageProducts
@property (nonatomic, readonly, copy) NSString * _Nullable text;
@property (nonatomic, readonly, copy) NSArray<id <PlansPageSubscriptionProduct>> * _Nullable products;
@end
