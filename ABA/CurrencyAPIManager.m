//
//  CurrencyAPIManager.m
//  ABA
//
//  Created by MBP13 Jesus on 11/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "AFNetworking.h"

#import "CurrencyAPIManager.h"
#import "CurrencyManagerResponseSerializer.h"
#import "CurrencyCacheManager.h"

#define kURLBase @"http://query.yahooapis.com/v1/public/yql?q=select Name, Rate from yahoo.finance.xchange where pair in (%@)&env=store://datatables.org/alltableswithkeys"

@implementation CurrencyAPIManager

+ (void)loadCurrencyWithCurrencyCodeArray:(NSArray *)arrayOfCurrencyCodes andTargetCurrency:(NSString *)targetCurrency
{
    // Creating url and replacing <spaces> with %20 due to AFNetworking does not handle the created url properly (it's very strange)
    NSString *urlWithCurrencies = [NSString stringWithFormat:kURLBase, [self currencyPairsURLFormatWithCurrencies:arrayOfCurrencyCodes andTargetCurrency:targetCurrency]];
    NSString *escapedString = [urlWithCurrencies stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    AFHTTPRequestOperationManager * requestManager = [[AFHTTPRequestOperationManager alloc] init];
    requestManager.responseSerializer = [[CurrencyManagerResponseSerializer alloc] init];
    [requestManager GET:escapedString
             parameters:nil
                success:^(AFHTTPRequestOperation *operation, id responseObject) {

                    // Storing the result if it's an array
                    if ([responseObject isKindOfClass:[NSArray class]])
                    {
                        [CurrencyCacheManager storeCurrenciesInCache:responseObject];
                    }
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Currency update failed");
                }];
}

#pragma mark - Private methods

/**
 It creates an array of currency pairs.Iit creates a pair with USD for each currency defined.
 */
+ (NSArray *)currenciesPairsArrayWithCurrencies:(NSArray *)arrayOfCurrencyCodes andTargetCurrency:(NSString *)targetCurrency
{
    NSMutableArray * arrayOfCurrencyPairs = [NSMutableArray array];
    for (NSString * currencyCode in arrayOfCurrencyCodes)
    {
        NSString * pair = [targetCurrency stringByAppendingString:currencyCode];
        [arrayOfCurrencyPairs addObject:pair];
    }
    
    return arrayOfCurrencyPairs;
}

/**
 It converts the array of pairs into an string to use it within the url request
 */
+ (NSString *)currencyPairsURLFormatWithCurrencies:(NSArray *)arrayOfCurrencyCodes andTargetCurrency:(NSString *)targetCurrency
{
    NSString * startQuotes = @"'";
    NSString * concatenatedCurrencyPairs = [[self currenciesPairsArrayWithCurrencies:arrayOfCurrencyCodes
                                                                   andTargetCurrency:targetCurrency] componentsJoinedByString:@"','"];
    NSString * endQuotes = @"'";
    
    return [startQuotes stringByAppendingString:[concatenatedCurrencyPairs stringByAppendingString:endQuotes]];
}

@end
