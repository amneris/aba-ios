//
//  ABAWriteDialog.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAPhrase.h"

@interface ABAWriteDialog : RLMObject

@property NSString *role;
@property RLMArray<ABAPhrase *><ABAPhrase> *dialog;

@end

RLM_ARRAY_TYPE(ABAWriteDialog)