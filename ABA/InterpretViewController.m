//
//  InterpretSelectionViewController.m
//  ABA
//
//  Created by Marc Guell on 24/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAAlert.h"
#import "ABAInterpret.h"
#import "ABAInterpretPhrase.h"
#import "ABAInterpretRole.h"
#import "ABANavigationController.h"
#import "ABATeacherTipView.h"
#import "ABAUnit.h"
#import "APIManager.h"
#import "AudioController.h"
#import "DataController.h"
#import "Definitions.h"
#import "ImageHelper.h"
#import "InterpretCollectionViewCell.h"
#import "InterpretController.h"
#import "InterpretConversationViewController.h"
#import "InterpretViewController.h"
#import "LegacyTrackingManager.h"
#import "Resources.h"
#import "SDVersion.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "ABA-Swift.h"

@interface InterpretViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, InterpretCollectionViewCellDelegate>

@property(weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(weak, nonatomic) IBOutlet UILabel *titleChooseCharacterLabel;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopSpaceConstraint;

@property InterpretController *interpretController;
@property AudioController *audioController;

@property NSMutableArray *completedRoles;
@property NSMutableArray *incompletedRoles;

@property BOOL mustShowCompletedAlert;

@property ABATeacherTipView *abaTeacher;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *chooseLabelTopConstraint;

@end

@implementation InterpretViewController
@dynamic section;

- (id)initWithCoder:(NSCoder *)aDecoder
{

    if ((self = [super initWithCoder:aDecoder]))
    {
        _interpretController = [[InterpretController alloc] init];
        _audioController = [[AudioController alloc] init];

        _mustShowCompletedAlert = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [DataController saveProgressActionForSection:self.section andUnit:[self.section getUnitFromSection] andPhrases:nil errorText:nil isListen:NO isHelp:NO completionBlock:^(NSError *error, id object){

    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self updateCompletedRoles];

    [self setupView];
    [self setupTeacherView];

    [self addBackButtonWithTitle:STRTRAD(@"interpretKey", @"Interpreta") andSubtitle:[_interpretController getPercentageForSection:self.section]];

    //
    // We load the background image
    //
    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:self.section.unit.filmImageInactiveUrl];
    NSURL *imageUrl = [NSURL URLWithString:urlString];

    [_backgroundImage sd_setImageWithURL:imageUrl];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showCompletedAlertIfNeeded)
                                                 name:@"showCompletedAlertIfNeeded"
                                               object:nil];

    [[LegacyTrackingManager sharedManager] trackPageView:@"interpretsection"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupTeacherView
{
    if ([self.view.subviews containsObject:_abaTeacher])
    {
        [_abaTeacher removeFromSuperview];
    }

    _abaTeacher = [[ABATeacherTipView alloc] initTeacherWithText:@"" withImage:@"teacher" withShadow:NO];

    if ([_completedRoles count] == 0)
    {
        NSString *str = STRTRAD(@"interpretTipKey", @"En esta sección interpretarás cada uno de los personajes del video inicial para mejorar tu fluidez. ¡Es como si hicieras teatro en inglés!");

        NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];

        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:2.0f];
        [mutAttStr addAttribute:NSParagraphStyleAttributeName
                          value:style
                          range:NSMakeRange(0, str.length)];

        [_abaTeacher setAttributedText:mutAttStr.copy];
    }
    else if ([_completedRoles count] < [self.section.roles count])
    {
        NSString *txt0 = STRTRAD(@"congratsNextInterpret1Key", @"¡Felicidades por tu interpretación!");

        NSString *txt1 = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"congratsNextInterpret2Key", @"No te quedes a medias, y prueba a interpretar a"), ((ABAInterpretRole *)[_incompletedRoles objectAtIndex:0]).name];

        NSString *str = [NSString stringWithFormat:@"%@\n%@", txt0, txt1];

        NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
        [mutAttStr addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:14]
                          range:[str rangeOfString:txt0]];

        [mutAttStr addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:ABA_MUSEO_SANS_300 size:14]
                          range:[str rangeOfString:txt1]];

        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:4.0f];
        [mutAttStr addAttribute:NSParagraphStyleAttributeName
                          value:style
                          range:NSMakeRange(0, str.length)];

        [_abaTeacher setAttributedText:mutAttStr.copy];
    }
    else // All roles are completed
    {
        NSString *txt0 = STRTRAD(@"congratsNextInterpret1Key", @"¡Felicidades por tu interpretación!");

        NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:txt0];
        [mutAttStr addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:14]
                          range:[txt0 rangeOfString:txt0]];

        [_abaTeacher setAttributedText:mutAttStr.copy];
    }

    if ([[_interpretController getPercentageForSection:self.section] isEqualToString:@"0%"])
    {
        [self.view addSubview:_abaTeacher];
        [_abaTeacher setConstraintsAttributedText];
        // We update the constraint
        _chooseLabelTopConstraint.constant = _abaTeacher.frame.size.height;
    }
    else
    {
        _chooseLabelTopConstraint.constant = _abaTeacher.frame.size.height * 0.5;
    }
}

- (void)showCompletedAlertIfNeeded
{
    if (_mustShowCompletedAlert)
    {
        [self.delegate showAlertCompletedUnit:kABAInterpretSectionType];
    }
}

- (void)updateCompletedRoles
{
    NSMutableArray *previousIncompleteRoles = [_incompletedRoles copy];

    _completedRoles = [[NSMutableArray alloc] init];
    _incompletedRoles = [[NSMutableArray alloc] init];

    for (ABAInterpretRole *role in self.section.roles)
    {
        if ([role.completed isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            [_completedRoles addObject:role];
        }
        else
        {
            [_incompletedRoles addObject:role];
        }
    }

    if ([previousIncompleteRoles count] > 0 &&
        [_incompletedRoles count] == 0)
    {
        _mustShowCompletedAlert = YES;
    }
}

- (void)setupView
{
    if ([_completedRoles count] == 0)
    {
        [self showTitleChooseLabelWithText:[STRTRAD(@"chooseCharacterKey", @"Escoge tu personaje") uppercaseString]];
    }
    else if ([_completedRoles count] < [self.section.roles count])
    {
        [self showTitleChooseLabelWithText:[STRTRAD(@"chooseNextCharacterKey", @"Interpreta al siguiente personaje") uppercaseString]];
    }
    else // All roles are completed
    {
        [self hideTitleChooseLabel];
    }
}

- (void)showTitleChooseLabelWithText:(NSString *)text
{
    _titleChooseCharacterLabel.text = text;
    _collectionViewTopSpaceConstraint.constant = -1;
}

- (void)hideTitleChooseLabel
{
    _titleChooseCharacterLabel.text = @"";
    _collectionViewTopSpaceConstraint.constant = -40;
}

- (void)retryRole:(ABAInterpretRole *)role
{
    //[self eraseProgressAndRetryForRole:role];

    InterpretConversationViewController *interpretConversationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InterpretConversationViewController"];

    [interpretConversationVC setInterpret:self.section];
    [interpretConversationVC setCurrentRole:role];

    [interpretConversationVC setCurrentPhrase:[self.section.dialog objectAtIndex:0]];
    [interpretConversationVC setPreviousVC:self];

    ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime = YES;
    [self.navigationController pushViewController:interpretConversationVC animated:YES];
    
    [CoolaDataTrackingManager trackInterpretationStarted:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaInterpret];
}

- (void)listenRole:(ABAInterpretRole *)role
{
    [self pushToInterpretConversationListenAllVCforRole:role];
}

- (void)pushToInterpretConversationListenAllVCforRole:(ABAInterpretRole *)role
{
    InterpretConversationViewController *interpretConversationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InterpretConversationViewController"];

    [interpretConversationVC setInterpret:self.section];
    [interpretConversationVC setCurrentRole:role];
    [interpretConversationVC setType:kABAInterpretConversationTypeListenAll];
    [interpretConversationVC setCurrentPhrase:[self.section.dialog objectAtIndex:0]];

    [interpretConversationVC setPreviousVC:self];

    ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime = YES;
    [self.navigationController pushViewController:interpretConversationVC animated:YES];
}

- (void)eraseProgressAndRetryForRole:(ABAInterpretRole *)role
{
    __weak typeof(self) weakSelf = self;

    [_interpretController eraseProgressForRole:role
                           forInterpretSection:self.section
                               completionBlock:^(NSError *error, id object) {
                                   weakSelf.section = (ABAInterpret *)object;

                                   [weakSelf pushToInterpretConversationVCforRole:role];
                               }];
}

- (void)chooseCharacterWithRole:(ABAInterpretRole *)role
{
    [self pushToInterpretConversationVCforRole:role];
}

- (void)pushToInterpretConversationVCforRole:(ABAInterpretRole *)role
{
    InterpretConversationViewController *interpretConversationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InterpretConversationViewController"];

    [interpretConversationVC setInterpret:self.section];
    [interpretConversationVC setCurrentRole:role];

    [interpretConversationVC setCurrentPhrase:[_interpretController getCurrentPhraseForRole:role forInterpretSection:self.section]];
    [interpretConversationVC setPreviousVC:self];

    ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime = YES;
    [self.navigationController pushViewController:interpretConversationVC animated:YES];
    
    [CoolaDataTrackingManager trackInterpretationStarted:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaInterpret];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    NSInteger items = 0;

    if (view == _collectionView)
    {
        items = [self.section.roles count];
    }

    return items;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (cv == _collectionView)
    {
        InterpretCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:IS_IPAD ? @"RoleInterpretCellId_iPad" : @"RoleInterpretCellId" forIndexPath:indexPath];

        // I've just found a fucking UICollectionView & UITableView bug.
        // More info: http://stackoverflow.com/questions/19132908/auto-layout-constraints-issue-on-ios7-in-uitableviewcell
        cell.contentView.frame = cell.bounds;
        cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;

        cell.delegate = self;

        [cell setupWithABAInterpretRole:[self.section.roles objectAtIndex:indexPath.item]];

        [cell layoutSubviews];

        return cell;
    }

    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self chooseCharacterWithRole:[self.section.roles objectAtIndex:indexPath.item]];

    return;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ABAInterpretRole *role = [self.section.roles objectAtIndex:indexPath.item];

    if ([role.completed isEqualToNumber:[NSNumber numberWithInt:0]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    CGSize size = CGSizeMake(self.view.bounds.size.width, [self getCellHeight]);

    return size;
}

- (CGFloat)getCellHeight
{
    if ([SDVersion deviceSize] == Screen3Dot5inch)
    {
        return 135;
    }
    else if ([SDVersion deviceSize] == Screen4inch)

    {
        return 175;
    }
    else if ([SDVersion deviceSize] == Screen4Dot7inch)
    {
        return 220;
    }
    else //if([SDiPhoneVersion deviceSize] == iPhone55inch)
    {
        return 250;
    }
}

- (NSString *)change3xIfNeededWithUrlString:(NSString *)urlString
{
    if ([SDVersion deviceSize] == Screen5Dot5inch)
    {
        return [urlString stringByReplacingOccurrencesOfString:@"@2x"
                                                    withString:@"@3x"];
    }
    else
    {
        return urlString;
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

@end
