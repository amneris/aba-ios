//
//  CooladataTrackingManager-Study.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/05/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

extension CoolaDataTrackingManager {

    // MARK: Media

    class func trackWatchedFilm(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("WATCHED_FILM", properties: eventProperties)
    }

    class func trackListenedToAudio(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType, exerciseId: String) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        eventProperties["exercise_id"] = exerciseId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("LISTENED_AUDIO", properties: eventProperties)
    }

    class func trackRecordedAudio(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType, exerciseId: String) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        eventProperties["exercise_id"] = exerciseId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("RECORDED_AUDIO", properties: eventProperties)
    }

    class func trackComparedAudio(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType, exerciseId: String) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        eventProperties["exercise_id"] = exerciseId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("COMPARED_AUDIO", properties: eventProperties)
    }

    // MARK: Texts

    class func trackVerifiedText(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType, exerciseId: String, isResultCorrect: Bool, writtenText: String) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        eventProperties["exercise_id"] = exerciseId as AnyObject?
        eventProperties["result"] = (isResultCorrect) ? "CORRECT" as AnyObject? : "INCORRECT" as AnyObject?
        eventProperties["text_written"] = writtenText as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("VERIFIED_TEXT", properties: eventProperties)
    }

    class func trackTextSuggestion(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType, exerciseId: String) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        eventProperties["exercise_id"] = exerciseId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("GOT_TEXT_SUGGESTION", properties: eventProperties)
    }
    
    // MARK: Evaluation
    
    class func trackEvaluation(_ userId: String, levelId: String, unitId: String, responses: String, score: String) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["responses"] = responses as AnyObject?
        eventProperties["result_score"] = score as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("REVIEWED_TEST_RESULTS", properties: eventProperties)
    }
    
    // MARK: Interpret
    
    class func trackInterpretationStarted(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType) {
        var eventProperties = createBasicStudyMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionType.rawValue as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("STARTED_INTERPRETATION", properties: eventProperties)
    }
}
