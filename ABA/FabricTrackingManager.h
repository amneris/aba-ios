//
//  FabricTrackingManager.h
//  ABA
//
//  Created by MBP13 Jesus on 28/07/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "GenericTrackingManager.h"

@class ABARealmLevel;
@class ABAPlan;
@class ABASection;

@interface FabricTrackingManager : GenericTrackingManager

+ (instancetype)sharedManager;

- (void)trackLoginEvent;
- (void)trackFacebookLoginEvent;
- (void)trackLoginOfflineEvent;
- (void)trackRegister:(RecordType)type;
- (void)trackLevelChange:(ABARealmLevel *)fromLevel toLevel:(ABARealmLevel *)toLevel;
- (void)trackPricingScreenArrivalWithOriginController:(PlansViewControllerOriginType)type;
- (void)trackPricingSelected:(ABAPlan *)plan;
- (void)trackPurchase:(SKPaymentTransaction *)transaction withPeriod:(NSInteger)period withProduct:(SKProduct *)product;
- (void)trackSection:(ABASection *)section withSecondsOfStudyTime:(double)secondsOfStudy withInitialProgress:(NSNumber *)initialProgress withFinalProgress:(NSNumber *)finalProgress;

@end
