//
//  DataController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Blocks.h"
#import "Definitions.h"
#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@class ABARealmLevel, ABAUnit, ABARealmUser, ABAPhrase, ABARealmProgressAction;

@interface DataController : NSObject

+ (NSArray *)getUnits;

+ (void)getSectionForUnit:(ABAUnit *)unit withLang:(NSString *)locale completion:(CompletionBlock)block;

+ (float)getProgressForSection:(ABASection *)section;

+ (void)setCompletedPhrases:(NSArray *)phrases
            completionBlock:(CompletionBlock)completionBlock;

+ (void)setPhrasesListened:(NSArray *)phrases
          completionBlock:(CompletionBlock)block;

+ (void)setCompletedSection:(ABASection *)section completionBlock:(CompletionBlock)completionBlock;

+ (void)setLevelProgress:(ABARealmLevel *)level newProgress:(NSNumber *)progress completionBlock:(CompletionBlock)block;

+ (void)saveProgressActionForSection:(ABASection *)section
                             andUnit:(ABAUnit *)unit
                          andPhrases:(NSArray *)phrases
                           errorText:(NSString *)text
                            isListen:(BOOL)listen
                              isHelp:(BOOL)help
                     completionBlock:(CompletionBlock)block;

+ (void)syncCompletedActions:(NSArray *)completedActions
                     forUnit:(ABAUnit *)unit;

+ (void)syncUnitProgress:(NSArray *)unitProgress
         completionBlock:(CompletionBlock)block;

+ (void)getAllUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang completionBlock:(CompletionBlock)block;

+ (RLMResults *)getPendingProgressActions;

+ (void)setProgressActionsAsSent:(NSArray *)progressActionsIds;

+ (void)updateLastChangedUnit:(ABAUnit *)unit;

+ (void)updateProgressUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)completionBlock;

+ (RLMResults *)getLevelsAndUnits;

+ (void)cleanEntitiesWithCompletionBlock:(ErrorBlock)block;

@end
