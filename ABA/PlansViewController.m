//
//  PlansViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "ABAAlert.h"
#import "ABANavigationController.h"
#import "ABAPlan.h"
#import "APIManager.h"
#import "InitialPlansViewController.h"
#import "LanguageController.h"
#import "LegacyTrackingManager.h"
#import "MenuViewController.h"
#import "PlanCell.h"
#import "PlansViewController.h"
#import "Resources.h"
#import "SubscriptionController.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import <RESideMenu/RESideMenu.h>
@import SVProgressHUD;

// Analytics
#import "FabricTrackingManager.h"
#import "MixpanelTrackingManager.h"

// Swift
#import "ABA-Swift.h"

#import "PlansViewInput.h"

static NSString *cellPlan = @"PlanCell";
static float heighCollectionCell_iPhone = 120.0f;
static float separacionCollectionCell_iPhone = 20.0f;

static NSString *cellPlan_iPad = @"PlanCell_iPad";
static float heighCollectionCell_iPad = 172.0f;
static float separacionCollectionCell_iPad = 60.0f;

@interface PlansViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ReloadViewControllerProtocol>

@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (nonatomic, strong) IBOutlet UILabel * labelAdditionalInfo;
@property (nonatomic, strong) IBOutlet UIButton * buttonInfo;

@property NSArray *dataSource;
@property(nonatomic, strong) SubscriptionController *subscriptionController;
@property(nonatomic, strong) ReloadViewController *reloadVC;
@property(nonatomic, strong) FirebaseRemoteConfigManager * remoteConfig;

@end

@implementation PlansViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.remoteConfig = [[FirebaseRemoteConfigManager alloc] init];
    self.view.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    
    if (self.shouldShowBackButton)
    {
        [self addBackButtonWithTitleBlue:STRTRAD(@"chooseYourSubscriptionKey", @"Elige tu suscripción a ABA Premium")];
    }
    else
    {
        [self addMenuButtonWithTitleBlue:STRTRAD(@"chooseYourSubscriptionKey", @"Elige tu suscripción a ABA Premium")];
    }
    
    float insets = IS_IPAD ? (separacionCollectionCell_iPad / 2) : (separacionCollectionCell_iPhone * 0.75);
    self.collectionView.contentInset = UIEdgeInsetsMake(insets, 0, insets, 0);
    _infoLabel.text = @"";
    [self fetchSubscriptionProducts];
    CLS_LOG(@"PlanViewController: entering planVC");
    
    [self.labelAdditionalInfo setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 18 : 11]];
    [self.labelAdditionalInfo setTextColor:ABA_COLOR_DARKGREY];
    [self.labelAdditionalInfo setText:STRTRAD(@"subscription_view_autorenewal_info_text", @"")];
    
    // Hidding labels until all data is rendered
    [self.labelAdditionalInfo setHidden:YES];
    [self.buttonInfo setHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[LegacyTrackingManager sharedManager] trackPageView:@"paymentprices"];
    [[MixpanelTrackingManager sharedManager] trackPricingScreenArrivalWithOriginController:self.originController];
    [[FabricTrackingManager sharedManager] trackPricingScreenArrivalWithOriginController:self.originController];
    [CoolaDataTrackingManager trackOpenedPrices:[UserController currentUser].idUser];
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    for (UIViewController *tempVC in navigationArray)
    {
        if ([tempVC isKindOfClass:[InitialPlansViewController class]])
        {
            [navigationArray removeObject:tempVC];
            break;
        }
    }
    self.navigationController.viewControllers = navigationArray;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCollection) name:kUpdateRotationFromMenu object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBAction

- (IBAction)showPurchaseInfoActionSheet
{
    UIAlertController * sheet = [UIAlertController alertControllerWithTitle:nil
                                                                    message:nil
                                                             preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction * termsAndContidions = [UIAlertAction actionWithTitle:STRTRAD(@"profile_term_of_use_button", @"")
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    [self showTermsOfUse];
                                                                }];
    
    UIAlertAction * legalInfo = [UIAlertAction actionWithTitle:STRTRAD(@"profile_privacy_policy_button", @"")
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           [self showLegalInfo];
                                                       }];
    
    UIAlertAction * cancelButton = [UIAlertAction actionWithTitle:STRTRAD(@"offline_dialog_cancel", @"")
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              [sheet dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    
    [sheet addAction:termsAndContidions];
    [sheet addAction:legalInfo];
    [sheet addAction:cancelButton];
    
    sheet.popoverPresentationController.sourceView = self.buttonInfo;
    sheet.popoverPresentationController.sourceRect = self.buttonInfo.bounds;
    
    [self presentViewController:sheet animated:YES completion:nil];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(_collectionView.frame.size.width - (IS_IPAD ? separacionCollectionCell_iPad : separacionCollectionCell_iPhone), (IS_IPAD ? heighCollectionCell_iPad : heighCollectionCell_iPhone));
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ABAPlan *planToBuildInCell = [_dataSource objectAtIndex:indexPath.row];
    
    PlanCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IS_IPAD ? cellPlan_iPad : cellPlan forIndexPath:indexPath];
    cell.titleLabel.text = [planToBuildInCell titlePlan];
    cell.priceLabel.text = [planToBuildInCell pricePlan];
    
    // if same plans then the original price should be hidden
    if ([planToBuildInCell.originalIdentifier isEqualToString:planToBuildInCell.discountIdentifier])
    {
        cell.originalPriceLabel.alpha = 0.0;
        [cell.originalPriceLabel addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[price(==0)]"
                                                                                        options:0
                                                                                        metrics:nil
                                                                                          views:@{ @"price" : cell.originalPriceLabel }]];
    }
    else
    {
        cell.originalPriceLabel.attributedText = [[NSAttributedString alloc] initWithString:planToBuildInCell.originalPricePlan attributes:@{NSStrikethroughStyleAttributeName : [NSNumber numberWithInteger:NSUnderlineStyleSingle]}];
    }
    
    cell.descriptionLabel.text = [planToBuildInCell descriptionPlan];
    [cell.buttonLabel setTitle:STRTRAD(@"seleccionarPlanButtonKey", @"seleccionar") forState:UIControlStateNormal];
    cell.offer2x1ImageView.hidden = !planToBuildInCell.is2x1;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ABAPlan *cellPlan = [_dataSource objectAtIndex:indexPath.row];
    
    [[MixpanelTrackingManager sharedManager] trackPricingSelected:cellPlan];
    [[FabricTrackingManager sharedManager] trackPricingSelected:cellPlan];
    [CoolaDataTrackingManager trackSubscriptionSelected:[UserController currentUser].idUser daysInPlan:cellPlan.days];
    
    // Starting subscription process
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscriptionCompletedSuccess) name:kSubscriptionProcessCompletedOK object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscriptionCompletionFailed) name:kSubscriptionProcessCompletedFailed object:nil];
    [SubscriptionController subscribeToPlan:cellPlan];
    CLS_LOG(@"PlanViewController: Plan selected %@", cellPlan.titlePlan);
}

#pragma mark - SubscriptionDelegate Methods

- (void)subscriptionCompletedSuccess
{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedOK object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedFailed object:nil];
    
    ABANavigationController *menuNavigation = (ABANavigationController *)self.sideMenuViewController.leftMenuViewController;
    MenuViewController *menuVC = menuNavigation.viewControllers[0];
    [menuVC setupView];
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
    [self.sideMenuViewController setContentViewController:nc animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

- (void)subscriptionCompletionFailed
{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedOK object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedFailed object:nil];
}

#pragma mark - Private methods

- (void)reloadCollection
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

- (void)fetchSubscriptionProducts
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    if ([self.remoteConfig isSelligentPlanActive]) {
        // Using Selligent products
        ABARealmUser * user = [UserController currentUser];
        [self.presenter getPlans:[user idUser]];
    }
    else
    {
        // Using ABAWebapps products
        typeof(self) __weak weakSelf = self;
        [SubscriptionController getSubscriptionProducts:^(NSError *error, id object) {
            [SVProgressHUD dismiss];
            
            if (error)
            {
                [self showReloadPage];
            }
            else
            {
                [SVProgressHUD dismiss];
                
                if (self.reloadVC)
                {
                    [self dissmissReloadPage];
                }
                
                [weakSelf refreshWithDataSource:object[@"products"] andLabelText:object[@"text"]];
            }
        }];
    }
}

- (void)refreshWithDataSource:(NSArray *)arrayOfProducts andLabelText:(NSString *)text
{
    if (![self.remoteConfig isSixMonthsTierActive])
    {
        NSMutableArray * filteredArrayOfPlans = [NSMutableArray array];
        for (ABAPlan * plan in arrayOfProducts)
        {
            if (plan.days != 180)
            {
                [filteredArrayOfPlans addObject:plan];
            }
        }
        
        self.dataSource = filteredArrayOfPlans;
    }
    else
    {
        self.dataSource = arrayOfProducts;
    }
    
    self.infoLabel.text = text;
    
    // Overwritting top text with an special message for users that will pay with Mexican Pesos
    if (self.dataSource.count > 0)
    {
        ABAPlan *firstPlan = self.dataSource.firstObject;
        if ([[firstPlan.productWithOriginalPrice.priceLocale localeIdentifier] containsString:@"MXN"])
        {
            self.infoLabel.text = STRTRAD(@"payment_will_be_done_in_MXN", @"Tu pago se realizará en MXS$");
        }
    }
    
    [self.labelAdditionalInfo setHidden:NO];
    [self.buttonInfo setHidden:NO];
    
    typeof(self) __weak weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView reloadData];
    });
}

#pragma mark - Private methods showing/hiding Reload page

- (void)showReloadPage
{
    if (!self.reloadVC)
    {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        self.reloadVC = (ReloadViewController *)[mainStoryBoard instantiateViewControllerWithIdentifier:@"ReloadViewController"];
        self.reloadVC.reloadProtocol = self;
        
        [self addChildViewController:self.reloadVC];
        [self.view addSubview:self.reloadVC.view];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.3];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        CALayer *layer = self.view.layer;
        [layer setFrame:self.view.bounds];
        
        [layer addAnimation:animation forKey:@"MoveView"];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.35 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void) {
        ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"errorFetchingSubscriptions", @"No se han podido recuperar las suscripciones")];
        [abaAlert show];
    });
}

- (void)reloadButtonAction
{
    [self fetchSubscriptionProducts];
}

- (void)dissmissReloadPage
{
    [self.reloadVC.view removeFromSuperview];
    self.reloadVC = nil;
    
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.3];
    [animation setType:kCATransitionFade];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[self.view layer] addAnimation:animation forKey:@"MoveView"];
}

#pragma mark - private methods

- (void)showTermsOfUse
{
    [[MixpanelTrackingManager sharedManager] trackInfoButtonPushed:kPlanInfoTypeTermsOfUse];
    
    LegalInfoViewController * legalVC = [[UIStoryboard storyboardWithName:@"Profile" bundle:nil] instantiateViewControllerWithIdentifier:@"LegalInfoViewController"];
    [legalVC setText:STRTRAD(@"profileTemsDescriptionKey", @"")];
    [legalVC addBackButtonWithTitleBlue:STRTRAD(@"profileTemsTitleKey", @"")];
    
    [self.navigationController pushViewController:legalVC animated:YES];
}

- (void)showLegalInfo
{
    [[MixpanelTrackingManager sharedManager] trackInfoButtonPushed:kPlanInfoTypePrivacyPolicy];
    
    LegalInfoViewController * legalVC = [[UIStoryboard storyboardWithName:@"Profile" bundle:nil] instantiateViewControllerWithIdentifier:@"LegalInfoViewController"];
    [legalVC setText:STRTRAD(@"profilePolicyDescriptionKey", @"")];
    [legalVC addBackButtonWithTitleBlue:STRTRAD(@"profilePolicyTitleKey", @"")];
    
    [self.navigationController pushViewController:legalVC animated:YES];
}

#pragma mark - PlansViewInput

- (void)renderPlans:(id<PlansPageProducts>)viewModel;
{
    [SVProgressHUD dismiss];
    
    if (self.reloadVC)
    {
        [self dissmissReloadPage];
    }
    
    [self refreshWithDataSource:[viewModel products] andLabelText:[viewModel text]];
}

- (void)renderError
{
    [SVProgressHUD dismiss];
    [self showReloadPage];
}

@end
