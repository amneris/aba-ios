//
//  ABAPulseView.m
//  ABA
//
//  Created by Jaume Cornadó Panadés on 21/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAPulseView.h"
#import "Resources.h"

@implementation ABAPulseView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (void)layoutSubviews {
    [self setLayerProperties];
    [self attachAnimations];
}

- (void)setLayerProperties {
    CAShapeLayer *layer = (CAShapeLayer *)self.layer;
    layer.path = [UIBezierPath bezierPathWithOvalInRect:self.bounds].CGPath;
    layer.fillColor = [UIColor colorWithHue:0 saturation:1 brightness:.8 alpha:0.0].CGColor;
    
}

- (void)attachAnimations {
    [self attachPathAnimation];
    [self attachColorAnimation];
}

- (void)attachColorAnimation {
    CABasicAnimation *animation = [self animationWithKeyPath:@"fillColor"];
    animation.fromValue = (__bridge id)RGB(155, 228, 244).CGColor;
    [self.layer addAnimation:animation forKey:animation.keyPath];
}

- (void)attachPathAnimation {
    CABasicAnimation *animation = [self animationWithKeyPath:@"path"];
    animation.toValue = (__bridge id)[UIBezierPath bezierPathWithOvalInRect:CGRectInset(self.bounds, 20, 20)].CGPath;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.layer addAnimation:animation forKey:animation.keyPath];
}

- (CABasicAnimation *)animationWithKeyPath:(NSString *)keyPath {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
    //animation.autoreverses = YES;
    animation.repeatCount = 1;
    animation.duration = 0.50;
    return animation;
}

@end
