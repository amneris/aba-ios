//
//  ABAPulseViewController.m
//  ABA
//
//  Created by Jaume Cornadó Panadés on 21/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAPulseViewController.h"
#import "ABAPulseView.h"

#define PULSE_MAX_MILLIS 0.1

@interface ABAPulseViewController ()

@property NSDate *lastPulse;

@end

@implementation ABAPulseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) createPulse {
    
    if(_lastPulse != nil && [[NSDate date] timeIntervalSinceDate:_lastPulse] < PULSE_MAX_MILLIS) {
        //Another pulse is going on
        return;
    }
    
    ABAPulseView *pulseView = [[ABAPulseView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:pulseView];
    _lastPulse = [NSDate date];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
