
//
//  RootViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ResideMenuViewController.h"
#import "Utils.h"

// Zendesk
#import <ZendeskSDK/ZendeskSDK.h>
#import "Definitions.h"

#import "UserController.h"
#import "ABARealmUser.h"
#import "ABAPopup.h"

#import "ABA-Swift.h"

@interface ResideMenuViewController ()

@property (nonatomic, strong) ABAPopup *abaPopup;

@end

@implementation ResideMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self setUserZendesk];
    [self showFeedbackPopUp];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if(IS_IPAD)
    {
        return UIInterfaceOrientationMaskAll;
    }
    else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuNavigationController"];
    
    [self setContentViewShadowEnabled:YES];
    [self setContentViewShadowOpacity:0.8f];
    [self setContentViewShadowOffset:CGSizeMake(-5, 0)];
    [self setContentViewScaleValue:0.5f];
    [self setContentViewInPortraitOffsetCenterX:50.f];
    
    ABARealmUser * user = [UserController currentUser];
    [CoolaDataTrackingManager trackEnteredCourseIndex:user.idUser levelId:user.currentLevel.idLevel];
}

#pragma mark - custom methods

- (void)showFeedbackPopUp
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kShowZendeskPopUp]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kShowZendeskPopUp];
        [NSUserDefaults.standardUserDefaults synchronize];
        
        _abaPopup = [ABAPopup abaPopupWithType:kABAPopupTypeCrash];
        [_abaPopup showInView:self.view];
    }
}

- (void)setUserZendesk
{    
    if ([UserController isUserLogged]) {
        
        ZDKAnonymousIdentity *identity = [ZDKAnonymousIdentity new];
        
        identity.name = [UserController currentUser].name;
        identity.email = [UserController currentUser].email;
        
        [ZDKConfig instance].userIdentity = identity;
    }
}


@end
