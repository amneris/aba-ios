//
//  SelligentPlansInteractorInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

protocol SelligentPlansInteractorInput {
    func getProducts(userId: String) -> Observable<SelligentProducts>
}

protocol ItunesPlansInteractorInput {
    func fetchInfoFromApple(selligentResponse: SelligentProducts) -> Observable<PlansPageProducts>
}

