//
//  TrackingDefinitions.h
//  ABA
//
//  Created by MBP13 Jesus on 28/07/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#ifndef ABA_TrackingDefinitions_h
#define ABA_TrackingDefinitions_h

typedef NS_ENUM(NSUInteger, PlansViewControllerOriginType)
{
    kPlansViewControllerOriginBanner = 1,
    kPlansViewControllerOriginUnlockContent = 2,
    kPlansViewControllerOriginMenu = 3
};

typedef NS_ENUM(NSUInteger, RecordType)
{
    kRegisterTypeFacebook = 1,
    kRegisterTypeMail = 2
};

typedef NS_ENUM(NSUInteger, RegistrationFunnelActionType)
{
    kRegistrationFunnelActionTypeRegister = 1,
    kRegistrationFunnelActionTypeLogin = 2
};

typedef NS_ENUM(NSUInteger, PlanInfoType)
{
    kPlanInfoTypeTermsOfUse = 1,
    kPlanInfoTypePrivacyPolicy = 2
};


// User type definitions
#define kUserTypePremium @"Premium"
#define kUserTypeFree @"Free"

// Registration type definitions
#define kRegistrationTypeFacebook @"Facebook"
#define kRegistrationTypeEmail @"Email"
#define kRegistrationTypeUnknown @"Registration unknown"

// Plans type information definitions
#define kPlanInfoTypeTerms @"Terms of use"
#define kPlanInfoTypePrivacy @"Privacy policy"
#define kPlanInfoTypeUnknown @"Plan info unknown"

// plans screen arrival origin type definitions
#define kArrivalTypeBanner @"Banner"
#define kArrivalTypeLockedSection @"Locked section"
#define kArrivalTypeMenu @"Menu"
#define kArrivalTypeUnknown @"Origin unknown"

#endif
