//
//  UserFactory.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Wrap
import Unbox

enum NetworkUserParserError: Swift.Error, CustomStringConvertible {
    public var description: String { return "NetworkUserParserError.." }
    case invalidData
}

struct NetworkUserParser {

    // MARK: From raw
    
    static func parseNetworkUser(_ userDictionary: Any) throws -> NetworkUser? {
        if let userDictionary = userDictionary as? UnboxableDictionary {
            let user = try Unboxer.performCustomUnboxing(dictionary: userDictionary, closure: { unboxer -> NetworkUser? in
                return try NetworkUser(unboxer: unboxer)
            })
            
            return user
        }
        
        throw NetworkUserParserError.invalidData
    }
    
    static func parseNetworkUser(_ jsonData: Data) throws -> NetworkUser? {
        let user = try Unboxer.performCustomUnboxing(data: jsonData, closure: { unboxer -> NetworkUser? in
            return try NetworkUser(unboxer: unboxer)
        })
        
        return user
    }
}
