//
//  ABARoscoController.h
//  ABA
//
//  Created by Oriol Vilaró on 14/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABAUnit.h"

@protocol RoscoControllerDelegate <NSObject>

- (void)didSelectSectionWithType:(kABASectionTypes)type;

- (void)showPlans;
- (void)showConnectionAlert;
- (void)showLockMessage;
- (void)showAssesmentLockAlert;

- (void)updateTitleWithProgress;
- (void)updateImages;

- (void)showLoadingProgress;
- (void)hideLoadingProgress;

@end

@interface ABARoscoController : NSObject

@property(weak, atomic) id<RoscoControllerDelegate> delegate;

+ (ABARoscoController *)roscoControllerWithUnit:(ABAUnit *)unit
                                  containerView:(UIView *)containerView;

- (void)loadSectionDataAnimated:(BOOL)animated;

- (void)setupCenterView;

- (void)udpateConstraintsIsLandscape:(BOOL)isLandscape
                  shouldHideMenuView:(BOOL)shouldHideMenuView;

- (void)refreshProgressUnit;

@end
