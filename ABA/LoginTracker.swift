//
//  LoginTracker.swift
//  ABA
//
//  Created by MBP13 Jesus on 30/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol LoginTracker: FacebookTracker {
    func trackLoginWithEmailAndPassword()
    func trackLoginWithToken()
    func trackLoginOffline()
}