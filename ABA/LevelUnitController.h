//
//  UnitListController.h
//  ABA
//
//  Created by Marc Güell Segarra on 5/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"
#import "Utils.h"


@class ABARealmLevel, ABAUnit, ABARealmUser;

@interface LevelUnitController : NSObject

/**
 *  Return the total amount of levels
 *
 *  @return NSInteger number of levels
 */
+ (NSInteger)getNumberOfLevels;

/**
 * Gets all levels
 *
 * @return NSArray level objects
 */
+ (RLMResults *)getAllLevels;

/**
 * Gets all levels sorted descending by idLevel. All units for the level are also sorted descending by idUnit
 *
 * @return NSArray level objects sorted
 */
+ (RLMResults *)getAllLevelsDescending;

/**
 * Gets all units for the level sorted descending by idUnit
 *
 * @return NSArray unit objects sorted
 */
+ (RLMResults *)getUnitsDescendingForLevel:(ABARealmLevel *)level;

/**
 *  Get corresponding level for levelId
 *
 *  @param idLevel idLevel for corresponding level
 *
 *  @return ABARealmLevel object
 */
+ (ABARealmLevel *)getLevelWithID:(NSString *)idLevel;

/**
 *  Get corresponding unit for idUnit in corresponding level
 *
 *  @param idUnit NSString idUnit
 *  @param level  ABARealmLevel object
 *
 *  @return ABAUnit object
 */
+ (ABAUnit *)getUnitWithID:(NSNumber *)idUnit forLevel:(ABARealmLevel *)level;


+ (ABAUnit *)getUnitWithID:(NSNumber *)idUnit;

/**
 *  Get last completed unit for corresponding level
 *
 *  @param level ABARealmLevel
 *
 *  @return ABAUnit object
 */
+ (ABAUnit *)getLastCompletedUnitForLevel:(ABARealmLevel *)level;

/**
 *  Get an array of all the completed units for corresponding level
 *
 *  @param level ABARealmLevel object
 *
 *  @return NSArray of ABAUnits objects
 */
+ (NSArray *)getCompletedUnitsForLevel:(ABARealmLevel *)level;

/**
 *  Get an array of all the incompleted units for corresponding level
 *
 *  @param level ABARealmLevel object
 *
 *  @return NSArray of ABAUnits objects
 */
+ (NSArray *)getInompletedUnitsForLevel:(ABARealmLevel *)level;

/**
 *  Check if the ABAUnit is the last completed unit of the ABARealmLevel object
 *
 *  @param unit  ABAUnit that you want to know if it's the last completed unit
 *  @param level the ABARealmLevel that you want to check units
 *
 *  @return YES/NO
 */
+ (BOOL)isLastCompleted:(ABAUnit *)unit forLevel:(ABARealmLevel *)level;


+ (void)setCompletedUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)completionBlock;
+ (void)setCompletedLevel:(ABARealmLevel *)level completionBlock:(CompletionBlock)completionBlock;


/**
 Sets the unit as completed and sets the next unit to the user
 
 @param unit the unit to mark as completed
 @param user the user to set the new unit
 */
+ (void)completeUnit:(ABAUnit *)unit forUser:(ABARealmUser *)user completionBlock:(CompletionBlock) completionBlock;

/**
 Sets the next level available to the user
 
 @param user the user to be increased one level
 */
+ (void)goToNextLevel:(ABARealmUser *)user;

/**
 Devuelve el nivel siguente
 
 @param level el nivel con el que comparamos
 */
+ (ABARealmLevel *)getNextLevel:(ABARealmLevel *)level;

/**
 Gets the next unit
 
 @param unit the current unit
 */
+ (ABAUnit *)getNextUnit:(ABAUnit *)unit;

+ (NSString *)getIdLevelForIdUnit:(NSNumber *)idUnit;

+ (void)checkForUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang andBlock:(ErrorBlock) block;

+ (void)refreshAllLevelsProgress:(CompletionBlock) block;

+ (ABAUnit *)getCurrentUnitForLevel:(ABARealmLevel *)level;

@end