//
//  ABAEvaluationQuestion.h
//  ABA
//
//  Created by Ivan Ferrera on 27/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAEvaluationOption.h"


@interface ABAEvaluationQuestion : RLMObject

@property NSNumber<RLMInt>* order;
@property NSString* question;
@property NSNumber<RLMInt> * answered;
@property RLMArray<ABAEvaluationOption *><ABAEvaluationOption> *options;

@end

RLM_ARRAY_TYPE(ABAEvaluationQuestion)
