//
//  User.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@objc protocol User {

    var countryNameIso: String? { get set }
    var email: String? { get set }
    var idSession: String? { get set }
    var idUser: String { get set }
    var name: String? { get set }
    var partnerID: String? { get set }
    var sourceID: String? { get set }
    var surnames: String? { get set }
    var teacherId: String? { get set }
    var teacherName: String? { get set }
    var teacherImage: String? { get set }
    var token: String { get set }
    var type: String { get set }
    var urlImage: String? { get set }
    var userLang: String? { get set }

    var gender: String? { get set }
    var birthDate: String? { get set }
    var phone: String? { get set }
    var externalKey: String? { get set }

    var expirationDate: Date? { get set }
    var lastLoginDate: Date? { get set }

    var userLevel: String? { get set }

    var isNewUser: Bool { get set }
}
