//
//  ABAInterpretRole.m
//  ABA
//
//  Created by Marc Güell Segarra on 12/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ABAInterpret.h"
#import "ABAInterpretRole.h"

#import "ABAPhrase.h"

@interface ABAInterpretRole ()
@property (readonly) RLMLinkingObjects<ABAInterpretPhrase *> *interpretRoles;
@end

@implementation ABAInterpretRole

- (RLMResults *)interpretPhrases
{
    return self.interpretRoles;
}

+ (NSDictionary *)linkingObjectsProperties
{
    return @{
        @"interpretRoles" : [RLMPropertyDescriptor descriptorWithClass:ABAInterpretPhrase.class propertyName:@"role"],
    };
}

@end
