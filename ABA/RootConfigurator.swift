//
//  RootConfigurator.swift
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

@objc
class RootConfigurator: NSObject {
    
    func configureModuleForViewInput(_ viewInput: RootViewInput) {
        
        if let appDelegate = viewInput as? AppDelegate {
            configure(appDelegate)
        }
    }
    
    fileprivate func configure(_ application: AppDelegate) {
        
        let repository = LoginRepository()
        let interactor = LoginInteractor(repository: repository)
        
        let presenter = RootPresenter(tracker: Tracker.sharedInstance)
        presenter.app = application
        presenter.interactor = interactor
        presenter.router = RootRouter()
        application.presenter = presenter
    }
}
