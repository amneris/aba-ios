//
//  UnitListViewController.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import SnapKit
import SVProgressHUD
import LGAlertView
import SDWebImage

class UnitListViewController : UIViewController {
    
    internal let PLANS_VIEW_HEIGHT:CGFloat = 70.0
    
    // UI layer
    internal let backgroundImageView = UIImageView(frame: CGRect.zero)
    internal let tableView = UITableView(frame: CGRect.zero)
    internal var downloadButton: UIButton?
    internal let plansView = UIView(frame: CGRect.zero)
    internal let goToPlansButton = UIButton(type: .custom)
    
    internal var currentAlert: ABAAlert?
    internal var loadingProgressAlert: ABAAlert?
    internal var currentProgressDialog: LGAlertView?
    internal var currentCompletionAlert: ABAAlertCompletedUnit? = nil
    
    // Domain layer
    internal var unitId: NSNumber
    internal var currentModel : UnitViewModel?
    
    // Viper
    var presenter: UnitDetailViewOutput?
    
    init(unitIdToLoad: NSNumber) {
        unitId = unitIdToLoad
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) will not be implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        configureAutolayout()
        
        // Hack for mantain infoAlert TODO: Find better approach to get HUD id or etc
        //        _lockInfoAlert = NO;
        
        self.addBackButtonNavBar()
        self.updateTitleWithProgress()
        
        self.setupBackgroundImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        LegacyTrackingManager.shared().trackPageView("sectionlist")
        
        // Refresh section and progress every time the unit is shown
        self.presenter?.getSectionsAndProgress(backgroundLoad: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Refresh tableview
        self.tableView.reloadData()
        
        // Update title
        self.updateTitleWithProgress()
        
        // Completion alert
        if let completionAlert = currentCompletionAlert {
            self.view.addSubview(completionAlert)
            
            UIView.animate(withDuration: 0.5, delay: 4.5, options: .curveEaseOut, animations: {
                completionAlert.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    completionAlert.removeFromSuperview()
                    self.currentCompletionAlert = nil
                }
            })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(unitProgressUpdated), name: NSNotification.Name(rawValue: kUnitViewProgressUpdatedNotification), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kUnitViewProgressUpdatedNotification), object: nil)
        
        loadingProgressAlert?.dismiss()
        loadingProgressAlert = nil
    }
}

// MARK: - Private methods

internal extension UnitListViewController {
    
    func configureViews() {
        
        self.backgroundImageView.backgroundColor = UIColor.black
        self.backgroundImageView.contentMode = .scaleAspectFill
        self.view.addSubview(backgroundImageView)
        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        self.tableView.isHidden = true // first state is hidden
        self.tableView.alpha = 0.0
        self.tableView.cellLayoutMarginsFollowReadableWidth = false
        self.view.addSubview(tableView)
        
        self.goToPlansButton.setTitle(String.localize("gain_access_unit_list_button", comment: "LIBERA TODO EL CONTENIDO"), for: .normal)
        self.goToPlansButton.setTitleColor(UIColor.white, for: .normal)
        self.goToPlansButton.backgroundColor = UnitDetailConstants.colorSectionCurrent
        self.goToPlansButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        self.goToPlansButton.layer.cornerRadius = 2.0
        self.goToPlansButton.contentEdgeInsets = UIEdgeInsetsMake(3, 10, 3, 10)
        self.goToPlansButton.addTarget(self, action: #selector(bottomButtonPushed), for: .touchUpInside)
        
        self.plansView.addSubview(self.goToPlansButton)
        self.plansView.backgroundColor = UnitDetailConstants.colorGreyTranslucent
        self.view.addSubview(self.plansView)
        self.plansView.isHidden = true
    }
    
    func configureAutolayout() {
        backgroundImageView.snp.makeConstraints({ make in
            make.edges.equalTo(self.view)
        })
        
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(self.view)
        }
        
        self.goToPlansButton.snp.makeConstraints { make in
            make.center.equalTo(self.plansView)
            make.width.lessThanOrEqualTo(self.plansView)
        }
        
        self.plansView.snp.makeConstraints { make in
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(PLANS_VIEW_HEIGHT)
        }
    }
    
    func addDownloadButton() {
        downloadButton = UIButton(type: .custom)
        downloadButton?.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        downloadButton?.tintColor = UIColor.white
        downloadButton?.addTarget(self, action: #selector(downloadOfflineMode), for: .touchUpInside)
        
        let image = UIImage(named: "icon_download")
        downloadButton?.setBackgroundImage(image, for: .normal)
        
        let barButton = UIBarButtonItem(customView: downloadButton!)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func toggleDownloadButton(shouldShow: Bool, shouldEnable maybeStatus: Bool?) {
        
        if shouldShow {
            
            if downloadButton == nil {
                addDownloadButton()
            }
            
            if let status = maybeStatus, status {
                let image = UIImage(named: "icon_download")
                downloadButton?.setBackgroundImage(image, for: .normal)
                downloadButton?.isUserInteractionEnabled = true
                Crashlytics.sharedInstance().setObjectValue("NO", forKey: "Unit downloaded")
            }
            else {
                let image = UIImage(named: "icon_downloaded")
                downloadButton?.setBackgroundImage(image, for: .normal)
                downloadButton?.isUserInteractionEnabled = false
                Crashlytics.sharedInstance().setObjectValue("YES", forKey: "Unit downloaded")
            }
        }
    }
    
    func setupBackgroundImage() {
        
        guard let abaUnit = LevelUnitController.getUnitWithID(unitId) else {
            return
        }
        
        guard let imageString = ImageHelper.change3xIfNeeded(withUrlString: abaUnit.filmImageInactiveUrl) else {
            return
        }
        
        guard let imageUrl = URL(string: imageString) else {
            return
        }
        
        backgroundImageView.sd_setImageWithPreviousCachedImage(with: imageUrl, placeholderImage: nil, options: .retryFailed, progress: nil, completed: nil)
    }
    
    func updateTitleWithProgress()  {
        guard let abaUnit = LevelUnitController.getUnitWithID(unitId) else {
            return
        }
        
        let unitLocalizedString: String = String.localize("unitMenuKey", comment: "Unidad")
        self.addTitleNavbar("\(unitLocalizedString) \(abaUnit.idUnit!)", andSubtitle: abaUnit.title, andProgress: abaUnit.getPercentage())
    }
    
    func unitProgressUpdated() {
        self.presenter?.getSectionsAndProgress(backgroundLoad: true)
    }
}

// MARK: - Actions

internal extension UnitListViewController {
    
    func downloadOfflineMode() {
        self.presenter?.downloadUnit()
    }
    
    func bottomButtonPushed() {
        self.presenter?.ctaPushed()
    }
}

// MARK: - UnitDetailViewInput

extension UnitListViewController: UnitDetailViewInput {
    
    func showLoadingProgress() {
        
        // Showing hud
        SVProgressHUD.show(with: .clear)
        
        // creating progress alert
        self.loadingProgressAlert = ABAAlert(alertWithText: String.localize("ProgressDialogMessage", comment: "Estamos actualizando tu progreso. Espera unos segundos."))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if (SVProgressHUD.isVisible()) {
                self.loadingProgressAlert?.show(withOffset: 44, andDissmisAfter: 120.0)
            }
        }
    }
    
    func hideLoadingProgress() {
        SVProgressHUD.dismiss()
        
        loadingProgressAlert?.dismiss()
        loadingProgressAlert = nil
    }
    
    func showAlert(withText text: String){
        currentAlert = ABAAlert(alertWithText: text)
        currentAlert?.show(withOffset: 44, andDissmisAfter: 1.0)
    }
    
    func prepareToShowCompletionAlert(type: kABASectionTypes) {
        let initialFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100.0)
        currentCompletionAlert = ABAAlertCompletedUnit(frame: initialFrame, andSectionType: type)
    }
    
    func showProgressDialog() {
        
        currentProgressDialog?.dismiss(animated: true, completionHandler: nil)
        currentProgressDialog = LGAlertView(progressViewStyleWithTitle: nil, message: String.localize("offlineDownloadingFileKey", comment: ""), progressLabelText: "0 %", buttonTitles: nil, cancelButtonTitle: String.localize("alertSubscriptionKOButton", comment: ""), destructiveButtonTitle: nil)
        
        currentProgressDialog?.didDismissHandler = { alertView in
            self.presenter?.cancelDownload()
        }
        
        currentProgressDialog?.setProgress(0.0, progressLabelText: "0 %")
        currentProgressDialog?.show(animated: true, completionHandler: { })
        currentProgressDialog?.isCancelOnTouch = false
    }
    
    func showRateAppPopup() {
        let abaPopup = ABAPopup(type: .abaPopupTypeRate)
        abaPopup?.show(in: self.view)
    }
    
    func updateDownloadProgressDialog(withProgress progress: Float) {
        let currentProgress = progress / 100
        currentProgressDialog?.setProgress(currentProgress, progressLabelText: "\(progress) %")
    }
    
    func dismissDownloadProgressDialog() {
        currentProgressDialog?.dismiss(animated: true, completionHandler: nil)
    }
    
    func refreshDataSource(model: UnitViewModel) {
        
        self.currentModel = model
        
        if (model.shouldShowPlansButton) {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, PLANS_VIEW_HEIGHT, 0)
            self.plansView.isHidden = false
        } else {
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            self.plansView.isHidden = true
        }
        
        self.setupBackgroundImage()
        self.tableView.reloadData()
        self.toggleDownloadButton(shouldShow: model.shouldShowDownloadButton, shouldEnable: model.shouldEnableDownloadButton)
        
        // Showing table view animatelly
        if self.tableView.isHidden {
            
            self.tableView.isHidden = false
            UIView.transition(with: self.view, duration: 0.4, options: .curveEaseInOut, animations: {
                self.tableView.alpha = 1.0
            }, completion: { finished in
                if finished {
                    self.presenter?.screenRendered()
                }
            })
        }
        else {
            self.presenter?.screenRendered()
        }
    }
    
    func refreshTeacher() {
        
        self.tableView.reloadRows(at: [IndexPath(item: 0, section: 0)], with: .none) // to reload teacher
    }
}

// MARK: - UITableViewDelegate

extension UnitListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return CGFloat(TeacherCell.TEACHER_CELL_HEIGHT)
        }
        else {
            return CGFloat(SectionCell.SECTION_CELL_HEIGHT)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (indexPath.row > 0) {
            let sectionType: kABASectionTypes = kABASectionTypes(rawValue: UInt(indexPath.row))
            presenter?.actionForSectionPushed(section: sectionType)
        }
    }
}

// MARK: - UITableViewDataSource

extension UnitListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let model = currentModel else {
            self.tableView.isHidden = true
            return 0
        }
        
        guard let sectionsCount = model.sections?.count else {
            self.tableView.isHidden = false
            return 1 // Teacher
        }
        
        // sections + teacher
        return sectionsCount + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            // Teacher
            var cell = tableView.dequeueReusableCell(withIdentifier: TeacherCell.TEACHER_CELL_REUSER_IDENTIFIER) as? TeacherCell
            
            if cell == nil {
                cell = TeacherCell()
            }
            
            cell!.configureCell(teacherText: currentModel!.teacherText)
            return cell!
        }
        else {
            
            // Section
            
            var cell = tableView.dequeueReusableCell(withIdentifier: SectionCell.SECTION_CELL_REUSER_IDENTIFIER) as? SectionCell
            if cell == nil {
                cell = SectionCell()
            }
            
            guard let model = currentModel?.sections?[indexPath.row - 1] else {
                // It should never happens
                return cell!
            }
            
            cell!.loadSection(sectionModel: model)
            return cell!
            
        }
    }
}
