//
//  RegisterParameters.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

struct RegistrationFacebookParameters {

    let email: String
    let name: String
    let surnames: String
    let facebookId: String
    let gender: String

    let languageEnvironment: String
    let deviceId: String
    let sysOper: String
    let deviceName: String
    let ipMobile: String

    var idPartner: String {
        get {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                return partneridIphone
            case .pad:
                return partneridIpad
            default:
                return "Unspecified idPartner"
            }
        }
    }

    let idSourceList: String = "0"

    var deviceTypeSource: String {
        get {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                return "m"
            case .pad:
                return "t"
            default:
                return "Unspecified device type"
            }
        }
    }
}

extension RegistrationFacebookParameters: ApiSignable {

    var orderedParams: [String] {
        return [
            "email",
            "name",
            "surnames",
            "fbId",
            "gender",
            "langEnv",
            "deviceId",
            "sysOper",
            "deviceName",
            "ipMobile",
            "idPartner",
            "idSourceList",
            "deviceTypeSource"]
    }

    func params() -> [String: String] {
        return [
            "email": email,
            "name": name,
            "surnames": surnames,
            "fbId": facebookId,
            "gender": gender,
            "langEnv": languageEnvironment,
            "deviceId": deviceId,
            "sysOper": sysOper,
            "deviceName": deviceName,
            "ipMobile": ipMobile,
            "idPartner": idPartner,
            "idSourceList": idSourceList,
            "deviceTypeSource": deviceTypeSource
        ]
    }
}
