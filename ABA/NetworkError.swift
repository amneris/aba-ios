//
//  NetworkError.swift
//  ABA
//
//  Created by MBP13 Jesus on 09/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Unbox

struct NetworkError: Unboxable {
    var status: String
    var message: String

    init(unboxer: Unboxer) throws {
        self.status = try unboxer.unbox(key: "status")
        self.message = try unboxer.unbox(key: "message")
    }
}
