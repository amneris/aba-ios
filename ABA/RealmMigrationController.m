//
//  RealmMigrationController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 09/02/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "RealmMigrationController.h"
#import <Realm/Realm.h>

@implementation RealmMigrationController

+ (void)migrateRealmVersion {
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    // Set the new schema version. This must be greater than the previously used
    // version (if you've never set a schema version before, the version is 0).
    config.schemaVersion = 2;
    
    // Set the block which will be called automatically when opening a Realm with a
    // schema version lower than the one set above
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 2) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    };
    
    // Tell Realm to use this new configuration object for the default Realm
    [RLMRealmConfiguration setDefaultConfiguration:config];
}

@end
