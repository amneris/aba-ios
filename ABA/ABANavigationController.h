//
//  ABANavigationController.h
//  ABA
//
//  Created by Oriol Vilaró on 12/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABANavigationController : UINavigationController

@property (nonatomic, assign) BOOL shouldContinueTrackingTime;

@end
