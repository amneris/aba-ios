//
//  ABASpeakDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABASpeakDataController.h"
#import <Realm/Realm.h>
#import "ABAUnit.h"
#import "ABASpeak.h"
#import "ABASpeakPhrase.h"

@implementation ABASpeakDataController

+ (void)parseABASpeakSection:(NSArray *)abaSpeakDataArray onUnit: (ABAUnit*) unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABASpeak *speak = unit.sectionSpeak;
    
    [realm transactionWithBlock:^{
        
        if (!speak) {
            speak = [[ABASpeak alloc] init];
            unit.sectionSpeak = speak;
        }
        
        for(NSDictionary *abaSpeakData in abaSpeakDataArray) {
            
            if (![abaSpeakData objectForKey:@"dialog"])
            {
                continue;
            }
            
            ABASpeakDialog *speakDialog = [[ABASpeakDialog alloc] init];
            
            NSArray *dialogs = [abaSpeakData objectForKey:@"dialog"];
            
            speakDialog.role = [abaSpeakData objectForKey:@"Role"];
            
            if ([abaSpeakData objectForKey:@"dialog"] != nil && [[abaSpeakData objectForKey:@"dialog"] isKindOfClass:[NSArray class]]) {
                //Parse the dialog data
                BOOL firstItem = YES;
                NSInteger dialogCount = 0;
                for(NSDictionary *dialogData in [abaSpeakData objectForKey:@"dialog"]) {
                    dialogCount ++;
                    ABASpeakDialogPhrase *phrase = [[ABASpeakDialogPhrase alloc] init];
                    phrase.idPhrase = [dialogData objectForKey:@"audio"];
                    phrase.audioFile = [dialogData objectForKey:@"audio"];
                    phrase.text = [dialogData objectForKey:@"text"];
                    phrase.page = [dialogData objectForKey:@"page"];
                    if (![[abaSpeakData objectForKey:@"translation"] isEqual:[NSNull null]]) {
                        if ([[abaSpeakData objectForKey:@"translation"] isKindOfClass:[NSArray class]]) {
                            if (dialogCount == dialogs.count) {
                                phrase.translation = [[abaSpeakData objectForKey:@"translation"] objectAtIndex:0];
                            }
                        }
                        else {
                            if (dialogCount == dialogs.count) {
                                phrase.translation = [abaSpeakData objectForKey:@"translation"];
                            }
                        }
                    }
                    if (firstItem) {
                        firstItem = NO;
                        phrase.role = [abaSpeakData objectForKey:@"Role"];
                    }
                    else {
                        phrase.role = nil;
                    }
                    [speakDialog.dialog addObject:phrase];
                }
            }
            
            if ([abaSpeakData objectForKey:@"sample"] != nil && [[abaSpeakData objectForKey:@"sample"] isKindOfClass:[NSArray class]]) {
                //Parse the dialog data
                for(NSDictionary *sampleData in [abaSpeakData objectForKey:@"sample"]) {
                    
                    if ([sampleData isKindOfClass:[NSDictionary class]]) {
                        
                        if (![[sampleData objectForKey:@"text"] isEqual:@""]) { //controlar textos vacios
                            ABASpeakPhrase *phrase = [[ABASpeakPhrase alloc] init];
                            phrase.idPhrase = [sampleData objectForKey:@"audio"];
                            phrase.audioFile = [sampleData objectForKey:@"audio"];
                            phrase.text = [sampleData objectForKey:@"text"];
                            phrase.page = [sampleData objectForKey:@"page"];
                            if (![[sampleData objectForKey:@"translation"] isEqual:[NSNull null]]) {
                                phrase.translation = [sampleData objectForKey:@"translation"];
                            }
                            [speakDialog.sample addObject:phrase];
                        }
                        
                    }
                    else { // array de phrases de un sample
                        
                        ABASpeakPhrase *phraseFather = [[ABASpeakPhrase alloc] init];
                        
                        for (NSDictionary *sampleDict in sampleData) {
                            
                            ABASpeakPhrase *phrase = [[ABASpeakPhrase alloc] init];
                            phrase.idPhrase = [sampleDict objectForKey:@"audio"];
                            phrase.audioFile = [sampleDict objectForKey:@"audio"];
                            phrase.text = [sampleDict objectForKey:@"text"];
                            phrase.page = [sampleDict objectForKey:@"page"];
                            if (![[sampleDict objectForKey:@"translation"] isEqual:[NSNull null]]) {
                                phrase.translation = [sampleDict objectForKey:@"translation"];
                            }
                            
                            [phraseFather.subPhrases addObject:phrase];
                        }
                        
                        [speakDialog.sample addObject:phraseFather];
                    }
                    
                }
            }
            
            [speak.content addObject:speakDialog];
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, speak);
    }
    else
    {
        block(error, nil);
    }
}

@end
