//
//  ABAPlan+Methods.m
//  ABA
//
//  Created by MBP13 Jesus on 07/01/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "ABAPlan+Methods.h"
#import "Utils.h"

@implementation ABAPlan (Methods)

+ (NSString *)getSubscriptionStringFromDays:(NSInteger)days
{
    NSInteger months = days / 30;
    if (months == 1)
    {
        return [NSString stringWithFormat:@"%ld %@", (long)months, STRTRAD(@"mes", @"mes")];
    }
    else
    {
        return [NSString stringWithFormat:@"%ld %@", (long)months, STRTRAD(@"meses", @"meses")];
    }
}

+ (NSString *)getSubscriptionPlanPriceInOneCharge:(SKProduct *)product forDays:(NSInteger)days
{
    NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
    [priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [priceFormatter setGeneratesDecimalNumbers:YES];

    [priceFormatter setLocale:product.priceLocale];

    if (days == 30)
    {
        return [NSString stringWithFormat:@"%@ %@", [priceFormatter stringFromNumber:product.price], STRTRAD(@"chargedEveryMonth", @"pago mensual")];
    }
    else if (days < 360)
    {
        return [NSString stringWithFormat:@"%@ %@", [priceFormatter stringFromNumber:product.price], STRTRAD(@"chargedEveryKey", @"cargado cada")];
    }
    else if (days == 360)
    {
        return [NSString stringWithFormat:@"%@ %@", [priceFormatter stringFromNumber:product.price], STRTRAD(@"chargedEveryKey", @"cargado cada año")];
    }
    else if (days > 360)
    {
        return [NSString stringWithFormat:@"%@ %@", [priceFormatter stringFromNumber:product.price], STRTRAD(@"chargedEveryKey", @"cargado cada")];
    }

    return @"";
}

+ (NSString *)getSubscriptionPlanPriceForMonth:(SKProduct *)product forDays:(NSInteger)days
{
    NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
    [priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [priceFormatter setGeneratesDecimalNumbers:YES];
    [priceFormatter setLocale:product.priceLocale];

    NSInteger months = days / 30;
    CGFloat monthPrice = [product.price floatValue] / months;

    return [NSString stringWithFormat:@"%@/%@", [priceFormatter stringFromNumber:[NSNumber numberWithFloat:monthPrice]], STRTRAD(@"mes", @"mes")];
}

+ (NSInteger)getProductPeriod:(SKProduct *)product
{
    NSString *productIdentifier = product.productIdentifier;
    if ([productIdentifier hasPrefix:@"1M"])
    {
        return 30;
    }
    else if ([productIdentifier hasPrefix:@"6M"])
    {
        return 180;
    }
    else if ([productIdentifier hasPrefix:@"12M"])
    {
        return 360;
    }

    return 0;
}

+ (NSString *)getProductNameForTrackingPurposes:(NSString *)productIdentifier {
    if (productIdentifier) {
        if ([productIdentifier hasPrefix:@"1M"])
        {
            return @"1 month";
        }
        else if ([productIdentifier hasPrefix:@"6M"])
        {
            return @"6 months";
        }
        else if ([productIdentifier hasPrefix:@"12M"])
        {
            return @"12 months";
        }
    }
    
    return @"Unknown";
}

@end
