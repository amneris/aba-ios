//
//  CertUnitCell.m
//  ABA
//
//  Created by Marc Güell Segarra on 5/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "CertUnitCell.h"
#import "Resources.h"
#import "ABARealmCert.h"
#import "ABARealmLevel.h"
#import "Utils.h"

@implementation CertUnitCell

- (void)setupWithABARealmCertificate:(ABARealmCert *)ABARealmCert
{
    NSString *certTitleLocalizedString   = STRTRAD(@"certificateTitleKey", @"Certificado nivel");
    NSMutableString *composedString   = [NSMutableString stringWithString:certTitleLocalizedString];;
    
    [composedString appendString:@" "];
    [composedString appendString:ABARealmCert.level.idLevel];
    
    [_certificateTitleLabel setText:[composedString uppercaseString]];
    [_certificateLevelTitleLabel setText:ABARealmCert.level.name];
    
    
    if([ABARealmCert.level.completed integerValue] == 1)
    {
        [_certificateTitleLabel setTextColor:[UIColor whiteColor]];
        [_certificateLevelTitleLabel setTextColor:[UIColor whiteColor]];
        [_lowerLineView setBackgroundColor:ABA_COLOR_GREEN_COMPLETED];
        [_certificateIcon setImage:[UIImage imageNamed:@"circleGreenCertificate"]];
    }
    else
    {
        [_certificateTitleLabel setTextColor:ABA_COLOR_MIDGREY];
        [_certificateLevelTitleLabel setTextColor:ABA_COLOR_MIDGREY];
        [_lowerLineView setBackgroundColor:[UIColor whiteColor]];
        [_certificateIcon setImage:[UIImage imageNamed:@"circleWhiteCertificate"]];
    }
}

@end
