//
//  WriteViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "WriteViewController.h"
#import "ABAWrite.h"
#import "ABAWriteDialog.h"
#import "ABAPhrase.h"
#import "Utils.h"
#import "Resources.h"
#import "ABAAlert.h"
#import "UIViewController+ABA.h"
#import "AudioController.h"
#import "ABAUnit.h"
#import "MDTUtils.h"
#import "WriteController.h"
#import "ABAActionButtonView.h"
#import "SolutionTextController.h"
#import "ABAScrollView.h"
#import "ABATeacherTipView.h"
#import "ImageHelper.h"
#import "SDVersion.h"
#import "DataController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "DataController.h"
#import "PureLayout.h"
#import "APIManager.h"
#import "ABA-Swift.h"

// trakers
#import "LegacyTrackingManager.h"
#import <Crashlytics/Crashlytics.h>

//CGFloat const kiPadPortraitActionButtonTopInset = 0.35f;
//CGFloat const kiPadLandscapeActionButtonTopInset = 0.10f;

CGFloat const kiPhoneActionButtonTopConstant = 1.20f;
CGFloat const kiPadActionButtonTopConstant = 1.50f;

CGFloat const kActionButtonWidth = 0.35f;
CGFloat const kActionButtonHeight = 0.20f;

@interface WriteViewController ()

@property(nonatomic, strong) AudioController *audioController;
@property(nonatomic, strong) WriteController *writeController;
@property(nonatomic, strong) SectionController *sectionController;
@property(nonatomic, strong) SolutionTextController *solutionTextController;
@property(nonatomic, strong) PhrasesController *phrasesController;

@property(strong, nonatomic) PHFComposeBarView *composeBarView;
@property UITextView *textView;

@property NSLayoutConstraint *actionButtonBottomConstraint;

@property ABAAlert *abaAlert;
@property ABATeacherTipView *abaTeacher;
@property UIView *infoView;
@property UIButton *infoButton;
@property NSTimer *infoViewTimer;

@property BOOL helpHasShown;

@property UITapGestureRecognizer *singleTap;
@property UITapGestureRecognizer *singleHelpTap;

@property BOOL readOnlyMode;
@property NSArray *readOnlyDataSource;
@property int currentIndex;

@property CGFloat textFieldFontSize;
@property BOOL mustRestoreTextColor;

@end

@implementation WriteViewController

@dynamic section;
@synthesize composeBarView = _composeBarView;

- (id)initWithCoder:(NSCoder *)aDecoder
{

    if ((self = [super initWithCoder:aDecoder]))
    {
        _writeController = [[WriteController alloc] init];
        _sectionController = [[SectionController alloc] init];
        _phrasesController = [PhrasesController new];
        _solutionTextController = [[SolutionTextController alloc] init];
        _solutionTextController.strictContractedForms = YES;

        _audioController = [AudioController new];
        _audioController.audioControllerDelegate = self;

        _helpHasShown = NO;
        _readOnlyMode = NO;
        _mustRestoreTextColor = NO;

        if ([SDVersion deviceSize] == Screen3Dot5inch)
        {
            _textFieldFontSize = 13.0f;
        }
        else if (IS_IPAD)
        {
            _textFieldFontSize = 20.0f;
        }
        else
        {
            _textFieldFontSize = 15.0f;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view addSubview:[self composeBarView]];

    [self addBackButtonNavBar];

    self.view.backgroundColor = ABA_COLOR_REG_BACKGROUND;

    if ([_writeController isSectionCompleted:self.section])
    {
        _readOnlyMode = YES;
        _readOnlyDataSource = [_writeController getPhrasesDataSourceFromSection:self.section];
        _currentIndex = 0;

        ABAPhrase *phrase = [_readOnlyDataSource objectAtIndex:_currentIndex];
        _composeBarView.textView.text = phrase.text;
        _composeBarView.textView.textColor = ABA_COLOR_REG_MAIL_BUT;
        [_composeBarView handleTextViewChangeAnimated:YES];
        [_composeBarView setButtonTitle:STRTRAD(@"nextButtonWriteKey", @"Siguiente")];
        _composeBarView.textView.editable = NO;
    }
    else
    {
        _currentStatus = kABAWriteInitial;
#ifdef SHEPHERD_DEBUG
        if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
            self.composeBarView.textView.text = [self getCurrentPhrase].text;
        }
#endif
    }

    [self setupView];

    [DataController saveProgressActionForSection:self.section
                                         andUnit:[self.section getUnitFromSection]
                                      andPhrases:nil
                                       errorText:nil
                                        isListen:NO
                                          isHelp:NO
                                 completionBlock:^(NSError *error, id object){

                                 }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [self setupActionButton];

    if (![self.view.subviews containsObject:_abaTeacher] && [[_writeController getPercentageForSection:self.section] isEqualToString:@"0%"])
    {
        _abaTeacher = [[ABATeacherTipView alloc]
            initTeacherWithText:STRTRAD(@"sectionWriteTeacherKey", @"Escucha y escribe lo que oigas")
                      withImage:@"teacher"
                     withShadow:YES];

        UIColor *color = ABA_COLOR_REG_BACKGROUND;
        _abaTeacher.backgroundColor = [color colorWithAlphaComponent:1.0];

        [self.view addSubview:_abaTeacher];
        [_abaTeacher setConstraints];

        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTeacherView)];
        [self.view addGestureRecognizer:_singleTap];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [[LegacyTrackingManager sharedManager] trackPageView:@"writesection"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];

    [super viewWillDisappear:animated];

    _audioController.audioControllerDelegate = nil;

    if ([_audioController.player isPlaying])
    {
        [[_audioController player] stop];
    }

    if (_abaAlert)
    {
        [_abaAlert dismiss];
    }
}

#pragma mark - AudioControllerDelegate

- (void)audioStarted
{
}

- (void)audioFinished
{
    [_actionButton addButtonTapRecognizer];
    
    // Cooladata tracking
    [CoolaDataTrackingManager trackListenedToAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaWrite exerciseId:[self getCurrentPhrase].idPhrase];
}

- (void)audioDidGetError:(NSError *)error
{
    [self audioFinished];
}

#pragma mark - Private methods

- (void)keyboardWillToggle:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval duration;
    UIViewAnimationCurve animationCurve;
    CGRect startFrame;
    CGRect endFrame;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&startFrame];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&endFrame];
    
    if (CGRectEqualToRect(startFrame, endFrame))
    {
        return;
    }
    
    NSInteger signCorrection = 1;
    if (startFrame.origin.y < 0 || startFrame.origin.x < 0 || endFrame.origin.y < 0 || endFrame.origin.x < 0)
        signCorrection = -1;
    
    CGFloat heightChange = (endFrame.origin.y - startFrame.origin.y) * signCorrection;
    
    // We ignore this notification because it appears to be a error (iPads with >=iOS9)
    if (IS_IPAD && abs((int)heightChange) == KB_CHANGE_HEIGHT_IGNORE)
    {
        return;
    }
    
    CGRect newContainerFrame = [[self composeBarView] frame];
    newContainerFrame.origin.y += heightChange;
    
    _actionButtonBottomConstraint.constant += heightChange;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:(animationCurve << 16) | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [[self composeBarView] setFrame:newContainerFrame];
                         [_actionButton layoutIfNeeded];
                     }
                     completion:NULL];
    
    [self hideTeacherView];
}

- (void)setupActionButton
{
    _actionButton = [[ABAActionButtonView alloc] initWithSectionType:kABAWriteSectionType];
    _actionButton.translatesAutoresizingMaskIntoConstraints = NO;
    _actionButton.delegate = self;
    
    [self.view addSubview:_actionButton];
    
    CGFloat actionButtonWidth = MAX(self.view.frame.size.width, self.view.frame.size.height) * kActionButtonWidth;
    CGFloat actionButtonHeight = MAX(self.view.frame.size.width, self.view.frame.size.height) * kActionButtonHeight;
    
    [_actionButton autoSetDimension:ALDimensionWidth toSize:actionButtonWidth];
    [_actionButton autoSetDimension:ALDimensionHeight toSize:actionButtonHeight];
    [_actionButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    CGFloat actionButtonConstant = IS_IPAD ? kiPadActionButtonTopConstant : kiPhoneActionButtonTopConstant;
    _actionButtonBottomConstraint = [_actionButton autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:_composeBarView.frame.size.height * actionButtonConstant];
    
    
    [_actionButton layoutIfNeeded];
    
    _audioController.pulseViewController = _actionButton.pulseViewController;
}

- (void)hideTeacherView
{
    [self.view removeGestureRecognizer:_singleTap];
    
    [UIView animateWithDuration:0.3
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.abaTeacher.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [self.abaTeacher removeFromSuperview];
                         [_composeBarView.textView performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.1];
                     }];
}

- (PHFComposeBarView *)composeBarView
{
    if (!_composeBarView)
    {
        
        CGRect frame = CGRectMake(0.0f,
                                  [UIScreen mainScreen].bounds.size.height - PHFComposeBarViewInitialHeight,
                                  [UIScreen mainScreen].bounds.size.width,
                                  PHFComposeBarViewInitialHeight);
        
        if (IS_IPAD)
        {
            _composeBarView = [[PHFComposeBarView alloc] initWithFrame:frame andSpacingConstant:40.0f];
        }
        else
        {
            _composeBarView = [[PHFComposeBarView alloc] initWithFrame:frame];
        }
        
        _composeBarView.textView.font = [UIFont fontWithName:ABA_MUSEO_SLAB_700 size:_textFieldFontSize];
        
        _composeBarView.textView.textColor = ABA_COLOR_DARKGREY;
        _composeBarView.textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        _composeBarView.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        _composeBarView.textView.spellCheckingType = UITextSpellCheckingTypeNo;
        
        [_composeBarView.textView setDelegate:self];
        
        _composeBarView.textView.tintColor = [UIColor blueColor];
        
        // hide shorcuts panel
        if ([_composeBarView.textView respondsToSelector:@selector(inputAssistantItem)])
        {
            UITextInputAssistantItem *item = [_composeBarView.textView inputAssistantItem];
            item.leadingBarButtonGroups = @[];
            item.trailingBarButtonGroups = @[];
        }
        
        CGRect textViewFrame = _composeBarView.textView.frame;
        textViewFrame.origin.y += 3.0f;
        [_composeBarView.textView setFrame:textViewFrame];
        
        [_composeBarView setButtonTitle:STRTRAD(@"doneButtonWriteKey", @"Hecho")];
        
        [_composeBarView.button.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:_textFieldFontSize]];
        [_composeBarView.button.titleLabel setTextColor:ABA_COLOR_BLUE_MENU_UNIT];
        [_composeBarView setButtonTintColor:ABA_COLOR_BLUE_MENU_UNIT];
        
        [_composeBarView setPlaceholder:STRTRAD(@"typeTextWriteKey", @"Type the text you heard here...")];
        _composeBarView.placeholderLabel.font = [UIFont fontWithName:ABA_MUSEO_SLAB_500 size:_textFieldFontSize];
        [_composeBarView setAccessibilityLabel:@"test"];
        [_composeBarView setDelegate:self];
    }
    
    return _composeBarView;
}


- (void)listenButtonTapped
{
    [self hideTeacherView];
    
    [_actionButton removeButtonTapRecognizer];
    
    [_audioController playAudioFile:[self getCurrentPhrase].audioFile withUnit:self.section.unit forRec:NO];
}

- (void)setupView
{
    _infoButton = [UIButton newAutoLayoutView];
    [self.view addSubview:_infoButton];
    
    CGFloat widthForCircleButton = MAX(self.view.frame.size.width, self.view.frame.size.height) * 0.05;
    
    [_infoButton autoSetDimension:ALDimensionHeight toSize:widthForCircleButton];
    [_infoButton autoSetDimension:ALDimensionWidth toSize:widthForCircleButton];
    [_infoButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:IS_IPAD ? 10.0f : 5.0f];
    [_infoButton autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:IS_IPAD ? 10.0f : 5.0f];
    [_infoButton layoutIfNeeded];
    
    _infoButton.backgroundColor = ABA_COLOR_DARKGREY4;
    _infoButton.layer.cornerRadius = widthForCircleButton / 2;
    _infoButton.layer.masksToBounds = YES;
    
    UIImageView *questionIcon = [UIImageView newAutoLayoutView];
    [questionIcon setImage:[UIImage imageNamed:@"info"]];
    [questionIcon setContentMode:UIViewContentModeScaleAspectFit];
    [_infoButton addSubview:questionIcon];
    
    [questionIcon autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_infoButton withMultiplier:0.50];
    [questionIcon autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_infoButton withMultiplier:0.50];
    [questionIcon autoCenterInSuperview];
    
    [_infoButton addTarget:self action:@selector(showInfoCorrectAnswer:) forControlEvents:UIControlEventTouchUpInside];
    _infoButton.adjustsImageWhenHighlighted = NO;
    _infoButton.adjustsImageWhenDisabled = NO;
    
    if (_readOnlyMode)
    {
        [_infoButton setHidden:YES];
    }
    
    [self createHeaderProgress];
}

- (void)showInfoCorrectAnswer:(id)sender
{
    [self toggleHelpViewWithText:[self getCurrentPhrase].text];
    
    //	[self checkTextInput];
}

- (void)toggleHelpViewWithText:(NSString *)text
{
    if (_abaAlert)
    {
        [_abaAlert dismiss];
    }
    
    if (!_helpHasShown)
    {
        [self showHelpViewWithText:text];
    }
    else
    {
        [self hideHelpView];
    }
}

- (void)showHelpViewWithText:(NSString *)text
{
    ABAPhrase * phraseToAskForHelpAt = [self getCurrentPhrase];
    [DataController saveProgressActionForSection:self.section
                                         andUnit:nil
                                      andPhrases:@[ phraseToAskForHelpAt ]
                                       errorText:@""
                                        isListen:NO
                                          isHelp:YES
                                 completionBlock:^(NSError *error, id object){

                                 }];

    _infoButton.enabled = NO;

    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 18.0f : 14.0f];
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * 0.75, 9999);
    CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];

    if (size.height < (_infoButton.frame.size.height + (IS_IPAD ? 15.0f : 5.0f) * 2))
    {
        size.height = _infoButton.frame.size.height + (IS_IPAD ? 15.0f : 5.0f) * 2;
    }
    else
    {
        size.height += 10.0f * 2;
    }

    CGRect finalFrame = CGRectMake(0, 0, MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), size.height);

    _infoView = [[UIView alloc] initWithFrame:CGRectMake(MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), 0, MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), size.height)];
    _infoView.backgroundColor = ABA_COLOR_DARKGREY4;

    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f,
                                                                   [UIScreen mainScreen].bounds.size.width * 0.75,
                                                                   size.height)];

    infoLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 18.0f : 14.0f];
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.text = text;
    infoLabel.numberOfLines = 0;
    infoLabel.lineBreakMode = NSLineBreakByWordWrapping;

    [_infoView addSubview:infoLabel];

    [self.view insertSubview:_infoView belowSubview:_infoButton];

    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _infoButton.transform = CGAffineTransformMakeRotation(-3.141516 / 2);
                     }
                     completion:NULL];

    [UIView animateWithDuration:0.3
        delay:0.4
        options:UIViewAnimationOptionCurveEaseIn
        animations:^{

            [_infoView setFrame:finalFrame];

        }
        completion:^(BOOL finished) {
            _helpHasShown = YES;
            _infoButton.enabled = YES;

            _infoViewTimer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(hideHelpView) userInfo:nil repeats:NO];

            _singleHelpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideHelpView)];
            [self.view addGestureRecognizer:_singleHelpTap];

        }];
    
    // Cooladata tracking
    [CoolaDataTrackingManager trackTextSuggestion:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaWrite exerciseId:phraseToAskForHelpAt.idPhrase];
}

- (void)hideHelpView
{
    [self.view removeGestureRecognizer:_singleHelpTap];
    _infoButton.enabled = NO;
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                         _infoButton.transform = CGAffineTransformMakeRotation(0);
                         _infoButton.backgroundColor = ABA_COLOR_DARKGREY4;
                         
                         [_infoView setFrame:CGRectMake(MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height), _infoView.frame.origin.y, _infoView.frame.size.width, _infoView.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished) {
                         [_infoViewTimer invalidate];
                         _infoViewTimer = nil;
                         
                         _helpHasShown = NO;
                         _infoButton.enabled = YES;
                         
                         [_infoView removeFromSuperview];
                     }];
}

- (void)createHeaderProgress
{
    [self addTitleNavbarSection:STRTRAD(@"sectionWriteTitleKey", @"Escribe") andSubtitle:[_writeController getPercentageForSection:self.section]];
    [self addBackButtonNavBar];
}

- (void)done
{
    if (_readOnlyMode)
    {
        if (_currentIndex < [_readOnlyDataSource count] - 1)
        {
            _currentIndex++;
            ABAPhrase *phrase = [_readOnlyDataSource objectAtIndex:_currentIndex];
            _composeBarView.textView.text = phrase.text;
            [_composeBarView handleTextViewChangeAnimated:YES];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        if (_currentStatus == kABAWriteOK)
        {
            [self newStatus:kABAWriteInitial];
        }
        else if ([self checkTextInput])
        {
            [self newStatus:kABAWriteOK];
        }
        else
        {
            [self newStatus:kABAWriteKO];
            _mustRestoreTextColor = YES;
        }
    }
}

- (NSString *)lowercaseTextMinusFirstLetters:(NSString *)text
{
    NSArray *words = [text componentsSeparatedByString:@" "];
    NSMutableString *finalText = [[NSMutableString alloc] init];
    
    for (NSString *word in words)
    {
        [finalText appendString:[NSString stringWithFormat:@"%@ ", [self convertToLowercase:word]]];
    }
    
    return finalText;
}

- (NSString *)convertToLowercase:(NSString *)text
{
    if ([text length] <= 0)
    {
        return @"";
    }
    
    NSString *firstChar = [text substringToIndex:1];
    
    NSString *restOfLetters = [text substringWithRange:NSMakeRange(1, [text length] - 1)];
    
    return [NSString stringWithFormat:@"%@%@", firstChar, [restOfLetters lowercaseString]];
}

- (BOOL)checkTextInput
{
    // First we convert the text following some rules and for mantaining the same coherence (contracted forms, lower/uppercase, etc)
    NSMutableString *answerFormatted = [[_solutionTextController formatSolutionText:_composeBarView.textView.text] mutableCopy];
    
    // We get a dictionary in wich the key is the index of the word in the phrase. The value will be blank if the word is correct.
    NSDictionary *errorDict = [_solutionTextController checkSolutionText:answerFormatted withCorrectSolutionText:[self getCurrentPhrase].text];
    
    BOOL almostOneWordIsIncorrect = [_solutionTextController checkPhrase:answerFormatted withErrorDict:errorDict];
    BOOL missingWords = [_solutionTextController areThereMissingWords:answerFormatted withErrorDict:errorDict];
    
    // Get a formatted string with colors green and red (correct or wrong)
    NSMutableAttributedString *mutAttStr = [_solutionTextController getAttributedString:answerFormatted withErrorDict:errorDict];
    
    // Aply general font-face
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SLAB_700 size:_textFieldFontSize]
                      range:[answerFormatted rangeOfString:answerFormatted]];
    
    // If there is almost one word that is incorrect, we apply the attributed string created just before
    if (almostOneWordIsIncorrect)
    {
        [_composeBarView.textView setText:answerFormatted];
        [_composeBarView.textView setAttributedText:mutAttStr.copy];
        
        // Placing the cursor where the word is missing
        if (!missingWords)
        {
            _composeBarView.textView.selectedRange = [_solutionTextController getFirstIncorrectWordPosition:answerFormatted withErrorDict:errorDict];
        }
    }
    else
    {
        // mustChangeOriginalText will be true if the user has typed all letters uppercase. In that case we have to correct it.
        if (_solutionTextController.mustChangeOriginalText)
        {
            [_composeBarView.textView setText:[_solutionTextController formatSolutionText:[self getCurrentPhrase].text]];
        }
        else
        {
            [_composeBarView.textView setText:_composeBarView.textView.text];
        }
        
        NSMutableAttributedString *mutAttStr1 = [[NSMutableAttributedString alloc] initWithString:_composeBarView.textView.text];
        
        // Apply font style for correct answer
        
        [mutAttStr1 addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SLAB_700 size:_textFieldFontSize]
                           range:[_composeBarView.textView.text rangeOfString:_composeBarView.textView.text]];
        
        [mutAttStr1 addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_REG_MAIL_BUT
                           range:[_composeBarView.textView.text rangeOfString:_composeBarView.textView.text]];
        
        [_composeBarView.textView setAttributedText:mutAttStr1.copy];
    }
    
    [_composeBarView handleTextViewChangeAnimated:YES];
    
    // Cooladata tracking
    [CoolaDataTrackingManager trackVerifiedText:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaWrite exerciseId:[self getCurrentPhrase].idPhrase isResultCorrect:!almostOneWordIsIncorrect writtenText:_composeBarView.textView.text];
    
    return !almostOneWordIsIncorrect;
}

- (void)newStatus:(kABAWriteStatus)newStatus
{
    
    _currentStatus = newStatus;
    [_abaAlert dismiss];
    
    switch (_currentStatus)
    {
        case kABAWriteInitial:
        {
            [_composeBarView.button setEnabled:YES];
            break;
        }
        case kABAWriteKO:
        {
            _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"sectionWriteKOKey", @"incorrecto") withCorrect:NO];
            [_abaAlert showWithOffset:44];
            
            [_phrasesController setPhraseKO:[self getCurrentPhrase]
                                 forSection:self.section
                                  errorText:_composeBarView.textView.text
                            completionBlock:^(NSError *error, id object){
                                
                            }];
            
            break;
        }
        case kABAWriteOK:
        {
            __weak typeof(self) weakSelf = self;
            
            _composeBarView.textView.textColor = ABA_COLOR_REG_MAIL_BUT;
            _composeBarView.button.enabled = NO;

            [_phrasesController setPhrasesDone:@[ [self getCurrentPhrase] ]
                                    forSection:self.section
                            sendProgressUpdate:YES
                               completionBlock:^(NSError *error, id object) {
                                   if (!error)
                                   {
                                       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                           weakSelf.abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"sectionWriteOKKey", @"correcto") withCorrect:YES];
                                           [weakSelf.abaAlert showWithOffset:44];

                                           [weakSelf.audioController playSound:@"correct.aiff"];
                                           //AudioServicesPlaySystemSound(1057);

                                           if ([weakSelf.writeController isSectionCompleted:weakSelf.section])
                                           {
                                               [weakSelf.writeController setCompletedSection:weakSelf.section
                                                                             completionBlock:^(NSError *error, id object) {
                                                                                 if (!error)
                                                                                 {
                                                                                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                                                         [weakSelf.navigationController popViewControllerAnimated:YES];
                                                                                         [weakSelf.delegate showAlertCompletedUnit:kABAWriteSectionType];

                                                                                     });
                                                                                 }
                                                                                 else
                                                                                 {
                                                                                     // alarm!
                                                                                 }
                                                                             }];
                                           }
                                           else
                                           {
                                               [weakSelf createHeaderProgress];
                                               weakSelf.currentStatus = kABAWriteInitial;

                                               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                   weakSelf.composeBarView.textView.text = @"";
                                                   weakSelf.composeBarView.textView.textColor = ABA_COLOR_DARKGREY;
                                                   [weakSelf listenButtonTapped];
                                                   [weakSelf.composeBarView handleTextViewChangeAnimated:YES];

#ifdef SHEPHERD_DEBUG
                                                   if ([ABAShepherdAutomatorPlugin isAutomationEnabled])
                                                   {
                                                       weakSelf.composeBarView.textView.text = [weakSelf getCurrentPhrase].text;
                                                       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                           [weakSelf done];
                                                       });
                                                   }
#endif
                                               });
                                           }
                                       });
                                   }
                                   else
                                   {
                                       // alarm!
                                   }
                               }];
            break;
        }
            
        case kABAWriteCompleted:
        {
            
            break;
        }
        default:
            break;
    }
}

- (ABAPhrase *)getCurrentPhrase
{
    if (_readOnlyMode)
    {
        return [_readOnlyDataSource objectAtIndex:_currentIndex];
    }
    else
    {
        for (ABAPhrase *phrase in [_writeController getPhrasesDataSourceFromSection:self.section])
        {
            if (![phrase.done boolValue])
            {
                [[Crashlytics sharedInstance] setObjectValue:phrase.text forKey:@"Current phrase"];
                [[LegacyTrackingManager sharedManager] trackInteraction];
                return phrase;
            }
        }
        
        return nil;
    }
}

#pragma mark - PHFComposeBarViewDelegate

- (void)composeBarViewDidPressButton:(PHFComposeBarView *)composeBarView
{
    [self done];
}

- (void)composeBarView:(PHFComposeBarView *)composeBarView willChangeFromFrame:(CGRect)startFrame toFrame:(CGRect)endFrame duration:(NSTimeInterval)duration animationCurve:(UIViewAnimationCurve)animationCurve
{
    UIEdgeInsets insets = UIEdgeInsetsMake(0.0f, 0.0f, endFrame.size.height, 0.0f);
    UITextView *textView = [self textView];
    [textView setContentInset:insets];
    [textView setScrollIndicatorInsets:insets];
    
    NSInteger signCorrection = 1;
    if (startFrame.origin.y < 0 || startFrame.origin.x < 0 || endFrame.origin.y < 0 || endFrame.origin.x < 0)
        signCorrection = -1;
    
    CGFloat heightChange = (endFrame.origin.y - startFrame.origin.y) * signCorrection;
    
    _actionButtonBottomConstraint.constant += heightChange;
    [_actionButton layoutIfNeeded];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    if (_mustRestoreTextColor)
    {
        textView.textColor = ABA_COLOR_DARKGREY;
        _mustRestoreTextColor = NO;
    }
}


@end
