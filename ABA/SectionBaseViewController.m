//
//  SectionBaseViewController.m
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "SectionBaseViewController.h"
#import "ABANavigationController.h"
#import "ABASection+Methods.h"
#import "DataController.h"

// Analytics
#import "FabricTrackingManager.h"
#import "MixpanelTrackingManager.h"
#import <Crashlytics/Crashlytics.h>

@interface SectionBaseViewController()
{
    NSDate * _dateForDidAppear;
    NSDate * _dateForDidDisappear;
    BOOL _shouldContinueTrackingTime;
    NSNumber * _startProgress;
    double _studyTime;
}
@end

@implementation SectionBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    _startProgress = self.section.progress;
    NSString * sectionType = [ABASection localizedNameforType:[self.section getSectionType]];
    CLS_LOG(@"SectionBase controlle: entering section: %@", sectionType);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (_shouldContinueTrackingTime || ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime)
    {
        // There is a timer already running
        _shouldContinueTrackingTime = NO;
        ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime = NO;
    }
    else
    {
        // Create new timer
        [self createTimer];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_shouldContinueTrackingTime || ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime)
    {
        // Do not calculate timing because the timer should not stop
    }
    else
    {
        [self calculateElapsedTimeAndAggregate];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (_shouldContinueTrackingTime || ((ABANavigationController *)self.navigationController).shouldContinueTrackingTime)
    {
        // Do not report time because the timer is still running
    }
    else
    {
        float finalProgress = [DataController getProgressForSection:self.section];
        
        // Report
        [[MixpanelTrackingManager sharedManager] trackSection:self.section withSecondsOfStudyTime:_studyTime withInitialProgress:_startProgress withFinalProgress:[NSNumber numberWithFloat:finalProgress]];
        [[FabricTrackingManager sharedManager] trackSection:self.section withSecondsOfStudyTime:_studyTime withInitialProgress:_startProgress withFinalProgress:[NSNumber numberWithFloat:finalProgress]];
    }
}

#pragma mark - Notifications

// Called when the app is going to background or one of the iOS bars (top or bottom) are shown
- (void)appResignActive
{
    [self calculateElapsedTimeAndAggregate];
}

// Called when the app comes from background or one the iOS bars are hidden
- (void)appBecomeActive
{
    // Restart the timer
    [self createTimer];
}

#pragma mark - Private methods

- (void)calculateElapsedTimeAndAggregate
{
    NSDate * dateForWillDisappear = [NSDate date];
    NSTimeInterval elapsedTime = [dateForWillDisappear timeIntervalSinceDate:_dateForDidAppear];
    _studyTime += elapsedTime;
    
    NSLog(@"---------------------------------------");
    NSLog(@"Study time = %f", elapsedTime);
    NSLog(@"Aggregated time = %f", _studyTime);
    NSLog(@"---------------------------------------");
}

- (void)createTimer
{
    _dateForDidAppear = [NSDate date];
}

#pragma mark - Overriding

- (void)presentViewController:(UIViewController *)viewControllerToPresent
                     animated:(BOOL)flag
         continueTrackingTime:(BOOL)skipTrackingTime
                   completion:(void (^)(void))completion
{
    _shouldContinueTrackingTime = skipTrackingTime;
    
    [super presentViewController:viewControllerToPresent
                        animated:flag
                      completion:completion];
}

@end
