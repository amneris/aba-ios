//
//  ABASection+MixpanelTracking.m
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABASection+GenericTrackingManager.h"
#import "ABAUnit.h"

@implementation ABASection(GenericTrackingManager)

- (NSDictionary *)trackingProperties
{
    NSMutableDictionary * dictionaryWithProperties = [NSMutableDictionary dictionary];
    
    if ([self getUnitFromSection].idUnit)
    {
        [dictionaryWithProperties setObject:[self getUnitFromSection].idUnitString forKey:@"Unitid"];
    }
    
    // Adding section id and name
    [dictionaryWithProperties setObject:[NSNumber numberWithInteger:[self getSectionType]] forKey:@"Sectionid"];
    [dictionaryWithProperties setObject:[self stringForSection] forKey:@"Section name"];
    
    return dictionaryWithProperties;
}

#pragma mark - Private methods

- (NSString *)stringForSection
{
    return NSStringFromClass([self class]);
}

@end
