//
//  InterpretCollectionViewCell.m
//  ABA
//
//  Created by Marc Güell Segarra on 2/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "InterpretCollectionViewCell.h"
#import "ABAInterpret.h"
#import "ABAInterpretRole.h"
#import "Utils.h"
#import "ABAUnit.h"
#import "SDVersion.h"
#import "ImageHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface InterpretCollectionViewCell ()

@property ABAInterpretRole *role;
@property UITapGestureRecognizer *retryTapRecognizer;
@property UITapGestureRecognizer *listenTapRecognizer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleVerticalSpace;

@end

@implementation InterpretCollectionViewCell

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if((self = [super initWithCoder:aDecoder]))
    {
        _retryTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(retryAction:)];
        [_retryTapRecognizer setNumberOfTapsRequired:1];
        
        _listenTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(listenAction:)];
        [_listenTapRecognizer setNumberOfTapsRequired:1];
    }
    return self;
}

-(void)setupWithABAInterpretRole: (ABAInterpretRole *)role
{
    _role = role;
    
    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:_role.imageBigUrl];
    NSURL *imageUrl = [NSURL URLWithString:urlString];
    
    [self.characterImage sd_setImageWithURL:imageUrl];
    
    CALayer *imageLayer = _characterImage.layer;
    [imageLayer setCornerRadius:(_characterImage.frame.size.width/2)];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];

	_retryLabel.text = STRTRAD(@"interpretRetryLabelKey", @"REHACER");
	_listenLabel.text = STRTRAD(@"interpretListenLabelKey", @"ESCUCHAR");

    if([role.completed isEqualToNumber:[NSNumber numberWithInt:1]])
    {
        [_retryButton setHidden:NO];
        [_retryLabel setHidden:NO];
        [_listenButton setHidden:NO];
        [_listenLabel setHidden:NO];
        [_doneCircle setHidden:NO];

        _chooseCharacterLabel.text = role.name;
    }
    else    // incomplete
    {
        [_retryButton setHidden:YES];
        [_retryLabel setHidden:YES];
        [_listenButton setHidden:YES];
        [_listenLabel setHidden:YES];
        [_doneCircle setHidden:YES];

        //_chooseCharacterLabel.text = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"chooseCharacterKey", @"Escoger a"), role.name];
		
		_chooseCharacterLabel.text = role.name;
    }
	
    [_retryButton addGestureRecognizer:_retryTapRecognizer];
    [_listenButton addGestureRecognizer:_listenTapRecognizer];
    
    if([SDVersion deviceSize] == Screen3Dot5inch)
    {
        _titleVerticalSpace.constant = 2.0f;
    }

}

-(IBAction)retryAction:(id)sender
{
    [self.delegate retryRole:_role];
}

-(IBAction)listenAction:(id)sender
{
    [self.delegate listenRole:_role];
}

@end
