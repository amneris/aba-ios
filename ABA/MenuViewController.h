//
//  ViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MenuSection) {
    MenuUnits,
    MenuLevels,
    MenuCertificates,
    MenuPremium,
    MenuHelp
};

@interface MenuViewController : UIViewController

- (void)setupView;

@end

