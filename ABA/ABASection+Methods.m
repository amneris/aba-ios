//
//  ABASection+MixpanelTracking.m
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABASection.h"

#import "ABAWrite.h"
#import "ABAVocabulary.h"
#import "ABASpeak.h"
#import "ABAUnit.h"
#import "ABAInterpret.h"
#import "ABAExercises.h"
#import "ABAFilm.h"
#import "ABAEvaluation.h"
#import "ABAVideoClass.h"
#import "Utils.h"

@implementation ABASection(Methods)

- (kABASectionTypes)getSectionType
{
    if ([self isKindOfClass:[ABAWrite class]])
        return kABAWriteSectionType;
    
    if ([self isKindOfClass:[ABAVocabulary class]])
        return kABAVocabularySectionType;
    
    if ([self isKindOfClass:[ABASpeak class]])
        return kABASpeakSectionType;
    
    if ([self isKindOfClass:[ABAInterpret class]])
        return kABAInterpretSectionType;
    
    if ([self isKindOfClass:[ABAExercises class]])
        return kABAExercisesSectionType;
    
    if ([self isKindOfClass:[ABAEvaluation class]])
        return kABAAssessmentSectionType;
    
    if ([self isKindOfClass:[ABAVideoClass class]])
        return kABAVideoClassSectionType;
    
    if ([self isKindOfClass:[ABAFilm class]])
        return kABAFilmSectionType;
    
    return -1;
}

+ (NSString *)localizedNameforType:(kABASectionTypes)type
{
    switch(type)
    {
        case kABAFilmSectionType:
        {
            return STRTRAD(@"unitMenuTitle1Key", @"Film");
            break;
        }
        case kABASpeakSectionType:
        {
            return STRTRAD(@"unitMenuTitle2Key", @"Habla") ;
            break;
        }
        case kABAWriteSectionType:
        {
            return STRTRAD(@"unitMenuTitle3Key", @"Escribe");
            break;
        }
        case kABAInterpretSectionType:
        {
            return STRTRAD(@"unitMenuTitle4Key", @"Interpreta");
            break;
        }
        case kABAVideoClassSectionType:
        {
            return STRTRAD(@"unitMenuTitle5Key", @"Videoclase");
            break;
        }
        case kABAExercisesSectionType:
        {
            return STRTRAD(@"unitMenuTitle6Key", @"Ejercicios");
            break;
        }
        case kABAVocabularySectionType:
        {
            return STRTRAD(@"unitMenuTitle7Key", @"Vocabulario");
            break;
        }
        case kABAAssessmentSectionType:
        {
            return STRTRAD(@"unitMenuTitle8Key", @"Evaluación");
            break;
        }
    }
}

- (ABAUnit *)getUnitFromSection
{
    if ([self isKindOfClass:[ABAWrite class]])
    {
        ABAWrite* write = (ABAWrite *) self;
        return write.unit;
    }
    
    if ([self isKindOfClass:[ABAVocabulary class]])
    {
        ABAVocabulary* vocabulary = (ABAVocabulary *) self;
        return vocabulary.unit;
    }
    
    if ([self isKindOfClass:[ABASpeak class]])
    {
        ABASpeak* speak = (ABASpeak *) self;
        return speak.unit;
    }
    
    if ([self isKindOfClass:[ABASpeak class]])
    {
        ABASpeak* speak = (ABASpeak *) self;
        return speak.unit;
    }
    
    if ([self isKindOfClass:[ABAInterpret class]])
    {
        ABAInterpret* interpret = (ABAInterpret *) self;
        return interpret.unit;
    }
    
    if ([self isKindOfClass:[ABAExercises class]])
    {
        ABAExercises* exercises = (ABAExercises *) self;
        return exercises.unit;
    }
    
    if ([self isKindOfClass:[ABAFilm class]])
    {
        ABAFilm* film = (ABAFilm *) self;
        return film.unit;
    }
    
    if ([self isKindOfClass:[ABAVideoClass class]])
    {
        ABAVideoClass* film = (ABAVideoClass *) self;
        return film.unit;
    }
    
    if ([self isKindOfClass:[ABAEvaluation class]])
    {
        ABAEvaluation* evaluation = (ABAEvaluation *)self;
        return evaluation.unit;
    }
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"Not implemented: %@", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

-(float)getPercentageCertificateMaxProgress:(float)maxProgress {
    
    CGFloat progressInt = [self.progress floatValue];
    CGFloat percentage;

    if (progressInt > 100) {
        progressInt = 100;
    }
    
    if(progressInt > 0) {
        percentage = (maxProgress / 100.0) * progressInt;
//        percentage = (maxProgress * progressInt) / 100.0;
    }
    else {
        percentage = 0.0f;
    }
    
    return percentage;
}

@end
