//
//  ABAPlanResponseSerializer.m
//  ABA
//
//  Created by MBP13 Jesus on 08/01/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "ABAPlanResponseSerializer.h"
#import "ABAPlan+Methods.h"
#import "Utils.h"

@implementation ABAPlanResponseSerializer

#pragma mark - AFURLResponseSerialization

- (NSDictionary * _Nullable)responseObjectForResponse:(nullable NSURLResponse *)response
                                    data:(nullable NSData *)data
                                   error:(NSError * __nullable __autoreleasing *)error
{
    NSDictionary * jsonDictionary = [super responseObjectForResponse:response data:data error:error];
    NSDictionary * plansDictionary = [self parseABAPlans:jsonDictionary];
    return plansDictionary;
}

#pragma mark - Private methods

- (NSDictionary *)parseABAPlans:(NSDictionary *)object {
    
    NSMutableArray *abaProducts = @[].mutableCopy;
    
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
    
    
    for (NSDictionary *productInfo in object[@"tiers"])
    {
        ABAPlan * plan = [[ABAPlan alloc] init];
        plan.discountIdentifier = productInfo[@"discountidentifier"];
        plan.originalIdentifier = productInfo[@"originalidentifier"];
        plan.days = [productInfo[@"days"] integerValue];
        plan.is2x1 = [productInfo[@"is2x1"] isEqualToString:@"1"];
        
        [abaProducts addObject:plan];
    }
    
    [myDictionary setObject:abaProducts  forKey:@"products"];
    
    if ([object objectForKey: @"text"] != nil) {
        [myDictionary setObject:object[@"text"]  forKey:@"text"];
    }
    
    return myDictionary;
}

@end
