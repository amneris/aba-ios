//
//  ABARealmUser+MixpanelTracking.h
//  ABA
//
//  Created by Jesus Espejo on 19/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABARealmUser.h"
#import "TrackingManagerProtocol.h"

@interface ABARealmUser(GenericTrackingManager) < ABATrackingManagerProtocol >

@end
