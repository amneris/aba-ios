//
//  RegisterInteractor.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

class RegisterInteractor {

    let repository: RegisterRepository

    init(repository: RegisterRepository) {
        self.repository = repository
    }
}

extension RegisterInteractor: UnitDownloader {
    // Now capable of downloading units
}

extension RegisterInteractor: RegisterInteractorInput {

    func register(name: String, email: String, password: String) -> Observable<User> {
        return self.repository
            .registerWithEmail(name: name, email: email, password: password)
            .catchError { (error) -> Observable<UserDataModel> in
                if let error = error as? RegisterRepositoryError, error == RegisterRepositoryError.emailAlreadyExists {
                    throw SessionInteractorError.emailAlreadyExists
                }
                else {
                    throw SessionInteractorError.connectionError
                }
        }
            .flatMap { self.downloadUnits($0) }
            .catchError { if let error = $0 as? SessionInteractorError { throw error } else { throw SessionInteractorError.unableToDownloadUnits } }
            .do(onNext: { user in
                APIManager.shared().token = user.token
                DataController.saveProgressAction(for: nil, andUnit: nil, andPhrases: nil, errorText: nil, isListen: false, isHelp: false, completionBlock: nil)
            })
    }

    func registerWithFacebook(_ facebookUser: FacebookUser) -> Observable<User> {
        return self.repository
            .registerWithFacebook(facebookId: facebookUser.facebookId, name: facebookUser.firstName, surname: facebookUser.lastName, email: facebookUser.email, gender: facebookUser.gender, avatar: facebookUser.imageUrl)
            .catchError { _ in throw SessionInteractorError.connectionError }
            .flatMap { self.downloadUnits($0) }
            .catchError { error in throw SessionInteractorError.unableToDownloadUnits }
            .do(onNext: { user in
                APIManager.shared().token = user.token
                DataController.saveProgressAction(for: nil, andUnit: nil, andPhrases: nil, errorText: nil, isListen: false, isHelp: false, completionBlock: nil)
            })
    }
}
