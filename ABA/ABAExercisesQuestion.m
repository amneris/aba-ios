//
//  ABAExercisesQuestion.m
//  ABA
//
//  Created by Marc Güell Segarra on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAExercisesGroup.h"
#import "ABAExercisesPhrase.h"
#import "ABAExercisesQuestion.h"

@interface ABAExercisesQuestion ()
@property (readonly) RLMLinkingObjects<ABAExercisesGroup *> *questions;
@end

@implementation ABAExercisesQuestion

- (ABAExercisesGroup *)exercisesGroup
{
    return self.questions.firstObject;
}

+ (NSDictionary *)linkingObjectsProperties
{
    return @{
        @"questions" : [RLMPropertyDescriptor descriptorWithClass:ABAExercisesGroup.class propertyName:@"questions"],
    };
}

@end