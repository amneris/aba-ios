//
//  LoginRepository.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import Moya

/// Enum describing errors that can occur during unboxing
enum LoginRepositoryError: Swift.Error, CustomStringConvertible {
    var description: String { return "LoginRepositoryError.." }
    // Thrown when there is an error returned from the server
    case wrongUserPassword
    case networkErrorResponse
    case invalidData
    case invalidToken
    case serverOffline
    case deviceIsOffline
}

class LoginRepository {

    func login(email: String, password: String) -> Observable<UserDataModel> {
        return loginAPICall(email: email, password: password)
            .do(onNext: {  self.saveUser($0) })
    }

    func login(token: String) -> Observable<UserDataModel> {
        return loginAPICall(token: token)
            .do(onNext: {  self.saveUser($0) })
            .catchError { error -> Observable<UserDataModel> in
                
                print("error \(error)")

                switch error {
                case Moya.MoyaError.underlying(_):
                    throw LoginRepositoryError.deviceIsOffline
                default:
                    return Observable.error(error)
                }
            }
    }

    func loginWithFacebook(facebookId id: String, name: String, surname: String, email: String, gender: String, avatar: String) -> Observable<UserDataModel> {
        return registerWithFacebookAPICall(facebookId: id, name: name, surname: surname, email: email, gender: gender, avatar: avatar)
            .do(onNext: {  self.saveUser($0) })
    }
}

extension LoginRepository: FacebookRegistrationRepository {

}

// MARK: Private methods

private extension LoginRepository {
    func loginAPICall(email: String, password: String) -> Observable<UserDataModel> {
        let parameters = LoginParameters(deviceId: ABASystem.sharedInstance.deviceId, sysOper: ABASystem.sharedInstance.sysOper, deviceName: ABASystem.sharedInstance.deviceName, email: email, password: password, appVersion: ABASystem.sharedInstance.appVersion)
        return APIProvider
            .request(.login(loginParameters: parameters)).map({ (response) -> Moya.Response in
                switch response.statusCode {
                case 401:
                    throw LoginRepositoryError.wrongUserPassword
                default:
                    break
                }
                return response
        })
            .mapJSON()
            .map { userDictionary -> UserDataModel in
                if let _ = try NetworkErrorParser.parseNetworkError(userDictionary) {
                    throw LoginRepositoryError.networkErrorResponse
                }
                if let user = try NetworkUserParser.parseNetworkUser(userDictionary)
                {
                    return user as UserDataModel
                }
                throw LoginRepositoryError.invalidData
        }
    }

    func loginAPICall(token: String) -> Observable<UserDataModel> {
        return APIProvider
            .request(.loginWithToken(token: token, platform: ABASystem.sharedInstance.devicePlatform)).map({ (response) -> Moya.Response in
                if response.statusCode > 400 && response.statusCode < 500 {
                    throw LoginRepositoryError.invalidToken
                }
                if response.statusCode >= 500 {
                    throw LoginRepositoryError.serverOffline
                }
                return response
        })
            .mapJSON()
            .map { userDictionary -> UserDataModel in
                if let _ = try NetworkErrorParser.parseNetworkError(userDictionary) {
                    throw LoginRepositoryError.networkErrorResponse
                }
                if let user = try NetworkUserParser.parseNetworkUser(userDictionary)
                {
                    return user as UserDataModel
                }
                throw LoginRepositoryError.invalidData
        }
    }
}

// MARK: Storage

extension LoginRepository {
    func saveUser(_ user: UserDataModel) {
        UserDataModelDAO.saveUser(user)
    }
}
