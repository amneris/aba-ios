//
//  PurchaseHandler.h
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"

@class PurchaseHandler;

@interface PurchaseHandler : NSObject

+ (PurchaseHandler *)sharedInstance;
- (void)handlePurchaseStatus:(SubscriptionStatus)status;

@end
