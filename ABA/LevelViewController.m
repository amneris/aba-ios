//
//  LevelViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 31/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "LevelViewController.h"
#import "AppDelegate.h"
#import "Resources.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "LevelCell.h"
#import "ABARealmLevel.h"
#import "ABARealmUser.h"
#import "Utils.h"
#import "ABAAlert.h"
#import "UIViewController+ABA.h"
#import "LevelCell.h"
#import "SDVersion.h"

// Analytics
#import "LegacyTrackingManager.h"
#import "MixpanelTrackingManager.h"
#import "FabricTrackingManager.h"
#import "ABA-Swift.h"
@import SVProgressHUD;

static NSString *CellIdentifierLevel = @"LevelCell";
static NSString *CellIdentifierLevelContent = @"LevelCellContent";

static float minHeighCollectionCell = 80.0f;

@interface LevelViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, LevelCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@property RLMResults *dataSource;
@property (nonatomic, strong) NSIndexPath * selectedIndexPath;
@property (nonatomic, strong) UILabel * ghostLavelView;

@property BOOL helpHasShown;
@property UIView *infoView;
@property (nonatomic, strong) ABARealmLevel * infoLevel;
@property NSTimer *infoViewTimer;
@property UITapGestureRecognizer *singleHelpTap;
@property (nonatomic, strong) NSIndexPath * indexSelected;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentHeightConstraint;

@end

@implementation LevelViewController

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self != nil)
    {
        // We init the data controllers
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupView];
    
    [self setupConstants];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"levelselection"];
    
    _helpHasShown = NO;
    CLS_LOG(@"LevelViewController: entering levelVC");

}

-(void)setupConstants {
    if(IS_IPAD) {
        _commentHeightConstraint.constant = 60.0f;
    }
}

-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCollection) name:kUpdateRotationFromMenu object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)reloadCollection {
     __weak typeof(self) weakSelf = self;
    
	dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.collectionView reloadData];
    });
}

-(void)setupView
{
    if(_presentedFromMenu)
    {
        [[LegacyTrackingManager sharedManager] trackPageView:@"levelchange"];
        [self addToolbarAndMenuButtonWithTitle:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"levelsKey", @"Niveles")];
        
        NSString *commentString = STRTRAD(@"chooseLevelCommentKey", @"Puesdes modificar tu nivel del curso");
        [_commentLabel setText:commentString];
        [_commentLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    }
    else  // First time that you choose level
    {
        [[LegacyTrackingManager sharedManager] trackPageView:@"levelselection"];
        [self addToolbarWithTitle: STRTRAD(@"chooseLevelKey", @"Para empezar, elige un nivel")];
        
        NSString *commentString = STRTRAD(@"chooseLevelFirstCommentKey", @"Puesdes modificar tu nivel del curso después");
        [_commentLabel setText:commentString];
        [_commentLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    }

    _dataSource = [LevelUnitController getAllLevelsDescending];
	
    [self reloadCollection];
}

-(void) viewDidLayoutSubviews {
    
    if([SDVersion deviceSize] == Screen3Dot5inch) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_dataSource.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    }
}

#pragma mark - UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    if (collectionView == _collectionView) {

        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        
        ABARealmLevel *level = [_dataSource objectAtIndex:indexPath.row];
        ABARealmLevel *fromLevel = [UserController returnCurrentLevel];
        
        [UserController setCurrentLevel:level completionBlock:^(NSError *error, id object)
         {
             [SVProgressHUD dismiss];
             
             if(weakSelf.newRegister)
             {
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:@"1" forKey:newregister];
                 [defaults synchronize];
             }
             
             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
             
             if(!error && object)
             {
                 [[MixpanelTrackingManager sharedManager] trackLevelChange:fromLevel toLevel:level];
                 [[FabricTrackingManager sharedManager] trackLevelChange:fromLevel toLevel:level];
                 [CoolaDataTrackingManager trackLevelChange:[UserController currentUser].idUser newLevelId:level.idLevel];
                 [appDelegate.presenter loadMainStoryboard];
             }
             else
             {
                 if (error.code == CODE_CONNECTION_ERROR)
                 {
                     //Error de conexión
                     ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:error.localizedDescription];
                     [abaAlert show];
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"errorSettingCurrentUnit", @"error en settear currentLevel")];
                                        [abaAlert show];
                                    });
                 }
             }
             
             [appDelegate syncProgress];
         }];
    }
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (collectionView == _collectionView){
        return [_dataSource count];
    }
    
    return 1;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _collectionView){
    
        LevelCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifierLevel forIndexPath:indexPath];
        
        UILabel * cellNumberLabel = (UILabel*)[cell viewWithTag:10];
        UILabel * cellTitleLabel = (UILabel*)[cell viewWithTag:11];
        UIButton * cellImageButton = (UIButton*)[cell viewWithTag:12];
        UIImageView * cellImage = (UIImageView*)[cell viewWithTag:13];
        
        if(indexPath.row == _selectedIndexPath.row && _selectedIndexPath != nil) {
            cellImage.hidden = NO;
            [cellImageButton setImage:[UIImage imageNamed:@"levelsInfoGrey"] forState:UIControlStateNormal];
        }
        else {
            cellImage.hidden = YES;
            [cellImageButton setImage:[UIImage imageNamed:@"levelsInfoBlue"] forState:UIControlStateNormal];
        }
        
        UICollectionView * descriptionCollection = (UICollectionView*)[cell viewWithTag:2];
        
        ABARealmLevel *level = [_dataSource objectAtIndex:indexPath.row];
        ABARealmUser *currentUser = [UserController currentUser];
        
        cellNumberLabel.text = level.idLevel;
        [cellNumberLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?22:18]];
        cellNumberLabel.layer.cornerRadius = cellNumberLabel.frame.size.width/2;
        [cellNumberLabel setTextColor:ABA_COLOR_DARKGREY];
        cellNumberLabel.backgroundColor = ABA_COLOR_REG_BACKGROUND;
        
        cellTitleLabel.text = level.name;
        
        if([level isEqualToObject:currentUser.currentLevel] && _presentedFromMenu) {

            cell.backgroundColor = ABA_COLOR_DARKGREY;
            
            [cellTitleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?22:18]];
            [cellTitleLabel setTextColor:[UIColor whiteColor]];
            
        }
        else {

            cell.backgroundColor = [UIColor whiteColor];
            
            [cellTitleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?22:18]];
            [cellTitleLabel setTextColor:ABA_COLOR_DARKGREY];
            
        }
		
        [cell setDelegate:self];
        
        [cell.collectionView setBackgroundColor:ABA_COLOR_LIGHTGREY3];
		
		dispatch_async(dispatch_get_main_queue(), ^
					   {
						   [descriptionCollection reloadData];
						   [cell.collectionView reloadData];
					   });

        return cell;
    }
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifierLevelContent forIndexPath:indexPath];

    UILabel * cellDescriptionLabel = (UILabel*)[cell viewWithTag:20];
    cellDescriptionLabel.textColor = ABA_COLOR_INTRO_TITLE_BUT;
    [cellDescriptionLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    cellDescriptionLabel.textAlignment = NSTextAlignmentLeft;
    
    ABARealmLevel *level = [_dataSource objectAtIndex:_selectedIndexPath.row];
    
    cellDescriptionLabel.text = STRTRAD(level.desc, @"descripción nivel");
    cellDescriptionLabel.numberOfLines = 0;
    
    cell.backgroundColor = ABA_COLOR_LIGHTGREY3;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat ghostlabelwidth = self.view.bounds.size.width - 40.0; // 40 es el valor de las constraints left&right del label
    CGFloat labelHeight = 0;
    if (collectionView == _collectionView){
        ABARealmLevel *level = [_dataSource objectAtIndex:indexPath.row];
        CGFloat height = minHeighCollectionCell;
        if (_selectedIndexPath && _selectedIndexPath.row == indexPath.row){
            if (_selectedIndexPath && _selectedIndexPath.row == indexPath.row){
                
                _ghostLavelView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ghostlabelwidth, 9999)];
                _ghostLavelView.textAlignment = NSTextAlignmentLeft;

                _ghostLavelView.text = STRTRAD(level.desc, @"descripción nivel");
                _ghostLavelView.textColor = ABA_COLOR_INTRO_TITLE_BUT;
                [_ghostLavelView setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
                _ghostLavelView.numberOfLines = 0;
                [_ghostLavelView sizeToFit];
                labelHeight = _ghostLavelView.frame.size.height + 30;
            }
            height+=labelHeight;
            
            return CGSizeMake(self.view.frame.size.width, height);
        }
        return CGSizeMake(self.view.frame.size.width, minHeighCollectionCell);
    }

    _ghostLavelView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ghostlabelwidth, 9999)];
    _ghostLavelView.textAlignment = NSTextAlignmentLeft;

    NSIndexPath *indexPath2 = [_collectionView indexPathForCell:(UICollectionViewCell*)collectionView.superview.superview];
    
    ABARealmLevel *level = [_dataSource objectAtIndex:indexPath2.row];
    
    _ghostLavelView.text = STRTRAD(level.desc, @"descripción nivel");
    _ghostLavelView.textColor = ABA_COLOR_INTRO_TITLE_BUT;
    [_ghostLavelView setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    _ghostLavelView.numberOfLines = 0;
    [_ghostLavelView sizeToFit];
//    NSLog(@"HEIGHT : %f", _ghostLavelView.frame.size.height + 30);
//    NSLog(@"WIDTH : %f", ghostlabelwidth);
//    NSLog(@"HEIGHT COLLECTION : %f", collectionView.frame.size.height);
    
    return CGSizeMake(self.view.frame.size.width, _ghostLavelView.frame.size.height + 30);
}

#pragma mark - Delegate

-(void) infoButtonPressed:(LevelCell *)sender {
    
    NSIndexPath *indexPath = [_collectionView indexPathForCell:sender];
    
    __weak typeof(self) weakSelf = self;

    [_collectionView performBatchUpdates:^{
        // append your data model to return a larger size for
        // cell at this index path
        if(weakSelf.selectedIndexPath != nil && weakSelf.selectedIndexPath.row != indexPath.row) {
            [weakSelf hideImageAtIndexPath:weakSelf.selectedIndexPath];
        }
        if (weakSelf.selectedIndexPath && weakSelf.selectedIndexPath.row == indexPath.row){
            [weakSelf hideImageAtIndexPath:weakSelf.selectedIndexPath];
            weakSelf.selectedIndexPath = nil;
        }else{
            weakSelf.selectedIndexPath = indexPath;
            [weakSelf showImageAtIndexPath:indexPath];
        }
    } completion:^(BOOL finished) {
        [weakSelf.collectionView scrollToItemAtIndexPath:weakSelf.selectedIndexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
        
    }];
}

-(void) showImageAtIndexPath:(NSIndexPath *) indexPath {

    UICollectionViewCell * cell = [_collectionView cellForItemAtIndexPath:indexPath];
    UIImageView * cellImage = (UIImageView*)[cell viewWithTag:13];
    
    cellImage.hidden = NO;
    
    UIButton * cellImageButton = (UIButton*)[cell viewWithTag:12];
    [cellImageButton setImage:[UIImage imageNamed:@"levelsInfoGrey"] forState:UIControlStateNormal];
}

-(void) hideImageAtIndexPath:(NSIndexPath *) indexPath {
    
    UICollectionViewCell * cell = [_collectionView cellForItemAtIndexPath:indexPath];
    UIImageView * cellImage = (UIImageView*)[cell viewWithTag:13];
    cellImage.hidden = YES;
    
    UIButton * cellImageButton = (UIButton*)[cell viewWithTag:12];
    [cellImageButton setImage:[UIImage imageNamed:@"levelsInfoBlue"] forState:UIControlStateNormal];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self reloadCollection];
}

@end
