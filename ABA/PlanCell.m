//
//  PlanCell.m
//  ABA
//
//  Created by Jordi Mele on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "PlanCell.h"
#import "Resources.h"
#import "Utils.h"

@implementation PlanCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
	[_titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?36:24]];
    [_titleLabel setTextColor:ABA_COLOR_DARKGREY];
    
    [_priceLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?24:16]];
    [_priceLabel setTextColor:ABA_COLOR_BLUE_INFO];
    
    _originalPriceLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?18:11];
    _originalPriceLabel.textColor = ABA_COLOR_LIGHTGREY3;
    
    [_descriptionLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:11]];
    [_descriptionLabel setTextColor:ABA_COLOR_DARKGREY];
	
	[_subPhrase setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:11]];
	[_subPhrase setTextColor:ABA_COLOR_DARKGREY];
	[_subPhrase setText:STRTRAD(@"plansSubphraseKey", @"Acceso ilimitado a todos los niveles")];
    
    [_buttonLabel setBackgroundColor:ABA_COLOR_BLUE_INFO];
    [_buttonLabel.titleLabel setTextColor:[UIColor whiteColor]];
	[_buttonLabel.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:16]];
    _buttonLabel.layer.cornerRadius = 3.0f;
    
    _offer2x1ImageView.transform = CGAffineTransformMakeRotation(-18 * M_PI/180);
}

@end
