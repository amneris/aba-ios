//
//  ABAUtilsHelper.m
//  ABA
//
//  Created by Jaume on 19/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAUtilsHelper.h"
#import "LevelUnitController.h"
#import "ABARealmLevel.h"
#import "ABAUnit.h"

@implementation ABAUtilsHelper

+ (ABAUnit *)getRandomUnit
{
    ABARealmLevel *randomLevel = [ABAUtilsHelper getRandomLevel];
    return [ABAUtilsHelper getRandomUnitFromLevel:randomLevel];
}

+ (ABARealmLevel *)getRandomLevel
{
    ABARealmLevel *_randomLevel = nil;
    RLMResults *levels = [LevelUnitController getAllLevels];

    if ([levels count] > 0)
    {
        int newLevelIndex = arc4random_uniform((int)[levels count]);
        _randomLevel = [levels objectAtIndex:newLevelIndex];
    }

    return _randomLevel;
}

+ (ABAUnit *)getRandomUnitFromLevel:(ABARealmLevel *)level
{
    if (level != nil)
    {        
        int newUnitIndex = arc4random_uniform((int)[level.units count]);

        return [level.units objectAtIndex:newUnitIndex];
    }

    return nil;
}

@end
