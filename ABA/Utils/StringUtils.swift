//
//  StringUtils.swift
//  ABA
//
//  Created by MBP13 Jesus on 26/02/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

class StringUtils: NSObject {
    class func randomStringWithLength(_ length: Int) -> String {
        let charset: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString: NSMutableString = NSMutableString(capacity: length)
        let lettersLength = UInt32 (charset.length)
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(lettersLength)
            randomString.appendFormat("%C", charset.character(at: Int(rand)))
        }
        
        return randomString as String
    }
}
