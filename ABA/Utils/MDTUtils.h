//
//  MDTUtils.h
//  ABA
//
//  Created by Jordi Mele on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define CHARACTERS_NUMBERS [CHARACTERS stringByAppendingString:@"1234567890"]
#define CHARACTERS_TEXT @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'- "

@interface MDTUtils : NSObject


+ (BOOL)isNameEmpty:(NSString *)name;

//isEmailValid...
//i al mail que tngui un @
+ (BOOL)isEmailValid:(NSString *)string;

//Als camps de text es mira que contingui text
+ (BOOL)isTextEmpty:(NSString *)string;

//isCorrectPassword...
+ (BOOL)isCorrectPassword:(NSString *)string;

//Formateja string per tenir solament CHARACTERS_TEXT
+ (NSString *)formatText:(NSString *)phrase;

@end
