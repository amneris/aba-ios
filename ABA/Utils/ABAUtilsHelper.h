//
//  ABAUtilsHelper.h
//  ABA
//
//  Created by Jaume on 19/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ABAUnit, ABARealmLevel;

@interface ABAUtilsHelper : NSObject

+ (ABAUnit *)getRandomUnit;

+ (ABARealmLevel *)getRandomLevel;

+ (ABAUnit *)getRandomUnitFromLevel:(ABARealmLevel *)level;

@end
