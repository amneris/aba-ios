//
//  FilesUtils.swift
//  ABA
//
//  Created by MBP13 Jesus on 04/01/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

class FilesUtils {
 
    class func deleteCachedUnitFiles() {
        let fileManager = FileManager.default
        let cachePath =  fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
        let cachePathString = cachePath.path
        
        do {
            if let cachePathString = cachePathString
            {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(cachePathString)")
                
                try fileNames.filter { filename -> Bool in
                    return filename.contains("unit")
                }.forEach({ (filename) in
                    let filePathName = "\(cachePathString)/\(filename)"
                    try fileManager.removeItem(atPath: filePathName)
                })
                
                #if DEBUG
                    let files = try fileManager.contentsOfDirectory(atPath: "\(cachePathString)")
                    print("all files in cache after deleting files: \(files)")
                #endif
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    private static let averageUnitSize = 30000000.0 // 30MB in bytes
    
    class func calculateUnitFilesWeigth() -> Double  {
        let fileManager = FileManager.default
        let cachePath =  fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
        let cachePathString = cachePath.path
        
        do {
            if let cachePathString = cachePathString
            {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(cachePathString)")
                
                let sizeSum =
                        fileNames
                        .filter { filename -> Bool in
                            return filename.contains("unit")
                        }
                        .map { unitFolderName -> [Double]? in
                            // Skipping all the file calculation. Using average
                            /*
                            let directoryPathName = "\(cachePathString)/\(unitFolderName)"
                            let filesInDirectory = try fileManager.contentsOfDirectory(atPath: directoryPathName)
                            
                            return try filesInDirectory.map { (fileInDir) -> Double in
                                let filePath = "\(directoryPathName)/\(fileInDir)"
                                let fileAttributes = try fileManager.attributesOfItem(atPath: filePath)
                                let fileWeight = fileAttributes[FileAttributeKey.size] as! Double
                                return fileWeight
                            }
                            */
                            
                            return [averageUnitSize]
                        }
                        .reduce(0.0, { (accumulated, weightArray) -> Double in
                            guard let weightArray = weightArray else { return accumulated }
                            return weightArray.reduce(0.0, {$0 + $1}) + accumulated
                        })
                
                return sizeSum / 1024.0 / 1024.0
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
        
        return 0.0
    }
}
