//
//  MDTUtils.m
//  ABA
//
//  Created by Jordi Mele on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "MDTUtils.h"

@implementation MDTUtils

+ (BOOL)isNameEmpty:(NSString *)name
{
    if (name != nil && [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0)
    {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isEmailValid:(NSString *)string
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];

    return [emailTest evaluateWithObject:string];
}

+ (BOOL)isTextEmpty:(NSString *)string
{
    if ([string isEqualToString:@""] || string == nil)
    {
        return YES;
    }

    return NO;
}

+ (BOOL)isCorrectPassword:(NSString *)string
{
    if (string.length >= 6)
    {
        return YES;
    }

    return NO;
}

+ (NSString *)formatText:(NSString *)phrase
{
    NSString *phraseFormated;

    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:CHARACTERS_TEXT] invertedSet];
    phraseFormated = [[phrase componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];

    NSString *firstCharacter = [phraseFormated substringToIndex:1];
    if (![firstCharacter isEqualToString:@"I"])
    {
        firstCharacter = [firstCharacter lowercaseString];
    }

    NSRange range = NSMakeRange(1, phraseFormated.length - 1);
    phraseFormated = [NSString stringWithFormat:@"%@%@", firstCharacter, [phraseFormated substringWithRange:range]];

    return phraseFormated;
}

@end
