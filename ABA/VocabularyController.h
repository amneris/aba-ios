//
//  VocabularyController.h
//  ABA
//
//  Created by Jordi Mele on 2/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"
#import "Blocks.h"

@class ABAVocabulary, ABAVocabularyPhrase, ABAPhrase;

@interface VocabularyController : SectionController

/**
 *  Get number of ABAVocabularyPhrase completed
 *
 *  @param vocabulary      ABAVocabulary object
 *  @return                NSInteger
 */
-(NSInteger) getTotalPhraseCompletedForVocabularySection: (ABAVocabulary *)vocabulary;

-(NSInteger) getTotalPhraseDoneForVocabularySection: (ABAVocabulary *)vocabulary;

-(NSArray *)getAllAudioIDsForVocabularySection: (ABAVocabulary *)vocabulary;

-(void) completeAllSection:(ABAVocabulary *)vocabulary;

-(void)sendAllPhrasesDoneForVocabularySection: (ABAVocabulary *)vocabulary completionBlock:(CompletionBlock)block;

-(ABAVocabularyPhrase*) getRandomPhraseForVocabularySection:(ABAVocabulary *)vocabulary;

-(ABAPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onVocabulary: (ABAVocabulary*) vocabulary;

-(BOOL) isSectionCompleted: (ABASection*) section;

@end
