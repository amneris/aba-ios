//
//  ABAPopup.h
//  ABA
//
//  Created by Oriol Vilaró on 25/8/15.
//  Refactored by Jesus on 24/01/2017.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@class ABAPopup;

@interface ABAPopup : UIView

+ (ABAPopup *)abaPopupWithType:(kABAPopupType)type;
- (void)showInView:(UIView *)contentView;
- (void)dismiss;

@end
