//
//  PlansViewOutput.h
//  ABA
//
//  Created by MBP13 Jesus on 14/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PlansViewOutput <NSObject>

- (void)getPlans:(NSString *)userId;

@end
