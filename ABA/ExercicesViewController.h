//
//  ExercicesViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "SectionProtocol.h"
#import "ABAExercises.h"

@class ABAExercisesQuestion, ABAExercisesGroup, ABAExercisesPhrase;

@interface ExercicesViewController : SectionBaseViewController

@property (strong, nonatomic) ABAExercises *section;
@property (strong, nonatomic) ABAExercisesGroup *currentGroup;
@property (strong, nonatomic) ABAExercisesQuestion *currentQuestion;
@property (weak) id <SectionProtocol> delegate;

@end
