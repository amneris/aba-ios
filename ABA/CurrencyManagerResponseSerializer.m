//
//  CurrencyManagerResponseSerializer.m
//  ABA
//
//  Created by MBP13 Jesus on 10/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "CurrencyManagerResponseSerializer.h"
#import "CurrencyPairModel.h"

@interface CurrencyManagerResponseSerializer() < NSXMLParserDelegate >
@property (nonatomic, strong) NSMutableArray * arrayOfCurrencies;
@property (nonatomic, strong) CurrencyPairModel * parsingCurrencyPair;
@property (nonatomic, strong) NSMutableString * accumulatedString;
@end

@implementation CurrencyManagerResponseSerializer

#pragma mark - AFURLResponseSerialization

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    NSXMLParser * xmlParser = [super responseObjectForResponse:response data:data error:error];
    xmlParser.delegate = self;
    [xmlParser parse];
    
    return [NSArray arrayWithArray:_arrayOfCurrencies];
}

#pragma mark - NSXMLParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    _arrayOfCurrencies = [NSMutableArray array];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"rate"])
    {
        // Create rate
        _parsingCurrencyPair = [[CurrencyPairModel alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"rate"])
    {
        // Adding currency pair to the array as long as it's properly set
        if ([_parsingCurrencyPair isProperlySet])
        {
            [self.arrayOfCurrencies addObject:_parsingCurrencyPair];
        }
    }
    else if ([elementName isEqualToString:@"Name"])
    {
        // Set rate name
        [_parsingCurrencyPair setCurrencyPairCode:self.accumulatedString];
    }
    else if ([elementName isEqualToString:@"Rate"])
    {
        // Add rate to array
        [_parsingCurrencyPair setCurrencyRate:self.accumulatedString];
    }
    
    self.accumulatedString = nil;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!_accumulatedString)
    {
        self.accumulatedString = [[NSMutableString alloc] initWithString:string];
    }
    else
    {
        [self.accumulatedString appendString:string];
    }
}

@end
