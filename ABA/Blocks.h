//
//  Blocks.h
//  ABA
//
//  Created by Jordi Mele on 13/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#ifndef ABA_Blocks_h
#define ABA_Blocks_h

#import "ABASection+Methods.h"


/**
 *  Common Completion block
 *
 *  @param error    error or nil
 *  @param object   object returned
 */
typedef void (^CompletionBlock)(NSError *error, id object);


/**
 *  Common Completion block
 *
 *  @param error        error or nil
 *  @param object       object returned
 *  @param isNewUser    BOOL to know whether the user is new for ABA English or not.
 */
typedef void (^CompletionBlockFacebook)(NSError *error, id object, BOOL isNewUser);


/**
 *  Common Error block
 *
 *  @param error    error or nil
 */
typedef void (^ErrorBlock)(NSError *error);

/**
 *  Simple block
 */
typedef void(^SimpleBlock)();

/**
 *  ABAFilm Completion block
 *
 *  @param error    error or nil
 *  @param object   object returned
 */
typedef void (^ABASectionCompletionBlock)(NSError *error, ABASection* object);

/**
 *  Common Completion block
 *
 *  @param error    error or nil
 *  @param object   BOOL returned
 */
typedef void (^BoolCompletionBlock)(NSError *error, BOOL object);

/**
 *  Common Progress block
 *
 *  @param error          error or nil
 *  @param progress       current progress
 *  @param didFinish      finished status
 */
typedef void (^DownloadProgressBlock)(NSError *error, float progress, BOOL didFinish);


#endif
