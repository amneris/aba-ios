//
//  ABAExperiment.m
//  ABA
//
//  Created by Oriol Vilaró on 5/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAExperiment.h"
#import "Definitions.h"

@implementation ABAExperiment

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (!self)
    {
        return nil;
    }

    self.experimentIdentifier = [decoder decodeObjectForKey:kExperimentIdentifier];
    self.experimentVariationIdentifier = [decoder decodeObjectForKey:kExperimentVariationIdentifier];
    self.userExperimentVariationId = [decoder decodeObjectForKey:kUserExperimentVariationId];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.experimentIdentifier forKey:kExperimentIdentifier];
    [encoder encodeObject:self.experimentVariationIdentifier forKey:kExperimentVariationIdentifier];
    [encoder encodeObject:self.userExperimentVariationId forKey:kUserExperimentVariationId];
}

@end
