//
//  ABASystem.swift
//  ABA
//
//  Created by MBP13 Jesus on 01/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import UIKit

struct ABASystem {

    static let sharedInstance = ABASystem()

    let deviceId: String = UIDevice.current.identifierForVendor?.uuidString ?? "Unknown deviceId"
    let sysOper: String = UIDevice.current.systemVersion
    let appVersion: String = Bundle.main.releaseVersionNumber ?? "Unspecified app version"
    let devicePlatform: String = UIDevice.current.model
    
    var deviceName: String {
        get {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                return "iPhone"
            case .pad:
                return "iPad"
            // case .Unspecified:
            default:
                return "Unspecified device name"
            }
        }
    }
}

// Jesus: From http://stackoverflow.com/questions/25965239/how-do-i-get-the-app-version-and-build-number-using-swift

extension Bundle {
    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
}
