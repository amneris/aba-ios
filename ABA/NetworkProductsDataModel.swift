//
//  NetworkProductsDataModel.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/12/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Unbox
import Wrap

@objc
class NetworkProducts:NSObject, SelligentProducts, Unboxable {
    var text: String?
    var products: [SelligentSubscriptionProduct]? {
        get {
            return networkProducts
        }
    }
    var networkProducts : [NetworkSubscriptionProduct]?
    
    required init(unboxer: Unboxer) throws {
        self.text = unboxer.unbox(key: "text")
        self.networkProducts = unboxer.unbox(key: "tiers")
    }
}

@objc
class NetworkSubscriptionProduct:NSObject, SelligentSubscriptionProduct, Unboxable {
    var originalIdentifier: String?
    var discountIdentifier: String?
    var days: String?
    var promocode: String?
    var is2x1: String?
    
    required init(unboxer: Unboxer) throws {
        self.originalIdentifier = unboxer.unbox(key: "originalidentifier")
        self.discountIdentifier = unboxer.unbox(key: "discountidentifier")
        self.days = unboxer.unbox(key: "days")
        self.promocode = unboxer.unbox(key: "promocode")
        self.is2x1 = unboxer.unbox(key: "is2x1")
    }
}

