//
//  UserDataModel.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Unbox
import Wrap

class UserDataModel: NSObject, User, Unboxable, NSCoding {

    var countryNameIso: String?
    var email: String?
    var idSession: String?
    var idUser: String
    var name: String?
    var partnerID: String?
    var sourceID: String?
    var surnames: String?
    var teacherId: String?
    var teacherName: String?
    var teacherImage: String?
    var token: String
    var type: String
    var urlImage: String?
    var userLang: String?

    var gender: String?
    var birthDate: String?
    var phone: String?
    var externalKey: String?

    var expirationDate: Date?
    var lastLoginDate: Date?

    var userLevel: String?
    var isNewUser: Bool

    // MARK: Unboxable

    required init(unboxer: Unboxer) throws {
        self.countryNameIso = unboxer.unbox(key:"countryNameIso")
        self.email = unboxer.unbox(key:"email")
        // self.idSession = "random"
        self.idUser = try unboxer.unbox(key:"userId")
        self.name = unboxer.unbox(key:"name")
        self.partnerID = unboxer.unbox(key:"idPartner")
        self.sourceID = unboxer.unbox(key:"idSource")
        self.surnames = unboxer.unbox(key:"surnames")
        self.teacherId = unboxer.unbox(key:"teacherId")
        self.teacherName = unboxer.unbox(key:"teacherName")
        self.teacherImage = unboxer.unbox(key:"teacherImage")
        self.token = try unboxer.unbox(key:"token")
        self.type = try unboxer.unbox(key:"userType")
        self.urlImage = unboxer.unbox(key:"avatar")
        self.userLang = unboxer.unbox(key:"userLang")
        self.lastLoginDate = NSDate() as Date // current date is last login date

        self.gender = unboxer.unbox(key:"gender")
        self.birthDate = unboxer.unbox(key:"birthDate")
        self.phone = unboxer.unbox(key:"phone")
        self.externalKey = unboxer.unbox(key:"externalKey")

        // Dates
        let expirationDateFormatter = DateFormatter()
        expirationDateFormatter.dateFormat = "dd-MM-yyyy"
        self.expirationDate = unboxer.unbox(key:"expirationDate", formatter: expirationDateFormatter)

        self.userLevel = unboxer.unbox(key:"userLevel")

        if let profiles: [[String: AnyObject]] = unboxer.unbox(key:"profiles") {
            ExperimentRunner.saveExperiment(withExperimentProfile: profiles)
        }

        // Reading and storing cooladata properties into user defaults
        if let cooladata: [[String: AnyObject]] = unboxer.unbox(key:"cooladata") {
            CoolaDataTrackingManager.recordCooladataProperties(cooladata)
        }

        if let isNewUserIntegerValue: Int = unboxer.unbox(key:"isnewuser") {
            self.isNewUser = isNewUserIntegerValue == 1 ? true : false
        }
        else {
            self.isNewUser = false
        }
    }

    // MARK: NSCoding

    @objc required init?(coder aDecoder: NSCoder) {
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.idUser = aDecoder.decodeObject(forKey: "idUser") as! String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.token = aDecoder.decodeObject(forKey: "token") as! String
        self.type = aDecoder.decodeObject(forKey: "type") as! String

        self.countryNameIso = aDecoder.decodeObject(forKey: "countryNameIso") as? String
        self.idSession = aDecoder.decodeObject(forKey: "idSession") as? String
        self.partnerID = aDecoder.decodeObject(forKey: "partnerID") as? String
        self.sourceID = aDecoder.decodeObject(forKey: "sourceID") as? String
        self.surnames = aDecoder.decodeObject(forKey: "surnames") as? String
        self.teacherId = aDecoder.decodeObject(forKey: "teacherId") as? String
        self.teacherImage = aDecoder.decodeObject(forKey: "teacherImage") as? String
        self.teacherName = aDecoder.decodeObject(forKey: "teacherName") as? String
        self.urlImage = aDecoder.decodeObject(forKey: "urlImage") as? String
        self.userLang = aDecoder.decodeObject(forKey: "userLang") as? String
        self.gender = aDecoder.decodeObject(forKey: "gender") as? String
        self.birthDate = aDecoder.decodeObject(forKey: "birthDate") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.externalKey = aDecoder.decodeObject(forKey: "externalKey") as? String
        self.expirationDate = aDecoder.decodeObject(forKey: "expirationDate") as? Date
        self.lastLoginDate = aDecoder.decodeObject(forKey: "lastLoginDate") as? Date

        self.userLevel = aDecoder.decodeObject(forKey: "userLevel") as? String
        
        if aDecoder.containsValue(forKey: "isnewuser") {
            self.isNewUser = aDecoder.decodeBool(forKey: "isnewuser")
        }
        else {
            self.isNewUser = false
        }
    }

    @objc func encode(with aCoder: NSCoder) {
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.idUser, forKey: "idUser")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.token, forKey: "token")
        aCoder.encode(self.type, forKey: "type")

        aCoder.encode(self.countryNameIso, forKey: "countryNameIso")
        aCoder.encode(self.idSession, forKey: "idSession")
        aCoder.encode(self.partnerID, forKey: "partnerID")
        aCoder.encode(self.sourceID, forKey: "sourceID")
        aCoder.encode(self.surnames, forKey: "surnames")
        aCoder.encode(self.teacherId, forKey: "teacherId")
        aCoder.encode(self.teacherImage, forKey: "teacherImage")
        aCoder.encode(self.teacherName, forKey: "teacherName")
        aCoder.encode(self.urlImage, forKey: "urlImage")
        aCoder.encode(self.userLang, forKey: "userLang")
        aCoder.encode(self.gender, forKey: "gender")
        aCoder.encode(self.birthDate, forKey: "birthDate")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.externalKey, forKey: "externalKey")
        aCoder.encode(self.expirationDate, forKey: "expirationDate")
        aCoder.encode(self.lastLoginDate, forKey: "lastLoginDate")

        aCoder.encode(self.userLevel, forKey: "userLevel")
        aCoder.encode(self.isNewUser, forKey: "isnewuser")
    }
}

extension UserDataModel {
    func toDict() throws -> [String: AnyObject] {
        let dictionary: WrappedDictionary = try wrap(self)
        return dictionary as [String : AnyObject]
    }
}
