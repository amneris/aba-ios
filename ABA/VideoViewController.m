//
//  VideoViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAAlert.h"
#import "ABAFilm.h"
#import "ABASection+Methods.h"
#import "ABATeacherTipView.h"
#import "ABAUnit.h"
#import "ABAVideoClass.h"
#import "APIManager.h"
#import "DataController.h"
#import "FilmController.h"
#import "ImageHelper.h"
#import "LanguageController.h"
#import "LegacyTrackingManager.h"
#import "PureLayout.h"
#import "Resources.h"
#import "UIViewController+ABA.h"
#import "UserController.h"
#import "Utils.h"
#import "VideoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "ABA-Swift.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
@import SVProgressHUD;

typedef NS_ENUM(NSUInteger, kVideoSubtitlesType) {
    kVideoEnglishSubtitles,
    kVideoTranslatedSubtitles,
    kVideoNoneSubtitles,
};

static const float kSeparatorHeight = 1.0;

@interface VideoViewController ()

@property(assign, nonatomic) BOOL currentSubsAnimating, currentSubsShowed;

@property kVideoSubtitlesType selectedSubtitleOption, selectFirstCurrentSub, selectSecondCurrentSub;
@property NSMutableDictionary *subtitlesOptions;
@property CGFloat buttonOffset;

@property(weak, nonatomic) IBOutlet UIView *labelsView;
@property(weak, nonatomic) IBOutlet UILabel *unitLabel;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property(weak, nonatomic) IBOutlet UIImageView *bgImage;

@property(weak, nonatomic) IBOutlet UIView *playView;
@property(weak, nonatomic) IBOutlet UIView *playAlphaView;
@property(weak, nonatomic) IBOutlet UIButton *playButton;

- (IBAction)playAction:(id)sender;

@property(weak, nonatomic) IBOutlet UIView *selectSubsView;
@property(weak, nonatomic) IBOutlet UIView *selectSubsAlphaView;
@property(weak, nonatomic) IBOutlet UIButton *selectFirstSubsButton;
@property(weak, nonatomic) IBOutlet UIView *selectSubsSeparator;
@property(weak, nonatomic) IBOutlet UIButton *selectSecondSubsButton;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *selectSubsBottomContraint;

- (IBAction)selectFirstSubsAction:(id)sender;
- (IBAction)selectSecondSubsAction:(id)sender;

@property(weak, nonatomic) IBOutlet UIView *currentSubsView;
@property(weak, nonatomic) IBOutlet UIButton *currentSubsButton;

@property(strong, nonatomic) FilmController *filmController;

@property BOOL loadFilm;
@property(strong, nonatomic) UIImageView *arrowButtonImage;

- (IBAction)currentSubsAction:(id)sender;

@property ABAAlert *abaAlert;
@property ABATeacherTipView *abaTeacher;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *subtitleButtonHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *totalSubtitleButtonsHeightConstraint;

@property(nonatomic, strong) AVPlayerViewController *currentPlayerViewController;

@end

//
//@interface VideoViewController (Videplayer)
//
//@end

@implementation VideoViewController
@dynamic section;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        _filmController = [[FilmController alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [DataController saveProgressActionForSection:self.section andUnit:[self.section getUnitFromSection] andPhrases:nil errorText:nil isListen:NO isHelp:NO completionBlock:^(NSError *error, id object){

    }];

    [self loadSubtitles];

    [self setupView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (_abaAlert)
    {
        [_abaAlert dismiss];
    }
}

- (void)loadSubtitles
{
    _subtitlesOptions = [[NSMutableDictionary alloc] init];

    [_subtitlesOptions setObject:STRTRAD(@"subsEnglishSelected", @"subtítulos en inglés") forKey:[NSNumber numberWithInt:kVideoEnglishSubtitles]];
    [_subtitlesOptions setObject:STRTRAD(@"subsNoneSelected", @"sin subtítulos") forKey:[NSNumber numberWithInt:kVideoNoneSubtitles]];

    _buttonOffset = IS_IPAD ? 60 : 44;

    if (![[UserController getUserLanguage] isEqualToString:@"en"] && [self videoTranslatedSubtitlesAvailable])
    {
        [_subtitlesOptions setObject:STRTRAD(@"subsTranslatedSelected", @"subtítulos en español") forKey:[NSNumber numberWithInt:kVideoTranslatedSubtitles]];
    }
}

- (BOOL)videoTranslatedSubtitlesAvailable
{
    if (_currentSection == kABAFilmSectionType)
    {
        ABAFilm *film = (ABAFilm *)self.section;
        NSLog(@"%@", film.translatedSubtitles);
        return [film.translatedSubtitles length] > 0;
    }
    else
    {
        ABAVideoClass *videoclass = (ABAVideoClass *)self.section;
        NSLog(@"%@", videoclass.translatedSubtitles);
        return [videoclass.translatedSubtitles length] > 0;
    }
}

#pragma mark - IBActions

- (IBAction)playAction:(id)sender
{
    NSString *srtURL, *videoURL_SD, *videoURL_HD, *strURLDownloaded, *videoUrlDownloaded;
    BOOL videoDownloaded = false;

    // section specific setup
    if (_currentSection == kABAFilmSectionType)
    {

        ABAFilm *film = (ABAFilm *)self.section;

        videoDownloaded = [_filmController checkIfVideoExists:film forSectionType:kABAFilmSectionType];
        if (videoDownloaded)
        {
            videoUrlDownloaded = [_filmController getLocalVideoPath:film forSectionType:kABAFilmSectionType];
        }

        switch (self.selectedSubtitleOption)
        {
        case kVideoEnglishSubtitles:
        {
            srtURL = film.englishSubtitles;
            if (videoDownloaded)
            {
                strURLDownloaded = [_filmController getLocalSubtitlePath:film forSectionType:kABAFilmSectionType forSubtitle:kABASubtitleTypeEnglish];
            }
            break;
        }
        case kVideoTranslatedSubtitles:
        {
            srtURL = film.translatedSubtitles;
            if (videoDownloaded)
            {
                strURLDownloaded = [_filmController getLocalSubtitlePath:film forSectionType:kABAFilmSectionType forSubtitle:kABASubtitleTypeTranslate];
            }
            break;
        }
        case kVideoNoneSubtitles:
        default:
        {
            srtURL = nil;
            strURLDownloaded = nil;
            break;
        }
        }

        videoURL_SD = film.sdVideoURL;
        videoURL_HD = film.hdVideoURL;
    }
    else if (_currentSection == kABAVideoClassSectionType)
    {

        ABAVideoClass *videoClass = (ABAVideoClass *)self.section;

        videoDownloaded = [_filmController checkIfVideoExists:videoClass forSectionType:kABAVideoClassSectionType];
        if (videoDownloaded)
        {
            videoUrlDownloaded = [_filmController getLocalVideoPath:videoClass forSectionType:kABAVideoClassSectionType];
        }

        switch (self.selectedSubtitleOption)
        {
        case kVideoEnglishSubtitles:
        {
            srtURL = videoClass.englishSubtitles;
            if (videoDownloaded)
            {
                strURLDownloaded = [_filmController getLocalSubtitlePath:videoClass forSectionType:kABAVideoClassSectionType forSubtitle:kABASubtitleTypeEnglish];
            }
            break;
        }
        case kVideoTranslatedSubtitles:
        {
            srtURL = videoClass.translatedSubtitles;
            if (videoDownloaded)
            {
                strURLDownloaded = [_filmController getLocalSubtitlePath:videoClass forSectionType:kABAVideoClassSectionType forSubtitle:kABASubtitleTypeTranslate];
            }
            break;
        }
        case kVideoNoneSubtitles:
        default:
        {
            srtURL = nil;
            strURLDownloaded = nil;
            break;
        }
        }

        videoURL_SD = videoClass.sdVideoURL;
        videoURL_HD = videoClass.hdVideoURL;
    }

    BOOL reachability;
    NSString *videoURL;

    if ([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWWAN)
    {
        reachability = YES;
        videoURL = videoURL_SD;
    }
    else if ([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWiFi)
    {
        reachability = YES;
        videoURL = videoURL_HD;
    }
    else
    {
        reachability = NO;
    }

    __weak typeof(self) weakSelf = self;

    if (reachability || videoDownloaded)
    {
        if (videoDownloaded)
        {
            NSURL *downloadedVideoUrl = [[NSURL alloc] initFileURLWithPath:videoUrlDownloaded];
            NSURL *subtitleUrl = nil;
            if (strURLDownloaded != nil) {
                subtitleUrl = [[NSURL alloc] initFileURLWithPath:strURLDownloaded];
            }

            [self playVideoWithUrl:downloadedVideoUrl andSubtitle:subtitleUrl];
        }
        else
        {
            if (srtURL != nil)
            {
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                [[APIManager sharedManager] getContentsOfURL:srtURL completionBlock:^(NSError *error, id object) {

                    [SVProgressHUD dismiss];

                    if (error)
                    {
                        [weakSelf showAlertWithKey:@"errorConnection" andText:@"Imposible reproducir el video :("];
                        NSLog(@"error: %@", error);
                    }
                    else
                    {
                        NSURL *onlineVideoUrl = [[NSURL alloc] initWithString:videoURL];
                        NSURL *downloadedSubtitles = [self saveDataAsSubtitle:object];
                        [weakSelf playVideoWithUrl:onlineVideoUrl andSubtitle:downloadedSubtitles];
                    }
                }];
            }
            else
            {
                NSURL *onlineVideoUrl = [[NSURL alloc] initWithString:videoURL];
                [self playVideoWithUrl:onlineVideoUrl andSubtitle:nil];
            }
        }
    }
    else // No reachability
    {
        [self showAlertWithKey:@"errorConnection" andText:@"Imposible reproducir el video :("];
    }
}

- (IBAction)selectFirstSubsAction:(id)sender
{

    [self setSelectedSubtitles:_selectFirstCurrentSub animated:YES];
}

- (IBAction)selectSecondSubsAction:(id)sender
{

    [self setSelectedSubtitles:_selectSecondCurrentSub animated:YES];
}

- (IBAction)currentSubsAction:(id)sender
{

    if (_currentSubsShowed)
    {

        [self hideSelectSubtitles];
    }
    else
    {

        [self showSelectSubtitles];
    }
}

#pragma mark - custom methods

- (void)setupTeacherView
{
    _abaTeacher = [[ABATeacherTipView alloc] initTeacherWithText:@"" withImage:@"teacher" withShadow:NO];

    // section specific setup
    if (_currentSection == kABAFilmSectionType)
    {

        NSString *txt0 = STRTRAD(@"filmSectionTeacher1Key", @"Vas a ver un vídeo en inglés, para");
        NSString *str = [NSString stringWithFormat:@"%@", txt0];

        NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
        [mutAttStr addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 16 : 14]
                          range:[str rangeOfString:txt0]];

        [_abaTeacher setAttributedText:mutAttStr.copy];
    }
    else if (_currentSection == kABAVideoClassSectionType)
    {
        NSString *txt0 = STRTRAD(@"videoClassSectionTeacher1Key", @"Es el momento de pensar en la gramática");

        ABAUnit *unit = [self.section getUnitFromSection];
        if (![UserController isUserPremium] && [unit getIndex] != 1)
        {
            txt0 = STRTRAD(@"videoClassSectionTeacher1NOPremiumKey", @"Es el momento de pensar en la gramática");
        }

        NSString *str = [NSString stringWithFormat:@"%@", txt0];

        NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
        [mutAttStr addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 16 : 14]
                          range:[str rangeOfString:txt0]];

        [_abaTeacher setAttributedText:mutAttStr.copy];
    }

    if (![self.view.subviews containsObject:_abaTeacher] && ![self.section.completed isEqual:[NSNumber numberWithInt:1]])
    {
        [self.view addSubview:_abaTeacher];
        [_abaTeacher setConstraintsAttributedText];
    }
}

- (void)setupView
{
    NSString *percentage = @"0%";

    if ([self.section.completed isEqual:[NSNumber numberWithInt:1]])
    {
        percentage = @"100%";
    }

    if (![[UserController getUserLanguage] isEqualToString:@"en"])
    {
        if ([self videoTranslatedSubtitlesAvailable])
        {
            [self setSelectedSubtitles:kVideoTranslatedSubtitles animated:NO];
        }
        else
        {
            [self setSelectedSubtitles:kVideoEnglishSubtitles animated:NO];
        }
    }
    else
    {
        [self setSelectedSubtitles:kVideoEnglishSubtitles animated:NO];
    }

    // section specific setup
    if (_currentSection == kABAFilmSectionType)
    {

        [self addBackButtonWithTitle:STRTRAD(@"abaFilmKey", @"Aba Film") andSubtitle:percentage];

        ABAUnit *unit = [self.section getUnitFromSection];

        [_titleLabel setText:[unit.title uppercaseString]];

        NSString *unitString = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"unitKey", @"Unidad"), unit.idUnit];
        [_unitLabel setText:[unitString uppercaseString]];

        [_descriptionLabel setText:unit.desc];

        [[LegacyTrackingManager sharedManager] trackPageView:@"abafilmsection"];
    }
    else if (_currentSection == kABAVideoClassSectionType)
    {
        [self addBackButtonWithTitle:STRTRAD(@"abaVideClassKey", @"Videoclase") andSubtitle:percentage];

        //[_labelsView setHidden:YES];
        [[LegacyTrackingManager sharedManager] trackPageView:@"videoclasssection"];
    }

    if (!_loadFilm)
    {
        [self setupTeacherView];
    }

    //
    // Setup background image
    //
    NSString *imageUrlString;

    if (_currentSection == kABAVideoClassSectionType)
    {
        imageUrlString = [self.section getUnitFromSection].videoClassImageUrl;
    }
    else
    {
        imageUrlString = [self.section getUnitFromSection].filmImageUrl;
    }

    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:imageUrlString];
    NSURL *imageUrl = [NSURL URLWithString:urlString];

    [_bgImage sd_setImageWithURL:imageUrl];

    [_playAlphaView setBackgroundColor:ABA_COLOR_BLUE_MENU_UNIT];
    [_playAlphaView setAlpha:0.8f];
    [_playAlphaView.layer setCornerRadius:_playAlphaView.frame.size.width / 2.0f];
    [_playButton setImage:[UIImage imageNamed:@"playButton"] forState:UIControlStateNormal];
    [_playButton setAccessibilityLabel:@"playButton"];

    [_selectSubsAlphaView setBackgroundColor:ABA_COLOR_GREY_SUBS];
    [_selectSubsAlphaView setAlpha:0.8f];
    [_selectSubsSeparator setBackgroundColor:ABA_COLOR_GREY_SUBS];

    [_selectFirstSubsButton setTitleColor:ABA_COLOR_DARKGREY forState:UIControlStateNormal];
    [_selectFirstSubsButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 16 : 14]];
    [_selectFirstSubsButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
    _selectFirstSubsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    if ([_subtitlesOptions count] > 2)
    {
        [_selectSecondSubsButton setTitleColor:ABA_COLOR_DARKGREY forState:UIControlStateNormal];
        [_selectSecondSubsButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 16 : 14]];
        [_selectSecondSubsButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        _selectSecondSubsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    else
    {
        [_selectSecondSubsButton setHidden:YES];
    }

    [_currentSubsView setBackgroundColor:ABA_COLOR_DARKGREY];
    [_currentSubsButton setTintColor:[UIColor whiteColor]];
    [_currentSubsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_currentSubsButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 16 : 14]];
    _currentSubsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    _arrowButtonImage = [UIImageView newAutoLayoutView];
    _arrowButtonImage.image = [UIImage imageNamed:@"subsButton"];
    [_currentSubsButton addSubview:_arrowButtonImage];

    [_arrowButtonImage autoSetDimension:ALDimensionWidth toSize:_arrowButtonImage.image.size.width];
    [_arrowButtonImage autoSetDimension:ALDimensionHeight toSize:_arrowButtonImage.image.size.height];
    [_arrowButtonImage autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:15.0f];
    [_arrowButtonImage autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:-(_arrowButtonImage.image.size.height - _buttonOffset) / 2];

    [_currentSubsButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];

    if (IS_IPAD)
    {
        _subtitleButtonHeightConstraint.constant = 60.0f;
        _totalSubtitleButtonsHeightConstraint.constant = 121.0f;
    }
}

- (void)setSelectedSubtitles:(kVideoSubtitlesType)type animated:(BOOL)animated
{

    if (_currentSubsAnimating)
    {
        return;
    }

    _selectedSubtitleOption = type;

    NSString *currentSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:_selectedSubtitleOption]];

    [_currentSubsButton setTitle:currentSubsTitle forState:UIControlStateNormal];

    NSString *selectFirstSubsTitle, *selectSecondSubsTitle;

    switch (_selectedSubtitleOption)
    {
    case kVideoEnglishSubtitles:
    {
        selectFirstSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoTranslatedSubtitles]];

        if (selectFirstSubsTitle == nil)
        {
            selectFirstSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoNoneSubtitles]];
            _selectFirstCurrentSub = kVideoNoneSubtitles;
        }
        else
        {
            _selectFirstCurrentSub = kVideoTranslatedSubtitles;

            selectSecondSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoNoneSubtitles]];
            _selectSecondCurrentSub = kVideoNoneSubtitles;
        }

        break;
    }
    case kVideoTranslatedSubtitles:
    {
        selectFirstSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoEnglishSubtitles]];
        _selectFirstCurrentSub = kVideoEnglishSubtitles;

        selectSecondSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoNoneSubtitles]];
        _selectSecondCurrentSub = kVideoNoneSubtitles;

        break;
    }
    case kVideoNoneSubtitles:
    default:
    {
        selectFirstSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoEnglishSubtitles]];
        _selectFirstCurrentSub = kVideoEnglishSubtitles;

        selectSecondSubsTitle = [_subtitlesOptions objectForKey:[NSNumber numberWithInt:kVideoTranslatedSubtitles]];
        _selectSecondCurrentSub = kVideoTranslatedSubtitles;

        break;
    }
    }

    [_selectFirstSubsButton setTitle:selectFirstSubsTitle forState:UIControlStateNormal];
    [_selectSecondSubsButton setTitle:selectSecondSubsTitle forState:UIControlStateNormal];

    if (animated)
    {
        [self hideSelectSubtitles];
    }
    else
    {
        _selectSubsBottomContraint.constant = -(_buttonOffset + kSeparatorHeight);
        [self.view layoutIfNeeded];
        _currentSubsShowed = NO;
        _currentSubsAnimating = NO;
    }
}

- (void)showSelectSubtitles
{

    if (_currentSubsAnimating)
    {
        return;
    }

    _currentSubsAnimating = YES;
    [UIView animateWithDuration:0.3f animations:^{

        _selectSubsBottomContraint.constant = ([_subtitlesOptions count] > 2) ? _buttonOffset : 0;
        [self.view layoutIfNeeded];

    }
        completion:^(BOOL finished) {

            [_arrowButtonImage setImage:[UIImage imageNamed:@"subsButtonInverted"]];

            _currentSubsShowed = YES;
            _currentSubsAnimating = NO;
        }];
}

- (void)hideSelectSubtitles
{

    if (_currentSubsAnimating)
    {
        return;
    }

    _currentSubsAnimating = YES;
    [UIView animateWithDuration:0.3f animations:^{

        _selectSubsBottomContraint.constant = -(_buttonOffset + kSeparatorHeight);
        [self.view layoutIfNeeded];

    }
        completion:^(BOOL finished) {

            [_arrowButtonImage setImage:[UIImage imageNamed:@"subsButton"]];
            _currentSubsShowed = NO;
            _currentSubsAnimating = NO;
        }];
}

- (void)showAlertWithKey:(NSString *)key andText:(NSString *)text
{
    _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(key, text)];
    [_abaAlert showWithOffset:44];
}

#pragma mark - video player

- (void)playVideoWithUrl:(NSURL *)videoUrl andSubtitle:(NSURL *)subtitleUrl
{
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:videoUrl];
    AVPlayer *player = [[AVPlayer alloc] initWithPlayerItem:playerItem];

    self.currentPlayerViewController = [[AVPlayerViewController alloc] init];
    [self.currentPlayerViewController setPlayer:player];

    [self presentViewController:self.currentPlayerViewController animated:YES completion:nil];

    if (subtitleUrl)
    {
        [[self.currentPlayerViewController addSubtitles] openWithFile:subtitleUrl];
        [[self.currentPlayerViewController subtitleLabel] setTextColor:[UIColor whiteColor]];
    }
    
    [[self.currentPlayerViewController player] play];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
}

- (NSURL *)saveDataAsSubtitle:(NSData *)data
{
    NSString *content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSURL *cacheURL = [NSURL fileURLWithPath:cachePath];
    NSURL *srtURL = [cacheURL URLByAppendingPathComponent:@"subtitle.srt"];
    [[content dataUsingEncoding:NSUTF8StringEncoding] writeToURL:srtURL atomically:YES];

    return srtURL;
}

- (void)itemDidFinishPlaying:(NSNotification *)notification
{
    NSLog(@"itemDidFinishPlaying");

    BOOL sectionIsCompleted = NO;
    CooladataSectionType section;

    if (_currentSection == kABAFilmSectionType)
    {
        ABAFilm *film = (ABAFilm *)self.section;
        sectionIsCompleted = [film.completed boolValue];

        // cooladata tracking
        section = CooladataSectionTypeAbaFilm;
    }
    else if (_currentSection == kABAVideoClassSectionType)
    {
        ABAVideoClass *videoClass = (ABAVideoClass *)self.section;
        sectionIsCompleted = [videoClass.completed boolValue];

        // cooladata tracking
        section = CooladataSectionTypeAbaVideoClass;
    }

    if (sectionIsCompleted)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        __weak typeof(self) weakSelf = self;
        [_filmController setCompletedSection:self.section completionBlock:^(NSError *error, id object) {
            if (!error)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                    [weakSelf.delegate showAlertCompletedUnit:weakSelf.currentSection];
                });
            }
            else
            {
                [weakSelf showAlertWithKey:@"errorConnection" andText:@"Imposible guardar progreso :("];
            }
        }];
    }

    ABAUnit *unit = [self.section getUnitFromSection];
    [CoolaDataTrackingManager trackWatchedFilm:[UserController currentUser].idUser levelId:unit.level.idLevel unitId:unit.idUnitString sectionType:section];

    [[self.currentPlayerViewController player] pause];
    [self.currentPlayerViewController dismissViewControllerAnimated:YES completion:nil];

    // Removing observers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
