//
//  CertUnitCell.h
//  ABA
//
//  Created by Marc Güell Segarra on 5/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABARealmCert;

@interface CertUnitCell : UICollectionViewCell

/**
 *  IBOutlets
 */
@property (weak, nonatomic) IBOutlet UILabel *certificateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *certificateLevelTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *lowerLineView;
@property (weak, nonatomic) IBOutlet UIImageView *certificateIcon;

/**
 *  Setup the cell with the logic data in ABARealmCertificate object
 *
 *  @param ABARealmCert The ABARealmCert object
 */
- (void)setupWithABARealmCertificate:(ABARealmCert *)ABARealmCert;

@end
