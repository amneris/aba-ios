//
//  Definitions.h
//  ABA
//
//  Created by Jaume Cornadó on 19/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#ifndef ABA_Definitions_h
#define ABA_Definitions_h


typedef NS_OPTIONS(NSUInteger, kABASectionTypes) {
    kABAFilmSectionType = 1,
    kABASpeakSectionType = 2,
    kABAWriteSectionType = 3,
    kABAInterpretSectionType = 4,
    kABAVideoClassSectionType = 5,
    kABAExercisesSectionType = 6,
    kABAVocabularySectionType = 7,
    kABAAssessmentSectionType = 8,
};

typedef NS_OPTIONS(NSUInteger, kABASectionStyleOptions)
{
    kABASectionStyleDefault             = 0,
    kABASectionStyleBgColorDone         = 1 << 0,
    kABASectionStyleBgNumberColorCurrent = 1 << 1,
    kABASectionStyleLockEnabled         = 1 << 2,
    kABASectionStyleAlphaEnabled        = 1 << 3,
    kABASectionStyleAnimatedYes         = 1 << 4,
};

typedef NS_OPTIONS(NSUInteger, kABASectionActionStatus)
{
    kABASectionActionStatusGranted                         = 1,
    kABASectionActionStatusLockedReasonAssessment          = 2,
    kABASectionActionStatusLockedReasonUserNotPremium      = 3,
    kABASectionActionStatusLockedReasonPreviusNotCompleted = 4,
    kABASectionActionStatusOfflineWithoutData              = 5,
};

typedef NS_OPTIONS(NSUInteger, kABAWriteStatus) {
    kABAWriteInitial = 1,
    kABAWriteOK = 2,
    kABAWriteKO = 3,
    kABAWriteCompleted = 4,
};

typedef NS_OPTIONS(NSUInteger, kABADashboardRowType) {
    kLevelRowType,
    kCertRowType,
    kUnitRowType
};

typedef NS_OPTIONS(NSUInteger, kABAInterpretConversationType) {
    kABAInterpretConversationTypeNormal,
    kABAInterpretConversationTypeListenAll,
};

typedef NS_OPTIONS(NSUInteger, kABAInterpretRowType) {
    kABAInterpretListenRowType,
    kABAInterpretRecordRowType,
};

typedef NS_OPTIONS(NSUInteger, kABAInterpretRowStatus) {
    kABAInterpretRowStatusCurrent,
    kABAInterpretRowStatusNormal,
};

typedef NS_OPTIONS(NSUInteger, kABASpeakStatus) {
    kABASpeakListen = 1,
    kABASpeakRec = 2,
    kABASpeakCompare = 3,
    kABASpeakCompareRecording = 4,
    kABASpeakContinue = 5,
    kABASpeakBeginRecordSound = 6,
    kABASpeakEndRecordSound = 7,
};

typedef NS_OPTIONS(NSUInteger, kABASpeakType) {
    kABASpeakTypeDialog = 1,
    kABASpeakTypeCompleted = 2,
    kABASpeakTypeSample = 3,
};

typedef NS_OPTIONS(NSUInteger, kABAVocabularyStatus) {
    kABAVocabularyListen = 1,
    kABAVocabularyRec = 2,
    kABAVocabularyCompare = 3,
    kABAVocabularyCompareRecording = 4,
    kABAVocabularyContinue = 5,
    kABAVocabularyBeginRecordSound = 6,
    kABAVocabularyEndRecordSound = 7,
};

typedef NS_OPTIONS(NSUInteger, kABAActionButtonType) {
    kABAActionButtonTypeListen = 1,
    kABAActionButtonTypeRecord = 2,
    kABAActionButtonTypeCompare = 3,
    kABAActionButtonTypeContinue = 4
};

typedef NS_OPTIONS(NSUInteger, kABAActionButtonBehaviour) {
    kABAActionButtonBehaviourListenRecord = 1,
    kABAActionButtonBehaviourListenRecordCompare = 2,
    kABAActionButtonBehaviourListen = 3,
    kABAActionButtonBehaviourListenRecordCompareContinue = 4
};

typedef NS_OPTIONS(NSUInteger, kABAActionButtonStatus) {
    kABAActionButtonStatusReadyToListen = 1,
    kABAActionButtonStatusReadyToRecord = 2,
    kABAActionButtonStatusListening = 3,
    kABAActionButtonStatusRecording = 4
};

typedef NS_OPTIONS(NSUInteger, kABAExercisePhraseItemType) {
    kABAExercisePhraseItemTypeNormal = 1,
    kABAExercisePhraseItemTypeBlank = 2
};

typedef NS_OPTIONS(NSUInteger, kABAProgressActions) {
    kABAProgressActionRecorded = 1,
    kABAProgressActionDictation = 2,
    kABAProgressActionWritten = 3,
    kAbAProgressActionBlank = 4
};

typedef NS_OPTIONS(NSUInteger, kABAExercisesType) {
    kABAExercisesTypeExample = 1,
    kABAExercisesTypeExercise = 2
};

typedef NS_OPTIONS(NSUInteger, kABAExercisesStatus) {
    kABAExercisesStatusInit,
    kABAExercisesStatusMustShowExercise,
    kABAExercisesStatusExerciseOK,
    kABAExercisesStatusExerciseKO,
    kABAExercisesStatusMustSave,
    kABAExercisesStatusMustShowNextExercise,
    kABAExercisesStatusMustJumpToNextGroup,
    kABAExercisesStatusMustShowNextGroup,
    kABAExercisesStatusDone
};

typedef NS_OPTIONS(NSUInteger, kAudioControllerType) {
    kAudioControllerTypeSoundEffectBeginRecord,
    kAudioControllerTypeSoundEffectEndRecord,
    kAudioControllerTypeAudio,
};

typedef NS_OPTIONS(NSUInteger, kABASpeakDialogCellType) {
    kABASpeakDialogAvatarCell,
    kABASpeakDialogNormalCell,
    kABASpeakDialogTranslationCell,
    kABASpeakDialogSample,
    kABASpeakDialogSeparation
};

typedef NS_OPTIONS(NSUInteger, kABAProfileHeaderCellType) {
    kABAProfileHeaderLevelCell,
    kABAProfileHeaderUnitCell
};

typedef NS_OPTIONS(NSUInteger, kABAVideoSubtitleType) {
    kABASubtitleTypeEnglish = 1,
    kABASubtitleTypeTranslate = 2
};

typedef NS_OPTIONS(NSUInteger, kABAPopupType) {
    kABAPopupTypeCrash,
    kABAPopupTypeRate
};

// Subscription Statuses
typedef NS_ENUM(NSInteger, SubscriptionStatus) {
    SubscriptionStatusActivationProcessSucceed,           // Whole process is completed
    SubscriptionStatusActivationProcessFailed,            // Error communicating to ABA Server
    SubscriptionStatusTransactionVerificationFailed,      // Error verifying transaction
    SubscriptionStatusPurchaseProcessFailed,              // Error during the Apple purchase
    SubscriptionStatusRestoreProcessFailed,               // Error restoring purchase
    SubscriptionStatusRestoreNoTransactions,              // No restorable transactions found
    SubscriptionStatusPurchaseAlreadyAssigned,            // The purchase is already assigned to another ABARealmUser.
    SubscriptionStatusProductNotFound                     // Product not found
};

// zendesk keys
#define kShowZendeskPopUp @"ShowZendeskPopUp"
#define kDontShowRateApp @"DontShowRateApp"

// deeplink keys

#define kDeepLinkUnit @"DeepLinkUnit"
#define kDeepLinkSection @"DeepLinkSection"
#define kDeepLinkPlans @"DeepLinkPlans"
#define kDeepLinkToken @"DeepLinkToken"

// version control keys

#define kLastSupportedVersion @"LastSupportedVersion"
#define kUnknownVersion @"UnknownVersion"

// experiments keys (A/B testing)
#define kExperimentsKey @"ExperimentsKey"

#define kExperimentIdentifier @"ExperimentIdentifier"
#define kExperimentVariationIdentifier @"ExperimentVariationIdentifier"
#define kUserExperimentVariationId @"UserExperimentVariationId"

#endif
