//
//  ABAProfile.h
//  ABA
//
//  Created by Jordi Melé on 25/3/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABAProfile : NSObject

@property (nonatomic, retain) NSString * idHelp;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * desc;

@end
