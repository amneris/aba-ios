//
//  UserController.m
//  ABA
//
//  Created by Oriol Vilaró on 7/11/14.
//  Copyright (c)2014 mOddity. All rights reserved.
//

#import "UserController.h"
#import "UserDataController.h"
#import "DataController.h"
#import "ABARealmUser.h"
#import "ABARealmUser+Methods.h"
#import "ABARealmLevel.h"
#import "ABAUnit.h"
#import "LevelUnitController.h"
#import "APIManager.h"
#import "ProgressController.h"
#import "DataManager.h"
#import "ABAError.h"
#import "DownloadController.h"
@import SVProgressHUD;

// Tracking
#import "LegacyTrackingManager.h"
#import "MixpanelTrackingManager.h"
#import "FabricTrackingManager.h"

// Facebook
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

// Shepherd
#import "ABAShepherdForceUserTypePlugin.h"

// Swift
#import "ABA-Swift.h"


@implementation UserController

+ (BOOL)isUserLogged {
    if ([UserDataController getCurrentUser] != nil){
        return YES;
    }
    
    return NO;
}

+ (BOOL)isUserPremium {
    return [UserController isPremium:[self currentUser]];
}

+ (BOOL)isPremium:(ABARealmUser *)user {
    
#ifdef SHEPHERD_DEBUG
    if ([ABAShepherdForceUserTypePlugin shouldForceFree])
    {
        NSLog(@"Shepherd plugin - Forcing User Type Free");
        
        return NO;
    }
    else if ([ABAShepherdForceUserTypePlugin shouldForcePremium])
    {
        NSLog(@"Shepherd plugin - Forcing User Type Premium");
        
        return YES;
    }
#endif
    
    if (user && [user.type isEqualToString:@"2"])
    {
        return YES;
    }
    else if (user && [user.type isEqualToString:@"4"]) // chequeamos si es usertype4(teacher)y lo tratamos como premium
    {
        return YES;
    }
    return NO;
}

+ (ABARealmUser *)currentUser
{
    return [UserDataController getCurrentUser];
}

+ (BOOL)isUserImage
{
    if ([UserDataController getCurrentUser].urlImage != nil)
    {
        return YES;
    }
    
    return NO;
}

+ (NSString *)getUserLanguage
{
    return [self currentUser].userLang;
}

+ (void)logout
{
    CLS_LOG(@"UserController: logout");
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kDontShowRateApp];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [UserDataController deleteCurrentUser];
    [LanguageController resetCurrentLanguage];
	
    [[APIManager sharedManager] setToken:nil];
	[[[APIManager sharedManager] manager].operationQueue cancelAllOperations];
	[[[APIManager sharedManager] progressManager].operationQueue cancelAllOperations];
    [[[APIManager sharedManager] fileManager].operationQueue cancelAllOperations];
    [DataManager sharedManager].progressCache = nil;
    [[MixpanelTrackingManager sharedManager] reset];
    [DownloadController removeDownloadedFiles];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserLoggedOutNotification" object:nil];
}

+ (void)logoutWithBlock:(ErrorBlock)blockOrNil
{
    CLS_LOG(@"UserController: logout with block");
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kDontShowRateApp];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [UserDataController deleteCurrentUserWithBlock:^(NSError *error)
    {
        if(!error)
        {
            [LanguageController resetCurrentLanguage];
            [[APIManager sharedManager] setToken:nil];
			[[[APIManager sharedManager] manager].operationQueue cancelAllOperations];
			[[[APIManager sharedManager] progressManager].operationQueue cancelAllOperations];
            [[[APIManager sharedManager] fileManager].operationQueue cancelAllOperations];
            [DataManager sharedManager].progressCache = nil;
            [[MixpanelTrackingManager sharedManager] reset];
            [DownloadController removeDownloadedFiles];

			[[NSNotificationCenter defaultCenter] postNotificationName:@"UserLoggedOutNotification" object:nil];

            if (blockOrNil) {
                blockOrNil(nil);
            }
        }
        else if (blockOrNil) {
            blockOrNil(error);
        }
    }];
}

+ (ABARealmLevel *)returnCurrentLevel
{    
    return [self currentUser].currentLevel;
}

+ (ABAUnit *)returnCurrentUnit
{
    return [self getCurrentUnitForUser:[self currentUser]];
}

+ (void)setCurrentLevel:(ABARealmLevel *)level completionBlock:(CompletionBlock)completionBlock
{
    [UserDataController setUserCurrentLevel:level completionBlock:^(NSError *error, id object) {
        
        id objectToReturn = nil;
        
        if(!error)
        {
            objectToReturn = object;
        }
        
        if (completionBlock != nil)
        {
            completionBlock(error, objectToReturn);
        }
    }];
}

+ (ABAUnit *)getCurrentUnitForUser:(ABARealmUser *)user
{
    if(user == nil)
    {
        return nil;
    }
    
    // No debería pasar nunca
    // SI pasa cuando cambiamos de nivel a uno que está completado
    if ([[LevelUnitController getCompletedUnitsForLevel:user.currentLevel] count] == [user.currentLevel.units count])
    {
        // [unitController goToNextLevel:user];
        // return [self getCurrentUnitForUser:user];
    }
    
    ABAUnit *lastCompleted = [LevelUnitController getLastCompletedUnitForLevel:user.currentLevel];
    
    if (lastCompleted == nil)
    {
        return [[LevelUnitController getUnitsDescendingForLevel:user.currentLevel] lastObject];
    }
    else
    {
        return [LevelUnitController getNextUnit:lastCompleted];
    }
}

+ (void)registerUserWithFacebookID:(NSString *)facebookID email:(NSString *)email name:(NSString *)name surnames:(NSString *)surnames gender:(NSString *)gender avatar:(NSString*)avatar completionBlock:(CompletionBlockFacebook)completionBlockFacebook {
    
    [UserDataController postRegisterWithFacebookID:facebookID email:email name:name surnames:surnames gender:gender avatar:(NSString*)avatar completionBlock:^(NSError *error, id object, BOOL isNewUser){
        
         if (!error)
         {
             ABARealmUser * user = [UserController currentUser];
             if (isNewUser)
             {
                 [[LegacyTrackingManager sharedManager] trackRegister];
                 [[MixpanelTrackingManager sharedManager] trackRegister:kRegisterTypeFacebook];
                 [[FabricTrackingManager sharedManager] trackRegister:kRegisterTypeFacebook];
                 [CoolaDataTrackingManager trackUserRegistered:user.idUser];
             }
             else
             {
                 [[LegacyTrackingManager sharedManager] trackLogin];
                 [[MixpanelTrackingManager sharedManager] trackLoginSuperproperties];
                 [[MixpanelTrackingManager sharedManager] trackLoginEvent:kRegisterTypeFacebook];
                 [[FabricTrackingManager sharedManager] trackFacebookLoginEvent];
                 [CoolaDataTrackingManager trackUserRegistered:user.idUser];
             }
             [CrashlyticsKit setUserEmail: user.email];
             [CrashlyticsKit setUserName: user.name];

             [DataController saveProgressActionForSection:nil andUnit:nil andPhrases:nil errorText:nil isListen: NO isHelp: NO completionBlock:^(NSError *error, id object){
                 if (!error){
                     completionBlockFacebook(nil, nil, isNewUser);
                 }
				 else {
					 completionBlockFacebook(error, nil, isNewUser);
				 }
             }];
             
         }
         else
         {
             completionBlockFacebook(error, nil, isNewUser);
         }
     }];
    
}

+ (void)registerUserWithEmail:(NSString *)email
                 withPassword:(NSString *)password
                     withName:(NSString *)name
              completionBlock:(CompletionBlock)completionBlock
{

    [UserDataController postRegisterWithEmail:email
                                 password:password
                                     name:name
                          completionBlock:^(NSError *error, id object) {
                              if (!error)
                              {
                                  ABARealmUser * user = [UserController currentUser];
                                  
                                  [[LegacyTrackingManager sharedManager] trackRegister];
                                  [[MixpanelTrackingManager sharedManager] trackRegister:kRegisterTypeMail];
                                  [[FabricTrackingManager sharedManager] trackRegister:kRegisterTypeMail];
                                  [CoolaDataTrackingManager trackUserRegistered:user.idUser];
                                  [CrashlyticsKit setUserEmail: user.email];
                                  [CrashlyticsKit setUserName: user.name];

                                  [DataController saveProgressActionForSection:nil
                                                                       andUnit:nil
                                                                    andPhrases:nil
                                                                     errorText:nil
                                                                      isListen:NO
                                                                        isHelp:NO
                                                               completionBlock:^(NSError *error, id object) {
                                                                   if (!error)
                                                                   {
                                                                       completionBlock(nil, nil);
                                                                   }
                                                                   else
                                                                   {
                                                                       completionBlock(error, nil);
                                                                   }
                                                               }];
                              }
                              else
                              {
                                  completionBlock(error, nil);
                              }
                          }];
}

+ (void)checkUserPasswordWithEmail:(NSString *)email withPassword:(NSString *)password completionBlock:(BoolCompletionBlock)completionBlock {
    
    [UserDataController checkPasswordWithEmail:email password:password completionBlock:^(NSError *error, BOOL correct) {
         if(!error)
         {
             completionBlock(nil, YES);
         }
         else
         {
             completionBlock(error, NO);
         }
     }];
}


+ (void)loginUserWithEmail:(NSString *)email withPassword:(NSString *)password completionBlock:(CompletionBlock)completionBlock
{
    [UserDataController postLoginWithEmail:email password:password completionBlock:^(NSError *error, id object) {
        
         if(!error)
         {
             // User data
             ABARealmUser * currentUser = [UserDataController getCurrentUser];
             
             // Overwritting language
             [LanguageController setCurrentLanguage:currentUser.userLang];
             
             // Analytics
             [[LegacyTrackingManager sharedManager] trackLogin];
             [[MixpanelTrackingManager sharedManager] trackLoginSuperproperties];
             [[MixpanelTrackingManager sharedManager] trackLoginEvent:kRegisterTypeMail];
             [[FabricTrackingManager sharedManager] trackLoginEvent];
             [CoolaDataTrackingManager trackLoginEventWithLoginOfflineFlag:NO userId:currentUser.idUser];
             
             
             [CrashlyticsKit setUserEmail: currentUser.email];
             [CrashlyticsKit setUserName: currentUser.name];
             
             // Downloading information
             [DataController saveProgressActionForSection:nil andUnit:nil andPhrases:nil errorText:nil isListen: NO isHelp: NO completionBlock:^(NSError *error, id object){
                 if (!error)
                 {
                     completionBlock(nil, currentUser);
                 }
				 else
                 {
					 completionBlock(error, nil);
				 }
             }];
         }
         else
         {
             completionBlock(error, nil);
         }
     }];
}

+ (void)updatePasswordUserWithToken:(NSString *)token withPassword:(NSString *)password completionBlock:(CompletionBlock)completionBlock {
    
    [UserDataController updatePasswordUserWithToken:token withPassword:password completionBlock:^(NSError *error, id object) {
         if(!error)
         {
             completionBlock(nil, nil);
         }
         else
         {
             completionBlock(error, nil);
         }
     }];
}

+ (void)recoverPassword:(NSString *)email completionBlock:(CompletionBlock)completionBlock
{
    [UserDataController postRecoverPassword:email completionBlock:completionBlock];
}

+ (void)updateUserToPremium
{
    [UserDataController updateUserToPremium];
}
// TODO: IMPLEMENT PROPER METHODS

+ (void)saveCurrentUser:(ABARealmUser *)user
{
    [UserDataController updateUser:user];
}

+ (void)saveCurrentUser:(ABARealmUser *)user completionBlock:(CompletionBlock)completionBlock
{
    [UserDataController updateUser:user completionBlock:completionBlock];
}


#pragma mark - Private methods

+ (void)handleLoginWithError:(NSError *)error andObject:(id)object andcCompletionBlock:(CompletionBlock)completionBlock
{
    // Error 4xx comes from the server and they mean login error
    if ((error && error.code >= CODE_HANDLED_SERVER_SEGMENT_ERROR_400 && error.code < CODE_UNHANDLED_SERVER_SEGMENT_ERROR_500))
    {
        // block
        completionBlock(error, nil);
    }
    else
    {
        // Reading user data first
        ABARealmUser * currentUser = [UserDataController getCurrentUser];
        
        NSLog(@"Last login performed on: %@", currentUser.lastLoginDate);
        
        // If there was an error and the last login date was not recent
        if (error && ![currentUser isRecentLogin])
        {
            // block
            completionBlock(error, nil);
        }
        else
        {
            // Overwritting language
            [LanguageController setCurrentLanguage:currentUser.userLang];
            
            // Continuing with user data
            [DataController saveProgressActionForSection:nil andUnit:nil andPhrases:nil errorText:nil isListen: NO isHelp: NO completionBlock:^(NSError *error, id object)
             {
                 if (!error)
                 {
                     completionBlock(nil, nil);
                 }
                 else
                 {
                     // Error saving
                     completionBlock(error, nil);
                 }
             }];
            
            // Tracking section:
            // Once here the login has been done.
            if (error)
            {   // If there was an error the login failed due to a connection issue
                
                // Traking login
                [[LegacyTrackingManager sharedManager] trackLogin];
                [[MixpanelTrackingManager sharedManager] trackLoginSuperproperties];
                
                // Traking special event for offline login
                [[MixpanelTrackingManager sharedManager] trackLoginOfflineEvent];
                [[FabricTrackingManager sharedManager] trackLoginOfflineEvent];
                [CoolaDataTrackingManager trackLoginEventWithLoginOfflineFlag:YES userId:currentUser.idUser];
            }
            else
            {   // Traking proper login.
                
                // Tracking login in LegacyTrackingManager. We are not interested in tracking this 'Login with token' in
                // Mixpanel and other mobile tracking oriented platforms.
                [[LegacyTrackingManager sharedManager] trackLogin];
                [[MixpanelTrackingManager sharedManager] trackLoginSuperproperties];
                [CoolaDataTrackingManager trackLoginEventWithLoginOfflineFlag:NO userId:currentUser.idUser];
            }
        }
    }
}

+ (BOOL)isTheFirstCompletedUnit
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kUserFirstUnitCompletedFacebookPost])
    {
        return false;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserFirstUnitCompletedFacebookPost];
        return true;
    }
}

#pragma mark - Facebook login register

+ (void)loginWithFacebookCompletionBlock:(CompletionBlockFacebook)completionBlock
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut]; // clear old tokens
    [login logInWithReadPermissions:@[ @"public_profile", @"user_birthday", @"email" ]
                 fromViewController:nil
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {

                                if (error) // facebook error
                                {
                                    completionBlock(error, nil, NO);
                                }
                                else if (result.isCancelled) // user cancelled
                                {
                                    completionBlock([ABAError errorWithKey:@"cancelled"], nil, NO);
                                }
                                else
                                {
                                    if ([FBSDKAccessToken currentAccessToken])
                                    {
                                        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                                        
                                        [[[FBSDKGraphRequest alloc]
                                            initWithGraphPath:@"me"
                                                   parameters:@{ @"fields" : @"id, email, first_name, last_name, gender" }]
                                            startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {

                                                if (error) // error request information
                                                {
                                                    [SVProgressHUD dismiss];

                                                    completionBlock(error, nil, NO);
                                                }
                                                else
                                                {
                                                    if (![[FBSDKAccessToken currentAccessToken] hasGranted:@"email"] || ![result objectForKey:@"email"]) // no mail
                                                    {
                                                        [SVProgressHUD dismiss];

                                                        completionBlock([ABAError errorWithKey:STRTRAD(@"regErrorFacebookEmailKey", @"Compruebe la privacidad de su cuenta de facebook")], nil, NO);
                                                    }
                                                    else
                                                    {
                                                        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                            initWithGraphPath:[NSString stringWithFormat:@"me/picture?type=large&redirect=false"]
                                                                   parameters:nil
                                                                   HTTPMethod:@"GET"];
                                                        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id resultImage, NSError *error) {
                                                            
                                                            NSString *urlImage;
                                                            if (error) // no profile image
                                                            {
                                                                urlImage = nil;
                                                            }
                                                            else
                                                            {
                                                                urlImage = [resultImage valueForKeyPath:@"data.url"];
                                                            }

                                                            [UserController registerUserWithFacebookID:[result objectForKey:@"id"]
                                                                                                 email:[result objectForKey:@"email"]
                                                                                                  name:[result objectForKey:@"first_name"]
                                                                                              surnames:[result objectForKey:@"last_name"]
                                                                                                gender:[result objectForKey:@"gender"]
                                                                                                avatar:urlImage
                                                                                       completionBlock:^(NSError *error, id object, BOOL isNewUser) {

                                                                                           [SVProgressHUD dismiss];

                                                                                           if (!error)
                                                                                           {
                                                                                               ABARealmUser *user = [UserController currentUser];

                                                                                               completionBlock(nil, user, isNewUser);
                                                                                               [CrashlyticsKit setUserEmail: user.email];
                                                                                               [CrashlyticsKit setUserName: user.name];
                                                                                           }
                                                                                           else
                                                                                           {
                                                                                               if (error.code == CODE_CONNECTION_ERROR) // connection error
                                                                                               {
                                                                                                   //Error de conexión
                                                                                                   completionBlock(error, nil, NO);
                                                                                               }
                                                                                               else if (error.code == CODE_LANGUAGE_ERROR) // language error
                                                                                               {
                                                                                                   //Error de idioma NO es
                                                                                                   completionBlock([ABAError errorWithKey:STRTRAD(@"loginLanguageError", @"Oops! This version is only available in Spanish!Your language is coming soon...")], nil, NO);
                                                                                               }
                                                                                               else // facebook login error
                                                                                               {
                                                                                                   //Login incorrecto
                                                                                                   completionBlock([ABAError errorWithKey:STRTRAD(@"errorRegisterFacebook", @"error en el registro con facebook")], nil, NO);
                                                                                               }
                                                                                           }

                                                                                       }];
                                                        }];
                                                    }
                                                }
                                            }];
                                    }
                                }
                            }];
}

@end
