//
//  ABAControlView.m
//  ABA
//
//  Created by Marc Güell Segarra on 4/5/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ABAControlView.h"
#import "PureLayout.h"
#import "Utils.h"

@interface ABAControlView()

@property (nonatomic, assign) BOOL didSetupConstraints;
@property kABASectionTypes sectionType;

@property (nonatomic, strong) UIView *redoView;
@property (nonatomic, strong) UILabel *redoLabel;
@property (nonatomic, strong) UIImageView *redoButton;
@property (nonatomic, strong) UIView *redoButtonView;

@property (nonatomic, strong) UIView *compareView;
@property (nonatomic, strong) UILabel *compareLabel;
@property (nonatomic, strong) UIImageView *compareButton;
@property (nonatomic, strong) UIView *compareButtonView;

@property (nonatomic, strong) UIView *timeRecordView;
@property (nonatomic, strong) UIView *timeRecordRedDot;
@property (nonatomic, strong) UILabel *timeRecordedLabel;
@property (nonatomic, strong) NSTimer *timeRecordedTimer;

@end

@implementation ABAControlView

-(id) initWithSectionType: (kABASectionTypes)type {
	
	self = [super init];
	
	if(self) {
		_sectionType = type;
		
		self.backgroundColor = ABA_COLOR_REG_TITLE_TEXT;
		
		[self addSubview:self.actionButton];
		
		if(_sectionType == kABASpeakSectionType || _sectionType == kABAVocabularySectionType) {
			
			self.backgroundColor = ABA_COLOR_REG_ALPHA;
			
			[self.redoView addSubview:self.redoLabel];
			[self.redoView addSubview:self.redoButtonView];
			[self.redoButtonView addSubview:self.redoButton];
			[self addSubview:self.redoView];
			
			[self.compareView addSubview:self.compareLabel];
			[self.compareView addSubview:self.compareButtonView];
			[self.compareButtonView addSubview:self.compareButton];
			[self addSubview:self.compareView];
		}
		
		if(_sectionType == kABAInterpretSectionType ||
		   _sectionType == kABASpeakSectionType ||
		   _sectionType == kABAVocabularySectionType) {
			[self.timeRecordView addSubview:self.timeRecordRedDot];
			[self.timeRecordView addSubview:self.timeRecordedLabel];
			[self addSubview:self.timeRecordView];
		}
		
		self.didSetupConstraints = NO;
	}
	
	return self;
}

-(void)layoutIfNeeded {
	
	[super layoutIfNeeded];
	
	if(!self.didSetupConstraints) {
		// At this point we'll have the superview reference for applying autolayout constraints
		// Let's place the elements:
		[_actionButton autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self withMultiplier:(IS_IPAD?0.45f:0.70f)];
		[_actionButton autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self withMultiplier:0.92f];
		
		[_actionButton autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
		[_actionButton autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
		[_actionButton setNeedsLayout];
		[_actionButton setNeedsUpdateConstraints];
		[_actionButton layoutIfNeeded];

		if(_sectionType == kABASpeakSectionType || _sectionType == kABAVocabularySectionType) {
			// We setup the auxiliary buttons
			
			CGFloat auxButtonHPadding = _actionButton.frame.size.height*(IS_IPAD?0.6:0.1);
			
			CGFloat heightForAuxButton = _actionButton.frame.size.height*0.80;
			[_redoView autoSetDimension:ALDimensionWidth toSize:heightForAuxButton];
			[_redoView autoSetDimension:ALDimensionHeight toSize:heightForAuxButton];
			[_redoView autoAlignAxis:ALAxisHorizontal toSameAxisOfView:_actionButton];
			[_redoView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:auxButtonHPadding];
			[_redoView setNeedsLayout];
			[_redoView setNeedsUpdateConstraints];
			[_redoView layoutIfNeeded];

			[_redoLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_redoView];
			[_redoLabel autoSetDimension:ALDimensionHeight toSize:(IS_IPAD?18.0f:12.0f)*1.15];
			[_redoLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:_redoView];
			[_redoLabel autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:_redoView];
			[_redoLabel setNeedsLayout];
			[_redoLabel setNeedsUpdateConstraints];
			[_redoLabel layoutIfNeeded];
			
			CGFloat heightAvailable = (_redoView.frame.size.height - _redoLabel.frame.size.height);
			CGFloat heightAvailableForImage = heightAvailable * 0.70f;
			CGFloat margin = heightAvailable * 0.15f;

			[_redoButtonView autoSetDimension:ALDimensionHeight toSize:heightAvailableForImage];
			[_redoButtonView autoSetDimension:ALDimensionWidth toSize:heightAvailableForImage];
			[_redoButtonView autoAlignAxis:ALAxisVertical toSameAxisOfView:_redoView];
			[_redoButtonView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_redoLabel withOffset:-margin];
			[_redoButtonView setNeedsLayout];
			[_redoButtonView setNeedsUpdateConstraints];
			[_redoButtonView layoutIfNeeded];
			
			_redoButtonView.layer.cornerRadius = _redoButtonView.frame.size.height/2;
			_redoButtonView.clipsToBounds = YES;

			[_redoButton autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_redoButtonView withMultiplier:0.50];
			[_redoButton autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_redoButtonView withMultiplier:0.50];
			[_redoButton autoCenterInSuperview];
			[_redoButton setNeedsLayout];
			[_redoButton setNeedsUpdateConstraints];
			[_redoButton layoutIfNeeded];
			
			[_compareView autoSetDimension:ALDimensionWidth toSize:heightForAuxButton];
			[_compareView autoSetDimension:ALDimensionHeight toSize:heightForAuxButton];
			[_compareView autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self];
			[_compareView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:-auxButtonHPadding];
			[_compareView setNeedsLayout];
			[_compareView setNeedsUpdateConstraints];
			[_compareView layoutIfNeeded];
			
			[_compareLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_compareView];
			[_compareLabel autoSetDimension:ALDimensionHeight toSize:(IS_IPAD?18.0f:12.0f)*1.15];
			[_compareLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:_compareView];
			[_compareLabel autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:_compareView];
			[_compareLabel setNeedsLayout];
			[_compareLabel setNeedsUpdateConstraints];
			[_compareLabel layoutIfNeeded];

			[_compareButtonView autoSetDimension:ALDimensionHeight toSize:heightAvailableForImage];
			[_compareButtonView autoSetDimension:ALDimensionWidth toSize:heightAvailableForImage];
			[_compareButtonView autoAlignAxis:ALAxisVertical toSameAxisOfView:_compareView];
			[_compareButtonView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_compareLabel withOffset:-margin];
			[_compareButtonView setNeedsLayout];
			[_compareButtonView setNeedsUpdateConstraints];
			[_compareButtonView layoutIfNeeded];

			_compareButtonView.layer.cornerRadius = _compareButtonView.frame.size.height/2;
			_compareButtonView.clipsToBounds = YES;
			
			[_compareButton autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_compareButtonView withMultiplier:0.50];
			[_compareButton autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_compareButtonView withMultiplier:0.50];
			[_compareButton autoCenterInSuperview];
			[_compareButton setNeedsLayout];
			[_compareButton setNeedsUpdateConstraints];
			[_compareButton layoutIfNeeded];
		}

		if(_sectionType == kABAInterpretSectionType ||
		   _sectionType == kABASpeakSectionType ||
		   _sectionType == kABAVocabularySectionType) {
			// We setup the record dot view
			
			CGFloat redDotWidth = IS_IPAD?20.0f:10.0f;

			[_timeRecordView autoSetDimension:ALDimensionWidth toSize:125];
			[_timeRecordView autoSetDimension:ALDimensionHeight toSize:redDotWidth];
			[_timeRecordView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:redDotWidth/2];
			[_timeRecordView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:redDotWidth/2];
			[_timeRecordView setNeedsLayout];
			[_timeRecordView setNeedsUpdateConstraints];
			[_timeRecordView layoutIfNeeded];

			[_timeRecordRedDot autoSetDimension:ALDimensionWidth toSize:redDotWidth];
			[_timeRecordRedDot autoSetDimension:ALDimensionHeight toSize:redDotWidth];
			[_timeRecordRedDot autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:IS_IPAD?0.0f:5.0f];
			[_timeRecordRedDot autoPinEdgeToSuperviewEdge:ALEdgeLeft];
			[_timeRecordRedDot setNeedsLayout];
			[_timeRecordRedDot setNeedsUpdateConstraints];
			[_timeRecordRedDot layoutIfNeeded];
			
			[_timeRecordRedDot.layer setCornerRadius:redDotWidth/2];
			_timeRecordRedDot.clipsToBounds = YES;

			[_timeRecordedLabel autoSetDimension:ALDimensionWidth toSize:100.0f];
			[_timeRecordedLabel autoSetDimension:ALDimensionHeight toSize:20.0f];
			[_timeRecordedLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:redDotWidth*1.35];
			[_timeRecordedLabel autoPinEdgeToSuperviewEdge:ALEdgeTop];
			[_timeRecordedLabel setNeedsLayout];
			[_timeRecordedLabel setNeedsUpdateConstraints];
			[_timeRecordedLabel layoutIfNeeded];
			
		}
		
		self.didSetupConstraints = YES;
	}
}

-(ABAActionButtonView *)actionButton {
	if(!_actionButton) {
		_actionButton = [[ABAActionButtonView alloc] initWithSectionType:_sectionType];
		_actionButton.translatesAutoresizingMaskIntoConstraints = NO;
		_actionButton.buttonBehaviour = [self getButtonBehaviourFromSectionType];
	}
	
	return _actionButton;
}

-(kABAActionButtonBehaviour)getButtonBehaviourFromSectionType {
	
	switch (_sectionType) {
		case kABAWriteSectionType: {
			return kABAActionButtonBehaviourListen;
			break;
		}
		case kABAInterpretSectionType: {
			return kABAActionButtonBehaviourListenRecord;
			break;
		}
		case kABASpeakSectionType:
		case kABAVocabularySectionType: {
			return kABAActionButtonBehaviourListenRecordCompareContinue;
			break;
		}
			
		default:
			return 0;
			break;
	}
}

-(UIView *)redoView {
	if(!_redoView) {
		_redoView = [UIView newAutoLayoutView];
		[_redoView setHidden:YES];
	}
	
	return _redoView;
}

-(UILabel *)redoLabel {
	if(!_redoLabel) {
		_redoLabel = [UILabel newAutoLayoutView];
		[_redoLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:12]];
		_redoLabel.textColor = [UIColor whiteColor];
		_redoLabel.text = [STRTRAD(@"sectionSpeakRedoLabelKey", @"rehacer") uppercaseString];
		_redoLabel.textAlignment = NSTextAlignmentCenter;
	}
	
	return _redoLabel;
}

-(UIImageView *)redoButton {
	if(!_redoButton) {
		_redoButton = [UIImageView newAutoLayoutView];
		[_redoButton setImage:[UIImage imageNamed:@"redo"]];
	}
	
	return _redoButton;
}

-(UIView *)redoButtonView {
	if(!_redoButtonView) {
		_redoButtonView = [UIView newAutoLayoutView];
		_redoButtonView.backgroundColor = ABA_COLOR_REG_BACKGROUND;
		
		UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(redoButtonTapped:)];
		[tapRecognizer setNumberOfTapsRequired:1];
		[_redoButtonView addGestureRecognizer:tapRecognizer];
	}
	
	return _redoButtonView;
}


-(UILabel *)compareLabel {
	if(!_compareLabel) {
		_compareLabel = [UILabel newAutoLayoutView];
		[_compareLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:12]];
		_compareLabel.textColor = [UIColor whiteColor];
		_compareLabel.text = [STRTRAD(@"sectionCompareLabelKey", @"comparar") uppercaseString];
		_compareLabel.textAlignment = NSTextAlignmentCenter;
	}
	
	return _compareLabel;
}

-(UIView *)compareButtonView {
	if(!_compareButtonView) {
		_compareButtonView = [UIView newAutoLayoutView];
		_compareButtonView.backgroundColor = ABA_COLOR_REG_BACKGROUND;
		
		UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(compareButtonTapped:)];
		[tapRecognizer setNumberOfTapsRequired:1];
		[_compareButtonView addGestureRecognizer:tapRecognizer];
	}
	
	return _compareButtonView;
}

-(UIImageView *)compareButton {
	if(!_compareButton) {
		_compareButton = [UIImageView newAutoLayoutView];
		[_compareButton setImage:[UIImage imageNamed:@"compare"]];
	}
	
	return _compareButton;
}

-(UIView *)compareView {
	if(!_compareView) {
		_compareView = [UIView newAutoLayoutView];
		[_compareView setHidden:YES];
	}
	
	return _compareView;
}

-(UIView *)timeRecordView {
	if(!_timeRecordView) {
		_timeRecordView = [UIView newAutoLayoutView];
		_timeRecordView.alpha = 0.0f;
	}
	
	return _timeRecordView;
}

-(UIView *)timeRecordRedDot {
	if(!_timeRecordRedDot) {
		_timeRecordRedDot = [UIView newAutoLayoutView];
		
		_timeRecordRedDot.backgroundColor = [UIColor redColor];
	}
	
	return _timeRecordRedDot;
}

-(UILabel *)timeRecordedLabel {
	if(!_timeRecordedLabel) {
		_timeRecordedLabel = [UILabel newAutoLayoutView];
		_timeRecordedLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?16:12];
		_timeRecordedLabel.textColor = [UIColor whiteColor];
		_timeRecordedLabel.text = @"00:00 s";
	}
	
	return _timeRecordedLabel;
}

-(void)showRecordingTime
{
	_timeRecordedLabel.text = @"00:00 s";

	[UIView animateWithDuration:0.2
						  delay:0.0
						options:UIViewAnimationOptionCurveEaseIn animations:^
	 {
		 _timeRecordView.alpha = 1.0f;
	 }
					 completion:^(BOOL finished) {
						 
						 CABasicAnimation *buttonAnumation = [CABasicAnimation animationWithKeyPath:@"opacity"];
						 buttonAnumation.autoreverses = YES;
						 buttonAnumation.duration = 0.3;
						 buttonAnumation.repeatCount = HUGE_VALF;
						 buttonAnumation.toValue = [NSNumber numberWithFloat:0.0];
						 
						 [_timeRecordRedDot.layer addAnimation:buttonAnumation forKey:@"opacity"];
					 }];
	
	self.timeRecordedTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateRecordingLabel) userInfo:nil repeats:YES];
}

-(void)hideRecordingTime
{
	[_timeRecordedTimer invalidate];
	_timeRecordedTimer = nil;
	
	[_timeRecordRedDot.layer removeAllAnimations];
	
	[UIView animateWithDuration:0.1
						  delay:0.0
						options:UIViewAnimationOptionCurveEaseOut animations:^
	 {
		 _timeRecordView.alpha = 0.0f;
	 }
					 completion:nil];
}

- (void)updateRecordingLabel
{
	float minutes = floor(_audioController.recorder.currentTime/60);
	float seconds = _audioController.recorder.currentTime - (minutes * 60);
	
	NSString *time = [[NSString alloc]
					  initWithFormat:@"%02.0f:%02.0f s",
					  minutes, seconds];
	
	_timeRecordedLabel.text = time;
}

-(void) showAuxiliaryButtons:(BOOL)show {
	_compareView.hidden = !show;
	_redoView.hidden = !show;
}

-(void)redoButtonTapped:(id)sender {
	if([_delegate respondsToSelector:@selector(listenButtonTapped)])
		[self.delegate controlViewRedoButtonTapped];
}

-(void)compareButtonTapped:(id)sender {
	if([_delegate respondsToSelector:@selector(compareButtonTapped)])
		[self.delegate controlViewCompareButtonTapped];
}

@end
