//
//  EvaluationQuestionViewController.m
//  ABA
//
//  Created by Ivan Ferrera on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "EvaluationQuestionViewController.h"
#import "UIViewController+ABA.h"
#import "ABAEvaluation.h"
#import "ABAEvaluationQuestion.h"
#import "ABAEvaluationOption.h"
#import "Utils.h"
#import "Resources.h"
#import "Definitions.h"
#import "ABAUnit.h"
#import "ABAAlert.h"
#import "EvaluationResultViewController.h"
#import "EvaluationController.h"
#import "LegacyTrackingManager.h"
#import "UserController.h"

#import "ABA-Swift.h"
#import <Crashlytics/Crashlytics.h>

@interface EvaluationQuestionViewController ()

@property(weak, nonatomic) IBOutlet UIButton *buttonOptionA;
@property(weak, nonatomic) IBOutlet UIButton *buttonOptionB;
@property(weak, nonatomic) IBOutlet UIButton *buttonOptionC;
@property(weak, nonatomic) IBOutlet UILabel *numPreguntaLabel;
@property(weak, nonatomic) IBOutlet UILabel *questionLabel;
@property(weak, nonatomic) IBOutlet UIImageView *wrongImageView;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *questionViewTopConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonATrailingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonALeadingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBTrailingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBLeadingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonCTrailingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonCLeadingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBCVerticalConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonABVerticalConstraint;

@property EvaluationController *evaluationController;
@property(copy, nonatomic) NSArray *questionsToAnswer;
@property(assign, nonatomic) NSUInteger currentAnswerIndex;

- (IBAction)optionSelected:(id)sender;

@end

@implementation EvaluationQuestionViewController
@dynamic section;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        _evaluationController = [[EvaluationController alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IPAD)
    {
        _questionViewTopConstraint.constant = _questionViewTopConstraint.constant * 2.5f;
        _buttonsViewHeightConstraint.constant = _buttonsViewHeightConstraint.constant * 1.5f;
        _buttonHeightConstraint.constant = _buttonHeightConstraint.constant * 1.3f;
        
        _buttonATrailingConstraint.constant = _buttonATrailingConstraint.constant * 10;
        _buttonALeadingConstraint.constant = _buttonALeadingConstraint.constant * 10;
        
        _buttonBTrailingConstraint.constant = _buttonATrailingConstraint.constant;
        _buttonBLeadingConstraint.constant = _buttonALeadingConstraint.constant;
        _buttonCTrailingConstraint.constant = _buttonATrailingConstraint.constant;
        _buttonCLeadingConstraint.constant = _buttonALeadingConstraint.constant;
        
        _buttonBCVerticalConstraint.constant = _buttonBCVerticalConstraint.constant * 1.5;
        _buttonABVerticalConstraint.constant = _buttonABVerticalConstraint.constant * 1.5;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [[LegacyTrackingManager sharedManager] trackPageView:@"assessmentsection"];
}

- (void)setupView
{
    [self addBackButtonWithTitle:STRTRAD(@"evaluaKey", @"Evaluación") andSubtitle:@"" withTarget:self withAction:@selector(doBack:)];

    _buttonOptionA.layer.cornerRadius = 3.0f;
    _buttonOptionA.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionA.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 30.0f : 18.0f];

    _buttonOptionB.layer.cornerRadius = 3.0f;
    _buttonOptionB.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionB.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 30.0f : 18.0f];

    _buttonOptionC.layer.cornerRadius = 3.0f;
    _buttonOptionC.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionC.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 30.0f : 18.0f];

    _questionLabel.textColor = ABA_COLOR_DARKGREY;
    _questionLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 28.0f : 18.0f];

    // Initially we set them with 0 alpha for the fade animation
    _numPreguntaLabel.alpha = 0.0f;
    _numPreguntaLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD ? 24.0f : 16.0f];

    _questionLabel.alpha = 0.0f;
    _buttonOptionA.titleLabel.alpha = 0.0f;
    _buttonOptionB.titleLabel.alpha = 0.0f;
    _buttonOptionC.titleLabel.alpha = 0.0f;

    _buttonOptionB.titleLabel.adjustsFontSizeToFitWidth = YES;
    _buttonOptionC.titleLabel.adjustsFontSizeToFitWidth = YES;

    _buttonOptionA.titleLabel.numberOfLines = 2;
    _buttonOptionA.titleLabel.textAlignment = NSTextAlignmentCenter;
    _buttonOptionA.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    _buttonOptionB.titleLabel.numberOfLines = 2;
    _buttonOptionB.titleLabel.textAlignment = NSTextAlignmentCenter;
    _buttonOptionB.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    _buttonOptionC.titleLabel.numberOfLines = 2;
    _buttonOptionC.titleLabel.textAlignment = NSTextAlignmentCenter;
    _buttonOptionC.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    // We can have here three cases:
    // 1. New evaluation
    // 2. User wants to repeat evaluation already finished
    // 3. Evaluation with missing or wrong answers
    
    NSMutableArray *pendingQuestionsMutArray = @[].mutableCopy;
    for (ABAEvaluationQuestion *question in self.section.content)
    {
        if (question.answered == nil)
        {
            [pendingQuestionsMutArray addObject:question];
        }
    }

    // 1. All questions not answered
    if ([pendingQuestionsMutArray count] == [self.section.content count])
    {
        _questionsToAnswer = pendingQuestionsMutArray.copy;
        _currentAnswerIndex = 0;
    }

    // 2. All questions answered
    else if ([pendingQuestionsMutArray count] == 0)
    {
        _questionsToAnswer = (id)self.section.content;
        _currentAnswerIndex = 0;
    }

    // 3. Some questions answered and others not
    else
    {
        _questionsToAnswer = pendingQuestionsMutArray.copy;
        _currentAnswerIndex = 0;
    }

    [self loadNextQuestion];
}

- (void)doBack:(id)sender
{
    __weak typeof(self) weakself = self;
    [EvaluationController reloadEvaluationAnswers:weakself.section
                                  completionBlock:^(NSError *error, id object) {
                                      [weakself.navigationController popViewControllerAnimated:YES];
                                  }];
}

- (void)loadNextQuestion
{
    if (_currentAnswerIndex == [_questionsToAnswer count])
    {
        [self finishEvaluation];

        return;
    }

    ABAEvaluationQuestion *question = _questionsToAnswer[_currentAnswerIndex];
    [self loadQuestion:question];

    _currentAnswerIndex++;
}

- (IBAction)optionSelected:(id)sender
{
    _buttonOptionA.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionB.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionC.backgroundColor = ABA_COLOR_BLUE;

    _buttonOptionA.userInteractionEnabled = NO;
    _buttonOptionB.userInteractionEnabled = NO;
    _buttonOptionC.userInteractionEnabled = NO;

    [UIView animateWithDuration:0.3
                     animations:^{

                         _wrongImageView.alpha = 0.0f;
                     }];

    UIButton *senderButton = (UIButton *)sender;
    [senderButton setBackgroundColor:ABA_COLOR_DARKGREY];
    ABAEvaluationOption *optionSelected;
    if (senderButton == _buttonOptionA)
    {
        optionSelected = _currentQuestion.options[0];
    }
    else if (senderButton == _buttonOptionB)
    {
        optionSelected = _currentQuestion.options[1];
    }
    else if (senderButton == _buttonOptionC)
    {
        optionSelected = _currentQuestion.options[2];
    }

    typeof(self) __weak weakself = self;

    [EvaluationController evaluationQuestionAnsweredWithOption:optionSelected
                                               completionBlock:^(NSError *error, id object) {

                                                   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{

                                                       [weakself loadNextQuestion];
                                                   });

                                               }];
}

- (void)loadQuestion:(ABAEvaluationQuestion *)question
{
    _buttonOptionA.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionB.backgroundColor = ABA_COLOR_BLUE;
    _buttonOptionC.backgroundColor = ABA_COLOR_BLUE;

    [[Crashlytics sharedInstance] setObjectValue:_currentQuestion.question forKey:@"Current question"];
    [[LegacyTrackingManager sharedManager] trackInteraction];

    _currentQuestion = question;
    
    ABAEvaluationOption *optionA = _currentQuestion.options[0];
    ABAEvaluationOption *optionB = _currentQuestion.options[1];
    ABAEvaluationOption *optionC = _currentQuestion.options[2];

    // show or hide wrong image
    BOOL shouldShowWrongImage = NO;
    NSString *wrongText = @"";
    
    if (_currentQuestion.answered == nil)
    {
        ABAEvaluationOption *option;
        for (ABAEvaluationOption *optionLoop in _currentQuestion.options) {
            if (optionLoop.selected) {
                option = optionLoop;
            }
        }
        
        if (option && ![option.isGood boolValue])
        {
            shouldShowWrongImage = YES;
            wrongText = option.text;
        }
    }
    
    __weak typeof(self) weakSelf = self;

    [UIView animateWithDuration:0.3
        animations:^{
            _numPreguntaLabel.alpha = 0.0f;
            _questionLabel.alpha = 0.0f;
            _buttonOptionA.titleLabel.alpha = 0.0f;
            _buttonOptionB.titleLabel.alpha = 0.0f;
            _buttonOptionC.titleLabel.alpha = 0.0f;
            _wrongImageView.alpha = 0.0f;
        }
        completion:^(BOOL finished) {
            [_buttonOptionA setTitle:optionA.text forState:UIControlStateNormal];
            [_buttonOptionA setTitle:optionA.text forState:UIControlStateSelected];
            [_buttonOptionB setTitle:optionB.text forState:UIControlStateNormal];
            [_buttonOptionB setTitle:optionB.text forState:UIControlStateSelected];
            [_buttonOptionC setTitle:optionC.text forState:UIControlStateNormal];
            [_buttonOptionC setTitle:optionC.text forState:UIControlStateSelected];
            NSString *numPreguntaString = [NSString stringWithFormat:@"%@ %li %@ %lu",
                                                                     STRTRAD(@"evaluationPreguntaLabelKey", @"Pregunta"),
                                                                     (long)weakSelf.currentQuestion.order.integerValue,
                                                                     STRTRAD(@"evaluationPreguntaLabelDeKey", @"de"),
                                                                     (unsigned long)weakSelf.section.content.count];

            NSMutableAttributedString *numPreguntaAttrString = [[NSMutableAttributedString alloc] initWithString:numPreguntaString];
            [numPreguntaAttrString addAttribute:NSForegroundColorAttributeName value:ABA_COLOR_DARKGREY range:NSMakeRange(0, [numPreguntaString length])];
            [numPreguntaAttrString addAttribute:NSForegroundColorAttributeName value:ABA_COLOR_BLUE range:[numPreguntaString rangeOfString:weakSelf.currentQuestion.order.stringValue]];
            weakSelf.numPreguntaLabel.attributedText = numPreguntaAttrString;
            
            NSMutableParagraphStyle *questionStringParagraph = [[NSMutableParagraphStyle alloc] init];
            questionStringParagraph.lineSpacing = 5.0f;
            questionStringParagraph.lineBreakMode = NSLineBreakByTruncatingTail;
            NSAttributedString *questionAttrString = [[NSAttributedString alloc] initWithString:weakSelf.currentQuestion.question
                                                                                     attributes:@{ NSParagraphStyleAttributeName : questionStringParagraph }];
            
            if (shouldShowWrongImage && [weakSelf.currentQuestion.question rangeOfString:@"_"].location != NSNotFound)
            {
                
                NSUInteger location = [weakSelf.currentQuestion.question rangeOfString:@"_"].location;
                NSUInteger lenght = [[weakSelf.currentQuestion.question componentsSeparatedByString:@"_"] count] - 1;
                NSRange range = NSMakeRange(location, lenght);
                
                NSMutableString *mutString = weakSelf.currentQuestion.question.mutableCopy;
                [mutString replaceCharactersInRange:range withString:wrongText];
                
                NSMutableAttributedString *mutAttrString = [[NSMutableAttributedString alloc] initWithString:mutString.copy
                                                                                                  attributes:@{ NSParagraphStyleAttributeName : questionStringParagraph }];
                
                [mutAttrString addAttributes:@{
                                               NSFontAttributeName : [UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD ? 28.0f : 18.0f],
                                               NSForegroundColorAttributeName : [UIColor blackColor]
                                               }
                                       range:[mutAttrString.string rangeOfString:wrongText]];
                
                questionAttrString = mutAttrString.copy;
            }
            
            weakSelf.questionLabel.attributedText = questionAttrString;
            
            if (IS_IPAD){
                weakSelf.questionLabel.textAlignment = NSTextAlignmentCenter;
            }

            [UIView animateWithDuration:0.3
                             animations:^{
                                 weakSelf.numPreguntaLabel.alpha = 1.0f;
                                 weakSelf.questionLabel.alpha = 1.0f;
                                 weakSelf.buttonOptionA.titleLabel.alpha = 1.0f;
                                 weakSelf.buttonOptionB.titleLabel.alpha = 1.0f;
                                 weakSelf.buttonOptionC.titleLabel.alpha = 1.0f;

                                 weakSelf.buttonOptionA.userInteractionEnabled = YES;
                                 weakSelf.buttonOptionB.userInteractionEnabled = YES;
                                 weakSelf.buttonOptionC.userInteractionEnabled = YES;

                                 if (shouldShowWrongImage)
                                 {
                                     weakSelf.wrongImageView.alpha = 1.0f;
                                 }
                             }];
        }];
}

- (void)finishEvaluation
{
    EvaluationResultViewController *evaluationResultVC = [[UIStoryboard storyboardWithName:@"Evaluation" bundle:nil] instantiateViewControllerWithIdentifier:@"EvaluationResultViewController"];
    [evaluationResultVC setEvaluation:self.section];
    [self.navigationController pushViewController:evaluationResultVC animated:YES];
    
    // Cooladata tracking
    [self trackEvaluationFinished];
}

- (void)trackEvaluationFinished {

    NSMutableString *answers = [NSMutableString new];
    NSOrderedSet *evaluationAnswers = [EvaluationController getEvaluationAnswers:self.section];
    NSUInteger count = 0;
    for (ABAEvaluationOption *option in evaluationAnswers)
    {
        count++;
        [answers appendString:option.optionLetter];
        if (count != evaluationAnswers.count) {
            [answers appendString:@","];
        }
    }
    
    NSInteger correctAnswers = [EvaluationController getEvaluationCorrectAnswers:self.section];
    [CoolaDataTrackingManager trackEvaluation:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString responses:answers score:[NSString stringWithFormat:@"%ld", (long)correctAnswers]];
}

@end
