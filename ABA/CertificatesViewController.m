//
//  CertificatesViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "CertificatesViewController.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import "CertificateCell.h"
#import "LevelUnitController.h"
#import "ABARealmLevel.h"
#import "Resources.h"
#import "LegacyTrackingManager.h"
#import "Notifications.h"
#import <Crashlytics/Crashlytics.h>


static NSString *cellCertificate = @"CertificateCell";
static NSString *cellCertificate_iPad = @"CertificateCell_iPad";
static float heighCollectionCell = 77.0f;
static float heighCollectionCell_iPad = 129.0f;
static NSTimeInterval kAnimationTime = 1.0;

@interface CertificatesViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property RLMResults *dataSource;

@property BOOL appear;

@end

@implementation CertificatesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addMenuButtonWithTitle:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"yourCertificatesKey", @"Tus certificaciones")];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCollection) name:kUpdateRotationFromMenu object:nil];
    CLS_LOG(@"CertificatesVC: entering certificatesVC");
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    _appear = YES;
    
    _dataSource = [LevelUnitController getAllLevelsDescending];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"certificates"];

	__weak typeof (self) weakSelf = self;

    [LevelUnitController refreshAllLevelsProgress:^(NSError *error, id object) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [weakSelf.collectionView reloadData];
                       });
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [LevelUnitController getNumberOfLevels];
    
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGSize cellSize = CGSizeMake(self.view.frame.size.width, IS_IPAD?heighCollectionCell_iPad:heighCollectionCell);
    return cellSize;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
	CertificateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IS_IPAD?cellCertificate_iPad:cellCertificate forIndexPath:indexPath];
    
    ABARealmLevel *level = [_dataSource objectAtIndex:indexPath.row];
    
    if([level.completed boolValue]) {
        [cell.certificateImage setImage:[UIImage imageNamed:@"icon_certificate"]];
    }
    else {
        [cell.certificateImage setImage:[UIImage imageNamed:@"icon_certificateNO"]];
    }
    
    NSString *certificateName = [NSString stringWithFormat:@"certificateNameLevel%@",level.idLevel];
    cell.certificateTitleLabel.text = STRTRAD(certificateName, @"nombre del certificado");

    cell.certificateSubTitleLabel.text = [NSString stringWithFormat:@"%@ %@",STRTRAD(@"certificateLevelKey", @"Certificado nivel"),level.idLevel];

    cell.progressBarBackground.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    cell.progressBarBackground.layer.cornerRadius = 4.0;
    
    cell.progressBar.backgroundColor = ABA_COLOR_GREEN_COMPLETED;
    cell.progressBar.layer.cornerRadius = 4.0;

    if(_appear) {
        [UIView animateWithDuration:kAnimationTime animations:^{
            NSInteger progressLevel = [level getPercentageCertificateMaxProgress:cell.progressBarBackground.frame.size.width];
            cell.progressBarWidthConstraint.constant = progressLevel;
            [cell layoutIfNeeded];
        } completion:^(BOOL finished) {
            if((_dataSource.count-1) == indexPath.item) {
                _appear = NO;
            }
        }];
    }

    return  cell;
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    

}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)reloadCollection {
	dispatch_async(dispatch_get_main_queue(), ^
				   {
					   [self.collectionView reloadData];
				   });
}

#pragma mark - roatation events


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) { } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         [self.collectionView.collectionViewLayout invalidateLayout];
         kAnimationTime = 0.0;
         _appear = YES;
     }];
    
    [self reloadCollection];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

@end
