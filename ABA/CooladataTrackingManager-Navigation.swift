//
//  CooladataTrackingManager-Login.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/05/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

extension CoolaDataTrackingManager {

    fileprivate static let loginPropertiesKeys = ["user_id", "user_expiration_date", "user_user_type", "user_lang_id", "user_birthday", "user_level", "user_country_id", "user_partner_first_pay_id", "user_partner_source_id", "user_partner_current_id", "user_source_id", "user_device_type", "user_entry_date", "user_product", "user_conversion_date"]

    // MARK: Login

    class func trackLoginEvent(loginOfflineFlag loginOffline: Bool, userId: String) {
        // Reading user properties
        let userProperties = readScopedUserProperties()

        var loginProperties = createBasicNavigationMap()
        userProperties.keys.forEach { key in
            loginProperties[key] = userProperties[key] as AnyObject?
        }

        loginProperties["type"] = (loginOffline) ? "offline" as AnyObject? : "regular" as AnyObject?
        loginProperties["user_id"] = userId as AnyObject?
        loginProperties["device_language"] = LanguageController.getDeviceLanguageCodeKey() as AnyObject?;
        
        CoolaDataTracker.getInstance().trackEvent("LOGGED_IN", properties: loginProperties)
    }

    // MARK: Navigation tracking

    class func trackEnteredCourseIndex(_ userId: String, levelId: String) {
        var eventProperties = createBasicNavigationMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("ENTERED_COURSE_INDEX", properties: eventProperties)
    }

    class func trackEnteredUnit(_ userId: String, levelId: String, unitId: String) {
        var eventProperties = createBasicNavigationMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("ENTERED_UNIT", properties: eventProperties)
    }

    class func trackEnteredSection(_ userId: String, levelId: String, unitId: String, sectionType: CooladataSectionType) {
        var eventProperties = createBasicNavigationMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["level_id"] = levelId as AnyObject?
        eventProperties["unit_id"] = unitId as AnyObject?
        eventProperties["section_id"] = sectionTypeRepresentation(sectionType: sectionType) as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("ENTERED_SECTION", properties: eventProperties)
    }

    class func trackOpenedPrices(_ userId: String) {
        var eventProperties = createBasicNavigationMap()
        eventProperties["user_id"] = userId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("OPENED_PRICES", properties: eventProperties)
    }

    class func trackOpenedHelp(_ userId: String) {
        var eventProperties = createBasicNavigationMap()
        eventProperties["user_id"] = userId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("OPENED_HELP", properties: eventProperties)
    }

    class func trackSubscriptionSelected(_ userId: String, daysInPlan: Int) {
        var eventProperties = createBasicNavigationMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["selected_plan"] = String(daysInPlan) as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("SELECTED_PRICE_PLAN", properties: eventProperties)
    }
}
