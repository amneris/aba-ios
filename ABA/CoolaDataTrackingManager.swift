//
//  CoolaDataTrackingManager.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 24/02/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

class CoolaDataTrackingManager: NSObject
{
    fileprivate static let loginPropertiesKeys = ["user_expiration_date", "user_user_type", "user_lang_id", "user_birthday", "user_level", "user_country_id", "user_partner_first_pay_id", "user_partner_source_id", "user_partner_current_id", "user_source_id", "user_device_type", "user_entry_date", "user_product", "user_conversion_date", "user_id"]

    #if DEBUG
        private static let coolaDataKey: String = "cjp54xkbq4p61d16o29gq0dbg6nubetb"
    #else
        fileprivate static let coolaDataKey: String = "yde15a9wffj2vyufel7kx04aenz7x13a"
    #endif

    class func setup() {
        CoolaDataTracker.getInstance().setup(withAppKey: coolaDataKey);
    }

    // MARK: Login recording

    class func recordCooladataProperties(_ cooladataArray: [[String: AnyObject]]?) {

        // First it removes previous properties
        wipeUserProperties();

        guard let cooladataArray = cooladataArray else { return }

        // working only within the "userScope" dictionary
        cooladataArray.forEach { (cooladataPropertyDictionary) in

            guard let cooladataUserPropertyDictionary = cooladataPropertyDictionary["userScope"] as? [String: AnyObject]
            else { return }

            // once here "userScope" exists
            let defaults = UserDefaults.standard
            defer { defaults.synchronize() }

            // walking through the array of known properties and setting them into user defaults
            for propertyKey in loginPropertiesKeys
            {
                if let propertyValue = cooladataUserPropertyDictionary[propertyKey] as? String {
                    print("K: \(propertyKey) V: \(propertyValue)")
                    defaults.set(propertyValue, forKey: propertyKey)
                }
                else
                {
                    print("K: \(propertyKey) V: nil")
                    defaults.set(nil, forKey: propertyKey)
                }
            }
        }
    }

    class func wipeUserProperties() {
        let defaults = UserDefaults.standard
        defer { defaults.synchronize() }
        for propertyKey in loginPropertiesKeys
        {
            defaults.set(nil, forKey: propertyKey)
        }
    }

    fileprivate static let kUserScope = "{u}"
    class func readScopedUserProperties() -> [String: String]
    {
        let userProperties = readUserProperties()
        var scopedProperties = [String: String]()
        
        for propertyKey in userProperties.keys {
            let propertyScopedKey = kUserScope + propertyKey
            scopedProperties[propertyScopedKey] = userProperties[propertyKey]
        }
        
        return scopedProperties
    }
    
    class func readUserProperties() -> [String: String] {
        let defaults = UserDefaults.standard
        var properties = [String: String]()
        for propertyKey in loginPropertiesKeys
        {
            if let propertyValue = defaults.object(forKey: propertyKey) as? String
            {
                properties[propertyKey] = propertyValue
            }
            else
            {
                properties[propertyKey] = ""
            }
        }
        
        return properties
    }
}
