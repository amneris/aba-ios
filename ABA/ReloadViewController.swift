//
//  ReloadViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 19/02/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit

@IBDesignable class GenericButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            setupViews()
        }
    }
    //this init fires usually called, when storyboards UI objects created:
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    //during developing IB fires this init to create object
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    
    func setupViews() {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = cornerRadius > 0
    }
    
    //required method to present changes in IB
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupViews()
    }
}

@objc protocol ReloadViewControllerProtocol {
    func reloadButtonAction()
}

class ReloadViewController: UIViewController {
    @IBOutlet weak var errorTitle: UILabel!
    @IBOutlet weak var reloadButton: UIButton!
    
    var reloadProtocol: ReloadViewControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // STRTRAD swift version
        let errorString: String = NSLocalizedString("errorRegister", tableName: LanguageController.getCurrentLanguage(), comment: "error en el registro")
        let reloadButtonString: String = NSLocalizedString("reloadPlansAfterError", tableName: LanguageController.getCurrentLanguage(), comment: "Intentar de nuevo")
        
        self.errorTitle.text = errorString
        self.reloadButton.setTitle(reloadButtonString, for: UIControlState())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func reloadViewAction(_ sender: AnyObject) {
        reloadProtocol?.reloadButtonAction()
    }
}
