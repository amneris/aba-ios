//
//  ABAUnit.h
//  ABA
//
//  Created by Jordi Melé on 30/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Definitions.h"
#import "ABARole.h"

@class ABAEvaluation, ABAExercises, ABAFilm, ABAInterpret, ABARealmLevel, ABASpeak, ABAVideoClass, ABAVocabulary, ABAWrite, ABASection;

@interface ABAUnit : RLMObject

@property NSNumber<RLMBool> * completed;
@property NSString * desc;
@property NSString * filmImageInactiveUrl;
@property NSString * filmImageUrl;
@property NSNumber<RLMInt> * idUnit;
@property NSNumber<RLMInt> * progress;
@property NSString * teacherTip;
@property NSString * title;
@property NSString * unitImage;
@property NSString * unitImageInactive;
@property NSNumber<RLMInt> * unitSectionProgress;
@property NSString * videoClassImageUrl;
@property NSDate * lastChanged;

@property RLMArray<ABARole *><ABARole> *roles;
@property ABAEvaluation *sectionEvaluation;
@property ABAExercises *sectionExercises;
@property ABAFilm *sectionFilm;
@property ABAInterpret *sectionInterpret;
@property ABASpeak *sectionSpeak;
@property ABAVideoClass *sectionVideoClass;
@property ABAVocabulary *sectionVocabulary;
@property ABAWrite *sectionWrite;
@property (readonly, getter=isEvaluationSectionUnlocked) BOOL evaluationSectionUnlocked;
@property (readonly) NSString *idUnitString;

- (ABARealmLevel *)level;

/**
 *  Get the current level percentage in string format with %
 *
 *  @return string with formatted percentage
 */
-(NSString *)getPercentage;

/**
 *  Get the index of the unit for the current level (1...24)
 *
 *  @return NSInteger value
 */
-(NSInteger)getIndex;

-(NSNumber *)getPreviousUnitId;

+(kABASectionTypes) getCurrentSectionForUnit: (ABAUnit*) unit;

/**
 *  Get the current unit percentage in string format with %
 *
 *  @return string with formatted percentage
 */
+(NSString *) getPercentageUnit:(ABAUnit*) unit;

/**
 *  Get the current unit progress
 *
 *  @return float with progress
 */
+(float) getProgressUnit:(ABAUnit*) unit;

/**
 *  Looks for section data
 *
 *  @return BOOL
 */
- (BOOL)shouldDownloadSectionData;
- (ABASection *)getSectionForType:(kABASectionTypes)type;

@end

RLM_ARRAY_TYPE(ABAUnit)

