//
//  GenericTrackingManager.m
//  ABA
//
//  Created by MBP13 Jesus on 28/07/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "GenericTrackingManager.h"

#import "ABARealmLevel.h"

@implementation GenericTrackingManager

#pragma mark - Private methods (Attributes)

- (NSString *)attributeForRecordType:(RecordType)type
{
    // Event
    NSString *attributeName;
    switch (type)
    {
    case kRegisterTypeFacebook:
        attributeName = kRegistrationTypeFacebook;
        break;
    case kRegisterTypeMail:
        attributeName = kRegistrationTypeEmail;
        break;
    default:
        attributeName = kRegistrationTypeUnknown;
        break;
    }

    return attributeName;
}

- (NSString *)attributeForPlansViewControllerOriginType:(PlansViewControllerOriginType)plantype
{
    NSString *attributeName;
    switch (plantype)
    {
    case kPlansViewControllerOriginBanner:
        attributeName = kArrivalTypeBanner;
        break;
    case kPlansViewControllerOriginUnlockContent:
        attributeName = kArrivalTypeLockedSection;
        break;
    case kPlansViewControllerOriginMenu:
        attributeName = kArrivalTypeMenu;
        break;
    default:
        attributeName = kArrivalTypeUnknown;
        break;
    }

    return attributeName;
}

- (NSString *)attributeForNotificationStatus:(BOOL)enabled
{
    if (enabled)
    {
        return @"yes";
    }
    else
    {
        return @"no";
    }
}

- (NSDictionary *)dictionaryWithLevelChange:(ABARealmLevel *)fromLevel toLevel:(ABARealmLevel *)toLevel
{
    NSMutableDictionary *dictionaryWithLevelChange = [[NSMutableDictionary alloc] init];
    if (fromLevel.idLevel)
    {
        [dictionaryWithLevelChange setObject:fromLevel.idLevel forKey:@"Old level"];
    }
    if (toLevel.idLevel)
    {
        [dictionaryWithLevelChange setObject:toLevel.idLevel forKey:@"New level"];
    }

    return dictionaryWithLevelChange;
}

- (NSDictionary *)dictionaryWithTimestamp
{
    NSMutableDictionary *dictionaryWithTimestamp = [[NSMutableDictionary alloc] init];
    [dictionaryWithTimestamp setObject:[self currentMixpanelFormattedDate] forKey:@"Timestamp"];

    return dictionaryWithTimestamp;
}

- (NSDictionary *)dictionaryWithABTestingRegisterFunnel
{
    NSString *valueForExperimentVariant = @"Short registration funnel";
    
    NSMutableDictionary *attributesToSend = [[NSMutableDictionary alloc] init];
    [attributesToSend setObject:valueForExperimentVariant forKey:@"Register funnel type"];
    
    return attributesToSend;
}

- (NSString *)attributeForPlanInfoType:(PlanInfoType)type {
    // Event
    NSString *attributeName;
    switch (type)
    {
        case kPlanInfoTypePrivacyPolicy:
            attributeName = kPlanInfoTypePrivacy;
            break;
        case kPlanInfoTypeTermsOfUse:
            attributeName = kPlanInfoTypeTerms;
            break;
        default:
            attributeName = kPlanInfoTypeUnknown;
            break;
    }
    
    return attributeName;
}

#pragma mark - Private methods (Dictionaries)

/**
 Method to create a dictionary including initial progress, final progress and the minutes of study
 */
- (NSDictionary *)dictionaryFromSecondsOfStudyTime:(double)secondsOfStudy withInitialProgress:(NSNumber *)initialProgress withFinalProgress:(NSNumber *)finalProgress
{
    // Calculating minutes of study by dividing seconds by 60
    double minutes = secondsOfStudy / 60.0;

    NSMutableDictionary *dictionaryWithProperties = [NSMutableDictionary dictionary];
    if (initialProgress)
    {
        [dictionaryWithProperties setObject:initialProgress forKey:@"Initial progress"];
    }
    if (finalProgress)
    {
        [dictionaryWithProperties setObject:finalProgress forKey:@"Final progress"];
    }
    [dictionaryWithProperties setObject:[NSNumber numberWithDouble:minutes] forKey:@"Minutes of study"];

    return dictionaryWithProperties;
}

#pragma mark - Private methods (timestamp)

#define kMixpanelDatePattern @"yyyy-MM-dd'T'HH:mm:ss"

- (NSString *)currentMixpanelFormattedDate
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:kMixpanelDatePattern];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];

    NSDate *current = [NSDate new];
    NSString *currentDateString = [dateFormatter stringFromDate:current];
    return currentDateString;
}

@end
