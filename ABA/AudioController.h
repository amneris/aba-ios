//
//  AudioController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Definitions.h"
#import "Blocks.h"

@class ABAPulseViewController, ABAUnit;

@protocol AudioControllerDelegate <NSObject>
@optional

- (void)audioStarted;
- (void)audioFinished;
- (void)audioDidGetError:(NSError *)error;
- (void)recordingStarted;
- (void)recordingFinished;
- (void)recordingDidGetError:(NSError *)error;

@end

@interface AudioController : NSObject <AVAudioPlayerDelegate, AVAudioRecorderDelegate>

@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, strong) AVAudioRecorder *recorder;
@property (nonatomic, weak) ABAPulseViewController *pulseViewController;
@property (nonatomic, strong) NSTimer *playerPulseTimer;
@property (nonatomic, strong) NSTimer *recorderPulseTimer;
@property (nonatomic, strong) NSTimer *recorderMaxSecondsTimer;
@property (nonatomic, strong) NSString *currentAudioId;
@property (nonatomic, strong) ABAUnit *currentUnit;

@property (nonatomic, assign) id<AudioControllerDelegate> audioControllerDelegate;

@property kABASectionTypes sectionType;

@property kAudioControllerType audioType;

@property NSNumber *redownloadRetryCounter;

// #warning Must be private on future
- (void)playAudioFile:(NSString *)audioID withUnit:(ABAUnit *)unit forRec:(BOOL)rec;
- (void)recordAudioFile:(NSString *)audioID withUnit:(ABAUnit *)unit;
- (void)stopRecording;

- (BOOL)checkIfAudioExists:(NSString *)audioID andUnit:(ABAUnit *)unit forRec:(BOOL)rec;
- (void)downloadAudios:(NSArray *)audioArray withUnit:(ABAUnit *)unit completionBlock:(ErrorBlock)block;
- (BOOL)audiosDownloaded:(NSArray *)audioArray withUnit:(ABAUnit *)unit;

- (void)playSound:(NSString *)audioFile;

@end
