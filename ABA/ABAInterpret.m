//
//  ABAInterpret.m
//  ABA
//
//  Created by Marc Guell on 1/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAInterpret.h"
#import "ABAUnit.h"

@implementation ABAInterpret

-(BOOL)hasContent
{
    if (self.roles && self.dialog)
    {
        return YES;
    }
    return NO;
}

- (ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionInterpret == %@", self];
    RLMResults *results = [ABAUnit objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}


@end
