//
//  RealmMigrationController.h
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 09/02/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RealmMigrationController : NSObject

+ (void)migrateRealmVersion;

@end
