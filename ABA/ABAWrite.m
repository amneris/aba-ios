//
//  ABAWrite.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAWrite.h"
#import "ABAUnit.h"


@implementation ABAWrite

-(BOOL)hasContent
{
    if (self.content)
    {
        return YES;
    }
    return NO;
}

- (ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionWrite == %@", self];
    RLMResults *results = [ABAUnit objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}

@end
