//
//  ABAInterpret.h
//  ABA
//
//  Created by Marc Guell on 1/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASection+Methods.h"
#import "ABAInterpretRole.h"
#import "ABAInterpretPhrase.h"

@class ABAUnit;

@interface ABAInterpret : ABASection

@property (readonly) ABAUnit *unit;
@property RLMArray<ABAInterpretRole *><ABAInterpretRole> *roles;
@property RLMArray<ABAInterpretPhrase *><ABAInterpretPhrase> *dialog;

-(BOOL) hasContent;

@end

