//
//  TeacherCell.swift
//  ABA
//
//  Created by MBP13 Jesus on 06/03/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import SnapKit

class TeacherCell: UITableViewCell {
    
    static let TEACHER_CELL_REUSER_IDENTIFIER = "TeacherCellReuseIdentifier"
    static let TEACHER_CELL_HEIGHT = 100.0
    
    let teacherView: ABATeacherTipView
    
    init() {
        
        teacherView = ABATeacherTipView(teacherWithText: "", withImage: "teacher", withShadow: false)
        
        super.init(style: .default, reuseIdentifier: TeacherCell.TEACHER_CELL_REUSER_IDENTIFIER)
        
        self.contentView.addSubview(teacherView)
        configureUI()
        configureLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TeacherCell {
    func configureCell(teacherText: String?) {
        teacherView.titleLabel.text = teacherText
    }
}

internal extension TeacherCell {
    
    func configureUI() {
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
    }
    
    func configureLayout() {
        self.teacherView.snp.makeConstraints{ make in
            make.edges.equalTo(self.contentView)
        }
    }
}
