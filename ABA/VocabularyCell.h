//
//  VocabularyCell.h
//  ABA
//
//  Created by Jordi Mele on 27/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VocabularyCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *txtLabel;

@end
