//
//  AppDelegate.m
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "AppDelegate.h"
#import "APIManager.h"
#import "UserController.h"
#import "Resources.h"
#import "DataController.h"
#import "ABARealmUser.h"
#import "ABARealmLevel.h"
#import "ABAUnit.h"
#import "ABARealmCert.h"
#import "Utils.h"
#import "LevelUnitController.h"
#import "ABAError.h"
#import "LevelViewController.h"
#import "ABARole.h"
#import <Adjust/Adjust.h>
#import "SDVersion.h"
#import "LanguageController.h"
#import "RealmMigrationController.h"
#import "PlansViewController.h"
#import "ResideMenuViewController.h"
#import "LGAlertView.h"
@import Firebase;
@import SVProgressHUD;

//
#import "ProgressActionScheduler.h"

// Analytics
#import <Fabric/Fabric.h>
#import "MixpanelTrackingManager.h"
#import "LegacyTrackingManager.h"
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>

// Currency updater
#import "CurrencyManager.h"

// Shepherd
#import "ABAShepherdEditor.h"
#import "ABACoreShepherdConfiguration.h"
#import "ABAShepherdEditorClearKeychainPlugin.h"
#import "ABAShepherdAppVersioningPlugin.h"
#import "ABAShepherdEditorCrashPlugin.h"
#import "ABAShepherdCleanCachePlugin.h"
#import "ABAShepherdController.h"
#import "ABAShepherdCleanDDBBPlugin.h"
#import "ABAShepherdExternalLogin.h"
#import "ABAShepherdLanguagePlugin.h"
#import "ABAShepherdEvaluationPlugin.h"
#import "ABAShepherdForceUserTypePlugin.h"
#import "ABAShepherdUnitDownloaderPlugin.h"

// Purchases
#import "SubscriptionController.h"
#import "PurchaseHandler.h"

// Zendesk
#import <ZendeskSDK/ZendeskSDK.h>
#import "Definitions.h"

// Facebook
#import <FBSDKCoreKit/FBSDKCoreKit.h>

// Swift
#import "ABA-Swift.h"

#import "SDiOSVersion.h"
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate()

@property (nonatomic, strong) FirebaseRemoteConfigManager * firebaseRemoteConfig;

@end

@implementation AppDelegate

#pragma mark - Lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupTrackingSystemsWithOptions:launchOptions];
    
    CLS_LOG(@"App did finish launching");
    
    // @warning, this method needs to be called before deploying the DB
    [self detectFirstSessionAndNotify];
    
    // DB WORKER
    [RealmMigrationController migrateRealmVersion];
    
    _progressController = [ProgressController new];
    
#ifdef SHEPHERD_DEBUG
    // Plugings
    [self setupShepherd];
    application.applicationSupportsShakeToEdit = YES;
#endif
    
    // Environments
    [ABAShepherdEditor.shared registerConfiguration:[[ABACoreShepherdConfiguration alloc] init]];
    
    if (isTargetTest)
    {
        return YES;
    }
    
    [self setupZendesk];
    
    // Initiating purchases
    [SubscriptionController configureCargoWithListener:^(SubscriptionStatus status) {
        // Check status
        [[PurchaseHandler sharedInstance] handlePurchaseStatus:status];
    }];
    
    // Seding pending progress actions
    [[ProgressActionScheduler sharedManager] startScheduling];
    
    [[[RootConfigurator alloc] init] configureModuleForViewInput:self];
    
    [self configureForUITests];

    [self.presenter appDidFinishLaunching:application options:launchOptions];
    
    NSLog(@"--------------------didFinishLaunchingWithOptions");
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    CLS_LOG(@"App will resign active");
    NSLog(@"--------------------App will resign active");
    
    // Pausing the progress action scheduler. Actions will not be sent during the app in background
    [[ProgressActionScheduler sharedManager] pauseScheduling];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    CLS_LOG(@"App did enter backgound");
    NSLog(@"--------------------App did enter backgound");
    [[LegacyTrackingManager sharedManager] stopSession];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    CLS_LOG(@"App will enter foreground");
    NSLog(@"--------------------App will enter foreground");
    [[LegacyTrackingManager sharedManager] startSession];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    CLS_LOG(@"App did become active");
    NSLog(@"--------------------App did become active");
    
    __weak typeof(self) weakSelf = self;
    if (!_versionManager) {
        _versionManager = [[SupportedVersionManager alloc] init];
    }
    
    [_versionManager isCurrentVersionValid:^(BOOL isValid) {
        if (!isValid)
        {
            [self.presenter showBlockView];
        }
        else
        {
            [FBSDKAppEvents activateApp];
            
            [weakSelf syncProgress];
            
            // Updating currencies
            [CurrencyManager updateCurrencies];
        }
    }];
    
    // Resuming the progress action scheduler
    [[ProgressActionScheduler sharedManager] resumeScheduling];
    
    // Updating remote config from Firebase
    [self.firebaseRemoteConfig updateConfig];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    CLS_LOG(@"App will terminate");
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options
{
    DeepLinkController *deepLinkCtrl = [[DeepLinkController alloc] initWithUrl:url];
    
    if ([deepLinkCtrl canParseDeepLink])
    {
        NSLog(@"DEBUG: openURL");
        
        [deepLinkCtrl saveDeepLink];
        
        [self.presenter startApp];

        return YES;
    }

    return [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

#pragma mark - APNS lifecycle Methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[MixpanelTrackingManager sharedManager] registerToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler
{
    CLS_LOG(@"App did receive remote notification");
    
    [self.presenter appDidReceivePush:userInfo];
    [[MixpanelTrackingManager sharedManager] trackPushNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

#ifdef __IPHONE_8_0

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    // register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"])
    {
    }
    else if ([identifier isEqualToString:@"answerAction"])
    {
    }
}

#endif

#pragma mark - Private methods (app setup)

- (void)setupAppearance
{
    [SVProgressHUD setBackgroundColor:ABA_COLOR_DARKGREY];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
}

/**
 In order to detect whether is a first session for the user we are checking whether the DB is already deployed or not.
 If there is not a deployed DB then we consider it's a first app open.
 It there is a deployed DB then is not a first session.
 */
- (void)detectFirstSessionAndNotify
{
    // Checking if DB file exists
    NSString *filePath = [RLMRealmConfiguration defaultConfiguration].fileURL.relativePath;
    BOOL realmFileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    // If DB file does not exist then we consider it as first app open
    if (!realmFileExists)
    {
        NSLog(@"ABA English first session. Tracking...");
        
        CLS_LOG(@"App first session");
        
        // Sending event to mixpanel. The method itself makes sure the event it sent once
        [[MixpanelTrackingManager sharedManager] trackFirstAppOpenOnce];
    }
}

#pragma mark - Push private methods

- (void)checkPushConfigurationAndRegister
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *apnsStatus = [defaults objectForKey:kApnsConfigurationStateKey];
    
    // if apnsStatus is enabled or it has not been configured yet
    if ([apnsStatus isEqualToString:kApnsEnabled] || !apnsStatus)
    {
        [self registerForRemoteNotification];
    }
}

- (void)registerForRemoteNotification
{
    [[MixpanelTrackingManager sharedManager] trackNotificationSuperproperties:YES];
    
    // Registration is done for iOS8 since we are not targeting iOS7.
    UIApplication *application = [UIApplication sharedApplication];
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
    [application registerUserNotificationSettings:settings];
}

- (void)unregisterForRemoteNotification
{
    [[MixpanelTrackingManager sharedManager] trackNotificationSuperproperties:NO];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

#pragma mark - CrashlyticsDelegate

- (void)crashlyticsDidDetectReportForLastExecution:(CLSReport * __nonnull)report completionHandler:(void (^ __nonnull)(BOOL))completionHandler{
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kShowZendeskPopUp];
    [NSUserDefaults.standardUserDefaults synchronize];
    
    // Jesus: Recomendation from
    // https://docs.fabric.io/apple/crashlytics/advanced-setup.html#crashlytics-frames-appearing-in-crashes
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (completionHandler) {
            completionHandler(YES);
        }
    }];
}

#pragma mark - App configuaration methods

-(void)setupShepherd {
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdEditorClearKeychainPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdEditorCrashPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdAppVersioningPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdCleanCachePlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdCleanDDBBPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdExternalLogin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdLanguagePlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdEvaluationPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdForceUserTypePlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdAutomatorPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdLoginPlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdProfilePlugin new]];
    [ABAShepherdEditor.shared registerPlugin:[ABAShepherdUnitDownloaderPlugin new]];
}

-(void)setupZendesk {
    [ZDKConfig instance].userLocale = [NSLocale preferredLanguages][0];
    [[ZDKConfig instance] initializeWithAppId:@"c702d12fdf9de1f046c558db0ead6ef24c048f6f64ae16f8" zendeskUrl:@"https://abaenglish.zendesk.com" clientId:@"mobile_sdk_client_ef7684c1b8f3be76646b"];
    
#ifdef DEBUG
    [ZDKLogger enable:NO];
    [ZDKLogger setLogLevel:ZDKLogLevelVerbose];
#endif
    
    [self setupZendeskAppearance];
}

- (void)setupTrackingSystemsWithOptions:(NSDictionary *)options
{
    // Crashlytics
    [[Crashlytics sharedInstance] setDelegate:self];
    [Fabric with:@[ CrashlyticsKit ]];

    [LegacyTrackingManager sharedManager];
    [MixpanelTrackingManager sharedManager];

    [self setupAppearance];
    // Before it was done with eMMa
    // It check the push configuration and register in case of having APNS enabled
    [self checkPushConfigurationAndRegister];

    [CoolaDataTrackingManager setup];

    // Mixpanel push tracking
    [[MixpanelTrackingManager sharedManager] trackPushNotificationIfExists:options];
    
    NSString * firebaseConfigFile = nil;
#ifdef RELEASE
    firebaseConfigFile = @"GoogleService-Info";
#else
    firebaseConfigFile = @"GoogleService-Info-debug";
#endif
    
    self.firebaseRemoteConfig = [[FirebaseRemoteConfigManager alloc] init];
#ifdef TEST
#else
    self.firebaseRemoteConfig.tracker = [MixpanelTrackingManager sharedManager];
#endif
    
    FIROptions * fireOptions = [[FIROptions alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:firebaseConfigFile ofType:@"plist"]];
    [FIRApp configureWithOptions:fireOptions];
}

#pragma mark - RootViewInput

- (void)resetAPNSConfiguration
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:kApnsEnabled forKey:kApnsConfigurationStateKey];
    [defaults synchronize];
}

- (void)syncProgress
{
    if ([UserController isUserLogged])
    {
        [APIManager sharedManager].token = [UserController currentUser].token;
        [_progressController getCourseProgress:^(NSError *error, id object) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kUnitProgressUpdatedNotification object:nil];
        }];
    }
}

- (UIWindow *)appWindow
{
    if (!self.window)
    {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }

    return self.window;
}

- (void)showPushAlert:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDestructive
                                                     handler:nil];
    [alert addAction:okAction];
    [self.appWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Zendesk helpers

#pragma get additional info about device for Zendesk ticket

- (NSString *)requestDeviceInfo
{
    NSString *deviceModel = [NSString stringWithFormat:@"Device: %@", DeviceVersionNames[[SDVersion deviceVersion]]];
    NSString *systemVersion = [NSString stringWithFormat:@"iOS ver: %@", [UIDevice currentDevice].systemVersion];
    NSString *appVersion = [NSString stringWithFormat:@"App ver: %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];

    return [NSString stringWithFormat:@"\n-----------------------\n%@ \n%@ \n%@", deviceModel, systemVersion, appVersion];
}

- (void)setupZendeskAppearance
{
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];

    [ZDKHelpCenter setNavBarConversationsUIType:ZDKNavBarConversationsUITypeImage];
    [ZDKRequests setNavBarCreateRequestUIType:ZDKNavBarCreateRequestUITypeImage];

    [ZDKRequests configure:^(ZDKAccount *account, ZDKRequestCreationConfig *requestCreationConfig) {
        requestCreationConfig.additionalRequestInfo = [self requestDeviceInfo];
        requestCreationConfig.subject = STRTRAD(@"emailTitleHelpKey", @"New ABA English App - ¡Necesito ayuda!");

        NSString *deviceLanguage = [LanguageController getDeviceLanguageCodeKey];
        requestCreationConfig.tags = @[ deviceLanguage ];
    }];

#pragma mark global appearance

    //Set title attributes
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                           [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 18 : 16],
                                                                           NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];

    // set backbutton image and force size and alignment
    UIImage *backButtonImage = [[UIImage imageNamed:@"back"] imageWithAlignmentRectInsets:UIEdgeInsetsMake(5, 0, 5, 0)];
    [UINavigationBar appearance].backIndicatorImage = backButtonImage;
    [UINavigationBar appearance].backIndicatorTransitionMaskImage = backButtonImage;

    // hide backbutton text
    UIOffset backButtonTextOffset = UIOffsetMake(0, -60);
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:backButtonTextOffset
                                                         forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBarTintColor:ABA_COLOR_DARKGREY];
    [[UINavigationBar appearance] setTranslucent:NO];

    [[UISearchBar appearance] setBarTintColor:ABA_COLOR_REG_BACKGROUND2];
    
    [[UIScrollView appearanceWhenContainedInInstancesOfClasses:@[[ZDKRequestListViewController class]]] setBackgroundColor:ABA_COLOR_REG_BACKGROUND2];
}

- (void)configureForUITests
{
    if ([[[NSProcessInfo processInfo].environment objectForKey:@"uitesting"] isEqualToString:@"1"])
    {
        [UIView setAnimationsEnabled:NO];

        [UserController logout];
        ABAShepherdEditor *sharedShepherd = [ABAShepherdEditor shared];
        for (id<ABAShepherdConfiguration> config in sharedShepherd.configurations)
        {
            for (id<ABAShepherdEnvironment> environment in config.environments)
            {
                if ([environment.environmentName isEqualToString:@"Integration"])
                {
                    [sharedShepherd setcurrentEnvironment:environment forConfiguration:config];
                    break;
                }
            }
        }
    }
}
@end
