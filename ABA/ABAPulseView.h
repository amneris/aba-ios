
//
//  ABAPulseView.h
//  ABA
//
//  Created by Jaume Cornadó Panadés on 21/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABAPulseView : UIView

- (CABasicAnimation *)animationWithKeyPath:(NSString *)keyPath;

@end
