//
//  InterpretConversationViewController.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@class ABAInterpret, ABAInterpretRole, InterpretViewController, ABAInterpretPhrase;

@interface InterpretConversationViewController : UIViewController

@property (strong, nonatomic) ABAInterpret *interpret;
@property (strong, nonatomic) ABAInterpretRole *currentRole;
@property (strong, nonatomic) InterpretViewController *previousVC;
@property (strong, nonatomic) ABAInterpretPhrase *currentPhrase;
@property (nonatomic) kABAInterpretConversationType type;
@property (nonatomic) kAudioControllerType audioType;

@end
