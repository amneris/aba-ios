//
//  PublicIpRepository.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import Moya

enum PublicIpRepositoryError: Swift.Error, CustomStringConvertible {
    var description: String { return "PublicIpRepositoryError.." }
    case errorGettingIP
}

class PublicIpRepository {
    class func publicIpAPICall() -> Observable<String> {
        return APIProvider
            .request(.publicIP).map{ response -> Moya.Response in
                guard response.statusCode == 200 else { throw PublicIpRepositoryError.errorGettingIP }
                return response
            }
            .mapJSON()
            .map { ipDictionary -> String in
                if let ipAddress = try IpAddressParser.parseIpAddress(ipDictionary) {
                    return ipAddress.ip
                } else if let _ = try NetworkErrorParser.parseNetworkError(ipDictionary) {
                    throw LoginRepositoryError.networkErrorResponse
                }
                
                throw PublicIpRepositoryError.errorGettingIP
        }
    }

}
