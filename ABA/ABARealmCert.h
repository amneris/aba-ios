//
//  ABARealmCert.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@class ABARealmLevel;

@interface ABARealmCert : RLMObject

@property NSString *url;
@property ABARealmLevel *level;

@end

RLM_ARRAY_TYPE(ABARealmCert)
