//
// Created by Oleksandr Gnatyshyn on 13/06/16.
// Copyright (c) 2016 ABA English. All rights reserved.
//

import RxSwift
import Foundation

@objc
class LoginPresenter: NSObject {

    // VIPER
    weak var view: LoginViewInput?
    var interactor: LoginInteractorInput?
    var router: SessionRouterInput?
    var facebookInteractor: FacebookInteractorInput?

    var tracker: LoginTracker?

    // RX
    let disposableBag = DisposeBag()

    init(tracker: LoginTracker?) {
        self.tracker = tracker
    }
}

extension LoginPresenter: LoginViewOutput {

    func login(withEmail email: String, andPassword: String) {
        loginWithEmailAndPassword(email, password: andPassword)
    }
}

extension LoginPresenter: LoginFormViewOutput {

    func loginWithEmailAndPassword(_ email: String, password: String) {
        let validation = parameterValidation(email, pass: password)
        switch validation {
        case .Success:
            // Params are valid
            view?.showLoadingIndicator()
            performLogin(email, pass: password)
        default:
            view?.renderValidationError(validation)
        }
    }

    func loginUserWithFacebook() {
        performFacebookRegistation()
    }

    func goToForgotPassword() {
        self.router?.goToForgotPassword()
    }

    func goToRegister() {
        self.router?.openRegister()
    }

    // FormValidator

    func validateName(_ name: String?) {
        // Does nothing
    }

    func validateEmail(_ email: String?) {
        if !isEmailValid(email) {
            self.view?.renderValidationError(.WrongEmailFormat)
        }
    }

    func validatePassword(_ password: String?) {
        if !isPasswordValid(password) {
            self.view?.renderValidationError(.WrongPasswordFormat)
        }
    }
}

extension LoginPresenter: CredentialsStorer {

}

////////////////////////////////////
// MARK: private methods
////////////////////////////////////

extension LoginPresenter {

    func performLogin(_ email: String, pass: String) {
        
        let dependencies = Dependencies()
        
        interactor?.login(email: email, password: pass)
            .subscribeOn(dependencies.backgroundWorkScheduler)
            .observeOn(dependencies.mainScheduler)
            .subscribe { event in
                switch event {
                case .next:
                    self.tracker?.trackLoginWithEmailAndPassword()
                    self.view?.hideLoadingIndicator()
                    self.loginFinishedSuccessfully(email, pass: pass, newUser: false)
                case .error:
                    self.view?.hideLoadingIndicator()
                    if let loginerror = event.error as? LoginRepositoryError, loginerror == LoginRepositoryError.wrongUserPassword {
                        self.view?.showEmailPasswordError()
                    }
                    else {
                        self.view?.showGenericError()
                    }
                    print("login error")
                default:
                    print("Error: Login presenter - performLogin - default")
                }
        }.addDisposableTo(disposableBag)
    }

    func performFacebookRegistation() {
        
        let dependencies = Dependencies()
        
        facebookInteractor?
            .loginWithFacebook()
            .subscribeOn(dependencies.mainScheduler)
            .do(onNext: { _ in self.view?.showLoadingIndicator() })
            .observeOn(dependencies.backgroundWorkScheduler)
            .flatMap({ facebookUser -> Observable<User> in
                if let interactor = self.interactor {
                    return interactor.loginWithFacebook(facebookUser)
                } else {
                    return Observable.error(SessionInteractorError.unableToLogin)
                }
        })
            .observeOn(dependencies.mainScheduler)
            .subscribe { event in
                switch event {
                case .next:
                    self.view?.hideLoadingIndicator()

                    guard let loggedUser = event.element else {
                        self.view?.showGenericError()
                        return
                    }

                    self.tracker?.trackAccessWithFacebook(loggedUser.email!, name: loggedUser.name!, newUser: loggedUser.isNewUser)
                    self.loginFinishedSuccessfully(loggedUser.email!, pass: nil, newUser: loggedUser.isNewUser)

                case .error:
                    self.view?.hideLoadingIndicator()

                    guard let interactorError = event.error as? SessionInteractorError else {
                        self.view?.showGenericError()
                        return
                    }

                    if interactorError == SessionInteractorError.facebookUserCanceled {
                        // Do nothing as the user cancelled the facebook login on purpose
                    }
                    else if interactorError == SessionInteractorError.facebookGenericError {
                        self.view?.showFacebookError()
                    }
                    else if interactorError == SessionInteractorError.facebookEmailNotGranted {
                        self.view?.showPermissionError()
                    }
                    else {
                        self.view?.showGenericError()
                    }

                default:
                    print("OnComplete: register presenter - performregistration - default")
                }

        }.addDisposableTo(disposableBag)
    }

    func loginFinishedSuccessfully(_ email: String, pass: String?, newUser: Bool) {
        // Storing login credentials
        storeLogin(email, pass: pass)

        // rendering confirmation into view
        view?.renderLoginConfirmation()

        // Navigation
        if newUser {
            router?.goToLevelSelection()
        } else {
            router?.goToUnitList()
        }
    }
}
