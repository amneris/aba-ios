//
//  UnitViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@class ABAUnit;

@interface UnitRoscoViewController : UIViewController

@property (nonatomic) BOOL newRegister;

-(void)setUnit:(ABAUnit *)unit;

@end
