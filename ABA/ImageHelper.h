//
//  ImageHelper.h
//  ABA
//
//  Created by Marc Güell Segarra on 13/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@class ABAUnit, ABARealmLevel;
@class UIImage, UIView;

@interface ImageHelper : NSObject

@property (nonatomic) BOOL isDownloading;

//-(void)loadImagesInBackground;
-(void)loadImagesInBackgroundForLevel: (ABARealmLevel *)level;
-(void)loadImagesInBackgroundForUnit: (ABAUnit *)unit;
-(void)downloadImagesForUnit: (ABAUnit *)unit withBlock:(ErrorBlock)block;

+(NSString *)change3xIfNeededWithUrlString:(NSString *)urlString;

+(UIImage *)imageWithView:(UIView *)view;

+ (void)getImageWithURL:(NSString *)urlString completionBlock:(CompletionBlock)completionBlock;

@end
