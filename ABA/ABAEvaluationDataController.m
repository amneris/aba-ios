//
//  ABAEvaluationDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAEvaluationDataController.h"
#import "ABAEvaluation.h"
#import "ABAUnit.h"

@class ABAUnit;

@implementation ABAEvaluationDataController

+ (void)parseABAEvaluationSection:(NSArray *)abaEvaluationDataArray onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAEvaluation *evaluation = unit.sectionEvaluation;
    
    [realm transactionWithBlock:^{
        
        if (!evaluation)
        {
            evaluation = [[ABAEvaluation alloc] init];
            unit.sectionEvaluation = evaluation;
        }
        
        for (NSDictionary *evaluationQuestion in abaEvaluationDataArray)
        {
            ABAEvaluationQuestion *question = [[ABAEvaluationQuestion alloc] init];
            question.order = [NSNumber numberWithInteger:[evaluationQuestion[@"Order"] integerValue]];
            question.question = evaluationQuestion[@"Question"];
            [evaluation.content addObject:question];
            
            ABAEvaluationOption *optionA = [[ABAEvaluationOption alloc] init];
            optionA.text = evaluationQuestion[@"OptionA"];
            optionA.isGood = @NO;
            optionA.optionLetter = @"a";
            [question.options addObject:optionA];
            
            ABAEvaluationOption *optionB = [[ABAEvaluationOption alloc] init];
            optionB.text = evaluationQuestion[@"OptionB"];
            optionB.isGood = @NO;
            optionB.optionLetter = @"b";
            [question.options addObject:optionB];
            
            ABAEvaluationOption *optionC = [[ABAEvaluationOption alloc] init];
            optionC.text = evaluationQuestion[@"OptionC"];
            optionC.isGood = @NO;
            optionC.optionLetter = @"c";
            [question.options addObject:optionC];
            
            if ([evaluationQuestion[@"Answer"] isEqualToString:@"a"])
            {
                optionA.isGood = @YES;
            }
            else if ([evaluationQuestion[@"Answer"] isEqualToString:@"b"])
            {
                optionB.isGood = @YES;
            }
            else if ([evaluationQuestion[@"Answer"] isEqualToString:@"c"])
            {
                optionC.isGood = @YES;
            }
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, evaluation);
    }
    else
    {
        block(error, nil);
    }
}

+ (void)setQuestionEvaluationDone:(ABAEvaluationOption *)option completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        
        option.selected = @YES;
        option.question.answered = @YES;
        
        // clean all other previusly selected options
        for (ABAEvaluationOption *anotherOption in option.question.options)
        {
            if (![anotherOption.text isEqualToString:option.text])
            {
                anotherOption.selected = nil;
            }
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, nil);
    }
    else
    {
        block(error, nil);
    }
}

+ (void)setAllQuestionEvaluationNone:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        
        for (ABAEvaluationQuestion * question in evaluation.content){
            question.answered = nil;
            for (ABAEvaluationOption * option in question.options){
                option.selected = nil;
            }
        }
        
    } error:&error];
    
    if (!error){
        block(nil, nil);
    }else{
        block(error, nil);
    }
}

+ (void)setWrongQuestionEvaluationNone:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        
        for (ABAEvaluationQuestion *question in evaluation.content)
        {
            if ([question.answered boolValue])
            {
                ABAEvaluationOption *option;
                for (ABAEvaluationOption *optionLoop in question.options)
                {
                    if ([optionLoop.selected boolValue])
                    {
                        option = optionLoop;
                    }
                }
                
                if (![option.isGood boolValue])
                {
                    question.answered = nil;
                }
            }
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, nil);
    }
    else
    {
        block(error, nil);
    }
}

@end
