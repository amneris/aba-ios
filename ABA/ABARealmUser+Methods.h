//
//  ABARealmUser+Methods.h
//  ABA
//
//  Created by Jesus Espejo on 25/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABARealmUser.h"

@interface ABARealmUser(Methods)

/**
 Method to calculate whether the last login was done not so long ago enough. There is a window of time defined within the implementation.
 @return BOOL that will reveal whether the user did login within the defined window of time
 */
- (BOOL)isRecentLogin;

@end
