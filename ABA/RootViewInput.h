//
//  RootViewInput.h
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RootViewInput <NSObject>

// Accesors
- (UIWindow *)appWindow;

// Actions
- (void)resetAPNSConfiguration;
- (void)syncProgress;
- (void)showPushAlert:(NSString *)message;

@end
