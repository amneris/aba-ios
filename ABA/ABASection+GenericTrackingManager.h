//
//  ABASection+MixpanelTracking.h
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABASection+Methods.h"
#import "TrackingManagerProtocol.h"

@interface ABASection(GenericTrackingManager) < ABATrackingManagerProtocol >

@end
