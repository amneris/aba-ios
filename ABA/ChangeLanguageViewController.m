//
//  ChangeLanguageViewController.m
//  ABA
//
//  Created by Jordi Mele on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ChangeLanguageViewController.h"
#import "UIViewController+ABA.h"
#import "Utils.h"
#import "Resources.h"
#import "ABALanguage.h"
#import "LanguageCell.h"
#import "LanguageController.h"
#import "AppDelegate.h"
#import "DataController.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "ABARealmUser.h"

static NSString *cellLanguage = @"LanguageCell";
static float heighCollectionCell = 75.0f;
static NSString *cellLanguage_iPad = @"LanguageCell_iPad";
static float heighCollectionCell_iPad = 100.0f;

@interface ChangeLanguageViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property NSMutableArray *dataSource;

@end

@implementation ChangeLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackButtonNavBar];
    [self addTitleNavbar:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"changeLanguageTitleKey", @"Idioma de la aplicación")];
    
    [self setupView];
    
    [self createDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void) setupView {
    
    _headerView.backgroundColor = ABA_COLOR_BLUE_INFO;
    
    _headerTitleLabel.textColor = [UIColor whiteColor];
    [_headerTitleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    _headerTitleLabel.text = STRTRAD(@"changeLanguageTitleKey", @"Selecciona el idioma de la aplicación");
    
    _collectionView.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    
}

-(void) createDataSource {
    
    _dataSource = [[NSMutableArray alloc] init];
    
    for (int i=0; i<kSuportedLanguagesArray.count; i++) {
        
        ABALanguage *language = [ABALanguage new];
        
        language.idLanguage = [kSuportedLanguagesArray objectAtIndex:i];
        
        if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"de"]) {
            language.nameLanguage = STRTRAD(@"languageDeutchKey", @"alemán");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"es"]) {
            language.nameLanguage = STRTRAD(@"languageSpainKey", @"español");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"fr"]) {
            language.nameLanguage = STRTRAD(@"languageFrenchKey", @"francés");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"en"]) {
            language.nameLanguage = STRTRAD(@"languageEnglishKey", @"inglés");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"it"]) {
            language.nameLanguage = STRTRAD(@"languageItalianKey", @"italiano");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"pt"]) {
            language.nameLanguage = STRTRAD(@"languagePortugueseKey", @"portugués");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"ru"]) {
            language.nameLanguage = STRTRAD(@"languageRussianKey", @"ruso");
        }
        else if([[kSuportedLanguagesArray objectAtIndex:i] isEqualToString:@"zh"]) {
            language.nameLanguage = STRTRAD(@"languageChinneseKey", @"chino");
        }
        
        [_dataSource addObject:language];
    }
    
    [_collectionView reloadData];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return _dataSource.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(_collectionView.frame.size.width, IS_IPAD?heighCollectionCell_iPad:heighCollectionCell);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LanguageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IS_IPAD?cellLanguage_iPad:cellLanguage forIndexPath:indexPath];
    
    ABALanguage *languageCell = [_dataSource objectAtIndex:indexPath.row];
    
    [cell.nameLanguageLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?22:18]];
    NSString *capitalized = [[[languageCell.nameLanguage substringToIndex:1] uppercaseString] stringByAppendingString:[languageCell.nameLanguage substringFromIndex:1]];
    cell.nameLanguageLabel.text = capitalized;
    
    if([languageCell.idLanguage isEqualToString:[LanguageController getCurrentLanguage]]) {
        cell.languageImage.hidden = NO;
        cell.backgroundColor = ABA_COLOR_DARKGREY;
        cell.nameLanguageLabel.textColor = [UIColor whiteColor];
    }
    else {
        cell.languageImage.hidden = YES;
        cell.backgroundColor = [UIColor whiteColor];
        cell.nameLanguageLabel.textColor = ABA_COLOR_DARKGREY;
    }
    
    return  cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Idima Actual : %@",[LanguageController getCurrentLanguage]);
    ABALanguage *cellLanguage = [_dataSource objectAtIndex:indexPath.row];
    
    [LanguageController setCurrentLanguage:cellLanguage.idLanguage];
    NSLog(@"Idima Modificado : %@",[LanguageController getCurrentLanguage]);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.presenter loadMainStoryboard];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
