//
//  ABANavigationController.m
//  ABA
//
//  Created by Oriol Vilaró on 12/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABANavigationController.h"
#import "Resources.h"
#import "Utils.h"

@interface ABANavigationController ()

@end

@implementation ABANavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationBar setBarTintColor:ABA_COLOR_DARKGREY];
    [self.navigationBar setTranslucent:NO];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    if(IS_IPAD)
    {
        return UIInterfaceOrientationMaskAll;
    }
    else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (UITraitCollection *)overrideTraitCollectionForChildViewController:(UIViewController *)childViewController
{
    if (CGRectGetWidth(self.view.bounds) < CGRectGetHeight(self.view.bounds)) {
        return [UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassCompact];
    } else {
        return [UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassRegular];
    }
}

@end
