//
//  LevelCell.h
//  ABA
//
//  Created by Oriol Vilaró on 31/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LevelCell;

@protocol LevelCellDelegate <NSObject>

-(void) infoButtonPressed:(LevelCell*) sender;

@end

@interface LevelCell : UICollectionViewCell


@property (weak) id<LevelCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *infoButton;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)infoButtonAction:(id)sender;

@end
