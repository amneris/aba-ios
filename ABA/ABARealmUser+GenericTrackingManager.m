//
//  ABARealmUser+MixpanelTracking.m
//  ABA
//
//  Created by Jesus Espejo on 19/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABARealmUser+GenericTrackingManager.h"
#import "ABARealmLevel.h"
#import "TrackingDefinitions.h"
#import "Utils.h"

@implementation ABARealmUser(GenericTrackingManager)

- (NSDictionary *)trackingProperties
{
    NSMutableDictionary * dictionaryWithProperties = [[NSMutableDictionary alloc] init];
    
    if (self.idUser)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Userid" : self.idUser}];
    }
    if (self.name)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"$name" : self.name}];
    }
    if (self.email)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"$email" : self.email}];
    }
    if (self.country)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"User country" : self.country}];
    }
    if (self.userLang)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Language" : self.userLang}];
    }
    if (self.currentLevel.idLevel)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Level" : self.currentLevel.idLevel}];
    }
    if (self.teacherId)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Teacherid" : self.teacherId}];
    }
    if (self.teacherName)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Teacher name" : self.teacherName}];
    }
    if (self.partnerID)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"PartnerID" : self.partnerID}];
    }
    if (self.sourceID)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"SourceID" : self.sourceID}];
    }
    
    // Migration
    if ([self respondsToSelector:@selector(gender)] && self.gender)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Gender" : self.gender}];
    }
    if ([self respondsToSelector:@selector(phone)] && self.phone)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Phone" : self.phone}];
    }
    if ([self respondsToSelector:@selector(birthdate)] && self.birthdate)
    {
        [dictionaryWithProperties addEntriesFromDictionary:@{@"Birthdate" : self.birthdate}];
    }
    
    // User type
    [dictionaryWithProperties addEntriesFromDictionary:@{@"User Type" : [self userTypeString]}];

    // Adding device information
    if (IS_IPAD) {
        [dictionaryWithProperties setObject:@"yes" forKey:@"Has iPad"];
    } else {
        [dictionaryWithProperties setObject:@"yes" forKey:@"Has iPhone"];
    }

    return dictionaryWithProperties;
}

- (NSDictionary *)trackingSuperProperties
{
    NSMutableDictionary * dictionaryWithSuperProperties = [[NSMutableDictionary alloc] init];
    
    if (self.idUser)
    {
        [dictionaryWithSuperProperties addEntriesFromDictionary:@{@"Userid" : self.idUser}];
    }
    
    if (self.email)
    {
        [dictionaryWithSuperProperties addEntriesFromDictionary:@{@"Email" : self.email}];
    }
    
    if (self.partnerID)
    {
        [dictionaryWithSuperProperties setObject:self.partnerID forKey:@"PartnerID"];
    }
    
    // User type
    [dictionaryWithSuperProperties setObject:[self userTypeString] forKey:@"User Type"];
    return dictionaryWithSuperProperties;
}

#pragma mark - Private methods

- (NSString *)userTypeString
{
    // Free / Premium / Teacher (0-lead,Free-1,Premium-2,Plus-3, Teacher-4)
    if ([self.type isEqualToString:@"2"])
    {
        return kUserTypePremium;
    }
    else
    {
        return kUserTypeFree;
    }
}

@end
