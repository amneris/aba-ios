//
//  SpeakController.m
//  ABA
//
//  Created by Jordi Mele on 3/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SpeakController.h"
#import "ABASpeak.h"
#import "ABASpeakPhrase.h"
#import "ABASpeakDialog.h"
#import "ABASpeakDialogPhrase.h"
#import "PhrasesController.h"

@implementation SpeakController

-(RLMArray*) getDialogDataSourceFromSpeak: (ABASpeak*) speak {
    return speak.content;
}

-(ABASpeakDialog*) getCurrentDialogFromSection:(ABASpeak *)speak {
    
    for(ABASpeakDialog *dialog in [self getDialogDataSourceFromSpeak:speak]) {
        for(ABASpeakPhrase *phrase in dialog.dialog) {
            if(!phrase.done)
                return dialog;
        }
        
        for(ABASpeakPhrase *phrase in dialog.sample) {
            if(!phrase.done && [phrase.subPhrases count] == 0)
                return dialog;
            
            for(ABASpeakPhrase *subPhrase in phrase.subPhrases) {
                if(!subPhrase.done && ![subPhrase.text isEqualToString:@""])
                    return dialog;
            }
        }
    }
    
    return nil;
}

-(id) getCurrentPhraseFromSection:(ABASpeak *)speak {
    
    for(ABASpeakDialog *dialog in [self getDialogDataSourceFromSpeak:speak]) {
        for(ABASpeakDialogPhrase *dPhrase in dialog.dialog) {
            if(!dPhrase.done)
                return dPhrase;
        }
        
        for(ABASpeakPhrase *phrase in dialog.sample) {
            if(!phrase.done && [phrase.subPhrases count] == 0)
                return phrase;
            
            for(ABASpeakPhrase *subPhrase in phrase.subPhrases) {
                if(!subPhrase.done && ![subPhrase.text isEqualToString:@""])
                    return phrase;
            }
        }
    }
    
    return nil;
}

-(ABASpeakPhrase *) getCurrentSamplePhraseFromSection: (ABASpeak*) speak {
    
    for(ABASpeakPhrase *phrase in [self getCurrentDialogFromSection:speak].sample) {
        
        if([phrase.subPhrases count] > 0) {
            for(ABAPhrase* subPhrase in phrase.subPhrases) {
                if(!subPhrase.done && ![subPhrase.text isEqualToString:@""])
                    return phrase;
            }
        }
        
        if(![phrase.done boolValue] && [phrase.subPhrases count] == 0) {
            return phrase;
        }
        
    }
    
    return nil;
}

-(NSInteger) getTotalPhraseCompletedForSpeakSection:(ABASpeak *)speak {
    
    NSInteger total = 0;
    
    for(ABASpeakDialog *dialog in speak.content) {
        for(ABASpeakPhrase *phrase in dialog.dialog) {
            
            if([phrase.listened boolValue])
                total++;
            
            if([phrase.done boolValue])
                total ++;
            
        }
        for(ABASpeakPhrase *phraseSample in dialog.sample) {
            if([phraseSample.subPhrases count] > 0) {
                for(ABASpeakPhrase *subPhrase in phraseSample.subPhrases) {
                    if([subPhrase.listened boolValue] && ![subPhrase.text isEqualToString:@""])
                        total++;
                    
                    if([subPhrase.done boolValue] && ![subPhrase.text isEqualToString:@""])
                        total++;
                }
            } else {
                if([phraseSample.listened boolValue])
                    total ++;
                
                if([phraseSample.done boolValue])
                    total ++;
            }
        }
    }
    
    return total;
}

-(CGFloat) getTotalElementsForSection: (ABASection*) section {
    
    CGFloat total = 0.0f;
    
    ABASpeak *speak = (ABASpeak*) section;
    
    for(ABASpeakDialog *dialog in speak.content) {
        
        total += dialog.dialog.count;

        for(ABASpeakPhrase* phrase in dialog.sample) {
            if([phrase.subPhrases count] > 0) {
                for (ABASpeakPhrase* subPhrase in phrase.subPhrases) {
                    if(![subPhrase.text isEqualToString:@""]) {
                        total ++;
                    }
                }
            } else {
                total++;
            }
        }
    }
    
    
    return total * 2.0;
}

-(CGFloat) getElementsCompletedForSection: (ABASection*) section {
    ABASpeak *speak = (ABASpeak*) section;
    return [self getTotalPhraseCompletedForSpeakSection:speak];
}

-(BOOL) isSectionCompleted: (ABASection*) section {
    ABASpeak *speak = (ABASpeak*) section;
    if([self getTotalElementsForSection:section] == [self getTotalPhraseCompletedForSpeakSection:speak]) {
        return YES;
    }
    return NO;
}

-(CGFloat) getTotalPhraseCompletedDialogForSection:(ABASpeakDialog*) dialog {
    
    CGFloat total = 0.0f;
    
    for (ABASpeakPhrase *phrase in dialog.dialog) {
        if([phrase.done boolValue]) {
            total ++;
        }
    }
    
    return total;
}

-(CGFloat) getTotalPhraseCompletedSampleForSection:(ABASpeakDialog*) dialog {
    
    CGFloat total = 0.0f;
    
    for (ABASpeakPhrase *phrase in dialog.sample) {
		
        if(phrase.subPhrases.count > 0) {
            for(ABASpeakPhrase *subPhrase in phrase.subPhrases) {
				
                if([subPhrase.done boolValue] && ![subPhrase.text isEqualToString:@""])
                    total ++;
            }
        }
        else {
            if([phrase.done boolValue]) {
                total ++;
            }
        }
    }
    
    return total;
}

-(CGFloat) getTotalPhraseSampleForSection:(ABASpeakDialog*) dialog {
    
    CGFloat total = 0.0f;
    
    for (ABASpeakPhrase *phrase in dialog.sample) {
        
        if(phrase.subPhrases.count > 0) {
			
            for(ABASpeakPhrase *subPhrase in phrase.subPhrases) {
				
                if(![subPhrase.text isEqualToString:@""]) {
                    total ++;
                }
            }
        }
        else {
            total ++;
        }
    }
    
    return total;
}

-(NSArray*) getPhrasesFromSpeak:(ABASpeak *)speak {
    
    NSMutableArray *phrases = [NSMutableArray new];
    
    for(ABASpeakDialog *dialog in speak.content) {
        for (ABASpeakDialogPhrase *phrase in dialog.dialog) {
            [phrases addObject:phrase];
        }
    }
    
    return phrases;
}

-(NSArray *) getArrayPhraseCurrenDialogFromSection:(ABASpeak *)speak {
    
    NSMutableArray *mArray = [[NSMutableArray alloc] init];
    
    if([self getCurrentDialogFromSection:speak].dialog.count == [self getTotalPhraseCompletedDialogForSection:[self getCurrentDialogFromSection:speak]]) {
        return nil;
    }
    
    for(ABASpeakPhrase *phrase in [self getCurrentDialogFromSection:speak].dialog) {
        
        [mArray addObject:phrase];
    }
    
    ABASpeakDialogPhrase *translatePhrase = [[self getCurrentDialogFromSection:speak].dialog lastObject];
    
    if(translatePhrase.translation != nil && translatePhrase.translation.length > 0) {
        [mArray addObject:translatePhrase];
    }
    
    return mArray;
}

-(BOOL) isDialogCompleted: (ABASpeakDialog*) dialog {
    
    if(dialog.dialog.count == [self getTotalPhraseCompletedDialogForSection:dialog]) {
        
        if([self getTotalPhraseSampleForSection:dialog] == [self getTotalPhraseCompletedSampleForSection:dialog]) {
            return YES;
        }
        
    }
    return NO;
}

- (void)syncCompletedActions:(NSArray *)actions withSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    NSMutableArray *listenedPhrasesArray = @[].mutableCopy;
    NSMutableArray *donePhrasesArray = @[].mutableCopy;

    for (NSDictionary *action in actions)
    {
        ABASpeakPhrase *phrase = [self phraseWithID:[action objectForKey:@"Audio"]
                                            andPage:[action objectForKey:@"Page"]
                                            onSpeak:(ABASpeak *)section];
        if (phrase == nil)
        {
            NSLog(@"ACTION NOT FOUND: %@", action);
            continue;
        }

        if ([[action objectForKey:@"Action"] isEqualToString:@"LISTENED"])
        {
            if ([phrase.listened boolValue] == YES)
            {
                continue;
            }
            
            [listenedPhrasesArray addObject:phrase];
        }
        else
        {
            if ([phrase.done boolValue] == YES)
            {
                continue;
            }
            
            [donePhrasesArray addObject:phrase];
        }
    }
    if (listenedPhrasesArray.count)
    {
        [phrasesController setPhrasesListened:listenedPhrasesArray.copy forSection:section sendProgressUpdate:NO completionBlock:^(NSError *error, id object){
        }];
    }

    if (donePhrasesArray.count)
    {
        [phrasesController setPhrasesDone:donePhrasesArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object) {
                          }];
        
        
        [self checkTextPhrases:donePhrasesArray.copy forSection:section];
        
        if ([self isSectionCompleted:section] && (section.completed == nil || section.completed == [NSNumber numberWithBool:NO]))
        {
            [self setCompletedSection:section completionBlock:^(NSError *error, id object) {
            }];
        }
    }
}

- (void)checkTextPhrases:(NSArray *)donePhrases forSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    ABASpeak *abaSpeak = (ABASpeak *)section;
    NSMutableArray *subPhrasesArray = @[].mutableCopy;

    for (ABASpeakPhrase *donePhrase in donePhrases)
    {
        for (ABASpeakDialog *dialog in abaSpeak.content)
        {
            for (ABASpeakPhrase *phrase in dialog.sample)
            {
                for (ABASpeakPhrase *subPhrase in phrase.subPhrases)
                {
                    if ([subPhrase.audioFile isEqualToString:donePhrase.audioFile] &&
                        [subPhrase.page isEqualToString:donePhrase.page])
                    {
                        [subPhrasesArray addObject:subPhrase];
                    }
                }
            }
        }
    }

    if (subPhrasesArray.count)
    {
        [phrasesController setPhrasesDone:subPhrasesArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object){
                          }];
    }
}

-(ABASpeakPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onSpeak: (ABASpeak*) speak {
    
    for(ABASpeakDialog *dialog in [self getDialogDataSourceFromSpeak:speak]) {
        for(ABASpeakPhrase *phrase in dialog.dialog) {
            if([phrase.audioFile isEqualToString:audioID] && [phrase.page isEqualToString:page])
                return phrase;
        }
        
        for(ABASpeakPhrase *phrase in dialog.sample) {
            if([phrase.audioFile isEqualToString:audioID] && [phrase.page isEqualToString:page])
                return phrase;
            
            for(ABASpeakPhrase *subPhrase in phrase.subPhrases) {
                if([subPhrase.audioFile isEqualToString:audioID] && [subPhrase.page isEqualToString:page])
                    return subPhrase;
            }
        }
    }
    
    return nil;
}

-(NSArray *)getAllAudioIDsForSpeakSection: (ABASpeak *)speak
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(ABASpeakDialog *dialog in speak.content)
    {
		for(ABASpeakPhrase *phrase in dialog.dialog) {
			if(![phrase.audioFile isEqualToString:@""] && phrase.audioFile != nil)
			{
				[array addObject:phrase.audioFile];
			}
		}

        for(ABASpeakPhrase* phrase in dialog.sample)
        {
            if(![phrase.audioFile isEqualToString:@""] && phrase.audioFile != nil)
            {
                [array addObject:phrase.audioFile];
            }

            if([phrase.subPhrases count] > 0)
            {
                for (ABASpeakPhrase* subPhrase in phrase.subPhrases)
                {
                    if(![subPhrase.audioFile isEqualToString:@""] && subPhrase.audioFile != nil)
                    {
                        [array addObject:subPhrase.audioFile];
                    }
                }
            }
        }
    }
    
    return array;
}

-(CGFloat) getTotalElementsForDoInSection: (ABASection*) section {
    
    CGFloat total = 0.0f;
    
    ABASpeak *speak = (ABASpeak*) section;
    
    for(ABASpeakDialog *dialog in speak.content) {
        
        total += dialog.dialog.count;
        //total += dialog.sample.count;
        for(ABASpeakPhrase* phrase in dialog.sample) {
            if([phrase.subPhrases count] > 0) {
                for (ABASpeakPhrase* subPhrase in phrase.subPhrases) {
                    if(![subPhrase.text isEqualToString:@""]) {
                        total ++;
                    }
                }
            } else {
                total++;
            }
        }
    }

    return total;
}

- (void)sendAllPhrasesDoneForSpeakSection:(ABASpeak *)speak completionBlock:(CompletionBlock)block
{
    __weak typeof(self) weakSelf = self;

    __block NSInteger numberOfPhrasesCompleted = 0;
    __block NSInteger numberOfTotalPhrases = [self getTotalElementsForDoInSection:speak];
    NSLog(@"Total Phrase :%ld", (long)numberOfTotalPhrases);
    PhrasesController *phrasesController = [PhrasesController new];

    NSMutableArray *donePhrasesArray = @[].mutableCopy;

    for (ABASpeakDialog *dialog in speak.content)
    {
        for (ABASpeakPhrase *phrase in dialog.dialog)
        {
            [donePhrasesArray addObject:phrase];
        }

        for (ABASpeakPhrase *phrase in dialog.sample)
        {
            if ([phrase.subPhrases count] == 0)
            {
                [donePhrasesArray addObject:phrase];
            }
            else
            {
                for (ABASpeakPhrase *subPhrase in phrase.subPhrases)
                {
                    if (![subPhrase.text isEqualToString:@""])
                    {
                        [donePhrasesArray addObject:subPhrase];
                    }
                }
            }
        }
    }

    if (donePhrasesArray.count)
    {
        [phrasesController setPhrasesDone:donePhrasesArray.copy forSection:speak sendProgressUpdate:NO completionBlock:^(NSError *error, id object) {

            if (error)
            {
                block(error, nil);
                NSLog(@"Error");
            }
            else
            {
                numberOfPhrasesCompleted = donePhrasesArray.count;
                NSLog(@"Phrase Done :%ld", (long)numberOfPhrasesCompleted);
                if (numberOfPhrasesCompleted == numberOfTotalPhrases)
                {
                    [weakSelf setCompletedSection:speak completionBlock:^(NSError *error, id object) {
                        
                            block(error, speak);
                    }];
                }
                else
                {
                    block(nil, speak);
                    NSLog(@"Finished");
                }
            }
        }];
    }
    else
    {
        block(nil, speak);
        NSLog(@"Finished");
    }
}

@end
