//
//  IpAddressParser.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Unbox

enum IpAddressParserError: Swift.Error, CustomStringConvertible {
    public var description: String { return "IpAddressParserError.." }
    case invalidData
}

struct IpAddressParser {
    static func parseIpAddress(_ unboxableDict: Any) throws -> IpAddress? {
        if let unboxableDict = unboxableDict as? UnboxableDictionary {
            let user = try Unboxer.performCustomUnboxing(dictionary: unboxableDict, closure: { (unboxer) -> IpAddress? in
                return try IpAddress(unboxer: unboxer)
            })
            return user
        }
        
        throw IpAddressParserError.invalidData
    }
}
