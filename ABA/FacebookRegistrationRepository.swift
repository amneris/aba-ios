//
//  FacebookRecordRepository.swift
//  ABA
//
//  Created by MBP13 Jesus on 04/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import Moya

protocol FacebookRegistrationRepository {
    func registerWithFacebookAPICall(facebookId id: String, name: String, surname: String, email: String, gender: String, avatar: String) -> Observable<UserDataModel>
}

extension FacebookRegistrationRepository {
    
    func registerWithFacebookAPICall(facebookId id: String, name: String, surname: String, email: String, gender: String, avatar: String) -> Observable<UserDataModel> {

        return PublicIpRepository.publicIpAPICall().flatMap { ip -> Observable<UserDataModel> in

            let parameters = RegistrationFacebookParameters(email: email, name: name, surnames: surname, facebookId: id, gender: gender, languageEnvironment: LanguageController.getCurrentLanguage(), deviceId: ABASystem.sharedInstance.deviceId, sysOper: ABASystem.sharedInstance.sysOper, deviceName: ABASystem.sharedInstance.deviceName, ipMobile: ip)

            return APIProvider
                .request(.registerWithFacebook(registerParameters: parameters))
                .mapJSON()
                .map { userDictionary -> UserDataModel in
                    if let user = try NetworkUserParser.parseNetworkUser(userDictionary)
                    {
                        user.email = email
                        return user as UserDataModel
                    }
                    else if let _ = try NetworkErrorParser.parseNetworkError(userDictionary as AnyObject) {
                        throw LoginRepositoryError.networkErrorResponse
                    }

                    throw RegisterRepositoryError.invalidData
            }
        }
    }
}
