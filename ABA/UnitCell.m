//
//  UnitCell.m
//  ABA
//
//  Created by Oriol Vilaró on 30/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "UnitCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ABAUnit.h"
#import "Resources.h"
#import "Utils.h"
#import "ABARealmLevel.h"
#import "ABARealmUser.h"
#import "UserController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageHelper.h"
#import "LevelUnitViewController.h"
#import "LevelUnitController.h"

@implementation UnitCell
{
    BOOL _isUnitLastCompletedForLevel;
    ABAUnit *_abaUnit;
    ABAUnit *_currentUnit;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if((self = [super initWithCoder:aDecoder])) {
    }
    return self;
}

-(void)setupCellWithUnitCellType:(UnitCellType)unitCellType
               andStatusCellType:(StatusCellType)statusCellType
{
    _unitCellType = unitCellType;
    _statusCellType = statusCellType;
    
    switch (self.unitCellType)
    {
        case TopUnitCellType:
            
            _lowerLineView.hidden = NO;
            
            break;
        case MiddleUnitCellType:
        {
            _lowerLineView.hidden = NO;
            
            break;
        }
        case BottomUnitCellType:
            
            _lowerLineView.hidden = YES;
            break;
            
        default:
            break;
    }
    

    if([_currentUnit.level isEqualToObject:_abaUnit.level] && [_currentUnit getIndex] == [_abaUnit getIndex])
    {
        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:_abaUnit.unitImage];
        NSURL *imageUrl = [NSURL URLWithString:urlString];

        [_bgImage sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"placeholder-abierto"]];
    }
    else
    {
        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:_abaUnit.unitImageInactive];
        NSURL *imageUrl = [NSURL URLWithString:urlString];
        
        [_bgImage sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"placeholder-cerrado"]];
	}
                             
    switch (self.statusCellType)
    {
        case DoneStatusCellType:
        {
            [_upperLineView setBackgroundColor:ABA_COLOR_GREEN_COMPLETED];
            [_lowerLineView setBackgroundColor:ABA_COLOR_GREEN_COMPLETED];
            [_unitImage setImage:[UIImage imageNamed:@"circleGreen"]];
            [_unitCircleLabel setTextColor:[UIColor whiteColor]];
            
            break;
        }
        case InProgressStatusCellType: {
            ABAUnit *previousUnit = [LevelUnitController getUnitWithID:_abaUnit.getPreviousUnitId];
            if([previousUnit.completed integerValue] == 1)
            {
                [_lowerLineView setBackgroundColor:ABA_COLOR_GREEN_COMPLETED];
            }
            else
            {
                [_lowerLineView setBackgroundColor:[UIColor whiteColor]];
            }
      
            [_upperLineView setBackgroundColor:[UIColor whiteColor]];
            [_unitCircleLabel setTextColor:[UIColor blackColor]];
            [_unitImage setImage:[UIImage imageNamed:@"circleWhite"]];
            
            break;
        }
        case ToDoStatusCellType:
            
            [_upperLineView setBackgroundColor:[UIColor whiteColor]];
            [_lowerLineView setBackgroundColor:[UIColor whiteColor]];
            [_unitCircleLabel setTextColor:[UIColor blackColor]];
            [_unitImage setImage:[UIImage imageNamed:@"circleWhite"]];

            break;
            
        default:
            break;
    }
}


- (void)setupUnitWithABAUnit:(ABAUnit *)abaUnit
                 currentUnit: (ABAUnit*) currentUnit
 isLastUnitCompletedForLevel:(BOOL)isUnitLastCompletedForLevel;
{
    _abaUnit = abaUnit;
    _isUnitLastCompletedForLevel = isUnitLastCompletedForLevel;
    _currentUnit = currentUnit;
   
    // We set the unit progress
    _progress = [abaUnit.progress integerValue];

    NSString *unitIndex = [NSString stringWithFormat:@"%ld", (long)[abaUnit getIndex]];

    /**
     *  Setting labels with text and font
     */
    [_unitTitleLabel setText:abaUnit.title];
    [_unitDescLabel setText:abaUnit.desc];
    [_unitCircleLabel setText:abaUnit.idUnitString];
    
    NSString *titleString = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"unitKey", @"Unidad"), abaUnit.idUnit];
    [_unitNumberLabel setText:[titleString uppercaseString]];
    
    [_progressLabel setText:[_abaUnit getPercentage]];
    
    /**
     *  Configure the cell
     */
    UnitCellType tipus;
    StatusCellType status;
    
    // If the unit is the first, we will positionate it at bottom (inverse)
    if([unitIndex isEqualToString:@"1"] ||
       ([abaUnit getIndex] == 1 && ![UserController isUserPremium]))
    {
        tipus = BottomUnitCellType;
    }
    else
    {
        tipus = MiddleUnitCellType;
    }
    
    // Check the progress and status of the unit
    if([abaUnit.completed integerValue] == 1)
    {
        status = DoneStatusCellType;
    }
    else if(abaUnit.progress > 0)
    {
        status = InProgressStatusCellType;
    }
    else
    {
        status = ToDoStatusCellType;
    }
    
    [self setupCellWithUnitCellType:tipus andStatusCellType:status];
	
	[_continueButton setTitle:STRTRAD(@"continueUnitDashboardKey", "CONTINUAR") forState:UIControlStateNormal];
	[_continueButton setTitle:STRTRAD(@"continueUnitDashboardKey", "CONTINUAR") forState:UIControlStateHighlighted];
	[_continueButton setTitle:STRTRAD(@"continueUnitDashboardKey", "CONTINUAR") forState:UIControlStateSelected];
	
	[_continueButton addTarget:self action:@selector(continueAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)continueAction:(id)object {
	[_parentVC continueButtonLoadUnit:_abaUnit];
}

- (IBAction)unitDownloadButtonAction:(id)sender {
    [_delegate downloadUnitButtonPressed:self];
}

@end
