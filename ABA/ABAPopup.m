//
//  ABAPopup.m
//  ABA
//
//  Created by Oriol Vilaró on 25/8/15.
//  Refactored by Jesus on 24/01/2017.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAPopup.h"
#import "Resources.h"
#import "Utils.h"
#import <ZendeskSDK/ZendeskSDK.h>
#import "ResideMenuViewController.h"

@interface ABAPopup ()

@property (nonatomic, assign) kABAPopupType type;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *secondButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdButton;

- (IBAction)firstAction:(id)sender;
- (IBAction)secondAction:(id)sender;
- (IBAction)thirdAction:(id)sender;

@end

@implementation ABAPopup

+ (ABAPopup *)abaPopupWithType:(kABAPopupType)type
{
    NSString *xib;
    switch (type) {
        case kABAPopupTypeCrash: {
            xib = @"ABAPopUpTwoButtons";
            break;
        }
        case kABAPopupTypeRate: {
            xib = @"ABAPopUpThreeButtons";
            break;
        }
        default: {
            break;
        }
    }
    
    ABAPopup * popup = [[[NSBundle mainBundle] loadNibNamed:xib owner:self options:nil] objectAtIndex:0];
    popup.type = type;
    return popup;
}

#pragma mark - Public methods

- (void)showInView:(UIView *)contentView
{
    [self setupView];
    
    switch (_type) {
        case kABAPopupTypeCrash: {
            
            if (IS_IPAD) {
                [self setFrame:CGRectMake(0, 0, 350, 300)];
            }else{
                [self setFrame:CGRectMake(0, 0, 280, 220)];
            }
            
            break;
        }
        case kABAPopupTypeRate: {
            
            if (IS_IPAD) {
                [self setFrame:CGRectMake(0, 0, 350, 400)];
            }else{
                [self setFrame:CGRectMake(0, 0, 280, 300)];
            }
            
            break;
        }
        default: {
            break;
        }
    }
    
    self.center = CGPointMake(contentView.frame.size.width / 2.0, contentView.frame.size.height / 2.0);
    self.alpha = 0.0;
    [contentView addSubview:self];
    [UIView transitionWithView:self
                      duration:0.2
                       options:UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        self.alpha = 1.0;
                    } completion:nil];
}

- (void)dismiss
{
    [UIView transitionWithView:self
                      duration:0.15
                       options:UIViewAnimationOptionCurveEaseOut
                    animations:^{
                        self.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        [self removeFromSuperview];
                        self.alpha = 1.0;
                    }];
}

#pragma mark - IBAction

- (IBAction)firstAction:(id)sender {
    
    switch (_type) {
        case kABAPopupTypeCrash: {
            
            // show zendesk feedback composer
            [ZDKRequests presentRequestCreationWithViewController:[self currentNavigationController]];
            
            [self dismiss];
            
            break;
        }
        case kABAPopupTypeRate: {
            
            // rate app
            
            NSURL *appURL = [NSURL URLWithString:AppStoreURL];
            [[UIApplication sharedApplication] openURL:appURL];
            
            [self dismiss];
            
            break;
        }
        default: {
            break;
        }
    }
    
    [self dismiss];
}

- (IBAction)secondAction:(id)sender {
    
    switch (_type) {
        case kABAPopupTypeCrash: {
            
            // close popup
            
            [self dismiss];
            
            break;
        }
        case kABAPopupTypeRate: {
            
            // show zendesk feedback composer
            [ZDKRequests presentRequestCreationWithViewController:[self currentNavigationController]];
            
            [self dismiss];
            
            break;
        }
        default: {
            break;
        }
    }
}

- (IBAction)thirdAction:(id)sender {
    
    switch (_type) {
        case kABAPopupTypeCrash: {
            
            // nothing
            
            break;
        }
        case kABAPopupTypeRate: {
            
            // close popup and don't show anymore
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDontShowRateApp];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self dismiss];
            
            break;
        }
        default: {
            break;
        }
    }
}

#pragma mark - custom methods

- (void)setupView
{
    self.backgroundColor =  ABA_COLOR_LIGHTGREY3;
    
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 4.0;
    
    [_headerView setBackgroundColor:ABA_COLOR_DARKGREY3];
    [_headerLabel setTextColor:[UIColor whiteColor]];
    [_headerLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?22.0f:17.0f]];
    
    void (^setupButton)(UIButton *) = ^void(UIButton* button){
        
        button.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?20.0f:15.0f];
        [button setTitleColor:ABA_COLOR_DARKGREY3 forState:UIControlStateNormal];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.backgroundColor = ABA_COLOR_REG_BACKGROUND;
        button.titleLabel.numberOfLines = 0;
    };
    
    setupButton(_firstButton);
    setupButton(_secondButton);
    setupButton(_thirdButton);
    
    [_headerLabel setText:STRTRAD(@"rate_my_app_dialog_title_label", @"")];
    
    switch (_type) {
        case kABAPopupTypeCrash: {
            
            [_firstButton setTitle:STRTRAD(@"rate_my_app_dialog_negative_action_label", @"") forState:UIControlStateNormal];
            [_secondButton setTitle:STRTRAD(@"rateAlertLaterKey", @"") forState:UIControlStateNormal];
            
            break;
        }
        case kABAPopupTypeRate: {
            
            [_firstButton setTitle:STRTRAD(@"rate_my_app_dialog_positive_action_label", @"") forState:UIControlStateNormal];
            [_secondButton setTitle:STRTRAD(@"rate_my_app_dialog_negative_action_label", @"") forState:UIControlStateNormal];
            [_thirdButton setTitle:STRTRAD(@"rateAlertCancelKey", @"") forState:UIControlStateNormal];
            
            break;
        }
        default: {
            break;
        }
    }
}

#pragma mark - utils

- (UINavigationController *)currentNavigationController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    
    if ([topController isKindOfClass:[ResideMenuViewController class]]) {
        topController = ((ResideMenuViewController*)topController).contentViewController;
        
        return (UINavigationController*)topController;
    }
    
    return nil;
}

@end
