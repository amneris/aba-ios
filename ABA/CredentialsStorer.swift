//
//  CredentialsStorer.swift
//  ABA
//
//  Created by MBP13 Jesus on 04/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol CredentialsStorer {
    func storeLogin(_ email: String, pass: String?)
}

extension CredentialsStorer {
    
    func storeLogin(_ email: String, pass: String?)
    {
        #if SHEPHERD_DEBUG
            if let pass = pass {
                let defaults = UserDefaults.standard
                defaults.set(email, forKey: ABAShepherdLoginPlugin.kLastLoginEmailKey)
                defaults.set(pass, forKey: ABAShepherdLoginPlugin.kLastLoginPassKey)
                defaults.synchronize()

                // Saving credentials
                let env = ABAShepherdEditor.shared().environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.classForCoder()).environmentName
                ABAShepherdLoginPlugin.addLoginCredentials(email, pass: pass, environment: env!)
            }
        #endif
    }
}
