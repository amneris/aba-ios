//
//  RegisterFacebookInteractor.swift
//  ABA
//
//  Created by MBP13 Jesus on 19/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

import FacebookCore
import FacebookLogin
import FacebookShare

class FacebookInteractor {
    
    let tracker: FacebookParserTracker?
    
    init(tracker: FacebookParserTracker?) {
        self.tracker = tracker
    }

}
    
extension FacebookInteractor: FacebookInteractorInput {
    
    func loginWithFacebook() ->  Observable<FacebookUser> {
        
        let basicDataObservable = self.basicUserDataRequest().subscribeOn(MainScheduler.instance).observeOn(MainScheduler.instance)
        let profileImageObservable = self.facebookProfileImageRequest().subscribeOn(MainScheduler.instance).observeOn(MainScheduler.instance)
        
        let zip = Observable.zip(basicDataObservable, profileImageObservable, resultSelector: { (profileData, imageData) -> FacebookUser in
            guard let profileData = profileData else {
                CLSLogv("Facebook registration - not enought data to parse", getVaList([]));
//                self.tracker?.trackFacebookEmailError()
                throw SessionInteractorError.facebookEmailNotGranted
            }
            
            // Ensuring the facebook user is properly parsed
            do {
                CLSLogv("Parsing Facebook user", getVaList([]));
                
                let facebookUser = try FacebookUserParser.parseFacebookUser(profileData, imageDataResponse: imageData)
//                self.tracker?.trackFacebookEmailSuccess()
                
                CLSLogv("Facebook user Parsed", getVaList([]));
                return facebookUser
            }
            catch FacebookUserParserError.facebookEmailNotGranted {
//                self.tracker?.trackFacebookEmailError()
                throw SessionInteractorError.facebookEmailNotGranted
            }
            catch {
//                self.tracker?.trackFacebookParsingError()
                throw SessionInteractorError.facebookEmailNotGranted
            }
        })
        
        return facebookPermissionRequest().flatMap({ () -> Observable<FacebookUser> in
            return zip
        })
    }
}

extension FacebookInteractor {
    
    struct FacebookCustomProfileRequest: GraphRequestProtocol {
        
        enum FacebookCustomProfileRequestType {
            case userInfo
            case userPhoto
        }
        
        struct Response: GraphResponseProtocol {
            var result: Any?
            init(rawResponse: Any?) {
                result = rawResponse as Any
            }
        }
        
        init(type: FacebookCustomProfileRequestType) {
            self.type = type
            switch self.type {
            case .userInfo:
                graphPath = "/me"
                parameters =  ["fields": "id, email, first_name, last_name, gender"]
            case .userPhoto:
                graphPath = "/me/picture?type=large&redirect=false"
                parameters =  nil
            }
        }
        
        let type: FacebookCustomProfileRequestType
        var graphPath: String
        var parameters: [String : Any]?
        var accessToken: AccessToken? {
            get {
                return AccessToken.current
            }
        }
        let httpMethod: GraphRequestHTTPMethod = .GET
        let apiVersion: GraphAPIVersion = GraphAPIVersion.defaultVersion
    }
    
    func facebookPermissionRequest() -> Observable<()> {
        return Observable.create { observer -> Disposable in
            let loginManager = LoginManager()
            
            CLSLogv("Requesting facebook permissions", getVaList([]));
            
            // loging previous user out
            loginManager.logOut()
            
            // getting permission to access the public data
            loginManager.logIn([ReadPermission.publicProfile, ReadPermission.email], viewController: nil) { loginResult in
                
                switch loginResult {
                case .failed(_):
                    CLSLogv("Requesting facebook permissions - failed", getVaList([]));
                    observer.onError(SessionInteractorError.facebookGenericError)
                    
                case .cancelled:
                    CLSLogv("Requesting facebook permissions - cancelled", getVaList([]));
                    observer.onError(SessionInteractorError.facebookUserCanceled)
                    
                case .success(_, let declinedPermissions, _):
                    
                    // checking all the permissions are granted
                    guard declinedPermissions.isEmpty else {
                        CLSLogv("Requesting facebook permissions - email declined", getVaList([]));
                        observer.onError(SessionInteractorError.facebookEmailNotGranted)
                        return
                    }
                    
                    observer.onNext(())
                    observer.onCompleted()
                }
            }
            
            return Disposables.create()
        }
    }
    
    func basicUserDataRequest() ->  Observable<[String: AnyObject]?> {
        let profileDataRequest = FacebookCustomProfileRequest(type: .userInfo)
        return facebookGraphResquest(profileDataRequest)
    }
    
    func facebookProfileImageRequest() ->  Observable<[String: AnyObject]?> {
        let profileDataRequest =  FacebookCustomProfileRequest(type: .userPhoto)
        return facebookGraphResquest(profileDataRequest)
    }
    
    func facebookGraphResquest(_ graphRequest: FacebookCustomProfileRequest) -> Observable<[String: AnyObject]?> {
        return Observable.create { observer -> Disposable in
            
            CLSLogv("Requesting Facebook graph", getVaList([]));
            
            let connection = GraphRequestConnection()
            connection.add(graphRequest) { (graphResponse, graphResult:GraphRequestResult<FacebookCustomProfileRequest>) in
                switch graphResult {
                case .success(let graphResultResponse):
                    if let resultAsdictionary = graphResultResponse.result as? [String: AnyObject]? {
                        observer.onNext(resultAsdictionary)
                        observer.onCompleted()
                    }
                    else {
                        observer.onError(SessionInteractorError.facebookGenericError as Error)
                    }
                case .failed( let error ):
                    print("graph error \(error) for request: \(graphRequest.graphPath)")
                    if graphRequest.type == .userPhoto {
                        observer.onNext(nil)
                        observer.onCompleted()
                    } else {
                        observer.onError(SessionInteractorError.facebookGenericError as Error)
                    }
                }
            }
            connection.start()
            return Disposables.create() {
                connection.cancel()
            }
        }
    }
}
