//
//  PlansViewInput.h
//  ABA
//
//  Created by MBP13 Jesus on 14/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Products.h"

@protocol PlansViewInput <NSObject>
- (void)renderPlans:(id<PlansPageProducts>)arrayOfPlans;
- (void)renderError;

@end
