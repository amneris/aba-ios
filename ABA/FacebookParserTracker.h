//
//  FacebookParserTracker.h
//  ABA
//
//  Created by MBP13 Jesus on 14/12/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

@protocol FacebookParserTracker <NSObject>

- (void)trackFacebookParsingError;
- (void)trackFacebookEmailError;
- (void)trackFacebookEmailSuccess;

@end
