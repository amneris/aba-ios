//
//  EvaluationViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SectionBaseViewController.h"
#import "ABAEvaluation.h"

@interface EvaluationViewController : UIViewController

@property (nonatomic, strong) ABAEvaluation * section;


@end
