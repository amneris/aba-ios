//
//  GenericTrackingManager.h
//  ABA
//
//  Created by MBP13 Jesus on 28/07/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrackingDefinitions.h"

#define kSectionTimeThreshold 30 //30 seconds. Under that time no progress nor event is sent

@class ABARealmLevel;

@interface GenericTrackingManager : NSObject

- (NSString *)attributeForRecordType:(RecordType)type;
- (NSString *)attributeForPlansViewControllerOriginType:(PlansViewControllerOriginType)plantype;
- (NSString *)attributeForNotificationStatus:(BOOL)enabled;
- (NSDictionary *)dictionaryWithLevelChange:(ABARealmLevel *)fromLevel toLevel:(ABARealmLevel *)toLevel;
- (NSDictionary *)dictionaryFromSecondsOfStudyTime:(double)secondsOfStudy withInitialProgress:(NSNumber *)initialProgress withFinalProgress:(NSNumber *)finalProgress;
- (NSDictionary *)dictionaryWithTimestamp;
- (NSDictionary *)dictionaryWithABTestingRegisterFunnel;
- (NSString *)currentMixpanelFormattedDate;

- (NSString *)attributeForPlanInfoType:(PlanInfoType)type;

@end
