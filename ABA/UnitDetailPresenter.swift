//
//  UnitDetailPresenter.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

class UnitDetailPresenter: NSObject {
    
    // VIPER
    weak var view: UnitDetailViewInput?
    var detailInteractor: UnitDetailInteractorInput?
    var router: UnitDetailRouterInput?
    
    // RX
    let disposableBag = DisposeBag()
    var downloadSubscription: Disposable?
    
    internal var isNewRegister: Bool
    internal var isUserPremium: Bool
    internal var unitId: NSNumber
    
    init(unitIdToLoad: NSNumber, isPremium: Bool, isNewUser: Bool) {
        unitId = unitIdToLoad
        isUserPremium = isPremium
        isNewRegister = isNewUser
        super.init()
    }
}

extension UnitDetailPresenter: UnitDetailViewOutput {
    
    func getSectionsAndProgress(backgroundLoad: Bool) {
        
        if !backgroundLoad {
            self.view?.showLoadingProgress()
        }
        
        let dependencies = Dependencies()
        
        self.detailInteractor?.getSections(unitId: unitId)
            .flatMap { () -> Observable<Void> in
                return self.detailInteractor!.getProgress(unitId: self.unitId)
            }
            .subscribeOn(dependencies.backgroundWorkScheduler)
            .observeOn(dependencies.mainScheduler)
            .subscribe { event in
                
                if !backgroundLoad {
                    self.view?.hideLoadingProgress()
                }
                
                switch event {
                    
                case .next():
                    
                    self.view?.refreshDataSource(model: UnitViewModel(shouldShowPlansButton: self.shouldShowPlansButton(), shouldShowDownloadButton: self.isUserPremium, shouldEnableDownloadButton: self.shouldEnableDownloadButton(), teacherText: self.getTeacherText(), sections: self.getAllSectionsViewModel()))
                    
                case .error(let error):
                    
                    // Error code for progress update is not a blocker.
                    if let errorcode = error as? UnitDetailInteractorError, errorcode == UnitDetailInteractorError.getProgressError {
                        
                        self.view?.refreshDataSource(model: UnitViewModel(shouldShowPlansButton: self.shouldShowPlansButton(), shouldShowDownloadButton: self.isUserPremium, shouldEnableDownloadButton: self.shouldEnableDownloadButton(), teacherText: self.getTeacherText(), sections: self.getAllSectionsViewModel()))
                        
                    } else {
                        
                        self.view?.showAlert(withText: String.localize("getAllSectionsForUnitErrorKey", comment: "¡Ups! ... imposible cargar secciones ..."))
                    }
                    
                case .completed:
                    break
                }
                
            }.addDisposableTo(disposableBag)
    }
    
    func downloadUnit() {
        
        let dependencies = Dependencies()
        
        view?.showProgressDialog()
        downloadSubscription = self.detailInteractor?.downloadUnit(unitId: unitId)
            .subscribeOn(dependencies.mainScheduler)
            .observeOn(dependencies.mainScheduler)
            .subscribe { event in
                switch event {
                case .next(let progress):
                    self.view?.updateDownloadProgressDialog(withProgress: progress)
                    break
                case .error(let error):
                    // Fabric error logging
                    self.view?.dismissDownloadProgressDialog()
                    if let anError = error as? UnitDetailInteractorError, anError == UnitDetailInteractorError.unitDownloadStopped {
                        self.view?.showAlert(withText: String.localize("offlineDownloadStoppedKey", comment: "download stopped"))
                        CLSLogv("UnitListViewController: User stopped unit %@ download", getVaList([self.unitId]))
                    }
                    else {
                        self.view?.showAlert(withText: String.localize("offlineDownloadErrorKey", comment: "download failed"))
                        CLSLogv("UnitListViewController: Error downloading unit %@", getVaList([self.unitId]))
                        
                        let abaUnit = LevelUnitController.getUnitWithID(self.unitId)
                        MixpanelTrackingManager.shared().trackDownloadError(abaUnit)
                    }
                    break
                case .completed:
                    self.view?.dismissDownloadProgressDialog()
                    self.view?.refreshDataSource(model: UnitViewModel(shouldShowPlansButton: self.shouldShowPlansButton(), shouldShowDownloadButton: self.isUserPremium, shouldEnableDownloadButton: self.shouldEnableDownloadButton(), teacherText: self.getTeacherText(), sections: self.getAllSectionsViewModel()))
                    
                    let abaUnit = LevelUnitController.getUnitWithID(self.unitId)
                    MixpanelTrackingManager.shared().trackDownloadFinish(abaUnit)
                    
                    break
                }
        }
        
        downloadSubscription?.addDisposableTo(disposableBag)
        
        let abaUnit = LevelUnitController.getUnitWithID(self.unitId)
        MixpanelTrackingManager.shared().trackDownloadStarted(abaUnit)
    }
    
    func cancelDownload() {
        downloadSubscription?.dispose()
    }
    
    func actionForSectionPushed(section: kABASectionTypes) {
        
        let action:kABASectionActionStatus = getActionForSection(sectionType: section)
        
        switch action {
        case kABASectionActionStatus.abaSectionActionStatusGranted:
            
            let dependencies = Dependencies()
            self.view?.showLoadingProgress()
            
            // Making sure section progress is updated
            return self.detailInteractor!.getProgress(unitId: self.unitId)
                .subscribeOn(dependencies.backgroundWorkScheduler)
                .observeOn(dependencies.mainScheduler)
                .subscribe { event in
                    
                    switch event {
                        
                    case .next():
                        self.view?.hideLoadingProgress()
                        self.router?.loadSection(section: section, unitId: self.unitId, sectionDelegate: self)
                        self.isNewRegister = false
                        
                    case .error:
                        
                        self.view?.hideLoadingProgress()
                        
                        // Error code for progress update is not a blocker whenever the unit is downloaded
                        let isUnitDownloaded = !self.shouldEnableDownloadButton()
                        if isUnitDownloaded {
                            
                            self.router?.loadSection(section: section, unitId: self.unitId, sectionDelegate: self)
                            self.isNewRegister = false
                            
                        } else {
                            
                            self.view?.showAlert(withText: String.localize("getAllSectionsForUnitErrorKey", comment: "¡Ups! ... imposible cargar secciones ..."))
                        }
                        
                    case .completed:
                        break
                    }
                    
                }.addDisposableTo(disposableBag)
            
        case kABASectionActionStatus.abaSectionActionStatusOfflineWithoutData:
            view?.showAlert(withText: String.localize("getAllSectionsForUnitErrorKey", comment: "¡Ups! ... imposible cargar secciones ..."))
            
        case kABASectionActionStatus.abaSectionActionStatusLockedReasonAssessment:
            view?.showAlert(withText: String.localize("cannotEnterAssesmentKey", comment: "Todavía no puedes realizar la prueba final..."))
            
        case kABASectionActionStatus.abaSectionActionStatusLockedReasonUserNotPremium:
            router?.loadPlans()
            
        case kABASectionActionStatus.abaSectionActionStatusLockedReasonPreviusNotCompleted:
            // Not in use anymore
            break
        default: // do nothing
            break
        }
    }
    
    func ctaPushed() {
        router?.loadPlans()
    }
    
    func screenRendered() {
        checkAndLoadSectionDeepLink()
    }
}

extension UnitDetailPresenter: SectionProtocol {
    
    func showAlertCompletedUnit(_ type: kABASectionTypes) {
        
        // show completed alert
        self.view?.prepareToShowCompletionAlert(type: type)
        
        if type == .abaInterpretSectionType {
            if !UserDefaults.standard.bool(forKey: kDontShowRateApp) {
                self.view?.showRateAppPopup()
            }
        }
    }
}

// MARK: - Private methods

extension UnitDetailPresenter {
    
    func getActionForSection(sectionType: kABASectionTypes) -> kABASectionActionStatus {
        guard let abaUnit = LevelUnitController.getUnitWithID(unitId) else {
            return .abaSectionActionStatusOfflineWithoutData
        }
        
        var returnType:kABASectionActionStatus = .abaSectionActionStatusGranted
        var shouldForceEvaluationUnlock = false
        #if SHEPHERD_DEBUG
            if ABAShepherdEvaluationPlugin.forceUnlock() {
                shouldForceEvaluationUnlock = true
            }
        #endif
        
        if self.isUserPremium {
            
            if shouldForceEvaluationUnlock && sectionType == .abaAssessmentSectionType {
                
                // Shepherd
                print("Shepherd plugin - Unlocking Evaluation")
                returnType = .abaSectionActionStatusGranted
            }
            else if !abaUnit.isEvaluationSectionUnlocked && sectionType == .abaAssessmentSectionType {
                
                returnType = .abaSectionActionStatusLockedReasonAssessment
            }
        }
        else {
            // Not premium
            
            // Not first unit and not videoclass
            if !isCurrentUnitFree() && sectionType != .abaVideoClassSectionType {
                
                returnType = .abaSectionActionStatusLockedReasonUserNotPremium
            }
            else if shouldForceEvaluationUnlock && sectionType == .abaAssessmentSectionType {
                
                // Shepherd
                print("Shepherd plugin - Unlocking Evaluation")
                returnType = .abaSectionActionStatusGranted
            }
            else if !abaUnit.isEvaluationSectionUnlocked && sectionType == .abaAssessmentSectionType {
                
                returnType = .abaSectionActionStatusLockedReasonAssessment
            }
        }
        
        return returnType
    }
    
    func isCurrentUnitFree() -> Bool {
        return unitId.doubleValue.truncatingRemainder(dividingBy: 24.0) == 1
    }
    
    func checkAndLoadSectionDeepLink() {
        
        defer {
            UserDefaults.standard.removeObject(forKey: kDeepLinkSection)
            UserDefaults.standard.synchronize()
        }
        
        if let sectionDeepLink = UserDefaults.standard.object(forKey: kDeepLinkSection) as? String {
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            
            if let sectionNumber = formatter.number(from: sectionDeepLink) as NSNumber? {
                
                let section = kABASectionTypes.init(rawValue: sectionNumber.uintValue)
                self.actionForSectionPushed(section: section)
            }
        }
    }
    
    func getTeacherText() -> String {
        
        var teacherText = String.localize("unitTeacherText\(unitId)", comment: "")
        if let abaUnit = LevelUnitController.getUnitWithID(unitId) {
            
            // Teacher view shows info about the current section
            let currentSectionType = ABAUnit.getCurrentSection(for: abaUnit)
            
            if let progress = abaUnit.progress, let completed = abaUnit.completed, progress.intValue > 0 && !completed.boolValue {
                
                if currentSectionType == .abaSpeakSectionType {
                    teacherText = String.localize("unitTeacherFilmContinueKey", comment: "ahora puedes continuas con el film...")
                }
                else if currentSectionType == .abaAssessmentSectionType {
                    teacherText = String.localize("unitContinueSectionAssesment", comment: "")
                }
                else {
                    let sectionName = ABASection.localizedNameforType(currentSectionType)
                    teacherText = "\(String.localize("unitContinueSectionTeacherText", comment: "")) \(sectionName))"
                }
            }
            else if let completed = abaUnit.completed, completed.boolValue {
                
                teacherText = String.localize("unitCompletedTeacherText\(unitId)", comment: "")
            }
            else if isNewRegister {
                
                teacherText = String.localize("unitTeacherNewRegister", comment: "").replacingOccurrences(of: "%@", with: UserController.currentUser().teacherName)
            }
        }
        
        return teacherText
    }
    
    func shouldEnableDownloadButton() -> Bool {
        let abaUnit = LevelUnitController.getUnitWithID(unitId)
        return DownloadController.shouldDownloadOfflineMode(for: abaUnit)
    }
    
    func shouldShowPlansButton() -> Bool {
        if isUserPremium {
            return false
        }
        else if isCurrentUnitFree() {
            return false
        }
        
        return true
    }
    
    func statusForSection(sectionType: kABASectionTypes) -> kABASectionStyleOptions {
        
        guard let abaUnit = LevelUnitController.getUnitWithID(unitId), let section = abaUnit.getSectionForType(sectionType) else {
            return .abaSectionStyleLockEnabled
        }
        
        let nextSectionType = [ABAUnit.getCurrentSection(for: abaUnit)]
        var returnType:kABASectionStyleOptions = kABASectionStyleOptions(rawValue: 0) // Default
        
        if self.isUserPremium || isCurrentUnitFree() {
            
            if !abaUnit.isEvaluationSectionUnlocked && sectionType == .abaAssessmentSectionType {
                
                returnType = .abaSectionStyleLockEnabled
            }
            else if let completed = section.completed, let progress = section.progress, (completed.boolValue || progress.intValue == 100) {
                
                returnType = .abaSectionStyleBgColorDone
            }
            else if nextSectionType.contains(sectionType) {
                
                returnType = .abaSectionStyleBgNumberColorCurrent
            }
            
        }
        else { // no premium
            
            // Videoclass section can be either enabled, locked or current
            if sectionType == .abaVideoClassSectionType {
                
                // completed
                if let progress = abaUnit.progress, let completed = abaUnit.sectionVideoClass.completed, completed.boolValue || progress.intValue == 100 {
                    
                    returnType = .abaSectionStyleBgColorDone
                }
                else { // current
                    
                    returnType = .abaSectionStyleBgNumberColorCurrent
                }
            }
            else {
                
                returnType = .abaSectionStyleLockEnabled
            }
        }
        
        return returnType
    }
    
    func getAllSectionsViewModel() -> [SectionViewModel]  {
        
        var sections:[SectionViewModel] = []
        
        // Hardcoding the 8 sections
        (1...8).forEach { index in
            
            let unitToRender: ABAUnit = LevelUnitController.getUnitWithID(unitId)!
            let sectionType = kABASectionTypes(rawValue: UInt(index))
            let sectionStatus = statusForSection(sectionType: sectionType)
            let sectionToRender = unitToRender.getSectionForType(sectionType)
            let percentage = NSNumber(value: DataController.getProgressFor(sectionToRender))
            
            let sectionModel = SectionViewModel(sectionType: sectionType, sectionStatus: sectionStatus, sectionProgress: percentage)
            sections.append(sectionModel)
        }
        
        return sections
    }
}
