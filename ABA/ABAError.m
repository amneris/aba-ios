//
//  ABAError.m
//  ABA
//
//  Created by Oriol Vilaró on 3/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAError.h"
#import "Utils.h"

@implementation ABAError

+ (instancetype)errorWithKey:(NSString *)errorKey withCode:(NSInteger)code
{
    NSDictionary *details = @{NSLocalizedDescriptionKey : errorKey};
    ABAError *error = [ABAError errorWithDomain:@"aba" code:code userInfo:details];
    return error;
}

+ (instancetype)errorWithKey:(NSString *)errorKey
{
    return [self errorWithKey:errorKey withCode:CODE_GENERIC_ERROR];
}

+ (instancetype)errorConnection
{
    NSDictionary *details = @{NSLocalizedDescriptionKey : STRTRAD(@"errorConnection", @"Sin conexión")};
    ABAError *error = [ABAError errorWithDomain:@"aba" code:CODE_CONNECTION_ERROR userInfo:details];
    return error;
}

+(instancetype)errorMailExists
{
    NSDictionary *details = @{NSLocalizedDescriptionKey : STRTRAD(@"regErrorEmailExist", @"¡Ups! Este email ya está registrado")};
    ABAError *error = [ABAError errorWithDomain:@"aba" code:CODE_EMAIL_EXIST_ERROR userInfo:details];
    return error;
}

@end
