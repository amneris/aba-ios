//
//  ABAPlan+MixpanelTrackingManager.m
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAPlan+GenericTrackingManager.h"

@implementation ABAPlan(GenericTrackingManager)

- (NSDictionary *)trackingProperties
{
    NSMutableDictionary * dictionaryWithProperties = [[NSMutableDictionary alloc] init];
    
    if (self.days)
    {
        [dictionaryWithProperties setObject:[NSNumber numberWithInteger:self.days] forKey:@"Product"];
    }
    
    if (self.productWithDiscount.price)
    {
        [dictionaryWithProperties setObject:self.productWithDiscount.price forKey:@"Price"];
    }
    
    if (self.productWithDiscount.priceLocale)
    {
        NSString *currencyCode = [self.productWithDiscount.priceLocale objectForKey: NSLocaleCurrencyCode];
        [dictionaryWithProperties setObject:currencyCode forKey:@"Currency"];
    }
    
    return dictionaryWithProperties;
}

@end
