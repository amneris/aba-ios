//
//  ABAExercises.m
//  ABA
//
//  Created by Marc Güell Segarra on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAExercises.h"
#import "ABAExercisesGroup.h"
#import "ABAUnit.h"


@implementation ABAExercises

-(BOOL)hasContent
{
    if (self.exercisesGroups)
    {
        return YES;
    }
    return NO;
}

- (ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionExercises == %@", self];
    RLMResults *results = [ABAUnit objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}

@end
