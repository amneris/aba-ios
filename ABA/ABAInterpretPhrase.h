//
//  ABAInterpretPhrase.h
//  ABA
//
//  Created by Marc Guell on 1/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAPhrase.h"

@class ABAInterpret, ABAInterpretRole;

@interface ABAInterpretPhrase : ABAPhrase

@property ABAInterpretRole *role;
@property (readonly) ABAInterpret *interpret;

@end

RLM_ARRAY_TYPE(ABAInterpretPhrase)