//
//  ProgressActionController.m
//  ABA
//
//  Created by MBP13 Jesus on 16/03/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "ProgressActionScheduler.h"

#import "ABAProgressOperationQueue.h"
#import "DataController.h"
#import "ProgressController.h"

#define PROGRESS_ACTION_SCHEDULER_TIME 30.0

typedef enum {
    kInitialMode,
    kPausedMode,
    kErrorMode,
    kRegularMode,
    kFastMode
} ProgressActionControllerStatus;

@interface ProgressActionScheduler ()

@property(nonatomic, strong) NSTimer *progressTimer;
@property(nonatomic, strong) ProgressController *progressController;
@property(nonatomic, assign) ProgressActionControllerStatus status;

@end

@implementation ProgressActionScheduler

#pragma mark Singleton Methods

+ (id)sharedManager
{
    static ProgressActionScheduler *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _status = kInitialMode;
        _progressController = [ProgressController new];
    }
    return self;
}

#pragma mark - Public methods

- (void)startScheduling
{
    // Avoiding more than one dispatch at the same time
    if (_status != kInitialMode) {
        return;
    }
    
    [self scheduleActionsDispatch];
}

- (void)pauseScheduling
{
    _status = kPausedMode;
    [_progressTimer invalidate];
}

- (void)resumeScheduling
{
    if (_status == kPausedMode) {
        _status = kInitialMode;
        [self startScheduling];
    }
}

#pragma mark - Private methods

- (void)scheduleActionsDispatch
{
    switch (_status)
    {
        case kInitialMode:
        {
            // kInitialMode goes directly into kRegularMode and it triggers the actions dispatch
            _status = kRegularMode;
            [self doSendActions];
            break;
        }
        case kPausedMode:
        {
            // Does nothing to stop the pace
            return;
        }
        case kErrorMode:
        {
            // kErrorMode == kRegularMode
        }
        case kRegularMode:
        {
            // kRegularMode -> Schedule each 30 secs
            [_progressTimer invalidate];
            _progressTimer = [NSTimer scheduledTimerWithTimeInterval:PROGRESS_ACTION_SCHEDULER_TIME target:self selector:@selector(doSendActions) userInfo:nil repeats:NO];
            break;
        }
        case kFastMode:
        {
            // kFastMode -> No scheduling but directly executing
            [self doSendActions];
        }
            
        default:
            break;
    }
}

- (void)doSendActions
{
    [_progressController sendProgressActionsWithLimit:kProgressActionLimit withCompletionBlock:^(NSError *error, id object) {
        if (error)
        {
            NSLog(@"Error sending progress actions: %@", error.description);
            
            // After error mode goes back to kRegularMode
            [self forceStatus:kErrorMode];
        }
        else
        {
            ProgressActionControllerStatus newStatus = [self calculateStatus];
            [self forceStatus:newStatus];
        }
    }];
}

#pragma mark - Private methods (status calculation)

- (void)forceStatus:(ProgressActionControllerStatus)status
{
    // If the scheduler is paused this method should not overwrite that status.
    if (self.status != kPausedMode)
    {
        self.status = status;
    }
    
    // Restarting the cycle
    [self scheduleActionsDispatch];
}

- (ProgressActionControllerStatus)calculateStatus
{
    RLMResults *pendingActions = [DataController getPendingProgressActions];
    if (pendingActions.count > kProgressActionLimit)
    {
        return kFastMode;
    }
    else
    {
        return kRegularMode;
    }
}

@end
