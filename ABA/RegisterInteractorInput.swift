//
//  RegisterInteractorInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

enum SessionInteractorError: Swift.Error, CustomStringConvertible {
    var description: String { return "Register interactor error.." }
    // Thrown when there is an error returned from the server
    case emailAlreadyExists     // Translation from repo
    case connectionError        // Translation from repo
    case unableToDownloadUnits  // Translation from UnitDownloader
    case unableToRegister
    case unableToLogin
    case facebookGenericError
    case facebookEmailNotGranted
    case facebookUserCanceled
}

protocol RegisterInteractorInput {
    func register(name: String, email: String, password: String) -> Observable<User>
    func registerWithFacebook(_ facebookUser: FacebookUser) -> Observable<User>
}

