//
//  ABAAlertCompletedUnit.m
//  ABA
//
//  Created by Marc Güell Segarra on 19/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAAlertCompletedUnit.h"
#import "Utils.h"

@interface ABAAlertCompletedUnit ()
@property (weak, nonatomic) IBOutlet UILabel *congratsLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ABAAlertCompletedUnit

-(id) initWithFrame:(CGRect)frame andSectionType:(kABASectionTypes)type
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ABAAlertCompletedUnit" owner:self options:nil] objectAtIndex:0];
    
    if(self)
    {
        self.frame = frame;
        
        [_titleLabel setText:[NSString stringWithFormat:@"%@ %@",STRTRAD(@"youHaveFinishSectionKey", @"Has terminado la sección "),[self getSectionStringWithSectionType:type]]];
    }
    return self;
}

-(NSString *)getSectionStringWithSectionType: (kABASectionTypes)type
{
    switch(type)
    {
        case kABAFilmSectionType:
        {
            return STRTRAD(@"filmSectionKey", @"Film");
            break;
        }
        case kABASpeakSectionType:
        {
            return STRTRAD(@"speakSectionKey", @"Habla");
            break;
        }
        case kABAWriteSectionType:
        {
            return STRTRAD(@"writeSectionKey", @"Escribe");
            break;
        }
        case kABAExercisesSectionType:
        {
            return STRTRAD(@"exercisesSectionKey", @"Ejercicios");
            break;
        }
        case kABAInterpretSectionType:
        {
            return STRTRAD(@"interpretSectionKey", @"Interpreta");
            break;
        }
        case kABAAssessmentSectionType:
        {
            return STRTRAD(@"assessmentSectionKey", @"Evaluación");
            break;
        }
        case kABAVideoClassSectionType:
        {
            return STRTRAD(@"videoClassSectionKey", @"Videoclase");
            break;
        }
        case kABAVocabularySectionType:
        {
            return STRTRAD(@"vocabularySectionKey", @"Vocabulario");
            break;
        }
    }
}

@end
