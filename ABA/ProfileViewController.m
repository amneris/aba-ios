//
//  ProfileViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "ProfileViewController.h"
#import "UIViewController+ABA.h"
#import "UserController.h"
#import "AppDelegate.h"
#import "ABAAlert.h"
#import "Utils.h"
#import "Resources.h"
#import "ABARealmUser.h"
#import "LanguageController.h"
#import "SDVersion.h"
#import "PlansViewController.h"
#import "ImageHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChangePasswordViewController.h"
#import "ABAProfile.h"
#import <CargoBay/CargoBay.h>
#import "APIManager.h"
#import "ABANavigationController.h"
#import <RESideMenu/RESideMenu.h>
#import "MenuViewController.h"
#import "SubscriptionController.h"
@import SVProgressHUD;

#import "ABA-Swift.h"

// Analytics
#import "LegacyTrackingManager.h"
#import "MixpanelTrackingManager.h"

static NSString *CellIdentifierHelp = @"HelpCell";
static NSString *CellIdentifierHelpContent = @"HelpContentCell";

@interface ProfileViewController () < UICollectionViewDataSource, UICollectionViewDelegate >

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIButton *userNameButton;
@property (weak, nonatomic) IBOutlet UIButton *userEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *userLanguageButton;
@property (weak, nonatomic) IBOutlet UIButton *userPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *userShoppingRestoreButton;
@property (weak, nonatomic) IBOutlet UIButton *userNotificationsButton;
@property (weak, nonatomic) IBOutlet UILabel *yesNotificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *noNotificationLabel;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userNotificationButtonBottomConstraint;

@property (weak, nonatomic) IBOutlet UIView *registerView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UIView *endView;
@property (weak, nonatomic) IBOutlet UILabel *endLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *registerViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *noRegisterView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *info2Label;
@property (weak, nonatomic) IBOutlet UILabel *info3Label;
@property (weak, nonatomic) IBOutlet UIButton *premiumButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noRegisterViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *userTypeButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *viewSeparationUp;
@property (weak, nonatomic) IBOutlet UIView *viewSeparationDown;


@property (nonatomic, strong) NSIndexPath * selectedIndexPath;
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UITextView * ghostTextView;

- (IBAction)changePasswordButtonAction:(id)sender;
- (IBAction)premiumButtonAction:(id)sender;
- (IBAction)changeNotificationsAction:(id)sender;
- (IBAction)shoppingRestoreButtonAction:(id)sender;

@end

@implementation ProfileViewController
{
    ABARealmUser *_user;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addMenuButtonWithTitle:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"yourProfileKey", @"Tu perfil")];
    
    _user = UserController.currentUser;
    
    [self setupView];
    
    float heightFooterView;
    if ([UserController isUserPremium])
    {
        _registerView.hidden = NO;
        _noRegisterView.hidden = YES;
        [self setupRegisterView];
        _scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _registerViewHeightConstraint.constant, 0);
        heightFooterView = _registerViewHeightConstraint.constant;
    }
    else
    {
        _registerView.hidden = YES;
        _noRegisterView.hidden = NO;
        [self setupNoRegisterView];
        _scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _noRegisterViewHeightConstraint.constant, 0);
        heightFooterView = _noRegisterViewHeightConstraint.constant;
    }
    
    [_scrollView setScrollEnabled:YES];
    _userNotificationButtonBottomConstraint.constant = 20.0f + heightFooterView;
    
    _registerView.alpha = 0.9f;
    _noRegisterView.alpha = 0.9f;
    
    [self createDataSource];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[LegacyTrackingManager sharedManager] trackPageView:@"profile"];
    CLS_LOG(@"ProfileViewController: Entering profile");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadCollection) name:kUpdateRotationFromMenu object:nil];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self reloadCollection];
}

#pragma mark - View setup

- (void)createDataSource
{
    _dataSource = [[NSMutableArray alloc] init];
    
    ABAProfile *newProfile1 = [ABAProfile new];
    newProfile1.title = STRTRAD(@"profileTemsTitleKey", @"Condiciones generales");
    newProfile1.desc = STRTRAD(@"profileTemsDescriptionKey", @"Descripción de las Condiciones generales");
    [_dataSource addObject:newProfile1];
    
    ABAProfile *newProfile2 = [ABAProfile new];
    newProfile2.title = STRTRAD(@"profilePolicyTitleKey", @"Política de Privacidad");
    newProfile2.desc = STRTRAD(@"profilePolicyDescriptionKey", @"Descripción de la Política de Privacidad");
    [_dataSource addObject:newProfile2];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
					   [_collectionView reloadData];
    });
}

- (void)setupRegisterView
{
    _endView.backgroundColor = ABA_COLOR_DARKGREY3;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:_user.expirationDate];
    
    NSString *endDate = [NSString stringWithFormat:@"%@: %@",STRTRAD(@"profileRegister2Key", @"suscripción válida hasta"), dateString];
    _endLabel.text = endDate;
    [_endLabel setFont:[UIFont fontWithName:ABA_MUSEO_SLAB_500 size:IS_IPAD?18:14]];
    [_endLabel setTextColor:ABA_COLOR_GREY_SUBS2];
}

- (void)setupNoRegisterView
{
    _noRegisterView.backgroundColor = ABA_COLOR_DARKGREY;
    
    _info2Label.text =STRTRAD(@"titlePremiumProfileKey", @"Pásate al plan premium");;
    _info2Label.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16];
    _info2Label.textColor = [UIColor whiteColor];
    
    _premiumButton.backgroundColor = ABA_COLOR_BLUE;
    _premiumButton.layer.cornerRadius = 3.0;
    _premiumButton.layer.masksToBounds = YES;
    _premiumButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?19:15];
    [_premiumButton setTitle:STRTRAD(@"buttonPremiumKey", @"Ver precios") forState:UIControlStateDisabled];
    [_premiumButton setTitle:STRTRAD(@"buttonPremiumKey", @"Ver precios") forState:UIControlStateNormal];
    [_premiumButton setTitle:STRTRAD(@"buttonPremiumKey", @"Ver precios") forState:UIControlStateSelected];
}

- (void)setupView
{
    self.view.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    
    if ([UserController isUserImage])
    {
        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:UserController.currentUser.urlImage];
        NSURL *imageUrl = [NSURL URLWithString:urlString];
        
        _userImage.layer.cornerRadius = _userImage.frame.size.width/2;
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        
        if (imageUrl)
        {
            [_userImage sd_setImageWithURL:imageUrl];
        }
        else
        {
            [_userImage setImage:[UIImage imageNamed:@"blankAvatar"]];
        }
    }
    
    NSString *name;
    if (_user.surnames == nil) {
        name = _user.name;
    }
    else {
        name = [NSString stringWithFormat:@"%@ %@", _user.name, _user.surnames];
    }
    
    // User name
    _userNameButton.backgroundColor = [UIColor clearColor];
    _userNameButton.layer.cornerRadius = 3.0;
    _userNameButton.layer.masksToBounds = YES;
    [_userNameButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
    [_userNameButton setTitleColor:ABA_COLOR_REG_TEXTFIELD forState:UIControlStateNormal];
    [_userNameButton.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
    [_userNameButton.layer setBorderWidth:1];
    [_userNameButton setTitle:name forState:UIControlStateNormal];
    
    _userEmailButton.backgroundColor = [UIColor clearColor];
    _userEmailButton.layer.cornerRadius = 3.0;
    _userEmailButton.layer.masksToBounds = YES;
    [_userEmailButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
    [_userEmailButton setTitleColor:ABA_COLOR_REG_TEXTFIELD forState:UIControlStateNormal];
    [_userEmailButton setTitle:_user.email forState:UIControlStateNormal];
    [_userEmailButton.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
    [_userEmailButton.layer setBorderWidth:1];
    
    NSString *language;
    if ([[LanguageController getCurrentLanguage] isEqualToString:@"es"])
    {
        language = STRTRAD(@"languageSpainKey", @"español");
    }
    else if ([[LanguageController getCurrentLanguage] isEqualToString:@"en"])
    {
        language = STRTRAD(@"languageEnglishKey", @"inglés");
    }
    else if ([[LanguageController getCurrentLanguage] isEqualToString:@"it"])
    {
        language = STRTRAD(@"languageItalianKey", @"italiano");
    }
    else if ([[LanguageController getCurrentLanguage] isEqualToString:@"de"])
    {
        language = STRTRAD(@"languageDeutchKey", @"alemán");
    }
    else if ([[LanguageController getCurrentLanguage] isEqualToString:@"fr"])
    {
        language = STRTRAD(@"languageFrenchKey", @"francés");
    }
    else if ([[LanguageController getCurrentLanguage] isEqualToString:@"ru"])
    {
        language = STRTRAD(@"languageRussianKey", @"ruso");
    }
    else if ([[LanguageController getCurrentLanguage] isEqualToString:@"pt"])
    {
        language = STRTRAD(@"languagePortugueseKey", @"portugués");
    }
    else
    {
        language = @"unknown";
    }
    
    _userLanguageButton.backgroundColor = [UIColor clearColor];
    _userLanguageButton.layer.cornerRadius = 3.0;
    _userLanguageButton.layer.masksToBounds = YES;
    _userLanguageButton.layer.borderColor = ABA_COLOR_REG_BORDER.CGColor;
    _userLanguageButton.layer.borderWidth = 1;
    _userLanguageButton.backgroundColor = [UIColor clearColor];
    [_userLanguageButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
    [_userLanguageButton setTitleColor:ABA_COLOR_REG_TEXTFIELD forState:UIControlStateNormal];
    [_userLanguageButton setTitle:language forState:UIControlStateNormal];
    
    _userPasswordButton.backgroundColor = ABA_COLOR_BLUE;
    _userPasswordButton.layer.cornerRadius = 3.0;
    _userPasswordButton.layer.masksToBounds = YES;
    _userPasswordButton.layer.borderColor = ABA_COLOR_REG_BORDER.CGColor;
    _userPasswordButton.layer.borderWidth = 1;
    
    NSString *titleButton = [STRTRAD(@"profileChangePasswordKey", @"cambiar contraseña") uppercaseString];
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:titleButton];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?18:14]
                      range:[titleButton rangeOfString:titleButton]];
    [mutAttStr addAttribute:NSKernAttributeName
                      value:[NSNumber numberWithFloat:2.0]
                      range:NSMakeRange(0, [titleButton length])];
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:[titleButton rangeOfString:titleButton]];
    [_userPasswordButton setAttributedTitle:mutAttStr.copy forState:UIControlStateNormal];
    
    _userNotificationsButton.backgroundColor = [UIColor whiteColor];
    _userNotificationsButton.layer.cornerRadius = 3.0;
    _userNotificationsButton.layer.masksToBounds = YES;
    _userNotificationsButton.layer.borderColor = ABA_COLOR_REG_BORDER.CGColor;
    _userNotificationsButton.layer.borderWidth = 1;
    [_userNotificationsButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
    [_userNotificationsButton setTitleColor:ABA_COLOR_REG_TEXTFIELD forState:UIControlStateNormal];
    [_userNotificationsButton setTitle:STRTRAD(@"profileNotificationsKey", @"recibir notificaciones") forState:UIControlStateNormal];
    
    [_logoutButton setTitle:STRTRAD(@"logoutKey", @"desconectar") forState:UIControlStateNormal];
    _logoutButton.layer.cornerRadius = 3.0;
    _logoutButton.layer.masksToBounds = YES;
    _logoutButton.layer.borderColor = ABA_COLOR_REG_BORDER.CGColor;
    _logoutButton.layer.borderWidth = 1;
    _logoutButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16];
    
    // APNS configuration switch
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *apnsStatus = [defaults objectForKey:kApnsConfigurationStateKey];
    if (!apnsStatus || [apnsStatus isEqualToString:kApnsEnabled])
    {
        [self setupNotificationsActive];
    }
    else
    {
        [self setupNotificationsDisabled];
    }
    
    NSString *userType;
    if ([UserController isUserPremium])
    {
        userType = [NSString stringWithFormat:@"%@ : %@", STRTRAD(@"typeStudentkey", @"Tipo de alumno"), @"Premium"];
    }
    else
    {
        userType = [NSString stringWithFormat:@"%@ : %@", STRTRAD(@"typeStudentkey", @"Tipo de alumno"), @"Free"];
    }
    _userTypeButton.backgroundColor = [UIColor clearColor];
    _userTypeButton.layer.cornerRadius = 3.0;
    _userTypeButton.layer.masksToBounds = YES;
    _userTypeButton.layer.borderColor = ABA_COLOR_REG_BORDER.CGColor;
    _userTypeButton.layer.borderWidth = 1;
    _userTypeButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16];
    [_userTypeButton setTitleColor:ABA_COLOR_REG_TEXTFIELD forState:UIControlStateNormal];
    [_userTypeButton setTitle:userType forState:UIControlStateNormal];
    
    _userShoppingRestoreButton.backgroundColor = ABA_COLOR_BLUE;
    _userShoppingRestoreButton.layer.cornerRadius = 3.0;
    _userShoppingRestoreButton.layer.masksToBounds = YES;
    _userShoppingRestoreButton.layer.borderColor = ABA_COLOR_REG_BORDER.CGColor;
    _userShoppingRestoreButton.layer.borderWidth = 1;
    
    //TODO phraseApp
    NSString *titleButtonShopping = [STRTRAD(@"profileShoppingRestoreKey", @"Restaurar compras") uppercaseString];
    NSMutableAttributedString *mutAttStrShopping = [[NSMutableAttributedString alloc] initWithString:titleButtonShopping];
    [mutAttStrShopping addAttribute:NSFontAttributeName
                              value:[UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?18:14]
                              range:[titleButtonShopping rangeOfString:titleButtonShopping]];
    [mutAttStrShopping addAttribute:NSKernAttributeName
                              value:[NSNumber numberWithFloat:2.0]
                              range:NSMakeRange(0, [titleButtonShopping length])];
    [mutAttStrShopping addAttribute:NSForegroundColorAttributeName
                              value:[UIColor whiteColor]
                              range:[titleButtonShopping rangeOfString:titleButtonShopping]];
    [_userShoppingRestoreButton setAttributedTitle:mutAttStrShopping.copy forState:UIControlStateNormal];
    
    _viewSeparationUp.backgroundColor = ABA_COLOR_REG_BORDER;
    _viewSeparationDown.backgroundColor = ABA_COLOR_REG_BORDER;
}

- (void)setupNotificationsActive
{
    _yesNotificationLabel.text = STRTRAD(@"yesProfileNotificationKey", @"SI");
    _yesNotificationLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?18:14];
    _yesNotificationLabel.backgroundColor = ABA_COLOR_BLUE;
    _yesNotificationLabel.textColor = [UIColor whiteColor];
    _yesNotificationLabel.layer.cornerRadius = 3.0f;
    
    _noNotificationLabel.text = STRTRAD(@"noProfileNotificationKey", @"NO");
    _noNotificationLabel.backgroundColor = [UIColor clearColor];
    _noNotificationLabel.textColor = ABA_COLOR_DARKGREY;
    _noNotificationLabel.layer.cornerRadius = 3.0f;
}

- (void)setupNotificationsDisabled
{
    _yesNotificationLabel.text = STRTRAD(@"yesProfileNotificationKey", @"SI");
    _yesNotificationLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?18:14];
    _yesNotificationLabel.backgroundColor = [UIColor clearColor];
    _yesNotificationLabel.textColor = ABA_COLOR_DARKGREY;
    _yesNotificationLabel.layer.cornerRadius = 3.0f;
    
    _noNotificationLabel.text = STRTRAD(@"noProfileNotificationKey", @"NO");
    _noNotificationLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?18:14];
    _noNotificationLabel.backgroundColor = ABA_COLOR_BLUE;
    _noNotificationLabel.textColor = [UIColor whiteColor];
    _noNotificationLabel.layer.cornerRadius = 3.0f;
}

#pragma mark - IBActions

- (IBAction)changePasswordButtonAction:(id)sender
{
    ChangePasswordViewController *changePasswordViewController = [[UIStoryboard storyboardWithName:@"Profile" bundle:nil] instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    [self.navigationController pushViewController:changePasswordViewController animated:YES];
}

- (IBAction)premiumButtonAction:(id)sender
{
    PlansViewController *plansViewController = [PlansConfigurator createPlansViperStackWithType:kPlansViewControllerOriginBanner shouldShowBackButton:YES];
    [self.navigationController pushViewController:plansViewController animated:YES];
}

- (IBAction)changeNotificationsAction:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *notifications = [defaults objectForKey:kApnsConfigurationStateKey];
    
    // If APNS is currently enabled
    if ([notifications isEqualToString:kApnsEnabled] || !notifications)
    {
        // Disabling APNS
        [defaults setObject:kApnsDisabled forKey:kApnsConfigurationStateKey];
        [defaults synchronize];
        
        [self setupNotificationsDisabled];
        
        // Unregistering the app for notifications
        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) unregisterForRemoteNotification];
        
        [[MixpanelTrackingManager sharedManager] trackNotificationConfigurationChange];
    }
    else
    {
        // If disabled, then enabling APNS
        [defaults setObject:kApnsEnabled forKey:kApnsConfigurationStateKey];
        [defaults synchronize];
        
        [self setupNotificationsActive];
        
        // Registering the app for notifications
        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) registerForRemoteNotification];
        
        [[MixpanelTrackingManager sharedManager] trackNotificationConfigurationChange];
    }
}

- (IBAction)shoppingRestoreButtonAction:(id)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscriptionCompletedSuccess) name:kSubscriptionProcessCompletedOK object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscriptionCompletionFailed) name:kSubscriptionProcessCompletedFailed object:nil];
    [SubscriptionController restorePurchases];
}

- (IBAction)logoutAction:(id)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [UserController logoutWithBlock:^(NSError *error) {
         [SVProgressHUD dismiss];
         if (!error)
         {
             AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
             [appDelegate.presenter loadStartStoryboard];
         }
         else
         {
             ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"errorLogout", @"Error en hacer logout")];
             [abaAlert show];
         }
    }];
    
    AppDelegate * appDelegatec = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegatec resetAPNSConfiguration];
}

- (void)displayAlertViewWithMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UICollectionView Methods

- (void)initialRotateIcon:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [_collectionView cellForItemAtIndexPath:indexPath];
    UIImageView * desplegableImage = (UIImageView*)[cell viewWithTag:3];
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         desplegableImage.transform = CGAffineTransformMakeRotation(-3.141516);
                     }
                     completion:NULL];
}

- (void)finalRotateIcon:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [_collectionView cellForItemAtIndexPath:indexPath];
    UIImageView * desplegableImage = (UIImageView*)[cell viewWithTag:3];
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         desplegableImage.transform = CGAffineTransformMakeRotation(0);
                     }
                     completion:NULL];
}

- (void)updateHeightCollectionView
{
    if (_selectedIndexPath == nil)
    {
        _collectionViewHeightConstraint.constant = 120;
    }
    else
    {
        ABAProfile *helpSelected = [_dataSource objectAtIndex:_selectedIndexPath.row];
        
        CGFloat ghostlabelwidth = _collectionView.frame.size.width - 30.0; // 30 es el valor de las constraints left&right del label
        _ghostTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, ghostlabelwidth, 300)];
        _ghostTextView.textAlignment = NSTextAlignmentLeft;
        
        _ghostTextView.text = helpSelected.desc;
        _ghostTextView.textColor = ABA_COLOR_REG_TEXTFIELD;
        [_ghostTextView setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
        [_ghostTextView sizeToFit];
        
        //        [_scrollView setScrollEnabled:YES];
        _collectionViewHeightConstraint.constant = 120 + _ghostTextView.frame.size.height + 30;
    }
}


#pragma mark - UICollectionViewDelegate Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    __weak typeof(self) weakself = self;

    [collectionView performBatchUpdates:^{
        // append your data model to return a larger size for
        // cell at this index path
        if (weakself.selectedIndexPath && weakself.selectedIndexPath.row == indexPath.row)
        {
            [weakself finalRotateIcon:weakself.selectedIndexPath];
            weakself.selectedIndexPath = nil;
        }
        else
        {
            weakself.selectedIndexPath = indexPath;
            [weakself initialRotateIcon:weakself.selectedIndexPath];
        }

        [weakself updateHeightCollectionView];

    }
        completion:^(BOOL finished) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakself.collectionView reloadData];
            });

            NSInteger heighPosition;
            if (indexPath.row == 0)
            {
                heighPosition = 434;
            }
            else
            {
                heighPosition = 494;
            }
            [weakself.scrollView scrollRectToVisible:CGRectMake(0, heighPosition, 10, 10) animated:YES];
        }];
}

#pragma mark - UICollectionViewDataSource Methods

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == _collectionView){
        
        UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifierHelp forIndexPath:indexPath];
        
        UILabel * cellLabel = (UILabel*)[cell viewWithTag:1];
        UICollectionView * categoriesCollection = (UICollectionView*)[cell viewWithTag:2];
        UIImageView * desplegableImage = (UIImageView*)[cell viewWithTag:3];
        UIView * separationView = (UIView*)[cell viewWithTag:4];
        
        ABAProfile *helpSelected = [_dataSource objectAtIndex:_selectedIndexPath.row];
        ABAProfile *help = [_dataSource objectAtIndex:indexPath.row];
        
        cellLabel.text = help.title;
        
        if (help == helpSelected && _selectedIndexPath!=nil) {
            [cellLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
            desplegableImage.transform = CGAffineTransformMakeRotation(-3.141516);
        }
        else {
            [cellLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
            desplegableImage.transform = CGAffineTransformMakeRotation(0);
        }
        
        [cellLabel setTextColor:ABA_COLOR_REG_TEXTFIELD];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        desplegableImage.hidden = NO;
        
        separationView.backgroundColor = ABA_COLOR_REG_BACKGROUND;
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [categoriesCollection reloadData];
                       });
        
        return cell;
        
    }
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifierHelpContent forIndexPath:indexPath];
    
    UITextView * cellTextView = (UITextView*)[cell viewWithTag:12];
    cellTextView.textColor = ABA_COLOR_REG_TEXTFIELD;
    [cellTextView setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
    cellTextView.textAlignment = NSTextAlignmentLeft;
    
    cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    
    ABAProfile *helpContent = [_dataSource objectAtIndex:_selectedIndexPath.row];
    
    cellTextView.text = helpContent.desc;
    [cellTextView sizeToFit];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _collectionView){
        return [_dataSource count];
    }
    
    return 1;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat ghostlabelwidth = _collectionView.frame.size.width - 30.0; // 30 es el valor de las constraints left&right del label
    CGFloat labelHeight = 0;
    if (collectionView == _collectionView){
        ABAProfile *help = [_dataSource objectAtIndex:indexPath.row];
        CGFloat height = 60;
        if (_selectedIndexPath && _selectedIndexPath.row == indexPath.row){
            if (_selectedIndexPath && _selectedIndexPath.row == indexPath.row){
                if (!_ghostTextView){
                    _ghostTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, ghostlabelwidth, 9999)];
                    _ghostTextView.textAlignment = NSTextAlignmentLeft;
                }
                _ghostTextView.text = help.desc;
                _ghostTextView.textColor = ABA_COLOR_REG_TEXTFIELD;
                [_ghostTextView setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
                [_ghostTextView sizeToFit];
                labelHeight = _ghostTextView.frame.size.height + 30;
            }
            height+=labelHeight;
            return CGSizeMake(collectionView.frame.size.width, height);
        }
        return CGSizeMake(collectionView.frame.size.width, height);
    }
    
    //    if (!_ghostTextView){
    _ghostTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, ghostlabelwidth, 9999)];
    _ghostTextView.textAlignment = NSTextAlignmentLeft;
    //    }
    
    NSIndexPath *indexPath2 = [_collectionView indexPathForCell:(UICollectionViewCell*)collectionView.superview.superview];
    
    ABAProfile *helpSelected = [_dataSource objectAtIndex:indexPath2.row];
    
    _ghostTextView.text = helpSelected.desc;
    _ghostTextView.textColor = ABA_COLOR_REG_TEXTFIELD;
    [_ghostTextView setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16]];
    [_ghostTextView sizeToFit];
//    NSLog(@"HEIGHT : %f", _ghostTextView.frame.size.height + 30);
//    NSLog(@"WIDTH : %f", ghostlabelwidth);
//    NSLog(@"HEIGHT COLLECTION : %f", collectionView.frame.size.height);
    return CGSizeMake(collectionView.frame.size.width, _ghostTextView.frame.size.height + 30);
}

- (void)reloadCollection
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

#pragma mark - SubscriptionDelegate Methods

- (void)subscriptionCompletedSuccess
{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedOK object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedFailed object:nil];
    
    ABANavigationController * menuNavigation = (ABANavigationController*)self.sideMenuViewController.leftMenuViewController;
    MenuViewController * menuVC = menuNavigation.viewControllers[0];
    [menuVC setupView];
    
    UIStoryboard * mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    ABANavigationController *nc = [[ABANavigationController alloc] initWithRootViewController:vc];
    [self.sideMenuViewController setContentViewController:nc animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

- (void)subscriptionCompletionFailed
{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedOK object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSubscriptionProcessCompletedFailed object:nil];
}

@end
