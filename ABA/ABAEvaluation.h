//
//  ABAEvaluation.h
//  ABA
//
//  Created by Ivan Ferrera on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASection+Methods.h"
#import "ABAEvaluationQuestion.h"

@class ABAUnit;

@interface ABAEvaluation : ABASection

@property (readonly) ABAUnit *unit;
@property RLMArray<ABAEvaluationQuestion *><ABAEvaluationQuestion> *content;

-(BOOL) hasContent;

@end
