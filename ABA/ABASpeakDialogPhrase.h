//
//  ABASpeakDialogPhrase.h
//  
//
//  Created by Jordi Mele on 12/1/15.
//
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAPhrase.h"


@interface ABASpeakDialogPhrase : ABAPhrase

@property NSString *role;

@end

RLM_ARRAY_TYPE(ABASpeakDialogPhrase)