//
//  ABAControlView.h
//  ABA
//
//  Created by Marc Güell Segarra on 4/5/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"
#import "Resources.h"
#import "ABAActionButtonView.h"
#import "AudioController.h"

@protocol ABAControlViewDelegate <NSObject>

@optional
-(void)controlViewRedoButtonTapped;
-(void)controlViewCompareButtonTapped;

@end

@interface ABAControlView : UIView <ABAActionButtonViewDelegate>

@property (nonatomic, weak) id <ABAControlViewDelegate> delegate;
@property (strong, nonatomic) ABAActionButtonView *actionButton;
@property (strong, nonatomic) AudioController *audioController;

-(id) initWithSectionType: (kABASectionTypes)type;
-(void)showAuxiliaryButtons:(BOOL)show;
-(void)showRecordingTime;
-(void)hideRecordingTime;

@end
