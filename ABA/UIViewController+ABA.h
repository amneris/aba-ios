//
//  UIViewController+ABA.h
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ABA)

- (void)addMenuButton;

- (void)addBackButtonNavBar;

- (void)addToolbarWithBackButtonAndTitle:(NSString *)title;

-(void)addToolbarWithTitle: (NSString *)title;

-(void)addMenuButtonWithTitle: (NSString *)title
                  andSubtitle: (NSString *)subtitle;

-(void)addBackButtonWithTitle: (NSString *)title
                  andSubtitle: (NSString *)subtitle;

-(void)addBackButtonWithTitle: (NSString *)title
                  andSubtitle: (NSString *)subtitle
                   withTarget: (id)target
                   withAction: (SEL)action;

-(void)addToolbarAndMenuButtonWithTitle: (NSString *)title
                            andSubtitle: (NSString *)subtitle;

-(void)addMenuButtonWithTitleBlue: (NSString *)title;

-(void)addMenuButtonWithTitleWhite: (NSString *)title;

-(void)addBackButtonWithTitleBlue: (NSString *)title;

-(void)addTitleNavbar: (NSString *)title
          andSubtitle: (NSString *)subtitle;

-(void)addTitleNavbarSection: (NSString *)title
          andSubtitle: (NSString *)subtitle;

-(void)addTitleNavbar:(NSString *)title andSubtitle:(NSString *)subtitle andProgress:(NSString *)progress;

@end
