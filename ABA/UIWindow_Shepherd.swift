//
//  UIWindow_Shepherd.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 10/10/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit

extension UIWindow {
    override open var canBecomeFirstResponder : Bool {
        return true
    }
    
    override open func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        #if SHEPHERD_DEBUG
        if motion == .motionShake {
            let sheperdController = ABAShepherdController()
            let navController = UINavigationController.init(rootViewController: sheperdController)
            self.rootViewController?.present(navController, animated: true, completion: nil)
        }
        #endif
    }
}
