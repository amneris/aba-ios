//
//  UserController.h
//  ABA
//
//  Created by Oriol Vilaró on 7/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@class ABARealmLevel, ABAUnit, ABARealmUser;

@interface UserController : NSObject

/**
 *  return user logged status
 *
 *  @return YES/NO user is logged
 */
+ (BOOL)isUserLogged;

/**
 *  return current user premium status
 *
 *  @return YES/NO user is premium
 */
+ (BOOL)isUserPremium;

/**
 *  return current user image status
 *
 *  @return YES/NO user is have image
 */
+ (BOOL)isUserImage;

/**
 *  Return the language for the current user
 *
 *  @return string user language
 */
+ (NSString *)getUserLanguage;
    
/**
 *  Logout for the current user
 */
+ (void)logout;

/**
 *  Logout for the current user with error block
 */
+ (void)logoutWithBlock:(ErrorBlock)blockOrNil;

/**
 *  Return the current level for the current user
 *
 *  @return Current ABARealmLevel object
 */
+ (ABARealmLevel *)returnCurrentLevel;

/**
 *  Return the current unit for the current user
 *
 *  @return Current ABAUnit object
 */
+ (ABAUnit *)returnCurrentUnit;

/**
 *  Set the current level & unit for the current user
 *
 *  @param level ABARealmLevel object
 *  @param unit  ABAUnit object
 *  @param completionBlock error and (id) ABARealmUser object updated
 */
+ (void)setCurrentLevel:(ABARealmLevel *)level
       completionBlock:(CompletionBlock)completionBlock;


/**
 *  register user facebook
 *
 *  @param facebookID  user facebookID
 *  @param email       user email
 *  @param name        user name
 *  @param surnames    user surnames
 *  @param gender      user gender
 *  @param avatar      user avatar
 *
 *  @return completionBlock
 */
+ (void)registerUserWithFacebookID:(NSString *)facebookID
                             email:(NSString *)email
                              name:(NSString *)name
                          surnames:(NSString *)surnames
                            gender:(NSString *)gender
                            avatar:(NSString *)avatar
                   completionBlock:(CompletionBlockFacebook)completionBlockFacebook;

/**
 *  register user
 *
 *  @param email     user email
 *  @param password  user password
 *  @param name      user name
 *
 *  @return completionBlock
 */
+ (void)registerUserWithEmail:(NSString *)email
                 withPassword:(NSString *)password withName:(NSString *)name completionBlock:(CompletionBlock)completionBlock;
/*
 *  check user password
 *
 *  @param email     user email
 *  @param password  user password
 *
 *  @return completionBlock
 */
+ (void)checkUserPasswordWithEmail:(NSString *)email
                      withPassword:(NSString *)password
                   completionBlock:(BoolCompletionBlock)completionBlock;

/*
 *  login user
 *
 *  @param email     user email
 *  @param password  user password
 *
 *  @return completionBlock
 */
+ (void)loginUserWithEmail:(NSString *)email
              withPassword:(NSString *)password completionBlock:(CompletionBlock)completionBlock;

/*
 *  change password user
 *
 *  @param token     user token
 *  @param password  user new password
 *
 *  @return completionBlock
 */
+ (void)updatePasswordUserWithToken:(NSString *)token
                       withPassword:(NSString *)password
                    completionBlock:(CompletionBlock)completionBlock;

/**
 *  recover password
 *
 *  @param email     user email
 *
 *  @return completionBlock
 */
+ (void)recoverPassword:(NSString *)email
        completionBlock:(CompletionBlock)completionBlock;

/**
 *  Sets the userType to Premium -> "2"
 */
+ (void)updateUserToPremium;

/**
 Saves the current user model to coredata
 */
+ (void)saveCurrentUser:(ABARealmUser *)user;

/**
 *  Saves the current user model to coredata
 *
 *  @param user            user
 *  @param completionBlock completion block
 */
+ (void)saveCurrentUser:(ABARealmUser *)user completionBlock:(CompletionBlock)completionBlock;

+ (ABAUnit *)getCurrentUnitForUser:(ABARealmUser *)user;

/**
 Checks if a user is premium. All checks must be done by this method
 
 @param user the user to check
 */
+ (BOOL)isPremium:(ABARealmUser *)user;

/**
 Will return true if it's the first time that user has completed an unit 
 within the app.
 */
+(BOOL)isTheFirstCompletedUnit;


+ (ABARealmUser *)currentUser;

+ (void)loginWithFacebookCompletionBlock:(CompletionBlockFacebook)completionBlock;


@end
