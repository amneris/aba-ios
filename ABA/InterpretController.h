//
//  InterpretController.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"
#import "Blocks.h"

@class ABAInterpret, ABAInterpretRole, ABAInterpretPhrase;

@interface InterpretController : SectionController

/**
 *  Gets the current phrase for role
 *
 *  @param interpret       ABAInterpret object
 *  @param role            role1/role2
 *  @return                ABAInterpretPhrase object
 */
-(ABAInterpretPhrase *) getCurrentPhraseForRole: (ABAInterpretRole *)role
                            forInterpretSection: (ABAInterpret *)interpret;

/**
 *  Set ABAInterpretPhrase done
 *
 *  @param phrase          ABAInterpretPhrase
 *  @param interpret       ABAInterpret

 *  @param completionBlock id, error
 */
-(void)setPhraseDone: (ABAInterpretPhrase *)phrase
 forInterpretSection: (ABAInterpret *)interpret
     withCurrentRole: (ABAInterpretRole *)role
     completionBlock: (CompletionBlock)completionBlock;

/**
 *  Set role done for ABAInterpret section
 *
 *  @param roles           role array to be set done
 *  @param interpret       interpret object
 *  @param completionBlock object, error
 */
- (void)setRolesDone:(NSArray *)roles
 forInterpretSection:(ABAInterpret *)interpret
     completionBlock:(CompletionBlock)completionBlock;

/**
 *  Erase the progress for the current role
 *
 *  @param role            role
 *  @param interpret       interpret object
 *  @param completionBlock object, error
 */
-(void)eraseProgressForRole: (ABAInterpretRole *)role
        forInterpretSection: (ABAInterpret *)interpret
            completionBlock: (CompletionBlock)completionBlock;

-(NSDictionary*) getPhrasesDictionaryForSection: (ABAInterpret*) section;

-(NSArray *)getAllAudioIDsForInterpretSection: (ABAInterpret *)interpret;

-(void) checkIfRolesAreDoneForSection: (ABAInterpret*) interpret;

-(void)sendAllPhrasesDoneForInterpretSection: (ABAInterpret *)interpret completionBlock:(CompletionBlock)block;

-(NSArray *)getDonePhrasesForRole: (ABAInterpretRole *)role forInterpretSection: (ABAInterpret *)interpret;

-(BOOL)allRolesAreCompletedForInterpretSection: (ABAInterpret *)interpret;

-(ABAInterpretPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onInterpret: (ABAInterpret*) interpret;

@end
