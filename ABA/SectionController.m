//
//  SectionController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"
#import "DataController.h"
#import "LanguageController.h"
#import "ABAWrite.h"
#import "ABAWriteDialog.h"
#import "ABAPhrase.h"
#import "UserController.h"

@interface SectionController ()

@end

@implementation SectionController

- (void)getAllSectionsForUnit:(ABAUnit *)unit contentBlock:(CompletionBlock)block
{
    [DataController getSectionForUnit:unit
                             withLang:[UserController getUserLanguage]
                           completion:^(NSError *error, id object) {
                               block(error, object);
                           }];
}

- (CGFloat)getTotalElementsForSection:(ABASection *)section
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (CGFloat)getElementsCompletedForSection:(ABASection *)section
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (BOOL)isSectionCompleted:(ABASection *)section
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (void)setCompletedSection:(ABASection *)section completionBlock:(CompletionBlock)completionBlock
{

    [DataController setCompletedSection:section completionBlock:completionBlock];
}

- (NSString *)getPercentageForSection:(ABASection *)section
{

    CGFloat totalWords = [self getTotalElementsForSection:section];

    CGFloat totalCompleted = [self getElementsCompletedForSection:section];

    CGFloat percentage = (totalCompleted / totalWords) * 100.0f;

    if ([section.progress floatValue] > percentage)
    {
        percentage = [section.progress floatValue];
    }

    if (percentage > 100.0 || [section.progress integerValue] >= 100)
        percentage = 100.0;

    NSString *percentageString = [NSString stringWithFormat:@"%ld%%", (long)percentage];

    return percentageString;
}

- (float)getProgressForSection:(ABASection *)section
{

    CGFloat totalWords = [self getTotalElementsForSection:section];

    CGFloat totalCompleted = [self getElementsCompletedForSection:section];

    CGFloat percentage = (totalCompleted / totalWords) * 100.0f;

    if (percentage > 100.0)
        percentage = 100.0;

    return percentage;
}

- (void)syncCompletedActions:(NSArray *)actions withSection:(ABASection *)section
{

    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

@end
