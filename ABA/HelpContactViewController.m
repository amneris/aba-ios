//
//  HelpContactViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 18/8/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "HelpContactViewController.h"
#import "UIViewController+ABA.h"
#import "Resources.h"
#import "LegacyTrackingManager.h"
#import "Utils.h"
#import <ZendeskSDK/ZendeskSDK.h>
#import <Crashlytics/Crashlytics.h>

@interface HelpContactViewController ()

@property (weak, nonatomic) IBOutlet UILabel *blueCircleLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIButton *knowledgeButton;
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;

- (IBAction)helpCenterAction:(id)sender;
- (IBAction)contactUsAction:(id)sender;
- (IBAction)rateAction:(id)sender;

@end

@implementation HelpContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self addMenuButtonWithTitle:STRTRAD(@"menuKey", @"Menú") andSubtitle:STRTRAD(@"helpAndContactKey", @"Ayuda y contacto")];
    CLS_LOG(@"HelpContactView controller: entering VC");
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    [self setupView];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    
    __weak typeof(self) weakSelf = self;
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        [weakSelf setupView];
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"helpandcontact"];
}

#pragma mark - Custom methods

- (void)setupView{
    
    [self.view setBackgroundColor:ABA_COLOR_REG_BACKGROUND];
    
    _blueCircleLabel.text = @"?";
    _blueCircleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?38:28];
    _blueCircleLabel.textColor = [UIColor whiteColor];
    _blueCircleLabel.textAlignment = NSTextAlignmentCenter;
    _blueCircleLabel.backgroundColor = ABA_COLOR_BLUE;
    _blueCircleLabel.layer.cornerRadius = _blueCircleLabel.frame.size.width/2;
    _blueCircleLabel.clipsToBounds = YES;
    
    _textLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:14];
    _textLabel.textColor = ABA_COLOR_DARKGREY;
    _textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel.numberOfLines = 2;
    
    void (^setupButton)(UIButton*) = ^void(UIButton* button){
      
        button.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:14];
        [button setTitleColor:ABA_COLOR_DARKGREY forState:UIControlStateNormal];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.backgroundColor = [UIColor whiteColor];
        button.layer.cornerRadius = 3.0f;
    };
    
    setupButton(_knowledgeButton);
    setupButton(_contactButton);
    setupButton(_rateButton);
    
    // strings
    _textLabel.text = STRTRAD(@"help_center_main_header", @"");
    [_knowledgeButton setTitle:STRTRAD(@"help_center_main_btn_knowledge_base", @"") forState:UIControlStateNormal];
    [_contactButton setTitle:STRTRAD(@"help_center_main_btn_contact_us", @"") forState:UIControlStateNormal];
    [_rateButton setTitle:STRTRAD(@"help_center_main_btn_rate_the_app", @"") forState:UIControlStateNormal];
}

- (IBAction)helpCenterAction:(id)sender
{
    NSArray *labels = @[ @"mobile%20app" ];
    [ZDKHelpCenter pushHelpCenterWithNavigationController:self.navigationController filterByArticleLabels:labels];
}

- (IBAction)contactUsAction:(id)sender
{
    [ZDKRequests presentRequestCreationWithViewController:self.navigationController];
}

- (IBAction)rateAction:(id)sender {
    
    NSURL *appURL = [NSURL URLWithString:AppStoreURL];
    [[UIApplication sharedApplication] openURL:appURL];
}

@end
