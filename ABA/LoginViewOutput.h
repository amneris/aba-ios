//
// Created by Oleksandr Gnatyshyn on 13/06/16.
// Copyright (c) 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoginViewOutput <NSObject>
- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password;
@end
