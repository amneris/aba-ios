//
//  PhrasesController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//
#import "PhrasesController.h"
#import "ABAPhrase.h"
#import "ABASection+Methods.h"
#import "Blocks.h"
#import "ABAUnit.h"
#import "DataController.h"
#import "UserController.h"
#import "ProgressController.h"
#import "ABARealmProgressAction.h"
#import "MDTUtils.h"
#import "ABAFilm.h"
#import "ABASpeak.h"
#import "ABAWrite.h"
#import "ABAInterpret.h"
#import "ABAVideoClass.h"
#import "ABAExercises.h"
#import "ABAVocabulary.h"
#import "ABAEvaluation.h"

@interface PhrasesController()

@end

@implementation PhrasesController

- (void)setPhrasesDone:(NSArray *)phrases forSection:(ABASection *)section sendProgressUpdate:(BOOL)updateProgress completionBlock:(CompletionBlock)completionBlock
{

    ABAUnit *currentUnit;

    switch ([section getSectionType])
    {
    case kABAFilmSectionType:
    {
        ABAFilm *film = (ABAFilm *)section;
        currentUnit = film.unit;
        break;
    }
    case kABASpeakSectionType:
    {
        ABASpeak *speak = (ABASpeak *)section;
        currentUnit = speak.unit;
        break;
    }
    case kABAWriteSectionType:
    {
        ABAWrite *write = (ABAWrite *)section;
        currentUnit = write.unit;
        break;
    }
    case kABAInterpretSectionType:
    {
        ABAInterpret *interpret = (ABAInterpret *)section;
        currentUnit = interpret.unit;
        break;
    }
    case kABAVideoClassSectionType:
    {
        ABAVideoClass *video = (ABAVideoClass *)section;
        currentUnit = video.unit;
        break;
    }
    case kABAExercisesSectionType:
    {
        ABAExercises *exercises = (ABAExercises *)section;
        currentUnit = exercises.unit;
        break;
    }
    case kABAVocabularySectionType:
    {
        ABAVocabulary *vocabulary = (ABAVocabulary *)section;
        currentUnit = vocabulary.unit;
        break;
    }
    case kABAAssessmentSectionType:
    {
        ABAEvaluation *evaluation = (ABAEvaluation *)section;
        currentUnit = evaluation.unit;
        break;
    }
    default:
        break;
    }

    [DataController setCompletedPhrases:phrases completionBlock:^(NSError *error, id object) {

        if (error)
        {
            NSLog(@"ERROR SAVING PHRASEs COMPLETED: %@", phrases);
            completionBlock(error, object);
        }
        else if (updateProgress)
        {
            [DataController updateProgressUnit:currentUnit completionBlock:^(NSError *error, id object) {
                
                if (error)
                {
                    completionBlock(error, object);
                }
                else
                {
                    [DataController saveProgressActionForSection:section andUnit:nil andPhrases:phrases errorText:nil isListen:NO isHelp:NO completionBlock:^(NSError *error, id object) {
                        
                        completionBlock(error, phrases);
                    }];
                }
            }];
        }
        else
        {
            completionBlock(nil, phrases);
        }
    }];

}

- (void)setPhrasesListened:(NSArray *)phrases forSection:(ABASection *)section sendProgressUpdate:(BOOL)updateProgress completionBlock:(CompletionBlock)block
{
    [DataController setPhrasesListened:phrases completionBlock:^(NSError *error, id object) {
        
        if(error)
        {
            block(error, object);
        }
        else if(updateProgress)
        {
            [DataController saveProgressActionForSection:section andUnit:nil andPhrases:phrases errorText:nil isListen: YES isHelp: NO completionBlock:^(NSError *error, id object) {

                block(error, phrases);
            }];
        }
        else
        {
            block(nil, phrases);
        }
    }];
}

- (void)setPhraseKO:(ABAPhrase *)phrase forSection:(ABASection *)section errorText: (NSString*) text completionBlock:(CompletionBlock)completionBlock {
    [DataController saveProgressActionForSection:section andUnit:nil andPhrases:@[phrase] errorText:text isListen: NO isHelp: NO completionBlock:^(NSError *error, id object) {
        completionBlock(error, object);
    }];
}

@end
