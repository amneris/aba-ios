//
//  ABAPlan.m
//  ABA
//
//  Created by Jordi Mele on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAPlan+Methods.h"
#import "ABAPlan.h"

@implementation ABAPlan

#pragma mark - Public methods (accessors)

- (NSString *)titlePlan
{
    return [ABAPlan getSubscriptionStringFromDays:self.days];
}

- (NSString *)pricePlan
{
    return [ABAPlan getSubscriptionPlanPriceForMonth:self.productWithDiscount forDays:self.days];
}

- (NSString *)originalPricePlan
{
    return [ABAPlan getSubscriptionPlanPriceForMonth:self.productWithOriginalPrice forDays:self.days];
}
 
- (NSString *)descriptionPlan
{
    return [ABAPlan getSubscriptionPlanPriceInOneCharge:self.productWithDiscount forDays:self.days];
}

#pragma mark - Public methods

- (void)mergeWithOriginalITunesProduct:(SKProduct *)originalProduct andDiscountProduct:(SKProduct *)discountProduct
{
    self.productWithDiscount = discountProduct;
    self.productWithOriginalPrice = originalProduct;
}

@end
