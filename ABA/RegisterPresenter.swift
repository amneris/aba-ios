//
//  RegisterPresenter.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

class RegisterPresenter {

    // VIPER
    weak var view: RegisterViewInput?
    var interactor: RegisterInteractorInput?
    var router: SessionRouterInput?
    var facebookInteractor: FacebookInteractorInput?

    var tracker: RegisterTracker?

    // RX
    let disposableBag = DisposeBag()

    init(tracker: RegisterTracker?) {
        self.tracker = tracker
    }
}

extension RegisterPresenter: RegisterViewOutput {

    // Commands

    func registerUser(name: String, email: String, password: String) {
        let validation = parameterValidation(name, email: email, pass: password)
        switch validation {
        case .Success:
            // Params are valid
            view?.showLoadingIndicator()
            performRegistration(name: name, email: email, pass: password)
        default:
            view?.renderValidationError(validation)
        }
    }

    func registerUserWithFacebook() {
        performFacebookLogin()
    }

    func goToForgotPassword() {
        self.router?.goToForgotPassword()
    }

    func goToLogin() {
        self.router?.openLogin()
    }

    // FormValidator

    func validateName(_ name: String?) {
        if !isNameValid(name) {
            self.view?.renderValidationError(.EmptyName)
        }
    }

    func validateEmail(_ email: String?) {
        if !isEmailValid(email) {
            self.view?.renderValidationError(.WrongEmailFormat)
        }
    }

    func validatePassword(_ password: String?) {
        if !isPasswordValid(password) {
            self.view?.renderValidationError(.WrongPasswordFormat)
        }
    }
}

extension RegisterPresenter: CredentialsStorer {

}

////////////////////////////////////
// MARK: private methods
////////////////////////////////////

extension RegisterPresenter {

    func performRegistration(name: String, email: String, pass: String) {
        
        CLSLogv("Starting email registration", getVaList([]));
        
        let dependencies = Dependencies()
        
        interactor?
            .register(name: name, email: email, password: pass)
            .subscribeOn(dependencies.backgroundWorkScheduler)
            .observeOn(dependencies.mainScheduler)
            .subscribe(onNext: { _ in
                self.tracker?.trackRegister(email, name: name)
                self.view?.hideLoadingIndicator()
                self.registrationFinished(email, pass: pass, newUser: true)
            }, onError: { eventError in
                self.view?.hideLoadingIndicator()
                if let registerError = eventError as? SessionInteractorError, registerError == SessionInteractorError.emailAlreadyExists {
                    self.view?.showEmailRepeatedError()
                }
                else {
                    self.view?.showGenericError()
                }
            })
            .addDisposableTo(disposableBag)
    }

    func performFacebookLogin() {
        
        CLSLogv("Starting Facebook registration", getVaList([]));
        
        let dependencies = Dependencies()
        
        facebookInteractor?
            .loginWithFacebook()
            .subscribeOn(dependencies.mainScheduler)
            .observeOn(dependencies.mainScheduler)
            .do(onNext: { _ in
                CLSLogv("Facebook registration process success. Proceeding to ABA registration.", getVaList([]));
                self.view?.showLoadingIndicator()
            } )
            .observeOn(dependencies.backgroundWorkScheduler)
            .flatMap({ facebookUser -> Observable<User> in
                if let interactor = self.interactor {
                    return interactor.registerWithFacebook(facebookUser)
                } else {
                    return Observable.error(SessionInteractorError.unableToRegister)
                }
        })
            .observeOn(dependencies.mainScheduler)
            .subscribe(onNext: { registeredUser in
                
                CLSLogv("User registered with Facebook", getVaList([]));
                
                self.view?.hideLoadingIndicator()
                self.tracker?.trackAccessWithFacebook(registeredUser.email!, name: registeredUser.name!, newUser: registeredUser.isNewUser)
                self.registrationFinished(registeredUser.email!, pass: nil, newUser: registeredUser.isNewUser)
                
            }, onError: { eventError in
                
                CLSLogv("Registration with Facebook failed", getVaList([]));
                
                self.view?.hideLoadingIndicator()
                
                guard let interactorError = eventError as? SessionInteractorError else {
                    self.view?.showGenericError()
                    return
                }
                
                if interactorError == SessionInteractorError.facebookUserCanceled {
                    // Do nothing as the user cancelled the facebook login on purpose
                }
                else if interactorError == SessionInteractorError.facebookGenericError {
                    self.view?.showFacebookError()
                }
                else if interactorError == SessionInteractorError.facebookEmailNotGranted {
                    self.view?.showPermissionError()
                }
                else if interactorError == SessionInteractorError.emailAlreadyExists {
                    self.view?.showEmailRepeatedError()
                }
                else {
                    self.view?.showGenericError()
                }
            })
            .addDisposableTo(disposableBag)
    }

    func registrationFinished(_ email: String, pass: String?, newUser: Bool) {
        // Storing login credentials
        storeLogin(email, pass: pass)

        // rendering confirmation into view
        self.view?.renderRegistrationConfirmation()

        // Navigation
        if newUser {
            router?.goToLevelSelection()
        } else {
            router?.goToUnitList()
        }
    }
}
