//
//  ABAExercisesTextField.m
//  ABA
//
//  Created by Marc Güell Segarra on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAExercisesTextField.h"
#import "Resources.h"
#import "SDVersion.h"
#import "Utils.h"

@implementation ABAExercisesTextField

#define INSET 10

-(id) initWithFrame: (CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        self.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.autocorrectionType = UITextAutocorrectionTypeNo;
        self.spellCheckingType = UITextSpellCheckingTypeNo;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width+(INSET*2), frame.size.height);
        
        self.textColor = ABA_COLOR_REG_TEXTFIELD;
        
        CGFloat fontSize;
         
        if ([SDVersion deviceSize] == Screen3Dot5inch ||
            [SDVersion deviceSize] == Screen4inch)
        {
            fontSize = 12.0f;
        }
        else
        {
			fontSize = IS_IPAD?24.0f:16.0f;
        }
        
        self.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:fontSize];
        
        [self.layer setBorderColor:ABA_COLOR_REG_BORDER.CGColor];
        [self.layer setBorderWidth:1];
        
        [self.layer setCornerRadius:7.0f];
        self.clipsToBounds = YES;        
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset( bounds , INSET , INSET );
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset( bounds , INSET , INSET );
}


@end
