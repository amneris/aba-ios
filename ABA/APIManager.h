//
//  APIManager.h
//  ABA
//
//  Created by Oriol Vilaró on 7/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Blocks.h"
#import "Definitions.h"
@import AFNetworking;
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@class ABAProgressOperationQueue, ABARealmProgressAction, ABAPlan, APIManager;

@interface APIManager : NSObject

+ (APIManager *)sharedManager;

/**
 *  Manager for ABA server ws
 */
@property(nonatomic, strong) AFHTTPRequestOperationManager *manager;

/**
 *  Manager for file downloads
 */
@property(nonatomic, strong) AFHTTPRequestOperationManager *fileManager;

/**
 This queue controlles how to handle the progresss events
 */
@property(nonatomic, strong) ABAProgressOperationQueue *progressQueue;

@property(nonatomic, strong) AFHTTPRequestOperationManager *progressManager;

@property(nonatomic) BOOL syncInProgress;

/**
 *  get current reachabilsity status
 *
 *       AFNetworkReachabilityStatusUnknown
 *       AFNetworkReachabilityStatusNotReachable
 *       AFNetworkReachabilityStatusReachableViaWWAN
 *       AFNetworkReachabilityStatusReachableViaWiFi 
 */
@property(readonly, getter=getReachabilityStatus) AFNetworkReachabilityStatus reachabilityStatus;

/**
 *  <#Description#>
 */
@property(strong, nonatomic) NSString *publicIp;

/** The when we are authorized, we save the token here */
@property(strong, nonatomic) NSString *token;

/**
 *  get current public ip
 *
 *  @param completionBlock completion block
 */
- (void)getPublicIP:(CompletionBlock)completionBlock;

- (void)postRegisterWithFacebookID:(NSString *)facebookID
                             email:(NSString *)email
                              name:(NSString *)name
                          surnames:(NSString *)surnames
                            gender:(NSString *)gender
                            avatar:(NSString *)avatar
                   completionBlock:(CompletionBlock)completionBlock;

/**
 *  <#Description#>
 *
 *  @param email           <#email description#>
 *  @param password        <#password description#>
 *  @param name            <#name description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)postRegisterWithEmail:(NSString *)email
                     password:(NSString *)password
                         name:(NSString *)name
              completionBlock:(CompletionBlock)completionBlock;

/**
 *  <#Description#>
 *
 *  @param email           <#email description#>
 *  @param password        <#password description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)postLoginWithEmail:(NSString *)email
                  password:(NSString *)password
           completionBlock:(CompletionBlock)completionBlock;

/**
 *  <#Description#>
 *
 *  @param email           <#email description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)postRecoverPassword:(NSString *)email
            completionBlock:(CompletionBlock)completionBlock;

/**
 *  <#Description#>
 *
 *  @param token           <#token description#>
 *  @param password        <#password description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)updatePasswordUserWithToken:(NSString *)token
                           password:(NSString *)password
                    completionBlock:(CompletionBlock)completionBlock;

/**
 *  Get all units
 *
 *  @param completionBlock <#completionBlock description#>
 */
- (void)getAllUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang completionBlock:(CompletionBlock)completionBlock;

/**
 *  <#Description#>
 *
 *  @param locale <#locale description#>
 *  @param unitID <#unitID description#>
 *  @param block  <#block description#>
 */
- (void)getSectionContentWithLocale:(NSString *)locale
                          andUnitID:(NSString *)unitID
                    completionBlock:(CompletionBlock)block;

- (void)getCurrencyConversionWithLocale:(NSString *)locale
                    withProduct:(SKProduct *)product
                completionBlock:(CompletionBlock)block;

/**
 *  <#Description#>
 *
 *  @param url             <#url description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)getContentsOfURL:(NSString *)url
         completionBlock:(CompletionBlock)completionBlock;

- (void)downloadFile:(NSURL *)url
          toFileName:(NSString *)fileName
     completionBlock:(CompletionBlock)completionBlock;

#pragma mark Progress methods

- (void)getCourseProgressWithLocale:(NSString *)locale
                          andUserID:(NSString *)userID
                    completionBlock:(CompletionBlock)block;

- (void)getUnitProgressWithLocale:(NSString *)locale
                        andUserID:(NSString *)userID
                        andUnitID:(NSString *)unitID
                  completionBlock:(CompletionBlock)block;

- (void)getCompletedActionsFromUserID:(NSString *)userID
                              andUnit:(ABAUnit *)unit
                        withSectionID:(NSString *)sectionType
                      completionBlock:(CompletionBlock)block;

- (void)updateUserLevel:(NSString *)idLevel
                 idUser:(NSString *)idUser
        completionBlock:(CompletionBlock)completionBlock;

- (void)sendProgressActionsToServerWithActions:(NSArray *)actions completionBlock:(CompletionBlock)block;

#pragma mark Subscription Methods

- (void)getSubscriptionProducts:(CompletionBlock)completionBlock;

- (void)updateUserToPremiumWithProductPrice:(NSDecimalNumber*)price
                           withCurrencyCode:(NSString*)currencyCode
                            withCountryCode:(NSString*)countryCode
                             withPediodDays:(NSInteger)days
                                 withUserId:(NSString *)userId
                              withSessionId:(NSString *)idSession
                                withReceipt:(NSDictionary *)receipt
                            completionBlock:(ErrorBlock)block;


#pragma mark Version Control

- (void)getCurrentValidAppVersion:(CompletionBlock)completionBlock;


@end
