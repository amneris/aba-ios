//
//  ForgotViewController.m
//  ABA
//
//  Created by Jordi Mele on 12/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ForgotViewController.h"
#import "ABACustomTextField.h"
#import "Resources.h"
#import "UIViewController+ABA.h"
#import "MDTUtils.h"
#import "ABAAlert.h"
#import "ABAScrollView.h"
#import "SDVersion.h"
#import "UserController.h"
#import "Utils.h"
#import "PureLayout.h"
@import SVProgressHUD;

@interface ForgotViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet ABAScrollView *scrollVC;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *imageLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleForgot;
@property (weak, nonatomic) IBOutlet UILabel *subTitleForgot;
@property (weak, nonatomic) IBOutlet ABACustomTextField *emailTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lockIconTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *forgotIconTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstLabelTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondLabelTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFieldHeightConstraint;

@end

@implementation ForgotViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Add gesture to dissmis keyboard
    if ([SDVersion deviceSize] != Screen3Dot5inch) {
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [tapRecognizer setNumberOfTapsRequired:1];
        
        [_contentView addGestureRecognizer:tapRecognizer];
    }
    
    [self configView];
    
    if (IS_IPAD) {
        [self applyConstraintsWith:[[UIApplication sharedApplication] statusBarOrientation]];
    }

    [_emailTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:1.0];
}

#pragma mark - Custom methods

- (void)configView {
    
    self.view.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    
    [_imageLabel.layer setBackgroundColor:ABA_COLOR_BLUE.CGColor];
    
    [_emailTextField setPlaceholder:STRTRAD(@"regPlaceholder2Key", @"tu email")];
    
    [_titleForgot setText:STRTRAD(@"forgotTitleKey", @"¿has olvidado tu contraseña?")];
    [_titleForgot setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD?22:18]];
    [_titleForgot setTextColor:ABA_COLOR_DARKGREY];
    
    [_subTitleForgot setText:STRTRAD(@"forgotSubTitleKey", @"danos tu email y te mandaremos instrucciones para recuperarla")];
    [_subTitleForgot setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:16]];
    [_subTitleForgot setTextColor:ABA_COLOR_DARKGREY];
    
    UIView *keyboardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [keyboardView setBackgroundColor:ABA_COLOR_DARKGREY];
    
    UIButton *keyboardButton = [UIButton newAutoLayoutView];
    [keyboardView addSubview:keyboardButton];
    
    [keyboardButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [keyboardButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [keyboardButton autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [keyboardButton autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:keyboardView];
    [keyboardButton autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:keyboardView];
    
    NSString *titleButton = [STRTRAD(@"keyboardTitleSend", @"enviar") uppercaseString];
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:titleButton];
    [mutAttStr addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_RALEWAY_BOLD size:IS_IPAD?16:14]
                       range:[titleButton rangeOfString:titleButton]];
    [mutAttStr addAttribute:NSKernAttributeName
                       value:[NSNumber numberWithFloat:1.0]
                       range:NSMakeRange(0, [titleButton length])];
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                       value:[UIColor whiteColor]
                       range:[titleButton rangeOfString:titleButton]];
    
    [keyboardButton setAttributedTitle:mutAttStr.copy forState:UIControlStateNormal];
    
    [keyboardButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    
    _emailTextField.inputAccessoryView = keyboardView;
    
    [self addToolbarWithBackButtonAndTitle:[STRTRAD(@"forgotTitleSection", @"recordar contraseña") uppercaseString]];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:YES];
}

/**
 *  Action send buttton
 */
- (void)send
{
    if ([self checkTextInput]) {
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [self.view endEditing:YES];
        
        [UserController recoverPassword:_emailTextField.text completionBlock:^(NSError *error, id object) {
            
            [SVProgressHUD dismiss];
            
            if (!error)
            {
                UIAlertController * alert = [UIAlertController  alertControllerWithTitle:nil message:STRTRAD(@"forgotPassAlertkey", @"Te hemos enviado un mail") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alert addAction:ok]; // add action to uialertcontroller
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                if (error.code == CODE_CONNECTION_ERROR)
                {
                    // Error de conexión
                    ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:error.localizedDescription];
                    [abaAlert show];
                }
                else
                {
                    // El email no existe
                    [_emailTextField setErrorBorderColor];
                    
                    ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"forgotEmailNoEmailkey", @"este email no está registrado contraseña")];
                    [abaAlert show];
                }
            }
        }];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self animateConstraintsFrom:fromInterfaceOrientation];
}

- (void)applyConstraintsWith:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (fromInterfaceOrientation == UIInterfaceOrientationPortrait ||
       fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) { // It's portrait
        
        _lockIconTopSpaceConstraint.constant = 120.0f;
        _forgotIconTopSpaceConstraint.constant = 134.0f;
        _firstLabelTopSpaceConstraint.constant = 25.0f;
        _secondLabelTopSpaceConstraint.constant = 25.0f;
        _textFieldTopConstraint.constant = 25.0f;
        _textFieldWidthConstraint.constant = 320.0f;
        _textFieldHeightConstraint.constant = 60.0f;
    }
    else {   // It's landscape
        _lockIconTopSpaceConstraint.constant = 80.0f;
        _forgotIconTopSpaceConstraint.constant = 94.0f;
        _firstLabelTopSpaceConstraint.constant = 18.0f;
        _secondLabelTopSpaceConstraint.constant = 18.0f;
        _textFieldTopConstraint.constant = 18.0f;
        _textFieldWidthConstraint.constant = 320.0f;
        _textFieldHeightConstraint.constant = 60.0f;
    }
}
- (void)animateConstraintsFrom:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if (fromInterfaceOrientation == UIInterfaceOrientationPortrait ||
       fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) { // From portrait to landscape
        [UIView animateWithDuration:0.5 animations:^{
            _lockIconTopSpaceConstraint.constant = 80.0f;
            _forgotIconTopSpaceConstraint.constant = 94.0f;
            _firstLabelTopSpaceConstraint.constant = 18.0f;
            _secondLabelTopSpaceConstraint.constant = 18.0f;
            _textFieldTopConstraint.constant = 18.0f;
            _textFieldWidthConstraint.constant = 320.0f;
            _textFieldHeightConstraint.constant = 60.0f;
            
            [self.view layoutIfNeeded];
        } completion:nil];
    }
    else {   // From landscape to portrait
        [UIView animateWithDuration:0.5 animations:^{
            
            _lockIconTopSpaceConstraint.constant = 120.0f;
            _forgotIconTopSpaceConstraint.constant = 134.0f;
            _firstLabelTopSpaceConstraint.constant = 25.0f;
            _secondLabelTopSpaceConstraint.constant = 25.0f;
            _textFieldTopConstraint.constant = 25.0f;
            _textFieldWidthConstraint.constant = 320.0f;
            _textFieldHeightConstraint.constant = 60.0f;
            [self.view layoutIfNeeded];
        } completion:nil];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self send];
    
    return YES;
}

/**
  *  Check textimput data
  *
  *  @return correct data
  */
- (BOOL)checkTextInput {
    
    if ([MDTUtils isTextEmpty:_emailTextField.text]) {
        [_emailTextField setErrorBorderColor];
        
        ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"regErrorEmailNil", @"necesitamos que nos digas un email")];
        [abaAlert show];
        return NO;
    }
    if (![MDTUtils isEmailValid:_emailTextField.text]) {
        [_emailTextField setErrorBorderColor];
        
        ABAAlert *abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"regErrorEmailFormat", @"El formato del email no es correcto")];
        [abaAlert show];
        return NO;
    }
    
    return YES;
}

@end
