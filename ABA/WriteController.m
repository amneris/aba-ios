//
//  WriteController.m
//  ABA
//
//  Created by Jordi Mele on 2/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "WriteController.h"
#import "ABAPhrase.h"
#import "ABAWrite.h"
#import "ABAWriteDialog.h"
#import "ProgressController.h"
#import "UserController.h"
#import "ABARealmProgressAction.h"

@implementation WriteController

-(NSInteger) getTotalPhraseCompletedForWriteSection:(ABAWrite *)write {

    NSInteger total = 0;
    
    for(ABAPhrase *phrase in [self getPhrasesDataSourceFromSection:write]) {
        
        if([phrase.listened boolValue])
            total++;
        
        if([phrase.done boolValue]) {
            total ++;
        }
    }
    
    return total;
}

-(NSArray*) getPhrasesDataSourceFromSection:(ABAWrite *)writeSection {
 
    NSMutableArray *dataSoruce = [NSMutableArray new];
    
    for(ABAWriteDialog *dialog in writeSection.content) {
        for(ABAPhrase *phrase in dialog.dialog) {
            [dataSoruce addObject:phrase];
        }
    }
    
    return dataSoruce;
}

-(NSDictionary*) getPhrasesDictionary:(ABAWrite *)writeSection {
    NSMutableDictionary *phrasesDict = [NSMutableDictionary new];
    
    for(ABAWriteDialog *dialog in writeSection.content) {
        for(ABAPhrase *phrase in dialog.dialog) {
            [phrasesDict setObject:phrase forKey:phrase.idPhrase];
        }
    }
    
    return phrasesDict;
}

-(CGFloat) getTotalElementsForSection: (ABASection*) section {
    return [[self getPhrasesDataSourceFromSection:(ABAWrite*)section] count]*2;
}

-(CGFloat) getElementsCompletedForSection: (ABASection*) section {
    ABAWrite *write = (ABAWrite*) section;
    return [self getTotalPhraseCompletedForWriteSection:write];
}

-(BOOL) isSectionCompleted: (ABASection*) section {
    ABAWrite *write = (ABAWrite*) section;
    
    if([self getTotalElementsForSection:write] == [self getTotalPhraseCompletedForWriteSection:write]) {
        return YES;
    }
    return NO;
}

- (void)syncCompletedActions:(NSArray *)actions withSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    NSDictionary *phrasesDict = [self getPhrasesDictionary:(ABAWrite *)section];
    NSMutableArray *phrasesDoneArray = @[].mutableCopy;

    for (NSDictionary *action in actions)
    {
        if ([[action objectForKey:@"IncorrectText"] isKindOfClass:[NSNull class]])
        {
            ABAPhrase *phrase = [phrasesDict objectForKey:[action objectForKey:@"Audio"]];

            if (phrase == nil)
            {
                NSLog(@"ACTION NOT FOUND: %@", action);
            }

            [phrasesDoneArray addObject:phrase];
        }
    }

    if (phrasesDoneArray.count)
    {
        [phrasesController setPhrasesDone:phrasesDoneArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object) {

                              if (!error) {

                                  if ([self isSectionCompleted:section] && (section.completed == nil || section.completed == [NSNumber numberWithBool:NO]))
                                  {
                                      [self setCompletedSection:section completionBlock:^(NSError *error, id object) {
                                          
                                      }];
                                  }
                              }
                          }];
    }
}

-(NSArray *)getAllAudioIDsForWriteSection: (ABAWrite *)write
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(ABAPhrase *phrase in [self getPhrasesDataSourceFromSection:write])
    {
        [array addObject:phrase.audioFile];
    }
    
    return array;
}

-(void) sendAllPhrasesDoneForWriteSection:(ABAWrite *)write completionBlock:(CompletionBlock)block {
    
    __weak typeof(self) weakSelf = self;
    
    __block NSInteger numberOfPhrasesCompleted = 0;
    __block NSInteger numberOfTotalPhrases = 0;
    
    NSArray *totalPhrases = [self getPhrasesDataSourceFromSection:write];
    numberOfTotalPhrases = totalPhrases.count;
    
    PhrasesController *phrasesController = [PhrasesController new];
    
    for(ABAWriteDialog *dialog in write.content) {
        for(ABAPhrase *phrase in dialog.dialog) {
            
            [phrasesController setPhrasesDone:@[phrase] forSection:write sendProgressUpdate:NO completionBlock:^(NSError *error, id object)
             {
                 
                 if(!error) {
                     numberOfPhrasesCompleted ++;
                     
                     if(numberOfPhrasesCompleted == numberOfTotalPhrases) {
                         [weakSelf setCompletedSection:write completionBlock:^(NSError *error, id object) {
                             if(!error) {
                                 block(nil,write);
                                 NSLog(@"Finished");
                             }
                             else {
                                 block(error,nil);
                                 NSLog(@"Error");
                             }
                             
                         }];
                     }
                 }
                 else {
                     block(error,nil);
                     NSLog(@"Error");
                 }
                 
             }];
        }
    }
}

@end
