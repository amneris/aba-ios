//
//  PlansConfigurator.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

@objc
class PlansConfigurator: NSObject {
    
    class func createPlansViperStack(type: PlansViewControllerOriginType, shouldShowBackButton: Bool) -> PlansViewController {
        let plansVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlansViewController") as! PlansViewController
        plansVC.originController = type
        plansVC.shouldShowBackButton = shouldShowBackButton
        configure(plansVC)
        
        return plansVC
    }
}

extension PlansConfigurator {
    
    class func configure(_ viewController: PlansViewController) {
        let repository = SelligentRepository()
        let presenter = PlansPresenter()
        let selligentInteractor = SelligentPlansInteractor(repository: repository)
        let itunesInteractor = ItunesPlansInteractor()
        //        let router = SessionRouter(vc: viewController)
        
        presenter.view = viewController
        presenter.selligentInteractor = selligentInteractor
        presenter.iTunesInteractor = itunesInteractor
        
        //        presenter.router = router
        viewController.presenter = presenter
    }
}
