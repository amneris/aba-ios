//
//  SectionBaseViewController.h
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABASection;

@interface SectionBaseViewController : UIViewController

//@property (nonatomic, readonly) double studyTime;
@property (strong, nonatomic) ABASection *section;

- (void)presentViewController:(UIViewController *)viewControllerToPresent
                     animated:(BOOL)flag
         continueTrackingTime:(BOOL)skipTrackingTime
                   completion:(void (^)(void))completion;
@end
