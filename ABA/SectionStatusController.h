//
//  SectionStatusController.h
//  ABA
//
//  Created by Oriol Vilaró on 16/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABASection+Methods.h"
#import "Definitions.h"

@interface SectionStatusController : NSObject

+ (kABASectionActionStatus)getStatusForSection:(ABASection *)section;

+ (kABASectionStyleOptions)getCreateStyleOptionsForSection:(ABASection *)section;
+ (kABASectionStyleOptions)getUpdateStyleOptionsForSection:(ABASection *)section;

@end
