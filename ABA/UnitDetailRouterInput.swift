//
//  UnitDetailRouterInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 06/03/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

protocol UnitDetailRouterInput {
    func loadSection(section: kABASectionTypes, unitId: NSNumber?, sectionDelegate: SectionProtocol)
    func loadPlans()
}
