//
//  CurrencyManagerResponseSerializer.h
//  ABA
//
//  Created by MBP13 Jesus on 10/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "AFURLResponseSerialization.h"

@interface CurrencyManagerResponseSerializer : AFXMLParserResponseSerializer

@end
