//
//  ABAInterpretPauseView.m
//  ABA
//
//  Created by Marc Güell Segarra on 28/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAInterpretPauseView.h"
#import "Utils.h"

@implementation ABAInterpretPauseView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"ABAInterpretPause" owner:self options:nil] objectAtIndex:0];
        self.frame = frame;

        _pausedInterpetLabel.text = STRTRAD(@"pausedInterpretTextKey", @"Interpretación pausada");
        [_continueButton setTitle:STRTRAD(@"continuePausedInterpretTextKey", @"Continuar desde el último punto de guardado") forState:UIControlStateNormal];
        [_restartButton setTitle:STRTRAD(@"restartPausedInterpretTextKey", @"Volver a empezar desde el principio") forState:UIControlStateNormal];
    }
    
    return self;
}

- (IBAction)continueButtonAction:(id)sender
{
    [self.delegate continuePauseViewAction];
}

- (IBAction)restartButtonAction:(id)sender
{
    [self.delegate restartPauseViewAction];
}

@end
