//
//  DataSource.h
//  ABA
//
//  Created by Oriol Vilaró on 7/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

+ (nonnull instancetype)sharedManager;

@property (strong, nonatomic) NSArray *progressCache;

- (void)clearData;

@end
