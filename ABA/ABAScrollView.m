//
//  ABAScrollView.m
//  ABA
//
//  Created by Jordi Mele on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAScrollView.h"
#import "SDVersion.h"


@implementation ABAScrollView


+(instancetype)alloc{
    
    if ([SDVersion deviceSize] == Screen3Dot5inch) {
        
        return (ABAScrollView*)[TPKeyboardAvoidingScrollView alloc];
        
    }else{
        
        return (ABAScrollView*)[UIScrollView alloc];
    }
}


@end
