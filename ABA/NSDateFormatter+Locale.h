//
//  NSDateFormatter+Locale.h
//  ABA
//
//  Created by Oriol Vilaró on 14/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Locale)

- (id)initWithSafeLocale;

@end
