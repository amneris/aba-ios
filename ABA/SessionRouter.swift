//
//  SessionRouter.swift
//  ABA
//
//  Created by MBP13 Jesus on 04/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import QuartzCore

class SessionRouter {
    let animationDuration = 0.33
    weak var viewController: UIViewController?

    init(vc: UIViewController) {
        viewController = vc
    }
}

extension SessionRouter: SessionRouterInput {

    @objc func openRegister() {
        let isRootController = self.viewController?.navigationController?.viewControllers.count == 2
        if isRootController {
            let registerFormVC = RegisterConfigurator.createRegisterViperStack()
            openController(registerFormVC)
        }
        else {
            pop()
        }
    }

    func openLogin() {
        let isRootController = self.viewController?.navigationController?.viewControllers.count == 2
        if isRootController {
            let lginFormVC = LoginConfigurator.createLoginViperStack()
            openController(lginFormVC)
        }
        else {
            pop()
        }
    }

    @objc func goToUnitList() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateInitialViewController()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window.rootViewController = vc

        // Starting to sync the progress
        appDelegate.syncProgress()
    }

    @objc func goToForgotPassword() {
        let storyboard = UIStoryboard(name: "Start", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "ForgotViewController")
        self.viewController?.navigationController?.pushViewController(loginVC, animated: true)
    }

    func goToLevelSelection() {
        let storyboard = UIStoryboard(name: "Start", bundle: nil)
        let levelVC = storyboard.instantiateViewController(withIdentifier: "LevelViewController") as! LevelViewController
        levelVC.newRegister = true
        self.viewController?.navigationController?.pushViewController(levelVC, animated: true)
    }
}

// Methods to push and pop from login to register and vice versa
private extension SessionRouter {
    
    func openController(_ vc: UIViewController) {
        UIView.animate(withDuration: animationDuration, animations: { () -> Void in
            UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
            self.viewController?.navigationController?.pushViewController(vc, animated: false)
            UIView.setAnimationTransition(UIViewAnimationTransition.flipFromRight, for: (self.viewController?.navigationController?.view)!, cache: false)
        })
    }

    func pop() {
        UIView.animate(withDuration: animationDuration, animations: { () -> Void in
            UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
            UIView.setAnimationTransition(UIViewAnimationTransition.flipFromLeft, for: (self.viewController?.navigationController?.view)!, cache: false)
        })
        _ = self.viewController?.navigationController?.popViewController(animated: false)
    }
}
