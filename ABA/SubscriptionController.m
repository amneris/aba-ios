
//
//  SubscriptionController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAError.h"
#import "ABAPlan+Methods.h"
#import "ABARealmUser.h"
#import "APIManager.h"
#import "NSDateFormatter+Locale.h"
#import "SubscriptionController.h"
#import "UserController.h"
#import "Utils.h"
#import <CargoBay/CargoBay.h>
#import <Adjust/Adjust.h>

// Model
#import "SKProduct+GenericTrackingManager.h"

// Analytics
#import "FabricTrackingManager.h"
#import "LegacyTrackingManager.h"
#import "MixpanelTrackingManager.h"
#import <Crashlytics/Crashlytics.h>

#define kDefaultRetryCount 5

@implementation SubscriptionController

#pragma mark - Public methods - Configuration

+ (void)configureCargoWithListener:(void (^)(SubscriptionStatus status))listener
{
    /**
     New purchase
     */
    [[CargoBay sharedManager] setPaymentQueueUpdatedTransactionsBlock:^(SKPaymentQueue *queue, NSArray *transactions) {

        __block BOOL updatingUserToPremium = NO;

        for (SKPaymentTransaction *transaction in transactions)
        {
            switch (transaction.transactionState)
            {
            case SKPaymentTransactionStatePurchased:
            {
                [[CargoBay sharedManager] verifyTransaction:transaction password:ABAInAppSharedSecretKey success:^(NSDictionary *receipt) {

                    NSDateFormatter *formatter = [[NSDateFormatter alloc] initWithSafeLocale];
                    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss VV";

                    NSDate *expirationDate = [formatter dateFromString:receipt[@"latest_receipt_info"][@"expires_date_formatted"]];
                    if ([receipt[@"status"] isEqualToNumber:@0] && !updatingUserToPremium && [expirationDate compare:[NSDate date]] == NSOrderedDescending)
                    {
                        updatingUserToPremium = YES;

                        // Restored transaction is valid.
                        [SubscriptionController updateUserToPremiumWithReceipt:receipt andTransaction:transaction withCompletionBlock:^(SubscriptionStatus status) {
                            /* Possible status here: SubscriptionStatusActivationProcessSucceed, SubscriptionStatusActivationProcessFailed, SubscriptionStatusPurchaseAlreadyAssigned, SubscriptionStatusProductNotFound */
                            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                            [self resetOriginalPrice];

                            [self notifyListener:listener withStatus:status withTransactionsToFinish:@[]];
                        }];
                    }
                    else
                    {
                        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    }

                }
                    failure:^(NSError *error) {

                        if (error.code != 6) // Error Domain=com.mattt.CargoBay.ErrorDomain Code=6 "Transaction ID (1000000164909246) is not unique."
                        {
                            [self notifyListener:listener withStatus:SubscriptionStatusTransactionVerificationFailed withTransactionsToFinish:@[]];
                            [self resetOriginalPrice];
                        }

                        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                    }];

                break;
            }
            case SKPaymentTransactionStateFailed:
            {
                [self notifyListener:listener withStatus:SubscriptionStatusPurchaseProcessFailed withTransactionsToFinish:@[]];
                [self resetOriginalPrice];

                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];

                break;
            }
            case SKPaymentTransactionStateRestored:
            {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
            case SKPaymentTransactionStateDeferred:
            {
                // Further update will be received
                break;
            }
            default:
                break;
            }
        }
    }];

    /**
     Restore purchase
     */
    [[CargoBay sharedManager]
        setPaymentQueueRestoreCompletedTransactionsWithSuccess:^(SKPaymentQueue *queue) {

            // Ordering transactions using Date
            NSArray *transactions = [queue.transactions sortedArrayUsingDescriptors:@[ [NSSortDescriptor sortDescriptorWithKey:@"transactionDate" ascending:NO] ]];

            // Restoring transactions
            // Making sure the process occurs in background
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self restorePurchasesWithListener:listener withTransactions:transactions withAccumulatedResult:SubscriptionStatusRestoreNoTransactions];
            });
        }
        failure:^(SKPaymentQueue *queue, NSError *error) {
            [self notifyListener:listener withStatus:SubscriptionStatusRestoreProcessFailed withTransactionsToFinish:@[]];
        }];

    /**
     Remove purchase
     */
    [[CargoBay sharedManager] setPaymentQueueRemovedTransactionsBlock:^(SKPaymentQueue *queue, NSArray *transactions) {
        for (SKPaymentTransaction *transaction in transactions)
        {
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        }
    }];

    /**
     Delegate set
     */
    [[SKPaymentQueue defaultQueue] addTransactionObserver:[CargoBay sharedManager]];
}

+ (void)restorePurchasesWithListener:(void (^)(SubscriptionStatus status))listener withTransactions:(NSArray *)transactions withAccumulatedResult:(SubscriptionStatus)accumulatedResult
{
    // If there are transactions
    if ([transactions count] > 0)
    {
        NSLog(@"%lu transactions to go.", (unsigned long)[transactions count]);

        // Getting next transaction
        SKPaymentTransaction *nextTransaction = transactions[0];

        if (nextTransaction.transactionState == SKPaymentTransactionStateRestored)
        {
            // Trying to activate the transaction
            [[CargoBay sharedManager]
                verifyTransaction:nextTransaction
                password:ABAInAppSharedSecretKey
                success:^(NSDictionary *receipt) {

                    // After logout and still using the app this call can be triggered and if further crashes
                    NSString *userId = [UserController currentUser].idUser;
                    if (!userId)
                    {
                        NSLog(@"User is null. Weird!");
                        // No way the process can continue without a user within the database
                        [self notifyListener:listener withStatus:SubscriptionStatusActivationProcessFailed withTransactionsToFinish:transactions];
                    }
                    else if (receipt)
                    {
                        if ([receipt[@"status"] isEqualToNumber:@0])
                        {
                            [self updateUserToPremiumWithReceipt:receipt andTransaction:nextTransaction withCompletionBlock:^(SubscriptionStatus status) {

                                switch (status)
                                {
                                case SubscriptionStatusActivationProcessSucceed:
                                    NSLog(@"Purchase successfully activated. Finishing process.");
                                    // If success then process finished and no more iterations
                                    [self notifyListener:listener withStatus:SubscriptionStatusActivationProcessSucceed withTransactionsToFinish:transactions];
                                    break;
                                case SubscriptionStatusActivationProcessFailed:
                                    NSLog(@"Purchase activation failed. Finishing process.");
                                    // If connection error then no more iterations
                                    [self notifyListener:listener withStatus:SubscriptionStatusActivationProcessFailed withTransactionsToFinish:transactions];
                                    break;
                                default:
                                    NSLog(@"Purchase activation failed with 404. Skipping and continuing with next transaction.");
                                    [[SKPaymentQueue defaultQueue] finishTransaction:nextTransaction];
                                    [self restorePurchasesWithListener:listener withTransactions:[transactions subarrayWithRange:NSMakeRange(1, transactions.count - 1)] withAccumulatedResult:(accumulatedResult < status) ? status : accumulatedResult];
                                    break;
                                }
                            }];
                        }
                        else
                        {
                            NSLog(@"Receipt status: %@. Skipping and continuing with next transaction.", receipt[@"status"]);
                            [[SKPaymentQueue defaultQueue] finishTransaction:nextTransaction];
                            [self restorePurchasesWithListener:listener withTransactions:[transactions subarrayWithRange:NSMakeRange(1, transactions.count - 1)] withAccumulatedResult:accumulatedResult];
                        }
                    }
                    else
                    {
                        NSLog(@"Receipt is null. Verification failed.");
                        [self notifyListener:listener withStatus:SubscriptionStatusTransactionVerificationFailed withTransactionsToFinish:transactions];
                    }
                }
                failure:^(NSError *error) {
                    NSLog(@"CargoBay verification failed.");
                    [self notifyListener:listener withStatus:SubscriptionStatusTransactionVerificationFailed withTransactionsToFinish:transactions];
                }];
        }
        else
        {
            NSLog(@"Transaction state is NOT Restored. Skipping and continuing with next transaction.");
            [[SKPaymentQueue defaultQueue] finishTransaction:nextTransaction];
            [self restorePurchasesWithListener:listener withTransactions:[transactions subarrayWithRange:NSMakeRange(1, transactions.count - 1)] withAccumulatedResult:accumulatedResult];
        }
    }
    else
    {
        NSLog(@"No more subscriptions. Finishing process.");
        [self notifyListener:listener withStatus:accumulatedResult withTransactionsToFinish:@[]];
    }
}

+ (void)notifyListener:(void (^)(SubscriptionStatus status))listener withStatus:(SubscriptionStatus)status withTransactionsToFinish:(NSArray *)transactions
{
    // Finishing remaining transactions
    for (SKPaymentTransaction *nextTransaction in transactions)
    {
        [[SKPaymentQueue defaultQueue] finishTransaction:nextTransaction];
    }

    // Tracking purchase result
    if (status != SubscriptionStatusActivationProcessSucceed)
    {    
        [[MixpanelTrackingManager sharedManager] trackSubscriptionErrorWithSubscriptionStatus:status];
    }

    // Calling listener
    NSLog(@"Finishing process");
    dispatch_async(dispatch_get_main_queue(), ^{
        listener(status);
    });
}

#pragma mark - Public methods - Fetcher

+ (void)getSubscriptionProducts:(CompletionBlock)completionBlock
{
    [[APIManager sharedManager] getSubscriptionProducts:^(NSError *error, NSDictionary *plansDictionary) {
        
        NSArray *arrayOfABAPlans = plansDictionary[@"products"];
        NSString *plansPhrase = plansDictionary[@"text"];

        if (!error && [arrayOfABAPlans count] > 0)
        {
            // Extracting identifiers
            NSMutableArray *allIdentifiers = [NSMutableArray array];
            [arrayOfABAPlans enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                [allIdentifiers addObject:[obj originalIdentifier]];
                [allIdentifiers addObject:[obj discountIdentifier]];
            }];

            // Getting information about apple products
            [self fetchProductsFromAppleWithIdentifiers:allIdentifiers withCompletionBlock:^(NSError *error, NSArray *products) {

                // Hydrating plans with apple products
                for (ABAPlan *plan in arrayOfABAPlans)
                {
                    NSArray *skProductSearchWithDiscount = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier = %@", plan.discountIdentifier]];
                    NSArray *skProductSearchWithOriginalPrice = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier = %@", plan.originalIdentifier]];

                    // Apple products retrieved
                    if ([skProductSearchWithDiscount count] > 0 && [skProductSearchWithOriginalPrice count] > 0)
                    {
                        [plan mergeWithOriginalITunesProduct:skProductSearchWithOriginalPrice[0] andDiscountProduct:skProductSearchWithDiscount[0]];
                    }
                    else
                    {
                        // No apple products
                        completionBlock([ABAError errorWithKey:STRTRAD(@"errorFetchingSubscriptions", @"No se han podido recuperar las suscripciones")], nil);
                        return;
                    }
                }
                completionBlock(nil, @{ @"products" : arrayOfABAPlans , @"text" : plansPhrase } );
            }];
        }
        else
        {
            completionBlock(error, nil);
        }
    }];
}

#pragma mark - Public methods - Purchase triggers

+ (void)subscribeToPlan:(ABAPlan *)plan
{
    // Storing promo code
    [self setOriginalPrice:plan.productWithOriginalPrice.price];

    // Adding purchase to queue
    SKPayment *payment = [SKPayment paymentWithProduct:plan.productWithDiscount];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

+ (void)restorePurchases
{
    // Restoring transactions
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    CLS_LOG(@"Subscription controller: restore purchases");
}

#pragma mark - Private methods

+ (void)fetchProductsFromAppleWithIdentifiers:(NSArray *)object withCompletionBlock:(CompletionBlock)completionBlock
{
    [self fetchProductsFromAppleWithIdentifiers:object withCompletionBlock:completionBlock withRetryCount:0];
}

+ (void)fetchProductsFromAppleWithIdentifiers:(NSArray *)arrayOfIdentifiers withCompletionBlock:(CompletionBlock)completionBlock withRetryCount:(NSInteger)retryCount
{
    [[CargoBay sharedManager] productsWithIdentifiers:[NSSet setWithArray:arrayOfIdentifiers] success:^(NSArray *products, NSArray *invalidIdentifiers) {

        completionBlock(nil, products);

    }
        failure:^(NSError *error) {
            if (retryCount != 0)
            {
                // Keep trying until the retry count is zero
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [SubscriptionController fetchProductsFromAppleWithIdentifiers:arrayOfIdentifiers withCompletionBlock:completionBlock withRetryCount:retryCount - 1];
                });
            }
            else
            {
                completionBlock(error, nil);
            }
        }];
}

+ (void)updateUserToPremiumWithReceipt:(NSDictionary *)receipt andTransaction:(SKPaymentTransaction *)transaction withCompletionBlock:(void (^)(SubscriptionStatus status))block
{
    NSString *purchasedProductId = receipt[@"receipt"][@"product_id"];
    if (purchasedProductId)
    {
        // Obteniendo la info de subscripcion
        [SubscriptionController fetchProductsFromAppleWithIdentifiers:[NSArray arrayWithObject:purchasedProductId] withCompletionBlock:^(NSError *error, NSArray *products) {
            if (!error && [products count] > 0)
            {
                SKProduct *purchasedProduct = [products objectAtIndex:0];
                NSInteger period = [ABAPlan getProductPeriod:purchasedProduct];

                // After logout and still using the app this call can be triggered and if further crashes
                NSString *userId = [UserController currentUser].idUser;
                if (!userId)
                {
                    block(SubscriptionStatusActivationProcessFailed);
                    return;
                }

                // Producto encontrado
                [[APIManager sharedManager] updateUserToPremiumWithProductPrice:purchasedProduct.price withCurrencyCode:[purchasedProduct.priceLocale objectForKey:NSLocaleCurrencyCode] withCountryCode:[purchasedProduct.priceLocale objectForKey:NSLocaleCountryCode] withPediodDays:period withUserId:[UserController currentUser].idUser withSessionId:[UserController currentUser].idSession withReceipt:receipt completionBlock:^(NSError *error) {
                    if (!error)
                    {
                        [[LegacyTrackingManager sharedManager] trackTransaction:transaction withPeriod:period withProduct:purchasedProduct];
                        [[MixpanelTrackingManager sharedManager] trackPurchase:transaction withPeriod:period withProduct:purchasedProduct withOriginalPrice:[self getOriginalPrice]];
                        [[FabricTrackingManager sharedManager] trackPurchase:transaction withPeriod:period withProduct:purchasedProduct];

                        [[MixpanelTrackingManager sharedManager] trackActivatedPurchase];

                        block(SubscriptionStatusActivationProcessSucceed);
                    }
                    else if (error.code == 410)
                    {
                        // The receipt was already used to activate another user into premium
                        block(SubscriptionStatusPurchaseAlreadyAssigned);
                    }
                    else
                    {
                        block(SubscriptionStatusActivationProcessFailed);
                    }
                }];
            }
            else
            {
                // Producto no encontrado
                block(SubscriptionStatusProductNotFound);
            }
        }];
    }
    else
    {
        // Producto no encontrado
        block(SubscriptionStatusProductNotFound);
    }
}

#pragma mark - Private methods (Defaults)

#define kSubscriptionOriginalPriceKey @"SubscriptionOriginalPriceKey"

+ (NSDecimalNumber *)getOriginalPrice
{
    NSDecimalNumber *price = [[NSUserDefaults standardUserDefaults] objectForKey:kSubscriptionOriginalPriceKey];
    return price;
}

+ (void)setOriginalPrice:(NSDecimalNumber *)price
{
    [[NSUserDefaults standardUserDefaults] setObject:price forKey:kSubscriptionOriginalPriceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)resetOriginalPrice
{
    [self setOriginalPrice:nil];
}

@end
