//
//  RepoUserDAO.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

struct UserDataModelDAO {

    static let RepoUserDefaultsKey = "RepoUserDefaultsKey"

    static func getCurrentUser() -> UserDataModel? {
        if let data = UserDefaults.standard.object(forKey: RepoUserDefaultsKey) as? Data
        {
            let repoUser = NSKeyedUnarchiver.unarchiveObject(with: data)
            return repoUser as? UserDataModel
        }

        return nil
    }

    static func saveUser(_ user: UserDataModel) {
        let data = NSKeyedArchiver.archivedData(withRootObject: user)
        let defaults = UserDefaults.standard
        defaults.set(data, forKey: RepoUserDefaultsKey)
        defaults.synchronize()
    }

    static func deleteAllUsers() {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: RepoUserDefaultsKey)
        defaults.synchronize()
    }
}
