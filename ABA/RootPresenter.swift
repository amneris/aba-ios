//
//  RootPresenter.swift
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

@objc class RootPresenter: NSObject {
    var app: RootViewInput!
    var interactor: LoginInteractorInput!
    var router: RootRouter!
    var tracker: LoginTracker?

    let disposableBag = DisposeBag()

    init(tracker: LoginTracker?) {
        self.tracker = tracker
    }
}

extension RootPresenter: RootViewOutput {

    // MARK: Public methods

    func loadMainStoryboard() {
        router.loadMainStoryboard(self.app.appWindow())
    }

    func loadStartStoryboard() {
        self.router.loadStartStoryboardAB(self.app.appWindow())
        MixpanelTrackingManager.shared().trackRegistrationFunnelShown()
    }

    func showBlockView() {
        router.showBlockViewController(self.app.appWindow())
    }

    func loadLevelSelectionViewController() {
        router.loadLevelSelectionViewController(self.app.appWindow())
    }

    func appDidFinishLaunching(_ application: UIApplication!, options: [AnyHashable: Any]?) {
        if !SupportedVersionManager().isCurrentVersionSupported() {
            router.showBlockViewController(self.app.appWindow())
            return
        }

        DeepLinkController.cleanDeeplinkingParameters()
        let deeplinkController = DeepLinkController(launchOptions: options as NSDictionary?)
        if deeplinkController.canParseDeepLink {
            deeplinkController.saveDeepLink()
        }
        
        startApp()
    }

    func appDidReceivePush(_ userinfo: [AnyHashable: Any]!) {

        // Reading app state
        let appState = UIApplication.shared.applicationState

        // Reading deeplink url (if exists)
        var customUrl: URL? = nil
        if let customUrlString = userinfo["deeplink_data"] as? String {
            customUrl = URL(string: customUrlString)
        }

        // No deeplink
        if customUrl == nil && appState == .active {
            if let aps = userinfo["aps"] as? NSDictionary, let message = aps.object(forKey: "alert") as? String {
                app.showPushAlert(message)
            }
        }

        // Deeplink and app is not active
        if (appState != .active && customUrl != nil) {
            let deeplinkController = DeepLinkController(url: customUrl as NSURL?)
            if deeplinkController.canParseDeepLink {
                deeplinkController.saveDeepLink()
                startApp()
            }
        }
    }
    
    ////////////
    // MARK: private methods
    ////////////
    
    func startApp() {
        router.loadSplashImage(self.app.appWindow())

        // Token handing
        let storedToken = DeepLinkController.storedToken
        let currentUser = UserController.currentUser()

        if let storedToken = storedToken {
            if UserController.isUserLogged() && storedToken != currentUser?.token {
                UserController.logout()
                self.app.resetAPNSConfiguration()
            }

            loginWithToken(storedToken)
        }
        else if UserController.isUserLogged() {
            APIManager.shared().token = currentUser?.token
            loginWithToken((currentUser?.token)!)
        }
        else {
            #if SHEPHERD_DEBUG
                if let shepherdToken = ABAShepherdExternalLogin.retrieveTokenAndErase() {
                    loginWithToken(shepherdToken)
                }
            #endif
            DeepLinkController.cleanDeeplinkingParameters()
            self.loadStartStoryboard()
        }
    }

    func loginWithToken(_ token: String) {
        self.interactor.login(token: token).subscribe { event in
            switch event {
            case .next:
                CLSLogv("Login with token %@", getVaList(["Success"]))
                self.tracker?.trackLoginWithToken()
                self.router.loadMainStoryboard(self.app.appWindow())
                self.app.syncProgress()

            case .error(let error):
                if let anError = error as? LoginRepositoryError, anError == LoginRepositoryError.deviceIsOffline {
                    self.tracker?.trackLoginOffline()
                    self.router.loadMainStoryboard(self.app.appWindow())
                    self.app.syncProgress()
                    CLSLogv("Login with token %@", getVaList(["Login Offline"]))
                }
                else {
                    UserController.logout()
                    self.app.resetAPNSConfiguration()
                    self.router.loadStartStoryboardAB(self.app.appWindow())
                    CLSLogv("Login with token %@", getVaList(["Login error --> Logout"]))
                }

            default:
                CLSLogv("Login with token %@", getVaList(["Default error"]))
            }

            DeepLinkController.storedToken = nil

        }.addDisposableTo(disposableBag)
    }
}
