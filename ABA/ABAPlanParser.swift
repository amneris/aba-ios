//
//  ABAPlanParser.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

class ABAPlanParser {
    
    class func parsePlans(skProducts:[SKProduct], selligentProducts:[SelligentSubscriptionProduct]) -> [PlansPageSubscriptionProduct] {
        
        var listOfPlans:[ABAPlan] = []
        
        selligentProducts.forEach { selligentProduct in
            
            let skProductWithOriginalIdentifier = skProducts.filter { $0.productIdentifier == selligentProduct.originalIdentifier
            }
            
            let skProductWithDiscountIdentifier = skProducts.filter { $0.productIdentifier == selligentProduct.discountIdentifier
            }
            
            let plan = ABAPlan()
            plan.originalIdentifier = selligentProduct.originalIdentifier
            plan.discountIdentifier = selligentProduct.discountIdentifier
            plan.days = Int(selligentProduct.days!)!
            plan.is2x1 = selligentProduct.is2x1 == "1"
            
            plan.productWithDiscount = skProductWithDiscountIdentifier[0]
            plan.productWithOriginalPrice = skProductWithOriginalIdentifier[0]
            
            listOfPlans.append(plan)
        }
        
        return listOfPlans
    }
}
