//
//  ApiSigner.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

public protocol ApiSignable {
    func params() -> [String: String]
    var orderedParams: [String] {get}
}

extension ApiSignable {
    func toMap() -> [String: String] {
        var parameters = params()
        parameters["signature"] = signature()
        return parameters
    }
    
    func signature() -> String {
        let raw = "Mobile Outsource Aba AgnKey" + paramsToSign()
        return raw.md5()
    }
    
    func paramsToSign() -> String {
        let parameters = params()
        var concatenatedParams = ""
        orderedParams.forEach { param in
            if let value = parameters[param] {
                concatenatedParams += value
            }
        }
        
        return concatenatedParams
    }
}