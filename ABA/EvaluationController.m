//
//  EvaluationController.m
//  ABA
//
//  Created by Ivan Ferrera on 27/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "EvaluationController.h"
#import "ABAEvaluation.h"
#import "ABAEvaluationQuestion.h"
#import "ABAEvaluationOption.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "DataController.h"
#import "ProgressController.h"
#import "ABAEvaluationDataController.h"


@implementation EvaluationController

+ (void)evaluationQuestionAnsweredWithOption:(ABAEvaluationOption *)option completionBlock:(CompletionBlock)completionBlock
{
    [ABAEvaluationDataController setQuestionEvaluationDone:option
                              completionBlock:^(NSError *error, id object) {
                                  if (!error)
                                  {
                                      completionBlock(nil, nil);
                                  }
                                  else
                                  {
                                      completionBlock(error, nil);
                                  }
                              }];
}

+ (NSInteger)getEvaluationCorrectAnswers:(ABAEvaluation *)evaluation
{
    NSInteger correctAnswers = 0;
    for (ABAEvaluationQuestion *question in evaluation.content)
    {
        if ([question.answered boolValue])
        {
            ABAEvaluationOption *option;
            for (ABAEvaluationOption *optionLoop in question.options)
            {
                if ([optionLoop.selected boolValue])
                {
                    option = optionLoop;
                }
            }

            if ([option.isGood boolValue])
            {
                correctAnswers++;
            }
        }
    }
    return correctAnswers;
}

+ (NSOrderedSet *)getEvaluationAnswers:(ABAEvaluation *)evaluation
{
    NSMutableOrderedSet *answers = [NSMutableOrderedSet new];
    for (ABAEvaluationQuestion *question in evaluation.content)
    {
        if ([question.answered boolValue])
        {
            ABAEvaluationOption *option;
            for (ABAEvaluationOption *optionLoop in question.options)
            {
                if ([optionLoop.selected boolValue])
                {
                    option = optionLoop;
                }
            }

            [answers addObject:option];
        }
    }

    return answers.copy;
}

+ (void)reloadEvaluationAnswers:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock
{
    [ABAEvaluationDataController setAllQuestionEvaluationNone:evaluation
                                 completionBlock:^(NSError *error, id object) {
                                     if (!error)
                                     {
                                         completionBlock(nil, nil);
                                     }
                                     else
                                     {
                                         completionBlock(error, nil);
                                     }
                                 }];
}

+ (void)reloadWrongAnswers:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock
{
    [ABAEvaluationDataController setWrongQuestionEvaluationNone:evaluation
                                   completionBlock:^(NSError *error, id object) {
                                       if (!error)
                                       {
                                           completionBlock(nil, nil);
                                       }
                                       else
                                       {
                                           completionBlock(error, nil);
                                       }
                                   }];
}

- (void)endEvaluationWithOK:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock
{
    //First mark section as completed
    [self setCompletedSection:evaluation
              completionBlock:^(NSError *error, id object) {

                  if (!error)
                  {
                      [DataController saveProgressActionForSection:evaluation
                                                           andUnit:nil
                                                        andPhrases:nil
                                                         errorText:nil
                                                          isListen:NO
                                                            isHelp:NO
                                                   completionBlock:^(NSError *error, id object) {
                                                       
                                                       if (!error)
                                                       {
                                                           //Mark unit as completed
                                                           [LevelUnitController completeUnit:evaluation.unit
                                                                                     forUser:[UserController currentUser]
                                                                             completionBlock:^(NSError *error, id object) {
                                                                                 completionBlock(error, evaluation);
                                                                             }];
                                                       }
                                                   }];
                  }
              }];
}

- (void)endEvaluationWithKO:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock
{

    [DataController saveProgressActionForSection:evaluation
                                         andUnit:nil
                                      andPhrases:nil
                                       errorText:nil
                                        isListen:NO
                                          isHelp:NO
                                 completionBlock:^(NSError *error, id object) {
                                     if (!error)
                                     {
                                     }
                                     completionBlock(error, nil);
                                 }];
}

@end
