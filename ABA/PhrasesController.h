//
//  PhrasesController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@class ABAPhrase, ABASection, ABARealmProgressAction;

@interface PhrasesController : NSObject {
    
}

/**
 *  Set ABAPhrases done
 *
 *  @param phrases array   ABAPhrases
 *  @param write           ABAWrite
 *  @param completionBlock id, error
 */
- (void)setPhrasesDone: (NSArray *)phrases
            forSection: (ABASection *)section
    sendProgressUpdate: (BOOL) updateProgress
       completionBlock: (CompletionBlock)completionBlock;

- (void)setPhrasesListened:(NSArray *)phrases
                forSection:(ABASection *)section
        sendProgressUpdate:(BOOL)updateProgress
           completionBlock:(CompletionBlock)block;

- (void)setPhraseKO: (ABAPhrase*) phrase
         forSection: (ABASection*) section
          errorText: (NSString*) text
    completionBlock: (CompletionBlock) completionBlock;

@end
