//
//  String-custom.swift
//  ABA
//
//  Created by MBP13 Jesus on 26/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

extension String {
    public static func localize(_ key: String, comment: String) -> String {
        return NSLocalizedString(key, tableName: LanguageController.getCurrentLanguage(), comment: comment)
    }
}

extension String {
    public static func printFonts() {
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
}

//Roboto Slab
//    == RobotoSlab-Bold
//    == RobotoSlab-Light
//    == RobotoSlab-Regular
//    == RobotoSlab-Thin

//Roboto
//    == Roboto-Thin
//    == Roboto-Italic
//    == Roboto-BlackItalic
//    == Roboto-Light
//    == Roboto-BoldItalic
//    == Roboto-Black
//    == Roboto-LightItalic
//    == Roboto-ThinItalic
//    == Roboto-Bold
//    == Roboto-Regular
//    == Roboto-Medium
//    == Roboto-MediumItalic
