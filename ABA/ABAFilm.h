//
//  ABAFilm.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABASection+Methods.h"
#import <Realm/Realm.h>

@class ABAUnit;

@interface ABAFilm : ABASection

@property NSString *englishSubtitles;
@property NSString *hdVideoURL;
@property NSString *previewImageURL;
@property NSString *sdVideoURL;
@property NSString *translatedSubtitles;
@property (readonly) ABAUnit *unit;

-(BOOL) hasContent;

@end
