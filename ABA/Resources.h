//
//  Resources.h
//  ABA
//
//  Created by Oriol Vilaró on 4/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#ifndef ABA_Resources_h
#define ABA_Resources_h

/**
 *  Common macros
 */
#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]


/**
 *  Custom ABA colors
 */

#define ABA_COLOR_BLUE             RGB(  7, 188, 230)
#define ABA_COLOR_LIGHTBLUE        RGB(131, 218, 255)
#define ABA_COLOR_DARKBLUE         RGB(15 ,  81, 113)
#define ABA_COLOR_YELLOW           RGB(230, 180, 29)
#define ABA_COLOR_DARKGREY         RGB( 67,  82,  92)
#define ABA_COLOR_MIDGREY          RGB(146, 144, 139)
#define ABA_COLOR_LIGHTGREY        RGB(231, 231, 231)
#define ABA_COLOR_LIGHTGREY2       RGB(151, 171, 183)
#define ABA_COLOR_LIGHTGREY3       RGB(204, 204, 204)
#define ABA_COLOR_DARKGREY2        RGB( 72,  88,  99)
#define ABA_COLOR_DARKGREY3        RGB( 52,  64,  71)
#define ABA_COLOR_DARKGREY4        RGB(194, 191, 187)
#define ABA_COLOR_DARKGREY5        RGB(94, 89, 83)

#define ABA_COLOR_MAGENTA_PREMIUM  RGB(134,   0,  56)
#define ABA_COLOR_GREEN_FOREVER    RGB( 51, 140,  38)
#define ABA_COLOR_GREEN_COMPLETED  RGB(62, 187, 109)
#define ABA_COLOR_ORANGE_PLUS      RGB(222,  84,  51)

#define ABA_COLOR_INTRO_TITLE_BUT  RGB( 51,  56,  66)
#define ABA_COLOR_INTRO_LOG_BUT    RGB(231, 231, 231)

#define ABA_COLOR_REG_MAIL_BUT     RGB( 62, 187, 109)
#define ABA_COLOR_REG_FACEB_BUT    RGB( 60,  90, 152)

#define ABA_COLOR_REG_BACKGROUND   RGB(236, 235, 231)
#define ABA_COLOR_REG_BACKGROUND2  RGB(240, 239, 236)
#define ABA_COLOR_REG_BORDER       RGB(213, 213, 213)
#define ABA_COLOR_REG_TITLE_TEXT   RGB(71 ,  84,  93)
#define ABA_COLOR_REG_ALPHA        RGBA(71 ,  84,  93, 0.9)
#define ABA_COLOR_REG_TITLE_BTN    RGB(255, 255, 255)
#define ABA_COLOR_REG_TEXTFIELD    RGB(71 ,  84,  93)
#define ABA_COLOR_REG_SHADOW       RGB(227, 227, 223)
#define ABA_COLOR_RED_ERROR        RGB(215,  61, 50)
#define ABA_COLOR_RED_DARK         RGB(115,  9, 9)
#define ABA_COLOR_RED_RECORD       RGB(215,  61, 50)

#define ABA_COLOR_BLUE_MENU_UNIT   RGB(4  , 187, 228)
#define ABA_COLOR_GREEN_MENU_UNIT  RGB( 62, 187, 109)

#define ABA_COLOR_GREY_SUBS        RGB(189, 187, 183)
#define ABA_COLOR_GREY_SUBS2       RGB(174 , 177, 179)

#define ABA_COLOR_BLUE_WRITE_UNIT  RGB(10 , 175, 212)
#define ABA_COLOR_GREY_WRITE_UNIT  RGB(151, 151, 151)
#define ABA_COLOR_BLUE_SOUND       RGB(34 , 195, 231)
#define ABA_COLOR_BLUE2_SOUND      RGB(155, 228, 244)
#define ABA_COLOR_BLUE_INFO        RGB(7  , 188, 228)

#define ABA_COLOR_BLUE_SPEAK_TEXT  RGB(15 ,  81, 113)
#define ABA_COLOR_BLUE_SPEAK_NOSEL RGB(165, 181, 192)

#define ABA_COLOR_EXERC_BG         RGB(242 , 241, 238)
#define ABA_COLOR_EXERC_BORDER     RGB(223 , 222, 217)

#define ABA_COLOR_LEVEL_TITLE      RGB(22  , 175, 221)
/**
 *  Custom ABA fonts
 */

#define ABA_MUSEO_SANS_100         @"MuseoSans-100"
#define ABA_MUSEO_SANS_300         @"MuseoSans-300"
#define ABA_MUSEO_SANS_500         @"MuseoSans-500"
#define ABA_MUSEO_SANS_700         @"MuseoSans-700"
#define ABA_MUSEO_SANS_900         @"MuseoSans-900"

#define ABA_MUSEO_SLAB_300         @"MuseoSlab-300"
#define ABA_MUSEO_SLAB_500Italic   @"MuseoSlab-500Italic"
#define ABA_MUSEO_SLAB_500         @"MuseoSlab-500"
#define ABA_MUSEO_SLAB_700         @"MuseoSlab-700"
#define ABA_MUSEO_SLAB_900         @"MuseoSlab-900"

#define ABA_RALEWAY_BOLD           @"Raleway-Bold"
#define ABA_RALEWAY_REGULAR        @"Raleway"

#define ABA_DEFAULT_LEVEL_NUMBER 6
#define ABA_DEFAULT_UNIT_NUMBER 144
#define ABA_DEFAULT_UNITS_PER_LEVEL 24

#define KB_CHANGE_HEIGHT_IGNORE 88

#endif
