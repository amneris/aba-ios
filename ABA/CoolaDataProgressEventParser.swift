//
//  CoolaDataParser.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 10/03/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

struct ABACooladataEventParser {
 
    // Jesus: Commenting code since it's not in use anymore
    /*
    static func cooladataPropertiesFromAction(_ actionDetails: [String: AnyObject]) -> ABACooladataProgressActionEventProperties {
        
        var abaEventProperties: ABACooladataProgressActionEventProperties = ABACooladataProgressActionEventProperties();
        
        abaEventProperties.idSession = actionDetails["idSession"] as? String
        abaEventProperties.idSite = actionDetails["idSite"] as? String
        abaEventProperties.idUser = actionDetails["idUser"] as? String
        abaEventProperties.device = actionDetails["device"] as? String
        abaEventProperties.dateInit = actionDetails["dateInit"] as? String
        
        return abaEventProperties;
    }
 */
    
    static func cooladataPropertiesFromResponce(_ progressUpdateResponce: [Dictionary<String, NSObject>], userId: String?) -> ABACooladataProgressActionEventProperties
    {
        
        let filmProgress: Int = self.safeExtraction(progressUpdateResponce, key:"AbaFilmPct")
        let speakProgress: Int = self.safeExtraction(progressUpdateResponce, key:"SpeakPct")
        let writeProgress: Int = self.safeExtraction(progressUpdateResponce, key:"WritePct")
        let interpretProgress: Int = self.safeExtraction(progressUpdateResponce, key:"InterpretPct")
        let videoclassProgress: Int = self.safeExtraction(progressUpdateResponce, key:"VideoClassPct")
        let exercisesProgress: Int = self.safeExtraction(progressUpdateResponce, key:"ExercisesPct")
        let vocabularyProgress: Int = self.safeExtraction(progressUpdateResponce, key:"VocabularyPct")
        let evaluationProgress: Int = self.safeExtraction(progressUpdateResponce, key:"AssessmentPct")
        let total: Int = self.safeExtraction(progressUpdateResponce, key:"Total")
        let totalWithEval: Int = self.safeExtraction(progressUpdateResponce, key: "totalWithEval")
        let unit: String? = progressUpdateResponce[0]["IdUnit"] as? String
        let lastchange: String? = progressUpdateResponce[0]["lastchange"] as? String
        
        var abaEventProperties: ABACooladataProgressActionEventProperties = ABACooladataProgressActionEventProperties()
        
        // Safely setting userId
        if userId != nil {
            abaEventProperties.idUser = userId
        } else {
            let currentUser = UserController.currentUser()
            abaEventProperties.idUser = currentUser?.idUser
        }
        
        abaEventProperties.progressABAFilm = filmProgress
        abaEventProperties.progressSpeak = speakProgress
        abaEventProperties.progressWrite = writeProgress
        abaEventProperties.progressInterpret = interpretProgress
        abaEventProperties.progressVideoClass = videoclassProgress
        abaEventProperties.progressExcercises = exercisesProgress
        abaEventProperties.progressVocabulary = vocabularyProgress
        abaEventProperties.progressAssessment = evaluationProgress
        abaEventProperties.progressTotalPercent = totalWithEval
        abaEventProperties.progressTotalPercentExcludingEvaluation = total
        abaEventProperties.lastChange = lastchange
        abaEventProperties.idUnit = unit
        
        return abaEventProperties
    }
    
    static func safeExtraction(_ progressUpdate: [Dictionary<String, NSObject>], key: String) -> Int {
        guard let progress = progressUpdate[0][key] else { return 0 }
        
        if let _progress = progress as? String {
            return Int(_progress)!
        } else {
            return progress as! Int
        }
    }
}
