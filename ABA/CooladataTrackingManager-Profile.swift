//
//  CooladataTrackingManager-Profile.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/05/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

extension CoolaDataTrackingManager {

    class func trackUserRegistered(_ userId: String) {
        // Reading user properties
        let userProperties = readScopedUserProperties()

        // Adding user properties to navigation properties
        var registerProperties = createBasicProfileMap()
        userProperties.keys.forEach { key in
            registerProperties[key] = userProperties[key] as AnyObject?
        }
        registerProperties["user_id"] = userId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("USER_REGISTERED", properties: registerProperties)
    }

    class func trackLevelChange(_ userId: String, newLevelId: String) {
        var eventProperties = createBasicProfileMap()
        eventProperties["user_id"] = userId as AnyObject?
        eventProperties["user_level"] = newLevelId as AnyObject?
        CoolaDataTracker.getInstance().trackEvent("CHANGED_LEVEL", properties: eventProperties)
    }
}
