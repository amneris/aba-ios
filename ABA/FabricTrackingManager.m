//
//  FabricTrackingManager.m
//  ABA
//
//  Created by MBP13 Jesus on 28/07/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "FabricTrackingManager.h"
#import "TrackingManagerProtocol.h"
#import "Utils.h"

// Model
#import "ABARealmUser.h"
#import "ABAPlan+Methods.h"
#import "ABAPlan+GenericTrackingManager.h"
#import "ABASection+GenericTrackingManager.h"
#import "ABARealmUser+GenericTrackingManager.h"
#import "SKProduct+GenericTrackingManager.h"

// Controllers
#import "UserController.h"

// Using libraries
#import <Crashlytics/Crashlytics.h>

@implementation FabricTrackingManager

+ (id)sharedManager
{
    if (isTargetTest)
    {
        return nil;
    }

    static FabricTrackingManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

#pragma mark - Public methods (Analytics/Login & Register)

- (void)trackLoginEvent
{
    ABARealmUser * currentUser = [UserController currentUser];
    
    // User properties
    NSDictionary * dictionaryWithProperties = [currentUser trackingSuperProperties];
    
    [Answers logLoginWithMethod:@"Login" success:@YES customAttributes:dictionaryWithProperties];
}

- (void)trackFacebookLoginEvent
{
    ABARealmUser * currentUser = [UserController currentUser];
    
    // User properties
    NSDictionary * dictionaryWithProperties = [currentUser trackingSuperProperties];
    
    [Answers logLoginWithMethod:@"Login with Facebook" success:@YES customAttributes:dictionaryWithProperties];
}

- (void)trackLoginOfflineEvent
{
    [Answers logCustomEventWithName:@"Login offline" customAttributes:nil];
}

- (void)trackRegister:(RecordType)type
{
    ABARealmUser * currentUser = [UserController currentUser];
    
    // User properties
    NSDictionary * dictionaryWithProperties = [currentUser trackingSuperProperties];
    
    // Registration type property
    NSString * stringForRecordType = [self attributeForRecordType:type];
    
    [Answers logSignUpWithMethod:stringForRecordType success:@YES customAttributes:dictionaryWithProperties];
}

#pragma mark - Public methods (Analytics/Events)

- (void)trackLevelChange:(ABARealmLevel *)fromLevel toLevel:(ABARealmLevel *)toLevel
{
    NSDictionary * levelChangeDictionary = [self dictionaryWithLevelChange:fromLevel toLevel:toLevel];
    [Answers logCustomEventWithName:@"Level change" customAttributes:levelChangeDictionary];
}

- (void)trackPricingScreenArrivalWithOriginController:(PlansViewControllerOriginType)type
{
    NSString * attributeName = [self attributeForPlansViewControllerOriginType:type];
    [Answers logCustomEventWithName:@"Subscription shown" customAttributes:@{
                                                                             @"Source": attributeName
                                                                             }];
}

- (void)trackPricingSelected:(ABAPlan *)plan
{
    // Event for ABA purchase funnel
    NSDictionary * planProperties = [plan trackingProperties];
    [Answers logCustomEventWithName:@"Subscription selected" customAttributes:planProperties];
    
    NSString *currencyCode = [plan.productWithDiscount.priceLocale objectForKey:NSLocaleCurrencyCode];
    NSDictionary * productProperties = [plan.productWithDiscount trackingProperties];
    
    @try {
        // Events for Twitter Purchase funnel
        [Answers logAddToCartWithPrice:plan.productWithDiscount.price
                              currency:currencyCode
                              itemName:[ABAPlan getProductNameForTrackingPurposes:plan.productWithDiscount.productIdentifier]
                              itemType:nil
                                itemId:plan.productWithDiscount.productIdentifier
                      customAttributes:productProperties];
        
        [Answers logStartCheckoutWithPrice:plan.productWithDiscount.price
                                  currency:currencyCode
                                 itemCount:[NSNumber numberWithInteger:1]
                          customAttributes:productProperties];
    }
    @catch (NSException *exception) {
        // Do nothing
    }
    @finally {
        // Do nothing
    }
}

- (void)trackPurchase:(SKPaymentTransaction *)transaction withPeriod:(NSInteger)period withProduct:(SKProduct *)product
{
    NSString *currencyCode = [product.priceLocale objectForKey:NSLocaleCurrencyCode];
    NSDictionary * planProperties = [product trackingProperties];
    
    @try {
        // Event for ABA purchase funnel
        [Answers logCustomEventWithName:@"Subscription purchased" customAttributes:planProperties];
        
        // Event for Twitter Purchase funnel
        [Answers logPurchaseWithPrice:product.price
                             currency:currencyCode
                              success:@YES
                             itemName:[ABAPlan getProductNameForTrackingPurposes:product.productIdentifier]
                             itemType:nil
                               itemId:product.productIdentifier
                     customAttributes:planProperties];
    }
    @catch (NSException *exception) {
        // Do nothing
    }
    @finally {
        // Do nothing
    }
}

- (void)trackSection:(ABASection *)section withSecondsOfStudyTime:(double)secondsOfStudy withInitialProgress:(NSNumber *)initialProgress withFinalProgress:(NSNumber *)finalProgress
{
    // Under the threshold the section is not tracked
    if (secondsOfStudy < kSectionTimeThreshold)
    {
        return;
    }
    
    // Creating attribute disctionaries
    NSDictionary * attributesForSection = [section trackingProperties];
    NSDictionary * attributesForSectionProgress = [self dictionaryFromSecondsOfStudyTime:secondsOfStudy withInitialProgress:initialProgress withFinalProgress:finalProgress];
    
    // Merging dictionaries
    NSMutableDictionary * attributesToSend = [[NSMutableDictionary alloc] init];
    [attributesToSend addEntriesFromDictionary:attributesForSection];
    [attributesToSend addEntriesFromDictionary:attributesForSectionProgress];
    
    @try {
        [Answers logCustomEventWithName:@"Section" customAttributes:attributesToSend];
    }
    @catch (NSException *exception) {
        // Do nothing
    }
    @finally {
        // Do nothing
    }
}


@end
