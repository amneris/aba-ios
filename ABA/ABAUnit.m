//
//  ABAUnit.m
//  ABA
//
//  Created by Jordi Melé on 30/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ABAEvaluation.h"
#import "ABAExercises.h"
#import "ABAFilm.h"
#import "ABAInterpret.h"
#import "ABARealmLevel.h"
#import "ABARole.h"
#import "ABASpeak.h"
#import "ABAUnit.h"
#import "ABAVideoClass.h"
#import "ABAVocabulary.h"
#import "ABAWrite.h"
#import "ExperimentRunner.h"
#import "LevelUnitController.h"
#import "Utils.h"
#import <UIKit/UIKit.h>

@interface ABAUnit ()
@property(readonly) RLMLinkingObjects<ABARealmLevel *> *levels;
@end

@implementation ABAUnit

- (NSString *)getPercentage
{
    CGFloat progressInt = [self.progress floatValue];
    CGFloat percentage = (progressInt > 0) ? progressInt : 0.0f;

    NSString *percentageString = [NSString stringWithFormat:@"%ld%%", (long)percentage];

    return percentageString;
}

- (NSInteger)getIndex
{
    return [self.idUnit integerValue] - (([self.level.idLevel integerValue] - 1) * 24);
}

- (NSNumber *)getPreviousUnitId
{
    NSInteger index = [self.idUnit integerValue];
    if (index == 1)
    {
        return [NSNumber numberWithInteger:index];
    }

    NSNumber *previousUnitId = [NSNumber numberWithInteger:index - 1];
    return previousUnitId;
}

+ (kABASectionTypes)getCurrentSectionForUnit:(ABAUnit *)unit
{
    if (![unit.sectionFilm.completed boolValue])
    {
        return kABAFilmSectionType;
    }
    else if (![unit.sectionSpeak.completed boolValue])
    {
        return kABASpeakSectionType;
    }
    else if (![unit.sectionWrite.completed boolValue])
    {
        return kABAWriteSectionType;
    }
    else if (![unit.sectionInterpret.completed boolValue])
    {
        return kABAInterpretSectionType;
    }
    else if (![unit.sectionVideoClass.completed boolValue])
    {
        return kABAVideoClassSectionType;
    }
    else if (![unit.sectionExercises.completed boolValue])
    {
        return kABAExercisesSectionType;
    }
    else if (![unit.sectionVocabulary.completed boolValue])
    {
        return kABAVocabularySectionType;
    }
    else if (![unit.sectionEvaluation.completed boolValue])
    {
        return kABAAssessmentSectionType;
    }
    return kABAAssessmentSectionType;
}

+ (NSString *)getPercentageUnit:(ABAUnit *)unit
{

    CGFloat percentage = 0.0f;
    CGFloat increment = 11.1111111f;

    if ([unit.sectionFilm.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionSpeak.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionWrite.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionInterpret.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionVideoClass.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionExercises.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionVocabulary.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionExercises.completed boolValue])
    {
        percentage += increment;
    }
    if ([unit.sectionEvaluation.completed boolValue])
    {
        percentage += increment;
    }

    NSString *percentageString = [NSString stringWithFormat:@"%1.1f", percentage];

    NSArray *listItems = [percentageString componentsSeparatedByString:@"."];
    if ([listItems[1] isEqualToString:@"0"])
    {
        percentageString = [NSString stringWithFormat:@"%1.0f%%", percentage];
    }
    else
    {
        percentageString = [NSString stringWithFormat:@"%1.1f%%", percentage];
    }

    return percentageString;
}

+ (float)getProgressUnit:(ABAUnit *)unit
{

    CGFloat progress = 0.0f;
    CGFloat increment = 0.125f;

    if ([unit.sectionFilm.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionSpeak.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionWrite.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionInterpret.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionVideoClass.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionExercises.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionVocabulary.completed boolValue])
    {
        progress += increment;
    }
    if ([unit.sectionExercises.completed boolValue])
    {
        progress += increment;
    }

    return progress;
}

- (BOOL)shouldDownloadSectionData
{
    if ([self.sectionFilm hasContent] &&
        [self.sectionSpeak hasContent] &&
        [self.sectionWrite hasContent] &&
        [self.sectionVocabulary hasContent] &&
        [self.sectionVideoClass hasContent] &&
        [self.sectionEvaluation hasContent] &&
        [self.sectionInterpret hasContent] &&
        [self.sectionExercises hasContent])
    {
        return NO;
    }

    return YES;
}

- (BOOL)isEvaluationSectionUnlocked
{
    return [self.sectionFilm.completed boolValue] &&
           [self.sectionSpeak.completed boolValue] &&
           [self.sectionWrite.completed boolValue] &&
           [self.sectionInterpret.completed boolValue] &&
           [self.sectionVideoClass.completed boolValue] &&
           [self.sectionExercises.completed boolValue] &&
           [self.sectionVocabulary.completed boolValue];
}

- (ABASection *)getSectionForType:(kABASectionTypes)type
{
    switch (type)
    {
    case kABAFilmSectionType:
    {
        return self.sectionFilm;
        break;
    }
    case kABASpeakSectionType:
    {
        return self.sectionSpeak;
        break;
    }
    case kABAWriteSectionType:
    {
        return self.sectionWrite;
        break;
    }
    case kABAInterpretSectionType:
    {
        return self.sectionInterpret;
        break;
    }
    case kABAVideoClassSectionType:
    {
        return self.sectionVideoClass;
        break;
    }
    case kABAExercisesSectionType:
    {
        return self.sectionExercises;
        break;
    }
    case kABAVocabularySectionType:
    {
        return self.sectionVocabulary;
        break;
    }
    case kABAAssessmentSectionType:
    {
        return self.sectionEvaluation;
        break;
    }
    default:
    {
        break;
    }
    }

    return nil;
}

- (ABARealmLevel *)level
{
    return self.levels.firstObject;
}

- (NSString *)idUnitString
{
    return [NSString stringWithFormat:@"%ld", (long)[self.idUnit integerValue]];
}

#pragma mark - Realm overriding

+ (NSDictionary *)linkingObjectsProperties
{
    return @{
        @"levels" : [RLMPropertyDescriptor descriptorWithClass:ABARealmLevel.class propertyName:@"units"],
    };
}

@end
