//
//  ABAInterpretPhrase.m
//  ABA
//
//  Created by Marc Guell on 1/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAInterpret.h"
#import "ABAInterpretPhrase.h"
#import "ABAInterpretRole.h"

@interface ABAInterpretPhrase ()
@property (readonly) RLMLinkingObjects<ABAInterpret *> *dialogs;
@end

@implementation ABAInterpretPhrase

- (ABAInterpret *)interpret
{
    return self.dialogs.firstObject;
}

+ (NSDictionary *)linkingObjectsProperties
{
    return @{
        @"dialogs" : [RLMPropertyDescriptor descriptorWithClass:ABAInterpret.class propertyName:@"dialog"],
    };
}

@end
