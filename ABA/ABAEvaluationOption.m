//
//  ABAEvaluationOption.m
//  ABA
//
//  Created by Ivan Ferrera on 22/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAEvaluationOption.h"
#import "ABAEvaluationQuestion.h"

@interface ABAEvaluationOption ()
@property (readonly) RLMLinkingObjects<ABAEvaluationQuestion *> *evaluationQuestions;
@end

@implementation ABAEvaluationOption

- (ABAEvaluationQuestion *)question
{
    return self.evaluationQuestions.firstObject;
}

+ (NSDictionary *)linkingObjectsProperties
{
    return @{
        @"evaluationQuestions" : [RLMPropertyDescriptor descriptorWithClass:ABAEvaluationQuestion.class propertyName:@"options"],
    };
}

@end
