//
//  RegisterRepository.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import Moya

enum RegisterRepositoryError: Swift.Error, CustomStringConvertible {
    var description: String { return "RegisterRepositoryError.." }
    case emailAlreadyExists
    case invalidData
}

class RegisterRepository {

    internal let CODE_EMAIL_EXIST_ERROR = 444

    func registerWithEmail(name: String, email: String, password: String) -> Observable<UserDataModel> {
        return registerWithEmailAPICall(name: name, email: email, password: password)
            .do(onNext: { user in
                self.saveUser(user)
            })
    }

    func registerWithFacebook(facebookId id: String, name: String, surname: String, email: String, gender: String, avatar: String) -> Observable<UserDataModel> {
        return registerWithFacebookAPICall(facebookId: id, name: name, surname: surname, email: email, gender: gender, avatar: avatar)
            .do(onNext: { user in
                self.saveUser(user)
            })
    }
    
}

extension RegisterRepository: FacebookRegistrationRepository {
    // Adding FacebookRecordRepository methods
}

// MARK: Private methods

private extension RegisterRepository {

    func registerWithEmailAPICall(name: String, email: String, password: String) -> Observable<UserDataModel> {

        return PublicIpRepository.publicIpAPICall().flatMap { ip -> Observable<UserDataModel> in

            let parameters = RegistrationEmailParameters(deviceId: ABASystem.sharedInstance.deviceId, sysOper: ABASystem.sharedInstance.sysOper, deviceName: ABASystem.sharedInstance.deviceName, email: email, password: password, languageEnvironment: LanguageController.getCurrentLanguage(), name: name, surnames: "", ipMobile: ip, appVersion: ABASystem.sharedInstance.appVersion)

            return APIProvider
                .request(.registerWithEmail(registerParameters: parameters))
                .map { response -> Moya.Response in
                    switch response.statusCode {
                    case self.CODE_EMAIL_EXIST_ERROR:
                        throw RegisterRepositoryError.emailAlreadyExists
                    default:
                        break
                    }
                    return response
                }
                .mapJSON()
                .map { userDictionary -> UserDataModel in
                    if let user = try NetworkUserParser.parseNetworkUser(userDictionary)
                    {
                        user.name = name
                        user.email = email
                        return user as UserDataModel
                    } else if let _ = try NetworkErrorParser.parseNetworkError(userDictionary) {
                        throw LoginRepositoryError.networkErrorResponse
                    }

                    throw RegisterRepositoryError.invalidData
            }
        }
    }

    func saveUser(_ user: UserDataModel) {
        UserDataModelDAO.saveUser(user)
    }
}
