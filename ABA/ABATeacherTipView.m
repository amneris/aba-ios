//
//  ABAViewTeacher.m
//  ABA
//
//  Created by Jordi Mele on 7/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ABATeacherTipView.h"
#import "Resources.h"
#import "ImageHelper.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageHelper.h"
#import "Utils.h"
#import "PureLayout.h"

#import <SnapKit/SnapKit-umbrella.h>

@interface ABATeacherTipView ()

@property (strong, nonatomic) NSString *titleText;

@property (weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightSpaceConstraint;

@property BOOL shadow;

@end

@implementation ABATeacherTipView

- (id)initTeacherWithText:(NSString *)text withImage:(NSString *)imageeee withShadow: (BOOL)shadow
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ABATeacherTipView" owner:self options:nil] objectAtIndex:0];
    
    if (self)
    {
        _titleText = text;
        _shadow = shadow;
        
        //
        // Configure the view
        //
        UIColor *color = ABA_COLOR_REG_BACKGROUND;
        self.backgroundColor = [color colorWithAlphaComponent:0.75f];
        
        [_titleLabel setText:_titleText];
        _titleLabel.textColor = ABA_COLOR_DARKGREY;
        [_titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?18:14]];
        
        ABARealmUser *user = UserController.currentUser;
        
        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:user.teacherImage];
        NSURL *imageUrl = [NSURL URLWithString:urlString];
        
        [_teacherImage sd_setImageWithPreviousCachedImageWithURL:imageUrl placeholderImage:nil options:SDWebImageRetryFailed progress:nil completed:nil];
    }
    
    return  self;
}

- (void)setAttributedText:(NSMutableAttributedString *)attributedText
{
    [_titleLabel setAttributedText:attributedText];
}

- (void)setConstraints {
    //
    // Calculate view's height and set the frame
    //
    CGFloat labelWidth = [UIScreen mainScreen].bounds.size.width - 88;
    
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.textColor = ABA_COLOR_DARKGREY;
    gettingSizeLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?18:14];
    gettingSizeLabel.text = _titleText;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(labelWidth, 9999);
    CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    
    size.height += 16.0f; // Constraints top&bottom
    
    if(size.height < 90.0f)
    {
        size.height = 90.0f;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.superview];
    [self autoSetDimension:ALDimensionHeight toSize:size.height];
    [self autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [self autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    
    //	if(IS_IPAD) {
    //		_leftSpaceConstraint.constant = 50.0f;
    //		_rightSpaceConstraint.constant = 50.0f;
    //	}
    
    [self layoutIfNeeded];
    [_titleLabel layoutIfNeeded];
    
    if(_shadow)
    {
        CALayer *layer = self.layer;
        layer.shadowOffset = CGSizeMake(1, 1);
        layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        layer.shadowRadius = 10.0f;
        layer.shadowOpacity = 1.0f;
        
        CGRect shadowPath = CGRectMake(0, size.height - 6, MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height) + 20, 5);
        
        layer.shadowPath = [[UIBezierPath bezierPathWithRect:shadowPath] CGPath];
    }
}

- (void)setConstraintsAttributedText {
    CGFloat labelWidth = [UIScreen mainScreen].bounds.size.width - 88;
    
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.textColor = ABA_COLOR_DARKGREY;
    gettingSizeLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?18:14];
    [gettingSizeLabel setAttributedText:_titleLabel.attributedText];
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(labelWidth, 9999);
    CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    
    if(size.height < 70.0f)
    {
        size.height = 90.0f;
    }
    else
    {
        size.height += 20.0f;
    }
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.superview];
    [self autoSetDimension:ALDimensionHeight toSize:size.height];
    [self autoPinEdgeToSuperviewEdge:ALEdgeTop];
    [self autoPinEdgeToSuperviewEdge:ALEdgeLeft];
    [self layoutIfNeeded];
    
    //	if(IS_IPAD) {
    //		_leftSpaceConstraint.constant = 50.0f;
    //		_rightSpaceConstraint.constant = 50.0f;
    //	}
    
    if(_shadow)
    {
        CALayer *layer = self.layer;
        layer.shadowOffset = CGSizeMake(1, 1);
        layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        layer.shadowRadius = 10.0f;
        layer.shadowOpacity = 0.90f;
        
        CGRect shadowPath = CGRectMake(0, size.height - 6, MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height) + 20, 5);
        
        layer.shadowPath = [[UIBezierPath bezierPathWithRect:shadowPath] CGPath];
    }
}

- (void)dismiss
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        
        [weakSelf setAlpha:0.0];
        
    }
                     completion:^(BOOL finished)
     {
         [weakSelf removeFromSuperview];
     }];
}

@end
