//
//  UnitDetailInteractor.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

enum UnitDetailInteractorError: Swift.Error, CustomStringConvertible {
    var description: String { return "Interactor error" }
    // Thrown when there is an error from iTunes
    case getSectionsError
    case getProgressError
    case unitDownloadError
    case unitDownloadStopped
}


class UnitDetailInteractor {
    
    // Repo
    let sectionController = SectionController()
    let progressController = ProgressController()
    var downloadController: DownloadController?
}

extension UnitDetailInteractor : UnitDetailInteractorInput {
    
    func getSections(unitId: NSNumber) -> Observable<Void> {
        
        return Observable.create { observable -> Disposable in
            
            let unitWithId = LevelUnitController.getUnitWithID(unitId)
            self.sectionController.getAllSections(for: unitWithId) { (error, object) in
                if error == nil {
                    observable.onNext()
                }
                else {
                    observable.onError(UnitDetailInteractorError.getSectionsError)
                }
            }
            
            return Disposables.create()
        }
    }
    
    func getProgress(unitId: NSNumber) -> Observable<Void> {
        
        return Observable.create { observable -> Disposable in
            
            let unitWithId = LevelUnitController.getUnitWithID(unitId)
            let user = UserController.currentUser()
            self.progressController.getCompletedActions(from: user, on: unitWithId) { (error, object) in
                if error == nil {
                    observable.onNext()
                }
                else {
                    observable.onError(UnitDetailInteractorError.getProgressError)
                }
            }
            
            return Disposables.create()
        }
    }
    
    func downloadUnit(unitId: NSNumber) -> Observable<Float> {
        
        return Observable.create { observable -> Disposable in
            
            self.downloadController?.cancelDownload()
            
            let unitWithId = LevelUnitController.getUnitWithID(unitId)
            self.downloadController = DownloadController(unit: unitWithId) { (maybeError, progress, didFinish: Bool) in
                
                if let _ = maybeError {
                    if didFinish {
                        observable.onError(UnitDetailInteractorError.unitDownloadStopped)
                    }
                    else {
                        observable.onError(UnitDetailInteractorError.unitDownloadError)
                    }
                }
                else if didFinish {
                    observable.onCompleted()
                }
                else {
                    observable.onNext(progress)
                }
            }
            
            self.downloadController?.startDownload()
            
            return Disposables.create()
        }
    }
}
