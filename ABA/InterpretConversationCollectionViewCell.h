//
//  ListenInterpretCollectionViewCell.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@class ABAInterpretPhrase, ABAInterpretLabel, ABAInterpretRole;

@interface InterpretConversationCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *characterImage;
@property (weak, nonatomic) IBOutlet UILabel *characterName;
@property (weak, nonatomic) IBOutlet ABAInterpretLabel *dialogLabel;
@property kABAInterpretRowStatus status;
@property ABAInterpretRole *role;

-(void)setupWithABAInterpretPhrase: (ABAInterpretPhrase *)phrase
                      withCellType:(kABAInterpretRowType)type
                           andRole:(ABAInterpretRole *)role
                  andCurrentPhrase:(ABAInterpretPhrase *)currentPhrase;

@end
