//
//  VideoViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "Definitions.h"
#import "SectionProtocol.h"

@class ABASection;

@interface VideoViewController : SectionBaseViewController

@property (strong, nonatomic) ABASection *section;
@property kABASectionTypes currentSection;
@property (weak) id <SectionProtocol> delegate;

@end
