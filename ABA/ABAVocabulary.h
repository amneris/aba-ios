//
//  ABAVocabulary.h
//  ABA
//
//  Created by Jordi Mele on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASection+Methods.h"
#import "ABAVocabularyPhrase.h"

@class ABAUnit;

@interface ABAVocabulary : ABASection

@property (readonly) ABAUnit *unit;
@property RLMArray<ABAVocabularyPhrase *><ABAVocabularyPhrase> *content;

-(BOOL) hasContent;

@end