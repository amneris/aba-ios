//
//  InterpretController.m
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "InterpretController.h"
#import "ABAInterpretRole.h"
#import "ABAInterpret.h"
#import "ABAInterpretPhrase.h"
#import "Blocks.h"
#import "UserController.h"
#import "ProgressController.h"
#import "PhrasesController.h"
#import "NSArray+ABA.h"


@implementation InterpretController

-(ABAInterpretPhrase *) getCurrentPhraseForRole: (ABAInterpretRole *)role
                            forInterpretSection: (ABAInterpret *)interpret
{
    ABAInterpretPhrase *lastDonePhrase = [self getLastDonePhraseForRole:role forInterpretSection:interpret];
    
    if(lastDonePhrase == nil)
    {
        return [interpret.dialog objectAtIndex:0];
    }
    else
    {
		NSUInteger indexForPreviousPhrase = [interpret.dialog indexOfObject:lastDonePhrase];

		if(indexForPreviousPhrase < [interpret.dialog count] - 1) {
			indexForPreviousPhrase = indexForPreviousPhrase + 1;
		}
		
		return [interpret.dialog objectAtIndex:indexForPreviousPhrase];
    }
}

-(NSArray *)getDonePhrasesForRole: (ABAInterpretRole *)role
              forInterpretSection: (ABAInterpret *)interpret
{
    NSMutableArray *donePhrases = [[NSMutableArray alloc] init];
    
    for(ABAInterpretPhrase *phrase in interpret.dialog)
    {
        if([phrase.done isEqualToNumber:[NSNumber numberWithInt:1]] &&
           [phrase.role isEqualToObject:role])
        {
            [donePhrases addObject:phrase];
        }
    }
    
    return donePhrases;
}

-(NSArray*) getListenedPhrasesForRole: (ABAInterpretRole*) role
                  forInterpretSection: (ABAInterpret*) interpret {
    NSMutableArray *listenedPhrases = [[NSMutableArray alloc] init];
    
    for(ABAInterpretPhrase *phrase in interpret.dialog)
    {
        if([phrase.listened isEqualToNumber:[NSNumber numberWithInt:1]] &&
           [phrase.role isEqualToObject:role])
        {
            [listenedPhrases addObject:phrase];
        }
    }
    
    return listenedPhrases;
}

-(NSDictionary*) getPhrasesDictionaryForSection:(ABAInterpret *)section {
    NSMutableDictionary *phrasesDictionary = [NSMutableDictionary new];
    
    for(ABAInterpretPhrase *phrase in section.dialog)
    {
        [phrasesDictionary setObject:phrase forKey:phrase.audioFile];
    }
    
    return phrasesDictionary;
    
}


-(NSArray *)getPhrasesForRole: (ABAInterpretRole *)role
          forInterpretSection: (ABAInterpret *)interpret
{
    NSMutableArray *donePhrases = [[NSMutableArray alloc] init];
    
    for(ABAInterpretPhrase *phrase in interpret.dialog)
    {
        if([phrase.role isEqualToObject:role])
        {
            [donePhrases addObject:phrase];
        }
    }
    
    return donePhrases;
}


-(ABAInterpretPhrase *) getLastDonePhraseForRole: (ABAInterpretRole *)role
                             forInterpretSection: (ABAInterpret *)interpret

{
    ABAInterpretPhrase *returnedPhrase = nil;

    for(ABAInterpretPhrase *phrase in interpret.dialog)
    {
        if([phrase.done isEqualToNumber:[NSNumber numberWithInt:1]] &&
           [phrase.role isEqualToObject:role])
        {
            returnedPhrase = phrase;
        }
    }
    
    return returnedPhrase;
}

-(void)setPhraseDone: (ABAInterpretPhrase *)phrase
 forInterpretSection: (ABAInterpret *)interpret
     withCurrentRole: (ABAInterpretRole *)role
     completionBlock: (CompletionBlock)completionBlock
{
    PhrasesController *phrasesController = [[PhrasesController alloc] init];
    
    // Check if the phrase is the last phrase for the currentRole
    NSArray *phrasesForRole = [self getPhrasesForRole:role forInterpretSection:interpret];
    
    __block BOOL mustSetRoleDone = [phrasesForRole indexOfRealmObject:phrase] == [phrasesForRole count] -1;
    __weak typeof (self) weakSelf = self;

    [phrasesController setPhrasesDone:@[ phrase ] forSection:interpret sendProgressUpdate:YES completionBlock:^(NSError *error, id object) {
        if (error)
        {
            completionBlock(error, nil);
        }
        else if (mustSetRoleDone)
        {
            [weakSelf setRolesDone:@[ role ] forInterpretSection:interpret completionBlock:^(NSError *error, id object) {
                
                completionBlock(error, interpret);
            }];
        }
        else
        {
            completionBlock(nil, interpret);
        }
    }];
}

- (void)setRolesDone:(NSArray *)roles forInterpretSection:(ABAInterpret *)interpret completionBlock:(CompletionBlock)completionBlock
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;

    [realm beginWriteTransaction];

    for (ABAInterpretRole *role in roles)
    {
        role.completed = [NSNumber numberWithInt:1];
    }

    // If all roles are completed, we set the section completed.
    if ([self allRolesAreCompletedForInterpretSection:interpret])
    {
        interpret.completed = [NSNumber numberWithInt:1];
    }

    [realm commitWriteTransaction:&error];

    if (error)
    {
        completionBlock(error, nil);
        return;
    }
    else
    {
        completionBlock(nil, interpret);
    }
}

- (void)eraseProgressForRole:(ABAInterpretRole *)role forInterpretSection:(ABAInterpret *)interpret completionBlock:(CompletionBlock)completionBlock
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        
        role.completed = [NSNumber numberWithInt:0];
        
        if ([interpret.completed isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            interpret.completed = [NSNumber numberWithInt:0];
        }
        
        for (ABAInterpretPhrase *phrase in interpret.dialog)
        {
            if ([phrase.role isEqualToObject:role])
            {
                phrase.done = [NSNumber numberWithInt:0];
            }
        }
        
    } error:&error];

    if (!error)
    {
        completionBlock(nil, interpret);
    }
    else
    {
        completionBlock(error, nil);
    }
}

-(BOOL)allRolesAreCompletedForInterpretSection: (ABAInterpret *)interpret
{
    for(ABAInterpretRole *role in interpret.roles)
    {
        if([role.completed isEqualToNumber:[NSNumber numberWithInt:0]])
        {
            return NO;
        }
    }
    
    return YES;
}

-(CGFloat)getNumberOfRolesCompletedForInterpretSection: (ABAInterpret *)interpret
{
    CGFloat numberOfRolesCompleted = 0;
    
    for(ABAInterpretRole *role in interpret.roles)
    {
        if([role.completed isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            numberOfRolesCompleted++;
        }
    }
    
    return numberOfRolesCompleted;
}

-(CGFloat) getTotalElementsForSection: (ABASection*) section
{
    ABAInterpret *interpret = (ABAInterpret *)section;
    
    CGFloat total = 0;
    
    for(ABAInterpretRole *role in interpret.roles)
    {
        total += [[self getPhrasesForRole:role forInterpretSection:interpret] count];
    }
    
    return total;
}

-(CGFloat) getElementsCompletedForSection: (ABASection*) section
{
    ABAInterpret *interpret = (ABAInterpret *)section;
    
    CGFloat total = 0;
    
    for(ABAInterpretRole *role in interpret.roles)
    {
        total += [[self getDonePhrasesForRole:role forInterpretSection:interpret] count];
    }
    
    return total;
}

-(BOOL) isSectionCompleted: (ABASection*) section
{
    ABAInterpret *interpret = (ABAInterpret *)section;
    
    if([self getNumberOfRolesCompletedForInterpretSection:interpret] == [interpret.roles count])
    {
        return YES;
    }
    
    return NO;
}

- (void)syncCompletedActions:(NSArray *)actions withSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    NSMutableArray *listenedPhrasesArray = @[].mutableCopy;
    NSMutableArray *donePhrasesArray = @[].mutableCopy;
    __weak typeof(self) weakSelf = self;
    
    for (NSDictionary *action in actions)
    {
        ABAInterpretPhrase *phrase = [self phraseWithID:[action objectForKey:@"Audio"]
                                                andPage:[action objectForKey:@"Page"]
                                            onInterpret:(ABAInterpret *)section];

        if (phrase == nil)
        {
            NSLog(@"ACTION NOT FOUND: %@", action);
            continue;
        }

        if ([[action objectForKey:@"Action"] isEqualToString:@"LISTENED"])
        {
            if ([phrase.listened boolValue] == YES)
            {
                continue;
            }
            
            [listenedPhrasesArray addObject:phrase];
        }
        else
        {
            if ([phrase.done boolValue] == YES)
            {
                continue;
            }
            
            [donePhrasesArray addObject:phrase];
        }
    }

    if (listenedPhrasesArray.count)
    {
        [phrasesController setPhrasesListened:listenedPhrasesArray.copy
                                   forSection:section
                           sendProgressUpdate:NO
                              completionBlock:^(NSError *error, id object) {
                                  
                                  if (!error) {
                                      [weakSelf checkIfRolesAreDoneForSection:(ABAInterpret *)section];
                                  }
                              }];
    }

    if (donePhrasesArray.count)
    {
        [phrasesController setPhrasesDone:donePhrasesArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object) {
                              
                              if (!error) {
                                  
                                  [weakSelf checkIfRolesAreDoneForSection:(ABAInterpret *)section];
                                  
                                  if ([weakSelf isSectionCompleted:section] && (section.completed == nil || section.completed == [NSNumber numberWithBool:NO]))
                                  {
                                      [weakSelf setCompletedSection:section completionBlock:^(NSError *error, id object) {
                                          
                                      }];
                                  }
                              }
                          }];
    }
}

-(ABAInterpretPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onInterpret: (ABAInterpret*) interpret {
    
    for(ABAInterpretPhrase *phrase in interpret.dialog)
    {
        if([phrase.audioFile isEqualToString:audioID] && [phrase.page isEqualToString:page]) {
            return phrase;
        }
    }
    
    return nil;
}

- (void)checkIfRolesAreDoneForSection:(ABAInterpret *)interpret
{
    NSMutableArray *doneRolesArray = @[].mutableCopy;

    for (ABAInterpretRole *role in interpret.roles)
    {
        if ([role.completed boolValue] == YES)
        {
            NSLog(@"ROLE: %@ COMPLETED", role.name);
            continue;
        }

        NSArray *donePhrases = [self getDonePhrasesForRole:role forInterpretSection:interpret];
        NSArray *rolePhrases = [self getPhrasesForRole:role forInterpretSection:interpret];

        if ([donePhrases count] == [rolePhrases count])
        {
            [doneRolesArray addObject:role];
        }
    }

    if (doneRolesArray.count)
    {
        [self setRolesDone:doneRolesArray.copy
            forInterpretSection:interpret
                completionBlock:^(NSError *error, id object){

                }];
    }
}

-(NSArray *)getAllAudioIDsForInterpretSection: (ABAInterpret *)interpret
{
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    
    for(ABAInterpretPhrase *phrase in interpret.dialog)
    {
        [returnedArray addObject:phrase.audioFile];
    }
    
    return returnedArray;
}

-(void)sendAllPhrasesDoneForInterpretSection: (ABAInterpret *)interpret completionBlock:(CompletionBlock)block
{
	__block NSInteger numberOfRolesToComplete = [interpret.roles count];
	__block NSInteger numberOfRolesCompleted = 0;

	__weak typeof(self) weakSelf = self;
	
	for(ABAInterpretRole *role in interpret.roles)
	{
		NSArray *array = [self getPhrasesForRole:role forInterpretSection:interpret];
		
		__block NSInteger numberOfPhrasesToComplete = [array count];
		__block NSInteger numberOfPhrasesCompleted = 0;

		for(ABAInterpretPhrase *phrase in array)
		{
			[self setPhraseDone:phrase forInterpretSection:interpret withCurrentRole:role completionBlock:^(NSError *error, id object)
			{
				if(!error)
				{
					numberOfPhrasesCompleted++;
					
					if(numberOfPhrasesCompleted == numberOfPhrasesToComplete)
					{
						[weakSelf setRolesDone:@[role] forInterpretSection:interpret completionBlock:^(NSError *error, id object)
						{
							if(!error)
							{
								numberOfRolesCompleted++;
								
								if(numberOfRolesCompleted == numberOfRolesToComplete)
								{
                                    block(nil, interpret);
									NSLog(@"Finished");
								}
							}
							else
							{
                                block(error,nil);
								NSLog(@"Error 2");
							}
						}];
                    }
				}
				else
				{
                    block(error,nil);
					NSLog(@"Error 1");
				}
			}];
		}
	}
}

@end
