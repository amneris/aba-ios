//
//  SectionController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"
#import "Definitions.h"
#import <CoreGraphics/CoreGraphics.h>

#define kNumberOfSections 8

@class ABAUnit, DataController, UserController;

typedef void(^SectionContentDownloadBlock)(BOOL completed, NSError *error, ABASection* lastDownloaded, ABAUnit* unit);

@interface SectionController : NSObject

-(void) getAllSectionsForUnit: (ABAUnit*) unit contentBlock: (CompletionBlock) block;

-(CGFloat) getTotalElementsForSection: (ABASection*) section;

-(CGFloat) getElementsCompletedForSection: (ABASection*) section;

-(BOOL) isSectionCompleted: (ABASection*) section;

/**
 *  Get the current section percentage in string format with %
 *
 *  @param section         ABASection object
 *  @return string with formatted percentage
 */
-(NSString *) getPercentageForSection:(ABASection *)section;

/**
 *  Get the current section percentage in float format
 *
 *  @param section         ABASection object
 *  @return string with formatted percentage
 */
-(float) getProgressForSection:(ABASection *)section;

/**
 Sets the section completed
 
 @param section  the section marked to be completed
 */
-(void) setCompletedSection: (ABASection*) section completionBlock: (CompletionBlock) completionBlock;


-(void) syncCompletedActions: (NSArray*) actions
                 withSection: (ABASection*) section;


@end
