//
//  ABASpeakPhrase.h
//  ABA
//
//  Created by Jordi Mele on 9/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAPhrase.h"

RLM_ARRAY_TYPE(ABASpeakPhrase)

@class ABASpeakPhrase;

@interface ABASpeakPhrase : ABAPhrase

@property RLMArray<ABASpeakPhrase *><ABASpeakPhrase> *subPhrases;

@end



