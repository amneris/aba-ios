//
//  ABAPlanResponseSerializer.h
//  ABA
//
//  Created by MBP13 Jesus on 08/01/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface ABAPlanResponseSerializer : AFJSONResponseSerializer

@end
