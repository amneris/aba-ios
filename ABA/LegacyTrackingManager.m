//
//  TrackingManager.m
//  ABA
//
//  Created by Ivan Ferrera on 27/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ABAPlan+Methods.h"
#import "ABARealmUser.h"
#import "APIManager.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LanguageController.h"
#import "LegacyTrackingManager.h"
#import "UserController.h"
#import "Utils.h"
#import <Adjust/Adjust.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>

@implementation LegacyTrackingManager

+ (id)sharedManager
{
    if (isTargetTest)
    {
        return nil;
    }

    static LegacyTrackingManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init])
    {
        _adJustEvents = @{
            @"login" : @"wj5p0v",
            @"registration" : @"1nenx0",
            @"purchase" : @"p1j2qo",
            @"interaction" : @"sal1qt"
        };

        [self initAdjust];
        [self initGoogleAnalytics];
    }
    return self;
}

- (void)initAdjust
{
    ADJConfig *adjustConfig;
#ifdef DEBUG
    adjustConfig = [ADJConfig configWithAppToken:@"242jv83nqguv" environment:ADJEnvironmentSandbox];
    //    [adjustConfig setLogLevel:ADJLogLevelVerbose];
    [adjustConfig setLogLevel:ADJLogLevelError];
#else
    adjustConfig = [ADJConfig configWithAppToken:@"242jv83nqguv" environment:ADJEnvironmentProduction];
#endif

    [Adjust appDidLaunch:adjustConfig];
}

- (void)initGoogleAnalytics
{
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);

    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;
#ifdef DEBUG
    [[gai logger] setLogLevel:kGAILogLevelVerbose];
#else
    [[gai logger] setLogLevel:kGAILogLevelNone];
#endif
}

#pragma public methods

- (void)trackPageView:(NSString *)pageView
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:pageView];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)trackTransaction:(SKPaymentTransaction *)transaction withPeriod:(NSInteger)period withProduct:(SKProduct *)product
{
    NSString *eventName = @"purchase";
    NSString *eventLabel;
    if (period == 30)
    {
        eventLabel = @"compra 1 mes";
    }
    else if (period == 180)
    {
        eventLabel = @"compra 6 meses";
    }
    else if (period == 360)
    {
        eventLabel = @"compra 12 meses";
    }

    ADJEvent *event = [ADJEvent eventWithEventToken:[_adJustEvents objectForKey:eventName]];
    [event setRevenue:product.price.doubleValue currency:[product.priceLocale objectForKey:NSLocaleCurrencyCode]];
    [event setTransactionId:transaction.transactionIdentifier];
    [Adjust trackEvent:event];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

    [tracker send:[[GAIDictionaryBuilder createTransactionWithId:transaction.transactionIdentifier
                                                     affiliation:@"In-app Store"
                                                         revenue:product.price
                                                             tax:@0
                                                        shipping:@0
                                                    currencyCode:[product.priceLocale objectForKey:NSLocaleCurrencyCode]] build]];

    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:transaction.transactionIdentifier
                                                                name:[ABAPlan getSubscriptionStringFromDays:period]
                                                                 sku:product.productIdentifier
                                                            category:@""
                                                               price:product.price
                                                            quantity:@1
                                                        currencyCode:[product.priceLocale objectForKey:NSLocaleCurrencyCode]] build]];

    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"payments"
                                                          action:eventName
                                                           label:eventLabel
                                                           value:nil] build]];
}

- (void)trackLogin
{
    [self trackEvent:@"login" withCategory:@"login" withLabel:@"inicio de sesion satisfactorio"];
}

- (void)trackRegister
{
    [self trackEvent:@"registration" withCategory:@"register" withLabel:@"registro realizado correctamente"];
}

- (void)trackInteraction
{
    [self trackEvent:@"interaction" withCategory:@"interaction" withLabel:@"interacción de actividad en app - section"];
}

- (void)trackAlertErrorWithText:(NSString *)errorShown andDelay:(CGFloat)delay withButtonText:(NSString *)firstButtonText withButtonText:(NSString *)secondButtonText
{
    // Only tracking through Appsee
    NSMutableDictionary *errorProperties = [NSMutableDictionary new];
    if (errorShown)
    {
        [errorProperties setObject:errorShown forKey:@"Description"];
    }

    if (firstButtonText)
    {
        [errorProperties setObject:firstButtonText forKey:@"Button 1"];
    }

    if (secondButtonText)
    {
        [errorProperties setObject:secondButtonText forKey:@"Button 2"];
    }

    [errorProperties setObject:[NSNumber numberWithFloat:delay] forKey:@"Delay"];
}

#pragma mark - FacebookParserTracker

- (void)trackFacebookParsingError {
    dispatch_async(dispatch_get_main_queue(), ^{
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"register"
                                                              action:@"Facebook parsing error"
                                                               label:@"Error al parsear la información de Facebook"
                                                               value:nil] build]];
    });
}

- (void)trackFacebookEmailError
{
    dispatch_async(dispatch_get_main_queue(), ^{
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"register"
                                                              action:@"Facebook email error"
                                                               label:@"El usuario no disponía de correo electrónico en Facebook"
                                                               value:nil] build]];
    });
}

- (void)trackFacebookEmailSuccess
{
    dispatch_async(dispatch_get_main_queue(), ^{
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"register"
                                                              action:@"Facebook email success"
                                                               label:@"Email correctamente obtenido de Facebook"
                                                               value:nil] build]];
    });
}

#pragma mark - Public methods - Google Analytics

- (void)startSession
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
    [builder set:@"start" forKey:kGAISessionControl];
    [tracker send:[builder build]];
}

- (void)stopSession
{
    GAIDictionaryBuilder *builder = [GAIDictionaryBuilder createScreenView];
    [builder set:@"end" forKey:kGAISessionControl];
}

#pragma mark - Private methods

- (void)trackEvent:(NSString *)event withCategory:(NSString *)category withLabel:(NSString *)label
{
    ADJEvent *adjustEvent = [ADJEvent eventWithEventToken:[_adJustEvents objectForKey:event]];
    if ([event isEqualToString:@"registration"])
    {
        ABARealmUser *currentUser = [UserController currentUser];

        [adjustEvent addCallbackParameter:@"user_id" value:currentUser.idUser];
    }
    [Adjust trackEvent:adjustEvent];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:event
                                                           label:label
                                                           value:nil] build]];
    [FBSDKAppEvents logEvent:event];
}

@end
