//
//  SupportedVersionManager.swift
//  ABA
//
//  Created by Oriol Vilaró on 26/1/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

class SupportedVersionManager: NSObject {
    
    var lastSupportedVersion: String {
        get {
            guard let version = UserDefaults.standard.object(forKey: kLastSupportedVersion) as? String else {
                return kUnknownVersion
            }
            
            return version
        }
        set {
            UserDefaults.standard.set(newValue, forKey: kLastSupportedVersion)
            UserDefaults.standard.synchronize()
        }
    }
    
    var currentVersion: String {
        get {
            guard let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String else {
                return kUnknownVersion
            }
            return version
        }
    }
    
    // MARK: Public methods
    
    func isCurrentVersionValid(_ completion: @escaping (Bool) -> Void) {
        self.fetchSupportedVersionNumber { supportedVersion in
            completion(self.checkCurrentVersionSupport(self.currentVersion, lastSupportedVersion: supportedVersion))
        }
    }
    
    func isCurrentVersionSupported() -> Bool {
        return isCurrentVersionSupported(currentVersion)
    }
    
    // MARK: Private methods
    
    func isCurrentVersionSupported(_ currentVersion: String) -> Bool {
        return checkCurrentVersionSupport(currentVersion, lastSupportedVersion: self.lastSupportedVersion )
    }
    
    func checkVersionIntegrity(_ version: String) -> Bool {
        let myRegex = "^(\\d+\\.)*(\\d+)$"
        if let _ = version.range(of: myRegex, options: .regularExpression) {
            return true
        }
        return false
    }
    
    func parseSupportedVersionNumber(_ object: Any) -> String {
        if let responseDict = object as? Dictionary<String, AnyObject> {
            if let iosVersion = responseDict["ios"] as? String {
                if self.checkVersionIntegrity(iosVersion) {
                    self.lastSupportedVersion = iosVersion
                    return iosVersion
                }
            }
        }
        return self.lastSupportedVersion;
    }
    
    func checkCurrentVersionSupport(_ currentVersion: String, lastSupportedVersion: String) -> Bool {
        return !currentVersion.versionToInt().lexicographicallyPrecedes(lastSupportedVersion.versionToInt())
    }
    
    // MARK: API Methods
    
    func fetchSupportedVersionNumber(_ completion: @escaping (String) -> Void) {
        let manager = APIManager.shared()
        manager?.getCurrentValidAppVersion { error, object in
            guard error == nil else { completion(self.lastSupportedVersion); return }
            
            completion(self.parseSupportedVersionNumber(object as AnyObject))
        }
    }
}

extension String {
    func versionToInt() -> [Int] {
        return self.components(separatedBy: ".").map { Int.init($0) ?? 0 }
    }
}
