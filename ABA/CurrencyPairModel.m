//
//  CurrencyRate.m
//  ABA
//
//  Created by MBP13 Jesus on 10/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "CurrencyPairModel.h"

#define kUnknownCurrencyPair @"N/A"
#define kCurrencyPairCodeCodingKey @"currencyPairCode"
#define kCurrencyRateCodingKey @"currencyRate"

@implementation CurrencyPairModel

#pragma mark - Public methods

- (BOOL)isProperlySet
{
    if (_currencyPairCode.length == 0)
    {
        return NO;
    }

    if (_formerCurrencyCode.length == 0)
    {
        return NO;
    }
    
    if (_latterCurrencyCode.length == 0)
    {
        return NO;
    }
    
    return YES;
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_currencyPairCode forKey:kCurrencyPairCodeCodingKey];
    [aCoder encodeObject:_currencyRate forKey:kCurrencyRateCodingKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.currencyPairCode = [aDecoder decodeObjectForKey:kCurrencyPairCodeCodingKey];
        self.currencyRate = [aDecoder decodeObjectForKey:kCurrencyRateCodingKey];
    }
    return self;
}

#pragma mark - Accessors

- (void)setCurrencyPairCode:(NSString *)currencyPairName
{
    if ([currencyPairName isEqualToString:kUnknownCurrencyPair])
    {
        return;
    }
    else
    {
        // Storing currency pair
        _currencyPairCode = currencyPairName;
        
        // Trying to split the pair
        NSRange rangeOfSeparator = [currencyPairName rangeOfString:@"/"];
        if (rangeOfSeparator.location != NSNotFound)
        {
            // Taking first currency code
            NSInteger indexOfSeparator = rangeOfSeparator.location;
            _formerCurrencyCode = [currencyPairName substringToIndex:indexOfSeparator];
            
            // Ensuring there is a secondary currency code
            if (currencyPairName.length >= indexOfSeparator + 1)
            {
                _latterCurrencyCode = [currencyPairName substringFromIndex:indexOfSeparator + 1];
            }
        }
    }
}

@end
