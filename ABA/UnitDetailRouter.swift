//
//  UnitDetailRouter.swift
//  ABA
//
//  Created by MBP13 Jesus on 06/03/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import UIKit

class UnitDetailRouter {
    
    weak var viewController: UIViewController?
    internal let userId: String
    init(vc: UIViewController, userId: String) {
        viewController = vc
        self.userId = userId
    }
}

extension UnitDetailRouter: UnitDetailRouterInput {
    
    func loadSection(section: kABASectionTypes, unitId: NSNumber?, sectionDelegate: SectionProtocol) {
        
        guard let abaUnit = LevelUnitController.getUnitWithID(unitId) else {
            return
        }
        
        switch section {
        case kABASectionTypes.abaFilmSectionType:
            
            let videoViewController = UIStoryboard(name: "Unit", bundle: nil).instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
            videoViewController.section = abaUnit.sectionFilm
            videoViewController.currentSection = section
            videoViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(videoViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaFilm)
            
        case kABASectionTypes.abaSpeakSectionType:
            
            let speakViewController = UIStoryboard(name: "Speak", bundle: nil).instantiateInitialViewController() as! SpeakViewController
            speakViewController.section = abaUnit.sectionSpeak
            speakViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(speakViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaSpeak)
            
        case kABASectionTypes.abaWriteSectionType:
            
            let writeViewController = UIStoryboard(name: "Write", bundle: nil).instantiateInitialViewController() as! WriteViewController
            writeViewController.section = abaUnit.sectionWrite
            writeViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(writeViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaWrite)
            
        case kABASectionTypes.abaInterpretSectionType:
            
            let interpretViewController = UIStoryboard(name: "Interpret", bundle: nil).instantiateInitialViewController() as! InterpretViewController
            interpretViewController.section = abaUnit.sectionInterpret
            interpretViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(interpretViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaInterpret)
            
        case kABASectionTypes.abaVideoClassSectionType:
            
            let videoclassViewController = UIStoryboard(name: "Unit", bundle: nil).instantiateViewController(withIdentifier: "VideoViewController") as! VideoViewController
            videoclassViewController.section = abaUnit.sectionVideoClass
            videoclassViewController.currentSection = section
            videoclassViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(videoclassViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaVideoClass)
            
        case kABASectionTypes.abaExercisesSectionType:
            
            let exercisesViewController = UIStoryboard(name: "Exercices", bundle: nil).instantiateInitialViewController() as! ExercicesViewController
            exercisesViewController.section = abaUnit.sectionExercises
            exercisesViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(exercisesViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaExercises)
            
        case kABASectionTypes.abaVocabularySectionType:
            
            let vocabulatyViewController = UIStoryboard(name: "Vocabulary", bundle: nil).instantiateInitialViewController() as! VocabularyViewController
            vocabulatyViewController.section = abaUnit.sectionVocabulary
            vocabulatyViewController.delegate = sectionDelegate
            
            self.viewController?.navigationController?.pushViewController(vocabulatyViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaVocabulary)

        case kABASectionTypes.abaAssessmentSectionType:
            
            let evaluationViewController = UIStoryboard(name: "Evaluation", bundle: nil).instantiateInitialViewController() as! EvaluationViewController
            evaluationViewController.section = abaUnit.sectionEvaluation
            
            self.viewController?.navigationController?.pushViewController(evaluationViewController, animated: true)
            
            CoolaDataTrackingManager.trackEnteredSection(userId, levelId: abaUnit.level().idLevel, unitId: abaUnit.idUnitString, sectionType: .abaEvaluation)
            
        default:
            break
        }
    }
    
    func loadPlans() {
        
        let plansViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InitialPlansViewController") as! InitialPlansViewController
        plansViewController.backButton = true
        
        self.viewController?.navigationController?.pushViewController(plansViewController, animated: true)
    }
}
