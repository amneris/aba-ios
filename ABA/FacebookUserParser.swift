//
//  FacebookUserParser.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

enum FacebookUserParserError : Swift.Error, CustomStringConvertible {
    var description: String { return "Facebook UserParser Error.." }
    case facebookEmailNotGranted
}

class FacebookUserParser {
    
    static func parseFacebookUser(_ facebookUserDictionary: [String: AnyObject?], imageDataResponse: [String: AnyObject?]?) throws -> FacebookUser {
        
        guard facebookUserDictionary["email"] != nil else {
            throw FacebookUserParserError.facebookEmailNotGranted
        }
        
        let facebookId = facebookUserDictionary["id"] as! String
        let email = facebookUserDictionary["email"] as! String
        var firstName = facebookUserDictionary["first_name"] as? String ?? ""
        var lastName = facebookUserDictionary["last_name"] as? String ?? ""
        let gender = facebookUserDictionary["gender"] as? String ?? ""
        var imageUrl: String? = ""
        
        if let imageDictionary = imageDataResponse?["data"] as? NSDictionary {
            if let url = imageDictionary["url"] as? String {
                imageUrl = url
            }
        }
        
        // Making sure the first name is always present
        if firstName.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty {
            
            // Both first name and last name are empty
            if lastName.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty  {
                firstName = "Unknown name"
            }
            else {
                // Just last name is empty
                firstName = lastName
                lastName = ""
            }
        }

        return FacebookUser(facebookId: facebookId, email: email, firstName: firstName, lastName: lastName, gender: gender, imageUrl: imageUrl!)
    }
}
