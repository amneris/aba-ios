//
//  MixpanelTrakingManagerProtocol.h
//  ABA
//
//  Created by Jesus Espejo on 19/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

@protocol ABATrackingManagerProtocol <NSObject>

- (NSDictionary *)trackingProperties;

@optional

- (NSDictionary *)trackingSuperProperties;

@end



