//
//  WriteController.h
//  ABA
//
//  Created by Jordi Mele on 2/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"
#import "PhrasesController.h"
#import "Blocks.h"
#import <UIKit/UIKit.h>

@class ABAPhrase, ABAWrite, ProgressAction;

@interface WriteController : SectionController

/**
 *  Get number of ABAPhrase completed
 *
 *  @param write           ABAWrite object
 *  @return                NSInteger
 */
-(NSInteger) getTotalPhraseCompletedForWriteSection: (ABAWrite *)write;


-(NSArray*) getPhrasesDataSourceFromSection: (ABAWrite*) writeSection;

-(NSDictionary*) getPhrasesDictionary: (ABAWrite*) writeSection;

-(NSArray *)getAllAudioIDsForWriteSection: (ABAWrite *)write;

-(void)sendAllPhrasesDoneForWriteSection: (ABAWrite *)write completionBlock:(CompletionBlock)block;

@end
