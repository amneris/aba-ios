//
//  CurrencyManager.h
//  ABA
//
//  Created by MBP13 Jesus on 10/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyManager : NSObject

+ (double)convertAmount:(double)amountToConverse toUSDollarFrom:(NSString *)currencyCode;
+ (double)convertAmount:(double)amountToConverse toEurFrom:(NSString *)currencyCode;
+ (void)updateCurrencies;
+ (double)roundValue:(double)value;

@end
