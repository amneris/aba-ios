//
//  FilmController.h
//  ABA
//
//  Created by Jaume Cornadó Panadés on 19/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"
#import "Definitions.h"

@interface FilmController : NSObject

- (void)setCompletedSection:(ABASection *)section completionBlock:(CompletionBlock)completionBlock;

-(void) downloadVideo:(ABASection *)section forSectionType:(kABASectionTypes)sectionType completionBlock:(CompletionBlock)completionBlock;

-(BOOL) checkIfVideoExists:(ABASection *)section forSectionType:(kABASectionTypes)sectionType;

-(BOOL)checkIfSubtitleExists:(ABASection *)section forSectionType:(kABASectionTypes)sectionType forSubtitle:(kABAVideoSubtitleType) subtitleType;

-(NSString*) getLocalVideoPath:(ABASection *)section forSectionType:(kABASectionTypes)sectionType;

-(void) downloadSubtitlesVideo:(ABASection *)section forSectionType:(kABASectionTypes)sectionType completionBlock:(CompletionBlock)completionBlock;

-(NSString*) getLocalSubtitlePath:(ABASection *)section forSectionType:(kABASectionTypes)sectionType forSubtitle:(kABAVideoSubtitleType) subtitleType;

-(BOOL) videoAndSubtituleDownloaded:(ABASection *)section forSectionType:(kABASectionTypes)sectionType;

@end
