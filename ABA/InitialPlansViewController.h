//
//  InitialPlansViewController.h
//  ABA
//
//  Created by Jordi Melé on 6/2/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InitialPlansViewController : UIViewController

@property BOOL backButton;

- (IBAction)goPlansButtonAction:(id)sender;

@end
