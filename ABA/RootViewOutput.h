//
//  RootViewOutput.h
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RootViewOutput <NSObject>

- (void)appDidFinishLaunching:(UIApplication *)application options:(NSDictionary *)options;
- (void)appDidReceivePush:(NSDictionary *)userinfo;
- (void)startApp;
- (void)loadMainStoryboard;
- (void)loadStartStoryboard;
- (void)showBlockView;
- (void)loadLevelSelectionViewController;

@end