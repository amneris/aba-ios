//
//  ABAExercisesPhrase.m
//  ABA
//
//  Created by Marc Güell Segarra on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAExercisesPhrase.h"
#import "ABAExercisesQuestion.h"

@interface ABAExercisesPhrase ()
@property (readonly) RLMLinkingObjects<ABAExercisesQuestion *> *questions;
@end

@implementation ABAExercisesPhrase

- (ABAExercisesQuestion *)exercisesQuestion
{
    return self.questions.firstObject;
}

+ (NSDictionary *)linkingObjectsProperties
{
    return @{
        @"questions" : [RLMPropertyDescriptor descriptorWithClass:ABAExercisesQuestion.class propertyName:@"phrases"],
    };
}

@end
