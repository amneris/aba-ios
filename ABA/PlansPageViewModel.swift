//
//  PlansPageViewModel.swift
//  ABA
//
//  Created by MBP13 Jesus on 16/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

@objc
class PlansPageViewModel: NSObject, PlansPageProducts {
    var text: String?
    var products: [PlansPageSubscriptionProduct]?
    
    init(text:String, products: [PlansPageSubscriptionProduct]) {
        self.text = text
        self.products = products
    }
}

