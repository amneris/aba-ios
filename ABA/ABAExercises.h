//
//  ABAExercises.h
//  ABA
//
//  Created by Marc Güell Segarra on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAExercisesGroup.h"
#import "ABASection+Methods.h"

@class ABAUnit;

@interface ABAExercises : ABASection

@property (readonly) ABAUnit* unit;
@property RLMArray<ABAExercisesGroup *><ABAExercisesGroup> *exercisesGroups;

-(BOOL) hasContent;

@end