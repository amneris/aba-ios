//
//  UnitDetailViewOutput.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

protocol UnitDetailViewOutput {
    // Sections and progress
    func getSectionsAndProgress(backgroundLoad: Bool)
    
    // Unit download
    func downloadUnit()
    func cancelDownload()
    
    // Actions
    func actionForSectionPushed(section: kABASectionTypes)
    func ctaPushed()
    func screenRendered()
}
