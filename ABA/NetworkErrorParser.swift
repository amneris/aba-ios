//
//  NetworkErrorParser.swift
//  ABA
//
//  Created by MBP13 Jesus on 10/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Unbox

enum NetworkErrorParserError: Swift.Error, CustomStringConvertible {
    public var description: String { return "NetworkErrorParserError.." }
    case invalidData
}

struct NetworkErrorParser {
    static func parseNetworkError(_ unboxableDict: Any) throws -> NetworkError? {
        if let unboxableDict = unboxableDict as? UnboxableDictionary {
            do {
                let parsedError = try Unboxer.performCustomUnboxing(dictionary: unboxableDict, closure: { unboxer -> NetworkError? in
                    return try NetworkError(unboxer: unboxer)
                })
                return parsedError
            } catch {
                return nil
            }
        }

        throw NetworkErrorParserError.invalidData
    }
}
