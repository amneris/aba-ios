//
//  ProfileStyle.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 14/12/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import BonMot


extension UIColor {
    convenience init(hex: UInt32, alpha: CGFloat = 1) {
        self.init(
            red: CGFloat((hex >> 16) & 0xff) / 255.0,
            green: CGFloat((hex >> 8) & 0xff) / 255.0,
            blue: CGFloat(hex & 0xff) / 255.0,
            alpha: alpha)
    }
    
}

struct ProfileStyle {
    static let sectionTitleStyle = StringStyle(
        .color(UIColor(hex: 0x8e8e92)),
        .font(UIFont(name: "Roboto-Bold", size: 16)!)
    )
    
    static let labelStyle = StringStyle(
        .color(UIColor(hex: 0x42505a)),
        .font(UIFont(name: "Roboto-Regular", size: 16)!)
    )
    
    static let logoutButtonStyle = StringStyle(
        .color(UIColor(hex: 0xea4041)),
        .font(UIFont(name: "Roboto-Regular", size: 16)!)
    )
    
    static let descriptionStyle = StringStyle(
        .color(UIColor(hex: 0x8e8e92)),
        .font(UIFont(name: "Roboto-Regular", size: 14)!)
    )
    
    static let subDescriptionStyle = StringStyle(
        .color(UIColor(hex: 0x8e8e92)),
        .font(UIFont(name: "Roboto-Regular", size: 12)!)
    )
    
    static let footerLabelStyle = StringStyle(
        .color(UIColor(hex: 0x8e8e92)),
        .font(UIFont(name: "Roboto-Regular", size: 13)!),
        .lineSpacing(5),
        .alignment(.center)
    )
    
    static let changePasswordButtonStyle = StringStyle(
        .color(UIColor.white),
        .font(UIFont(name: "Roboto-Bold", size: 16)!)
    )
}
