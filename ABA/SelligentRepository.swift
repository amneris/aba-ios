//
//  SelligentRepository.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/12/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift
import Moya

enum SelligentRepositoryError: Swift.Error, CustomStringConvertible {
    var description: String { return "SelligentRepositoryError.." }
    case errorGettingProducts
    case responseError
}

class SelligentRepository {
    
    func selligentProducts(userid: String) -> Observable<SelligentProducts> {
        return APIProvider
            .request(.selligentProducts(userid: userid))
            .catchError { error -> Observable<Response> in
                
                print("error \(error)")
                throw SelligentRepositoryError.responseError
            }
            .mapJSON()
            .map { productsDictionary -> SelligentProducts in
                
                if let products = try NetworkProductsParser.parseProducts(productsDictionary) {
                    return products
                }
                
                throw SelligentRepositoryError.errorGettingProducts
        }
    }
}
