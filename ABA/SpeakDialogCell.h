//
//  SpeakDialogCell.h
//  ABA
//
//  Created by Jordi Mele on 12/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakDialogCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property (weak, nonatomic) IBOutlet UILabel *txtLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtLabelLeftConstraint;

@end
