//
//  IpAddress.swift
//  ABA
//
//  Created by MBP13 Jesus on 07/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Unbox

struct IpAddress: Unboxable {
    var ip: String
    
    init(unboxer: Unboxer) throws {
        self.ip = try unboxer.unbox(key: "publicIp")
    }
}
