//
//  DownloadController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/8/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAUnit.h"
#import "Blocks.h"
#import <Foundation/Foundation.h>

@interface DownloadController : NSObject

+ (DownloadController *)controllerWithUnit:(ABAUnit *)unit
                  andDownloadProgressBlock:(DownloadProgressBlock)progressBlock;

- (void)startDownload;
- (void)cancelDownload;
+ (BOOL)shouldDownloadOfflineModeForUnit:(ABAUnit *)unit;
+ (void)removeDownloadedFiles;

@end
