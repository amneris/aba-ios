//
//  ABAProgressOperationQueue.m
//  ABA
//
//  Created by Jaume Cornadó Panadés on 11/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAProgressOperationQueue.h"

@implementation ABAProgressOperationQueue

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setMaxConcurrentOperationCount:1];
    }
    return self;
}

@end
