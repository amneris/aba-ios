//
//  ExercisesController.h
//  ABA
//
//  Created by Marc Güell Segarra on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"
#import "Blocks.h"

@class ABAExercises, ABAExercisesGroup, ABAExercisesQuestion, ABAExercisesPhrase;

@interface ExercisesController : SectionController

-(ABAExercisesQuestion *)getCurrentABAExercisesQuestionForABAExercises: (ABAExercises *)abaExercises;

-(NSArray *)getPossibleAnswersForExercisesGroup:(ABAExercisesGroup *)exerciseGroup;

-(void)setBlankPhrasesDoneForExercisesQuestion:  (ABAExercisesQuestion *)exercisesQuestion
                               forABAExercises: (ABAExercises *)abaExercises
                               completionBlock: (CompletionBlock)completionBlock;

-(void)setExercisesGroupsDone: (NSArray *)exercisesGroups
              forABAExercises: (ABAExercises *)abaExercises
              completionBlock: (CompletionBlock)completionBlock;

-(NSArray *)getExercisesItemsForExerciseQuestion: (ABAExercisesQuestion *)exercisesQuestion
                             forExercisesSection: (ABAExercises *)abaExercises;

-(NSArray *)getAllAudioIDsForExercisesSection: (ABAExercises *)exercises;
@end
