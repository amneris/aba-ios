//
//  VocabularyViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "Definitions.h"
#import "ABAActionButtonView.h"
#import "SectionProtocol.h"
#import "ABAControlView.h"
#import "ABAVocabulary.h"

@class ABAVocabularyPhrase;

@interface VocabularyViewController : SectionBaseViewController <ABAActionButtonViewDelegate, ABAControlViewDelegate>

@property (strong, nonatomic) ABAVocabulary *section;

@property ABAVocabularyPhrase *currentPhrase;

@property kABAVocabularyStatus currentStatus;
@property (weak) id <SectionProtocol> delegate;

@end
