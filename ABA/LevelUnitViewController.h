//
//  MainViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LevelUnitCell.h"
#import "LevelUnitController.h"
#import "Definitions.h"

@class ABAUnit, UserController;

@interface LevelUnitViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, LevelUnitCellDelegate>

- (void)continueButtonLoadUnit:(ABAUnit *)unit;

@end

