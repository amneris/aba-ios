//
//  ABASpeakDataController.h
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"

@class ABAUnit;

@interface ABASpeakDataController : NSObject

+ (void)parseABASpeakSection: (NSArray*) abaSpeakDataArray onUnit: (ABAUnit*) unit completionBlock: (CompletionBlock) block;

@end
