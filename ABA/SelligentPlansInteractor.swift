//
//  PlansInteractor.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

class SelligentPlansInteractor {
    let repository: SelligentRepository
    
    init(repository: SelligentRepository) {
        self.repository = repository
    }
}

extension SelligentPlansInteractor: SelligentPlansInteractorInput {
    func getProducts(userId: String) -> Observable<SelligentProducts>{
        return self.repository.selligentProducts(userid: userId)
    }
}
