//
//  ABAEnvironmentsController.h
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABAShepherdConfiguration.h"

@interface ABAConfigurationController : UITableViewController

@property (nonatomic, strong) id<ABAShepherdConfiguration> configuration;

@end
