//
//  ABAShepherdController.m
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdController.h"
#import "ABAShepherdEditor.h"

// Linkinh controllers
#import "ABAConfigurationController.h"

// Alert string definitions
static NSString* const kAlertButtonOk           = @"Ok";
static NSString* const kAlertButtonCancel       = @"Cancel";
static NSString* const kAlertTitleCloseRestart  = @"The changes you applied need an application restart. The application will now quit!";

// Table header string definitions
static NSString* const kPluginTableHeader       = @"Plugins";
static NSString* const kConfigurationTableHeader  = @"Configurations";
static NSString* const kUnknownTableHeader      = @"Unknown";

@interface ABAShepherdController()

@property (nonatomic, strong) UIBarButtonItem* doneButton;

@end

@implementation ABAShepherdController

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.navigationItem.leftBarButtonItem = self.doneButton;
    }
    
    return self;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section)
    {
        case 0: // plugins
        {
            [self tableView:tableView didSelectPluginRowAtIndexPath:indexPath];
            break;
        }
        case 1: // environments
        {
            [self tableView:tableView didSelectConfigurationRowAtIndexPath:indexPath];
            break;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return kPluginTableHeader;
            break;
        }
        case 1:
        {
            return kConfigurationTableHeader;
            break;
        }
        default:
            return kUnknownTableHeader;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return [ABAShepherdEditor shared].plugins.count;
            break;
        }
        case 1:
        {
            return [ABAShepherdEditor shared].configurations.count;
            break;
        }
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
        {
            id<ABAShepherdPlugin> plugin = ABAShepherdEditor.shared.plugins[indexPath.row];
            return [plugin cellForMasterTableview:tableView];
        }
        case 1:
        {
            id<ABAShepherdConfiguration> configuration = ABAShepherdEditor.shared.configurations[indexPath.row];
            UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            [cell.textLabel setText:[configuration configurationName]];
            return cell;
        }
        default:
            break;
    }
    
    return nil;
}

#pragma mark - Private methods

- (void)doneButtonTapped:(UIBarButtonItem *)sender
{
    if (ABAShepherdEditor.shared.didConfigurationChange)
    {
        
        // If changes applied, ask before closing the app
        
        UIAlertController *alertCtrl = [UIAlertController alertControllerWithTitle:kAlertTitleCloseRestart message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kAlertButtonCancel style:UIAlertActionStyleCancel handler:nil];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:kAlertButtonOk style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                exit(0);
            });
        }];
        
        [alertCtrl addAction:okAction];
        [alertCtrl addAction:cancelAction];
        
        [self presentViewController:alertCtrl animated:YES completion:nil];
    }
    else
    {
        // Dismissing controller without closing the app
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectPluginRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<ABAShepherdPlugin> plugin = ABAShepherdEditor.shared.plugins[indexPath.row];
    UITableViewCell * cell   = [tableView cellForRowAtIndexPath:indexPath];
    
    [plugin executeWithTableViewCell:cell fromViewController:self];
}

- (void)tableView:(UITableView *)tableView didSelectConfigurationRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<ABAShepherdConfiguration> configuration = ABAShepherdEditor.shared.configurations[indexPath.row];
    ABAConfigurationController *environmentDetail = [ABAConfigurationController new];
    environmentDetail.configuration = configuration;
    
    [self.navigationController pushViewController:environmentDetail animated:YES];
}

#pragma mark - Accessors

- (UIBarButtonItem *)doneButton
{
    if (_doneButton) return _doneButton;
    
    _doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonTapped:)];
    
    return _doneButton;
}

@end
