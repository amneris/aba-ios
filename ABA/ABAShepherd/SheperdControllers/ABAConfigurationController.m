//
//  ABAEnvironmentsController.m
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAConfigurationController.h"
#import "ABAShepherdEnvironment.h"
#import "ABAShepherdEditor.h"

@interface ABAConfigurationController()
@property (nonatomic, strong) id<ABAShepherdEnvironment> currentEnvironment;
@end

@implementation ABAConfigurationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.currentEnvironment = [ABAShepherdEditor.shared environmentForShepherdConfigurationClass:[self.configuration class]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = self.configuration.configurationName;
    self.tableView.rowHeight = 150;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Saving selection
    id<ABAShepherdEnvironment> selectedItem = self.configuration.environments[indexPath.row];
    [ABAShepherdEditor.shared setcurrentEnvironment:selectedItem forConfiguration:self.configuration];
    
    // Forcing restart
    ABAShepherdEditor.shared.didConfigurationChange = YES;
    
    // Refreshing table
    self.currentEnvironment = selectedItem;
    [tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.configuration.environments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Creating cell
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    
    // Configuring cell
    id<ABAShepherdEnvironment> selectedItem = self.configuration.environments[indexPath.row];
    cell.textLabel.text = selectedItem.environmentName;
    cell.detailTextLabel.text = selectedItem.description;
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.frame = CGRectMake(cell.detailTextLabel.frame.origin.x, cell.detailTextLabel.frame.origin.x, cell.detailTextLabel.frame.size.width, 100);
    
    BOOL selected = [selectedItem isEqual:self.currentEnvironment];
    cell.accessoryType = selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    cell.backgroundColor = selected ? [UIColor lightGrayColor] : [UIColor whiteColor];
    
    return cell;
}

@end
