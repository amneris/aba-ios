//
//  ABACoreEnvironment.m
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABACoreShepherdEnvironment.h"

#define kABA_SECURE_API_URL @"ABA_SECURE_API_URL"

@implementation ABACoreShepherdEnvironment

- (void)parseProperties:(NSDictionary *)dictionaryWithProperties
{
    self.abaSecureApiUrl = [dictionaryWithProperties objectForKey:kABA_SECURE_API_URL];
}

#pragma mark - Description

- (NSString *)description
{
    NSMutableString * descriptionToReturn = [[NSMutableString alloc] initWithString:@""];
    [descriptionToReturn appendFormat:@"\nabaSecureApiUrl: %@", self.abaSecureApiUrl];
    [descriptionToReturn appendFormat:@"\nname: %@", self.environmentName];
    [descriptionToReturn appendFormat:@"\nis default: %@", self.isDefault ? @"YES":@"NO"];
    
    return descriptionToReturn;
}

@end
