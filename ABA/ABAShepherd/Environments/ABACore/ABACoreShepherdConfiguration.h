//
//  ABAEnvironments.h
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdConfiguration.h"
#import "ABAGenericConfiguration.h"

@interface ABACoreShepherdConfiguration : ABAGenericConfiguration <ABAShepherdConfiguration>


@end
