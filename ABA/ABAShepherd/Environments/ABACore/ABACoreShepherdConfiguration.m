//
//  ABAEnvironments.m
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABACoreShepherdConfiguration.h"
#import "ABACoreShepherdEnvironment.h"

#define kCoreConfigurationFileName @"abacore_configuration.json"

@implementation ABACoreShepherdConfiguration

- (instancetype)init
{
    self = [super initWithJsonConfiguration:kCoreConfigurationFileName
                     withConfigurationClass:NSStringFromClass([ABACoreShepherdEnvironment class])];
    if (self)
    {
        // Nothing for now
    }
    return self;
}

#pragma mark - ABASheperdEnvironment

- (NSString *)environmentName
{
    return self.configurationName;
}

#pragma mark - Description

- (NSString *)description
{
    NSMutableString * descriptionToReturn = [[NSMutableString alloc] initWithString:[super description]];
    [descriptionToReturn appendFormat:@"\n name: %@", self.configurationName];
    [descriptionToReturn appendFormat:@"\n environments: %@", self.environments];
    
    return descriptionToReturn;
}

@end
