//
//  ABACoreEnvironment.h
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAGenericEnvironment.h"

@interface ABACoreShepherdEnvironment : ABAGenericEnvironment

@property (nonatomic, strong) NSString * abaSecureApiUrl;

@end
