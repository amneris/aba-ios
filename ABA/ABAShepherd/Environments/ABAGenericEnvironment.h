//
//  ABAGenericEnvironment.h
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdEnvironment.h"

@interface ABAGenericEnvironment : NSObject <ABAShepherdEnvironment>

@property (nonatomic, strong) NSString * environmentName;
@property (nonatomic, assign) BOOL isDefault;

- (void)parseWithDictionary:(NSDictionary *)dictionaryWithEnvironment;
- (void)parseProperties:(NSDictionary *)dictionaryWithProperties;

@end
