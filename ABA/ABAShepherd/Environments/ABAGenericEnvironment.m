//
//  ABAGenericEnvironment.m
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAGenericEnvironment.h"

@implementation ABAGenericEnvironment

- (void)parseWithDictionary:(NSDictionary *)dictionaryWithEnvironment
{
    self.environmentName = [dictionaryWithEnvironment objectForKey:@"environment_name"];
    self.isDefault = [[dictionaryWithEnvironment objectForKey:@"is_default"] boolValue];
    
    // Parsing specific properties
    [self parseProperties:[dictionaryWithEnvironment objectForKey:@"properties"]];
}

- (void)parseProperties:(NSDictionary *)dictionaryWithProperties
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

#pragma mark - Compare

- (BOOL)isEqual:(id)other
{
    if ([other isKindOfClass:[self class]])
    {
        ABAGenericEnvironment * otherEnv = other;
        return [otherEnv.environmentName isEqualToString:self.environmentName];
    }
    
    return NO;
}

@end
