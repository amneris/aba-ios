//
//  ABAGenericConfiguration.m
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAGenericConfiguration.h"
#import "ABAGenericEnvironment.h"

@implementation ABAGenericConfiguration

- (instancetype)initWithJsonConfiguration:(NSString *)fileName withConfigurationClass:(NSString *)className
{
    self = [super init];
    if (self)
    {
        NSDictionary *dictionary = [self getDictionaryFromFile:fileName];
        
        if (dictionary)
        {
            NSMutableArray * environments = [NSMutableArray new];
            BOOL defaultConfigurationExist = NO;
            for (NSDictionary * dictionaryWithEnvProps in [dictionary objectForKey:@"environments"])
            {
                ABAGenericEnvironment * environment = [[NSClassFromString(className) alloc] init];
                [environment parseWithDictionary:dictionaryWithEnvProps];
                [environments addObject:environment];
                
                if ([environment isDefault] && defaultConfigurationExist)
                {
                    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"SHEPHERD: There are two default configurations" userInfo:nil];
                }
                
                defaultConfigurationExist = defaultConfigurationExist || [environment isDefault];                
            }
            
            if (!defaultConfigurationExist)
            {
                @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"SHEPHERD: There is not any default configuration" userInfo:nil];
            }
            
            // Assigning properties
            self.environments = environments;
            self.configurationName = [dictionary objectForKey:@"configuration_name"];
        }
    }
    
    return self;
}

#pragma mark - Dictionary from file

- (NSDictionary *)getDictionaryFromFile:(NSString *)fileName
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath options:NSDataReadingMappedIfSafe error:&error];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];

    return dictionary;
}

@end
