//
//  ABAGenericConfiguration.h
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABAGenericConfiguration : NSObject

- (instancetype)initWithJsonConfiguration:(NSString *)fileName withConfigurationClass:(NSString *)className;

@property (nonatomic, strong) NSString * configurationName;
@property (nonatomic, strong) NSArray * environments;

@end
