//
//  ABASheperdEnvironment.h
//  ABA
//
//  Created by MBP13 Jesus on 29/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

@import UIKit;

@protocol ABAShepherdEnvironment <NSObject>

@property (nonatomic, strong) NSString * environmentName;
@property (nonatomic, assign) BOOL isDefault;

@end