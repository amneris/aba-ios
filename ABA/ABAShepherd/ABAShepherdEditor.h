//
//  ShepherdEditor.h
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"
#import "ABAShepherdConfiguration.h"
#import "ABAShepherdEnvironment.h"

@interface ABAShepherdEditor : NSObject

@property (nonatomic, assign) BOOL didConfigurationChange;
@property (nonatomic, strong, readonly) NSArray * plugins;
@property (nonatomic, strong, readonly) NSArray * configurations;


/**
 Singleton access
 */
+ (instancetype)shared;

/**
 Register a plugin for the editor window
 */
- (void)registerPlugin:(id<ABAShepherdPlugin>)sheepPlugin;

/**
 Register an environment for the editor window
 */
- (void)registerConfiguration:(id<ABAShepherdConfiguration>)sheepPlugin;


- (id <ABAShepherdEnvironment>)environmentForShepherdConfigurationClass:(Class)shepherdClass;
- (void)setcurrentEnvironment:(id <ABAShepherdEnvironment>)environment forConfiguration:(id <ABAShepherdConfiguration>)configuration;
- (void)resetCurrentEnvironments;
@end
