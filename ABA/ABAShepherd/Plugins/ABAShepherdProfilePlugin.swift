//
//  ABAShepherdProfilePlugin.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/01/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

@objc class ABAShepherdProfilePlugin: NSObject {
    
    internal static let kNewProfileKey = "kNewProfileKey"
    internal var tableToRefresh: UITableView?
    
    class func isNewProfileView() -> Bool {
        let isNewProfile = UserDefaults.standard.bool(forKey: kNewProfileKey)
        return isNewProfile
    }
    
    internal class func setProfileTypeNew() {
        UserDefaults.standard.set(true, forKey: kNewProfileKey)
    }
    
    internal class func setProfileTypeLegacy() {
        UserDefaults.standard.set(false, forKey: kNewProfileKey)
    }
}

extension ABAShepherdProfilePlugin: ABAShepherdPlugin {
    
    func cell(forMasterTableview tableView: UITableView) -> (UITableViewCell) {
        
        self.tableToRefresh = tableView
        
        let shouldShowNewProfile = ABAShepherdProfilePlugin.isNewProfileView()
        var cellString = "Profile view"
        if shouldShowNewProfile {
            cellString = cellString + " - New"
        }
        else {
            cellString = cellString + " - Legacy"
        }
        
        let tablecell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
        tablecell.textLabel?.text = cellString
        return tablecell
    }

    func execute(with tableViewCell: UITableViewCell!, from viewController: UIViewController!) {
        let alertController: UIAlertController!
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            alertController = UIAlertController(title: nil, message:"Select profile view?", preferredStyle: .alert)
        } else {
            alertController = UIAlertController(title: nil, message:"Select profile view?", preferredStyle: .actionSheet)
        }
        
        // Cancel action
        let cancelAction = UIAlertAction(title: "Legacy", style: .cancel) { (action) in
            ABAShepherdProfilePlugin.setProfileTypeLegacy()
            self.tableToRefresh?.reloadData()
        }
        alertController.addAction(cancelAction)
        
        // New profile action
        let newProfileAction = UIAlertAction(title: "New", style: .default) { (action) in
            ABAShepherdProfilePlugin.setProfileTypeNew()
            self.tableToRefresh?.reloadData()
        }
        alertController.addAction(newProfileAction)
        
        // Presentation info
        viewController.present(alertController, animated: true) {
            // ...
        }
    }

}
