//
//  ABASheperdEditorClearKeychainPlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//
//

#import "ABAShepherdEditorClearKeychainPlugin.h"
#import "ABAShepherdEditor.h"

@interface ABAShepherdEditorClearKeychainPlugin()

@end

@implementation ABAShepherdEditorClearKeychainPlugin

#pragma mark Initializer


#pragma mark ABAShepherdPlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView
{
    UITableViewCell* cell     = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text       = @"Clear keychain";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController
{
    UIAlertController *actionCtrl = [UIAlertController alertControllerWithTitle:@"Clear the keychain?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction * _Nonnull action) {
        [self clearKeychain];
        ABAShepherdEditor.shared.didConfigurationChange = YES;
    }];
    
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    
    
    [actionCtrl addAction:yesAction];
    [actionCtrl addAction:noAction];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

#pragma mark - Private

- (BOOL)clearKeychain
{
    return
        [self clearKeychainForSecClass:kSecClassGenericPassword] &&
        [self clearKeychainForSecClass:kSecClassInternetPassword] &&
        [self clearKeychainForSecClass:kSecClassCertificate] &&
        [self clearKeychainForSecClass:kSecClassKey] &&
        [self clearKeychainForSecClass:kSecClassIdentity];
}

- (BOOL)clearKeychainForSecClass:(CFTypeRef)secClass
{
    OSStatus result = SecItemDelete((__bridge CFDictionaryRef)@{(__bridge id) kSecClass:(__bridge id)secClass});
    return result != noErr && result != errSecItemNotFound;
}

@end
