//
//  ABAShepherdUnitDownloaderPlugin.h
//  ABA
//
//  Created by MBP13 Jesus on 04/01/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"

@interface ABAShepherdUnitDownloaderPlugin : NSObject <ABAShepherdPlugin>

@end
