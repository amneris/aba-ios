//
//  ABAShepherdAutomatorPlugin.swift
//  ABA
//
//  Created by MBP13 Jesus on 26/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

import Foundation

@objc class ABAShepherdAutomatorPlugin: NSObject, ABAShepherdPlugin {
    
    internal static let kAutomationKey = "kAutomationKey"
    internal var tableToRefresh: UITableView?
    
    func cell(forMasterTableview tableView: UITableView) -> (UITableViewCell) {
        
        self.tableToRefresh = tableView
        
        let shouldAutomate = ABAShepherdAutomatorPlugin.isAutomationEnabled()
        var cellString = "Automate sections?"
        if shouldAutomate {
            cellString = cellString + " - YES"
        }
        else {
            cellString = cellString + " - NO"
        }
        
        let tablecell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
        tablecell.textLabel?.text = cellString
        return tablecell
    }
    
    func execute(with tableViewCell: UITableViewCell!, from viewController: UIViewController!) {
        
        let alertController: UIAlertController!
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            alertController = UIAlertController(title: nil, message:"Automate sections?", preferredStyle: .alert)
        } else {
            alertController = UIAlertController(title: nil, message:"Automate sections?", preferredStyle: .actionSheet)
        }
        
        // Cancel action
        let cancelAction = UIAlertAction(title: "Reset", style: .cancel) { (action) in
            ABAShepherdAutomatorPlugin.setAutomation(false)
            self.tableToRefresh?.reloadData()
        }
        alertController.addAction(cancelAction)
        
        // Automate Action
        let OKAction = UIAlertAction(title: "Automate", style: .default) { (action) in
            ABAShepherdAutomatorPlugin.setAutomation(true)
            self.tableToRefresh?.reloadData()
        }
        alertController.addAction(OKAction)
        
        // Presentation info
        viewController.present(alertController, animated: true) {
            // ...
        }
    }
    
    internal class func setAutomation(_ enabled: Bool) {
        UserDefaults.standard.set(enabled, forKey: kAutomationKey)
    }
    
    class func isAutomationEnabled() -> Bool {
        let automate = UserDefaults.standard.bool(forKey: kAutomationKey)
        return automate
    }
}
