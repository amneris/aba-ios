//
//  ABAShepherdAppVersioning.h
//  ABA
//
//  Created by MBP13 Jesus on 30/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"

@interface ABAShepherdAppVersioningPlugin : NSObject<ABAShepherdPlugin>

@end
