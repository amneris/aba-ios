//
//  ABAShepherdUnitDownloaderPlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 04/01/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

#import "ABAShepherdUnitDownloaderPlugin.h"
#import "ABAUnit.h"
#import "LevelUnitController.h"
#import "DownloadController.h"

@interface ABAShepherdUnitDownloaderPlugin()
@property (nonatomic, assign) UIViewController *viewcontroller;
@property (nonatomic, strong) DownloadController *downloadcontroller;
@property (nonatomic, strong) UIAlertController *downloadingAlertContrlller;
@end

@implementation ABAShepherdUnitDownloaderPlugin

- (void)showInputError {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Input text error" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    
    [self.viewcontroller presentViewController:alert animated:YES completion:nil];
}

- (void)showDownloadError {
    [self.downloadingAlertContrlller dismissViewControllerAnimated:YES completion:nil];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Download error" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    
    [self.viewcontroller presentViewController:alert animated:YES completion:nil];
}

- (void)showCompletion {
    [self.downloadingAlertContrlller dismissViewControllerAnimated:YES completion:nil];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Finished" message:@"The download has finished" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    
    [self.viewcontroller presentViewController:alert animated:YES completion:nil];
}

- (void)showLoading {
    self.downloadingAlertContrlller = [UIAlertController alertControllerWithTitle:@"Downloading" message:@"Please wait" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"Cancel download" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self.downloadcontroller cancelDownload];
    }];
    [self.downloadingAlertContrlller addAction:action];
    
    [self.viewcontroller presentViewController:self.downloadingAlertContrlller animated:YES completion:nil];
}

#pragma mark - ABAShepherdPlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView {
    UITableViewCell* cell     = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text       = @"Unit files downloader";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController {
    
    self.viewcontroller = viewController;
    
    UIAlertController *actionCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Enter the units to download"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
    
    UIAlertAction* okAction = [UIAlertAction
                                  actionWithTitle:@"Download"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                     
                                      UITextField * field1 = [actionCtrl.textFields objectAtIndex:0];
                                      UITextField * field2 = [actionCtrl.textFields objectAtIndex:1];
                                      
                                      if (field1.text.length == 0 && field2.text.length == 0) {
                                          [self showInputError];
                                      }
                                      
                                      long fromUnit = [field1.text integerValue];
                                      long toUnit = [field2.text integerValue];
                                      
                                      if (fromUnit > 0 && fromUnit <= 144 && toUnit > 0 && toUnit <= 144 && fromUnit <= toUnit) {
                                          [self showLoading];
                                          [self downloadNextUnit:fromUnit lastUnit:toUnit];
                                      }
                                      else {
                                          [self showInputError];
                                      }
                                  }];
    
    [actionCtrl addAction:okAction];
    [actionCtrl addAction:cancelAction];
    
    [actionCtrl addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"from unit";
    }];
    [actionCtrl addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"to unit";
    }];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

#pragma mark - Downloader

- (void)downloadNextUnit:(long)unitIndex lastUnit:(long)lastUnit{

    ABAUnit * unit = [LevelUnitController getUnitWithID:[NSNumber numberWithLong:unitIndex]];
    self.downloadcontroller = [DownloadController controllerWithUnit:unit andDownloadProgressBlock:^(NSError *error, float progress, BOOL didFinish) {
        if (error){
            [self showDownloadError];
        }
        if (didFinish) {
            if (unitIndex == lastUnit) {
                [self showCompletion];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self downloadNextUnit:(unitIndex+1) lastUnit:lastUnit];
                });
            }
        }
    }];
    [self.downloadcontroller startDownload];
}

@end
