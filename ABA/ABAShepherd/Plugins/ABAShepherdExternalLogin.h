//
//  ABAShepherdExternalLogin.h
//  ABA
//
//  Created by MBP13 Jesus on 06/08/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"

#define kShepherdExternalTokenKey @"shepherdExternalTokenKey"

@interface ABAShepherdExternalLogin : NSObject <ABAShepherdPlugin>

+ (NSString *)retrieveTokenAndErase;

@end
