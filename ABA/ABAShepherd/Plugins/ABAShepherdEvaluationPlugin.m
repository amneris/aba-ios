//
//  ABAShepherdEvaluationPlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 02/09/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdEvaluationPlugin.h"
#define kEvaluationUnlockKey @"EvaluationUnlockKey"

@interface ABAShepherdEvaluationPlugin ()
@property(nonatomic, assign) UITableView *tableToRefresh;
@end

@implementation ABAShepherdEvaluationPlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView
{
    self.tableToRefresh = tableView;

    BOOL unlocked = [[NSUserDefaults standardUserDefaults] boolForKey:kEvaluationUnlockKey];

    NSString *cellString;
    if (unlocked)
    {
        cellString = @"Unlock evaluation? - YES";
    }
    else
    {
        cellString = @"Unlock evaluation? - NO";
    }

    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text = cellString;
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController
{
    UIAlertController *actionCtrl = [UIAlertController
                                      alertControllerWithTitle:@"Unlock evaluation?"
                                      message:nil
                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelBtn = [UIAlertAction
                              actionWithTitle:@"Reset"
                              style:UIAlertActionStyleDestructive
                              handler:^(UIAlertAction * action) {
                                  [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kEvaluationUnlockKey];
                                  [self.tableToRefresh reloadData];
                              }];
    
    UIAlertAction* yesBtn = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kEvaluationUnlockKey];
                                    [self.tableToRefresh reloadData];
                                }];
    
    [actionCtrl addAction:yesBtn];
    [actionCtrl addAction:cancelBtn];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

#pragma mark - Public methods

+ (BOOL)forceUnlock
{
    BOOL unlocked = [[NSUserDefaults standardUserDefaults] boolForKey:kEvaluationUnlockKey];
    return unlocked;
}

@end
