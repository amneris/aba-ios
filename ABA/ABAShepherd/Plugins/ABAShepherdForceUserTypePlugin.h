//
//  ABAShepherdForceUserTypePlugin.h
//  ABA
//
//  Created by Oriol Vilaró on 6/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"

@interface ABAShepherdForceUserTypePlugin : NSObject <ABAShepherdPlugin>

+ (BOOL)shouldForceFree;

+ (BOOL)shouldForcePremium;

@end
