//
//  ABAShepherdEvaluationPlugin.h
//  ABA
//
//  Created by MBP13 Jesus on 02/09/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"

@interface ABAShepherdEvaluationPlugin : NSObject <ABAShepherdPlugin>

+ (BOOL)forceUnlock;

@end
