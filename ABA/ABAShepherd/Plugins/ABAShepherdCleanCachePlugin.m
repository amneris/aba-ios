//
//  ABAShepherdCleanFilesPlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 30/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdCleanCachePlugin.h"
#import "ABAShepherdEditor.h"

@interface ABAShepherdCleanCachePlugin()

@end

@implementation ABAShepherdCleanCachePlugin

#pragma mark ABAShepherdPlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView
{
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text = @"Clean cached files";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController
{
    
    UIAlertController *actionCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Clean cached files?"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
    
    UIAlertAction* cleanAction = [UIAlertAction
                                  actionWithTitle:@"Clean!"
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction * action) {
                                      [self clearCache];
                                  }];
    
    [actionCtrl addAction:cleanAction];
    [actionCtrl addAction:cancelAction];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

#pragma mark - Private

- (void)clearCache
{
    NSArray *cachePath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    [self removeFileFromPath:[cachePath objectAtIndex:0]];
}

- (void)removeFileFromPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath:path error:nil];
    for (NSString *filename in fileArray)
    {
        [fileManager removeItemAtPath:[path stringByAppendingPathComponent:filename] error:NULL];
    }
}

@end
