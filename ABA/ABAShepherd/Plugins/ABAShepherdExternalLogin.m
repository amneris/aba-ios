//
//  ABAShepherdExternalLogin.m
//  ABA
//
//  Created by MBP13 Jesus on 06/08/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdExternalLogin.h"
#import "ABAShepherdEditor.h"

@interface ABAShepherdExternalLogin()
{
    UIViewController * _viewController;
}
@end

@implementation ABAShepherdExternalLogin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView {
    
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text = @"User external Token";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController {
    
    _viewController = viewController;
    
    UIAlertController *alertCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Enter the token"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    [alertCtrl addTextFieldWithConfigurationHandler:nil];
    
    
    UIAlertAction* doneBtn = [UIAlertAction
                                actionWithTitle:@"Done"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    NSString * enteredToken = alertCtrl.textFields.firstObject.text;
                                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                    [userDefaults setObject:enteredToken forKey:kShepherdExternalTokenKey];
                                    [userDefaults synchronize];
                                    
                                    [self showConfirmation];
                                    ABAShepherdEditor.shared.didConfigurationChange = YES;
                                }];
    
    [alertCtrl addAction:doneBtn];
    [viewController presentViewController:alertCtrl animated:YES completion:nil];
}

#pragma mark - Public methods

+ (NSString *)retrieveTokenAndErase {
    
    // Reading token
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * token = [userDefaults objectForKey:kShepherdExternalTokenKey];
    
    // Erasing token
    [userDefaults setObject:nil forKey:kShepherdExternalTokenKey];
    [userDefaults synchronize];
    
    // token return
    return token;
}

#pragma mark - Private methods

- (void)showConfirmation {
    
    UILabel * confirmationlabel = [[UILabel alloc] init];
    confirmationlabel.text = @"Copied!. Make sure you are logged out in order to user the token. Remember: Only one usage!";
    confirmationlabel.backgroundColor = [UIColor darkGrayColor];
    confirmationlabel.textColor = [UIColor whiteColor];
    confirmationlabel.layer.cornerRadius = 10;
    confirmationlabel.numberOfLines = 0;
    confirmationlabel.frame = CGRectMake(0, 0, 300, 100);
    confirmationlabel.layer.masksToBounds = YES;
    confirmationlabel.textAlignment = NSTextAlignmentCenter;
    confirmationlabel.center = CGPointMake(_viewController.view.frame.size.width / 2, _viewController.view.frame.size.height / 2);
    confirmationlabel.alpha = 0.0;
    
    [_viewController.view addSubview:confirmationlabel];
    
    // Showing label animation
    [UIView animateWithDuration:0.5
                     animations:^{
                         confirmationlabel.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         // Dismissing label animation
                         [UIView animateWithDuration:0.5
                                               delay:3
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              confirmationlabel.alpha = 0;
                                          }
                                          completion:^(BOOL finished) {
                                              [confirmationlabel removeFromSuperview];
                                          }];
                     }];
}

@end
