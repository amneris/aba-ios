//
//  ABAShepherdLanguagePlugin.h
//  ABA
//
//  Created by MBP13 Jesus on 11/08/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAShepherdPlugin.h"

@interface ABAShepherdLanguagePlugin : NSObject <ABAShepherdPlugin>

@end
