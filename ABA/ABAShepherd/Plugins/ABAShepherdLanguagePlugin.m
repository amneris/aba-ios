//
//  ABAShepherdLanguagePlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 11/08/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdLanguagePlugin.h"
#import "Utils.h"
#import "ABAShepherdEditor.h"
#import "LanguageController.h"

@interface ABAShepherdLanguagePlugin()

@end

@implementation ABAShepherdLanguagePlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView {
    
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text = @"Language selection";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController {
    UIAlertController *actionCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Select the language"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    for (NSString * language in kSuportedLanguagesArray) {
        UIAlertAction* action = [UIAlertAction
                                     actionWithTitle:language
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * _Nonnull action) {
                                         [LanguageController setCurrentLanguage:language];
                                         ABAShepherdEditor.shared.didConfigurationChange = YES;
                                     }];
        
        [actionCtrl addAction: action];
    }
    
    UIAlertAction* cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDestructive
                                   handler:nil];
    
    [actionCtrl addAction:cancelAction];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

@end
