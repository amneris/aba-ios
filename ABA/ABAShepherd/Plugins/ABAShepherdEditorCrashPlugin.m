//
//  ABASheperdEditorCrashPlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//
//

#import "ABAShepherdEditorCrashPlugin.h"
#import "ABAShepherdEditor.h"

@interface ABAShepherdEditorCrashPlugin ()

@property (nonatomic) UIViewController * showingViewController;

@end

@implementation ABAShepherdEditorCrashPlugin

- (UITableViewCell *) cellForMasterTableview: (UITableView *) tableView
{
    UITableViewCell* cell     = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text       = @"Crash (on purpose)";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController
{
    UIAlertController *actionCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Are you sure you wish to crash the application?"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:nil];
    
    UIAlertAction* cleanAction = [UIAlertAction
                                  actionWithTitle:@"Yes }:-)"
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction * action) {
                                      [UIView animateWithDuration:1 animations:^{
                                          
                                          UIViewController * rootViewController = _showingViewController;
                                          
                                          while (rootViewController.parentViewController)
                                          {
                                              rootViewController = rootViewController.parentViewController;
                                          }
                                          
                                          rootViewController.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
                                          rootViewController.view.transform = CGAffineTransformRotate(rootViewController.view.transform, M_PI);
                                          
                                      } completion:^(BOOL finished) {
                                          CFRelease(nil);
                                      }];
                                  }];
    
    [actionCtrl addAction:cleanAction];
    [actionCtrl addAction:cancelAction];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

@end
