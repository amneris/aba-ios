//
//  ABAShepherdForceUserTypePlugin.m
//  ABA
//
//  Created by Oriol Vilaró on 6/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAShepherdForceUserTypePlugin.h"

#define kUserTypeKey @"UserType"

#define kUserFree @"UserFree"
#define kUserPremium @"UserPremium"
#define kUserNone @"UserNone"

@interface ABAShepherdForceUserTypePlugin ()
@property (nonatomic, assign) UITableView *tableToRefresh;
@end

@implementation ABAShepherdForceUserTypePlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView
{
    self.tableToRefresh = tableView;
    
    NSString *type = [[NSUserDefaults standardUserDefaults] objectForKey:kUserTypeKey];
    
    if (!type)
    {
        type = kUserNone;
    }
    
    NSString *cellString;
    if ([type isEqualToString:kUserFree])
    {
        cellString = @"Forcing user type: Free";
    }
    else if ([type isEqualToString:kUserPremium])
    {
        cellString = @"Forcing user type: Premium";
    }
    else
    {
        cellString = @"Forcing user type: None";
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text = cellString;
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController
{
    UIAlertController *actionCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Force user type?"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [[NSUserDefaults standardUserDefaults] setObject:kUserNone forKey:kUserTypeKey];
                                       [self.tableToRefresh reloadData];
                                   }];
    
    UIAlertAction* freeTypeAction = [UIAlertAction
                                  actionWithTitle:@"Free"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      [[NSUserDefaults standardUserDefaults] setObject:kUserFree forKey:kUserTypeKey];
                                      [self.tableToRefresh reloadData];
                                  }];
    
    UIAlertAction* premiumTypeAction = [UIAlertAction
                                     actionWithTitle:@"Premium"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                        [[NSUserDefaults standardUserDefaults] setObject:kUserPremium forKey:kUserTypeKey];
                                        [self.tableToRefresh reloadData];
                                     }];
    
    
    [actionCtrl addAction:freeTypeAction];
    [actionCtrl addAction:premiumTypeAction];
    [actionCtrl addAction:cancelAction];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
    
}

#pragma mark - Public methods

+(BOOL)shouldForceFree
{
    NSString *type = [[NSUserDefaults standardUserDefaults] objectForKey:kUserTypeKey];
    if ([type isEqualToString:kUserFree])
    {
        return YES;
    }

    return NO;
}

+(BOOL)shouldForcePremium
{
    NSString *type = [[NSUserDefaults standardUserDefaults] objectForKey:kUserTypeKey];
    if ([type isEqualToString:kUserPremium])
    {
        return YES;
    }
    
    return NO;
}


@end
