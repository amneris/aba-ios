//
//  ABAShepherdCleanDDBBPlugin.m
//  ABA
//
//  Created by MBP13 Jesus on 30/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdCleanDDBBPlugin.h"
#import "ABAShepherdEditor.h"

@interface ABAShepherdCleanDDBBPlugin()
@end

@implementation ABAShepherdCleanDDBBPlugin

#pragma mark ABAShepherdPlugin

- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView
{
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    cell.textLabel.text = @"Clean DDBB";
    return cell;
}

- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController
{
    UIAlertController *actionCtrl = [UIAlertController
                                     alertControllerWithTitle:@"Clean DDBB? This will reset the DDBB"
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelAction = [UIAlertAction
                                actionWithTitle:@"No"
                                style:UIAlertActionStyleDefault
                                handler:nil];
    
    UIAlertAction* cleanAction = [UIAlertAction
                             actionWithTitle:@"Clean!"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action) {
                                 [self clearCache];
                                 ABAShepherdEditor.shared.didConfigurationChange = YES;
                             }];
    
    [actionCtrl addAction:cleanAction];
    [actionCtrl addAction:cancelAction];
    
    actionCtrl.popoverPresentationController.sourceView = tableViewCell;
    actionCtrl.popoverPresentationController.sourceRect = tableViewCell.bounds;
    
    [viewController presentViewController:actionCtrl animated:YES completion:nil];
}

#pragma mark - Private

- (void)clearCache
{
    NSArray *libraryPathList = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *appSupportPath    = [[libraryPathList  objectAtIndex:0] stringByAppendingPathComponent:@"Application Support"];
    [self removeFileFromPath:appSupportPath];
}

- (void)removeFileFromPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath:path error:nil];
    for (NSString *filename in fileArray)
    {
        [fileManager removeItemAtPath:[path stringByAppendingPathComponent:filename] error:NULL];
    }
}

@end
