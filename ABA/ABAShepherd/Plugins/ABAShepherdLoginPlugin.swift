//
//  ABAShepherdLoginPlugin.swift
//  ABA
//
//  Created by MBP13 Jesus on 11/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

import Foundation

@objc class ABAShepherdLoginPlugin: NSObject {
    
    // Definitions
    internal static let kCredentialsListKey = "kCredentialsListKey"
    static let kLastLoginEmailKey = "LastLoginEmailKey"
    static let kLastLoginPassKey = "LastLoginPassKey"
    
    // MARK: Primary methods (Storage)

    class func addLoginCredentials(_ login: String, pass: String,  environment: String) {
        
        let loginModel = LoginModel(login: login, pass: pass, environment: environment)
        let currentCredentialList = readLoginCredentials()
        var filteredList = currentCredentialList.filter { (loginModelAtIndex) -> Bool in
            return !(loginModelAtIndex == loginModel)
        }
        filteredList.insert(loginModel, at: 0)
        saveLoginCredentials(filteredList)
    }
    
    class func readLoginCredentials() -> [LoginModel] {
        let listOfRawLoginModel = UserDefaults.standard.array(forKey: ABAShepherdLoginPlugin.kCredentialsListKey)
        
        if let safeList = listOfRawLoginModel {
            let list = safeList as! [[String:String]]
            return list.map({ (rawtemAtIndex) -> LoginModel in
                LoginModel(dictionary: rawtemAtIndex)
            })
        }
        
        return []
    }
    
    class internal func saveLoginCredentials(_ list: [LoginModel]) {
        let listOfRawLoginModel = list.map { itemAtIndex in itemAtIndex.toDictionary() }
        UserDefaults.standard.set(listOfRawLoginModel, forKey: ABAShepherdLoginPlugin.kCredentialsListKey)
        UserDefaults.standard.synchronize()
    }
}

extension ABAShepherdLoginPlugin: ABAShepherdPlugin {
    
    func cell(forMasterTableview tableView: UITableView) -> (UITableViewCell) {
        
        let tablecell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
        tablecell.textLabel?.text = "Select Login credentials"
        return tablecell
    }
    
    func execute(with tableViewCell: UITableViewCell!, from viewController: UIViewController!) {
        
        let alertController: UIAlertController!
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            alertController = UIAlertController(title: nil, message:"Choose login credentials", preferredStyle: .alert)
        } else {
            alertController = UIAlertController(title: nil, message:"Choose login credentials", preferredStyle: .actionSheet)
        }
        
        // Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // Do nothing
        }
        alertController.addAction(cancelAction)
        
        for loginModel: LoginModel in ABAShepherdLoginPlugin.readLoginCredentials() {
            let actionTitle = loginModel.login + " / " + loginModel.password + " / " + loginModel.moreInfo;
            let action = UIAlertAction(title: actionTitle, style: .default, handler: { (action) -> Void in
                UserDefaults.standard.set(loginModel.login, forKey: ABAShepherdLoginPlugin.kLastLoginEmailKey)
                UserDefaults.standard.set(loginModel.password, forKey: ABAShepherdLoginPlugin.kLastLoginPassKey)
            })
            alertController.addAction(action)
        }
        
        // Presentation info
        viewController.present(alertController, animated: true) {
            // ...
        }
    }

}

struct LoginModel {
    var login: String
    var password: String
    var moreInfo: String
    
    func toDictionary() -> [String: String] {
        return ["login":login, "password":password, "moreInfo":moreInfo]
    }
    
    init(login: String, pass: String, environment: String) {
        self.login = login
        password = pass
        moreInfo = environment
    }
    
    init(dictionary: [String: String]) {
        login = dictionary["login"]!
        password = dictionary["password"]!
        moreInfo = dictionary["moreInfo"]!
    }
}

extension LoginModel: Equatable {}

func == (lhs: LoginModel, rhs: LoginModel) -> Bool {
    let areEqual = lhs.login == rhs.login &&
        lhs.password == rhs.password &&
        lhs.moreInfo == rhs.moreInfo
    
    return areEqual
}
