//
//  ABASheperDogEnvironment.h
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

@import UIKit;

@protocol ABAShepherdConfiguration <NSObject>

@property (nonatomic, strong) NSString * configurationName;
@property (nonatomic, strong) NSArray * environments;

@end
