//
//  ShepherdEditorPlugin.h
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

@import UIKit;

@protocol ABAShepherdPlugin <NSObject>

/**
 Generate a tableview cell for inside the editor window
 */
- (UITableViewCell *)cellForMasterTableview:(UITableView *)tableView;

/**
 Execute the plugin code
 - The tableviewcell will be the cell that was previousely created
 - The viewcontroller is the editor controller. Example usages:
 viewcontroller.view
 viewcontroller.navigationController
 */
- (void)executeWithTableViewCell:(UITableViewCell *)tableViewCell fromViewController:(UIViewController *)viewController;


@end