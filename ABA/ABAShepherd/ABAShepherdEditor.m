//
//  ShepherdEditor.m
//  ABA
//
//  Created by MBP13 Jesus on 26/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABAShepherdEditor.h"
#import "ABAShepherdEnvironment.h"
#import "Utils.h"

@implementation ABAShepherdEditor

#pragma mark - Shared instance

+ (instancetype)shared
{
    static id              instance = nil;
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

#pragma mark - Public methods (Register)

- (void)registerPlugin:(id<ABAShepherdPlugin>)sheepPlugin
{
    if (!_plugins)
    {
        _plugins = [NSArray arrayWithObject:sheepPlugin];
    }
    else
    {
        _plugins = [_plugins arrayByAddingObject:sheepPlugin];
    }
}

- (void)registerConfiguration:(id<ABAShepherdConfiguration>)sheepPlugin
{
    if (!_configurations)
    {
        _configurations = [NSArray arrayWithObject:sheepPlugin];
    }
    else
    {
        _configurations = [_configurations arrayByAddingObject:sheepPlugin];
    }
}

#pragma mark - Public methods (configuration get)

- (id<ABAShepherdEnvironment>)environmentForShepherdConfigurationClass:(Class)shepherdClass
{
    for (id<ABAShepherdConfiguration> configuration in self.configurations)
    {
        if ([configuration isKindOfClass:shepherdClass])
        {
            return [self currentEnvironmentForConfiguration:configuration];
        }
    }
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"Configuration not found for class %@ in method %@", shepherdClass, NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (void)setcurrentEnvironment:(id<ABAShepherdEnvironment>)environment forConfiguration:(id<ABAShepherdConfiguration>)configuration
{
    NSMutableDictionary * currentEnvDictionary = [self dictionaryWithCurrentEnvironments];
    [currentEnvDictionary setObject:environment.environmentName forKey:NSStringFromClass([configuration class])];
    [self saveDictionaryWithCurrentEnvironments:currentEnvDictionary];
}

#pragma mark - Private methods

- (id<ABAShepherdEnvironment>)currentEnvironmentForConfiguration:(id<ABAShepherdConfiguration>)configuration
{

#ifdef SHEPHERD_DEBUG
    // In development the utility returns the selected one, otherwise the default
    
    // Looking for selected
    NSDictionary * dictWithEnvs = [self dictionaryWithCurrentEnvironments];
    NSString * environmentName = [dictWithEnvs objectForKey:NSStringFromClass([configuration class])];
    if (environmentName)
    {
        for (id<ABAShepherdEnvironment> environment in configuration.environments)
        {
            if ([environment.environmentName isEqualToString:environmentName])
            {
                return environment;
            }
        }
    }
    
    // Looking for default
    for (id<ABAShepherdEnvironment> environment in configuration.environments)
    {
        if (environment.isDefault)
        {
            return environment;
        }
    }
#else
    // In production the utility just picks the default one
    for (id<ABAShepherdEnvironment> environment in configuration.environments)
    {
        if (environment.isDefault)
        {
            return environment;
        }
    }
#endif
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"Environment not found for class %@ in method %@", [configuration class], NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

#define kShepherdCurrentEnvKey @"ShepherdCurrentEnvKey"

- (void) resetCurrentEnvironments {
    [NSUserDefaults.standardUserDefaults removeObjectForKey:kShepherdCurrentEnvKey];
    [NSUserDefaults.standardUserDefaults synchronize];
}

#pragma mark - Private methods (NSUser defaults)

- (NSMutableDictionary *)dictionaryWithCurrentEnvironments
{
    NSMutableDictionary* dictionaryWithEnvs = [NSMutableDictionary dictionaryWithDictionary:[NSUserDefaults.standardUserDefaults objectForKey:kShepherdCurrentEnvKey]];
    
    return dictionaryWithEnvs;
}

- (void)saveDictionaryWithCurrentEnvironments:(NSDictionary *)dictionary
{
    [NSUserDefaults.standardUserDefaults setObject:dictionary forKey:kShepherdCurrentEnvKey];
    [NSUserDefaults.standardUserDefaults synchronize];
}

@end