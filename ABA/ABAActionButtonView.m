//
//  ABAActionButtonView.m
//  ABA
//
//  Created by Marc Güell Segarra on 3/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAActionButtonView.h"
#import "Utils.h"
#import "Resources.h"
#import "ABAPulseViewController.h"
#import "PureLayout.h"

@interface ABAActionButtonView ()

@property kABAActionButtonType type;
@property kABAActionButtonStatus status;
@property kABASectionTypes sectionType;

@property (nonatomic, assign) BOOL didSetupConstraints;
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UIImageView *buttonImage;
@property (nonatomic, strong) UIView *actionButtonPlaceholder;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end

@implementation ABAActionButtonView

-(id) initWithSectionType: (kABASectionTypes)sectionType {
	self = [super init];
	
	if(self) {
		_sectionType = sectionType;
		_type = [self getButtonTypeFromSectionType];
		
		self.backgroundColor = [UIColor clearColor];
		self.userInteractionEnabled = YES;
		
		[self addSubview:self.pulseViewController.view];
		[self addSubview:self.actionLabel];
		[self.buttonView addSubview:self.buttonImage];
		[self addSubview:self.buttonView];

		self.didSetupConstraints = NO;

		[self addButtonTapRecognizer];
	}
	
	return self;
}

-(kABAActionButtonType)getButtonTypeFromSectionType {

	switch (_sectionType) {
		case kABAWriteSectionType: {
			return kABAActionButtonTypeListen;
			break;
		}
		default: return kABAActionButtonTypeListen;
	}
}

-(void)layoutIfNeeded {
	
	[super layoutIfNeeded];
	
	if(!self.didSetupConstraints) {

		// At this point we'll have the superview reference for applying autolayout constraints
		// Let's place the elements:
		[self.actionLabel autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self];
		[self.actionLabel autoSetDimension:ALDimensionHeight toSize:(IS_IPAD?20.0f:14.0f)*1.15];
//		[self.actionLabel autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self withMultiplier:0.15f];
		[self.actionLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom];
		[self.actionLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft];
		[self.actionLabel setNeedsLayout];
		[self.actionLabel setNeedsUpdateConstraints];
		[self.actionLabel layoutIfNeeded];

		CGFloat heightAvailable = (self.frame.size.height - _actionLabel.frame.size.height);
		CGFloat heightAvailableForImage = heightAvailable * 0.75f;
		CGFloat margin = heightAvailable * 0.15f;
		
		[self.buttonView autoSetDimension:ALDimensionHeight toSize:heightAvailableForImage];	// It's a square! (1:1)
		[self.buttonView autoSetDimension:ALDimensionWidth toSize:heightAvailableForImage];
		[self.buttonView autoAlignAxis:ALAxisVertical toSameAxisOfView:self];
		[self.buttonView autoPinEdge:ALEdgeBottom toEdge:ALEdgeTop ofView:_actionLabel withOffset:-margin];
		[self.buttonView setNeedsLayout];
		[self.buttonView setNeedsUpdateConstraints];
		[self.buttonView layoutIfNeeded];
		
		self.buttonView.clipsToBounds = YES;
		self.buttonView.layer.cornerRadius = heightAvailableForImage/2;

		[self.buttonImage autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.buttonView withMultiplier:0.65f];
		[self.buttonImage autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.buttonView withMultiplier:0.65f];
		[self.buttonImage autoCenterInSuperview];
		[self.buttonImage setNeedsLayout];
		[self.buttonImage setNeedsUpdateConstraints];
		[self.buttonImage layoutIfNeeded];
		
		[self.pulseViewController.view autoAlignAxis:ALAxisHorizontal toSameAxisOfView:self.buttonView];
		[self.pulseViewController.view autoAlignAxis:ALAxisVertical toSameAxisOfView:self.buttonView];

		[self.pulseViewController.view autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.buttonView withMultiplier:2.0];
		[self.pulseViewController.view autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:self.buttonView withMultiplier:2.0];

		self.didSetupConstraints = YES;
	}
}

-(UILabel *)actionLabel {
	if(!_actionLabel) {
		_actionLabel = [[UILabel alloc] init];
		_actionLabel.backgroundColor    = [UIColor clearColor];
		_actionLabel.textColor          = [UIColor whiteColor];
		_actionLabel.textAlignment      = NSTextAlignmentCenter;
		_actionLabel.accessibilityLabel = @"actionLabelView";
		
		_actionLabel.text = [self getLabelTextForCurrentTypeAndStatus];
		_actionLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20.0f:14.0f];
		
		if(_sectionType == kABAWriteSectionType) {
			_actionLabel.textColor = ABA_COLOR_DARKGREY;
		}
	}
	
	return _actionLabel;
}

-(ABAPulseViewController *)pulseViewController {
	if(!_pulseViewController) {
		_pulseViewController = [[ABAPulseViewController alloc] init];
	}
	return _pulseViewController;
}

-(UITapGestureRecognizer *)tapRecognizer {
	if(!_tapRecognizer) {
		_tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
		[_tapRecognizer setNumberOfTapsRequired:1];
	}
	
	return _tapRecognizer;
}

-(UIView *)actionButtonPlaceholder {
	if(!_actionButtonPlaceholder) {
		// When animating UIImageView, the gesture taps doesn't work
		_actionButtonPlaceholder = [UIView newAutoLayoutView];
		_actionButtonPlaceholder.backgroundColor = [UIColor clearColor];
		_actionButtonPlaceholder.userInteractionEnabled = YES;
		_actionButtonPlaceholder.accessibilityLabel = @"actionButtonView";
	}
	
	return _actionButtonPlaceholder;
}

-(UIView *)buttonView {
	if(!_buttonView) {
		_buttonView = [UIView newAutoLayoutView];
		_buttonView.backgroundColor = ABA_COLOR_BLUE_MENU_UNIT;
		_buttonView.userInteractionEnabled = YES;
		_buttonView.accessibilityLabel = @"actionButtonView";
	}
	
	return _buttonView;
}

-(UIImageView *)buttonImage {
	if(!_buttonImage) {
		_buttonImage = [UIImageView newAutoLayoutView];
		[_buttonImage setImage:[UIImage imageNamed:[self getImageNameForCurrentType]]];
	}
	
	return _buttonImage;
}

-(void)switchToType: (kABAActionButtonType)type forSection:(kABASectionTypes)section
{
    _type = type;
    _sectionType = section;
    
    [_buttonImage setImage:[UIImage imageNamed:[self getImageNameForCurrentType]]];
    [_actionLabel setText:[self getLabelTextForCurrentTypeAndStatus]];
    
    if(_sectionType != kABAWriteSectionType)
    {
        switch (type) {
            case kABAActionButtonTypeRecord:
            {
				if(_sectionType != kABAInterpretSectionType) {
					[self startRecordingAnimation];
				}
				
                break;
            }
                
            case kABAActionButtonTypeListen:
            case kABAActionButtonTypeCompare:
            case kABAActionButtonTypeContinue:
            {
                [self removeButtonAnimations];
                break;
            }
            default:
                break;
        }
    }
}

-(NSString *)getImageNameForCurrentType
{
    switch(_type)
    {
        case kABAActionButtonTypeListen:
        {
            return @"escucharIconButton";
            break;
        }
        case kABAActionButtonTypeRecord:
        {
			return @"recordIconButton";
			
            break;
        }
        case kABAActionButtonTypeCompare:
        {
            return @"compararIconButton";
            
            break;
        }
        case kABAActionButtonTypeContinue:
        {
            return @"continuarIconButton";
            break;
        }
    }
    return nil;
}

-(NSString *)getLabelTextForCurrentTypeAndStatus
{
    switch(_type)
    {
        case kABAActionButtonTypeListen:
        {
            if(_status == kABAActionButtonStatusListening)
            {
                if(_sectionType == kABASpeakSectionType || _sectionType == kABAVocabularySectionType) {
                    return [STRTRAD(@"listenButtonSpeakVocKey", @"Escucha y repite") uppercaseString];
                }
                return [STRTRAD(@"listenButtonAssesKey", @"Lee y Escucha") uppercaseString];
            }
            else if(_sectionType == kABASpeakSectionType || _sectionType == kABAVocabularySectionType) {
                return [STRTRAD(@"listenButtonSpeakVocKey", @"Escucha y repite") uppercaseString];
            }
            else if(_sectionType == kABAAssessmentSectionType) {
                return [STRTRAD(@"listenButtonAssesKey", @"Lee y escucha") uppercaseString];
            }
			else if(_sectionType == kABAWriteSectionType) {
				return [STRTRAD(@"actionButtonCopyWriteKey", @"Escucha y escribe") uppercaseString];
			}
            else {
                return [STRTRAD(@"listenButtonAssesKey", @"Lee y escucha") uppercaseString];
            }
            
            break;
        }
        case kABAActionButtonTypeRecord:
        {
            return [STRTRAD(@"stopRecordingButtonKey", @"Parar tu grabación") uppercaseString];
            
            break;
        }
        case kABAActionButtonTypeCompare:
        {
            return [STRTRAD(@"sectionSpeakCompareLabelKey", @"Compara") uppercaseString];

            break;
        }
        case kABAActionButtonTypeContinue:
        {
            return [STRTRAD(@"sectionSpeakPlayLContinueKey", @"Continuar") uppercaseString];
            break;
        }
    }
}

- (void)handleTap:(id)sender
{
    switch(_type)
    {
        case kABAActionButtonTypeListen:
        {
            if([_delegate respondsToSelector:@selector(listenButtonTapped)])
                [self.delegate listenButtonTapped];
            break;
        }
        case kABAActionButtonTypeRecord:
        {
            if(_status == kABAActionButtonStatusRecording)
            {
                if([_delegate respondsToSelector:@selector(stopRecordButtonTapped)])
                    [self.delegate stopRecordButtonTapped];
            }
            else
            {
                if([_delegate respondsToSelector:@selector(startRecordButtonTapped)])
                    [self.delegate startRecordButtonTapped];
            }
            break;
        }
        case kABAActionButtonTypeCompare:
        {
            if([_delegate respondsToSelector:@selector(compareButtonTapped)])
                [self.delegate compareButtonTapped];
            break;
        }
        case kABAActionButtonTypeContinue:
        {
            if([_delegate respondsToSelector:@selector(continueButtonTapped)])
                [self.delegate continueButtonTapped];
            break;
        }
    }
}

-(void) removeButtonAnimations {
    [self.buttonView.layer removeAllAnimations];
}

- (void)startListeningAnimation
{
    _status = kABAActionButtonStatusListening;

    [_actionLabel setText:[self getLabelTextForCurrentTypeAndStatus]];
}

- (void)stopListeningAnimation
{
    _status = kABAActionButtonStatusReadyToListen;
    
    [_actionLabel setText:[self getLabelTextForCurrentTypeAndStatus]];
}

- (void)startRecordingAnimation
{
    _status = kABAActionButtonStatusRecording;
    
    [_buttonImage setImage:[UIImage imageNamed:[self getImageNameForCurrentType]]];
	_buttonView.backgroundColor = ABA_COLOR_RED_RECORD;

    [_actionLabel setText:[self getLabelTextForCurrentTypeAndStatus]];
    
    [self startFadeAnimation];
}

- (void)stopRecordingAnimation
{
    self.status = kABAActionButtonStatusReadyToRecord;
    
    [_actionLabel setText:[self getLabelTextForCurrentTypeAndStatus]];
	
    [self.buttonImage setImage:[UIImage imageNamed:[self getImageNameForCurrentType]]];
	_buttonView.backgroundColor = ABA_COLOR_BLUE_MENU_UNIT;

    [self.buttonView.layer removeAllAnimations];
    [self.buttonView setAlpha:1.0];
}

-(void)removeButtonTapRecognizer
{
    [self.buttonView removeGestureRecognizer:self.tapRecognizer];
}

-(void)addButtonTapRecognizer
{
    [self.buttonView addGestureRecognizer:self.tapRecognizer];
}

-(void)startFadeAnimation
{
    CABasicAnimation *buttonAnumation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    buttonAnumation.autoreverses = YES;
    buttonAnumation.duration = 0.5;
    buttonAnumation.repeatCount = HUGE_VALF;
    buttonAnumation.toValue = [NSNumber numberWithFloat:0.5];
    
    [self.buttonView.layer addAnimation:buttonAnumation forKey:@"opacity"];
}

-(void) showLabel:(BOOL)show {
    _actionLabel.hidden = !show;
}

-(void) setEnabled:(BOOL)enabled {
    if(!enabled)
        [self removeButtonTapRecognizer];
    else
        [self addButtonTapRecognizer];
}

@end
