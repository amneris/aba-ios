//
//  DataController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "DataController.h"
#import "ABARealmLevel.h"
#import "ABAUnit.h"
#import "ABARealmUser.h"
#import "ABARealmCert.h"
#import "APIManager.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "ABAFilm.h"
#import "ABASpeakDialog.h"
#import "ABAWriteDialog.h"
#import "ABASpeak.h"
#import "ABASpeakPhrase.h"
#import "ABASpeakDialogPhrase.h"
#import "ABAWrite.h"
#import "ABAPhrase.h"
#import "ABAInterpret.h"
#import "ABAInterpretPhrase.h"
#import "ABAInterpretRole.h"
#import "Utils.h"
#import "ABAVideoClass.h"
#import "ABAEvaluation.h"
#import "ABAEvaluationQuestion.h"
#import "ABAEvaluationOption.h"
#import "ABAVocabulary.h"
#import "ABAVocabularyPhrase.h"
#import "ABAExercises.h"
#import "ABAExercisesGroup.h"
#import "ABAExercisesPhrase.h"
#import "ABAExercisesQuestion.h"
#import "ABARealmProgressAction.h"
#import "ProgressController.h"
#import "SectionController.h"
#import "WriteController.h"
#import "InterpretController.h"
#import "EvaluationController.h"
#import "ABAError.h"
#import "SpeakController.h"
#import "LevelUnitController.h"
#import "ExercisesController.h"
#import "VocabularyController.h"
#import "DataManager.h"
#import "ABAExperiment.h"
#import "NSDateFormatter+Locale.h"
#import "ABAFilmDataController.h"
#import "ABASpeakDataController.h"
#import "ABAVideoClassDataController.h"

// DataControllers, delete after migration
#import "ABASpeakDataController.h"
#import "ABAWriteDataController.h"
#import "ABAExercisesDataController.h"
#import "ABAFilmDataController.h"
#import "ABAVideoClassDataController.h"
#import "ABAInterpretDataController.h"
#import "ABAVocabularyDataController.h"
#import "ABAEvaluationDataController.h"

// Traking
#import "LegacyTrackingManager.h"
#import <Crashlytics/Crashlytics.h>


@implementation DataController

+ (RLMResults *)getLevelsAndUnits
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    return [ABARealmLevel allObjectsInRealm:realm];
}

+ (RLMResults *)getUnits
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    return [ABAUnit allObjectsInRealm:realm];
}

+ (void)getSectionForUnit:(ABAUnit *)unitttt
                 withLang:(NSString *)locale
               completion:(CompletionBlock) block {

    // Check if we have already the section in coredata
    if (![unitttt shouldDownloadSectionData])
    {
        block(nil, unitttt);
        return;
    }
    
    NSLog(@"UNIT DOES NOT HAVE ALL THE DATA");
    
    NSNumber * unitID = unitttt.idUnit;
    
    //if we reach here, no coredata found, go to the fucking network
    [[APIManager sharedManager] getSectionContentWithLocale:locale andUnitID:unitttt.idUnitString completionBlock:^(NSError *error, id object)
     {
         if (!error)
         {
             ABAUnit * unitToUpdate = [LevelUnitController getUnitWithID:unitID];
             
             NSArray *unitContent = (NSArray*)object;
             
             __block int successCount = 0;
             __block int totalCalls = 8;
             
             for(NSDictionary* sectionContent in unitContent)
             {
                 if ([[sectionContent allKeys] containsObject:@"AbaFilm"])
                 {
                     NSDictionary *abaFilmContent = [[sectionContent objectForKey:@"AbaFilm"] objectAtIndex:0];
                     
                     [ABAFilmDataController createABAFilmWithDictionary:abaFilmContent onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [DataController syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Speak"])
                 {
                     [ABASpeakDataController parseABASpeakSection:[sectionContent objectForKey:@"Speak"] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [DataController syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Write"])
                 {
                     [ABAWriteDataController parseABAWriteSection:[sectionContent objectForKey:@"Write"] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [self syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Interpret"])
                 {
                     [ABAInterpretDataController parseABAInterpretSection:[sectionContent objectForKey:@"Interpret"] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [self syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Exercises"])
                 {
                     [ABAExercisesDataController parseABAExercisesSection:[sectionContent objectForKey:@"Exercises"] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [self syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Vocabulary"])
                 {
                     [ABAVocabularyDataController parseABAVocabularySection:[sectionContent objectForKey:@"Vocabulary"] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [self syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Assesment"])
                 {
                     [ABAEvaluationDataController parseABAEvaluationSection:[sectionContent objectForKey:@"Assesment"] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [self syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else if ([[sectionContent allKeys] containsObject:@"Video Classes"])
                 {
                     [ABAVideoClassDataController createABAVideoClassWithDictionary:[[sectionContent objectForKey:@"Video Classes"] objectAtIndex:0] onUnit:unitToUpdate completionBlock:^(NSError *error, id object)
                      {
                          if (!error)
                          {
                              successCount++;
                              
                              if (successCount == totalCalls)
                              {
                                  if ([[DataManager sharedManager] progressCache] != nil)
                                  {
                                      [self syncUnitProgress:[[DataManager sharedManager] progressCache] completionBlock:block];
                                  }
                                  else
                                  {
                                      block(nil, nil);
                                  }
                              }
                          }
                          else
                          {
                              block(error, nil);
                          }
                      }];
                 }
                 else
                 {
                     block([ABAError errorWithKey:@"unexpectedSectionContent"], nil);
                 }
             }
         }
         else
         {
             block(error,nil);
         }
     }];
}

+ (void)setCompletedPhrases:(NSArray *)phrases completionBlock:(CompletionBlock)completionBlock
{
    NSError *error;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    for (ABAPhrase *phrase in phrases) {
        phrase.listened = [NSNumber numberWithInt:1];
        phrase.done = [NSNumber numberWithInt:1];
    }

    [realm commitWriteTransaction:&error];

    if (!error)
    {
        completionBlock(nil, phrases);
    }
    else
    {
        completionBlock(error, nil);
    }
}

+ (void)setPhrasesListened:(NSArray *)phrases completionBlock:(CompletionBlock)block
{
    NSError *error;

    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    for (ABAPhrase *phrase in phrases) {
        phrase.listened = [NSNumber numberWithBool:YES];
    }
    
    [realm commitWriteTransaction:&error];
    
    if (!error)
    {
        block(nil, phrases);
    }
    else
    {
        block(error, nil);
    }
}

+ (float)getProgressForSection:(ABASection *)section
{
    switch ([section getSectionType]) {
        case kABAFilmSectionType:
            return [section.progress integerValue];
            break;
        case kABASpeakSectionType:
        {
            SpeakController *speakController = [SpeakController new];
            return [speakController getProgressForSection:(ABASpeak *)section];
            break;
        }
        case kABAWriteSectionType:
        {
            WriteController *writeController = [WriteController new];
            return [writeController getProgressForSection:(ABAWrite *)section];
        }
        case kABAInterpretSectionType:
        {
            InterpretController *interpretController = [InterpretController new];
            return [interpretController getProgressForSection:(ABAInterpret *)section];
        }
        case kABAVideoClassSectionType:
        {
            return [section.progress integerValue];
        }
        case kABAExercisesSectionType:
        {
            ExercisesController *exercisesController = [ExercisesController new];
            return [exercisesController getProgressForSection:(ABAExercises *)section];
        }
        case kABAVocabularySectionType:
        {
            VocabularyController *vocabularyController = [VocabularyController new];
            return [vocabularyController getProgressForSection:(ABAVocabulary *)section];
        }
        case kABAAssessmentSectionType:
        {
            return [section.progress integerValue];
        }
        default:
        {
            return 0;
            break;
        }
    }
}

+ (void)updateProgressUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)completionBlock
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;

    [realm transactionWithBlock:^{

        float filmProgress = [self getProgressForSection:unit.sectionFilm];
        float speakProgress = [self getProgressForSection:unit.sectionSpeak];
        float writeProgress = [self getProgressForSection:unit.sectionWrite];
        float interpretProgress = [self getProgressForSection:unit.sectionInterpret];
        float videoClassProgress = [self getProgressForSection:unit.sectionVideoClass];
        float exercisesProgress = [self getProgressForSection:unit.sectionExercises];
        float vocabularyProgress = [self getProgressForSection:unit.sectionVocabulary];
        float evaluationProgress = [self getProgressForSection:unit.sectionEvaluation];

        float unitSectionProgress = (filmProgress + speakProgress + writeProgress + interpretProgress + videoClassProgress + exercisesProgress + vocabularyProgress + evaluationProgress) / 8;

        if ([[NSNumber numberWithFloat:unitSectionProgress] integerValue] > 100)
            unitSectionProgress = 100;

        if ([[NSNumber numberWithFloat:unitSectionProgress] integerValue] > [unit.progress integerValue])
        {
            unit.progress = [NSNumber numberWithFloat:unitSectionProgress];
        }

    } error:&error];

    if (!error)
    {
        completionBlock(nil, unit);
    }
    else
    {
        completionBlock(error, nil);
    }
}

+ (void)setCompletedSection:(ABASection *)section completionBlock:(CompletionBlock)completionBlock {
    
    __block ABAUnit *currentUnit;
    
    switch ([section getSectionType]) {
        case kABAFilmSectionType: {
            ABAFilm *film = (ABAFilm *)section;
            currentUnit = film.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABASpeakSectionType: {
            ABASpeak *speak = (ABASpeak *)section;
            currentUnit = speak.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABAWriteSectionType: {
            ABAWrite *write = (ABAWrite *)section;
            currentUnit = write.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABAInterpretSectionType: {
            ABAInterpret *interpret = (ABAInterpret *)section;
            currentUnit = interpret.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABAVideoClassSectionType: {
            ABAVideoClass *video = (ABAVideoClass *)section;
            currentUnit = video.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABAExercisesSectionType: {
            ABAExercises *exercises = (ABAExercises *)section;
            currentUnit = exercises.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABAVocabularySectionType: {
            ABAVocabulary *vocabulary = (ABAVocabulary *)section;
            currentUnit = vocabulary.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        case kABAAssessmentSectionType: {
            ABAEvaluation *evaluation = (ABAEvaluation *)section;
            currentUnit = evaluation.unit;
            [[LegacyTrackingManager sharedManager] trackInteraction];
            break;
        }
        default:
            break;
    }
    
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];

    section.completed = [NSNumber numberWithInt:1];
    section.progress = [NSNumber numberWithInt:100];
    
    [realm commitWriteTransaction:&error];
    
    if (!error)
    {
        [DataController updateProgressUnit:currentUnit completionBlock:^(NSError *error, id object) {
            completionBlock(nil, section);
        }];
    }
    else
    {
        completionBlock(error, nil);
    }
}

+ (void)setLevelProgress:(ABARealmLevel *)level newProgress:(NSNumber *)progress completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [realm beginWriteTransaction];
    level.progress = progress;
    [realm commitWriteTransaction:&error];

    block(error, level);
}

+ (void)saveProgressActionForSection:(ABASection *)section andUnit:(ABAUnit *)unit andPhrases:(NSArray *)phrases errorText:(NSString *)text isListen: (BOOL) listen isHelp: (BOOL)help completionBlock:(CompletionBlock)block
{
    NSMutableArray *actions = [NSMutableArray arrayWithCapacity:[phrases count]];
    ABARealmUser * user = [UserController currentUser];
    NSDateFormatter *format = [[NSDateFormatter alloc] initWithSafeLocale];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    if (phrases == nil)
    {
        ABARealmProgressAction *action = [ABARealmProgressAction progressActionWithUser:user dateFormatter:format phrase:nil section:section unit:unit errorText:text isListen:listen isHelp:help];

        [actions addObject:action];
    }
    else
    {
        for (ABAPhrase *phrase in phrases)
        {
            ABARealmProgressAction *action = [ABARealmProgressAction progressActionWithUser:user dateFormatter:format phrase:phrase section:section unit:unit errorText:text isListen:listen isHelp:help];

            [actions addObject:action];
        }
    }

    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)
    
    // Add to Realm with transaction
    [realm beginWriteTransaction];
    [realm addOrUpdateObjectsFromArray:actions.copy];
    [realm commitWriteTransaction];
    
    if (block) {
        block(nil, actions.copy);
    }
}

+ (RLMResults *)getPendingProgressActions
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    // Actions with `audioID = nil && isHelp = (null)` should not be sent
    RLMResults *pendingLegitProgressActions = [ABARealmProgressAction objectsInRealm:realm where:@"sentToServer = false && (audioID != nil || isHelp != nil)"];
    RLMResults *sortedPendingProgressActions = [pendingLegitProgressActions sortedResultsUsingProperty:@"timeStamp" ascending:YES];

    return sortedPendingProgressActions;
}

+ (void)setProgressActionsAsSent:(NSArray *)progressActionsIds
{
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)

    RLMResults *progressActions = [ABARealmProgressAction objectsWhere:@"actionId IN %@", progressActionsIds];

    [realm beginWriteTransaction];

    for (ABARealmProgressAction *action in progressActions)
    {
        action.sentToServer = @YES;
    }

    [realm commitWriteTransaction];
}

+ (void)syncCompletedActions:(NSArray *)completedActions forUnit:(ABAUnit *)unit
{
    for (NSDictionary *section in completedActions)
    {
        NSNumber *sectionType = [section objectForKey:@"SectionTypeId"];
        NSArray *actions = [section objectForKey:@"Elements"];

        switch (sectionType.integerValue)
        {
        case kABASpeakSectionType:
        {
            SpeakController *speakController = [SpeakController new];
            if (![speakController isSectionCompleted:unit.sectionSpeak])
            {
                [speakController syncCompletedActions:actions
                                          withSection:unit.sectionSpeak];
            }
            break;
        }
        case kABAWriteSectionType:
        {
            WriteController *writeController = [WriteController new];
            if (![writeController isSectionCompleted:unit.sectionWrite])
            {
                [writeController syncCompletedActions:actions
                                          withSection:unit.sectionWrite];
            }

            break;
        }
        case kABAInterpretSectionType:
        {
            InterpretController *interpretController = [InterpretController new];

            if (![interpretController isSectionCompleted:unit.sectionInterpret])
            {
                [interpretController syncCompletedActions:actions
                                              withSection:unit.sectionInterpret];
            }
            break;
        }
        case kABAExercisesSectionType:
        {
            ExercisesController *exercisesController = [ExercisesController new];

            if (![exercisesController isSectionCompleted:unit.sectionExercises])
            {
                [exercisesController syncCompletedActions:actions
                                              withSection:unit.sectionExercises];
            }

            break;
        }
        case kABAVocabularySectionType:
        {
            VocabularyController *vocabularyController = [VocabularyController new];

            if (![vocabularyController isSectionCompleted:unit.sectionVocabulary])
            {
                [vocabularyController syncCompletedActions:actions
                                               withSection:unit.sectionVocabulary];
            }
            break;
        }
        case kABAFilmSectionType:
        case kABAVideoClassSectionType:
        default:
            break;
        }
    }
}

+ (void)syncUnitProgress:(NSArray *)unitProgress completionBlock:(CompletionBlock)block
{
    __block NSMutableArray *unitsToComplete = [[NSMutableArray alloc] init];
    __block NSMutableDictionary *unitsDict = [[NSMutableDictionary alloc] init];
    __strong typeof(self) strongSelf = self;
    
    // We fetch needed units before
    NSMutableArray *unitsToSync = [NSMutableArray new];
    
    for(NSDictionary *progressData in unitProgress)
    {
        NSString *unitID = [progressData objectForKey:@"UnitId"];
        
        if (unitID == nil)
        {
            unitID = [progressData objectForKey:@"IdUnit"];
        }
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *unitIDNumber = [f numberFromString:unitID];
        
        if (!unitIDNumber) {
            continue;
        }
        
        ABAUnit *unit = [LevelUnitController getUnitWithID:unitIDNumber];
        
        if (unit == nil) {
            NSLog(@"UNIT IS NIL: %@", unitID);
            continue;
        }
        
        [unitsToSync addObject:unitID];
        
        [unitsDict setObject:unit forKey:unitID];
    }
    
    
    if ([[APIManager sharedManager] syncInProgress] || [unitsToSync count] == 0)
    {
        NSDictionary * progressData = [unitProgress lastObject];
        NSString *unitID = [progressData objectForKey:@"UnitId"];
        
        if (unitID == nil) {
            unitID = [progressData objectForKey:@"IdUnit"];
        }
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *unitIDNumber = [f numberFromString:unitID];
        
        if (!unitIDNumber) {
            return;
        }
        
        ABAUnit *unit = [LevelUnitController getUnitWithID:unitIDNumber];
        
        if (unit == nil) {
            NSLog(@"UNIT IS NIL: %@", unitID);
            return;
        }

        block(nil,unit);
        
        return;
    }
    
    [[DataManager sharedManager] setProgressCache:unitProgress];
    
    [[APIManager sharedManager] setSyncInProgress:YES];
    
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)
    NSError *error;
    
    [realm beginWriteTransaction];

    
    for(NSDictionary *progressData in unitProgress)
    {
        
        NSString *unitID = [progressData objectForKey:@"UnitId"];
        if (unitID == nil)
        {
            unitID = [progressData objectForKey:@"IdUnit"];
        }
        
        ABAUnit *unit = [unitsDict objectForKey:unitID];
        
        if (unit == nil) {
            NSLog(@"UNIT IS NIL: %@", unitID);
            continue;
        }
        NSInteger filmProgress = [[progressData objectForKey:@"AbaFilmPct"] integerValue];
        NSInteger speakProgress = [[progressData objectForKey:@"SpeakPct"] integerValue];
        NSInteger writeProgress = [[progressData objectForKey:@"WritePct"] integerValue];
        NSInteger interpretProgress = [[progressData objectForKey:@"InterpretPct"] integerValue];
        NSInteger videoClassProgress = [[progressData objectForKey:@"VideoClassPct"] integerValue];
        NSInteger exercisesProgress = [[progressData objectForKey:@"ExercisesPct"] integerValue];
        NSInteger vocabularyProgress = [[progressData objectForKey:@"VocabularyPct"] integerValue];
        NSInteger evaluationProgress = [[progressData objectForKey:@"AssessmentPct"] integerValue];
        
        float unitSectionProgress = filmProgress + speakProgress + writeProgress + interpretProgress + videoClassProgress + exercisesProgress + vocabularyProgress + evaluationProgress;
        
        unit.unitSectionProgress = [NSNumber numberWithFloat:unitSectionProgress];
        
        if (filmProgress > unit.sectionFilm.progress.integerValue) {
            unit.sectionFilm.progress = [NSNumber numberWithInteger:filmProgress];
            if (filmProgress >= 100)
                unit.sectionFilm.completed = [NSNumber numberWithBool:YES];
        }
        
        if (speakProgress > unit.sectionSpeak.progress.integerValue) {
            unit.sectionSpeak.progress = [NSNumber numberWithInteger:speakProgress];
            if (speakProgress >= 100)
                unit.sectionSpeak.completed = [NSNumber numberWithBool:YES];
        }
        
        if (writeProgress > unit.sectionWrite.progress.integerValue) {
            unit.sectionWrite.progress = [NSNumber numberWithInteger:writeProgress];
            if (writeProgress >= 100)
                unit.sectionWrite.completed = [NSNumber numberWithBool:YES];
        }
        
        if (interpretProgress > unit.sectionInterpret.progress.integerValue) {
            unit.sectionInterpret.progress = [NSNumber numberWithInteger:interpretProgress];
            if (interpretProgress >= 100)
                unit.sectionInterpret.completed = [NSNumber numberWithBool:YES];
        }
        
        if (videoClassProgress > unit.sectionVideoClass.progress.integerValue) {
            unit.sectionVideoClass.progress = [NSNumber numberWithInteger:videoClassProgress];
            if (videoClassProgress >= 100)
                unit.sectionVideoClass.completed = [NSNumber numberWithBool:YES];
        }
        
        if (exercisesProgress > unit.sectionExercises.progress.integerValue) {
            unit.sectionExercises.progress = [NSNumber numberWithInteger:exercisesProgress];
            if (exercisesProgress >= 100)
                unit.sectionExercises.completed = [NSNumber numberWithBool:YES];
        }
        
        if (vocabularyProgress > unit.sectionVocabulary.progress.integerValue) {
            unit.sectionVocabulary.progress = [NSNumber numberWithInteger:vocabularyProgress];
            if (vocabularyProgress >= 100)
                unit.sectionVocabulary.completed = [NSNumber numberWithBool:YES];
        }
        
        if (evaluationProgress > unit.sectionEvaluation.progress.integerValue) {
            unit.sectionEvaluation.progress = [NSNumber numberWithInteger:evaluationProgress];
            if (evaluationProgress >= 100)
                unit.sectionEvaluation.completed = [NSNumber numberWithBool:YES];
        }
        
        NSInteger totalProgress = unitSectionProgress/8;
        
        if (totalProgress > 100)
            totalProgress = 100;
        
        if (totalProgress > unit.progress.integerValue)
        {
            unit.progress = [NSNumber numberWithInteger:totalProgress];
            
            if (totalProgress >= 100 && ![unit.completed boolValue])
            {
                [unitsToComplete addObject:unit.idUnit];
            }
        }
    }
    
    [realm commitWriteTransaction:&error];
    
    if (error)
    {
        block(error, nil);
        return;
    }

    
    NSDictionary * progressData = [unitProgress lastObject];
    NSString *unitID = [progressData objectForKey:@"UnitId"];
    
    if (unitID == nil) {
        unitID = [progressData objectForKey:@"IdUnit"];
    }
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *unitIDNumber = [f numberFromString:unitID];
    
    ABAUnit *unit = [LevelUnitController getUnitWithID:unitIDNumber];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUnitViewProgressUpdatedNotification object:nil];
    
    if ([unitsToComplete count] > 0)
    {
        [strongSelf completeUnits:unitsToComplete completionBlock:^(NSError *error, id object)
         {
             if (!error)
             {
                 [LevelUnitController refreshAllLevelsProgress:^(NSError *error, id object)
                  {
                      if (!error)
                      {
                          [[APIManager sharedManager] setSyncInProgress:NO];
                          [[NSNotificationCenter defaultCenter] postNotificationName:kLevelProgressUpdatedNotification object:nil];
                      }
                      
                      block(nil, unit);
                  }];
             }
             else
             {
                 block(error, nil);
             }
         }];
    }
    else
    {
        [LevelUnitController refreshAllLevelsProgress:^(NSError *error, id object)
         {
             if (!error)
             {
                 [[APIManager sharedManager] setSyncInProgress:NO];
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:kLevelProgressUpdatedNotification object:nil];
             }
             
             block(nil, unit);
         }];
    }
}

+ (void)completeUnits:(NSArray *)unitIdsArray completionBlock:(CompletionBlock)block
{
    __block NSInteger totalUnitsToComplete = [unitIdsArray count];
    __block NSInteger unitsProcessed = 0;
    
    if (totalUnitsToComplete == 0)
    {
        block(nil,nil);
    }
    else
    {
        for(NSNumber *idUnit in unitIdsArray)
        {
            ABAUnit *unit = [LevelUnitController getUnitWithID:idUnit];
            [LevelUnitController completeUnit:unit
                                 forUser:[UserController currentUser]
                         completionBlock:^(NSError *error, id object)
             {
                 if (!error)
                 {
                     unitsProcessed++;
                     
                     if (unitsProcessed == totalUnitsToComplete)
                     {
                         block(nil,nil);
                     }
                 }
                 else
                 {
                     block(error,nil);
                 }
             }];
        }
    }
}

+ (void)getAllUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang completionBlock:(CompletionBlock)block
{
    [[APIManager sharedManager] getAllUnitsWithToken:token withLanguage:userLang completionBlock:^(NSError *error, id object)
     {
         if (!error)
         {
             [self parseAllUnitsWithObject:object completionBlock:^(NSError *error, id object)
              {
                  if (!error)
                  {
                      block(nil, nil);
                  }
                  else
                  {
                      block(error, nil);
                  }
              }];
         }
         else
         {
             block(error, nil);
         }
     }];
}

+ (void)parseAllUnitsWithObject:(id)responseObject completionBlock:(CompletionBlock)block
{
    NSArray *unitsArray = responseObject;
    
    if ([unitsArray count] != 144)
    {
        block([ABAError errorWithKey:STRTRAD(@"errorGettingUnitsFromWS", @"Inconsistencia - numero de unidades no válido")],nil);
        
        return;
    }
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    [realm transactionWithBlock:^{
        
        for(NSArray *unitArray in unitsArray)
        {
            // TOTAL: 7 objects for unit
            // 1 - NSDictionary - Description, Title, idUnit
            // 2 - NSDictionary - Roles (and inside is a array of different NSDictionaries)
            // 3 - NSDictionary - AbaFilmImage
            // 4 - NSDictionary - AbaFilmImageInactive
            // 5 - NSDictionary - VideoClassImage
            // 6 - NSDictionary - UnitImage
            // 7 - NSDictionary - UnitImageInactive
            int index = 1;
            
            ABAUnit *unit = [[ABAUnit alloc] init];
            
            if ([unitArray count] != 7)
            {
                block([ABAError errorWithKey:STRTRAD(@"errorGettingInfoUnitFromWS", @"Inconsistencia - información no coherente de unidad")],nil);
                break;
            }
            else
            {
                for(id object in unitArray)
                {
                    NSDictionary *unitDict = object;
                    
                    switch(index)
                    {
                        case 1: // 1 - NSDictionary - Description, Title, idUnit
                        {
                            unit.idUnit    = unitDict[@"idUnit"];
                            unit.title     = unitDict[@"Title"];
                            unit.desc      = unitDict[@"Description"];
                            
                            NSString *idLevel = [LevelUnitController getIdLevelForIdUnit:unitDict[@"idUnit"]];
                            ABARealmLevel *level = [LevelUnitController getLevelWithID:idLevel];
                            
                            [level.units addObject:unit];
                            
                            break;
                        }
                        case 2: // 2 - NSDictionary - Roles (and inside it is a array of different NSDictionaries)
                        {
                            NSArray *rolesArray = unitDict[@"Roles"];
                            NSMutableDictionary *rolesDict = [[NSMutableDictionary alloc] init];
                            
                            for(NSDictionary *roleData in rolesArray)
                            {
                                if ([rolesDict objectForKey:roleData[@"RoleName"]] == nil)
                                {
                                    ABARole *role = [[ABARole alloc] init];
                                    role.name = roleData[@"RoleName"];
                                    
                                    [unit.roles addObject:role];
                                    
                                    if (roleData[@"RoleSmallImageUrl"] != nil)
                                    {
                                        role.imageUrl = roleData[@"RoleSmallImageUrl"];
                                    }
                                    
                                    if (roleData[@"RoleBigImageUrl"] != nil)
                                    {
                                        role.imageBigUrl = roleData[@"RoleBigImageUrl"];
                                    }
                                    
                                    [rolesDict setObject:role forKey:roleData[@"RoleName"]];
                                }
                                else
                                {
                                    ABARole *role = [rolesDict objectForKey:roleData[@"RoleName"]];
                                    
                                    if (roleData[@"RoleSmallImageUrl"] != nil)
                                    {
                                        role.imageUrl = roleData[@"RoleSmallImageUrl"];
                                    }
                                    
                                    if (roleData[@"RoleBigImageUrl"] != nil)
                                    {
                                        role.imageBigUrl = roleData[@"RoleBigImageUrl"];
                                    }
                                    
                                    [rolesDict setObject:role forKey:roleData[@"RoleName"]];
                                }
                            }
                            
                            break;
                        }
                        case 3: // 3 - NSDictionary - AbaFilmImage
                        {
                            unit.filmImageUrl = unitDict[@"AbaFilmImage"];
                            
                            break;
                        }
                        case 4: // 4 - NSDictionary - AbaFilmImageInactive
                        {
                            unit.filmImageInactiveUrl = unitDict[@"AbaFilmImageInactive"];
                            
                            break;
                        }
                        case 5: // 5 - NSDictionary - VideoClassImage
                        {
                            unit.videoClassImageUrl = unitDict[@"VideoClassImage"];
                            
                            break;
                        }
                        case 6: // 6 - NSDictionary - UnitImage
                        {
                            unit.unitImage = unitDict[@"UnitImage"];
                            
                            break;
                        }
                        case 7: // 7 - NSDictionary - UnitImageInactive
                        {
                            unit.unitImageInactive = unitDict[@"UnitImageInactive"];
                            
                            break;
                        }
                    }
                    
                    index++;
                }
                
                // create all sections
                unit.sectionFilm = [[ABAFilm alloc] init];
                unit.sectionSpeak = [[ABASpeak alloc] init];
                unit.sectionWrite = [[ABAWrite alloc] init];
                unit.sectionInterpret = [[ABAInterpret alloc] init];
                unit.sectionVideoClass = [[ABAVideoClass alloc] init];
                unit.sectionExercises = [[ABAExercises alloc] init];
                unit.sectionVocabulary = [[ABAVocabulary alloc] init];
                unit.sectionEvaluation= [[ABAEvaluation alloc] init];
                
                [realm addObject:unit];
            }
        }
        
    } error:&error];
    
    block(error, nil);

}

+ (void)updateLastChangedUnit:(ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm transactionWithBlock:^{

        NSDate *currentTime = [NSDate date];
        unit.lastChanged = currentTime;
    }];
}

+ (void)cleanEntitiesWithCompletionBlock: (ErrorBlock)block
{
    CLS_LOG(@"DataController: Flushing database");
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)
    NSError *error;
    
    [realm beginWriteTransaction];

    [realm deleteObjects:[ABARealmUser allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAEvaluation allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAEvaluationOption allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAEvaluationQuestion allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAExercises allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAExercisesGroup allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAExercisesQuestion allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAExercisesPhrase allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAFilm allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAInterpret allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAInterpretPhrase allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAInterpretRole allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAPhrase allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABASection allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABASpeak allObjectsInRealm:realm]];
    [realm deleteObjects:[ABASpeakDialog allObjectsInRealm:realm]];
    [realm deleteObjects:[ABASpeakDialogPhrase allObjectsInRealm:realm]];
    [realm deleteObjects:[ABASpeakPhrase allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAVideoClass allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAVocabulary allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAVocabularyPhrase allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABAWrite allObjectsInRealm:realm]];
    [realm deleteObjects:[ABAWriteDialog allObjectsInRealm:realm]];
    
    [realm deleteObjects:[ABARole allObjectsInRealm:realm]];
    
    // Removing all units since unit´s text might be changed due to new user language
    [realm deleteObjects:[ABAUnit allObjectsInRealm:realm]];
    
    RLMResults *ABARealmLevels = [ABARealmLevel allObjectsInRealm:realm];
    
    for(ABARealmLevel *level in ABARealmLevels)
    {
        level.completed = [NSNumber numberWithInt:0];
        level.progress = [NSNumber numberWithInt:0];
    }
    
    [realm commitWriteTransaction:&error];
    
    block(error);
}

@end
