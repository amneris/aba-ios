//
//  UnitDownloader.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

enum UnitDownloaderError: Swift.Error, CustomStringConvertible {
    var description: String { return "Unit downloader error.." }
    // Thrown when there is an error during the unit and level creation process
    case unableToDownloadUnits
}

protocol UnitDownloader {
    func downloadUnits(_ userDataModel: UserDataModel) -> Observable<User>
}

extension UnitDownloader {

    func downloadUnits(_ userDataModel: UserDataModel) -> Observable<User> {
        return Observable.create { (observer) -> Disposable in
            LevelUnitController.checkForUnits(withToken: userDataModel.token, withLanguage: userDataModel.userLang, andBlock: { leveUnitError in
                guard leveUnitError == nil else {
                    observer.onError(leveUnitError!)
                    return
                }
                
                do {
                    UserDataController.createUser(withUserModel: try userDataModel.toDict()) { userDataError in
                        guard userDataError == nil else {
                            observer.onError(userDataError!)
                            return
                        }
                        
                        // Setting the new language
                        LanguageController.setCurrentLanguage(userDataModel.userLang)
                        
                        // Notifying
                        observer.on(.next(userDataModel))
                        observer.onCompleted()
                    }
                    
                } catch {
                    observer.onError(UnitDownloaderError.unableToDownloadUnits)
                }
            })

            return Disposables.create()
            
        }.catchError { _ in
            throw UnitDownloaderError.unableToDownloadUnits
        }
    }
}
