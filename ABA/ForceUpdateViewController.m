//
//  ForceUpdateViewController.m
//  
//
//  Created by Oleksandr Gnatyshyn on 29/01/16.
//
//

#import "ForceUpdateViewController.h"
#import "Resources.h"
#import "Utils.h"
#import "LegacyTrackingManager.h"

@interface ForceUpdateViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *goToStore;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightConstraint;

@end

@implementation ForceUpdateViewController {
    float CONSTANT_titleFontSize;
    float CONSTANT_cornerRadius;
    float CONSTANT_buttonFontSize;
    float CONSTANT_buttonKern;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupConstants];
    [self setupView];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"Force Update"];
}

- (void)setupConstants
{
    if (IS_IPAD) {
        CONSTANT_titleFontSize = 48.0;
        CONSTANT_cornerRadius = 6;
        CONSTANT_buttonFontSize = 18.0;
        CONSTANT_buttonKern = 4;
        
        if ([[LanguageController getCurrentLanguage] isEqualToString:@"ru"]) {
            _titleWidthConstraint.constant = 600;
        }
        else {
            _titleWidthConstraint.constant = 400;
        }
        
        _logoWidthConstraint.constant = _logoWidthConstraint.constant * 1.3;
        _logoHeightConstraint.constant = _logoHeightConstraint.constant * 1.3;
        
        [self.view layoutIfNeeded];
        [self.view updateConstraintsIfNeeded];
    }
    else {
        CONSTANT_titleFontSize = 30.0;
        CONSTANT_cornerRadius = 3;
        CONSTANT_buttonFontSize = 14.0;
        CONSTANT_buttonKern = 3;
    }
}

#pragma mark - Custom methods

- (void)setupView
{
    [_titleLabel setTextColor:[UIColor whiteColor]];
    NSString *line0 = STRTRAD(@"blockAppTitle1Key", @"Necesitas actualizar");
    NSString *line1 = STRTRAD(@"blockAppTitle2Key", @"inglés online");
    NSString *str = [NSString stringWithFormat:@"%@\n%@", line0, line1];
    
    float spacing = 7.0f;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = spacing;
    
    float spacing2 = 2.0f;
    NSMutableParagraphStyle *paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle2.lineSpacing = spacing2;
    
    BOOL likeVodka = [[LanguageController getCurrentLanguage] isEqualToString:@"ru"] && !IS_IPAD;
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SLAB_500 size:likeVodka?24:CONSTANT_titleFontSize]
                      range:[str rangeOfString:line0]];
    [mutAttStr addAttribute:NSParagraphStyleAttributeName
                      value:paragraphStyle2
                      range:[str rangeOfString:line0]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SLAB_500 size:likeVodka?24:CONSTANT_titleFontSize]
                      range:[str rangeOfString:line1]];
    [mutAttStr addAttribute:NSParagraphStyleAttributeName
                      value:paragraphStyle
                      range:[str rangeOfString:line1]];
    
    [_titleLabel setAttributedText:mutAttStr.copy];

    [_goToStore setBackgroundColor:ABA_COLOR_BLUE];
    [_goToStore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _goToStore.layer.cornerRadius = CONSTANT_cornerRadius;
    _goToStore.layer.masksToBounds = YES;
    
    NSString *titleButton = [STRTRAD(@"goToStoreKey", @"Ir a App Store") uppercaseString];
    NSMutableAttributedString *mutAttStr2 = [[NSMutableAttributedString alloc] initWithString:titleButton];
    [mutAttStr2 addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_RALEWAY_BOLD size:CONSTANT_buttonFontSize]
                       range:[titleButton rangeOfString:titleButton]];
    [mutAttStr2 addAttribute:NSKernAttributeName
                       value:[NSNumber numberWithFloat:CONSTANT_buttonKern]
                       range:NSMakeRange(0, [titleButton length])];
    [mutAttStr2 addAttribute:NSForegroundColorAttributeName
                       value:[UIColor whiteColor]
                       range:[titleButton rangeOfString:titleButton]];
    
    [_goToStore setAttributedTitle:mutAttStr2.copy forState:UIControlStateNormal];
}

- (IBAction)goToStore:(id)sender
{
    NSURL* itunesLink = [NSURL URLWithString:@"https://itunes.apple.com/es/app/aprender-ingles-con-peliculas/id859243872?mt=8"];
    [[UIApplication sharedApplication] openURL:itunesLink];
}


@end
