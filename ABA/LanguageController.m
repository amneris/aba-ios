//
//  LanguageController.m
//  ABA
//
//  Created by Oriol Vilaró on 13/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "DataController.h"
#import "DataManager.h"
#import "LanguageController.h"
#import "UserDataController.h"
#import "Utils.h"

@implementation LanguageController

#pragma mark - Public static methods

static NSString *_currentLanguage = nil;

+ (NSString *)getCurrentLanguage
{
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _currentLanguage = [LanguageController readCurrentLanguageFromDefaults];
    });

    return _currentLanguage;
}

+ (void)setCurrentLanguage:(NSString *)newLanguage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:newLanguage forKey:kCurrentLanguageKey];
    [defaults synchronize];
    _currentLanguage = newLanguage;
}

+ (NSString *)getDeviceLanguageCodeKey
{
    NSString *language = [NSLocale preferredLanguages][0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = languageDic[@"kCFLocaleLanguageCodeKey"];
    return languageCode;
}

+ (void)resetCurrentLanguage
{
    _currentLanguage = [self readSupportedLanguageFromDeviceConfiguration];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_currentLanguage forKey:kCurrentLanguageKey];
    [defaults synchronize];
}

#pragma mark - Private static methods

+ (NSString *)readCurrentLanguageFromDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentLanguage = [defaults objectForKey:kCurrentLanguageKey];

    if (currentLanguage == nil)
    {
        currentLanguage = [self readSupportedLanguageFromDeviceConfiguration];

        [defaults setObject:currentLanguage forKey:kCurrentLanguageKey];
        [defaults synchronize];
    }

    return currentLanguage;
}

+ (NSString *)readSupportedLanguageFromDeviceConfiguration
{
    NSString *languageCode = [self getDeviceLanguageCodeKey];
    NSString *supportedLanguage = nil;

    if (languageCode)
    {
        // normalising catalan into spanish
        if ([languageCode isEqualToString:@"ca"])
        {
            languageCode = @"es";
        }

        NSArray *languageArray = kSuportedLanguagesArray;
        for (NSString *lang in languageArray)
        {
            if ([lang isEqualToString:languageCode])
            {
                supportedLanguage = languageCode;
                break;
            }
        }
    }

    if (!supportedLanguage)
    {
        supportedLanguage = kDefaultLanguage;
    }

    return supportedLanguage;
}

+ (NSArray *)getSupportedLanguages
{
    return kSuportedLanguagesArray;
}

@end
