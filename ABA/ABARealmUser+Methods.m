//
//  ABARealmUser+Methods.m
//  ABA
//
//  Created by Jesus Espejo on 25/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "ABARealmUser+Methods.h"

@implementation ABARealmUser(Methods)

 // 10 days in seconds
#define kRecentLoginThreshold 60 * 60 * 24 * 10

- (BOOL)isRecentLogin
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval elapsedTime = [currentDate timeIntervalSinceDate:self.lastLoginDate];
    
    // Calculating time
    if (fabs(elapsedTime) < kRecentLoginThreshold)
    {
        return YES;
    }
    
    return NO;
}

@end
