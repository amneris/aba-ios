//
//  SolutionTextController.m
//  ABA
//
//  Created by Marc Güell Segarra on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SolutionTextController.h"
#import "MDTUtils.h"
#import "ABAPhrase.h"
#import "Resources.h"
#import <UIKit/UIKit.h>

#define dayNamesArray @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday"]
#define monthNamesArray @[@"January", @"February", @"March", @"April", @"June", @"July", @"August", @"September", @"October", @"November", @"December"]
#define nationsAndLanguagesNamesArray @[@"Italian"]
#define properNamesArray @[@"Tom", @"London"]

#define contractedForms @[@"don't", @"doesn't", @"didn't", @"'m not", @"'m", @"isn't", @"'s", @"aren't", @"'re", @"wasn't", @"weren't", @"haven't", @"hasn't", @"hadn't", @"'ll", @"won't", @"'d", @"wouldn't", @"can't", @"couldn't", @"mustn't", @"shouldn't", @"oughtn't to", @"'ve got", @"'s got", @"'d got"]
#define uncontractedForms @[@"do not", @"does not", @"did not", @"am not", @"am", @"is not", @"is", @"are not", @"are", @"was not", @"were not", @"have not", @"has not", @"had not", @"will", @"will not", @"would", @"would not", @"cannot", @"could not", @"must not", @"should not", @"ought not to", @"has got", @"have got", @"had got"]

#define specialWords @[@"Côte", @"café", @"cafés", @"façades"]
#define specialWordsMatch @[@"Cote", @"cafe", @"cafes", @"facades"]


@implementation SolutionTextController

-(NSString *) formatSolutionText: (NSString *)text
{
    NSString *textFormated = text;

    // Override , ? ! .
    NSCharacterSet *notAllowedChars = [NSCharacterSet characterSetWithCharactersInString:@".?!,:\""];
    textFormated = [[textFormated componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@" "];

    // Remove trailing and leading spaces if needed
    textFormated = [textFormated stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    //  Avoid two or more spaces
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];

    textFormated = [regex stringByReplacingMatchesInString:textFormated options:0 range:NSMakeRange(0, [textFormated length]) withTemplate:@" "];

    // Check for first letter of the first word always lowercase (if it's possible due to exceptions)
    /*if(![textFormated isEqualToString:@""])
    {
        NSArray *wordsOfText = [textFormated componentsSeparatedByString:@" "];
        
        if([wordsOfText count] > 0)
        {
            if([self canLowercase:[wordsOfText objectAtIndex:0]])
            {
                NSString *firstLetter = [textFormated substringWithRange:NSMakeRange(0, 1)];
                NSString *restOfLetters = [textFormated substringWithRange:NSMakeRange(1, [textFormated length]-1)];
                
                textFormated = [NSString stringWithFormat:@"%@%@", [firstLetter lowercaseString], restOfLetters];
            }
        }
    }*/
    
    return textFormated;
}

-(BOOL)canLowercase: (NSString *)word
{
    if(word.length > 1)
    {
        NSString *firstTwoLetters = [word substringWithRange:NSMakeRange(0, 2)];
        
        if([firstTwoLetters isEqualToString:@"I'"])
        {
            return NO;
        }
    }

    // Check for I / I'
    if([word isEqualToString:@"I"])
    {
        return NO;
    }
    
    // check for dayNamesArray
    if([self checkIfWordExists:word inArray:dayNamesArray])
    {
        return NO;
    }
    
    // check for monthNamesArray
    if([self checkIfWordExists:word inArray:monthNamesArray])
    {
        return NO;
    }
    
    // check for nationsAndLanguagesNamesArray
    if([self checkIfWordExists:word inArray:nationsAndLanguagesNamesArray])
    {
        return NO;
    }
    
    // check for properNamesArray
    if([self checkIfWordExists:word inArray:properNamesArray])
    {
        return NO;
    }

    return YES;
}

-(BOOL)compareWord: (NSString*)word withCorrectWord: (NSString*)correctWord
{
    if([word isEqualToString:correctWord])
    {
        return YES;
    }
    else if([self canLowercase:correctWord] && [[correctWord lowercaseString] isEqualToString:[word lowercaseString]])
    {
        _mustChangeOriginalText = YES;
        return YES;
    }
    
    return NO;
}

-(BOOL)checkIfWordExists: (NSString *)word inArray:(NSArray *)array
{
    for (NSString *wordInArray in array)
    {
        NSString *lowerCaseWord = [wordInArray lowercaseString];
        
        if ([word isEqualToString:lowerCaseWord] ||
            [word isEqualToString:wordInArray])
        {
            return YES;
        }
    }
    
    return NO;
}

-(NSDictionary *) checkSolutionText:(NSString *)userText withCorrectSolutionText:(NSString *)correctText
{
    _mustChangeOriginalText = NO;
    
    NSMutableDictionary *errorDict = [[NSMutableDictionary alloc] init];
 
    // punctuation and whitespaces
    NSString *userTextFormatted = [self formatSolutionText:userText];
    NSString *correctTextFormatted = [self formatSolutionText:correctText];
    
    // special words with special chars
    userTextFormatted = [self convertSpecialWords:userTextFormatted];
    correctTextFormatted = [self convertSpecialWords:correctTextFormatted];

    NSString *userTextUncontracted = [self convertToUncontractedForm:userTextFormatted];
    NSString *correctTextUncontracted = [self convertToUncontractedForm:correctTextFormatted];

    if((_strictContractedForms && ![userTextFormatted isEqualToString:correctTextFormatted]) ||
	   (!_strictContractedForms && ![userTextUncontracted isEqualToString:correctTextUncontracted]))
    {
        NSArray *userTextWordArray = [userTextFormatted componentsSeparatedByString:@" "];
        NSArray *correctTextWordArray = [correctTextFormatted componentsSeparatedByString:@" "];
        
        if([correctTextWordArray count] != [userTextWordArray count])
        {
            if([correctTextWordArray count] > [userTextWordArray count]) // User has left words
            {
                for (int i=0; i<correctTextWordArray.count; i++)
                {
                    if(i <= [userTextWordArray count]-1)
                    {
                        if(![self compareWord:userTextWordArray[i] withCorrectWord:correctTextWordArray[i]])
                        {
                            [errorDict setObject:userTextWordArray[i] forKey:[NSString stringWithFormat:@"%d", i]];
                        }
                        else
                        {
                            [errorDict setObject:@"" forKey:[NSString stringWithFormat:@"%d", i]];
                        }
                    }
                    else
                    {
                        [errorDict setObject:correctTextWordArray[i] forKey:[NSString stringWithFormat:@"%d", i]];
                    }
                }
            }
            else // User has written more words than necessary
            {
                for (int i=0; i<userTextWordArray.count; i++)
                {
                    if(i > [correctTextWordArray count]-1)
                    {
                        [errorDict setObject:userTextWordArray[i] forKey:[NSString stringWithFormat:@"%d", i]];
                    }
                    else
                    {
                        if(![self compareWord:userTextWordArray[i] withCorrectWord:correctTextWordArray[i]])
                        {
                            [errorDict setObject:userTextWordArray[i] forKey:[NSString stringWithFormat:@"%d", i]];
                        }
                        else
                        {
                            [errorDict setObject:@"" forKey:[NSString stringWithFormat:@"%d", i]];
                        }
                    }
                }
            }
        }
        else
        {
            for (int i=0; i<userTextWordArray.count; i++)
            {
                if(![self compareWord:userTextWordArray[i] withCorrectWord:correctTextWordArray[i]])
                {
                    [errorDict setObject:userTextWordArray[i] forKey:[NSString stringWithFormat:@"%d", i]];
                }
                else
                {
                    [errorDict setObject:@"" forKey:[NSString stringWithFormat:@"%d", i]];
                }
            }
        }
    }
	else // All words are correct
    {
        NSArray *userTextWordArray = [userTextFormatted componentsSeparatedByString:@" "];

        for (int i=0; i<userTextWordArray.count; i++)
        {
            [errorDict setObject:@"" forKey:[NSString stringWithFormat:@"%d", i]];
        }
    }
    
    return errorDict;
}

-(NSString *)convertToContractedForm:(NSString *)textToTransform
{
    NSArray *arrayUncontractedForms = uncontractedForms;
    NSArray *arrayContractedForms = contractedForms;
    NSUInteger index;
    
    for(NSString *uncontractedItem in arrayUncontractedForms)
    {
        index = [arrayUncontractedForms indexOfObject:uncontractedItem];
        
        textToTransform = [textToTransform stringByReplacingOccurrencesOfString:uncontractedItem
                                             withString:arrayContractedForms[index]];
    }
    
    return textToTransform;
    
}

-(NSString *)convertToUncontractedForm:(NSString *)textToTransform
{
    NSArray *arrayUncontractedForms = uncontractedForms;
    NSArray *arrayContractedForms = contractedForms;
    NSUInteger index;
    
    // Some users will type ´ instead of ' for contracted forms.
    textToTransform = [textToTransform stringByReplacingOccurrencesOfString:@"´"
                                                                 withString:@"'"];

    for(NSString *contractedItem in arrayContractedForms)
    {
        index = [arrayContractedForms indexOfObject:contractedItem];
        
        textToTransform = [textToTransform stringByReplacingOccurrencesOfString:contractedItem
                                                                     withString:[NSString stringWithFormat:@" %@",arrayUncontractedForms[index]]];
    }
    
    // Remove trailing and leading spaces if needed
    textToTransform = [textToTransform stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    
    textToTransform = [regex stringByReplacingMatchesInString:textToTransform options:0 range:NSMakeRange(0, [textToTransform length]) withTemplate:@" "];

    return textToTransform;
    
}

-(NSString *)convertSpecialWords:(NSString *)textToTransform
{
    NSArray *arraySpecialWords = specialWords;
    NSArray *arraySpecialWordsMatch = specialWordsMatch;
    NSUInteger index = 0;
    
    for(NSString *wordItem in arraySpecialWords)
    {
        textToTransform = [textToTransform stringByReplacingOccurrencesOfString:wordItem
                                                                     withString:arraySpecialWordsMatch[index]];
        index ++;
    }
    
    return textToTransform;
    
}

-(BOOL)checkPhrase: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict
{
    BOOL almostOneWordIsIncorrect = NO;
    
    NSArray *textArray = [answerFormatted componentsSeparatedByString:@" "];
    
    // Detect if we missed some word
    if([[errorDict allKeys] count] != [textArray count] &&
       [textArray count] > 0)
    {
        NSString *text = [textArray objectAtIndex:0];
        
        if(![text isEqualToString:@""] && [text length] > 0)
        {
            almostOneWordIsIncorrect = YES;
        }
    }
    
    // Detect if we entered blanks
    if([answerFormatted isEqualToString:@""] ||
       [answerFormatted isEqualToString:@" "])
    {
        almostOneWordIsIncorrect = YES;
    }
    
    // We check every word
    for (int i=0; i<textArray.count; i++)
    {
        NSString *incorrectWord = [errorDict objectForKey:[NSString stringWithFormat:@"%d", i]];
        
        if(incorrectWord != nil && [incorrectWord length] > 0)
        {
            almostOneWordIsIncorrect = YES;
        }
    }

    return almostOneWordIsIncorrect;
}

-(BOOL)areThereMissingWords: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict
{
    BOOL missingWords = NO;
    
    NSArray *textArray = [answerFormatted componentsSeparatedByString:@" "];
    
    // Detect if we missed some word
    if([[errorDict allKeys] count] != [textArray count] &&
       [textArray count] > 0)
    {
        NSString *text = [textArray objectAtIndex:0];
        
        if(![text isEqualToString:@""] && [text length] > 0)
        {
            missingWords = YES;
        }
    }
    
    return missingWords;
}
    
-(NSRange)getFirstIncorrectWordPosition: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict
{
    BOOL firstWordIncorrect = NO;
    NSRange firstWordPosition = NSMakeRange(0, 0);
    
    int position  = 0;
    
    NSArray *textArray = [answerFormatted componentsSeparatedByString:@" "];
    
    // We check every word
    for (int i=0; i<textArray.count; i++)
    {
        NSString *incorrectWord = [errorDict objectForKey:[NSString stringWithFormat:@"%d", i]];
        
        if(i != 0)
        {
            position++;
        }
        
        if(incorrectWord != nil && [incorrectWord length] > 0)
        {
            if(!firstWordIncorrect)
            {
                firstWordPosition = NSMakeRange(position+[[textArray objectAtIndex:i] length], 0);
                firstWordIncorrect = YES;
            }
        }
        
        position += [[textArray objectAtIndex:i] length];
    }
    
    return firstWordPosition;
}

-(NSMutableAttributedString *)getAttributedString: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict
{
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:answerFormatted];

    NSArray *textArray = [answerFormatted componentsSeparatedByString:@" "];
    int position  = 0;
    
    // We check every word
    for (int i=0; i<textArray.count; i++)
    {
        NSString *incorrectWord = [errorDict objectForKey:[NSString stringWithFormat:@"%d", i]];
        
        UIColor *color;
        
        if(i != 0)
        {
            position++;
        }
        
        if(incorrectWord != nil && [incorrectWord length] > 0)
        {
            color = ABA_COLOR_RED_ERROR;
        }
        else
        {
            color = ABA_COLOR_REG_MAIL_BUT;
        }
        
        NSUInteger startPos = position;
        NSRange range = NSMakeRange(startPos, [[textArray objectAtIndex:i] length]);
        
        [mutAttStr addAttribute:NSForegroundColorAttributeName
                          value:color
                          range:range];
        
        position += [[textArray objectAtIndex:i] length];
    }

    return mutAttStr;
}

-(BOOL)testCheckPhrase: (NSString *)phrase withCorrectPhrase:(NSString *)correctPhrase
{
    // First we convert the text following some rules and for mantaining the same coherence (contracted forms, lower/uppercase, etc)
    NSMutableString *answerFormatted = [[self formatSolutionText:phrase] mutableCopy];
    
    // We get a dictionary in wich the key is the index of the word in the phrase. The value will be blank if the word is correct.
    NSDictionary *errorDict = [self checkSolutionText:answerFormatted withCorrectSolutionText:correctPhrase];

    return ![self checkPhrase:answerFormatted withErrorDict:errorDict];
}
@end
