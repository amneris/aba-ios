//
//  ABAVideoClassDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAVideoClassDataController.h"
#import <Realm/Realm.h>
#import "ABAVideoClass.h"
#import "ABAUnit.h"

@implementation ABAVideoClassDataController

+ (void)createABAVideoClassWithDictionary:(NSDictionary *)abaVideoClassData onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAVideoClass *videoClass = unit.sectionVideoClass;
    
    [realm transactionWithBlock:^{
        
        if (!videoClass)
        {
            videoClass = [[ABAVideoClass alloc] init];
            unit.sectionVideoClass = videoClass;
        }
        
        videoClass.englishSubtitles = [abaVideoClassData objectForKey:@"English Subtitles"];
        videoClass.hdVideoURL = [abaVideoClassData objectForKey:@"SD Video URL"];
        videoClass.previewImageURL = [abaVideoClassData objectForKey:@"Preview Image URL"];
        videoClass.sdVideoURL = [abaVideoClassData objectForKey:@"Mobile SD Video URL"];
        videoClass.translatedSubtitles = [abaVideoClassData objectForKey:@"Translated Subtitles"];
        
    } error:&error];
    
    if (!error)
    {
        block(nil, videoClass);
    }
    else
    {
        block(error, nil);
    }
}


@end
