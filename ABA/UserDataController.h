//
//  UserDataController.h
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 16/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "Blocks.h"
#import "Definitions.h"

@class ABARealmUser, ABARealmLevel;

@interface UserDataController : NSObject

+ (ABARealmUser *)getCurrentUser;

+ (void)deleteCurrentUser;
+ (void)deleteCurrentUserWithBlock:(ErrorBlock)block;

+ (void)setUserCurrentLevel: (ABARealmLevel *)level;
+ (void)setUserCurrentLevel: (ABARealmLevel *)level
            completionBlock: (CompletionBlock)completionBlock;


+ (void)setUserToken: (NSString *)token
     completionBlock: (ErrorBlock)block;

+ (void)updateUserWithDictionary: (NSDictionary *)dict
                 completionBlock: (ErrorBlock)block;

+ (void)updateUserAfterSubscribeWithDictionary:(NSDictionary *)dict
                               completionBlock:(ErrorBlock)block;
+ (void)updateUserToPremium;

+ (void)setCompletedLevel:(ABARealmLevel *)level
          completionBlock:(CompletionBlock)completionBlock;

+ (void)setCompletedUnit: (ABAUnit *)unit
         completionBlock: (CompletionBlock)completionBlock;

#pragma mark - Registration/Login

+ (void)postRegisterWithFacebookID:(NSString*)facebookID
                             email:(NSString*)email
                              name:(NSString*)name
                          surnames:(NSString*)surnames
                            gender:(NSString*)gender
                            avatar:(NSString*)avatar
                   completionBlock:(CompletionBlockFacebook)completionBlock;

+ (void)postRegisterWithEmail:(NSString*)email
                     password:(NSString*)password
                         name:(NSString*)name
              completionBlock:(CompletionBlock)completionBlock;

+ (void)checkPasswordWithEmail:(NSString*)email
                      password:(NSString*)password
               completionBlock:(BoolCompletionBlock)completionBlock;

+ (void)postLoginWithEmail:(NSString*)email
                  password:(NSString*)password
           completionBlock:(CompletionBlock)completionBlock;

+ (void)updatePasswordUserWithToken:(NSString*)token
                       withPassword:(NSString *)password
                    completionBlock:(CompletionBlock)completionBlock;

+ (void)postRecoverPassword:(NSString*)email
            completionBlock:(CompletionBlock)completionBlock;

+ (void)updateUser:(ABARealmUser *)user;
+ (void)updateUser:(ABARealmUser *)user completionBlock:(CompletionBlock) completionBlock;

#pragma mark - Swift accesible

+ (void)createUserWithUserModel:(NSDictionary *)usermodelDict completionBlock:(ErrorBlock)block;

@end
