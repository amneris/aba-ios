//
//  LevelUnitCell.m
//  ABA
//
//  Created by Marc Güell Segarra on 5/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "LevelUnitCell.h"
#import "Resources.h"
#import "ABARealmLevel.h"
#import "Utils.h"

@implementation LevelUnitCell

- (void)setupWithABARealmLevel:(ABARealmLevel *)ABARealmLevel
{
    _ABARealmLevel = ABARealmLevel;
    
    NSString *levelString = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"levelKey", @"Nivel"), ABARealmLevel.idLevel];

    if([ABARealmLevel.completed integerValue] == 1)
    {
        [_levelUnitTitleLabel setHidden:YES];
        [_levelUnitDescriptionLabel setHidden:YES];
        [_certificateIcon setHidden:NO];
        
        [_levelUnitTitleLabelIndent setHidden:NO];
        [_levelUnitDescriptionLabelIndent setHidden:NO];
        
        [_levelUnitTitleLabelIndent setText:[levelString uppercaseString]];
        [_levelUnitDescriptionLabelIndent setText:ABARealmLevel.name];
    }
    else
    {
        [_levelUnitTitleLabel setHidden:NO];
        [_levelUnitDescriptionLabel setHidden:NO];
        [_certificateIcon setHidden:YES];
        
        [_levelUnitTitleLabelIndent setHidden:YES];
        [_levelUnitDescriptionLabelIndent setHidden:YES];
        
        [_levelUnitTitleLabel setText:[levelString uppercaseString]];
        [_levelUnitDescriptionLabel setText:ABARealmLevel.name];
    }
    
    [_levelUnitCollapseButton setHidden:![self.delegate isLevelExpanded:ABARealmLevel]];
    [_levelUnitExpandButton setHidden:[self.delegate isLevelExpanded:ABARealmLevel]];
    
    if([ABARealmLevel.idLevel isEqualToString:@"1"])
    {
        [_separatorBottomLine setHidden:YES];
    }
    else
    {
        [_separatorBottomLine setHidden:NO];
    }
    
    // Disabling buttons interactions
    _levelUnitExpandButton.userInteractionEnabled = NO;
    _levelUnitCollapseButton.userInteractionEnabled = NO;
}

@end
