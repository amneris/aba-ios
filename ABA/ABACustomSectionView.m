//
//  ABACustomSectionView.m
//  ABA
//
//  Created by Jordi Mele on 19/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABACustomSectionView.h"
#import "Utils.h"
#import "Resources.h"
#import "ABASection+Methods.h"
#import "ABAFilm.h"
#import "ABASpeak.h"
#import "ABAWrite.h"
#import "ABAInterpret.h"
#import "ABAVideoClass.h"
#import "ABAExercises.h"
#import "ABAVocabulary.h"
#import "ABAEvaluation.h"
#import "Blocks.h"
#import "ABAUnit.h"
#import "UserController.h"
#import "LegacyTrackingManager.h"
#import "PureLayout.h"

@interface ABACustomSectionView ()

@property(weak, nonatomic) IBOutlet UIButton *sectionButton;
@property(weak, nonatomic) IBOutlet UILabel *titleSectionLabel;
@property(weak, nonatomic) IBOutlet UILabel *infoSectionLabel;
@property(weak, nonatomic) IBOutlet UIImageView *infoSectionLockedImage;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property(weak, nonatomic) IBOutlet UIView *iconSectionView;
@property(weak, nonatomic) IBOutlet UIView *numberView;

@property(nonatomic, assign) id<ABACustomSectionViewDelegate> delegate;

@property(nonatomic, assign) kABASectionTypes currentType;

- (IBAction)sectionButtonAction:(id)sender;

@end

@implementation ABACustomSectionView

- (id)initWithType:(kABASectionTypes)type delegate:(id<ABACustomSectionViewDelegate>)delegate view:(UIView *)view
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ABACustomSection" owner:self options:nil] objectAtIndex:0];

    if (self)
    {
        _currentType = type;

        [self setDelegate:delegate];

        CGFloat xMultiplierSectionView = 1.0f;
        CGFloat yMultiplierSectionView = 1.0f;

        CGFloat xMultiplierSectionNumber = 1.0f;
        CGFloat yMultiplierSectionNumber = 1.0f;

        switch (type)
        {
        case kABAFilmSectionType:
        {
            xMultiplierSectionView = 1.0f;
            yMultiplierSectionView = 0.3f;
            xMultiplierSectionNumber = 1.0f;
            yMultiplierSectionNumber = 1.7f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_film"] forState:UIControlStateNormal];
            [_sectionButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];

            break;
        }
        case kABASpeakSectionType:
        {
            xMultiplierSectionView = 1.5f;
            yMultiplierSectionView = 0.5f;
            xMultiplierSectionNumber = 0.5f;
            yMultiplierSectionNumber = 1.5f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_speak"] forState:UIControlStateNormal];
            [_sectionButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 0.0)];

            break;
        }
        case kABAWriteSectionType:
        {
            xMultiplierSectionView = 1.7f;
            yMultiplierSectionView = 1.0f;
            xMultiplierSectionNumber = 0.3f;
            yMultiplierSectionNumber = 1.0f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_write"] forState:UIControlStateNormal];

            break;
        }
        case kABAInterpretSectionType:
        {
            xMultiplierSectionView = 1.5f;
            yMultiplierSectionView = 1.5f;
            xMultiplierSectionNumber = 0.5f;
            yMultiplierSectionNumber = 0.5f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_interpret"] forState:UIControlStateNormal];

            break;
        }
        case kABAVideoClassSectionType:
        {
            xMultiplierSectionView = 1.0f;
            yMultiplierSectionView = 1.7f;
            xMultiplierSectionNumber = 1.0f;
            yMultiplierSectionNumber = 0.3f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_video"] forState:UIControlStateNormal];
            [_sectionButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 3.0, 0.0, 0.0)];

            break;
        }
        case kABAExercisesSectionType:
        {
            xMultiplierSectionView = 0.5f;
            yMultiplierSectionView = 1.5f;
            xMultiplierSectionNumber = 1.5f;
            yMultiplierSectionNumber = 0.5f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_exercise"] forState:UIControlStateNormal];
            [_sectionButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 0.0)];

            break;
        }
        case kABAVocabularySectionType:
        {
            xMultiplierSectionView = 0.3f;
            yMultiplierSectionView = 1.0f;
            xMultiplierSectionNumber = 1.7f;
            yMultiplierSectionNumber = 1.0f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_vocabulary"] forState:UIControlStateNormal];
            [_sectionButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 2.0)];

            break;
        }
        case kABAAssessmentSectionType:
        {
            xMultiplierSectionView = 0.5f;
            yMultiplierSectionView = 0.5f;
            xMultiplierSectionNumber = 1.5f;
            yMultiplierSectionNumber = 1.5f;
            [_sectionButton setImage:[UIImage imageNamed:@"icon_unit_evaluation"] forState:UIControlStateNormal];

            break;
        }
        default:
        {
            break;
        }
        }

        [view addSubview:self];

        [self setTranslatesAutoresizingMaskIntoConstraints:NO];

        // section views constraints
        [self autoAlignAxis:ALAxisVertical toSameAxisOfView:view withMultiplier:xMultiplierSectionView];
        [self autoAlignAxis:ALAxisHorizontal toSameAxisOfView:view withMultiplier:yMultiplierSectionView];

        [self autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:view withMultiplier:0.25];
        [self autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:view withMultiplier:0.25];

        // section number constraints
        [_numberView autoAlignAxis:ALAxisVertical toSameAxisOfView:_iconSectionView withMultiplier:xMultiplierSectionNumber];
        [_numberView autoAlignAxis:ALAxisHorizontal toSameAxisOfView:_iconSectionView withMultiplier:yMultiplierSectionNumber];

        [_numberView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:_iconSectionView withMultiplier:0.23];
        [_numberView autoMatchDimension:ALDimensionHeight toDimension:ALDimensionHeight ofView:_iconSectionView withMultiplier:0.23];

        self.alpha = 0.0;
        self.iconSectionView.alpha = 0.0;
        self.titleSectionLabel.alpha = 0.0;
        [self layoutIfNeeded];

        _titleSectionLabel.textColor = [UIColor whiteColor];
        [_infoSectionLabel setTextColor:ABA_COLOR_DARKGREY];
        _infoSectionLabel.textAlignment = NSTextAlignmentCenter;
        _numberView.backgroundColor = [UIColor whiteColor];

        [self updateViews];
    }

    return self;
}

#pragma mark - Lifecycle

- (void)drawRect:(CGRect)rect
{
    switch (_currentType)
    {
    case kABAFilmSectionType:
    {
        _infoSectionLabel.text = @"1";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle1Key", @"film") uppercaseString];

        break;
    }
    case kABASpeakSectionType:
    {
        _infoSectionLabel.text = @"2";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle2Key", @"habla") uppercaseString];

        break;
    }
    case kABAWriteSectionType:
    {
        _infoSectionLabel.text = @"3";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle3Key", @"escribe") uppercaseString];
        [_sectionButton setAccessibilityLabel:@"writeButton"];

        break;
    }
    case kABAInterpretSectionType:
    {
        _infoSectionLabel.text = @"4";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle4Key", @"interpreta") uppercaseString];

        break;
    }
    case kABAVideoClassSectionType:
    {
        _infoSectionLabel.text = @"5";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle5Key", @"videoclase") uppercaseString];

        break;
    }
    case kABAExercisesSectionType:
    {
        _infoSectionLabel.text = @"6";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle6Key", @"ejercicios") uppercaseString];

        break;
    }
    case kABAVocabularySectionType:
    {
        _infoSectionLabel.text = @"7";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle7Key", @"vocabulario") uppercaseString];

        break;
    }
    case kABAAssessmentSectionType:
    {
        _infoSectionLabel.text = @"8";
        _titleSectionLabel.text = [STRTRAD(@"unitMenuTitle8Key", @"evaluación") uppercaseString];

        break;
    }
    default:
        break;
    }
}

#pragma mark - Public methods

- (void)setStyleOptions:(kABASectionStyleOptions)styleOptions
{
    CGFloat finalAlpha = 1.0f;
    UIColor *backgroundColor = [UIColor whiteColor];
    _infoSectionLabel.hidden = NO;
    _infoSectionLockedImage.hidden = YES;
    _sectionButton.backgroundColor = ABA_COLOR_DARKGREY;

    if (OptionsHasValue(styleOptions, kABASectionStyleBgColorDone))
    {
        _sectionButton.backgroundColor = ABA_COLOR_GREEN_MENU_UNIT;
    }

    if (OptionsHasValue(styleOptions, kABASectionStyleBgNumberColorCurrent))
    {
        backgroundColor = ABA_COLOR_BLUE_MENU_UNIT;
    }

    if (OptionsHasValue(styleOptions, kABASectionStyleLockEnabled))
    {
        _infoSectionLabel.hidden = YES;
        _infoSectionLockedImage.hidden = NO;
    }
    
    if (OptionsHasValue(styleOptions, kABASectionStyleAlphaEnabled)) {
        finalAlpha = 0.6f;
    }

    if (OptionsHasValue(styleOptions, kABASectionStyleAnimatedYes))
    {
        [UIView animateWithDuration:0.1
            delay:_currentType * 0.05f
            options:UIViewAnimationOptionCurveEaseIn
            animations:^{
                self.alpha = 1.0f;
                self.iconSectionView.alpha = finalAlpha;
                self.titleSectionLabel.alpha = finalAlpha;

            }
            completion:^(BOOL finished) {
                self.numberView.backgroundColor = backgroundColor;
            }];
    }
    else
    {
        self.alpha = 1.0f;
        self.iconSectionView.alpha = finalAlpha;
        self.titleSectionLabel.alpha = finalAlpha;
        self.numberView.backgroundColor = backgroundColor;
    }
}

- (void)updateViews
{
    _sectionButton.layer.cornerRadius = _sectionButton.frame.size.width / 2;

    [_titleSectionLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:_titleSectionLabel.frame.size.height / 1.5]];

    [_infoSectionLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:_infoSectionLabel.frame.size.height / 1.5]];

    _numberView.layer.cornerRadius = _numberView.frame.size.width / 2;
}

- (IBAction)sectionButtonAction:(id)sender
{
    [_delegate loadSection:_currentType];
}

@end
