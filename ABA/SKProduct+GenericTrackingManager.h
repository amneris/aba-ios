//
//  SKProduct+GenericTrackingManager.h
//  ABA
//
//  Created by MBP13 Jesus on 07/01/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "TrackingManagerProtocol.h"

@interface SKProduct(GenericTrackingManager) < ABATrackingManagerProtocol >


@end
