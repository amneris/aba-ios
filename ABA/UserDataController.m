//
//  UserDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 16/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "UserDataController.h"
#import "DataController.h"
#import "ABARealmUser.h"
#import "ABAUnit.h"
#import "ABARealmLevel.h"
#import "APIManager.h"
#import "LevelUnitController.h"
#import "ABAExperiment.h"
#import "ExperimentRunner.h"

// Traking
#import "LegacyTrackingManager.h"
#import <Crashlytics/Crashlytics.h>

// Swift imports
#import "ABA-Swift.h"

@implementation UserDataController

+ (ABARealmUser *)getCurrentUser
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    return [[ABARealmUser allObjectsInRealm:realm] firstObject];
}

+ (void)deleteCurrentUserWithBlock:(ErrorBlock)block
{
    [DataController cleanEntitiesWithCompletionBlock:block];
}

+ (void)deleteCurrentUser
{
    [self deleteCurrentUserWithBlock:^(NSError *error) {}];
}

+ (void)setUserCurrentLevel: (ABARealmLevel *)level
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    ABARealmUser *user = [self getCurrentUser];
    
    [realm beginWriteTransaction];
    user.currentLevel = level;
    [realm commitWriteTransaction];
}

+ (void)setCompletedUnit: (ABAUnit *)unit completionBlock: (CompletionBlock)completionBlock
{
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    unit.progress = [NSNumber numberWithInt:100];
    unit.completed = [NSNumber numberWithInt:1];
    [realm commitWriteTransaction:&error];
    
    if (!error)
    {
        completionBlock(nil, [self getCurrentUser]);
    }
    else
    {
        completionBlock(error, nil);
    }
}

+ (void)setCompletedLevel: (ABARealmLevel *)level completionBlock: (CompletionBlock)completionBlock
{
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];

    ABARealmUser *user = [UserDataController getCurrentUser];
    
    level.progress = [NSNumber numberWithInt:100];
    level.completed = [NSNumber numberWithInt:1];
    
    [user.certs addObject:level.cert];

    [realm commitWriteTransaction:&error];
    
    if (!error)
    {
        completionBlock(nil, [self getCurrentUser]);
    }
    else
    {
        completionBlock(error, nil);
    }
}

+ (void)postRegisterWithFacebookID:(NSString *)facebookID email:(NSString *)email name:(NSString *)name surnames:(NSString *)surnames gender:(NSString *)gender avatar:(NSString *)avatar completionBlock:(CompletionBlockFacebook)completionBlockFacebook {
    
    [[APIManager sharedManager] postRegisterWithFacebookID:facebookID email:email name:name surnames:surnames gender:gender avatar:(NSString *)avatar completionBlock:^(NSError *error, id object)
     {
         if (object)
         {
             [self createUserWithDictionary:object andEmail:email completionBlock:^(NSError *error)
              {
                  // "isnewuser" key contains a value with "0" or "1". "0" means 'NO'.
                  BOOL isNewUser = [[object objectForKey:@"isnewuser"] boolValue];
                  completionBlockFacebook(error, nil, isNewUser);
              }];
         }
         else
         {
             completionBlockFacebook(error, nil, NO);
         }
     }];
}

+ (void)postRegisterWithEmail:(NSString *)email
                     password:(NSString *)password
                         name:(NSString *)name
              completionBlock:(CompletionBlock)completionBlock
{
    [[APIManager sharedManager] postRegisterWithEmail:email
                                             password:password
                                                 name:name
                                      completionBlock:^(NSError *error, id object) {
                                          if (object)
                                          {
                                              [self createUserWithDictionary:object
                                                                              andEmail:email
                                                                       completionBlock:^(NSError *error) {
                                                                           completionBlock(error, nil);
                                                                       }];
                                          }
                                          else
                                          {
                                              completionBlock(error, nil);
                                          }
                                      }];
}

+ (void)checkPasswordWithEmail:(NSString *)email password:(NSString *)password completionBlock:(BoolCompletionBlock)completionBlock
{
    
    [[APIManager sharedManager] postLoginWithEmail:email
                                          password:password
                                   completionBlock:^(NSError *error, id object)
     {
         if (object && !error)
         {
             completionBlock(error,YES);
         }
         else
         {
             completionBlock(error, NO);
         }
         
     }];
}

+ (void)postLoginWithEmail:(NSString *)email password:(NSString *)password completionBlock:(CompletionBlock)completionBlock
{
    [[APIManager sharedManager] postLoginWithEmail:email
                                          password:password
                                   completionBlock:^(NSError *error, id object)
     {
         if (object && !error)
         {
             [self createUserWithDictionary:object andEmail:email completionBlock:^(NSError *error)
              {
                  completionBlock(error,nil);
              }];
         }
         else
         {
             completionBlock(error, nil);
         }
         
     }];
}

+ (void)updatePasswordUserWithToken:(NSString *)token withPassword:(NSString *)password completionBlock:(CompletionBlock)completionBlock {
    
    [[APIManager sharedManager] updatePasswordUserWithToken:token password:password completionBlock:^(NSError *error, NSString *newToken)
     {
         if (!error)
         {
             [self setUserToken:newToken completionBlock:^(NSError *error)
              {
                  [[APIManager sharedManager] setToken:newToken];
                  completionBlock(error,nil);
              }];
         }
         else
         {
             completionBlock(error, nil);
         }
     }];
}

+ (void)updateUserLevel: (NSString *)idLevel withToken:(NSString *)token completionBlock:(CompletionBlock)completionBlock
{
    [[APIManager sharedManager] updateUserLevel:idLevel idUser:token completionBlock:^(NSError *error, id object) {
        if (!error)
        {
            completionBlock(nil,nil);
        }
        else
        {
            completionBlock(error, nil);
        }
    }];
}

+ (void)postRecoverPassword:(NSString *)email completionBlock:(CompletionBlock)completionBlock
{
    [[APIManager sharedManager] postRecoverPassword:email completionBlock:^(NSError *error, id object)
     {
         if (object)
             completionBlock(nil,nil);
         else
             completionBlock(error, nil);
     }];
}

+ (NSString *)randomStringIdSession {
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:32];
    for (NSUInteger i = 0U; i < 32; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    
    return s.mutableCopy;
}

+ (void)createUserWithDictionary:(NSDictionary *)dict andEmail:(NSString *)email completionBlock:(ErrorBlock)block
{
    NSString * token = [dict objectForKey:@"token"];
    NSString * language = [dict objectForKey:@"userLang"];
    
    [LevelUnitController checkForUnitsWithToken:token withLanguage:language andBlock:^(NSError *error) {
        
        if (!error)
        {
            NSError *error;
            RLMRealm *realm = [RLMRealm defaultRealm];
            ABARealmUser *user = [[ABARealmUser alloc] init];
            [self hydrateUser:user withDictionary:dict andEmail:email];
            
            [realm beginWriteTransaction];
            [realm addObject:user];
            [realm commitWriteTransaction:&error];
            
            block(error);
        }
        else
        {
            block(error);
        }
    }];
}

+ (void)createUserWithUserModel:(NSDictionary *)usermodelDict completionBlock:(ErrorBlock)block
{
    ABARealmUser *currentUser = [self getCurrentUser];
    if (!currentUser)
    {
        NSError *error;
        RLMRealm *realm = [RLMRealm defaultRealm];
        ABARealmUser *user = [[ABARealmUser alloc] init];
        [self hydrateUser:user withDictionary:usermodelDict andEmail:[usermodelDict objectForKey:@"email"]];
        
        [realm beginWriteTransaction];
        [realm addObject:user];
        [realm commitWriteTransaction:&error];
        
        block(error);
    }
    else
    {
        NSError *error;
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        [realm beginWriteTransaction];
        [self hydrateUser:currentUser withDictionary:usermodelDict andEmail:nil];
        [realm commitWriteTransaction:&error];
        
        block(error);
    }
}

+ (void)updateUserWithDictionary:(NSDictionary *)dict completionBlock:(ErrorBlock)block
{
    NSString *email = [dict objectForKey:@"email"];
    ABARealmUser *currentUser = [self getCurrentUser];
    
    if (!currentUser)
    {
        [self createUserWithDictionary:dict andEmail:email completionBlock:block];
    }
    else
    {
        NSError *error;
        RLMRealm *realm = [RLMRealm defaultRealm];

        [realm beginWriteTransaction];
        [self hydrateUser:currentUser withDictionary:dict andEmail:nil];
        [realm commitWriteTransaction:&error];
        
        block(error);
    }
}

+ (void)hydrateUser:(ABARealmUser *)user withDictionary:(NSDictionary *)dict andEmail:(NSString *)email
{
    // If Cooladata info exists
    if ([dict objectForKey:@"cooladata"]) {
        [CoolaDataTrackingManager recordCooladataProperties:[dict objectForKey:@"cooladata"]];
    }

    // Reading email if available
    if ([dict objectForKey:@"email"]) {
        user.email = [dict objectForKey:@"email"];
    }
    
    // Email & tracking info
    if (email && email.length > 0)
    {
        user.email = email;
    }
    
    [[Crashlytics sharedInstance] setObjectValue:user.email forKey:@"Email"];
    
    // Safely setting country
    NSString * country = [dict objectForKey:@"countryNameIso"];
    if (country && ![country isKindOfClass:[NSNull class]]) {
        user.country  = country;
    }
    
    user.name       = [dict objectForKey:@"name"];
    user.token      = [dict objectForKey:@"token"];
    user.userLang   = [dict objectForKey:@"userLang"];
    user.idSession  = [self randomStringIdSession];
    
    if ([dict objectForKey:@"userId"]) {
        user.idUser     = [dict objectForKey:@"userId"];
    } else {
        user.idUser     = [dict objectForKey:@"idUser"];
    }
    
    if ([dict objectForKey:@"idPartner"]) {
        user.partnerID = [dict objectForKey:@"idPartner"];
    }
    else {
        user.partnerID = [dict objectForKey:@"partnerID"];
    }
    
    if ([dict objectForKey:@"idSource"]) {
        user.sourceID   = [dict objectForKey:@"idSource"];
    }else{
        user.sourceID   = [dict objectForKey:@"sourceID"];
    }
    
    user.gender = [dict objectForKey:@"gender"];
    user.birthdate = [dict objectForKey:@"birthDate"];
    user.phone = [dict objectForKey:@"phone"];
    user.externalKey = [dict objectForKey:@"externalkey"];
    
    // Surnames if they exist in the dictionary and it's not a null class
    if ([dict objectForKey:@"surnames"] && ![[dict objectForKey:@"surnames"] isKindOfClass:[NSNull class]])
    {
        user.surnames   = [dict objectForKey:@"surnames"];
    }
    
    NSString * userType =[dict objectForKey:@"userType"];
    if (!userType) {
        userType =[dict objectForKey:@"type"];
    }
    
    if (userType == nil || !userType)
    {
        user.type = @"1";
    }
    else
    {
        if ([userType isEqualToString:@"1"] ||
            [userType isEqualToString:@"2"] ||
            [userType isEqualToString:@"3"] ||
            [userType isEqualToString:@"4"] ||
            [userType isEqualToString:@"5"] ||
            [userType isEqualToString:@"6"])
        {
            user.type = userType;
        }
        else
        {
            user.type = @"1";
        }
    }
    
    // Avatar
    if ([dict objectForKey:@"avatar"])
    {
        user.urlImage = [dict objectForKey:@"avatar"];
    }
    else {
        user.urlImage = [dict objectForKey:@"urlImage"];
    }
    
    // Two ways of parsing expiration date.
    // Format 'dd-MM-yyyy' used during registration
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    user.expirationDate = [dateFormatter dateFromString:[dict objectForKey:@"expirationDate"]];
    
    // Format 'yyyy-MM-dd HH:mm:ss' used during login with user/pass and token
    if (!user.expirationDate) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        user.expirationDate = [dateFormatter dateFromString:[dict objectForKey:@"expirationDate"]];
    }
    
    user.lastLoginDate = [NSDate date];
    user.teacherId = [dict objectForKey:@"teacherId"];
    user.teacherName = [dict objectForKey:@"teacherName"];
			 
    if (![[dict objectForKey:@"teacherImage"] isKindOfClass:[NSNull class]])
    {
        user.teacherImage = [dict objectForKey:@"teacherImage"];
    }
    
    // Updating API token
    [[APIManager sharedManager] setToken:user.token];
    
    if ([dict objectForKey:@"userLevel"] != nil)
    {
        ABARealmLevel *currentLevel = [LevelUnitController getLevelWithID:[dict objectForKey:@"userLevel"]];
        if (currentLevel == nil) {
            currentLevel = [LevelUnitController getLevelWithID:@"1"];
        }
        
        user.currentLevel = currentLevel;
    }
    
    // A/B testing experiments
    NSArray *profilesArray = [dict objectForKey:@"profiles"];
    
    if ([dict objectForKey:@"profiles"])
    {
        [ExperimentRunner saveExperimentWithExperimentProfile:profilesArray];
    }
}

+ (void)updateUserAfterSubscribeWithDictionary:(NSDictionary *)dict completionBlock:(ErrorBlock)block
{
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    ABARealmUser *user = [UserDataController getCurrentUser];
    
    [realm transactionWithBlock:^{
        
        user.token = [dict objectForKey:@"token"];
        if ([dict objectForKey:@"userType"] == nil || ![dict objectForKey:@"userType"])
        {
            user.type = @"1";
        }
        else
        {
            if ([[dict objectForKey:@"userType"] isEqualToString:@"1"] ||
                [[dict objectForKey:@"userType"] isEqualToString:@"2"] ||
                [[dict objectForKey:@"userType"] isEqualToString:@"3"] ||
                [[dict objectForKey:@"userType"] isEqualToString:@"4"] ||
                [[dict objectForKey:@"userType"] isEqualToString:@"5"] ||
                [[dict objectForKey:@"userType"] isEqualToString:@"6"])
            {
                user.type = [dict objectForKey:@"userType"];
            }
            else
            {
                user.type = @"1";
            }
        }
        
        [[APIManager sharedManager] setToken:user.token];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        user.expirationDate = [dateFormatter dateFromString:[dict objectForKey:@"expirationDate"]];
        
        if ([dict objectForKey:@"userLevel"] != nil)
        {
            ABARealmLevel *currentLevel = [LevelUnitController getLevelWithID:[dict objectForKey:@"userLevel"]];
            
            if (currentLevel == nil)
            {
                currentLevel = [LevelUnitController getLevelWithID:@"1"];
            }
            
            user.currentLevel = currentLevel;
        }
        
    } error:&error];
    
    block(error);
}

+ (void)updateUserToPremium
{
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    ABARealmUser *user = [UserDataController getCurrentUser];
    
    [realm beginWriteTransaction];
    user.type = @"2";
    [realm commitWriteTransaction:&error];
    
    if (error) {
        NSLog(@"%@", error);
    }
}

+ (void)setUserToken: (NSString *)token
     completionBlock: (ErrorBlock)block
{
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    ABARealmUser *user = [UserDataController getCurrentUser];
    
    [realm beginWriteTransaction];
    user.token = token;
    [realm commitWriteTransaction:&error];
    
    block(error);
}

+ (void)setUserCurrentLevel: (ABARealmLevel *)level
            completionBlock: (CompletionBlock)completionBlock
{
    ABARealmUser *currentUser = [self getCurrentUser];
    
    if (![currentUser.currentLevel.idLevel isEqualToString:level.idLevel])
    {
        NSString *idUser = currentUser.idUser;
        __block NSString *idLevel = level.idLevel;

        [[APIManager sharedManager] updateUserLevel:idLevel idUser:idUser completionBlock:^(NSError *error, id object) {
            if (!error) {
                
                RLMRealm *realm = [RLMRealm defaultRealm];
                NSError *error;
                
                ABARealmUser *blockUser = [self getCurrentUser];
                ABARealmLevel *blockLevel = [[ABARealmLevel objectsInRealm:realm withPredicate:[NSPredicate predicateWithFormat:@"idLevel == %@", idLevel]] firstObject];
                
                [realm beginWriteTransaction];
                blockUser.currentLevel = blockLevel;
                [realm commitWriteTransaction:&error];
                
                if (error)
                {
                    completionBlock(error, nil);
                }
                else
                {
                    completionBlock(nil, [self getCurrentUser]);
                }
            }
            else
            {
                completionBlock(error, nil);
            }
        }];
    }
    else
    {
        completionBlock(nil, currentUser);
    }
}

+ (void)updateUser:(ABARealmUser *)user
{
    [self updateUser:user completionBlock:nil];
}

+ (void)updateUser:(ABARealmUser *)user completionBlock:(CompletionBlock)completionBlock
{    
    NSError *error;
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    ABARealmUser *currentUser = [UserDataController getCurrentUser];
    
    [realm beginWriteTransaction];
    currentUser = user;
    [realm commitWriteTransaction:&error];
    
    if (error)
    {
        completionBlock(error, nil);
    }
    else
    {
        completionBlock(nil, [self getCurrentUser]);
    }
}

@end
