//
//  ABAEvaluationOption.h
//  ABA
//
//  Created by Ivan Ferrera on 22/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@class ABAEvaluationQuestion;

@interface ABAEvaluationOption : RLMObject

@property NSNumber<RLMBool>* isGood;
@property NSNumber<RLMBool>* selected;
@property NSString* text;
@property NSString* optionLetter;

- (ABAEvaluationQuestion *)question;

@end

RLM_ARRAY_TYPE(ABAEvaluationOption)
