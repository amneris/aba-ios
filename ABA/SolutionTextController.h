//
//  SolutionTextController.h
//  ABA
//
//  Created by Marc Güell Segarra on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SolutionTextController : NSObject

@property BOOL mustChangeOriginalText;
@property BOOL strictContractedForms;

-(NSString *) formatSolutionText: (NSString *)text;

-(NSDictionary *) checkSolutionText:(NSString *)userText withCorrectSolutionText:(NSString *)text;

-(BOOL)checkPhrase: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict;

-(BOOL)areThereMissingWords: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict;

-(NSRange)getFirstIncorrectWordPosition: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict;

-(NSMutableAttributedString *)getAttributedString: (NSMutableString *)answerFormatted withErrorDict:(NSDictionary *)errorDict;

-(BOOL)testCheckPhrase: (NSString *)phrase withCorrectPhrase:(NSString *)correctPhrase;

@end
