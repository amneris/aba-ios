//
//  ABARealmProgressAction.m
//  ABA
//
//  Created by Jordi Melé on 16/3/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ABAEvaluationOption.h"
#import "ABARealmProgressAction.h"
#import "ABARealmUser.h"
#import "ABAUnit.h"
#import "Utils.h"
#import <UIKit/UIKit.h>
#import "UserController.h"
#import "ABARealmUser.h"
#import "ABAPhrase.h"
#import "APIManager.h"
#import "ABASection.h"
#import "ProgressController.h"
#import "EvaluationController.h"


@implementation ABARealmProgressAction

+ (NSString *)primaryKey
{
    return @"actionId";
}

- (NSDictionary *)toDictionary
{
    NSMutableDictionary *parametersDict = [NSMutableDictionary new];

    if (self.idSession != nil)
    {
        parametersDict[@"idSession"] = self.idSession;
    }

    parametersDict[@"dateInit"] = self.timeStamp;

    if (self.ip != nil)
    {
        parametersDict[@"deviceIp"] = self.ip;
    }

    if (self.idUser != nil)
    {
        parametersDict[@"idUser"] = self.idUser;
    }

    parametersDict[@"idSite"] = @"2";

    if (IS_IPAD)
    {
        parametersDict[@"device"] = @"t";
    }
    else
    {
        parametersDict[@"device"] = @"m";
    }

    if ([self.startSession boolValue])
    {
        parametersDict[@"action"] = self.action;

        return parametersDict;
    }

    else if ([self.action isEqualToString:@"ENTER"])
    {
        parametersDict[@"idUnit"] = self.idUnitString;
        parametersDict[@"action"] = self.action;
        parametersDict[@"idSectionType"] = [self.secionType stringValue];

        return parametersDict;
    }

    else if (self.audioID == nil && ![self.isHelp boolValue])
    {
        return nil;
    }

    else
    {
        parametersDict[@"idUser"] = self.idUser;
        parametersDict[@"idUnit"] = self.idUnitString;
        parametersDict[@"idSectionType"] = [self.secionType stringValue];
        if ([self.isHelp boolValue])
        {
            parametersDict[@"action"] = @"HELP";
            parametersDict[@"audio"] = @"";
        }
        else
        {
            parametersDict[@"action"] = self.action;
            parametersDict[@"audio"] = self.audioID;
        }
        parametersDict[@"page"] = self.page.stringValue;
        parametersDict[@"correct"] = self.correct.stringValue;

        //Pending implementation
        //        parametersDict[@"time"] = @"0";

        if (self.text != nil)
            parametersDict[@"text"] = self.text;
        else
            parametersDict[@"text"] = @"";

        switch (self.secionType.integerValue)
        {
        case kABAAssessmentSectionType:
        {
            parametersDict[@"exercises"] = self.optionLettersEvaluation;
            parametersDict[@"puntuation"] = self.punctuation;
            break;
        }
        default:
            parametersDict[@"exercises"] = @"";
            parametersDict[@"puntuation"] = @0;
            break;
        }

        return parametersDict;
    }
}

- (NSString *)idUnitString
{
    return [NSString stringWithFormat:@"%ld", (long)[self.idUnit integerValue]];
}

+ (ABARealmProgressAction *)progressActionWithUser:(ABARealmUser *)user dateFormatter:(NSDateFormatter *)format phrase:(ABAPhrase *)phrase section:(ABASection *)section unit:(ABAUnit *)unit errorText:(NSString *)text isListen:(BOOL)listen isHelp:(BOOL)help
{
    ABARealmProgressAction *action = [[ABARealmProgressAction alloc] init];

    action.actionId = [[NSUUID UUID] UUIDString];

    action.idUser = user.idUser;
    action.idSession = user.idSession;
    action.audioID = @"";
    action.text = @"";
    action.idUnit = @0;
    action.optionLettersEvaluation = @"";

    NSDate *currentTime = [NSDate date];
    action.timeStamp = [format stringFromDate:currentTime];
    action.ip = [[APIManager sharedManager] publicIp];
    action.language = user.userLang;

    if (section != nil && unit == nil)
    {
        action.idUnit = [section getUnitFromSection].idUnit;

        if (listen)
        {
            action.action = @"LISTENED";
        }
        else
        {
            action.action = [ProgressController getActionStringForSection:section];
        }

        action.secionType = [NSNumber numberWithInteger:[section getSectionType]];

        switch ([section getSectionType])
        {
        case kABAAssessmentSectionType:
        {
            action.page = @0;
            action.correct = @YES;

            action.punctuation = [NSNumber numberWithInteger:[EvaluationController getEvaluationCorrectAnswers:(ABAEvaluation *)section]];

            NSMutableString *answers = [NSMutableString new];
            NSOrderedSet *evaluationAnswers = [EvaluationController getEvaluationAnswers:(ABAEvaluation *)section];

            for (ABAEvaluationOption *option in evaluationAnswers)
            {
                [answers appendFormat:@"%@,", option.optionLetter];
            }

            action.optionLettersEvaluation = [answers substringToIndex:answers.length - 1];

            break;
        }
        case kABAFilmSectionType:
        case kABAVideoClassSectionType:
        {
            action.page = @0;
            action.correct = @YES;
            break;
        }
        case kABAExercisesSectionType:
        {
            action.audioID = phrase.idPhrase;
            action.page = [NSNumber numberWithInteger:[phrase.page integerValue]];

            if (help)
            {
                action.isHelp = @YES;
            }

            if (text != nil)
            {
                action.text = text;
                action.correct = @NO;
            }
            else
            {
                action.correct = @YES;
            }

            break;
        }
        default:
            action.audioID = phrase.audioFile;
            action.page = [NSNumber numberWithInteger:[phrase.page integerValue]];

            if (help)
            {
                action.isHelp = @YES;
            }

            if (text != nil)
            {
                action.text = text;
                action.correct = @NO;
            }
            else
            {
                action.correct = @YES;
            }
            break;
        }
    }
    else if (section != nil && unit != nil)
    {
        action.idUnit = unit.idUnit;
        action.action = @"ENTER";
        action.secionType = [NSNumber numberWithInteger:[section getSectionType]];
    }
    else
    {
        action.startSession = @YES;
        action.action = @"NEWSESSION";
    }

    action.sentToServer = @NO;

    return action;
}

@end
