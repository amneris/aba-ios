//
//  LevelUnitCell.h
//  ABA
//
//  Created by Marc Güell Segarra on 5/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABARealmLevel;

@protocol LevelUnitCellDelegate <NSObject>
- (BOOL)isLevelExpanded:(ABARealmLevel *)ABARealmLevel;

@end

@interface LevelUnitCell : UICollectionViewCell

/**
 *  General properties
 */
@property (nonatomic, weak) id <LevelUnitCellDelegate> delegate;
@property (strong, nonatomic) ABARealmLevel *ABARealmLevel;

/**
 *  IBOutlets
 */
@property (weak, nonatomic) IBOutlet UILabel *levelUnitTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelUnitDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *levelUnitExpandButton;
@property (weak, nonatomic) IBOutlet UIButton *levelUnitCollapseButton;
@property (weak, nonatomic) IBOutlet UIImageView *certificateIcon;
@property (weak, nonatomic) IBOutlet UILabel *levelUnitTitleLabelIndent;
@property (weak, nonatomic) IBOutlet UILabel *levelUnitDescriptionLabelIndent;
@property (weak, nonatomic) IBOutlet UIView *separatorBottomLine;

/**
 *  Setup the cell with the logic data of ABARealmLevel object
 *
 *  @param ABARealmLevel ABARealmLevel object
 */
- (void)setupWithABARealmLevel:(ABARealmLevel *)ABARealmLevel;


@end

