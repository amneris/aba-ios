//
//  EvaluationQuestionViewController.h
//  ABA
//
//  Created by Ivan Ferrera on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "ABAEvaluation.h"

@class ABAEvaluationQuestion;

@interface EvaluationQuestionViewController : SectionBaseViewController

@property (nonatomic, strong) ABAEvaluation * section;
@property (nonatomic, strong) ABAEvaluationQuestion * currentQuestion;

@end
