//
//  CurrecyCacheManager.m
//  ABA
//
//  Created by MBP13 Jesus on 11/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import "CurrencyCacheManager.h"

#define kCurrencyCacheDataFile @"currency_cache.data"
#define kCurrencyHardcodedDataFile @"currency_hardcoded.data"

@implementation CurrencyCacheManager

+ (NSArray *)loadCurrenciesFromCache
{
    NSArray * arrayOfCurrenciesFromCache = nil;
    NSString * cacheFilePath = [self currencyCacheFilePath];
    
    // If file exists
    if ([[NSFileManager defaultManager] fileExistsAtPath:cacheFilePath])
    {
        NSData *data = [NSData dataWithContentsOfFile:cacheFilePath];
        arrayOfCurrenciesFromCache = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    // If there is not any read array it reads the one in the bundle
    if (!arrayOfCurrenciesFromCache || ![arrayOfCurrenciesFromCache isKindOfClass:[NSArray class]])
    {
        // Read from bundle
        NSString * hardcodedFilePath = [[NSBundle mainBundle] pathForResource:kCurrencyHardcodedDataFile ofType:nil];
        NSData *data = [NSData dataWithContentsOfFile:hardcodedFilePath];
        arrayOfCurrenciesFromCache = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    // Making sure it does not return other kind of data
    if (![arrayOfCurrenciesFromCache isKindOfClass:[NSArray class]])
    {
        return nil;
    }
    
    return arrayOfCurrenciesFromCache;
}

+ (void)storeCurrenciesInCache:(NSArray *)arrayOfCurrencies
{
    NSString * filePath = [self currencyCacheFilePath];
    [NSKeyedArchiver archiveRootObject:arrayOfCurrencies toFile:filePath];
}

#pragma mark - Private methods

+ (NSString *)currencyCacheFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectoryPath = [paths objectAtIndex:0];
    NSString *filePath = [cacheDirectoryPath stringByAppendingPathComponent:kCurrencyCacheDataFile];
    
    return filePath;
}

@end