//
//  ABAActionButtonView.h
//  ABA
//
//  Created by Marc Güell Segarra on 3/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

// iPhone 3.5" and 4"
#define ACTIONBUTTON_CONTROLVIEW_HEIGHT 100
#define ACTIONBUTTON_VIEW_WIDTH 160
#define ACTIONBUTTON_VIEW_HEIGHT 100
#define ACTIONBUTTON_BUTTON_WIDTH 55
#define ACTIONBUTTON_BUTTON_HEIGHT 55
#define ACTIONBUTTON_TOP_SPACE 10
#define ACTIONBUTTON_SEPARATOR 10
#define ACTIONBUTTON_LABEL_WIDTH 220
#define ACTIONBUTTON_LABEL_HEIGHT 21

// iPhone 4.7" and 5.5"
#define ACTIONBUTTON_CONTROLVIEW_HEIGHT_BIG 125
#define ACTIONBUTTON_VIEW_WIDTH_BIG 160
#define ACTIONBUTTON_VIEW_HEIGHT_BIG 120
#define ACTIONBUTTON_BUTTON_WIDTH_BIG 75
#define ACTIONBUTTON_BUTTON_HEIGHT_BIG 75
#define ACTIONBUTTON_TOP_SPACE_BIG 10
#define ACTIONBUTTON_SEPARATOR_BIG 10
#define ACTIONBUTTON_LABEL_WIDTH_BIG 220
#define ACTIONBUTTON_LABEL_HEIGHT_BIG 21

@class ABAPulseViewController;

@protocol ABAActionButtonViewDelegate <NSObject>

@optional
-(void)listenButtonTapped;
-(void)startRecordButtonTapped;
-(void)stopRecordButtonTapped;
-(void)compareButtonTapped;
-(void)continueButtonTapped;

@end

@interface ABAActionButtonView : UIView

@property (nonatomic, weak) id <ABAActionButtonViewDelegate> delegate;
@property (strong, nonatomic) ABAPulseViewController *pulseViewController;
@property (strong, nonatomic) UILabel *actionLabel;

@property kABAActionButtonBehaviour buttonBehaviour;

-(id) initWithSectionType: (kABASectionTypes)type;

- (void)startListeningAnimation;
- (void)stopListeningAnimation;
- (void)startRecordingAnimation;
- (void)stopRecordingAnimation;

-(void)removeButtonTapRecognizer;
-(void)addButtonTapRecognizer;

-(void)switchToType: (kABAActionButtonType)type forSection:(kABASectionTypes)section;

-(void)showLabel: (BOOL) show;

-(void)setEnabled: (BOOL) enabled;

-(void)removeButtonAnimations;

@end
