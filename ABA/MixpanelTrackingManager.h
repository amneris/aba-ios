//
//  MixpanelTrackingManager.h
//  ABA
//
//  Created by Jesus Espejo on 19/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#import "Definitions.h"
#import "GenericTrackingManager.h"

@class ABARealmLevel;
@class ABAPlan;
@class ABASection;
@class ABAUnit;

@interface MixpanelTrackingManager : GenericTrackingManager

+ (instancetype)sharedManager;

// Reset and config
- (void)reset;

// APNS
- (void)registerToken:(NSData *)deviceToken;
- (void)trackPushNotificationIfExists:(NSDictionary *)launchOptions;
- (void)trackPushNotification:(NSDictionary *)userInfo;

// Analytics

/**
 Downloads
 */
- (void)trackDownloadStarted:(ABAUnit *)unit;
- (void)trackDownloadCancelled:(ABAUnit *)unit;
- (void)trackDownloadError:(ABAUnit *)unit;
- (void)trackDownloadFinish:(ABAUnit *)unit;

/**
 Method to track an event when the app was opened for the first time.
 It internally checks whether it is the first time the method is called.
 */
- (void)trackFirstAppOpenOnce;

/**
 Method to track user login event
 */
- (void)trackLoginEvent:(RecordType)type;

/**
 Method to track login superproperties so that they are sent with every event
 */
- (void)trackLoginSuperproperties;

/**
 Method to track the user when he did offline login
 */
- (void)trackLoginOfflineEvent;

/**
 Method to track user registration
 */
- (void)trackRegister:(RecordType)type;

/**
 Method to track every level change
 */
- (void)trackLevelChange:(ABARealmLevel *)fromLevel toLevel:(ABARealmLevel *)toLevel;

/**
 Method to track whether the Pricing screen is open and the screen that invoked the very same Pricing screen
 */
- (void)trackPricingScreenArrivalWithOriginController:(PlansViewControllerOriginType)type;

/**
 Method to track the plan selected for the user
 */
- (void)trackPricingSelected:(ABAPlan *)plan;

/**
 Method to track the purchase
 */
- (void)trackPurchase:(SKPaymentTransaction *)transaction withPeriod:(NSInteger)period withProduct:(SKProduct *)product withOriginalPrice:(NSDecimalNumber *)originalPrice;

/**
 Method to track the purchase activation
 */
- (void)trackActivatedPurchase;

/**
 Method to track the info button action in the plans screen
 */
- (void)trackInfoButtonPushed:(PlanInfoType)infoType;

/**
 Method to track changes at the notification configuration
 */
- (void)trackNotificationConfigurationChange;

/**
 Method to update superproperties for APNS configuration
 */
- (void)trackNotificationSuperproperties:(BOOL)enabled;

/**
 Method to track the section progress and timespent
 */
- (void)trackSection:(ABASection *)section withSecondsOfStudyTime:(double)secondsOfStudy withInitialProgress:(NSNumber *)initialProgress withFinalProgress:(NSNumber *)finalProgress;

/**
 Method to track purchase errors
 */
- (void)trackSubscriptionErrorWithSubscriptionStatus:(SubscriptionStatus)status;

/**
 Method to show IN APP Notifications and Surveys
 */
- (void)showMixpanelNotificationsAndSurveys;

- (void)trackVariation:(nonnull NSString *)key value:(nonnull NSString *)value;
- (void)trackConfigurationChanged;

/**
 Methods to track Registration Funnel AB Version
 */
- (void)trackCurrentFunnelTypeSuperProperties;
- (void)trackRegistrationFunnelWithRecordType:(RecordType)type withFunnelActionType:(RegistrationFunnelActionType)action;
- (void)trackRegistrationFunnelShown;

@end
