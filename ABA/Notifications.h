//
//  Notifications.h
//  ABA
//
//  Created by MBP13 Jesus on 16/12/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#ifndef Notifications_h
#define Notifications_h

#define kUnitViewProgressUpdatedNotification @"UnitViewProgressUpdatedNotification"
#define kUnitProgressUpdatedNotification @"UnitProgressUpdatedNotification"
#define kLevelProgressUpdatedNotification @"LevelProgressUpdatedNotification"
#define kUpdateRotationFromMenu @"updateRotationFromMenu"

#endif /* Notifications_h */
