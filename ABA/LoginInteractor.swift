
//
// Created by Oleksandr Gnatyshyn on 13/06/16.
// Copyright (c) 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

class LoginInteractor {

    let repository: LoginRepository

    init(repository: LoginRepository) {
        self.repository = repository
    }

}

extension LoginInteractor: UnitDownloader {
    // Now capable of downloading units
}

extension LoginInteractor: LoginInteractorInput {
    func login(email: String, password: String) -> Observable<User> {
        return self.repository.login(email: email, password: password)
            .flatMap { self.downloadUnits($0) }
            .do(onNext: { user in
                APIManager.shared().token = user.token
                DataController.saveProgressAction(for: nil, andUnit: nil, andPhrases: nil, errorText: nil, isListen: false, isHelp: false, completionBlock: nil)
            })
    }

    func login(token: String) -> Observable<User> {
        return self.repository.login(token: token)
            .flatMap { self.downloadUnits($0) }
            .do(onNext: { user in APIManager.shared().token = user.token })
    }
    
    func loginWithFacebook(_ facebookUser: FacebookUser) -> Observable<User> {
        return self.repository
            .loginWithFacebook(facebookId: facebookUser.facebookId, name: facebookUser.firstName, surname: facebookUser.lastName, email: facebookUser.email, gender: facebookUser.gender, avatar: facebookUser.imageUrl)
            .catchError { _ in throw SessionInteractorError.connectionError }
            .flatMap { self.downloadUnits($0) }
            .catchError { error in throw SessionInteractorError.unableToDownloadUnits }
            .do(onNext: { user in
                APIManager.shared().token = user.token
                DataController.saveProgressAction(for: nil, andUnit: nil, andPhrases: nil, errorText: nil, isListen: false, isHelp: false, completionBlock: nil)
            })
    }
}

extension LoginInteractor {
    func singleLogin(email: String, password: String) -> Observable<User> {
        return self.repository.login(email: email, password: password).map { $0 as User }
    }

    func singleLogin(token: String) -> Observable<User> {
        return self.repository.login(token: token).map { $0 as User }
    }
}
