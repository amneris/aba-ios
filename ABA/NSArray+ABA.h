//
//  NSArray+ABA.h
//  ABA
//
//  Created by Oriol Vilaró on 25/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RLMObject;

@interface NSArray (ABA)

- (NSUInteger)indexOfRealmObject:(RLMObject *)anObject;

- (BOOL)containsRealmObject:(RLMObject *)anObject;

@end
