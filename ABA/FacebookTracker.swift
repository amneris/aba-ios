//
//  FacebookTracker.swift
//  ABA
//
//  Created by MBP13 Jesus on 04/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol FacebookTracker {
    func trackAccessWithFacebook(_ email: String, name: String, newUser: Bool)
}
