//
//  EvaluationViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "EvaluationViewController.h"
#import "UIViewController+ABA.h"
#import "ABAEvaluation.h"
#import "Utils.h"
#import "Resources.h"
#import "Definitions.h"
#import "ABAUnit.h"
#import "ABAAlert.h"
#import "EvaluationQuestionViewController.h"
#import "EvaluationController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageHelper.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import "LegacyTrackingManager.h"
#import "DataController.h"

@interface EvaluationViewController ()

@property (weak, nonatomic) IBOutlet UILabel *teacherLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewButtonTrailingConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewButtonLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewButtonTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewButtonBottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherImageWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherImageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherSubViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherSubviewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherImageTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *teacherLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelHeightConstraint;


- (IBAction)startAction:(id)sender;

@end

@implementation EvaluationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupView];
    
    [DataController saveProgressActionForSection:self.section andUnit:[self.section getUnitFromSection] andPhrases:nil errorText:nil isListen:NO isHelp: NO completionBlock:^(NSError *error, id object) {
        
    }];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"assessmentsection"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupView
{
    
    self.view.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    [self addBackButtonWithTitle:STRTRAD(@"evaluaKey", @"Evaluación") andSubtitle:@""];
    
    ABARealmUser *user = [UserController currentUser];
    
    if(user.teacherImage != nil)
    {
        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:user.teacherImage];
        NSURL *imageUrl = [NSURL URLWithString:urlString];
        
        [_teacherImage sd_setImageWithURL:imageUrl];
    }
    
    _teacherLabel.text = STRTRAD(@"evaluationTeacherMessageKey", @"Ahora es el momento de evaluar lo que has aprendido");
    _teacherLabel.textColor = ABA_COLOR_DARKGREY;
	_teacherLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?36:24];
	
    _startButton.layer.cornerRadius = 3.0f;
    _startButton.backgroundColor = ABA_COLOR_BLUE;
    [_startButton setTitle:STRTRAD(@"startEvaluationButtonKey", @"empezar evaluación") forState:UIControlStateNormal];
	_startButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:14];
	
    NSString *txt0 = STRTRAD(@"evaluationInfoText1Key", @"La evaluación te llevará un rato concentrado, así que asegurate de estar en un lugar tranquilo,");
    NSString *txt1 = STRTRAD(@"evaluationInfoText2Key", @"¡sin ruido y sin interrupciones!");
    NSString *txt2 = STRTRAD(@"evaluationInfoText3Key", @"Para superar el test, debes obtener una puntuación superior a 8/10. Si no obtienes este resultado, debes repetir la prueba hasta que lo consigas");
    NSString *str = [NSString stringWithFormat:@"%@ %@\n\n%@", txt0, txt1, txt2];
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
    [mutAttStr addAttribute:NSFontAttributeName
					  value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                      range:[str rangeOfString:txt0]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?18:14]
                      range:[str rangeOfString:txt1]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                      range:[str rangeOfString:txt2]];
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0;
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [mutAttStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    
    
    NSMutableParagraphStyle * negritaParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    negritaParagraphStyle.lineSpacing = 1.0;
    [mutAttStr addAttribute:NSParagraphStyleAttributeName value:negritaParagraphStyle range:[str rangeOfString:txt1]];
    
    _infoLabel.textColor = ABA_COLOR_DARKGREY;
    [_infoLabel setAttributedText:mutAttStr.copy];
    [_infoLabel sizeToFit];
	
    UILabel *ghostLavelView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - _infoLabelLeadingConstraint.constant - _infoLabelTrailingConstraint.constant, 9999)];
    ghostLavelView.numberOfLines = 0;
    ghostLavelView.textColor = ABA_COLOR_DARKGREY;
    [ghostLavelView setAttributedText:mutAttStr.copy];
    [ghostLavelView sizeToFit];
    
    _infoLabelHeightConstraint.constant = ghostLavelView.frame.size.height;
    
	if(IS_IPAD) {
		_infoLabel.textAlignment = NSTextAlignmentJustified;
		
		_bottomViewHeightConstraint.constant = 135.0f;
		_bottomViewButtonTrailingConstant.constant = 176.0f;
		_bottomViewButtonLeadingConstraint.constant = 176.0f;
		_bottomViewButtonTopSpaceConstraint.constant = 40.0f;
		_bottomViewButtonBottomSpaceConstraint.constant = 40.0f;
		_teacherImageWidthConstraint.constant = 110.0f;
		_teacherImageHeightConstraint.constant = 110.0f;
		
		_teacherLabelHeightConstraint.constant = _teacherLabelHeightConstraint.constant * 1.6;
		_infoLabelLeadingConstraint.constant = _infoLabelLeadingConstraint.constant * 4;
		_infoLabelTrailingConstraint.constant = _infoLabelTrailingConstraint.constant * 4;
		_teacherSubViewHeightConstraint.constant = _teacherSubViewHeightConstraint.constant * 1.3;
		_teacherViewHeightConstraint.constant = _teacherViewHeightConstraint.constant * 1.5;
		_teacherSubviewTopConstraint.constant = _teacherSubviewTopConstraint.constant * 1.6;
		_teacherLabelTopConstraint.constant = _teacherLabelTopConstraint.constant * 1.2;
		_infoLabelTopConstraint.constant = _infoLabelTopConstraint.constant * 2.5;
		_teacherImageTopConstraint.constant = _teacherImageTopConstraint.constant - 10.0f;
		_teacherLabelWidthConstraint.constant = _teacherLabelWidthConstraint.constant * 1.5f;
	}
}

- (IBAction)startAction:(id)sender {
    
    EvaluationQuestionViewController *questionViewController = [[UIStoryboard storyboardWithName:@"Evaluation" bundle:nil] instantiateViewControllerWithIdentifier:@"EvaluationQuestionViewController"];
    [questionViewController setSection:self.section];
    [self.navigationController pushViewController:questionViewController animated:YES];
    
}
@end
