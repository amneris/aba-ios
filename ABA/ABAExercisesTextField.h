//
//  ABAExercisesTextField.h
//  ABA
//
//  Created by Marc Güell Segarra on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABAExercisePhraseItem;

@interface ABAExercisesTextField : UITextField

@property ABAExercisePhraseItem *correctAnswer;

@end
