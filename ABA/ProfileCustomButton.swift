//
//  ProfileCustomButton.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 19/12/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit

@IBDesignable class ProfileCustomButton: UIButton {
    @IBInspectable var mainColor: UIColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) {
        didSet {
            layer.borderColor = mainColor.cgColor
            self.setTitleColor(mainColor, for: .normal)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet { layer.borderWidth = borderWidth }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 4.0
        clipsToBounds = true
    }
}
