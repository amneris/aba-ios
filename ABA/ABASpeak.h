//
//  ABASpeak.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASection+Methods.h"
#import "ABASpeakDialog.h"

@class ABAUnit;

@interface ABASpeak : ABASection

@property RLMArray<ABASpeakDialog *><ABASpeakDialog> *content;
@property (readonly) ABAUnit *unit;

-(BOOL) hasContent;

@end
