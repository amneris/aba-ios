//
//  NewChangePasswordViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 12/12/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import SkyFloatingLabelTextField
import Device
import SVProgressHUD

enum visibilityButton: Int {
    case oldPassword = 1, newPassword, repeatNewPassword
}

class NewChangePasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var oldPasswordField: SkyFloatingLabelTextField!
    @IBOutlet weak var oldPasswordVisibilityButton: UIButton!
    @IBOutlet weak var newPasswordField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordVisibilityButton: UIButton!
    @IBOutlet weak var repeatNewPasswordField: SkyFloatingLabelTextField!
    @IBOutlet weak var repeatNewPasswordVisibilityButton: UIButton!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    let currentUser = UserController.currentUser()
    let disposableBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton(withTitleBlue: String.localize("changePasswordTitleKey", comment: ""))
        
        setupConstrains()
        setupView()
        
        let oldPasswordStream = oldPasswordField
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.oldPasswordField.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
    
        let newPasswordStream = newPasswordField
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.newPasswordField.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
    
        let repeatNewPasswordStream = repeatNewPasswordField
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.repeatNewPasswordField.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
    
        Observable.combineLatest(oldPasswordStream, newPasswordStream, repeatNewPasswordStream) { [unowned self] (oldPassword, newPassword, repeatNewPassword) -> Bool in
            
            if let oldPassword = oldPassword, oldPassword.characters.count > 0 {
                self.validatePassword(self.oldPasswordField)
            }
    
            if let newPassword = newPassword, newPassword.characters.count > 0 {
                self.validatePassword(self.newPasswordField)
            }
    
            if let repeatNewPassword = repeatNewPassword, repeatNewPassword.characters.count > 0 {
                self.validatePasswordRepeat(self.newPasswordField, repeatInput: self.repeatNewPasswordField)
            }
    
            return self.isPasswordFieldValid(oldPassword) && self.isPasswordFieldValid(newPassword) && self.isPasswordFieldValid(repeatNewPassword) && newPassword == repeatNewPassword
            }.subscribe(onNext: { [unowned self] formIsComplete in
                self.updateRegisterButtonStyle(formIsComplete)
            }).addDisposableTo(disposableBag)
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if Device.type() == .iPad {
            return .all
        }
        else {
            return .portrait
        }
    }
    
    fileprivate func setupConstrains() {
        oldPasswordField.snp.makeConstraints { [unowned self] (make) in
            if RegisterFunnelConstants.isIpadOrIpadScreenSize() {
                make.width.equalTo(RegisterFunnelConstants.formWidthForIpad)
                make.centerX.equalTo(self.view)
            }
            else {
                make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
                make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
            }
    
            make.top.equalTo(self.view).offset(RegisterFunnelConstants.formTopPadding)
        }
        
        newPasswordField.snp.makeConstraints { [unowned self] (make) in
            make.left.equalTo(self.oldPasswordField.snp.left)
            make.right.equalTo(self.oldPasswordField.snp.right)
            make.top.equalTo(self.oldPasswordField.snp.bottom).offset(RegisterFunnelConstants.formFieldsPadding)
        }
        
        repeatNewPasswordField.snp.makeConstraints { [unowned self] (make) in
            make.left.equalTo(self.oldPasswordField.snp.left)
            make.right.equalTo(self.oldPasswordField.snp.right)
            make.top.equalTo(self.newPasswordField.snp.bottom).offset(RegisterFunnelConstants.formFieldsPadding)
        }
        
        changePasswordButton.snp.makeConstraints { [unowned self] (make) in
            make.height.equalTo(40)
            make.left.equalTo(self.oldPasswordField.snp.left)
            make.right.equalTo(self.oldPasswordField.snp.right)
            make.top.equalTo(self.repeatNewPasswordField.snp.bottom).offset(RegisterFunnelConstants.formFieldsPadding)
        }
        
        oldPasswordVisibilityButton.snp.makeConstraints { (make) in
            make.right.equalTo(oldPasswordField)
            make.centerY.equalTo(oldPasswordField)
            make.height.width.equalTo(30)
        }
        
        newPasswordVisibilityButton.snp.makeConstraints { (make) in
            make.right.equalTo(newPasswordField)
            make.centerY.equalTo(newPasswordField)
            make.height.width.equalTo(30)
        }
        
        repeatNewPasswordVisibilityButton.snp.makeConstraints { (make) in
            make.right.equalTo(repeatNewPasswordField)
            make.centerY.equalTo(repeatNewPasswordField)
            make.height.width.equalTo(30)
        }
    }
    
    fileprivate func setupView() {
        changePasswordButton.backgroundColor = RegisterFunnelConstants.colorFormGray
        changePasswordButton.setTitleColor(RegisterFunnelConstants.colorRegisterGrayText, for: .normal)
        changePasswordButton.setAttributedTitle(String.localize("profileChangePasswordKey", comment: "").uppercased().styled(with: ProfileStyle.changePasswordButtonStyle), for: .normal)
        changePasswordButton.layer.cornerRadius = 2
        
        oldPasswordField.placeholder = String.localize("changePassword1Key", comment: "")
        oldPasswordField.title = String.localize("changePassword1Key", comment: "")
        oldPasswordField.font = RegisterFunnelConstants.formFieldsFont
        oldPasswordField.tintColor = RegisterFunnelConstants.colorFormGray
        oldPasswordField.textColor = RegisterFunnelConstants.colorFormGray
        oldPasswordField.lineColor = RegisterFunnelConstants.colorFormGray
        oldPasswordField.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        oldPasswordField.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        oldPasswordField.autocorrectionType = .no
        oldPasswordField.keyboardType = UIKeyboardType.default
        oldPasswordField.isSecureTextEntry = true
        oldPasswordField.delegate = self
        oldPasswordField.returnKeyType = .join
        
        newPasswordField.placeholder = String.localize("changePassword3Key", comment: "")
        newPasswordField.title = String.localize("changePassword2Key", comment: "")
        newPasswordField.font = RegisterFunnelConstants.formFieldsFont
        newPasswordField.tintColor = RegisterFunnelConstants.colorFormGray
        newPasswordField.textColor = RegisterFunnelConstants.colorFormGray
        newPasswordField.lineColor = RegisterFunnelConstants.colorFormGray
        newPasswordField.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        newPasswordField.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        newPasswordField.autocorrectionType = .no
        newPasswordField.keyboardType = UIKeyboardType.default
        newPasswordField.isSecureTextEntry = true
        newPasswordField.delegate = self
        newPasswordField.returnKeyType = .join
        
        repeatNewPasswordField.placeholder = String.localize("changePassword3Key", comment: "")
        repeatNewPasswordField.title = String.localize("changePassword3Key", comment: "")
        repeatNewPasswordField.font = RegisterFunnelConstants.formFieldsFont
        repeatNewPasswordField.tintColor = RegisterFunnelConstants.colorFormGray
        repeatNewPasswordField.textColor = RegisterFunnelConstants.colorFormGray
        repeatNewPasswordField.lineColor = RegisterFunnelConstants.colorFormGray
        repeatNewPasswordField.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        repeatNewPasswordField.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        repeatNewPasswordField.autocorrectionType = .no
        repeatNewPasswordField.keyboardType = UIKeyboardType.default
        repeatNewPasswordField.isSecureTextEntry = true
        repeatNewPasswordField.delegate = self
        repeatNewPasswordField.returnKeyType = .join
        
  
        oldPasswordVisibilityButton.addTarget(self, action: #selector(showHidePasswordAction), for: .touchUpInside)
        newPasswordVisibilityButton.addTarget(self, action: #selector(showHidePasswordAction), for: .touchUpInside)
        repeatNewPasswordVisibilityButton.addTarget(self, action: #selector(showHidePasswordAction), for: .touchUpInside)
    }
    
    @IBAction func changePassword(_ sender: Any) {
        SVProgressHUD.show(with: .black)
        UserController.checkUserPassword(withEmail: currentUser?.email, withPassword: oldPasswordField.text) { [unowned self] (error, status) in
            
            if let nsError = error as NSError? {
                SVProgressHUD.dismiss()
                if (nsError.code == Int(CODE_CONNECTION_ERROR)) {
                    let abaAlert: ABAAlert = ABAAlert.init(alertWithText: nsError.localizedDescription)
                    abaAlert.show()
                }
                else {
                    let alert: ABAAlert =  ABAAlert.init(alertWithText: String.localize("changePassErrorOldPassword", comment: ""))
                    alert.show(withOffset: 44)
                }
            } else {
                UserController.updatePasswordUser(withToken: self.currentUser?.token, withPassword: self.newPasswordField.text, completionBlock: { [unowned self] (error, response) in
                    SVProgressHUD.dismiss()
                    if let nsError = error as NSError? {
                        if (nsError.code == Int(CODE_CONNECTION_ERROR)) {
                            let abaAlert: ABAAlert = ABAAlert.init(alertWithText: nsError.localizedDescription)
                            abaAlert.show()
                        }else {
                            let alert: ABAAlert =  ABAAlert.init(alertWithText: String.localize("errorUpdatePass", comment: ""))
                            alert.show(withOffset: 44)
                        }
                    } else {
                        let _ = self.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
    }
    
    fileprivate func isPasswordFieldValid(_ password: String?) -> Bool {
        return (!MDTUtils.isTextEmpty(password) && MDTUtils.isCorrectPassword(password))
    }
    
    fileprivate func validatePassword(_ input: SkyFloatingLabelTextField) {
        if !isPasswordFieldValid(input.text) {
            input.errorMessage = String.localize("regErrorPasswordMin", comment: "")
        }
    }
    
    fileprivate func validatePasswordRepeat(_ input: SkyFloatingLabelTextField, repeatInput: SkyFloatingLabelTextField) {
        if let repeatPass = repeatInput.text {
            if repeatPass != input.text {
                repeatInput.errorMessage = String.localize("changePassErrorEqualPassword", comment: "")
            }
        }
    }

    fileprivate func updateRegisterButtonStyle(_ isActive: Bool) {
        if isActive {
            changePasswordButton.backgroundColor = RegisterFunnelConstants.colorAquaBlue
            changePasswordButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            changePasswordButton.isUserInteractionEnabled = true
        } else {
            changePasswordButton.backgroundColor = RegisterFunnelConstants.colorFormGray
            changePasswordButton.setTitleColor(RegisterFunnelConstants.colorRegisterGrayText, for: .normal)
            changePasswordButton.isUserInteractionEnabled = false
        }
    }
    
    @objc fileprivate func showHidePasswordAction(_ sender: UIButton) {
        var passwordInput: UITextField? = nil
        
        switch sender.tag {
        case visibilityButton.oldPassword.rawValue:
            passwordInput = oldPasswordField
        case visibilityButton.newPassword.rawValue:
            passwordInput = newPasswordField
        case visibilityButton.repeatNewPassword.rawValue:
            passwordInput = repeatNewPasswordField
        default: break;
        }
        
        if let input = passwordInput {
            if input.isSecureTextEntry {
                input.isSecureTextEntry = false
                input.font = nil
                input.font = RegisterFunnelConstants.formFieldsFont
                sender.setImage(UIImage(named: "hide_password"), for: UIControlState())
            } else {
                input.isSecureTextEntry = true
                input.font = nil
                input.font = RegisterFunnelConstants.formFieldsFont
                sender.setImage(UIImage(named: "show_password"), for: UIControlState())
            }
        }
    }
}
