//
//  ExperimentRunner.h
//  ABA
//
//  Created by Oriol Vilaró on 6/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^VariationBlock)();

@class ABAExperiment;
@interface ExperimentRunner : NSObject

+ (void)runExperiment:(NSString *)experimentIdentifier withBaseline:(VariationBlock)baselineBlock andVariationBlockDict:(NSDictionary *)variationBlockDict;
+ (void)saveExperimentWithExperimentProfile:(NSArray *)profilesArray;

@end
