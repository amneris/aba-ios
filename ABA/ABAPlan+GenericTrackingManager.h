//
//  ABAPlan+MixpanelTrackingManager.h
//  ABA
//
//  Created by Jesus Espejo on 20/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABAPlan.h"
#import "TrackingManagerProtocol.h"

@interface ABAPlan(GenericTrackingManager) < ABATrackingManagerProtocol >

@end
