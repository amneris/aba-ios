//
//  UnitCell.h
//  ABA
//
//  Created by Oriol Vilaró on 30/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Type of cell
 */
typedef NS_ENUM(NSUInteger, UnitCellType){
    /**
     *  top cell
     */
    TopUnitCellType,
    /**
     *  middle cell
     */
    MiddleUnitCellType,
    /**
     *  bottom cell
     */
    BottomUnitCellType,
};

/**
 *  Status of unit
 */
typedef NS_ENUM(NSUInteger, StatusCellType){
    /**
     *  done status
     */
    DoneStatusCellType,
    /**
     *  in progress status
     */
    InProgressStatusCellType,
    /**
     *  To do status
     */
    ToDoStatusCellType,
};

@class ABAUnit, LevelUnitViewController, UnitCell;

@protocol UnitCellDelegate <NSObject>

-(void) downloadUnitButtonPressed:(UnitCell*) sender;

@end

@interface UnitCell : UICollectionViewCell

@property (weak) id<UnitCellDelegate> delegate;

/**
 *  IBOutlets
 */
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIView *upperLineView;
@property (weak, nonatomic) IBOutlet UIView *lowerLineView;
@property (weak, nonatomic) IBOutlet UILabel *unitCircleLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitDescLabel;
@property (weak, nonatomic) IBOutlet UIImageView *unitImage;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *unitDownloadButton;

- (IBAction)unitDownloadButtonAction:(id)sender;

/**
 *  Type and status
 */
@property (assign, readonly)    UnitCellType unitCellType;
@property (assign, readonly)    StatusCellType statusCellType;
@property (nonatomic)           BOOL isLastCompletedUnit;
@property (nonatomic)           NSInteger progress;

@property LevelUnitViewController *parentVC;

/**
 *  Setup the cell UnitCellId with ABAUnit data
 *
 *  @param ABARealmLevel ABAUnit object
 */
- (void)setupUnitWithABAUnit:(ABAUnit *)abaUnit
                 currentUnit:(ABAUnit*) currentUnit
 isLastUnitCompletedForLevel:(BOOL)isUnitLastCompletedForLevel;

@end
