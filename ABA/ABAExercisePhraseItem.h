//
//  ABAExercisePhraseItem.h
//  ABA
//
//  Created by Marc Güell Segarra on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"

@interface ABAExercisePhraseItem : NSObject

@property (strong,nonatomic) NSString *text;
@property (strong,nonatomic) NSString *audioFile;
@property (strong,nonatomic) NSString *idPhrase;
@property (strong,nonatomic) NSString *page;
@property kABAExercisePhraseItemType type;

@end
