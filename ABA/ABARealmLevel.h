//
//  ABARealmLevel.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAUnit.h"

@class ABARealmCert, ABARealmUser;

@interface ABARealmLevel : RLMObject

@property NSNumber<RLMBool> * completed;
@property NSString * desc;
@property NSString * idLevel;
@property NSString * name;
@property NSNumber<RLMInt> * progress;
@property (readonly) ABARealmCert *cert;
@property RLMArray<ABAUnit *><ABAUnit> *units;

-(NSString *)getPercentage;
-(float)getPercentageCertificateMaxProgress:(float)maxProgress;
-(ABAUnit *)getCurrentUnit;

@end
