//
//  NSAttributedString+Height.h
//  ABA
//
//  Created by Jordi Mele on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSAttributedString (Height)

-(CGFloat)heightForWidth:(CGFloat)width;

@end
