//
//  ABASpeakDialog.h
//  
//
//  Created by Jordi Mele on 12/1/15.
//
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASpeakPhrase.h"
#import "ABASpeakDialogPhrase.h"


@class ABAPhrase;

@interface ABASpeakDialog : RLMObject

@property NSString * role;
@property RLMArray<ABASpeakDialogPhrase *><ABASpeakDialogPhrase> *dialog;
@property RLMArray<ABASpeakPhrase *><ABASpeakPhrase> *sample;

@end

RLM_ARRAY_TYPE(ABASpeakDialog)
