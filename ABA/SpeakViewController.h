//
//  SpeakViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "Definitions.h"
#import "ABASpeakPhrase.h"
#import "ABAActionButtonView.h"
#import "SectionProtocol.h"
#import "ABAControlView.h"
#import "ABASpeak.h"

@class ABASpeak;

@interface SpeakViewController : SectionBaseViewController <ABAActionButtonViewDelegate, ABAControlViewDelegate>

@property (strong, nonatomic) ABASpeak *section;

@property kABASpeakStatus currentStatus;
@property (weak) id <SectionProtocol> delegate;

@end
