//
//  ABAAlert.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABAAlert;

/**
 *  <#Description#>
 *
 *  @param alert       <#alert description#>
 *  @param buttonIndex <#buttonIndex description#>
 */
typedef void (^ButtonBlock)(ABAAlert *alert, NSUInteger buttonIndex);

@interface ABAAlert : UIViewController

/**
 *  <#Description#>
 *
 *  @param text        <#text description#>
 *  @param buttonText  <#buttonText description#>
 *  @param buttonBlock <#buttonBlock description#>
 *
 *  @return <#return value description#>
 */
-(id)initAlertWithText:(NSString*)text
       withButtonText:(NSString*)buttonText
           buttonBlock:(ButtonBlock)buttonBlock;

/**
 *  <#Description#>
 *
 *  @param text             <#text description#>
 *  @param firstButtonText  <#firstButtonText description#>
 *  @param secondButtonText <#secondButtonText description#>
 *  @param buttonBlock      <#buttonBlock description#>
 *
 *  @return <#return value description#>
 */
-(id)initAlertWithText:(NSString*)text
       firstButtonText:(NSString*)firstButtonText
      secondButtonText:(NSString*)secondButtonText
           buttonBlock:(ButtonBlock)buttonBlock;

/**
 *  <#Description#>
 *
 *  @param text        <#text description#>
 *  @param correct     <#buttonText description#>
 *  @param buttonBlock <#buttonBlock description#>
 *
 *  @return <#return value description#>
 */
-(id)initAlertWithText:(NSString*)text
           withCorrect:(BOOL)correct;

/**
 *  <#Description#>
 *
 *  @param text        <#text description#>
 *  @param correct     <#buttonText description#>
 *  @param buttonBlock <#buttonBlock description#>
 *
 *  @return return value description
 */
-(id)initAlertEmailWithText:(NSString*)text
           withCorrect:(BOOL)correct;

/**
 *  <#Description#>
 *
 *  @param text <#text description#>
 *
 *  @return <#return value description#>
 */
-(id)initAlertHelpWithText:(NSString*)text;

/**
 *  <#Description#>
 *
 *  @param text <#text description#>
 *
 *  @return <#return value description#>
 */
-(id)initAlertWithText:(NSString*)text;

/**
 *  <#Description#>
 */
-(void)show;


/**
 *  <#Description#>
 */
-(void)showWithOffset:(CGFloat)offset;

/**
 *  <#Description#>
 */
-(void)dismiss;

-(void)showWithOffset:(CGFloat)offset andDissmisAfter: (CGFloat)delay;
@end
