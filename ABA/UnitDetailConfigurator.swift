//
//  UnitDetailConfigurator.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

@objc
class UnitDetailConfigurator: NSObject {
    
    class func createUnitDetailViperStack(unitID: NSNumber, userId: String, isPremium: Bool, isNewUser: Bool) -> UnitListViewController {
        
        let unitListDetail = UnitListViewController(unitIdToLoad: unitID)
        configure(unitListDetail, unitID: unitID, userId: userId, isPremium: isPremium, isNewUser: isNewUser)
        
        return unitListDetail
    }
}

extension UnitDetailConfigurator {
    
    class func configure(_ viewController: UnitListViewController, unitID: NSNumber, userId: String, isPremium: Bool, isNewUser: Bool) {
        
        let interactor = UnitDetailInteractor()
        let presenter = UnitDetailPresenter(unitIdToLoad: unitID, isPremium: isPremium, isNewUser: isNewUser)
        let router = UnitDetailRouter(vc: viewController, userId: userId)
        
        presenter.view = viewController
        presenter.detailInteractor = interactor
        presenter.router = router
        viewController.presenter = presenter
        
        // Setting presenter properties for a proper behaviour
        presenter.isUserPremium = isPremium
        presenter.isNewRegister = isNewUser
    }
}
