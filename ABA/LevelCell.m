//
//  LevelCell.m
//  ABA
//
//  Created by Oriol Vilaró on 31/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "LevelCell.h"
#import "Resources.h"
#import "ABARealmLevel.h"
#import "Utils.h"

@interface LevelCell()

@end

@implementation LevelCell

-(IBAction)infoButtonAction:(id)sender {
    [_delegate infoButtonPressed:self];
}

@end
