//
//  PlansPresenter.swift
//  ABA
//
//  Created by MBP13 Jesus on 14/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

@objc
class PlansPresenter: NSObject {
    
    // VIPER
    weak var view: PlansViewInput?
    var selligentInteractor: SelligentPlansInteractorInput?
    var iTunesInteractor: ItunesPlansInteractor?
    
    // RX
    let disposableBag = DisposeBag()
}

extension PlansPresenter: PlansViewOutput {
    
    func getPlans(_ userId: String!) {
        
        let dependencies = Dependencies()
        
        self.selligentInteractor?.getProducts(userId: userId)
            .subscribeOn(dependencies.backgroundWorkScheduler)
            .flatMap { selligentProduct -> Observable<PlansPageProducts> in
                return self.iTunesInteractor!.fetchInfoFromApple(selligentResponse: selligentProduct)
            }
            .observeOn(dependencies.mainScheduler)
            .subscribe { event in
                switch event {
                case .next(let abaPlans):
                    self.view?.renderPlans(abaPlans)
                case .error:
                    self.view?.renderError()
                case .completed:
                    break
                }
                
            }.addDisposableTo(disposableBag)
    }
}
