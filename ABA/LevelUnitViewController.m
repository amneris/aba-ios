//
//  MainViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 28/10/14.
//  Copyright (c)2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "LevelUnitViewController.h"
#import "DataManager.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "UnitCell.h"
#import "LevelUnitCell.h"
#import "CertUnitCell.h"
#import "UnitRoscoViewController.h"
#import "UIViewController+ABA.h"
#import "ABARealmLevel.h"
#import "ABARealmCert.h"
#import "ABAUnit.h"
#import "ABARealmUser.h"
#import "Resources.h"
#import <QuartzCore/CALayer.h>
#import "AppDelegate.h"
#import "ImageHelper.h"
#import <AVFoundation/AVFoundation.h>

#import "ABA-Swift.h"

// Tracking
#import "MixpanelTrackingManager.h"
#import "LegacyTrackingManager.h"
#import <Crashlytics/Crashlytics.h>

@interface LevelUnitViewController ()

@property (weak, nonatomic)IBOutlet UICollectionView *collectionView;

@property RLMResults *levels;                      //Levels datasource
@property ABARealmLevel * expandedLevel;
@property BOOL firstTimePresentedVC;

@property (nonatomic, strong) ABAUnit *currentUnit;

@property (nonatomic, strong) FirebaseRemoteConfigManager * remoteConfig;

@end

@implementation LevelUnitViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self != nil)
    {
        self.remoteConfig = [[FirebaseRemoteConfigManager alloc] init];
        
        // Get the levels from fetchedResultController
        _levels = [LevelUnitController getAllLevelsDescending];
        
        // Other init
        ABARealmUser * user = [UserController currentUser];
        if (user.currentLevel != nil)
        {
            self.expandedLevel = user.currentLevel;
        }
    }
    
    return self;
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLevelFromNotification) name:kLevelProgressUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUnitProgressFromNotification) name:kUnitProgressUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUnitProgressFromNotification) name:kUpdateRotationFromMenu object:nil];
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted)
        {
            // Microphone enabled code
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *title = STRTRAD(@"noMicrophoneTitleKey", @"Atención");
                NSString *text = STRTRAD(@"noMicrophoneTextKey", @"Para el correcto funcionamiento de la aplicación debes permitir el acceso al micrófono");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:text preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                                      style:UIAlertActionStyleDefault
                                                                 handler:nil];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
    }];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *newRegister = [defaults objectForKey:newregister];
    
    if ([newRegister isEqualToString:@"1"])
    {
        [defaults setObject:@"0" forKey:newregister];
        [defaults synchronize];
        
        ABARealmUser * user = [UserController currentUser];
        ABAUnit * unit = [self getCurrentUnitFromLevel:user.currentLevel];
        
        if ([self.remoteConfig shouldShowListAtUnitDetail]) {
            UnitListViewController *unitDetail = [UnitDetailConfigurator createUnitDetailViperStackWithUnitID:unit.idUnit userId:user.idUser isPremium:[UserController isPremium:user] isNewUser:true];
            [self.navigationController pushViewController:unitDetail animated:YES];
        } else {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Unit" bundle:nil];
            UnitRoscoViewController *unitVC = [storyboard instantiateViewControllerWithIdentifier:@"UnitViewController"];
            [unitVC setUnit:unit];
            unitVC.newRegister = YES;
            [self.navigationController pushViewController:unitVC animated:NO];
        }
        
        // Tracking entered unit
        [CoolaDataTrackingManager trackEnteredUnit:user.idUser levelId:unit.level.idLevel unitId:unit.idUnitString];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLevelProgressUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUnitProgressUpdatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateRotationFromMenu object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[MixpanelTrackingManager sharedManager] showMixpanelNotificationsAndSurveys];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"unitlist"];
    
    if (!_firstTimePresentedVC)
    {
        _firstTimePresentedVC = YES;
        
        if ([UserController currentUser].currentLevel != nil)
        {
            [self updateAndScrollToLevel:[UserController currentUser].currentLevel animated:NO expandIfNeeded:NO];
        }
    }
    
    // Get the progress for current level
   ABARealmLevel *currentLevel = [UserController currentUser].currentLevel;
    
    [self updateLevelProgressTitle:currentLevel];
    _currentUnit = [UserController returnCurrentUnit];
    
    [self checkAndLoadUnitDeepLink];
    
    [self.collectionView reloadData];
}

- (void)checkAndLoadUnitDeepLink
{
    NSString *unitDeepLink = [[NSUserDefaults standardUserDefaults] objectForKey:kDeepLinkUnit];
    if (unitDeepLink)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDeepLinkUnit];
        [[NSUserDefaults standardUserDefaults] synchronize];

        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *unitIDNumber = [formatter numberFromString:unitDeepLink];

        if (unitIDNumber)
        {
            ABAUnit *unit = [LevelUnitController getUnitWithID:unitIDNumber];
            ABARealmUser * user = [UserController currentUser];

            if ([self.remoteConfig shouldShowListAtUnitDetail]) {
                UnitListViewController *unitDetail = [UnitDetailConfigurator createUnitDetailViperStackWithUnitID:unit.idUnit userId:user.idUser isPremium:[UserController isPremium:user] isNewUser:false];
                [self.navigationController pushViewController:unitDetail animated:YES];
            } else {
                UnitRoscoViewController *unitViewController = [[UIStoryboard storyboardWithName:@"Unit" bundle:nil] instantiateInitialViewController];
                unitViewController.unit = unit;
                
                [self.navigationController pushViewController:unitViewController animated:YES];
            }
           
            // Tracking entered unit
            [CoolaDataTrackingManager trackEnteredUnit:user.idUser levelId:unit.level.idLevel unitId:unit.idUnitString];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addNavigationBarDropShadow];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeNavigationBarDropShadow];
}

- (NSIndexPath *)getIndexPathFromLevel:(ABARealmLevel *)level
{
    NSInteger section = [_levels indexOfObject:level];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
    return indexPath;
}

#pragma mark - Public methods

- (void)continueButtonLoadUnit:(ABAUnit *)unit
{
    if ([self.remoteConfig shouldShowListAtUnitDetail]) {
        
        ABARealmUser * user = [UserController currentUser];
        UnitListViewController *unitDetail = [UnitDetailConfigurator createUnitDetailViperStackWithUnitID:unit.idUnit userId:user.idUser isPremium:[UserController isPremium:user] isNewUser:false];
        [self.navigationController pushViewController:unitDetail animated:YES];
        
    } else {
        
        UnitRoscoViewController *unitViewController = [[UIStoryboard storyboardWithName:@"Unit" bundle:nil] instantiateInitialViewController];
        [unitViewController setUnit:unit];
        
        [[Crashlytics sharedInstance] setObjectValue:unit.idUnit forKey:@"Unit"];
        [self.navigationController pushViewController:unitViewController animated:YES];
    }
    
    // Tracking entered unit
    ABARealmUser * user = [UserController currentUser];
    [CoolaDataTrackingManager trackEnteredUnit:user.idUser levelId:unit.level.idLevel unitId:unit.idUnitString];
}

#pragma mark - Private methods (navigation bar changes)

- (void)addNavigationBarDropShadow
{
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.navigationController.navigationBar.layer.shadowOffset =  CGSizeMake(0,4);
    self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
    
    CGRect shadowPath = CGRectMake(self.navigationController.navigationBar.layer.bounds.origin.x - 10, self.navigationController.navigationBar.layer.bounds.size.height - 6, MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)+ 20, 5);
    self.navigationController.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
    self.navigationController.navigationBar.layer.shouldRasterize = YES;
}

- (void)removeNavigationBarDropShadow
{
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.0f;
}

#pragma mark - Private methods (Notifications)

- (void)updateLevelFromNotification
{
    ABARealmLevel *currentLevel = [UserController currentUser].currentLevel;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateLevelProgressTitle:currentLevel];
    });
}

- (void)reloadUnitProgressFromNotification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

#pragma mark - Private methods

- (void)updateLevelProgressTitle:(ABARealmLevel *)level
{
    [self addMenuButtonWithTitleWhite:[NSString stringWithFormat:@"%@ %@", level.name, [level getPercentage]]];
}

- (void)updateAndScrollToLevel:(ABARealmLevel *)ABARealmLevel animated:(BOOL)animated expandIfNeeded:(BOOL)expandIfNeeded
{
    if (ABARealmLevel == nil)
    {
        return;
    }
    
    ABAUnit *currentUnit = [self getCurrentUnitFromLevel:ABARealmLevel];
    
    NSInteger section = [_levels indexOfObject:currentUnit.level];
    NSInteger unitId = [currentUnit getIndex];
    NSInteger item;
    
    if (expandIfNeeded)
    {
        if (![self isLevelExpanded:currentUnit.level])
        {
            self.expandedLevel = currentUnit.level;
        }
        
        item = [currentUnit.level.units count] - unitId + 1;
    }
    else
    {
        if ([self isLevelExpanded:currentUnit.level])
        {
            item = [currentUnit.level.units count] - unitId + 1;
        }
        else
        {
            item = 0;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:section];
	
	// Check if section and item are a valid value within the expected ranges and index path was properly created
	if ((section >= 0 && section < [_collectionView numberOfSections]) && (item > 0 && item < [_collectionView numberOfItemsInSection:section]) && indexPath != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{

            [_collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:animated];
        });
	}
}

- (ABAUnit *)getCurrentUnitFromLevel:(ABARealmLevel *)ABARealmLevel
{
    return [LevelUnitController getCurrentUnitForLevel:ABARealmLevel];
}

- (NSArray *)getAllIndexPathsArrayForABARealmLevel:(ABARealmLevel *)ABARealmLevel
{
    NSMutableArray *indexPathsArray = [[NSMutableArray alloc] init];
    
    NSInteger section = [[self getIndexPathFromLevel:ABARealmLevel] section];
    
    // We add 2 cause we have the certificate cell & the level cell
    NSInteger cellsToDelete = [ABARealmLevel.units count] + 2;
    
    for(int i = 0; i < cellsToDelete; i++)
    {
        [indexPathsArray addObject:[NSIndexPath indexPathForItem:i inSection:section]];
    }
    
    return indexPathsArray;
}
- (ABARealmLevel *)getLevelFromIndexPath:(NSIndexPath *)indexPath
{
    return [_levels objectAtIndex:indexPath.section];
}

- (ABAUnit *)getUnitFromIndexPath:(NSIndexPath *)indexPath
{
    ABARealmLevel *level = [self getLevelFromIndexPath:indexPath];
    
    RLMResults *units = [LevelUnitController getUnitsDescendingForLevel:level];
    
    // The first row is the cert, so we need to substract one from indexpath
    return [units objectAtIndex:indexPath.item - 1];
}

- (NSInteger)getNumberOfItemsToShowForLevel:(ABARealmLevel *)level
{
    if ([self isLevelExpanded:level])
    {
        // We return 2 rows more, because we have to show Certificate cell and Level cell.
        return [level.units count] + 2;
    }
    else
    {
        // Only the level cell
        return 1;
    }
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    NSInteger itemsForLevel = 0;
    if (view == _collectionView)
    {
        ABARealmLevel *level = [_levels objectAtIndex:section];
        
        if ([self isLevelExpanded:level])
        {
            return [level.units count] + 2;
        }
        
        return 1;
    }
    
    return itemsForLevel;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == _collectionView)
    {
        // These are the total amount of difficulty levels
        return [_levels count];
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (cv == _collectionView)
    {
        // We ask to the UnitListController the item to be displayed for current section and item
        id object = [self getObjectForCollectionViewWithIndexPath:indexPath];
        
        // Check the object class and return configured cell
        if ([object isKindOfClass:[ABARealmLevel class]])
        {
            LevelUnitCell *levelCell = [cv dequeueReusableCellWithReuseIdentifier:IS_IPAD ? @"LevelUnitCellId_iPad" : @"LevelUnitCellId" forIndexPath:indexPath];
            levelCell.delegate = self;
            
            [levelCell setupWithABARealmLevel:(ABARealmLevel *)object];
            
            levelCell.accessibilityLabel = [NSString stringWithFormat:@"levelCell%@", ((ABARealmLevel *)object).idLevel];
            
            [levelCell layoutSubviews];
            
            return levelCell;
        }
        else if ([object isKindOfClass:[ABAUnit class]])
        {
            ABAUnit *unit                   = (ABAUnit *)object;
            BOOL lastCompletedUnit          = [LevelUnitController isLastCompleted:unit forLevel:unit.level];
            
            if ([unit isEqualToObject:[self getCurrentUnitFromLevel:unit.level]])
            {
                UnitCell *unitCell = [cv dequeueReusableCellWithReuseIdentifier:IS_IPAD ? @"CurrentUnitCellId_iPad" : @"CurrentUnitCellId" forIndexPath:indexPath];
                unitCell.parentVC = self;
                [unitCell setupUnitWithABAUnit:unit currentUnit:[self getCurrentUnitFromLevel:unit.level] isLastUnitCompletedForLevel:lastCompletedUnit];
                unitCell.accessibilityLabel = [NSString stringWithFormat:@"unitCell%@", unit.idUnit];
                [unitCell layoutSubviews];
                
                return unitCell;
            }
            else if ([unit getIndex] == 1 && ![[self getCurrentUnitFromLevel:unit.level].level.idLevel isEqualToString:unit.level.idLevel])
            {
                UnitCell *unitCell = [cv dequeueReusableCellWithReuseIdentifier:IS_IPAD ? @"CurrentUnitCellId_iPad" : @"CurrentUnitCellId" forIndexPath:indexPath];
                [unitCell setupUnitWithABAUnit:unit currentUnit:[self getCurrentUnitFromLevel:unit.level] isLastUnitCompletedForLevel:lastCompletedUnit];
                unitCell.accessibilityLabel = [NSString stringWithFormat:@"unitCell%@", unit.idUnit];
                [unitCell layoutSubviews];
                
                return unitCell;
            }
            else
            {
                UnitCell *unitCell = [cv dequeueReusableCellWithReuseIdentifier:IS_IPAD ? @"UnitCellId_iPad" : @"UnitCellId" forIndexPath:indexPath];
                [unitCell setupUnitWithABAUnit:unit currentUnit:[self getCurrentUnitFromLevel:unit.level] isLastUnitCompletedForLevel:lastCompletedUnit];
                unitCell.accessibilityLabel = [NSString stringWithFormat:@"unitCell%@", unit.idUnit];
                [unitCell layoutSubviews];
                
                return unitCell;
            }
        }
        else if ([object isKindOfClass:[ABARealmCert class]])
        {
            CertUnitCell *certCell = [cv dequeueReusableCellWithReuseIdentifier:IS_IPAD ? @"CertUnitCellId_iPad" : @"CertUnitCellId" forIndexPath:indexPath];
            [certCell setupWithABARealmCertificate:(ABARealmCert *)object];
            [certCell layoutSubviews];
            
            return certCell;
        }
    }
    
    return nil;            // Unexpected object
}

- (kABADashboardRowType)getObjectTypeFromIndexPath:(NSIndexPath *)indexPath
{
    ABARealmLevel *level = [self getLevelFromIndexPath:indexPath];
    
    if ([self isLevelExpanded:level])
    {
        if (indexPath.item == 0)
        {
            return kCertRowType;
        }
        else if (indexPath.item >= 1 && indexPath.item <= [level.units count])
        {
            return kUnitRowType;
        }
        else
        {
            return kLevelRowType;
        }
    }
    else
    {
        return kLevelRowType;
    }
    
}

- (id)getObjectForCollectionViewWithIndexPath:(NSIndexPath *)indexPath
{
    switch ([self getObjectTypeFromIndexPath:indexPath])
    {
        case kLevelRowType: {
            
            ABARealmLevel *level = [self getLevelFromIndexPath:indexPath];
            return (id)level;
        }
        case kCertRowType: {
            
            ABARealmLevel *level = [self getLevelFromIndexPath:indexPath];
            return (id)level.cert;
        }
        case kUnitRowType: {
            
            ABAUnit *unit = [self getUnitFromIndexPath:indexPath];
            return (id)unit;
        }
        default: {
            
            ABARealmLevel *level = [self getLevelFromIndexPath:indexPath];
            return (id)level;
        }
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([self getObjectTypeFromIndexPath:indexPath])
    {
        case kUnitRowType:
        {
            ABAUnit *unit = [self getUnitFromIndexPath:indexPath];
            ABARealmUser * user = [UserController currentUser];

            if ([self.remoteConfig shouldShowListAtUnitDetail])
            {
                UnitListViewController *unitDetail = [UnitDetailConfigurator createUnitDetailViperStackWithUnitID:unit.idUnit userId:user.idUser isPremium:[UserController isPremium:user] isNewUser:false];
                [self.navigationController pushViewController:unitDetail animated:YES];
            }
            else
            {
                UnitRoscoViewController *unitViewController = [[UIStoryboard storyboardWithName:@"Unit" bundle:nil] instantiateInitialViewController];
                unitViewController.unit = unit;
                
                [[Crashlytics sharedInstance] setObjectValue:unit.idUnit forKey:@"Unit"];
                [self.navigationController pushViewController:unitViewController animated:YES];
            }
        
            // Tracking entered unit
            [CoolaDataTrackingManager trackEnteredUnit:user.idUser levelId:unit.level.idLevel unitId:unit.idUnitString];
            
            break;
        }
        case kLevelRowType: {
            
            ABARealmLevel *level = [self getLevelFromIndexPath:indexPath];
            BOOL isLevelExpanded = [self isLevelExpanded:level];
            
            if (isLevelExpanded)
            {
                [self collapseExpandedLevelWithBlock:nil];
            }
            else
            {
                [self expandButtonForLevel:level];
            }
            
            break;
        }
        default:
            break;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(self.view.bounds.size.width, [self returnHeightForIndexPath:indexPath]);
    
    // We set the full width for each cell
    return size;
}

- (CGFloat)returnHeightForIndexPath:(NSIndexPath *)indexPath
{
    NSObject *object = [self getObjectForCollectionViewWithIndexPath:indexPath];
    
    if ([object isKindOfClass:[ABARealmLevel class]])
    {
        return IS_IPAD ? 70.0f : 60.0f;
    }
    else if ([object isKindOfClass:[ABAUnit class]])
    {
        if ([((ABAUnit *)object)isEqualToObject:[self getCurrentUnitFromLevel:((ABAUnit *)object).level]] ||
            ([((ABAUnit *)object)getIndex] == 1 && ![[self getCurrentUnitFromLevel:((ABAUnit *)object).level].level.idLevel isEqualToString:((ABAUnit *)object).level.idLevel]))
        {
            return IS_IPAD ? 240.0f : 165.0f;
        }
        else
        {
            return IS_IPAD ? 185.0f : 90.0f;
        }
    }
    else if ([object isKindOfClass:[ABARealmCert class]])
    {
        return IS_IPAD ? 100.0f : 70.0f;
    }
    else
    {
        return IS_IPAD ? 100.0f : 70.0f;
    }
}

#pragma mark - Private methods (Cell expansion/collapsion) 

- (void)expandButtonForLevel:(ABARealmLevel *)ABARealmLevel
{
    SimpleBlock expandBlock = ^
    {
        if (!self.expandedLevel)
        {
            self.expandedLevel = ABARealmLevel;
            [_collectionView performBatchUpdates:^ {
                [_collectionView deleteItemsAtIndexPaths:@[[self getIndexPathFromLevel:ABARealmLevel]]];
                [_collectionView insertItemsAtIndexPaths:[self getAllIndexPathsArrayForABARealmLevel:ABARealmLevel]];
            }
            completion:^(BOOL finished) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self updateAndScrollToLevel:ABARealmLevel animated:YES expandIfNeeded:NO];
                });
            }];
        }
    };
    
    if (self.expandedLevel)
    {
        [self collapseExpandedLevelWithBlock:expandBlock];
    }
    else
    {
        expandBlock();
    }
}

- (void)collapseExpandedLevelWithBlock:(SimpleBlock)block
{
    if (self.expandedLevel)
    {
        ABARealmLevel * levelToCollapse = self.expandedLevel;
        self.expandedLevel = nil;
        [_collectionView performBatchUpdates:^{
            [_collectionView deleteItemsAtIndexPaths:[self getAllIndexPathsArrayForABARealmLevel:levelToCollapse]];
            [_collectionView insertItemsAtIndexPaths:@[[self getIndexPathFromLevel:levelToCollapse]]];
        }
        completion:^(BOOL finished) {
            if (block) {
                block();
            }
        }];
    }
}

#pragma mark - LevelUnitCellDelegate

- (BOOL)isLevelExpanded:(ABARealmLevel *)level
{
    return [self.expandedLevel.idLevel isEqualToString:level.idLevel];
}

@end
