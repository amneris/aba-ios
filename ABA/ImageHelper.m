//
//  ImageHelper.m
//  ABA
//
//  Created by Marc Güell Segarra on 13/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "ImageHelper.h"
#import "SDVersion.h"
#import "ABAUnit.h"
#import "ABARole.h"
#import <UIKit/UIKit.h>
#import "ABARealmLevel.h"
#import "LevelUnitController.h"
#import "APIManager.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "AFNetworkReachabilityManager.h"
#import "Utils.h"
#import "ABAError.h" 
#import "UserController.h"
#import "ABARealmUser.h"

@implementation ImageHelper

-(id)init {
	self = [super init];
	
	if(self != nil) {
		_isDownloading = NO;
	}
	
	return self;
}

+(NSString *)change3xIfNeededWithUrlString:(NSString *)urlString
{
    if(urlString == nil)
    {
        return @"";
    }
    else
    {
        urlString = [urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if([SDVersion deviceSize] == Screen5Dot5inch || IS_IPAD)
        {
            return [urlString stringByReplacingOccurrencesOfString:@"@2x"
                                                        withString:@"@3x"];
        }
        else
        {
            return urlString;
        }
    }
}

//-(void)loadImagesInBackground
//{
//	if(!_isDownloading) {
//		
//		_isDownloading = YES;
//		
//		if([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWiFi && [self mustPrefetch])
//		{
//			LevelUnitController *levelController = [[LevelUnitController alloc] init];
//			
//			NSArray *levels = [levelController getAllLevels];
//			
//			NSMutableArray *urlsArray = [[NSMutableArray alloc] init];
//			
//			for(ABARealmLevel *level in levels)
//			{
//				NSArray *array = [self getArrayURLforLevel:level];
//				
//				for(NSURL *url in array)
//				{
//					[urlsArray addObject:url];
//				}
//			}
//			
//			[self prefetchImagesWithURLs:urlsArray];
//		}
//	}
//}
//
-(void)loadImagesInBackgroundForLevel: (ABARealmLevel *)level
{
    [self prefetchImagesWithURLs:[self getArrayURLforLevel:level]];
}

-(void)loadImagesInBackgroundForUnit: (ABAUnit *)unit
{
	[self prefetchImagesWithURLs:[self getArrayURLforUnit:unit]];
}

-(void)downloadImagesForUnit: (ABAUnit *)unit withBlock:(ErrorBlock)block
{
    [self prefetchImagesWithURLs:[self getArrayURLforUnit:unit] withBlock:block];
}

//-(BOOL)mustPrefetch
//{
//	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//	NSDate *lastTimePrefetched = [defaults objectForKey:@"lastTimePrefetchedImages"];
//	
//	if(lastTimePrefetched == nil)
//	{
//		return YES;
//	}
//	else
//	{
//		if([self daysBetweenDate:[NSDate date] andDate:lastTimePrefetched] > 0)
//		{
//			return YES;
//		}
//		else
//		{
//			return NO;
//		}
//	}
//}
//
-(void)updatePrefetchedSuccessfully
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	[defaults setObject:[NSDate date] forKey:@"lastTimePrefetchedImages"];
	[defaults synchronize];
	
	_isDownloading = NO;
}

//-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
//{
//	NSDate *fromDate;
//	NSDate *toDate;
//	
//	NSCalendar *calendar = [NSCalendar currentCalendar];
//	
//	[calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
//				 interval:NULL forDate:fromDateTime];
//	[calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
//				 interval:NULL forDate:toDateTime];
//	
//	NSDateComponents *difference = [calendar components:NSDayCalendarUnit
//											   fromDate:fromDate toDate:toDate options:0];
//	
//	return [difference day];
//}
//
-(NSArray *)getArrayURLforUnit: (ABAUnit *)unit
{
    NSMutableArray *urlsArray = [[NSMutableArray alloc] init];
    
    [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:unit.filmImageUrl]]];
    [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:unit.filmImageInactiveUrl]]];
    [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:unit.videoClassImageUrl]]];
    [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:unit.unitImage]]];
    [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:unit.unitImageInactive]]];
    
    ABARealmUser *user = UserController.currentUser;
    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:user.teacherImage];
    NSURL *imageUrl = [NSURL URLWithString:urlString];
    [urlsArray addObject:imageUrl];

    for(ABARole *role in unit.roles)
    {
        [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:role.imageBigUrl]]];
        [urlsArray addObject:[NSURL URLWithString:[self.class change3xIfNeededWithUrlString:role.imageUrl]]];
    }
    
    return urlsArray;
}

-(NSArray *)getArrayURLforLevel: (ABARealmLevel *)level
{
    NSMutableArray *urlsArray = [[NSMutableArray alloc] init];

    for(ABAUnit *unit in level.units)
    {
        NSArray *array = [self getArrayURLforUnit:unit];
        
        for(NSURL *url in array)
        {
            [urlsArray addObject:url];
        }
    }
    
    return urlsArray;
}

-(void)prefetchImagesWithURLs:(NSArray *)array
{
	__weak typeof(self) weakSelf = self;

	[[SDWebImagePrefetcher sharedImagePrefetcher]
prefetchURLs:array progress:nil completed:^(NSUInteger noOfFinishedUrls, NSUInteger noOfSkippedUrls)
	{
		if(noOfFinishedUrls + noOfSkippedUrls == [array count])
		{
			[weakSelf updatePrefetchedSuccessfully];
		}
	}];
}

-(void)prefetchImagesWithURLs:(NSArray *)array withBlock:(ErrorBlock)block
{
	[[SDWebImagePrefetcher sharedImagePrefetcher]
	 prefetchURLs:array progress:nil completed:^(NSUInteger noOfFinishedUrls, NSUInteger noOfSkippedUrls)
	 {
		 if(noOfSkippedUrls == 0) {
			 block(nil);
		 }
		 else {
			 block([ABAError errorWithKey:@"Error downloading images"]);
		 }
	 }];
}

+ (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (void)getImageWithURL_old:(NSString *)urlString completionBlock:(CompletionBlock)completionBlock
{
    NSString *urlStringFormatted = [[self class] change3xIfNeededWithUrlString:urlString];
    
    if([self checkIfImageExists:urlStringFormatted])
    {
        completionBlock(nil,[UIImage imageWithContentsOfFile:[self getLocalImagePathFromURL:urlStringFormatted]]);
    }
    else
    {
        __weak typeof(self) weakSelf = self;

        [[APIManager sharedManager] downloadFile:[NSURL URLWithString:urlStringFormatted]
                                      toFileName:[self getLocalImagePathFromURL:urlStringFormatted]
                                 completionBlock:^(NSError *error, id object)
        {
            if(!error)
            {
                completionBlock(nil,[UIImage imageWithContentsOfFile:[weakSelf getLocalImagePathFromURL:urlStringFormatted]]);
            }
            else
            {
                completionBlock(error, nil);
            }
        }];
    }
}

+ (void)getImageWithURL:(NSString *)urlString completionBlock:(CompletionBlock)completionBlock
{
    NSString *urlStringFormatted = [[self class] change3xIfNeededWithUrlString:urlString];

    [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[NSURL URLWithString:urlStringFormatted]
                                                        options:0
                                                       progress:^(NSInteger receivedSize, NSInteger expectedSize)
     {
         // progression tracking code
     }
                                                      completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
     {
         if (image && finished)
         {
             completionBlock(nil, image);
         }
         else if(error)
         {
             completionBlock(error, image);
         }
     }];
    
}

+(NSString*) getLocalImagePathFromURL: (NSString*) urlString
{
    NSArray *parts = [urlString componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *folder = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"images"];

    BOOL isDir = YES;
    if(![[NSFileManager defaultManager] fileExistsAtPath:folder isDirectory:&isDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *path = [folder stringByAppendingPathComponent:filename];
    return path;
}

+(BOOL) checkIfImageExists: (NSString*) urlString
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self getLocalImagePathFromURL:urlString]];
}

@end
