//
//  UnitDetailViewInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation

protocol UnitDetailViewInput: NSObjectProtocol {
    
    // Generic loading dialog
    func showLoadingProgress()
    func hideLoadingProgress()
    
    // Alerts
    func showAlert(withText text: String)
    func prepareToShowCompletionAlert(type: kABASectionTypes)
    func showRateAppPopup()
    
    // Progress dialog
    func showProgressDialog()
    func updateDownloadProgressDialog(withProgress progress: Float)
    func dismissDownloadProgressDialog()
    
    // Refresh methods
    func refreshDataSource(model: UnitViewModel)
    func refreshTeacher()
    
}
