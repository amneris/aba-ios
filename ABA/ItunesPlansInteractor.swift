//
//  ItunesPlansInteractor.swift
//  ABA
//
//  Created by MBP13 Jesus on 15/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

enum ItunesPlansInteractorError: Swift.Error, CustomStringConvertible {
    var description: String { return "iTunes interactor error.." }
    // Thrown when there is an error from iTunes
    case iTunesRequestError
    case noPlansError
}

class ItunesPlansInteractor: NSObject {
    var catcher:ItunesResponseCatcher?
    
    override init() {
        super.init()
        self.catcher = ItunesResponseCatcher();
    }
}

extension ItunesPlansInteractor: ItunesPlansInteractorInput {
    
    func fetchInfoFromApple(selligentResponse: SelligentProducts) -> Observable<PlansPageProducts> {
        
        return fetch(selligentResponse: selligentResponse).map { skproductList -> PlansPageProducts in
            let abaplans = ABAPlanParser.parsePlans(skProducts: skproductList, selligentProducts: selligentResponse.products!)
            let viewmodel = PlansPageViewModel(text: selligentResponse.text!, products: abaplans)
            return viewmodel
        }
    }
}

internal extension ItunesPlansInteractor {
    
    func fetch(selligentResponse: SelligentProducts) -> Observable<[SKProduct]> {
        
        return Observable.create { observable -> Disposable in
            
            if let listOfProducts = selligentResponse.products, let _ = selligentResponse.text {
                
                let successBlock: ItunesResponseCatcherSuccessClosure = { skProductsResponse -> (Void) in
                    observable.onNext(skProductsResponse.products)
                    observable.onCompleted()
                }
                
                let errorBlock: ItunesResponseCatcherErrorClosure = { () -> (Void) in
                    observable.onError(ItunesPlansInteractorError.iTunesRequestError)
                }
                
                self.catcher = ItunesResponseCatcher();
                self.catcher?.successBlock = successBlock
                self.catcher?.failureBlock = errorBlock
                
                var setOfIdentifiers:Set<String> = Set(minimumCapacity: listOfProducts.count * 2)
                listOfProducts.forEach { item in
                    if let originalIdentifier = item.originalIdentifier {
                        setOfIdentifiers.insert(originalIdentifier)
                    }
                    if let discountIdentifier = item.discountIdentifier {
                        setOfIdentifiers.insert(discountIdentifier)
                    }
                }
                
                self.catcher?.start(setOfIdentifiers: setOfIdentifiers)
                
            } else {
                observable.onError(ItunesPlansInteractorError.noPlansError)
            }
            
            return Disposables.create()
        }
    }
}

// MARK: - SKProductsRequestDelegate

typealias ItunesResponseCatcherErrorClosure = () -> (Void)
typealias ItunesResponseCatcherSuccessClosure = (_: SKProductsResponse) -> (Void)

internal class ItunesResponseCatcher: NSObject {
    
    var successBlock: ItunesResponseCatcherSuccessClosure?
    var failureBlock: ItunesResponseCatcherErrorClosure?
    var itunesRequest: SKProductsRequest?
    
    func start(setOfIdentifiers : Set<String>) {
        itunesRequest = SKProductsRequest(productIdentifiers: setOfIdentifiers)
        itunesRequest?.delegate = self
        itunesRequest?.start()
    }
    
    func stop() {
        itunesRequest?.cancel()
    }
}

extension ItunesResponseCatcher: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse){
        self.successBlock?(response)
    }
    
    public func requestDidFinish(_ request: SKRequest) {
        // Do nothing
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        self.failureBlock?()
    }
}
