//
//  UnitDetailInteractorInput.swift
//  ABA
//
//  Created by MBP13 Jesus on 28/02/17.
//  Copyright © 2017 ABA English. All rights reserved.
//

import Foundation
import RxSwift

protocol UnitDetailInteractorInput {
    func getSections(unitId: NSNumber) -> Observable<Void>
    func getProgress(unitId: NSNumber) -> Observable<Void>
    func downloadUnit(unitId: NSNumber) -> Observable<Float>
}
