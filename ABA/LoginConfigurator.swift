//
//  LoginConfigurator
//  ABA
//
//  Created by Xabier Losada on 14/06/16.
//  Copyright (c) 2016 ABA English. All rights reserved.
//

import Foundation

@objc
class LoginConfigurator: NSObject {
    class func createLoginViperStack() -> LoginFormViewController {
        let loginVC = LoginFormViewController()
        configure(loginVC)
        return loginVC
    }
    
    class func configureModuleForViewInput(_ viewInput: UIViewController) {

        if let viewController = viewInput as? LoginFormViewController {
            configure(viewController)
        }
    }
}

extension LoginConfigurator {
    
    fileprivate class func configure(_ viewController: LoginFormViewController) {
        let repository = LoginRepository()
        let presenter = LoginPresenter(tracker: Tracker.sharedInstance)
        let interactor = LoginInteractor(repository: repository)
        let router = SessionRouter(vc: viewController)

        presenter.view = viewController
        presenter.interactor = interactor
        presenter.facebookInteractor = FacebookInteractor(tracker: LegacyTrackingManager.shared())
        presenter.router = router
        viewController.presenter = presenter
    }
}
