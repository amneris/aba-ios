//
//  ABAInterpretPauseView.h
//  ABA
//
//  Created by Marc Güell Segarra on 28/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABAInterpretPauseView;

@protocol ABAInterpretPauseViewDelegate <NSObject>

-(void)continuePauseViewAction;
-(void)restartPauseViewAction;

@end

@interface ABAInterpretPauseView : UIView

@property (nonatomic, weak) id <ABAInterpretPauseViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *pausedInterpetLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *restartButton;


@end
