//
//  SubscriptionController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Blocks.h"


#define ABAInAppSharedSecretKey @"b7d5f16ee3b94964ae363b2785a7694d"

#define kSubscriptionProcessCompletedOK @"SubscriptionProcessCompletedOK"
#define kSubscriptionProcessCompletedFailed @"kSubscriptionProcessCompletedFailed"

@class ABAPlan;

@interface SubscriptionController : NSObject

+ (void)configureCargoWithListener:(void (^)(SubscriptionStatus status))listener;

/**
 *  Recupera los productos de la API de ABA, y seguidamente los consulta a partir de StoreKit
 *
 *  @param completionBlock Block con Array de ABAPlan o NSError si ha ocurrido algun fallo
 */
+ (void)getSubscriptionProducts:(CompletionBlock)completionBlock;

/**
 *  Envia una solicitud de transacción con el SKProduct que haya asignado al ABAPlan
 *
 *  @param plan al que se quieren suscribir
 */
+ (void)subscribeToPlan:(ABAPlan*) plan;

/**
 * Restablece las purchases del usuario en caso que tenga, y envia a la API el updateuserpremium en caso que el receipt sea válido
 */
+ (void)restorePurchases;


@end
