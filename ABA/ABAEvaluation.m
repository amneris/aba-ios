//
//  ABAEvaluation.m
//  ABA
//
//  Created by Ivan Ferrera on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAEvaluation.h"
#import "ABAEvaluationQuestion.h"
#import "ABAUnit.h"


@implementation ABAEvaluation

-(BOOL)hasContent
{
    if (self.content)
    {
        return YES;
    }
    return NO;
}

- (ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionEvaluation == %@", self];
    RLMResults *results = [ABAUnit objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}

@end
