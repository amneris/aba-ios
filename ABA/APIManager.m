//
//  APIManager.m
//  ABA
//
//  Created by Oriol Vilaró on 7/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#include <sys/utsname.h>

#import "APIManager.h"

// Model
#import "ABARealmProgressAction.h"
#import "ABAProgressOperationQueue.h"
#import "ABARealmUser.h"
#import "ABASection+Methods.h"
#import "ABAUnit.h"
#import "ABAEvaluation.h"
#import "ABAEvaluationOption.h"
#import "ABAPlan.h"

// Controllers
#import "DataController.h"
#import "UserController.h"
#import "UserDataController.h"
#import "LanguageController.h"

// Shepherd
#import "ABACoreShepherdEnvironment.h"
#import "ABACoreShepherdConfiguration.h"
#import "ABAShepherdEditor.h"

// Others
#import "ABAPlanResponseSerializer.h"
#import "MyAdditions.h"
#import "ABAError.h"
#import "Resources.h"
#import "Utils.h"
#import "ABA-Swift.h"

#define ABA_SECURE_API_URL(method) [NSString stringWithFormat:@"%@%@", ((ABACoreShepherdEnvironment *)[ABAShepherdEditor.shared environmentForShepherdConfigurationClass:[ABACoreShepherdConfiguration class]]).abaSecureApiUrl, method]

#define DEFAULT_TIMEOUT 30.0
#define PROGRESS_TIMEOUT 20.0
#define SHORT_TIMEOUT 10.0

@interface APIManager()

@property (nonatomic, strong) NSTimer *progressTimer;

@end

@implementation APIManager

#pragma mark Singleton Methods

+ (APIManager *)sharedManager; {
    static APIManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
        _fileManager = [[AFHTTPRequestOperationManager alloc] init];
        _fileManager.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];

        _progressManager = [AFHTTPRequestOperationManager new];
        _progressManager.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
        [_progressManager.requestSerializer setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
        [_progressManager.requestSerializer setTimeoutInterval:PROGRESS_TIMEOUT];
        
        _progressQueue = [ABAProgressOperationQueue new];
        _progressManager.operationQueue = _progressQueue;

        _manager = [[AFHTTPRequestOperationManager alloc] init];
        _manager.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
        [_manager.requestSerializer setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];

        NSOperationQueue *operationQueue = _manager.operationQueue;
        [_manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status)
            {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                [operationQueue setSuspended:YES];
                break;
            }
        }];

        [_manager.reachabilityManager startMonitoring];

        [self getPublicIP:^(NSError *error, id object){

        }];

    }
    return self;
}

#pragma mark - Getters

-(AFNetworkReachabilityStatus)getReachabilityStatus{
    
    AFNetworkReachabilityManager *manager = [_manager reachabilityManager];
    
    return manager.networkReachabilityStatus;
}

#pragma mark - Helpers

- (BOOL)conectionLost {
    
    switch ([self getReachabilityStatus])
    {
        case AFNetworkReachabilityStatusReachableViaWWAN:
        case AFNetworkReachabilityStatusReachableViaWiFi:
            return NO;
            break;
        case AFNetworkReachabilityStatusNotReachable:
            return YES;
            break;
        default:
            return NO;
            break;
    }

}

-(NSString*) signatureFromParametersConcatenated:(NSString*)parametersConcatenated{
    
    NSString *fixedData = @"Mobile Outsource Aba AgnKey";
    NSString *signature= [NSString stringWithFormat:@"%@%@", fixedData, parametersConcatenated];
    NSString *signatureMD5 = [signature md5];

    return signatureMD5;
}

-(NSString*) devicePlatform
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    return platform;
}

- (NSSet *)acceptableContentTypes
{
    return [NSSet setWithObjects:@"text/plain", @"application/json", nil];
}

#pragma mark - API methods

- (void)getPublicIP:(CompletionBlock)completionBlock
{
    __weak typeof(self) weakSelf = self;
    [_manager GET:ABA_SECURE_API_URL(@"api/abaEnglishApi/iprecognition") parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *ip = [responseObject objectForKey:@"publicIp"];
        if (ip && [ip isKindOfClass:[NSString class]] && ![ip isEqualToString:@""])
        {
            weakSelf.publicIp = ip;
            completionBlock(nil, ip);
        }
        else
        {
            weakSelf.publicIp = nil;
            completionBlock([ABAError errorWithKey:STRTRAD(@"errorNoPublicIP", @"error cuando no hay ip publica")], nil);
        }

    }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {

            weakSelf.publicIp = nil;
            completionBlock([ABAError errorWithKey:STRTRAD(@"errorNoPublicIP", @"error cuando no hay ip publica")], nil);
        }];
}

- (void)postRegisterWithFacebookID:(NSString *)facebookID email:(NSString *)email name:(NSString *)name surnames:(NSString *)surnames gender:(NSString *)gender avatar:(NSString*)avatar completionBlock:(CompletionBlock)completionBlock
{
    if ([self conectionLost])
    {
        completionBlock([ABAError errorConnection], nil);
        return;
    }
    
    [self getPublicIP:^(NSError *error, id response) {
		
        NSMutableDictionary *parametersDict = @{}.mutableCopy;
        NSMutableString *parametersConcatenated = @"".mutableCopy;
        
        // email
        [parametersDict setObject:email forKey:@"email"];
        [parametersConcatenated appendString:email];
        
        // name
        if (name && ![name isKindOfClass:[NSNull class]] )
        {
            [parametersDict setObject:name forKey:@"name"];
            [parametersConcatenated appendString:name];
        }
        else
        {
            [parametersDict setObject:@"" forKey:@"name"];
            [parametersConcatenated appendString:@""];
        }
        
        // surnames
        if (surnames && ![surnames isKindOfClass:[NSNull class]] )
        {
            [parametersDict setObject:surnames forKey:@"surnames"];
            [parametersConcatenated appendString:surnames];
        }
        else
        {
            [parametersDict setObject:@"" forKey:@"surnames"];
            [parametersConcatenated appendString:@""];
        }
        
        // facebookId
        [parametersDict setObject:facebookID forKey:@"fbId"];
        [parametersConcatenated appendString:facebookID];
        
        // gender
		if (gender != nil && [gender length] > 0)
        {
			[parametersDict setObject:gender forKey:@"gender"];
			[parametersConcatenated appendString:gender];
		}
		
        // langEnv
        NSString * langEnv = [LanguageController getCurrentLanguage];
        [parametersDict setObject:langEnv forKey:@"langEnv"];
        [parametersConcatenated appendString:langEnv];
        
        // deviceId
        NSString *deviceId = [UIDevice currentDevice].identifierForVendor.UUIDString ? [UIDevice currentDevice].identifierForVendor.UUIDString : @"";
        [parametersDict setObject:deviceId forKey:@"deviceId"];
        [parametersConcatenated appendString:deviceId];
        
        // sysOper
        NSString *sysOper = [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemVersion]];
        [parametersDict setObject:sysOper forKey:@"sysOper"];
        [parametersConcatenated appendString:sysOper];
        
        // deviceName
        NSString *deviceName;
        if (IS_IPAD)
        {
            deviceName=@"iPad";
        }
        else
        {
            deviceName=@"iPhone";
        }
        
        [parametersDict setObject:deviceName forKey:@"deviceName"];
        [parametersConcatenated appendString:deviceName];

        // ipMobile: Connection Ip from the device.
        if (!error)
        {
            NSString *ipMobile = response;
            [parametersDict setObject:ipMobile forKey:@"ipMobile"];
            [parametersConcatenated appendString:ipMobile];
        }
        
        // idPartner, integer, partner associated
        if (IS_IPAD)
        {
            [parametersDict setObject:partneridIpad forKey:@"idPartner"];
            [parametersConcatenated appendString:partneridIpad];
        }
        else
        {
            [parametersDict setObject:partneridIphone forKey:@"idPartner"];
            [parametersConcatenated appendString:partneridIphone];
        }
        
        // idSourceList, integer, campaign or ad marketing action associated.
        [parametersDict setObject:@"0" forKey:@"idSourceList"];
        [parametersConcatenated appendString:@"0"];
        
        // deviceTypeSource, string, could be c(computer) / m(mobile) / t(tablet)
        NSString *deviceTypeSource;
        if (IS_IPAD)
        {
            deviceTypeSource=@"t";
        }
        else
        {
            deviceTypeSource=@"m";
        }
        
        [parametersDict setObject:deviceTypeSource forKey:@"deviceTypeSource"];
        [parametersConcatenated appendString:deviceTypeSource];
        
        // signature
        [parametersDict setObject:[self signatureFromParametersConcatenated:parametersConcatenated.copy] forKey:@"signature"];
        
        [_manager POST:ABA_SECURE_API_URL(@"/register/registerfacebook") parameters:parametersDict.copy success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary *)responseObject];
            [responseDictionary setValue:avatar forKey:@"avatar"];
            
            completionBlock(nil, responseDictionary);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            completionBlock([ABAError errorWithKey:STRTRAD(@"errorRegisterFacebook", @"error en el registro con facebook")], nil);
            
        }];
        
    }];
    
}

- (void)postRegisterWithEmail:(NSString *)email password:(NSString *)password name:(NSString *)name completionBlock:(CompletionBlock)completionBlock
{

    if ([self conectionLost])
    {

        completionBlock([ABAError errorConnection], nil);

        return;
    }

    [self getPublicIP:^(NSError *error, id response) {

        NSMutableDictionary *parametersDict = @{}.mutableCopy;
        NSMutableString *parametersConcatenated = @"".mutableCopy;

        // deviceId,

        NSString *deviceId = [UIDevice currentDevice].identifierForVendor.UUIDString;

        [parametersDict setObject:deviceId forKey:@"deviceId"];
        [parametersConcatenated appendString:deviceId];

        // sysOper,

        NSString *sysOper = [NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemVersion]];

        [parametersDict setObject:sysOper forKey:@"sysOper"];
        [parametersConcatenated appendString:sysOper];

        // deviceName,

        NSString *deviceName;
        if (IS_IPAD)
        {
            deviceName = @"iPad";
        }
        else
        {
            deviceName = @"iPhone";
        }

        [parametersDict setObject:deviceName forKey:@"deviceName"];
        [parametersConcatenated appendString:deviceName];

        // email,

        [parametersDict setObject:email forKey:@"email"];
        [parametersConcatenated appendString:email];

        // password

        [parametersDict setObject:password forKey:@"password"];
        [parametersConcatenated appendString:password];

        // langEnv

        NSString *langEnv = [LanguageController getCurrentLanguage];
        [parametersDict setObject:langEnv forKey:@"langEnv"];
        [parametersConcatenated appendString:langEnv];

        // name

        [parametersDict setObject:name forKey:@"name"];
        [parametersConcatenated appendString:name];

        // surnames

        [parametersDict setObject:@"" forKey:@"surnames"];
        [parametersConcatenated appendString:@""];

        // ipMobile: Connection Ip from the device.
        if (!error)
        {
            NSString *ipMobile = response;

            [parametersDict setObject:ipMobile forKey:@"ipMobile"];
            [parametersConcatenated appendString:ipMobile];
        }

        // idPartner, integer, partner associated.

        if (IS_IPAD)
        {
            [parametersDict setObject:partneridIpad forKey:@"idPartner"];
            [parametersConcatenated appendString:partneridIpad];
        }
        else
        {
            [parametersDict setObject:partneridIphone forKey:@"idPartner"];
            [parametersConcatenated appendString:partneridIphone];
        }

        // idSourceList, integer, campaign or ad marketing action associated.

        [parametersDict setObject:@"0" forKey:@"idSourceList"];
        [parametersConcatenated appendString:@"0"];

        // deviceTypeSource, string, could be c  (computer)/ m (mobile)/ t (tablet)

        NSString *deviceTypeSource;
        if (IS_IPAD)
        {
            deviceTypeSource = @"t";
        }
        else
        {
            deviceTypeSource = @"m";
        }

        [parametersDict setObject:deviceTypeSource forKey:@"deviceTypeSource"];
        [parametersConcatenated appendString:deviceTypeSource];

        // appVersion,

        NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        [parametersDict setObject:appVersion forKey:@"appVersion"];
        [parametersConcatenated appendString:appVersion];

        // signature

        [parametersDict setObject:[self signatureFromParametersConcatenated:parametersConcatenated.copy] forKey:@"signature"];

        [_manager POST:ABA_SECURE_API_URL(@"/register/register")
            parameters:parametersDict.copy
            success:^(AFHTTPRequestOperation *operation, id responseObject) {

                NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary *)responseObject];

                [responseDictionary setValue:parametersDict[@"name"] forKey:@"name"];

                completionBlock(nil, responseDictionary);

            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {

                if (operation.response.statusCode == CODE_EMAIL_EXIST_ERROR)
                {
                    completionBlock([ABAError errorMailExists], nil);
                }
                else
                {
                    completionBlock([ABAError errorWithKey:STRTRAD(@"errorRegister", @"error en el registro")], nil);
                }
            }];
    }];
}

- (void)postLoginWithEmail:(NSString *)email password:(NSString *)password completionBlock:(CompletionBlock)completionBlock{
    
    if ([self conectionLost]) {
        
        completionBlock([ABAError errorConnection], nil);
        
        return;
    }

    NSMutableDictionary *parametersDict = @{}.mutableCopy;
    NSMutableString *parametersConcatenated = @"".mutableCopy;
    
    // deviceId,
    
    NSString *deviceId = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    [parametersDict setObject:deviceId forKey:@"deviceId"];
    [parametersConcatenated appendString:deviceId];
    
    // sysOper,
    
    NSString *sysOper=[NSString stringWithFormat:@"%@", [[UIDevice currentDevice] systemVersion]];
    
    [parametersDict setObject:sysOper forKey:@"sysOper"];
    [parametersConcatenated appendString:sysOper];

    // deviceName,
    
    NSString *deviceName;
    if (IS_IPAD){
        deviceName=@"iPad";
    }else{
        deviceName=@"iPhone";
    }
    
    [parametersDict setObject:deviceName forKey:@"deviceName"];
    [parametersConcatenated appendString:deviceName];
    
    // email,
    
    [parametersDict setObject:email forKey:@"email"];
    [parametersConcatenated appendString:email];
    
    // password
    
    [parametersDict setObject:password forKey:@"password"];
    [parametersConcatenated appendString:password];
    
    // appVersion,
    
    NSString *appVersion=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [parametersDict setObject:appVersion forKey:@"appVersion"];
    [parametersConcatenated appendString:appVersion];
    
    // signature
    
    [parametersDict setObject:[self signatureFromParametersConcatenated:parametersConcatenated.copy] forKey:@"signature"];
    
    
    [_manager POST:ABA_SECURE_API_URL(@"/api/apiuser/login") parameters:parametersDict.copy success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        completionBlock(nil, responseObject);
        
        NSLog(@"");
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completionBlock([ABAError errorWithKey:STRTRAD(@"errorLogin", @"error en el login")], nil);
        
    }];
}

- (void)postRecoverPassword:(NSString *)email completionBlock:(CompletionBlock)completionBlock{
    
    if ([self conectionLost]) {
        
        completionBlock([ABAError errorConnection], nil);
        
        return;
    }
    
    NSMutableDictionary *parametersDict = @{}.mutableCopy;
    NSMutableString *parametersConcatenated = @"".mutableCopy;
    
    // email,
    
    [parametersDict setObject:email forKey:@"email"];
    [parametersConcatenated appendString:email];
    
    // langEnv
    
    NSString * langEnv = [LanguageController getCurrentLanguage];
    [parametersDict setObject:langEnv forKey:@"langEnv"];
    [parametersConcatenated appendString:langEnv];
    
    // signature
    
    [parametersDict setObject:[self signatureFromParametersConcatenated:parametersConcatenated.copy] forKey:@"signature"];
    
    
    [_manager POST:ABA_SECURE_API_URL(@"/apiuser/recoverpassword") parameters:parametersDict.copy success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        completionBlock(nil, responseObject);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        completionBlock([ABAError errorWithKey:STRTRAD(@"errorRecoverPass", @"error en recuperar la contraseña")], nil);
        
    }];
}

- (void)updatePasswordUserWithToken:(NSString *)token password:(NSString *)password completionBlock:(CompletionBlock)completionBlock {
    
    if ([self conectionLost]) {
        
        completionBlock([ABAError errorConnection], nil);
        
        return;
    }
    
    NSMutableDictionary *parametersDict = @{}.mutableCopy;
    [parametersDict setObject:password forKey:@"password"];
    
    
    AFJSONRequestSerializer *responseSerializer = (AFJSONRequestSerializer *)_manager.requestSerializer;
    NSURLRequest *request = [responseSerializer requestWithMethod:@"POST" URLString:ABA_SECURE_API_URL(@"/apiuser/updateuserpassword") parameters:parametersDict.copy error:nil];
    
    NSMutableURLRequest *mutURLRequest = request.mutableCopy;
    
    [mutURLRequest setValue:token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        
        completionBlock(nil, [responseObject objectForKey:@"token"]);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        completionBlock([ABAError errorWithKey:STRTRAD(@"errorUpdatePass", @"error en el update del password")], nil);
    }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)updateUserLevel:(NSString *)idLevel idUser:(NSString *)idUser completionBlock:(CompletionBlock)completionBlock {
	
    if ([self conectionLost]) {
        
        completionBlock([ABAError errorConnection], nil);
        
        return;
    }
    
	AFJSONRequestSerializer *responseSerializer = (AFJSONRequestSerializer *)_manager.requestSerializer;

    NSString* apiURLString = [NSString stringWithFormat:@"/api/apiuser/updateuserlevel/%@/%@", idUser, idLevel];
    NSURLRequest *request = [responseSerializer requestWithMethod:@"GET" URLString:ABA_SECURE_API_URL(apiURLString) parameters:nil error:nil];

	NSMutableURLRequest *mutURLRequest = request.mutableCopy;

    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

	AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
	{
		if([responseObject[@"status"] isEqualToString:@"OK"])
		{
			completionBlock(nil, responseObject);
		}
		else
		{
			completionBlock([ABAError errorWithKey:STRTRAD(@"errorUserUpdate", @"error en el cambio de level")], nil);
		}
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		
		completionBlock([ABAError errorWithKey:STRTRAD(@"errorUserUpdate", @"error en el cambio de level")], nil);
	}];
	
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)getAllUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang completionBlock:(CompletionBlock)completionBlock
{
    if ([self conectionLost])
    {
        completionBlock([ABAError errorConnection], nil);
        return;
    }
    
    NSString *apiString = [NSString stringWithFormat:@"/api/%@/unit", userLang];
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ABA_SECURE_API_URL(apiString)]];
    
    [mutURLRequest setValue:token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        completionBlock(nil, responseObject);
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        completionBlock([ABAError errorWithKey:STRTRAD(@"errorGetAllUnits", @"error en recuperar todas las unidades")], nil);
    }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)getSectionContentWithLocale:(NSString *)locale andUnitID:(NSString *)unitID completionBlock:(CompletionBlock)block {
    
    if ([self conectionLost]) {
        
        block([ABAError errorConnection], nil);
        
        return;
    }
    
    NSAssert(_token != nil, @"Token can't be nil");
    
    NSString *apiString = [NSString stringWithFormat:@"/api/%@/content/%@", locale, unitID];
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ABA_SECURE_API_URL(apiString)]];
    
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
                                         {
                                             block(nil, responseObject);
                                         }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                         {
                                             block([ABAError errorWithKey:STRTRAD(@"errorGetSectionContent", @"error en recuperar el contenido de la sección")], nil);
                                             
                                         }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)getCurrencyConversionWithLocale:(NSString *)locale withProduct:(SKProduct *)product completionBlock:(CompletionBlock)block
{
    if ([self conectionLost])
    {
        block([ABAError errorConnection], nil);
        return;
    }

    NSAssert(_token != nil, @"Token can't be nil");

    NSString *countryCode = [product.priceLocale objectForKey:NSLocaleCurrencyCode];
    NSString *price = [NSString stringWithFormat:@"%@", product.price];
    NSString *apiString = [NSString stringWithFormat:@"/api/%@/currency/toeur/%@/%@", locale, price, countryCode];
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ABA_SECURE_API_URL(apiString)]];
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;
    
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
                                         {
                                             block(nil, responseObject);
                                         }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                         {
                                             block([ABAError errorWithKey:STRTRAD(@"errorGetSectionContent", @"error en recuperar el contenido de la sección")], nil);
                                             
                                         }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)getContentsOfURL:(NSString *)url completionBlock:(CompletionBlock)completionBlock{
    
    if ([self conectionLost]) {
        
        completionBlock([ABAError errorConnection], nil);
        
        return;
    }
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        completionBlock(nil, responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        completionBlock(error, nil);
    }];

    operation.responseSerializer = [AFCompoundResponseSerializer serializer];
    [_manager.operationQueue addOperation:operation];
}

- (void)downloadFile:(NSURL *)url toFileName:(NSString *)fileName completionBlock:(CompletionBlock)completionBlock
{
    if ([self conectionLost])
    {
        completionBlock([ABAError errorConnection], nil);
        return;
    }

    double currentTime = [[NSDate date] timeIntervalSince1970];
    NSString *randomString = [StringUtils randomStringWithLength:5];
    NSString *fileNameDownload = [fileName stringByAppendingFormat:@"-%f-%@.download", currentTime,randomString];

    NSMutableURLRequest *mutableURLRequest = [NSMutableURLRequest requestWithURL:url];
    mutableURLRequest.timeoutInterval = SHORT_TIMEOUT;
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:mutableURLRequest];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:fileNameDownload append:NO];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSError *error = nil;

        // Delete the file if previously existed
        if ([[NSFileManager defaultManager] fileExistsAtPath:fileName])
        {
            [[NSFileManager defaultManager] removeItemAtPath:fileName error:&error];
        }

        // Rename it with right file extension
        [[NSFileManager defaultManager] moveItemAtPath:fileNameDownload toPath:fileName error:&error];

        completionBlock(error, fileName);

    }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {

            // Delete the file if exists, because will be corrupted after download failure
            if ([[NSFileManager defaultManager] fileExistsAtPath:fileNameDownload])
            {
                [[NSFileManager defaultManager] removeItemAtPath:fileNameDownload error:&error];
            }

            completionBlock(error, nil);
        }];

    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_fileManager.operationQueue addOperation:operation];
}

- (void)getCourseProgressWithLocale:(NSString *)locale andUserID:(NSString *)userID completionBlock:(CompletionBlock)block {
    
    if ([self conectionLost]) {
        
        block([ABAError errorConnection], nil);
        
        return;
    }
    
    NSAssert(_token != nil, @"Token can't be nil");
    
    NSString *apiString = [NSString stringWithFormat:@"/api/%@/progress/coursesectionprogress/%@", locale, userID];
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ABA_SECURE_API_URL(apiString)]];
    
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
                                         {
                                             block(nil, responseObject);
                                         }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                         {
                                             block([ABAError errorWithKey:STRTRAD(@"errorGetSectionContent", @"error en recuperar el contenido de la sección")], nil);
                                         }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)getUnitProgressWithLocale:(NSString *)locale andUserID:(NSString *)userID andUnitID:(NSString *)unitID completionBlock:(CompletionBlock)block {
    
    if ([self conectionLost]) {
        
        block([ABAError errorConnection], nil);
        
        return;
    }
    
    NSAssert(_token != nil, @"Token can't be nil");
    
    NSString *apiString = [NSString stringWithFormat:@"/api/%@/progress/unitsectionprogress/%@/%@", locale, userID, unitID];
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ABA_SECURE_API_URL(apiString)]];
    
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
                                         {
                                             block(nil, responseObject);
                                         }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                         {
                                             block([ABAError errorWithKey:STRTRAD(@"errorGetSectionContent", @"error en recuperar el contenido de la sección")], nil);
                                         }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)getCompletedActionsFromUserID:(NSString *)userID andUnit:(ABAUnit *)unit withSectionID:(NSString *)sectionType completionBlock:(CompletionBlock)block {
    
    if ([self conectionLost]) {
        
        block([ABAError errorConnection], nil);
        
        return;
    }
    
    NSAssert(_token != nil, @"Token can't be nil");
    
    NSString *apiString = nil;
    NSString *lastChanged = [NSString stringWithFormat:@"%.0f", [unit.lastChanged timeIntervalSince1970]];
    
    if(sectionType != nil) {
        apiString = [NSString stringWithFormat:@"/api/%@/progress/sectionlistcompletedelements/%@/%@/%@/%@", [UserController getUserLanguage], userID, unit.idUnit, sectionType, lastChanged];
    } else {
       apiString = [NSString stringWithFormat:@"/api/%@/progress/unitlistcompletedelements/%@/%@/%@", [UserController getUserLanguage], userID, unit.idUnit, lastChanged];
    }
    
    NSMutableURLRequest *mutURLRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ABA_SECURE_API_URL(apiString)]];
    
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;

    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, id responseObject)
                                         {
                                             block(nil, responseObject);
                                         }
                                                                          failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                         {
                                             block([ABAError errorWithKey:STRTRAD(@"errorGetUserActions", @"error en recuperar las acciones del usuario")], nil);
                                         }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}

- (void)sendProgressActionsToServerWithActions:(NSArray *)actions completionBlock:(CompletionBlock)block
{
    if (_token == nil)
    {
        NSError *error = [NSError errorWithDomain:@"Invalid token" code:999 userInfo:nil];
        block(error, nil);
        return;
    }

    _progressManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [_progressManager.requestSerializer setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];

    NSString *apiURL = [NSString stringWithFormat:ABA_SECURE_API_URL(@"/api/%@/progress/register"), [UserController getUserLanguage]];
    [_progressManager POST:apiURL parameters:actions success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject)
        {
            block(nil, responseObject);
        }
        else
        {
            block(nil, nil);
        }
    }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            block(error, nil);
        }];
}

#pragma mark Subscription Methods

- (void)getSubscriptionProducts:(CompletionBlock)completionBlock
{
    if ([self conectionLost]) {
        
        completionBlock([ABAError errorConnection], nil);
        
        return;
    }
    
    NSLocale *locale = [NSLocale currentLocale];
    
    NSMutableDictionary *parametersDict = @{}.mutableCopy;
    
    NSLocale *fakeLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    // countryid=AppStore country
    [parametersDict setObject:[locale objectForKey:NSLocaleCountryCode]?:[fakeLocale objectForKey:NSLocaleCountryCode] forKey:@"countryid"];
    
    // lang=user lang
    [parametersDict setObject:[locale objectForKey:NSLocaleLanguageCode]?:[fakeLocale objectForKey:NSLocaleLanguageCode] forKey:@"lang"];
    
    // currency=AppStore currency
    [parametersDict setObject:[locale objectForKey:NSLocaleCurrencyCode]?:[fakeLocale objectForKey:NSLocaleCurrencyCode] forKey:@"currency"];

    [parametersDict setObject:@"productstiers" forKey:@"object"];
    
    NSString *userId = [UserController currentUser].idUser;
    NSString *urlPath = [@"api/GetProducts/ios/" stringByAppendingString:userId];
    
    AFJSONRequestSerializer *responseSerializer = (AFJSONRequestSerializer *)_manager.requestSerializer;
    NSURLRequest *request = [responseSerializer requestWithMethod:@"GET" URLString:ABA_SECURE_API_URL(urlPath) parameters:nil error:nil];

    NSMutableURLRequest *mutURLRequest = request.mutableCopy;
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;
    
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, NSArray *arrayOfABAPlan) {
        
        completionBlock(nil, arrayOfABAPlan);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString * errorResponse = [[NSString alloc] initWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding];
        NSLog(@"%@", errorResponse);
        completionBlock([ABAError errorWithKey:STRTRAD(@"errorGetProducts", @"Error al recuperar los productos de suscripción")], nil);
    }];
    
    operation.responseSerializer = [[ABAPlanResponseSerializer alloc] init];
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    
    [_manager.operationQueue addOperation:operation];
}

- (void)updateUserToPremiumWithProductPrice:(NSDecimalNumber*)price withCurrencyCode:(NSString*)currencyCode withCountryCode:(NSString*)countryCode withPediodDays:(NSInteger)days withUserId:(NSString *)userId withSessionId:(NSString *)idSession withReceipt:(NSDictionary *)receipt completionBlock:(ErrorBlock)block
{
    if ([self conectionLost])
    {
        block([ABAError errorConnection]);
        return;
    }
    
    NSMutableDictionary *parametersDict = @{}.mutableCopy;
    // object = freetopremium
    [parametersDict setObject:@"freetopremium" forKey:@"object"];
    // periodid=Subscription length in months
    [parametersDict setObject:[NSString stringWithFormat:@"%ld", (long)days / 30] forKey:@"periodId"];
    // currency=AppStore currency]
    [parametersDict setObject:currencyCode forKey:@"currency"];
    // price=AppStore price
    [parametersDict setObject:price forKey:@"price"];
    // countryid=AppStore country
    [parametersDict setObject:countryCode forKey:@"countryId"];
    // userid=userid
    [parametersDict setObject:userId forKey:@"userId"];
    // promocode
    [parametersDict setObject:@"" forKey:@"promocode"];
    
    // idSession=Session id
    if (idSession != nil)
    {
        [parametersDict setObject:idSession forKey:@"idSession"];
    }
    // purchasereceipt=Json with purchase summary from AppStore
    NSError *error;
    NSData *receiptData = [NSJSONSerialization dataWithJSONObject:receipt options:0 error:&error];
    NSString *receiptString = [[NSString alloc] initWithData:receiptData encoding:NSUTF8StringEncoding];
    [parametersDict setObject:receiptString forKey:@"purchaseReceipt"];
    
//    NSLog(@"%@", parametersDict);
    AFJSONRequestSerializer *responseSerializer = (AFJSONRequestSerializer *)_manager.requestSerializer;
    
    NSURLRequest *request = [responseSerializer requestWithMethod:@"POST" URLString:ABA_SECURE_API_URL(@"api/apiuser/freetopremium") parameters:parametersDict.copy error:nil];
    NSMutableURLRequest *mutURLRequest = request.mutableCopy;
    
    [mutURLRequest setValue:_token forHTTPHeaderField:@"ABA_API_AUTH_TOKEN"];
    [mutURLRequest setValue:[self devicePlatform] forHTTPHeaderField:@"DEVICE"];
    mutURLRequest.timeoutInterval = DEFAULT_TIMEOUT;
    
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:mutURLRequest.copy success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        
        if ([responseObject[@"status"] isEqualToString:@"OK"])
        {
            [UserDataController updateUserAfterSubscribeWithDictionary:responseObject completionBlock:^(NSError *error) {
                
                if (error)
                {
                    block(error);
                }
                else
                {
                    block(nil);
                }
            }];
        }
        else
        {
            block([ABAError errorWithKey:STRTRAD(@"alertSubscriptionKOMessage2", @"Error al cambiar a premium un usuario")]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSInteger statusCode = operation.response.statusCode;
        
        if (statusCode == 410)
        {
            block([ABAError errorWithKey:STRTRAD(@"alertSubscriptionKOMessage3", @"Error al cambiar a premium un usuario. El receipt ya está asignado a otro usuario") withCode:410]);
        }
       else if (statusCode == 401)
       {
           block([ABAError errorWithKey:STRTRAD(@"alertSubscriptionKOMessage2", @"Error al cambiar a premium un usuario") withCode:401]);
       }
       else
       {
           block([ABAError errorWithKey:STRTRAD(@"alertSubscriptionKOMessage2", @"Error al cambiar a premium un usuario")]);
       }
    }];
    
    operation.responseSerializer.acceptableContentTypes = [self acceptableContentTypes];
    [_manager.operationQueue addOperation:operation];
}


#pragma mark - Version Control

- (void)getCurrentValidAppVersion:(CompletionBlock)completionBlock
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    AFHTTPResponseSerializer *responseSerializer = manager.responseSerializer;
    responseSerializer.acceptableContentTypes = [self acceptableContentTypes];

    NSURL *URL = [NSURL URLWithString:ABA_SECURE_API_URL(@"api/abaEnglishApi/requiredVersion")];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        completionBlock(error, responseObject);
    }];
    
    [dataTask resume];
}

#pragma mark - Private methods

@end
