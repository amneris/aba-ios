//
//  ABAVideoClass.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABASection+Methods.h"

@class ABAUnit;

@interface ABAVideoClass : ABASection

@property NSString *englishSubtitles;
@property NSString *hdVideoURL;
@property NSString *previewImageURL;
@property NSString *sdVideoURL;
@property NSString *translatedSubtitles;
@property (readonly) ABAUnit *unit;

-(BOOL) hasContent;

@end
