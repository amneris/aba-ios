//
//  ABAPulseViewController.h
//  ABA
//
//  Created by Jaume Cornadó Panadés on 21/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ABAPulseViewController : UIViewController

@property (nonatomic, assign) AVAudioPlayer *player;

-(void) createPulse;

@end
