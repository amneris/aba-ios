//
//  EvaluationResultViewController.m
//  ABA
//
//  Created by Ivan Ferrera on 27/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "EvaluationResultViewController.h"
#import "UIViewController+ABA.h"
#import "ABAEvaluation.h"
#import "ABAEvaluationQuestion.h"
#import "ABAEvaluationOption.h"
#import "Utils.h"
#import "Resources.h"
#import "Definitions.h"
#import "ABAUnit.h"
#import "ABAAlert.h"
#import "UserController.h"
#import "EvaluationController.h"
#import "LevelUnitController.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import "ABARealmLevel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ImageHelper.h"
#import "LegacyTrackingManager.h"
#import "PureLayout.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

#define ABA_EVALUATION_RESULT_OK 10
#define ABA_EVALUATION_RESULT_MIDOK 8

@interface EvaluationResultViewController ()

@property(weak, nonatomic) IBOutlet UIButton *repetirButton;
@property(weak, nonatomic) IBOutlet UIButton *continuarButton;

@property(weak, nonatomic) IBOutlet UIImageView *smileyImageView;
@property(weak, nonatomic) IBOutlet UILabel *resultLabel;

@property(weak, nonatomic) IBOutlet UILabel *messageTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *messageContentLabel;

@property(weak, nonatomic) IBOutlet UILabel *comparteLabel;
@property(weak, nonatomic) IBOutlet UIView *shareView;
@property(weak, nonatomic) IBOutlet UIButton *facebookButton;
@property(weak, nonatomic) IBOutlet UIButton *twitterButton;
@property(weak, nonatomic) IBOutlet UIButton *linkedInButton;

@property(weak, nonatomic) IBOutlet UIImageView *teacherImage;
@property(weak, nonatomic) IBOutlet UILabel *teacherLabel;

@property(weak, nonatomic) IBOutlet NSLayoutConstraint *repetirEvaluacionButtonHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *continuarButtonHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *continuarButtonTopSpaceConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *shareViewHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *teacherViewHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonALeadingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonATrailingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBLeadingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBTrailingConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *messageTitleLabelTopConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *messageLabelHeightConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *repetirButtonTopConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *continuarButtonBottomConstraint;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint *teacherLabelHeightConstraint;

@property(nonatomic) NSInteger correctAnswers;

@property(nonatomic, strong) EvaluationController *evaluationController;

- (IBAction)continuarAction:(id)sender;
- (IBAction)repetirAction:(id)sender;
- (IBAction)shareAction:(id)sender;

@end

@implementation EvaluationResultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _evaluationController = [EvaluationController new];

    _correctAnswers = [EvaluationController getEvaluationCorrectAnswers:_evaluation];

    [self setupView];

    if (_correctAnswers == ABA_EVALUATION_RESULT_OK)
    {
        [self statusOK];
    }
    else if (_correctAnswers < ABA_EVALUATION_RESULT_MIDOK)
    {
        [self statusKO];
    }
    else
    {
        [self statusMidOK];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [[LegacyTrackingManager sharedManager] trackPageView:@"assessmentsection"];
}

- (void)setupView
{
    [self addBackButtonWithTitle:STRTRAD(@"evaluaKey", @"Evaluación") andSubtitle:@"" withTarget:self withAction:@selector(repetirAction:)];
    
    // if current language is chinese hide facebook button
    if ([[LanguageController getCurrentLanguage] isEqualToString:@"zh"])
    {
        [_facebookButton setHidden:YES];
        
        [_twitterButton.constraints autoRemoveConstraints];
        
        [_twitterButton autoCenterInSuperview];
        [_twitterButton autoSetDimensionsToSize:CGSizeMake(31, 31)];
        
        [self.view layoutIfNeeded];
    }
    
    _comparteLabel.text = STRTRAD(@"evaluationComparteKey", @"Comparte tu puntuación");
    _comparteLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 20.0f : 16.0f];

    _continuarButton.layer.cornerRadius = 3.0f;
    _repetirButton.layer.cornerRadius = 3.0f;
    _continuarButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 24.0f : 14.0f];
    _repetirButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 24.0f : 14.0f];

    if (IS_IPAD)
    {
        _continuarButtonHeightConstraint.constant = _continuarButtonHeightConstraint.constant * 2.0f;
        _repetirEvaluacionButtonHeightConstraint.constant = _repetirEvaluacionButtonHeightConstraint.constant * 2.0f;
        _continuarButtonTopSpaceConstraint.constant = _continuarButtonTopSpaceConstraint.constant * 1.4f;
        _buttonALeadingConstraint.constant = _buttonALeadingConstraint.constant * 10;
        _buttonATrailingConstraint.constant = _buttonATrailingConstraint.constant * 10;
        _buttonBLeadingConstraint.constant = _buttonBLeadingConstraint.constant * 10;
        _buttonBTrailingConstraint.constant = _buttonBTrailingConstraint.constant * 10;
        _messageTitleLabelTopConstraint.constant = _messageTitleLabelTopConstraint.constant * 2.5f;
        _messageLabelHeightConstraint.constant = _messageLabelHeightConstraint.constant * 2.0f;

        _repetirButtonTopConstraint.constant = _repetirButtonTopConstraint.constant * 2;
        _continuarButtonBottomConstraint.constant = _continuarButtonBottomConstraint.constant * 2;
    }

    _teacherLabel.text = STRTRAD(@"evaluationRemindTeacherMessageKey", @"Ojo: antes de repetir la evaluación quizá necesites repasar la unidad");
    _teacherLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD ? 20 : 14];
    [_teacherLabel sizeToFit];

    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = _teacherLabel.font;
    gettingSizeLabel.text = _teacherLabel.text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_teacherLabel.frame.size.width, 9999);
    CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    _teacherLabelHeightConstraint.constant = size.height;

    //	[_teacherLabel sizeToFit];
    [_teacherLabel setNeedsLayout];
    [_teacherLabel setNeedsUpdateConstraints];
    [_teacherLabel layoutIfNeeded];

    // linkedin button config
    [_linkedInButton setBackgroundColor:RGB(0, 127, 190)];
    _linkedInButton.layer.cornerRadius = 5.0f;
    _linkedInButton.titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 24.0f : 14.0f];
}

- (void)showLinkedInIfNeed
{
    // detect if should show linkedIn

    ABARealmLevel *currentLevel = [UserController returnCurrentLevel];
    NSArray *incompletedUnits = [LevelUnitController getInompletedUnitsForLevel:currentLevel];

//#warning test linkdin button
//    incompletedUnits = @[];
    
    if (incompletedUnits.count == 0)
    {
        [_linkedInButton setTitle:STRTRAD(@"evaluationLinkedinButtonKey", @"Add to profile") forState:UIControlStateNormal];

        [_comparteLabel setText:STRTRAD(@"evaluationUpdateYourCVLinkedin", @"¡Actualiza tu CV en LinkedIn!")];

        [_shareView setHidden:YES];
        [_linkedInButton setHidden:NO];
    }
}

- (void)statusOK
{
    [_repetirButton setTitle:STRTRAD(@"evaluationRepiteButtonKey", @"repetir la evaluación") forState:UIControlStateNormal];

    _smileyImageView.image = [UIImage imageNamed:@"evaluation-smiley-ok"];
    NSMutableAttributedString *resultAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d %@ %d", ABA_EVALUATION_RESULT_OK, STRTRAD(@"deKey", @"de"), ABA_EVALUATION_RESULT_OK]];
    [resultAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD ? 36 : 30] range:NSMakeRange(0, [resultAttrString.string length])];
    [resultAttrString addAttribute:NSForegroundColorAttributeName value:ABA_COLOR_DARKGREY range:NSMakeRange(0, [resultAttrString.string length])];
    [resultAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 30 : 24] range:[resultAttrString.string rangeOfString:STRTRAD(@"deKey", @"de")]];
    [resultAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[resultAttrString.string rangeOfString:[NSString stringWithFormat:@"%d", ABA_EVALUATION_RESULT_OK] options:NSBackwardsSearch]];
    _resultLabel.attributedText = resultAttrString;

    ABAUnit *currentUnit = _evaluation.unit;

    _messageTitleLabel.text = STRTRAD(@"evaluationFelicidadesKey", @"¡Felicidades!");
    _messageContentLabel.text = [NSString stringWithFormat:@"%@ %@ %@", STRTRAD(@"evaluationMessageOK1Key", @"tu puntuación en la evaluación de la unidad"), currentUnit.idUnit, STRTRAD(@"evaluationMessageOK2Key", @"ha sido perfecta")];
    _messageContentLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD ? 24.0f : 16.0f];
    _repetirEvaluacionButtonHeightConstraint.constant = 0;
    _continuarButtonTopSpaceConstraint.constant = 0;
    _teacherViewHeightConstraint.constant = 0;

    typeof(self) __weak weakself = self;

    [_evaluationController endEvaluationWithOK:_evaluation completionBlock:^(NSError *error, id object) {
        
        [weakself.continuarButton setTitle:[NSString stringWithFormat:@"%@ %@", STRTRAD(@"evaluationContinuaUnidadButtonKey", @"continuar con la unidad"), [UserController returnCurrentUnit].idUnit] forState:UIControlStateNormal];
    }];

    [self showCompletedUnitPostFacebookIfNeed];
    [self showLinkedInIfNeed];
}

- (void)statusMidOK
{
    [_repetirButton setTitle:STRTRAD(@"evaluationReviewErrorsKey", @"Repasa los errores") forState:UIControlStateNormal];

    _smileyImageView.image = [UIImage imageNamed:@"evaluation-smiley-ok"];

    NSMutableAttributedString *resultAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld %@ %d", (long)_correctAnswers, STRTRAD(@"deKey", @"de"), ABA_EVALUATION_RESULT_OK]];
    [resultAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD ? 36 : 30] range:NSMakeRange(0, [resultAttrString.string length])];
    [resultAttrString addAttribute:NSForegroundColorAttributeName value:ABA_COLOR_DARKGREY range:NSMakeRange(0, [resultAttrString.string length])];
    [resultAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 30 : 24] range:[resultAttrString.string rangeOfString:STRTRAD(@"deKey", @"de")]];
    [resultAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[resultAttrString.string rangeOfString:[NSString stringWithFormat:@"%d", ABA_EVALUATION_RESULT_OK] options:NSBackwardsSearch]];
    _resultLabel.attributedText = resultAttrString;

    ABAUnit *currentUnit = _evaluation.unit;

    _messageTitleLabel.text = STRTRAD(@"evaluationHasAprobadoKey", @"¡Has aprobado!");
    _messageContentLabel.text = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"evaluationMessageMidOK1Key", @"has superado la evaluación de la unidad"), currentUnit.idUnit];

    _teacherViewHeightConstraint.constant = 0;

    typeof(self) __weak weakself = self;

    [_evaluationController endEvaluationWithOK:_evaluation
                               completionBlock:^(NSError *error, id object) {

                                   [weakself.continuarButton setTitle:[NSString stringWithFormat:@"%@ %@", STRTRAD(@"evaluationContinuaUnidadButtonKey", @"continuar con la unidad"), [UserController returnCurrentUnit].idUnit] forState:UIControlStateNormal];
                               }];

    [self showCompletedUnitPostFacebookIfNeed];
    [self showLinkedInIfNeed];
}

- (void)statusKO
{
    [_repetirButton setTitle:STRTRAD(@"evaluationRepiteButtonKey", @"repetir la evaluación") forState:UIControlStateNormal];

    _smileyImageView.image = [UIImage imageNamed:@"evaluation-smiley-ko"];

    NSMutableAttributedString *resultAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld %@ %d", (long)_correctAnswers, STRTRAD(@"deKey", @"de"), ABA_EVALUATION_RESULT_OK]];
    [resultAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD ? 36 : 30] range:NSMakeRange(0, [resultAttrString.string length])];
    [resultAttrString addAttribute:NSForegroundColorAttributeName value:ABA_COLOR_DARKGREY range:NSMakeRange(0, [resultAttrString.string length])];
    [resultAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD ? 30 : 24] range:[resultAttrString.string rangeOfString:STRTRAD(@"deKey", @"de")]];
    [resultAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:[resultAttrString.string rangeOfString:[NSString stringWithFormat:@"%d", ABA_EVALUATION_RESULT_OK] options:NSBackwardsSearch]];
    _resultLabel.attributedText = resultAttrString;

    _messageTitleLabel.text = STRTRAD(@"evaluationVuelveIntentarloKey", @"¡Vuelve a intentarlo!");
    NSMutableAttributedString *messageContentAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %d/%d.", STRTRAD(@"evaluationVuelveIntentarMessage", @"para superar la evaluación de esta unidad tienes que obtener como mínimo una calificación de"), ABA_EVALUATION_RESULT_MIDOK, ABA_EVALUATION_RESULT_OK]];
    [messageContentAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_700 size:IS_IPAD ? 24 : 16] range:NSMakeRange(0, [messageContentAttrString.string length])];
    [messageContentAttrString addAttribute:NSForegroundColorAttributeName value:ABA_COLOR_DARKGREY range:NSMakeRange(0, [messageContentAttrString.string length])];
    [messageContentAttrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD ? 24 : 16] range:[messageContentAttrString.string rangeOfString:[NSString stringWithFormat:@"%d/%d", ABA_EVALUATION_RESULT_MIDOK, ABA_EVALUATION_RESULT_OK]]];

    _messageContentLabel.attributedText = messageContentAttrString;

    ABARealmUser *user = UserController.currentUser;

    if (user.teacherImage != nil)
    {
        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:user.teacherImage];
        NSURL *imageUrl = [NSURL URLWithString:urlString];

        [_teacherImage sd_setImageWithURL:imageUrl];
    }

    _teacherLabel.text = STRTRAD(@"evaluationRemindTeacherMessageKey", @"Ojo: antes de repetir la evaluación quizá necesites repasar la unidad");
    _teacherLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD ? 20 : 14];

    NSDictionary *attributes = @{NSFontAttributeName : _teacherLabel.font};
    // NSString class method: boundingRectWithSize:options:attributes:context is
    // available only on ios7.0 sdk.
    CGRect rect = [_teacherLabel.text boundingRectWithSize:CGSizeMake(_teacherLabel.frame.size.width, CGFLOAT_MAX)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:attributes
                                                   context:nil];

    _teacherLabelHeightConstraint.constant = rect.size.height;
    [_teacherLabel setNeedsLayout];
    [_teacherLabel setNeedsUpdateConstraints];
    [_teacherLabel layoutIfNeeded];

    _continuarButtonHeightConstraint.constant = 0;
    _continuarButtonTopSpaceConstraint.constant = 0;
    _shareViewHeightConstraint.constant = 0;
    _teacherViewHeightConstraint.constant = 92;

    [_evaluationController endEvaluationWithKO:_evaluation
                               completionBlock:^(NSError *error, id object){
                               }];
}

- (IBAction)continuarAction:(id)sender
{
    __weak typeof(self) weakself = self;
    [EvaluationController reloadEvaluationAnswers:weakself.evaluation
                                  completionBlock:^(NSError *error, id object) {
                                      [weakself.navigationController popToRootViewControllerAnimated:YES];
                                  }];
}

- (IBAction)repetirAction:(id)sender
{
    __weak typeof(self) weakself = self;

    if (_correctAnswers == ABA_EVALUATION_RESULT_OK)
    {
        [EvaluationController reloadEvaluationAnswers:weakself.evaluation
                                      completionBlock:^(NSError *error, id object) {
                                          [weakself.navigationController popToViewController:[[weakself.navigationController viewControllers] objectAtIndex:weakself.navigationController.viewControllers.count - 4] animated:YES];
                                      }];
    }
    else if (_correctAnswers < ABA_EVALUATION_RESULT_MIDOK)
    {
        [EvaluationController reloadEvaluationAnswers:weakself.evaluation
                                      completionBlock:^(NSError *error, id object) {
                                          [weakself.navigationController popToViewController:[[weakself.navigationController viewControllers] objectAtIndex:weakself.navigationController.viewControllers.count - 2] animated:YES];
                                      }];
    }
    else
    {
        [EvaluationController reloadWrongAnswers:weakself.evaluation
                                 completionBlock:^(NSError *error, id object) {
                                     [weakself.navigationController popToViewController:[[weakself.navigationController viewControllers] objectAtIndex:weakself.navigationController.viewControllers.count - 2] animated:YES];
                                 }];
    }
}

- (IBAction)shareAction:(id)sender
{
    ABARealmLevel *currentLevel = [UserController returnCurrentLevel];

    NSArray *incompletedUnits = [LevelUnitController getInompletedUnitsForLevel:currentLevel];
    NSString *certificateName = [NSString stringWithFormat:@"certificateLevel%@", currentLevel.idLevel];

    if (sender == _facebookButton)
    {
        NSString *facebookText;
        
        if (incompletedUnits.count == 0)
        {
            facebookText = [NSString stringWithFormat:STRTRAD(@"evaluationEndLevelSharingFacebookMessage", @"YES. I did it! I got my %@ Certificate! Thank you, ABA English :)"), STRTRAD(certificateName, @"tipo del certificado")];
        }
        else
        {
            facebookText = STRTRAD(@"evaluationSharingFacebookMessage", @"YES. I did it! My new superpower is learning English");
        }
        
        [self createFacebookPostWithText:facebookText andLink:AppSjareFbIos];
    }
    else if (sender == _twitterButton)
    {
        if (![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            [self showTwitterErrorAlert];
            return;
        }

        SLComposeViewController *twitterController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        if (!twitterController)
        {
            [self showTwitterErrorAlert];
            return;
        }

        NSString *twitterText;
        if (incompletedUnits.count == 0)
        {
            twitterText = [NSString stringWithFormat:STRTRAD(@"evaluationEndLevelSharingTwitterMessage", @"WooHOO! I graduated with my %@ Certificate from @ABAEnglish"), STRTRAD(certificateName, @"tipo del certificado")];
        }
        else
        {
            NSString *unitText = [NSString stringWithFormat:@"%@ %@", [_evaluation unit].idUnit, [[_evaluation unit] title]];

            twitterText = [NSString stringWithFormat:STRTRAD(@"evaluationSharingTwitterMessage", @"Oh yes, I finished unit %@ @ABAEnglish"), unitText];
        }

        [twitterController setInitialText:twitterText];
        [twitterController addURL:AppShareTwIos];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    else if (sender == _linkedInButton)
    {
        NSURL *appURL;
        NSInteger level = [_evaluation.unit.level.idLevel integerValue];

        //    Beginners: https://lnkd.in/dz2jezJ
        //    Lower Intermediate: https://lnkd.in/eEr_zQx
        //    Intermediate: https://lnkd.in/eZvtbdd
        //    Upper Intermediate: https://lnkd.in/eJwtcjH
        //    Advanced: https://lnkd.in/eF9rNjb
        //    Business: https://lnkd.in/eUJyeP8

        switch (level)
        {
        case 1:
            appURL = [NSURL URLWithString:@"https://lnkd.in/dz2jezJ"];
            break;
        case 2:
            appURL = [NSURL URLWithString:@"https://lnkd.in/eEr_zQx"];
            break;
        case 3:
            appURL = [NSURL URLWithString:@"https://lnkd.in/eZvtbdd"];
            break;
        case 4:
            appURL = [NSURL URLWithString:@"https://lnkd.in/eJwtcjH"];
            break;
        case 5:
            appURL = [NSURL URLWithString:@"https://lnkd.in/eF9rNjb"];
            break;
        case 6:
            appURL = [NSURL URLWithString:@"https://lnkd.in/eUJyeP8"];
            break;
        default:
            break;
        }

        [[UIApplication sharedApplication] openURL:appURL];
    }
}

- (void)showCompletedUnitPostFacebookIfNeed {
    
    if ([UserController isTheFirstCompletedUnit] && ![[LanguageController getCurrentLanguage] isEqualToString:@"zh"]) {
        NSString * facebookText;
        facebookText = STRTRAD(@"evaluationSharingFacebookMessage", @"YES. I did it! My new superpower is learning English");
        [self createFacebookPostWithText:facebookText andLink:AppSjareFbIos];
    }
}


- (void)createFacebookPostWithText:(NSString *)text andLink:(NSURL *)link {
    
    // First trying native way
    bool isNativeAvailable = NO;
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *facebookController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        if (facebookController) {
            isNativeAvailable = YES;
            
            [facebookController setInitialText:text];
            [facebookController addURL:link];
            [self presentViewController:facebookController animated:YES completion:nil];
        }
    }
    
    // Proceeding to library if native was not successful
    if (!isNativeAvailable) {
        FBSDKShareLinkContent *facebookContent = [[FBSDKShareLinkContent alloc] init];
        facebookContent.contentDescription = text;
        facebookContent.contentURL = link;
        [FBSDKShareDialog showFromViewController:self withContent:facebookContent delegate:nil];
    }
}


- (void)showTwitterErrorAlert
{
    ABAAlert *alert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"twitterShareErrorKey", @"Revisa configuración twitter en ajustes")];
    [alert show];
}

- (void)showFacebookErrorAlert
{
    ABAAlert *alert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"facebookShareErrorKey", @"Revisa configuración facebook en ajustes")];
    [alert show];
}

@end
