//
//  RootRouter.swift
//  ABA
//
//  Created by MBP13 Jesus on 05/07/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation
import Device
import BWWalkthrough

@objc class RootRouter: NSObject, BWWalkthroughViewControllerDelegate {

    var splashImageName: String {
        switch Device.size() {
        case .screen3_5Inch:
            return "LaunchImage-700@2x.png"
        case .screen4Inch,
             .screen4_7Inch:
            return "LaunchImage-800-667h@2x.png"
        case .screen5_5Inch:
            return "LaunchImage-800-Portrait-736h@3x.png"
        case .screen9_7Inch,
             .screen12_9Inch,
             .screen7_9Inch:
            return UIApplication.shared.statusBarOrientation.isPortrait ? "LaunchImage-700-Portrait@2x~ipad.png" : "LaunchImage-700-Landscape@2x~ipad.png"
        default:
            return "LaunchImage"
        }
    }

    // MARK: Routing

    func loadStartStoryboardAB(_ window: UIWindow) {
        let storyboard = UIStoryboard(name: "Start", bundle: Bundle.main)
        let vc = storyboard.instantiateInitialViewController()
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }

    func loadMainStoryboard(_ window: UIWindow) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateInitialViewController()
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }

    func loadLevelSelectionViewController(_ window: UIWindow) {
        let storyboard = UIStoryboard(name: "Start", bundle: Bundle.main)
        let levelVc = storyboard.instantiateViewController(withIdentifier: "LevelViewController") as? LevelViewController
        levelVc?.presentedFromMenu = false

        window.rootViewController = levelVc
        window.makeKeyAndVisible()
    }

    func showBlockViewController(_ window: UIWindow) {
        let storyboard = UIStoryboard(name: "Start", bundle: Bundle.main)
        let levelVc = storyboard.instantiateViewController(withIdentifier: "BlockViewController")

        window.rootViewController = levelVc
        window.makeKeyAndVisible()
    }

    // MARK: Private

    func loadSplashImage(_ window: UIWindow) {
        let splashImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        splashImageView.image = UIImage(named: splashImageName)

        let rootVC = UIViewController()
        rootVC.view = splashImageView

        window.rootViewController = rootVC
        window.makeKeyAndVisible()
    }
}
