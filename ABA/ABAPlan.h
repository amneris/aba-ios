//
//  ABAPlan.h
//  ABA
//
//  Created by Jordi Mele on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "Products.h"

@interface ABAPlan : NSObject <PlansPageSubscriptionProduct>

// Model attributes
@property (nonatomic, copy) NSString * _Nullable originalIdentifier;
@property (nonatomic, copy) NSString * _Nullable discountIdentifier;
@property (nonatomic, assign) NSInteger days;
@property (nonatomic, assign) BOOL is2x1;

// iTunes attributes
@property (nonatomic, strong) SKProduct * _Nullable productWithDiscount;
@property (nonatomic, strong) SKProduct * _Nullable productWithOriginalPrice;

#pragma mark - Public methods (accessors)

- (NSString * _Nonnull)titlePlan;
- (NSString * _Nonnull)pricePlan;
- (NSString * _Nonnull)originalPricePlan;
- (NSString * _Nonnull)descriptionPlan;
- (void)mergeWithOriginalITunesProduct:(SKProduct * _Nullable)originalProduct andDiscountProduct:(SKProduct * _Nullable)discountProduct;

@end
