//
//  ABAScrollView.h
//  ABA
//
//  Created by Jordi Mele on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ABAScrollView : UIScrollView

@end
