
//
//  ABASection.h
//  ABA
//
//  Created by Marc Güell Segarra on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"
#import <Realm/Realm.h>

@class ABAUnit;

@interface ABASection : RLMObject

@property NSNumber<RLMBool> * completed;
@property NSNumber<RLMInt> * progress;
@property NSNumber<RLMBool> * unlock;

@end
