//
//  UnitListController.m
//  ABA
//
//  Created by Marc Güell Segarra on 5/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "LevelUnitController.h"
#import "UserController.h"
#import "DataController.h"
#import "ABARealmUser.h"
#import "ABARealmLevel.h"
#import "ABAUnit.h"
#import "ABARealmCert.h"
#import "ABAError.h"
#import "Resources.h"
#import "ProgressController.h"
#import "SectionController.h"
#import "FilmController.h"
#import "AudioController.h"
#import "SpeakController.h"
#import "WriteController.h"
#import "InterpretController.h"
#import "ExercisesController.h"
#import "VocabularyController.h"
#import "ImageHelper.h"
#import "UserDataController.h"


@implementation LevelUnitController

#pragma mark - Public methods

+ (NSInteger)getNumberOfLevels
{
    return [[self getAllLevels] count];
}

+ (RLMResults *)getAllLevels
{
    return [DataController getLevelsAndUnits];
}

+ (RLMResults *)getAllLevelsDescending
{
    RLMResults *levels = [self getAllLevels];
    
    RLMSortDescriptor *sortLevels = [RLMSortDescriptor sortDescriptorWithProperty:@"idLevel" ascending:NO];
    RLMResults *sortedLevels = [levels sortedResultsUsingDescriptors:@[sortLevels]];
        
    return sortedLevels;
}

+ (RLMResults *)getUnitsDescendingForLevel:(ABARealmLevel *)level
{
    return [self getUnitsDescendingForArray:level.units];
}

+ (ABARealmLevel *)getLevelWithID:(NSString *)idLevel
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *levelFilterPredicate = [NSPredicate predicateWithFormat:@"idLevel = %@", idLevel];
    RLMResults *arrayWithLevel = [ABARealmLevel objectsInRealm:realm withPredicate:levelFilterPredicate];
    
    return [arrayWithLevel firstObject];
}

+ (ABAUnit *)getUnitWithID:(NSNumber *)idUnit forLevel:(ABARealmLevel *)level
{
    for (ABAUnit *unit in level.units)
    {
        if ([unit.idUnit isEqualToNumber:idUnit])
        {
            return unit;
        }
    }
    
    return nil;
}

+ (ABAUnit *)getUnitWithID:(NSNumber *)idUnit
{
    for (ABARealmLevel* level in [self getAllLevels])
    {
        ABAUnit* unit = [self getUnitWithID:idUnit forLevel:level];
        
        if (unit != nil)
        {
            return unit;
        }
    }
    
    return nil;
}

+ (ABAUnit *)getLastCompletedUnitForLevel:(ABARealmLevel *)level
{
    RLMResults *unitsSorted = [self getUnitsDescendingForLevel:level];
    NSArray *completedUnits = [self getCompletedUnitsForArray:unitsSorted];
    
    return [completedUnits firstObject];
}

+ (NSArray *)getCompletedUnitsForLevel:(ABARealmLevel *)level
{
    NSMutableArray *completedUnits = [[NSMutableArray alloc] init];
    
    for (ABAUnit *unit in level.units)
    {
        if ([unit.completed integerValue] == 1)
        {
            [completedUnits addObject:unit];
        }
    }
    
    return completedUnits;
}

+ (NSArray *)getInompletedUnitsForLevel:(ABARealmLevel *)level
{
    NSMutableArray *completedUnits = [[NSMutableArray alloc] init];
    
    for (ABAUnit *unit in level.units)
    {
        if ([unit.completed integerValue] == 0)
        {
            [completedUnits addObject:unit];
        }
    }
    
    return completedUnits;
}

+ (BOOL) isLastCompleted:(ABAUnit *)unit forLevel:(ABARealmLevel *)level
{
    return [unit.idUnitString isEqualToString:[self getLastCompletedUnitForLevel:level].idUnitString];
}

+ (void)setCompletedUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)completionBlock
{
    __weak typeof(self) weakSelf = self;
    
    [UserDataController setCompletedUnit:unit completionBlock:^(NSError *error, id object)
     {
         
         if (!error)
         {
             NSArray *completedUnits = [weakSelf getCompletedUnitsForLevel:unit.level];
             
             ABARealmLevel *currentLevel = [weakSelf getLevelWithID:unit.level.idLevel];
             
             if ([completedUnits count] == currentLevel.units.count)
             {
                 [weakSelf setCompletedLevel:currentLevel completionBlock:^(NSError *error, id object)
                  {
                      completionBlock(error, unit);
                  }];
             }
             else
             {
                 completionBlock(error, unit);
             }
         }
         else
         {
             completionBlock(error, nil);
         }
     }];
}

+ (void)setCompletedLevel:(ABARealmLevel *)level completionBlock:(CompletionBlock)completionBlock
{
    [UserDataController setCompletedLevel:level completionBlock:completionBlock];
}

+ (void)completeUnit:(ABAUnit *)unit forUser:(ABARealmUser *)user completionBlock:(CompletionBlock)completionBlock
{
    __strong typeof(self) weakSelf = self;
    
    //First mark unit as completed
    [self setCompletedUnit:unit completionBlock:^(NSError *error, id object)
     {
         if (!error)
         {
             if ([unit.level.completed boolValue])
             {
                 [weakSelf goToNextLevel:user];
             }
             
             completionBlock(error, user);
             
         } else {
             
             completionBlock(error, user);
         }
     }];
}

+ (ABARealmLevel *)getNextLevel:(ABARealmLevel *)level
{
    NSUInteger numberOfNextLevel = [level.idLevel integerValue] + 1;
    
    if (numberOfNextLevel > ABA_DEFAULT_LEVEL_NUMBER)
    {
        return level; //last level, so no nextlevel
    }
    
    return [self getLevelWithID:[NSString stringWithFormat:@"%lu", (unsigned long)numberOfNextLevel]];
}

+ (ABAUnit *)getNextUnit:(ABAUnit *)unit
{
    NSUInteger numberOfNextUnit = [unit.idUnit integerValue] + 1;
    
    if (numberOfNextUnit > ABA_DEFAULT_UNIT_NUMBER)
    {
        return unit;
    }
    
    ABAUnit *nextUnit = [self getUnitWithID:[NSNumber numberWithInteger:numberOfNextUnit] forLevel:unit.level];
    
    if (nextUnit == nil)
    {
        ABARealmLevel *nextLevel = [self getNextLevel:unit.level];
        NSInteger levelNumber = [nextLevel.idLevel integerValue];
        NSInteger numberOfFirstUnit = ((levelNumber - 1) * ABA_DEFAULT_UNITS_PER_LEVEL) + 1;
        return [self getUnitWithID:[NSNumber numberWithInteger:numberOfFirstUnit] forLevel:nextLevel];
    }
    else
    {
        return nextUnit;
    }
    
    return nil;
}

+ (NSString *)getIdLevelForIdUnit:(NSNumber *)idUnit
{
    if ([idUnit integerValue] >= 1 & [idUnit integerValue] <= 24)
    {
        return @"1";
    }
    else if ([idUnit integerValue] >= 25 & [idUnit integerValue] <= 48)
    {
        return @"2";
    }
    else if ([idUnit integerValue] >= 49 & [idUnit integerValue] <= 72)
    {
        return @"3";
    }
    else if ([idUnit integerValue] >= 73 & [idUnit integerValue] <= 96)
    {
        return @"4";
    }
    else if ([idUnit integerValue] >= 97 & [idUnit integerValue] <= 120)
    {
        return @"5";
    }
    else if ([idUnit integerValue] >= 121 & [idUnit integerValue] <= 144)
    {
        return @"6";
    }
    else return nil;
}

+ (void)checkForUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang andBlock:(ErrorBlock) block
{
    //__weak typeof(self) weakSelf = self;
    
    if ([[self getAllLevels] count] == 0)       //If we got no levels we seed it
    {
        [self seedLevels:^(NSError *error)
         {
             if (!error)
             {
                 [self seedUnitsWithToken:token withLanguage:userLang andBlock:^(NSError *error) // And then seed units
                  {
                      block(error);
                  }];
             }
             else
             {
                 block(error);
             }
         }];
    }
    else if ([[DataController getUnits] count] == 0) // There are levels but not units
    {
        [self seedUnitsWithToken:token withLanguage:userLang andBlock:^(NSError *error)
         {
             block(error);
         }];
    }
    else
    {
        block(nil);
    }
}

+ (void)refreshAllLevelsProgress:(CompletionBlock)block
{
    __block NSUInteger results = 0;
    __block NSUInteger levelCount = [[self getAllLevels] count];
    for (ABARealmLevel* level in [self getAllLevels]) {
        float progress = 0.0;
        for (ABAUnit* unit in level.units) {
            progress += [unit.unitSectionProgress floatValue];
        }
        
        float currentLevelProgress = progress / (24.0 * 800.0);
        
        currentLevelProgress = currentLevelProgress * 100.0;
        
        [DataController setLevelProgress:level newProgress:[NSNumber numberWithFloat:currentLevelProgress] completionBlock:^(NSError *error, id object) {
            results++;
            if (results == levelCount)
                block(nil,nil);
        }];
    }
}

+ (ABAUnit *)getCurrentUnitForLevel:(ABARealmLevel *)level
{
    RLMResults *unitsSorted = [self getUnitsDescendingForLevel:level];
    NSArray *incompletedUnits = [self getIncompletedUnitsForArray:unitsSorted];
    
    if ([incompletedUnits count] == 0)
    {
        return [unitsSorted lastObject];
    }
    else
    {
        return [incompletedUnits lastObject];
    }
}

#pragma mark - Private methods

+ (void)goToNextLevel:(ABARealmUser *)user
{
    [UserController setCurrentLevel:[self getNextLevel:user.currentLevel] completionBlock:nil];
}

+ (void)seedUnitsWithToken:(NSString *)token withLanguage:(NSString *)userLang andBlock:(ErrorBlock)block
{
    [DataController getAllUnitsWithToken:token withLanguage:userLang completionBlock:^(NSError *error, id object) {
        block(error);
    }];
}

+ (NSArray *)getCompletedUnitsForArray:(RLMResults *)unitsArray
{
    NSMutableArray *completedUnits = [[NSMutableArray alloc] init];
    
    for (ABAUnit *unit in unitsArray)
    {
        if ([unit.completed integerValue] == 1)
        {
            [completedUnits addObject:unit];
        }
    }
    
    return completedUnits;
}

+ (NSArray *)getIncompletedUnitsForArray:(RLMResults *)unitsArray
{
    NSMutableArray *incompletedUnits = [[NSMutableArray alloc] init];
    
    for (ABAUnit *unit in unitsArray)
    {
        if ([unit.completed integerValue] == 0)
        {
            [incompletedUnits addObject:unit];
        }
    }
    
    return incompletedUnits;
}

+ (RLMResults *)getUnitsDescendingForArray:(RLMArray *)unitsArray
{
    RLMSortDescriptor *aSortDescriptor = [RLMSortDescriptor sortDescriptorWithProperty:@"idUnit" ascending:NO];
    
    RLMResults *sortedUnitsArray = [unitsArray sortedResultsUsingDescriptors:[NSArray arrayWithObject:aSortDescriptor]];
    
    return sortedUnitsArray;
}

+ (NSArray *)getUnitsFromLocalJson
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"units" ofType:@"json"];
    
    NSArray *unitsArray = nil;
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath options:NSDataReadingMappedIfSafe error:&error];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (dictionary)
    {
        // We put the parsed information into unitsArray
        unitsArray = [[NSArray alloc] initWithArray:dictionary[@"units"]];
    }
    else
    {
        // Something with parsing went wrong
        NSLog(@"getUnitsFromLocalJson - Parsing error: %@",error);
    }
    
    return unitsArray;
}

+ (NSArray *)getLevelsFromLocalJson
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"levels" ofType:@"json"];
    
    NSArray *levelsArray = nil;
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath options:NSDataReadingMappedIfSafe error:&error];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (dictionary)
    {
        // We put the parsed information into levelsArray
        levelsArray = [[NSArray alloc] initWithArray:dictionary[@"levels"]];
    }
    else
    {
        // Something with parsing went wrong
        NSLog(@"getLevelsFromLocalJson - Parsing error: %@",error);
    }
    
    return levelsArray;
}

+ (void)seedLevels:(ErrorBlock)block
{
    NSArray *levelsArray = [self getLevelsFromLocalJson];
    
    if (levelsArray == nil)
    {
        block([ABAError errorWithKey:STRTRAD(@"errorParseLevelsLocalJson", @"Error en parse de JSON local de niveles")]);
    }
    
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    // You only need to do this once (per thread)
    NSError *error;

    [realm beginWriteTransaction];

    // We fill database with levels
    for (NSDictionary *levelDict in levelsArray)
    {
        ABARealmCert *cert = [[ABARealmCert alloc] init];
        
        ABARealmLevel *level    = [[ABARealmLevel alloc] init];
        level.idLevel      = levelDict[@"idLevel"];
        level.name         = levelDict[@"name"];
        level.desc         = levelDict[@"descKey"];
        
        cert.level = level;
        
        [realm addObject:cert];
    }
    
    [realm commitWriteTransaction:&error];
    
    block(error);
}

@end
