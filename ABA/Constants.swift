//
//  Constants.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

import Device

struct DefaultConstants {
    static let colorAquaBlue = UIColor(red: 7.0 / 255.0, green: 187.0 / 255.0, blue: 228.0 / 255.0, alpha: 1.0)
}

struct RegisterFunnelConstants {
    
    static let colorFormGray = UIColor(red: 142.0 / 255.0, green: 142.0 / 255.0, blue: 146.0 / 255.0, alpha: 1.0)
    static let colorAquaBlue = DefaultConstants.colorAquaBlue
    static let colorRegisterGrayBackground = UIColor(red: 179.0 / 255.0, green: 179.0 / 255.0, blue: 179.0 / 255.0, alpha: 1.0)
    static let colorRegisterGrayText = UIColor(red: 146.0 / 255.0, green: 144.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    static let colorRegisterBlueBackground = UIColor(red: 29.0 / 255.0, green: 103.0 / 255.0, blue: 190.0 / 255.0, alpha: 1.0)
    
    static let bottomButtonTextSize: CGFloat = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 18 : 14
    static let bottomButtonFont = UIFont(name: "Roboto-Regular", size: bottomButtonTextSize)
    
    static let formFieldsFont = UIFont(name: "Roboto-Regular", size: 17.0)
    static let formButtonsFont = UIFont(name: "Roboto-Bold", size: 16.0)
    static let rememberButtonFont = UIFont(name: "Roboto-Regular", size: 14.0)
    
    static let formSidePadding = RegisterFunnelConstants.isScreen55Inches() ? 60.0 : 25.0
    static let formTopPadding = RegisterFunnelConstants.isScreen4InchesOrLess() ? 35.0 : 50.0
    static let formFieldsPadding = RegisterFunnelConstants.isScreen4InchesOrLess() ? 60.0 : 50.0
    
    static let formWidthForIpad = 380.0
    static let formButtonsSeparation = 20.0
    static let formButtonsTopPadding = 35.0
    static let cornerRadius: CGFloat = 3.0
    static let logoPadding = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 50.0 : 35.0;
    
    static func isScreen4InchesOrLess() -> Bool {
        return Device.size() == .screen3_5Inch || Device.size() == .screen4Inch
    }
    
    static func isScreen55Inches() -> Bool {
        return Device.size() == .screen5_5Inch
    }
    
    static func isIpadOrIpadScreenSize() -> Bool {
        // iPad
        return Device.type() == .iPad ||
            // iPad Simulator
            Device.size() == .screen7_9Inch || Device.size() == .screen9_7Inch || Device.size() == .screen12_9Inch
    }
}

struct UnitDetailConstants {
    
    // Unit detail
    static let colorDefault = UIColor(red: 67.0 / 255.0, green: 82.0 / 255.0, blue: 92.0 / 255.0, alpha: 1.0)
    static let colorDisabledDefault = UIColor(red: 146.0 / 255.0, green: 144.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    static let colorSectionDefault = colorDefault
    static let colorSectionDone = UIColor(red: 59.0 / 255.0, green: 188.0 / 255.0, blue: 114.0 / 255.0, alpha: 1.0)
    static let colorSectionCurrent = DefaultConstants.colorAquaBlue
    static let colorGreyTranslucent = UIColor(red: 48.0 / 255.0, green: 62.0 / 255.0, blue: 72.0 / 255.0, alpha: 170.0/255.0)
}
