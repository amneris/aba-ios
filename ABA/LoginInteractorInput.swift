//
// Created by Oleksandr Gnatyshyn on 13/06/16.
// Copyright (c) 2016 ABA English. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginInteractorInput {
    func login(email: String, password: String) -> Observable<User>
    func login(token: String) -> Observable<User>
    func loginWithFacebook(_ facebookUser: FacebookUser) -> Observable<User>
}
