//
//  NSAttributedString+Height.m
//  ABA
//
//  Created by Jordi Mele on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "NSAttributedString+Height.h"
#include <CoreGraphics/CGBase.h>
#include <CoreGraphics/CGPath.h>
#include <CoreGraphics/CGGeometry.h>
#include <CoreText/CTLine.h>

@implementation NSAttributedString (Height)

-(CGFloat)heightForWidth:(CGFloat)width
{
    return ceilf(CGRectGetHeight([self boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                    options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                    context:nil])) + 1;
}


//- (CGFloat)heightForWidth:(CGFloat)width {
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGPathAddRect(path, NULL, CGRectMake(0, 0, width, 99999));
//    CGFloat h = [self heightForPath:path];
//    CGPathRelease(path);
//    return h;
//}

//- (CGFloat)heightForPath:(CGPathRef)path {
//    CGFloat height = 0;
//    CTFrameRef frame =  [self cfframeForPath:path];
//    if (frame != NULL) {
//        NSArray* lines = (__bridge NSArray*)CTFrameGetLines(frame);
//        
//        int l = [lines count];
//        if (l > 1) {
//            CGPoint origins[l];
//            
//            CTFrameGetLineOrigins(frame, CFRangeMake(0, l), origins);
//            
//            CGFloat yFirst = origins[0].y;
//            CGFloat yLast = origins[l-1].y;
//            
//            CGFloat ascent, descent, leading;
//            CTLineGetTypographicBounds((__bridge CTLineRef)[lines objectAtIndex:l-1], &ascent, &descent, &leading);
//            
//            height = ceilf((ascent+descent+leading)*1.3) + yFirst-yLast;
//        } else {
//            if (l==1) {
//                CGFloat ascent, descent, leading;
//                CTLineGetTypographicBounds((__bridge CTLineRef)[lines objectAtIndex:0], &ascent, &descent, &leading);
//                height = ceilf(ascent+descent+leading)*1.3;
//            }
//        }
//        CFRelease(frame);
//    }
//    return height;
//}
//
//- (CTFrameRef)cfframeForPath:(CGPathRef)p {
//    // hack to avoid bugs width different behavior in iOS <4.3 and >4.3
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGRect r = CGPathGetBoundingBox(p);
//    
//    CGAffineTransform t = CGAffineTransformIdentity;
//    
//    t = CGAffineTransformTranslate(t, r.origin.x, r.origin.y);
//    t = CGAffineTransformScale(t, 1, -1);
//    t = CGAffineTransformTranslate(t, r.origin.x, - ( r.origin.y + r.size.height ));
//    CGPathAddPath(path, &t, p);
//    
//    CGPathMoveToPoint(path, NULL, 0, 0);
//    CGPathCloseSubpath(path);
//    // hack end
//    
//    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)self);
//    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), path, NULL);
//    
//    CFRelease(framesetter);
//    CGPathRelease(path);
//    return frame;
//}


@end
