//
//  ABAError.h
//  ABA
//
//  Created by Oriol Vilaró on 3/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABAError : NSError

+ (instancetype)errorWithKey:(NSString *)errorKey withCode:(NSInteger)code;
+ (instancetype)errorWithKey:(NSString *)errorKey;
+ (instancetype)errorConnection;
+ (instancetype)errorMailExists;

@end
