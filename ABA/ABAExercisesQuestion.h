//
//  ABAExercisesQuestion.h
//  ABA
//
//  Created by Marc Güell Segarra on 4/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAExercisesPhrase.h"

@class ABAExercisesGroup;

@interface ABAExercisesQuestion : RLMObject

@property NSNumber<RLMBool> *completed;
@property NSString* exerciseTranslation;
@property RLMArray<ABAExercisesPhrase *><ABAExercisesPhrase> *phrases;

- (ABAExercisesGroup *)exercisesGroup;

@end

RLM_ARRAY_TYPE(ABAExercisesQuestion)

