//
//  ProgressController.m
//  ABA
//
//  Created by Jaume Cornadó Panadés on 10/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "ABARealmLevel.h"
#import "ABARealmUser.h"
#import "ABAUnit.h"
#import "APIManager.h"
#import "DataController.h"
#import "DataManager.h"
#import "LanguageController.h"
#import "LevelUnitController.h"
#import "ProgressController.h"
#import "UserController.h"

#import "ABARealmProgressAction.h"
#import "ABA-Swift.h"

@interface ProgressController ()
@property(nonatomic, strong) DataController *dataController;

@end

@implementation ProgressController

- (id)init
{
    self = [super init];
    if (self)
    {
        _dataController = [DataController new];
    }
    return self;
}

- (void)getCourseProgress:(CompletionBlock)block;
{
    [[APIManager sharedManager] getCourseProgressWithLocale:[UserController getUserLanguage] andUserID:[UserController currentUser].idUser completionBlock:^(NSError *error, id object) {

        if (error)
        {
            block(error, nil);
        }
        else
        {
            [DataController syncUnitProgress:object completionBlock:^(NSError *error, id object) {

                block(error, object);
            }];
        }
    }];
}

- (void)recalculateLevelProgress:(ABARealmLevel *)level completionBlock:(CompletionBlock)block
{
    CGFloat totalProgress = 0.0;
    CGFloat total = 24.0 * 100.0;
    for (ABAUnit *unit in level.units)
    {
        totalProgress += [unit.progress integerValue];
    }

    CGFloat progress = (totalProgress / total) * 100.0;
    [DataController setLevelProgress:level newProgress:[NSNumber numberWithFloat:progress] completionBlock:block];
}

- (void)getUnitProgress:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{

    [[APIManager sharedManager] getUnitProgressWithLocale:[UserController getUserLanguage] andUserID:[UserController currentUser].idUser andUnitID:unit.idUnitString completionBlock:^(NSError *error, id object) {

        [[APIManager sharedManager] setSyncInProgress:NO];

        [DataController syncUnitProgress:@[ object ] completionBlock:^(NSError *error, id object) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kUnitProgressUpdatedNotification object:object];
            block(error, object);
        }];

    }];
}

- (void)getCompletedActionsFromUser:(ABARealmUser *)user onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{
    NSNumber * unitId = unit.idUnit;
    [[APIManager sharedManager] getCompletedActionsFromUserID:user.idUser andUnit:unit withSectionID:nil completionBlock:^(NSError *error, id object) {
        if (!error)
        {
            ABAUnit * unitToUpdate = [LevelUnitController getUnitWithID:unitId];
            
            [DataController updateLastChangedUnit:unitToUpdate];
            [DataController syncCompletedActions:object forUnit:unitToUpdate];
            block(nil, unit);
        }
        else
        {
            block(error, unit);
        }
    }];
}

- (void)getCompletedActionsFromUser:(ABARealmUser *)user onUnit:(ABAUnit *)unit withSectionType:(kABASectionTypes)sectionType completionBlock:(CompletionBlock)block
{
    [[APIManager sharedManager] getCompletedActionsFromUserID:user.idUser andUnit:unit withSectionID:[[NSNumber numberWithInteger:sectionType] stringValue] completionBlock:^(NSError *error, id object) {
        [DataController updateLastChangedUnit:unit];
    }];
}

- (void)sendProgressActionsWithLimit:(int)limit withCompletionBlock:(CompletionBlock)block
{
    RLMResults *pendingActions = [DataController getPendingProgressActions];
    if (pendingActions.count == 0)
    {
        block(nil, nil);
        return;
    }

    // Processing the progress actions
    __block NSMutableArray *actions = [NSMutableArray arrayWithCapacity:pendingActions.count];
    __block NSMutableArray *realmActionsIds = [NSMutableArray arrayWithCapacity:pendingActions.count];
    
    int progressActionCount = 0;
    for (ABARealmProgressAction *progressAction in pendingActions)
    {
        // just taking the first elements up to `limit`
        if (progressActionCount >= limit)
        {
            break;
        }

        // Processing ProgressAction into list to  dispatch
        NSDictionary *actionDictionary = [progressAction toDictionary];
        if (actionDictionary != nil)
        {
            [actions addObject:actionDictionary];
            [realmActionsIds addObject:progressAction.actionId];
            progressActionCount++;
        }
    }
    
    // Making sure the call is performed with data
    if (actions.count == 0) {
        block(nil, nil);
        return;
    }

    // Sending
    [[APIManager sharedManager] sendProgressActionsToServerWithActions:actions completionBlock:^(NSError *error, id object) {
        if (error)
        {
            // error
            block(error, nil);
        }
        else
        {
            // ABA server accepted the actions
            
            // Success
            if (object)
            {
                // The server returns progress update
                
                // Syncing the progress
                [DataController syncUnitProgress:object completionBlock:^(NSError *error, id object) {
                    [DataController setProgressActionsAsSent:realmActionsIds];
                }];
                
                // Sending the progress to cooladata
                [CoolaDataTrackingManager sendUpdatedProgress:object];
            }
            else
            {
                [DataController setProgressActionsAsSent:realmActionsIds];
            }

            block(nil, nil);
        }
    }];
}

+ (NSString *)getActionStringForSection:(ABASection *)section
{
    switch ([section getSectionType])
    {
    case kABAWriteSectionType:
        return @"DICTATION";
        break;
    case kABASpeakSectionType:
    case kABAInterpretSectionType:
    case kABAVocabularySectionType:
        return @"RECORDED";
    case kABAExercisesSectionType:
        return @"WRITTEN";
        break;
    default:
        return @"BLANK";
        break;
    }
}

@end
