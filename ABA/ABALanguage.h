//
//  ABALanguage.h
//  ABA
//
//  Created by Jordi Mele on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABALanguage : NSObject

@property (nonatomic, strong) NSString * idLanguage;
@property (nonatomic, strong) NSString * nameLanguage;

@end
