//
//  ABACustomTextField.h
//  ABA
//
//  Created by Jordi Mele on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABACustomTextField : UITextField


-(void) setNormalBorderColor;

-(void) setErrorBorderColor;

@end
