//
//  ABARealmProgressAction.h
//  ABA
//
//  Created by Jordi Melé on 16/3/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@class ABARealmUser, ABAPhrase, ABASection, ABAUnit;

@interface ABARealmProgressAction : RLMObject

@property NSString *actionId;

@property NSString *action;
@property NSString *audioID;
@property NSNumber<RLMBool> *correct;
@property NSNumber<RLMBool> *isHelp;
@property NSNumber<RLMInt> *page;
@property NSNumber<RLMInt> *punctuation;
@property NSNumber<RLMInt> *secionType;
@property NSNumber<RLMBool> *sentToServer;
@property NSString *text;
@property NSNumber<RLMBool> *startSession;
@property NSString *timeStamp;
@property NSString *ip;
@property NSString *language;
@property NSString *idUser;
@property NSString *idSession;
@property NSNumber<RLMInt> *idUnit;
@property NSString *optionLettersEvaluation;

@property(readonly) NSString *idUnitString;

- (NSDictionary *)toDictionary;
+ (ABARealmProgressAction *)progressActionWithUser:(ABARealmUser *)user
                                     dateFormatter:(NSDateFormatter *)format
                                            phrase:(ABAPhrase *)phrase
                                           section:(ABASection *)section
                                              unit:(ABAUnit *)unit
                                         errorText:(NSString *)text
                                          isListen:(BOOL)listen
                                            isHelp:(BOOL)help;

@end
