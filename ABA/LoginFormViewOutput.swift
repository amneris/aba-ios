//
//  LoginViewOutput2.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol LoginFormViewOutput: FormValidator {
    func loginWithEmailAndPassword(_ email: String, password: String)
    func loginUserWithFacebook()
    func goToForgotPassword()
    func goToRegister()
}
