//
//  ABACustomSectionView.h
//  ABA
//
//  Created by Jordi Mele on 19/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@class ABACustomSectionView, ABASection;

@protocol ABACustomSectionViewDelegate <NSObject>

- (void)loadSection:(kABASectionTypes)type;

@end

@interface ABACustomSectionView : UIView

- (id)initWithType:(kABASectionTypes)type
          delegate:(id<ABACustomSectionViewDelegate>)delegate
              view:(UIView *)view;

- (void)setStyleOptions:(kABASectionStyleOptions)styleOptions;

- (void)updateViews;

@end
