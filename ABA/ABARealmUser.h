//
//  ABARealmUser.h
//  ABA
//
//  Created by Jesus Espejo on 22/05/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABARealmCert.h"

@class ABARealmLevel;

@interface ABARealmUser : RLMObject

// Properties to forward to Mixpanel
@property NSString * phone;
@property NSString * birthdate;
@property NSString * externalKey;
@property NSString * gender;

// Main properties
@property NSString * country;
@property NSString * email;
@property NSDate* expirationDate;
@property NSString * idSession;
@property NSString * idUser;
@property NSString * name;
@property NSString * partnerID;
@property NSString * sourceID;
@property NSNumber<RLMInt> * subscriptionPeriod;
@property NSString * surnames;
@property NSString * teacherId;
@property NSString * teacherImage;
@property NSString * teacherName;
@property NSString * token;
@property NSString * type;
@property NSString * urlImage;
@property NSString * userLang;
@property NSDate * lastLoginDate;
@property RLMArray<ABARealmCert *><ABARealmCert> *certs;
@property ABARealmLevel *currentLevel;

@end

