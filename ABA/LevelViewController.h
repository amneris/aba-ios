//
//  LevelViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 31/10/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelViewController : UIViewController

@property (nonatomic) BOOL presentedFromMenu;

@property (nonatomic) BOOL newRegister;

@end
