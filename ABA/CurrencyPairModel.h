//
//  CurrencyRate.h
//  ABA
//
//  Created by MBP13 Jesus on 10/06/15.
//  Copyright (c) 2015 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyPairModel : NSObject < NSCoding >

@property (nonatomic, strong) NSString * currencyPairCode;
@property (nonatomic, strong) NSString * currencyRate;

@property (nonatomic, strong, readonly) NSString * formerCurrencyCode;
@property (nonatomic, strong, readonly) NSString * latterCurrencyCode;

/**
 Method to know whether the currency pair is properly set and has all the needed fields for a proper conversion
 */
- (BOOL)isProperlySet;

@end
