//
//  SpeakDialogCell.m
//  ABA
//
//  Created by Jordi Mele on 12/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "SpeakDialogCell.h"

@implementation SpeakDialogCell
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
