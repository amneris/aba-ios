//
//  WelcomePageViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 06/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit
import BWWalkthrough
import Device
import BonMot

struct TextAttributedStyles {
    static let titleFontSize: CGFloat  = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 48 : 30
    static let descriptionFontSize: CGFloat  = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 20 : 14
    static let buttonRegisterFontSize: CGFloat = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 20 : 16
    
    static let blueStyle = StringStyle(
        .color(RegisterFunnelConstants.colorAquaBlue)
    )
    
    static let descriptionStyle = StringStyle(
        .lineSpacing(4),
        .font(UIFont(name:"RobotoSlab-Regular", size:descriptionFontSize)!)
    )

    static let buttonStyle = StringStyle(
        .font(UIFont(name: "Roboto-Bold", size:buttonRegisterFontSize)!),
        .color(UIColor.white)
    )
    
    static let style = StringStyle(
        .font(UIFont(name:"RobotoSlab-Regular", size:titleFontSize)!),
        .xmlRules([
            .style("blue", blueStyle)
        ])
    )
    
    static let leftPartWellcomeStyle = StringStyle(
        .color(UIColor.white),
        .font(RegisterFunnelConstants.bottomButtonFont!)
    )
    
    static let rightPartWellcomeStyle = StringStyle(
        .color(RegisterFunnelConstants.colorAquaBlue),
        .font(RegisterFunnelConstants.bottomButtonFont!)
    )
}

class WelcomePageViewController: BWWalkthroughPageViewController {

    let contentWidthConst: CGFloat = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 380 : 260
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    var titleTextString: String? {
        didSet {
            titleText.attributedText = titleTextString?.styled(with: TextAttributedStyles.style)
        }
    }
    
    var descriptionTextString: String? {
        didSet {
            descriptionText.attributedText = descriptionTextString?.styled(with: TextAttributedStyles.descriptionStyle)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        widthConstraint.constant = contentWidthConst
        view.setNeedsUpdateConstraints()
    }
}

