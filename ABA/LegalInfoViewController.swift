//
//  LegalInfoViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 15/12/2016.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit
import BonMot

class LegalInfoViewController: UIViewController {
    @IBOutlet weak var content: UITextView!
    var text: String?
    
    let style = StringStyle(
        .font(UIFont(name: "RobotoSlab-Regular", size: 16)!),
        .lineHeightMultiple(1.5)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.content.textContainerInset = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 10.0)
        content.attributedText = text?.styled(with: style)
    }
}
