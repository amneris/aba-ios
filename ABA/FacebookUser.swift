//
//  FacebookUser.swift
//  ABA
//
//  Created by MBP13 Jesus on 13/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

struct FacebookUser {

    var facebookId: String
    var email: String
    var firstName: String
    
    var lastName: String
    var gender: String
    var imageUrl: String
}