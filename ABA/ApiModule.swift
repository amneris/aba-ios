//
//  ApiModule.swift
//  aba-ios-5
//
//  Created by Oleksandr Gnatyshyn on 13/04/16.
//  Enhanced by the unicorns
//  Copyright © 2016 Oleksandr Gnatyshyn. All rights reserved.
//

import UIKit
import Moya
import RxSwift

enum ApiError: Swift.Error {
    case notAuthenticated
    case rateLimitExceeded
    case wrongJSONParsing
    case generic
}

let APIProvider = RxMoyaProvider<API>(endpointClosure: endpointClosure)
//let SELLIGENT_URL = "http://abaenglish.emsecure.net/optiext/optiextension.dll?ID=vHKtblTll2JrfgTN_T2CGgGKw0ygfTyVoGnX20w6LyMVU1vIszMrnimbOXS%2BGz46oyw07SBZHv"
let SELLIGENT_URL_APITOOL =   "http://mobile-selligent-apitools.proxy.abaenglish.com:10002/optiext/optiextension.dll?ID=NcWN7WrkwnHB0OzVkjJTZ5n1rKl%2BNw0gd0ROZbkqINg4k1z5zPNxVxgatI5enS_GLQqrwbrqcz"



public enum API {
    case login(loginParameters: ApiSignable)
    case loginWithToken(token: String, platform: String)
    case registerWithEmail(registerParameters: ApiSignable)
    case registerWithFacebook(registerParameters: ApiSignable)
    case publicIP
    case selligentProducts(userid: String)
}

extension API: Moya.TargetType {
    public var baseURL: URL {
        switch self {
        case .selligentProducts:
            return URL(string: SELLIGENT_URL_APITOOL)!
        default:
            let shepherdUrl = (ABAShepherdEditor.shared().environment(forShepherdConfigurationClass: ABACoreShepherdConfiguration.classForCoder()) as? ABACoreShepherdEnvironment)!.abaSecureApiUrl
            return URL(string: shepherdUrl!)!
        }
    }

    public var path: String {
        switch self {
        case .login(_):
            return "api/apiuser/login"
        case .loginWithToken(_, _):
            return "api/apiuser/login"
        case .registerWithEmail(_):
            return "register/register"
        case .registerWithFacebook(_):
            return "/register/registerfacebook"
        case .publicIP:
            return "api/abaEnglishApi/iprecognition"
        case .selligentProducts(_):
            return ""
        }
    }

    public var method: Moya.Method {
        switch self {
        case .login(_),
             .loginWithToken(_, _),
             .registerWithEmail(_),
             .registerWithFacebook(_):
            return .post
        case .publicIP,
             .selligentProducts(_):
            return .get
        }

    }

    public var parameters: [String: Any]? {
        switch self {
        case .login(let parameters):
            return parameters.toMap() as [String : AnyObject]?
        case .loginWithToken(_, _):
            return nil
        case .registerWithEmail(let parameters):
            return parameters.toMap() as [String : AnyObject]?
        case .registerWithFacebook(let parameters):
            return parameters.toMap() as [String : AnyObject]?
        case .publicIP:
            return nil
        case .selligentProducts(let userid):
            return ["IDUSER_WEB" : userid]
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    public var sampleData: Data {
        switch self {
        case .login(_),
             .loginWithToken(_, _):
            return kLoginWithEmailSampleData.data(using: String.Encoding.utf8)!
        case .registerWithEmail(_),
             .registerWithFacebook(_),
             .publicIP,
             .selligentProducts(_):
            return "".data(using: String.Encoding.utf8)!
        }
    }
    
    public var task: Task {
        switch self {
        default:
            return .request
        }
    }
    
    public var validate: Bool {
        return false
    }
}

var endpointClosure = { (target: API) -> Moya.Endpoint<API> in
    let url = target.baseURL.appendingPathComponent(target.path).absoluteString
    var endpoint: Moya.Endpoint<API> = Endpoint(url: url, sampleResponseClosure: { .networkResponse(200, target.sampleData) }, method: target.method, parameters: target.parameters)

    switch target {
    case .loginWithToken(let token, let platform):
        let header = ["ABA_API_AUTH_TOKEN": token, "DEVICE": platform] as [String: String]
        return endpoint.adding(httpHeaderFields: header)

    default:
        print("reaching endpoint: \(url)")
        return endpoint
    }
}

let kLoginWithEmailSampleData = "{\"userId\":\"31\",\"userType\":\"2\",\"userLevel\":\"4\",\"token\":\"19d07038b0558da4ebd9c4c3c82b109e\",\"name\":\"Student\",\"surnames\":\"31 A \",\"email\":\"student+es@abaenglish.com\",\"countryNameIso\":\"DO\",\"userLang\":\"es\",\"teacherId\":\"200001\",\"teacherName\":\"Student+200001\",\"teacherImage\":\"https://static.abaenglish.com/images/mobile/ios/iphone/teacher/teacherImage-Graham@2x.png\",\"expirationDate\":\"25-09-2020\",\"idPeriod\":\"\",\"idPartner\":\"300001\",\"idSource\":\"0\",\"gender\":\"Male\",\"birthDate\":\"1981-03-02\",\"phone\":\"555-1234\",\"externalKey\":\"e9c1566723ca5dd878eb15f769139b62\",\"profiles\":[],\"cooladata\":[{\"userScope\":{\"user_id\":\"31\",\"user_expiration_date\":\"2020-09-25 02:00:02\",\"user_user_type\":\"2\",\"user_lang_id\":\"es\",\"user_birthday\":\"1981-03-02 00:00:00\",\"user_level\":\"4\",\"user_country_id\":\"61\",\"user_partner_first_pay_id\":\"300001\",\"user_partner_source_id\":\"300001\",\"user_partner_current_id\":null,\"user_source_id\":\"\",\"user_device_type\":null,\"user_entry_date\":\"2005-03-10 20:09:03\",\"user_product\":\"\",\"user_conversion_date\":\"\"}}]}"
