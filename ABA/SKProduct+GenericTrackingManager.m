//
//  SKProduct+GenericTrackingManager.m
//  ABA
//
//  Created by MBP13 Jesus on 07/01/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import "SKProduct+GenericTrackingManager.h"
#import "ABAPlan+Methods.h"

@implementation SKProduct(GenericTrackingManager)

- (NSDictionary *)trackingProperties
{
    NSMutableDictionary * dictionaryWithProperties = [[NSMutableDictionary alloc] init];
    
    [dictionaryWithProperties setObject:[NSNumber numberWithInteger:[ABAPlan getProductPeriod:self]] forKey:@"Product"];
    
    if (self.price)
    {
        [dictionaryWithProperties setObject:self.price forKey:@"Price"];
    }
    
    if (self.priceLocale)
    {
        NSString *currencyCode = [self.priceLocale objectForKey: NSLocaleCurrencyCode];
        [dictionaryWithProperties setObject:currencyCode forKey:@"Currency"];
    }
    
    return dictionaryWithProperties;
}

@end
