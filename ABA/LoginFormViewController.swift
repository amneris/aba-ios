//
//  LoginFormViewController.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
import SnapKit
import SkyFloatingLabelTextField
import Device
import SVProgressHUD

class LoginFormViewController: UIViewController {
    fileprivate var backgroundImage: UIImage {
        get {
            if (self.traitCollection.horizontalSizeClass == .compact) {
                return UIImage(named: "login_page_landscape")!
            } else {
                return UIImage(named: "login_page")!
            }
        }
    }
    
    fileprivate let backgroundImageView = UIImageView()
    fileprivate let inputEmail = SkyFloatingLabelTextField()
    fileprivate let inputPassword = SkyFloatingLabelTextField()
    fileprivate let buttonLogin = UIButton(type: .custom)
    fileprivate let buttonForgotPassword = UIButton(type: .custom)
    fileprivate let buttonLoginFacebook = UIButton()
    fileprivate let buttonRegister = UIButton()
    fileprivate let imageViewAbaLogo = UIImageView(image: UIImage(named: "aba-logo"))
    fileprivate let buttonShowHidePassword = UIButton(type: .custom)

    // RX
    let disposableBag = DisposeBag()

    // VIPER
    var presenter: LoginFormViewOutput?

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) will not been implemented")
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if Device.type() == .iPad {
            return .all
        }
        else {
            return .portrait
        }
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        configureViews()
        setupConstraints()

        #if SHEPHERD_DEBUG
            let defaults = UserDefaults.standard
            let email = defaults.object(forKey: ABAShepherdLoginPlugin.kLastLoginEmailKey) as? String
            let pass = defaults.object(forKey: ABAShepherdLoginPlugin.kLastLoginPassKey) as? String

            inputEmail.text = email
            inputPassword.text = pass
        #endif

        let emailStream = inputEmail
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.inputEmail.errorMessage = "" })
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)

        let passwordStream = inputPassword
            .rx.text
            .distinctUntilChanged { return $0 == $1 }
            .do(onNext: { [unowned self] _ in self.inputPassword.errorMessage = "" } )
            .debounce(0.5, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)

        Observable
            .combineLatest(emailStream, passwordStream) { [unowned self] (email, password) -> Bool in
                if let presenter = self.presenter {
                    return presenter.isEmailValid(email) && presenter.isPasswordValid(password)
                } else {
                    return false
                }

        }.subscribe(onNext: { [unowned self] formIsComplete in
            self.updateLoginButtonStyle(formIsComplete)
        }).addDisposableTo(disposableBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CLSLogv("Login form will appear", getVaList([]))
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        backgroundImageView.image = backgroundImage
    }
    
}

extension LoginFormViewController: LoginViewInput {

    func renderLoginConfirmation() {
        // Does nothing
    }

    func renderValidationError(_ error: LoginParametersValidation) {
        switch error {
        case .EmptyName:
            print("renderValidationError: error. Validating name and name is not included in the form")
        case .EmptyEmail:
            self.inputEmail.errorMessage = String.localize("regErrorEmailFormat2", comment: "")
        case .WrongEmailFormat:
            self.inputEmail.errorMessage = String.localize("regErrorEmailFormat2", comment: "")
        case .EmptyPassword:
            self.inputPassword.errorMessage = String.localize("regErrorPasswordMin", comment: "")
        case .WrongPasswordFormat:
            self.inputPassword.errorMessage = String.localize("regErrorPasswordMin", comment: "")
        default:
            print("renderValidationError: default")
        }
    }

    func showLoadingIndicator() {
        showAbaLogo()
        SVProgressHUD.show(with: .black)
    }

    func hideLoadingIndicator() {
        SVProgressHUD.dismiss()
    }

    func showEmailPasswordError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("loginError", comment: ""))
        abaAlert?.show()
    }

    func showGenericError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("errorConnection", comment: ""))
        abaAlert?.show()
    }

    func showFacebookError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("errorRegisterFacebook", comment: ""))
        abaAlert?.show()
    }

    func showLanguageError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("loginLanguageError", comment: ""))
        abaAlert?.show()
    }
    
    func showPermissionError() {
        let abaAlert = ABAAlert(alertHelpWithText: String.localize("regErrorFacebookEmailKey", comment: ""))
        abaAlert?.show()
    }
}

extension LoginFormViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        // Hidding aba logo
        hideAbaLogo()
        
        // validating form in filled inputs
        if let safeEmail = inputEmail.text, safeEmail.characters.count > 0 {
            presenter?.validateEmail(safeEmail)
        }
        if let safePass = inputPassword.text, safePass.characters.count > 0 {
            presenter?.validatePassword(safePass)
        }
        
        return true
    }

    // moving along the form fields
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == inputEmail {
            _ = inputPassword.becomeFirstResponder()
        }
        else if textField == inputPassword {
            validateFormAndLogin()
            return true
        }

        return false
    }
}

// Field validation
extension LoginFormViewController {
    func updateLoginButtonStyle(_ isActive: Bool) {
        if isActive {
            buttonLogin.setTitleColor(UIColor.white, for: UIControlState())
            buttonLogin.backgroundColor = RegisterFunnelConstants.colorAquaBlue
        } else {
            buttonLogin.setTitleColor(RegisterFunnelConstants.colorRegisterGrayText, for: UIControlState())
            buttonLogin.backgroundColor = RegisterFunnelConstants.colorRegisterGrayBackground
        }
    }
}

extension LoginFormViewController {

    func configureViews() {

        // View heirarchy

        view.addSubview(backgroundImageView)
        view.addSubview(imageViewAbaLogo)
        view.addSubview(inputEmail)
        view.addSubview(inputPassword)
        view.addSubview(buttonShowHidePassword)
        view.addSubview(buttonLogin)
        view.addSubview(buttonForgotPassword)
        view.addSubview(buttonLoginFacebook)
        view.addSubview(buttonRegister)

        // Style
        backgroundImageView.image = backgroundImage
        backgroundImageView.contentMode = .scaleAspectFill
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showAbaLogo)))
        inputEmail.placeholder = String.localize("funnelABtestRegisterFieldEmail", comment: "")
        inputEmail.title = String.localize("funnelABtestRegisterFieldEmail", comment: "")
        inputEmail.font = RegisterFunnelConstants.formFieldsFont
        inputEmail.tintColor = RegisterFunnelConstants.colorAquaBlue
        inputEmail.textColor = UIColor.white
        inputEmail.lineColor = RegisterFunnelConstants.colorFormGray
        inputEmail.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        inputEmail.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        inputEmail.clearButtonMode = .whileEditing
        inputEmail.autocorrectionType = .no
        inputEmail.autocapitalizationType = .none
        inputEmail.keyboardType = UIKeyboardType.emailAddress
        inputEmail.delegate = self
        inputEmail.returnKeyType = .next

        let clearButton = inputEmail.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white

        inputPassword.placeholder = String.localize("register_funnel_password_field", comment: "")
        inputPassword.title = String.localize("funnelABtestRegisterFieldPassword", comment: "")
        inputPassword.font = RegisterFunnelConstants.formFieldsFont
        inputPassword.tintColor = RegisterFunnelConstants.colorAquaBlue
        inputPassword.textColor = UIColor.white
        inputPassword.lineColor = RegisterFunnelConstants.colorFormGray
        inputPassword.selectedLineColor = RegisterFunnelConstants.colorAquaBlue
        inputPassword.selectedTitleColor = RegisterFunnelConstants.colorAquaBlue
        inputPassword.autocorrectionType = .no
        inputPassword.keyboardType = UIKeyboardType.default
        inputPassword.isSecureTextEntry = true
        inputPassword.delegate = self
        inputPassword.returnKeyType = .join

        buttonShowHidePassword.setImage(UIImage(named: "show_password"), for: UIControlState())
        buttonShowHidePassword.addTarget(self, action: #selector(showHidePasswordAction), for: .touchUpInside)

        buttonLogin.setTitle(String.localize("loginButtonEnter", comment: ""), for: UIControlState())
        buttonLogin.layer.cornerRadius = RegisterFunnelConstants.cornerRadius
        buttonLogin.titleLabel?.font = RegisterFunnelConstants.formButtonsFont
        buttonLogin.addTarget(self, action: #selector(validateFormAndLogin), for: .touchUpInside)
        self.updateLoginButtonStyle(false)

        buttonForgotPassword.setTitle(String.localize("forgotTitleKey", comment: ""), for: UIControlState())
        buttonForgotPassword.setTitleColor(RegisterFunnelConstants.colorAquaBlue, for: UIControlState())
        buttonForgotPassword.addTarget(self, action: #selector(goToForgotPassword), for: .touchUpInside)
        buttonForgotPassword.titleLabel?.font = RegisterFunnelConstants.rememberButtonFont
        buttonForgotPassword.contentMode = .center

        buttonLoginFacebook.setTitle(String.localize("funnelABtestRegisterWithFacebookButton", comment: ""), for: UIControlState())
        buttonLoginFacebook.setTitleColor(UIColor.white, for: UIControlState())
        buttonLoginFacebook.layer.cornerRadius = RegisterFunnelConstants.cornerRadius
        buttonLoginFacebook.backgroundColor = RegisterFunnelConstants.colorRegisterBlueBackground
        buttonLoginFacebook.titleLabel?.font = RegisterFunnelConstants.formButtonsFont
        buttonLoginFacebook.addTarget(self, action: #selector(loginWithFacebookAction), for: .touchUpInside)

        let notRegisteredQuestion = String.localize("registerButtonQuestion", comment: "")
        let registerActionString = String.localize("registerButtonAnswer", comment: "")
        let fullRegisterString = notRegisteredQuestion + " " + registerActionString
        let fullRegisterNSString = fullRegisterString as NSString
        let attributedString = NSMutableAttributedString(string: fullRegisterString)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: RegisterFunnelConstants.colorAquaBlue, range: fullRegisterNSString.range(of: registerActionString))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: fullRegisterNSString.range(of: notRegisteredQuestion))
        attributedString.addAttribute(NSFontAttributeName, value: RegisterFunnelConstants.bottomButtonFont!, range: fullRegisterNSString.range(of: fullRegisterString))

        buttonRegister.setTitle("", for: UIControlState())
        buttonRegister.contentMode = .center
        buttonRegister.titleLabel?.font = RegisterFunnelConstants.bottomButtonFont
        buttonRegister.setTitleColor(UIColor.white, for: UIControlState())
        buttonRegister.setAttributedTitle(attributedString, for: UIControlState())
        buttonRegister.addTarget(self, action: #selector(goToRegister), for: .touchUpInside)
    }

    func setupConstraints() {
        backgroundImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }

        imageViewAbaLogo.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 110, height: 94))
            make.top.equalTo(self.view).offset(RegisterFunnelConstants.logoPadding)
            make.centerX.equalTo(self.view)
        }

        // inputName constaints
        adjustFormToLogo()

        inputPassword.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputEmail.snp.left)
            make.right.equalTo(self.inputEmail.snp.right)
            make.top.equalTo(self.inputEmail).offset(RegisterFunnelConstants.formFieldsPadding)
        }

        buttonShowHidePassword.snp.makeConstraints { (make) in
            make.right.equalTo(inputPassword)
            make.centerY.equalTo(inputPassword)
            make.size.equalTo(CGSize(width:30.0, height: 30.0))
        }

        buttonLogin.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputEmail.snp.left)
            make.right.equalTo(self.inputEmail.snp.right)
            make.top.equalTo(self.inputPassword.snp.bottomMargin).offset(RegisterFunnelConstants.formButtonsTopPadding)
            make.height.equalTo(55)
        }
        
        buttonForgotPassword.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputEmail.snp.left)
            make.right.equalTo(self.inputEmail.snp.right)
            make.top.equalTo(self.buttonLogin.snp.bottomMargin).offset(RegisterFunnelConstants.formButtonsSeparation / 2.0)
        }

        buttonLoginFacebook.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputEmail.snp.left)
            make.right.equalTo(self.inputEmail.snp.right)
            make.top.equalTo(self.buttonForgotPassword.snp.bottomMargin).offset(RegisterFunnelConstants.formButtonsSeparation)
            make.height.equalTo(55)
        }

        buttonRegister.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-10)
            make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
            make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
        }
    }

    // Method to update the form constains so that the form is aligned to the top of the screen
    // using Autolayouts
    func adjustFormToTop() {
        self.inputEmail.snp.remakeConstraints { (make) in
            if RegisterFunnelConstants.isIpadOrIpadScreenSize() {
                make.width.equalTo(RegisterFunnelConstants.formWidthForIpad)
                make.centerX.equalTo(self.view)
            }
            else {
                make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
                make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
            }

            make.top.equalTo(self.view).offset(RegisterFunnelConstants.formTopPadding)
        }
    }

    func adjustFormToLogo() {
        inputEmail.snp.remakeConstraints { (make) in
            if RegisterFunnelConstants.isIpadOrIpadScreenSize() {
                make.width.equalTo(RegisterFunnelConstants.formWidthForIpad)
                make.centerX.equalTo(self.view)
            }
            else {
                make.left.equalTo(self.view).offset(RegisterFunnelConstants.formSidePadding)
                make.right.equalTo(self.view).offset(-RegisterFunnelConstants.formSidePadding)
            }

            make.top.equalTo(imageViewAbaLogo.snp.bottomMargin).offset(RegisterFunnelConstants.formTopPadding)
        }
    }
}

// Private methods
extension LoginFormViewController {

    func hideAbaLogo() {
        // Hiding aba logo
        UIView.animate(withDuration: 0.5,
            animations: {
                self.imageViewAbaLogo.alpha = 0.0
            },
            // When finished, moving form to the top
            completion: { completed in
                if completed {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.adjustFormToTop()
                        self.view.layoutIfNeeded()
                    })
                }
        })
    }
    func showAbaLogo() {
        dismissKeyboard()
        UIView.animate(withDuration: 0.2,
            animations: {
                self.adjustFormToLogo()
                self.view.layoutIfNeeded()
            },
            // When finished, moving form to the top
            completion: { completed in
                if completed {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.imageViewAbaLogo.alpha = 1.0
                    })
                }
        })
    }

    func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }

    func validateFormAndLogin() {
        self.presenter?.loginWithEmailAndPassword(inputEmail.text!, password: inputPassword.text!)
    }

    func loginWithFacebookAction() {
        self.presenter?.loginUserWithFacebook()
    }

    func showHidePasswordAction() {
        if inputPassword.isSecureTextEntry {
            inputPassword.isSecureTextEntry = false
            inputPassword.font = nil
            inputPassword.font = RegisterFunnelConstants.formFieldsFont
            buttonShowHidePassword.setImage(UIImage(named: "hide_password"), for: UIControlState())
        } else {
            inputPassword.isSecureTextEntry = true
            inputPassword.font = nil
            inputPassword.font = RegisterFunnelConstants.formFieldsFont
            buttonShowHidePassword.setImage(UIImage(named: "show_password"), for: UIControlState())
        }
    }

    func goToRegister() {
        self.presenter?.goToRegister()
    }

    func goToForgotPassword() {
        self.presenter?.goToForgotPassword()
    }

}
