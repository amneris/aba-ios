//
//  TrackingManager.h
//  ABA
//
//  Created by Ivan Ferrera on 27/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "FacebookParserTracker.h"

@interface LegacyTrackingManager : NSObject<FacebookParserTracker>
{
    NSDictionary *_adJustEvents;
}

+ (instancetype)sharedManager;

/**
 Método para trackear una pageview con las distintas plataformas de tracking que lo soporten
 @param pageView el nombre de la PageView
 */
- (void)trackPageView:(NSString *)pageView;

/**
 Método para trackear una transaction con las distintas plataformas de tracking
 @param transaction Objeto que contiene información de la transacción
 @param period 
 @param product
 */
- (void)trackTransaction:(SKPaymentTransaction *)transaction withPeriod:(NSInteger)period withProduct:(SKProduct *)product;

/**
 Método que trackea el login del usuario con las distintas plataformas de tracking
 */
- (void)trackLogin;

/**
 Método que trackea el registro con las distintas plataformas de tracking
 */
- (void)trackRegister;

/**
 *  Método que trackea una interacción con el curso (progreso) con las distintas plataformas de tracking
 */
- (void)trackInteraction;

/**
 Method to track errors shown to the user
 */
- (void)trackAlertErrorWithText:(NSString *)errorShown andDelay:(CGFloat)delay withButtonText:(NSString *)firstButtonText withButtonText:(NSString *)secondButtonText;


- (void)startSession;
- (void)stopSession;

// FacebookParserTracker methods
//- (void)trackFacebookParsingError;
//- (void)trackFacebookEmailError;
//- (void)trackFacebookEmailSuccess;

@end
