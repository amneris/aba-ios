//
//  CertificateCell.h
//  ABA
//
//  Created by Jordi Mele on 10/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CertificateCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *certificateImage;
@property (weak, nonatomic) IBOutlet UILabel *certificateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *certificateSubTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *progressBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarWidthConstraint;
@property (weak, nonatomic) IBOutlet UIView *progressBarBackground;

@end
