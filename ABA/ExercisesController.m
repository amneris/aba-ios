//
//  ExercisesController.m
//  ABA
//
//  Created by Marc Güell Segarra on 5/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ExercisesController.h"
#import "ABAExercises.h"
#import "ABAExercisesGroup.h"
#import "ABAExercisesQuestion.h"
#import "ABAExercisesPhrase.h"
#import "ABAExercisePhraseItem.h"
#import "PhrasesController.h"
#import "ProgressController.h"
#import "UserController.h"

@implementation ExercisesController

-(ABAExercisesQuestion *)getCurrentABAExercisesQuestionForABAExercises: (ABAExercises *)abaExercises
{
	for(ABAExercisesGroup *group in abaExercises.exercisesGroups) {
		for(ABAExercisesQuestion *exerciseQuestion in group.questions)
		{
			NSArray *blankPhrases = [self getBlankPhrasesForExerciseQuestion: exerciseQuestion forExercisesSection:abaExercises];
			
			for(ABAExercisesPhrase *phrase in blankPhrases)
			{
				if(![phrase.done boolValue])
				{
					return phrase.exercisesQuestion;
				}
			}
		}
	}
	
    return nil;
}

-(NSArray *)getPossibleAnswersForExercisesGroup:(ABAExercisesGroup *)exerciseGroup
{
    NSMutableArray *possibleAnswers = [[NSMutableArray alloc] init];
    
    for(ABAExercisesQuestion *exerciseQuestion in exerciseGroup.questions)
    {
        for(ABAExercisesPhrase *phrase in exerciseQuestion.phrases)
        {
            if(![phrase.blank isEqualToString:@""] &&
               ![possibleAnswers containsObject:phrase.blank])
            {
                [possibleAnswers addObject:phrase.blank];
            }
        }
    }
    
    return possibleAnswers;
}

-(void)setBlankPhrasesDoneForExercisesQuestion:  (ABAExercisesQuestion *)exercisesQuestion
                                forABAExercises: (ABAExercises *)abaExercises
                                completionBlock: (CompletionBlock)completionBlock
{
    NSArray *blankPhrases = [self getBlankPhrasesForExerciseQuestion: exercisesQuestion forExercisesSection:abaExercises];
    PhrasesController *phrasesController = [[PhrasesController alloc] init];
    
    __block NSUInteger phrasesSettedDone = 0;
    __weak typeof (self) weakSelf = self;

    for(ABAExercisesPhrase *phrase in blankPhrases)
    {
        
        [phrasesController setPhrasesDone:@[phrase] forSection:abaExercises sendProgressUpdate:YES completionBlock:^(NSError *error, id object)
        {
            if(!error)
            {
                phrasesSettedDone += 1;
                
                if(phrasesSettedDone == [blankPhrases count])
                {
                    [weakSelf setExercisesQuestionsDone:@[exercisesQuestion] forABAExercises:abaExercises completionBlock:^(NSError *error, id object)
                    {
                        if(!error)
                        {
                            completionBlock(nil, object);
                        }
                        else
                        {
                            completionBlock(error, nil);
                        }
                    }];
                }
            }
            else
            {
                completionBlock(error, nil);
            }
        }];
    }
    
}

- (void)setExercisesQuestionsDone:(NSArray *)exercisesQuestions forABAExercises:(ABAExercises *)abaExercises completionBlock:(CompletionBlock)completionBlock
{
    NSMutableArray *doneExercicesGroup = @[].mutableCopy;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;

    [realm beginWriteTransaction];

    for (ABAExercisesQuestion *exercisesQuestion in exercisesQuestions)
    {
        exercisesQuestion.completed = [NSNumber numberWithInt:1];
        
        if ([self allQuestionsAreCompletedForExercisesGroup:exercisesQuestion.exercisesGroup
                                        forExercisesSection:abaExercises])
        {
            [doneExercicesGroup addObject:exercisesQuestion.exercisesGroup];
        }
    }

    [realm commitWriteTransaction:&error];

    if (error)
    {
        completionBlock(error, nil);
        return;
    }

    if (doneExercicesGroup.count)
    {
        [self setExercisesGroupsDone:doneExercicesGroup.copy
                     forABAExercises:abaExercises
                     completionBlock:^(NSError *error, id object) {

                         completionBlock(error, object);
                     }];
    }
    else
    {
        completionBlock(nil, abaExercises);
    }
}

- (void)setExercisesGroupsDone:(NSArray *)exercisesGroups forABAExercises:(ABAExercises *)abaExercises completionBlock:(CompletionBlock)completionBlock
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;

    [realm beginWriteTransaction];

    for (ABAExercisesGroup *exercisesGroup in exercisesGroups)
    {
        exercisesGroup.completed = [NSNumber numberWithInt:1];
    }

    [realm commitWriteTransaction:&error];

    if (error)
    {
        completionBlock(error, nil);
        return;
    }

    if ([self allGroupsAreCompletedForExercisesSection:abaExercises])
    {
        [self setCompletedSection:abaExercises
                  completionBlock:^(NSError *error, id object) {
                      if (!error)
                      {
                          completionBlock(nil, abaExercises);
                      }
                      else
                      {
                          completionBlock(error, nil);
                      }
                  }];
    }
    else
    {
        completionBlock(nil, abaExercises);
    }
}

-(BOOL)allQuestionsAreCompletedForExercisesGroup: (ABAExercisesGroup *)exercisesGroup
                                forExercisesSection: (ABAExercises *)abaExercises
{
    for(ABAExercisesQuestion *question in exercisesGroup.questions)
    {
        if(![question.completed isEqual:[NSNumber numberWithInt:1]])
        {
            return NO;
        }
    }
    
    return YES;
}

-(BOOL)allGroupsAreCompletedForExercisesSection: (ABAExercises *)abaExercises
{
    for(ABAExercisesGroup *group in abaExercises.exercisesGroups)
    {
        if(![group.completed isEqual:[NSNumber numberWithInt:1]])
        {
            return NO;
        }
    }
    
    return YES;
}

-(NSArray*) getPhrasesFromExercises: (ABAExercises*) exercises {
    NSMutableArray* phrases = [NSMutableArray new];
    
    for(ABAExercisesGroup *group in exercises.exercisesGroups)
    {
        for(ABAExercisesQuestion *question in group.questions)
        {
            for (ABAExercisesPhrase *phrase in question.phrases) {
                [phrases addObject:phrase];
            }
        }
    }
    
    return phrases;
}

-(CGFloat) getTotalElementsForSection: (ABASection*) section
{
    ABAExercises *abaExercises = (ABAExercises *)section;

	NSUInteger total = 0;
	
	for(ABAExercisesGroup *group in abaExercises.exercisesGroups)
	{
        total = total + [group.questions count];
	}
	
	return total;
}

-(CGFloat) getElementsCompletedForSection: (ABASection*) section
{
    ABAExercises *abaExercises = (ABAExercises *)section;
	
	return [self getNumberOfCompletedExercisesForSection:abaExercises];
}

-(int) getNumberOfCompletedExercisesForSection: (ABASection*) section
{
    ABAExercises *abaExercises = (ABAExercises *)section;
    
    int numberDone = 0;
    
    for(ABAExercisesGroup *group in abaExercises.exercisesGroups)
    {
        for(ABAExercisesQuestion *question in group.questions)
        {
			if([question.completed boolValue])
				numberDone++;
        }
    }
    return numberDone;
}

-(BOOL) isSectionCompleted: (ABASection*) section
{
    ABAExercises *abaExercises = (ABAExercises *)section;
    
    if([self getNumberOfGroupsCompletedForExercisesSection:abaExercises] == [abaExercises.exercisesGroups count])
    {
        return YES;
    }
    
    return NO;
}

-(NSUInteger)getNumberOfGroupsCompletedForExercisesSection: (ABAExercises *)abaExercises
{
    NSUInteger numberOfGroupsCompleted = 0;
    
    for(ABAExercisesGroup *group in abaExercises.exercisesGroups)
    {
        if([group.completed isEqual:[NSNumber numberWithInt:1]])
        {
            numberOfGroupsCompleted++;
        }
    }
    
    return numberOfGroupsCompleted;
}

-(NSArray *)getExercisesItemsForExerciseQuestion: (ABAExercisesQuestion *)exercisesQuestion
                             forExercisesSection: (ABAExercises *)abaExercises
{
    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
    NSUInteger index = 0;
    
    for(ABAExercisesPhrase *phrase in exercisesQuestion.phrases)
    {
        if(![phrase.blank isEqualToString:@""])
        {
            ABAExercisePhraseItem *phraseItem = [[ABAExercisePhraseItem alloc] init];
            phraseItem.type = kABAExercisePhraseItemTypeBlank;
            phraseItem.text = phrase.blank;
            phraseItem.audioFile = phrase.audioFile;
            phraseItem.idPhrase = phrase.idPhrase;
			phraseItem.page = phrase.page;
			
            [itemsArray insertObject:phraseItem atIndex:index];
            index++;
        }
        else
        {
            NSArray *wordsArray = [phrase.text componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
            
            for(NSString *word in wordsArray)
            {
                ABAExercisePhraseItem *phraseItem = [[ABAExercisePhraseItem alloc] init];
                phraseItem.type = kABAExercisePhraseItemTypeNormal;
                phraseItem.text = [NSString stringWithFormat:@"%@ ", word];
                phraseItem.audioFile = phrase.audioFile;
                phraseItem.idPhrase = phrase.idPhrase;
				phraseItem.page = phrase.page;

                [itemsArray insertObject:phraseItem atIndex:index];
                index++;
            }
        }
    }
    
    return itemsArray;
}



-(NSArray *)getBlankPhrasesForExerciseQuestion: (ABAExercisesQuestion *)exercisesQuestion
                           forExercisesSection: (ABAExercises *)abaExercises
{
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];

    for(ABAExercisesPhrase *phrase in exercisesQuestion.phrases)
    {
        if(![phrase.blank isEqualToString:@""])
        {
            [returnedArray addObject:phrase];
        }
    }
    
    return returnedArray;
}

-(NSArray *)getAllAudioIDsForExercisesSection: (ABAExercises *)exercises 
{
    NSMutableArray *returnedArray = [[NSMutableArray alloc] init];
    
    for(ABAExercisesGroup *group in exercises.exercisesGroups)
    {
        for(ABAExercisesQuestion *question in group.questions)
        {
            NSArray *blankPhrases = [self getBlankPhrasesForExerciseQuestion:question forExercisesSection:exercises];
            
            for(ABAExercisesPhrase *phrase in blankPhrases)
            {
                [returnedArray addObject:phrase.audioFile];
            }
        }

    }
    
    return returnedArray;
}

-(ABAPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onExercises: (ABAExercises*) exercises {
    
    NSArray *phrases = [self getPhrasesFromExercises:exercises];
    for(ABAExercisesPhrase* phrase in phrases) {
        
        if([phrase.idPhrase isEqualToString:audioID] && [phrase.page isEqualToString:page])
            return phrase;
    }
    return nil;
    
}

- (void)syncCompletedActions:(NSArray *)actions withSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    NSMutableArray *actionsArray = @[].mutableCopy;

    for (NSDictionary *action in actions)
    {
        if ([[action objectForKey:@"Action"] isEqualToString:@"WRITTEN"] &&
            [[action objectForKey:@"IncorrectText"] isKindOfClass:[NSNull class]])
        {
            ABAPhrase *phrase = [self phraseWithID:[action objectForKey:@"Audio"]
                                           andPage:[action objectForKey:@"Page"]
                                       onExercises:(ABAExercises *)section];

            NSLog(@"PHRASE : %@", phrase.audioFile);

            if (phrase == nil)
            {
                NSLog(@"ACTION NOT FOUND: %@", action);
                continue;
            }

            [actionsArray addObject:phrase];
        }
    }
    
    if (actionsArray.count)
    {
        [phrasesController setPhrasesDone:actionsArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object) {
                              
                              [self checkTextPhrases:actionsArray.copy forSection:section];
                              
                              if ([self isSectionCompleted:section] && (section.completed == nil || section.completed == [NSNumber numberWithBool:NO]))
                              {
                                  [self setCompletedSection:section completionBlock:^(NSError *error, id object) {
                                      
                                  }];
                              }
                          }];
    }
}

- (void)checkTextPhrases:(NSArray *)phrases forSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    ABAExercises *abaExercises = (ABAExercises *)section;
    NSMutableArray *doneBlankPhrasesArray = @[].mutableCopy;
    NSMutableArray *doneExercisesQuestionArray = @[].mutableCopy;

    for (ABAPhrase *phrase in phrases)
    {
        for (ABAExercisesGroup *exerciseGroup in abaExercises.exercisesGroups)
        {
            for (ABAExercisesQuestion *exerciseQuestion in exerciseGroup.questions)
            {
                NSArray *blankPhrases = [self getBlankPhrasesForExerciseQuestion:exerciseQuestion forExercisesSection:abaExercises];
                NSInteger totalPhrases = 0;

                NSMutableArray *phrasesDone = [NSMutableArray arrayWithCapacity:[blankPhrases count]];

                for (ABAExercisesPhrase *blankPhrase in blankPhrases)
                {
                    if ([blankPhrase.idPhrase isEqualToString:phrase.idPhrase])
                    {
                        totalPhrases++;

                        [phrasesDone addObject:blankPhrase];
                    }
                }
                
                if (totalPhrases == blankPhrases.count)
                {
                    [doneExercisesQuestionArray addObject:exerciseQuestion];
                }
            }
        }
    }

    if (doneBlankPhrasesArray.count)
    {
        [phrasesController setPhrasesDone:doneBlankPhrasesArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object){
                          }];
    }

    if (doneExercisesQuestionArray.count)
    {
        [self setExercisesQuestionsDone:doneExercisesQuestionArray.copy
                        forABAExercises:abaExercises
                        completionBlock:^(NSError *error, id object){
                        }];
    }
}

@end

