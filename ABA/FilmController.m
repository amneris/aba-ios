//
//  FilmController.m
//  ABA
//
//  Created by Jaume Cornadó Panadés on 19/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "FilmController.h"
#import "DataController.h"
#import "APIManager.h"
#import "ProgressController.h"
#import "ABAFilm.h"
#import "ABAVideoClass.h"
#import "ABAUnit.h"

@implementation FilmController

- (void)setCompletedSection:(ABASection *)section completionBlock:(CompletionBlock)completionBlock
{
    [DataController setCompletedSection:section completionBlock:^(NSError *error, id object) {
        
        if (!error)
        {
            [DataController saveProgressActionForSection:section andUnit:nil andPhrases:nil errorText:nil isListen: NO isHelp: NO completionBlock:^(NSError *error, id object) {
                
                if (!error)
                {
                    completionBlock(error, section);
                }
                else
                {
                    completionBlock(error, nil);
                }
            }];
        }
        else
        {
            completionBlock(error, nil);
        }
    }];
}

- (BOOL)videoAndSubtituleDownloaded:(ABASection *)section forSectionType:(kABASectionTypes)sectionType
{
    if (![self checkIfVideoExists:section forSectionType:sectionType])
    {
        return NO;
    }
    else
    {
        if (![self checkIfSubtitleExists:section forSectionType:sectionType forSubtitle:kABASubtitleTypeEnglish]) {
            return NO;
        }
        else if (![self checkIfSubtitleExists:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate]) {
            return NO;
        }
    }
    
    return YES;
}

- (void)downloadVideo:(ABASection *)section forSectionType:(kABASectionTypes)sectionType completionBlock:(CompletionBlock)completionBlock
{
    if (![self checkIfVideoExists:section forSectionType:sectionType])
    {
        [[APIManager sharedManager] downloadFile:[self getVideoFileURL:section forSectionType:sectionType] toFileName:[self getLocalVideoPath:section forSectionType:sectionType] completionBlock:^(NSError *error, id object) {
            if (!error)
            {
                completionBlock(nil, nil);
            }
            else
            {
                completionBlock(error, nil);
            }
        }];
    }
    else
    {
        NSLog(@"Video exist");
        completionBlock(nil, nil);
    }
}

- (BOOL)checkIfVideoExists:(ABASection *)section forSectionType:(kABASectionTypes)sectionType
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self getLocalVideoPath:section forSectionType: sectionType]];
}

- (NSString *)getLocalVideoPath:(ABASection *)section forSectionType:(kABASectionTypes)sectionType
{
    NSString *fileName, *unitFolder;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    if (sectionType == kABAFilmSectionType)
    {
        ABAFilm *film = (ABAFilm *)section;
        fileName = @"film.mp4";
        
        unitFolder = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"unit%@", film.unit.idUnit]];
    }
    else if (sectionType == kABAVideoClassSectionType)
    {
        ABAVideoClass *videoClass = (ABAVideoClass *)section;
        fileName = @"videoClass.mp4";
        
        unitFolder = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"unit%@", videoClass.unit.idUnit]];
    }
    
    BOOL isDir = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:unitFolder isDirectory:&isDir])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:unitFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *path = [unitFolder stringByAppendingPathComponent:fileName];
    return path;
}

-(NSURL *)getVideoFileURL:(ABASection *)section forSectionType:(kABASectionTypes)sectionType
{
    NSString * urlToDownload;
    if (sectionType == kABAFilmSectionType)
    {
        ABAFilm *film = (ABAFilm *)section;
        
        if ([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWiFi)
        {
            urlToDownload = film.hdVideoURL;
        }else{
            urlToDownload = film.sdVideoURL;
        }
    }
    else if (sectionType == kABAVideoClassSectionType)
    {
        ABAVideoClass *videoClass = (ABAVideoClass *)section;
        
        if ([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusReachableViaWiFi)
        {
            urlToDownload = videoClass.hdVideoURL;
        }else{
            urlToDownload = videoClass.sdVideoURL;
        }
    }
    
    NSURL *audioURL = [NSURL URLWithString:urlToDownload];
    return audioURL;
}

- (void)downloadSubtitlesVideo:(ABASection *)section forSectionType:(kABASectionTypes)sectionType completionBlock:(CompletionBlock)completionBlock
{
    if (![self checkIfSubtitleExists:section forSectionType:sectionType forSubtitle:kABASubtitleTypeEnglish]) {
        
        [[APIManager sharedManager] downloadFile:[self getSubtitleFileURL:section forSectionType:sectionType forSubtitle:kABASubtitleTypeEnglish] toFileName:[self getLocalSubtitlePath:section forSectionType:sectionType forSubtitle:kABASubtitleTypeEnglish] completionBlock:^(NSError *error, id object) {
            if (!error)
            {
                if (![self checkIfSubtitleExists:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate])
                {
                    [[APIManager sharedManager] downloadFile:[self getSubtitleFileURL:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate] toFileName:[self getLocalSubtitlePath:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate] completionBlock:^(NSError *error, id object)
                     {
                         if (!error) {
                             completionBlock(nil, nil);
                         }
                         else {
                             completionBlock(error, nil);
                         }
                     }];
                    
                }
                else {
                    NSLog(@"Subtitle translate exist");
                    completionBlock(nil, nil);
                }
                
            }
            else
            {
                completionBlock(error, nil);
            }
        }];
        
    }
    else
    {
        NSLog(@"Subtitle english exist");
        if (![self checkIfSubtitleExists:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate]) {
            
            [[APIManager sharedManager] downloadFile:[self getSubtitleFileURL:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate] toFileName:[self getLocalSubtitlePath:section forSectionType:sectionType forSubtitle:kABASubtitleTypeTranslate] completionBlock:^(NSError *error, id object) {
                if (!error)
                {
                    completionBlock(nil, nil);
                }
                else
                {
                    completionBlock(error, nil);
                }
            }];
            
        }
        else
        {
            NSLog(@"Subtitle translate exist");
            completionBlock(nil, nil);
        }
    }
    
}

- (BOOL)checkIfSubtitleExists:(ABASection *)section forSectionType:(kABASectionTypes)sectionType forSubtitle:(kABAVideoSubtitleType) subtitleType
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self getLocalSubtitlePath:section forSectionType: sectionType forSubtitle:subtitleType]];
}

- (NSString *)getLocalSubtitlePath:(ABASection *)section forSectionType:(kABASectionTypes)sectionType forSubtitle:(kABAVideoSubtitleType) subtitleType {
    
    NSString *fileName, *unitFolder;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    if (sectionType == kABAFilmSectionType)
    {
        ABAFilm *film = (ABAFilm *)section;
        
        if (subtitleType == kABASubtitleTypeEnglish)
        {
            fileName = @"ABAFilmEnglish.srt";
        }
        else
        {
            fileName = @"ABAFilmTranslate.srt";
        }
        
        unitFolder = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"unit%@", film.unit.idUnit]];
    }
    else if (sectionType == kABAVideoClassSectionType)
    {
        ABAVideoClass *videoClass = (ABAVideoClass *)section;
        
        if (subtitleType == kABASubtitleTypeEnglish)
        {
            fileName = @"ABAVideoClassEnglish.srt";
        }
        else
        {
            fileName = @"ABAVideoClassTranslate.srt";
        }
        
        unitFolder = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"unit%@", videoClass.unit.idUnit]];
    }
    
    BOOL isDir = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:unitFolder isDirectory:&isDir])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:unitFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *path = [unitFolder stringByAppendingPathComponent:fileName];
    return path;
}

- (NSURL *)getSubtitleFileURL:(ABASection *)section forSectionType:(kABASectionTypes)sectionType forSubtitle:(kABAVideoSubtitleType)subtitleType
{
    NSString *urlToDownload;
    if (sectionType == kABAFilmSectionType)
    {
        ABAFilm *film = (ABAFilm *)section;
        
        if (subtitleType == kABASubtitleTypeEnglish)
        {
            urlToDownload = film.englishSubtitles;
        }
        else
        {
            urlToDownload = film.translatedSubtitles;
        }
        
    }
    else if (sectionType == kABAVideoClassSectionType)
    {
        ABAVideoClass *videoClass = (ABAVideoClass *)section;
        
        if (subtitleType == kABASubtitleTypeEnglish)
        {
            urlToDownload = videoClass.englishSubtitles;
        }
        else
        {
            urlToDownload = videoClass.translatedSubtitles;
        }
    }
    
    NSURL *audioURL = [NSURL URLWithString:urlToDownload];
    return audioURL;
}

@end
