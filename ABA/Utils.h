//
//  Utils.h
//  ABA
//
//  Created by Oriol Vilaró on 13/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//
#import "LanguageController.h"

#ifndef ABA_Utils_h
#define ABA_Utils_h

//#warning TODO : Sacar el comentario y dejar la función correcta
#define STRTRAD(key, comment) NSLocalizedStringFromTable(key, [LanguageController getCurrentLanguage], comment)
//#define STRTRAD(key, comment) comment

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

// Registration
#define oldToken @"token"
#define oldEmail @"mail"
#define newregister @"newRegister"

// APNS
#define kApnsConfigurationStateKey @"apnsConfigurationStateKey"
#define kApnsEnabled @"1"
#define kApnsDisabled @"0"

// Mobile Downloads
#define kMobileDownloadsStateKey @"mobileDownloadsStateKey"

#define kUserFirstUnitCompletedFacebookPost @"userFirstUnitCompletedFacebookPost"

// Languages
#define kCurrentLanguageKey @"currentLanguageKey"
#define kSuportedLanguagesArray @[@"de", @"es", @"fr", @"en", @"it", @"pt", @"ru", @"zh"]
#define kDefaultLanguage @"en"

#define idConversionAddWords @"956885678"

#define partneridIphone @"400004"
#define partneridIpad @"400005"

/// Test helpers
#define TEST_USER_PREMIUM @"testuser@abaenglish.com"
#define TEST_PASSWORD_PREMIUM @"qpqpqp"

#define TEST_USER_NORMAL @"test24@test.com"
#define TEST_PASSWORD_NORMAL @"123456"

#define TEST_USER_REALM_MIGRATION @"migration.realm.free@test.com"
#define TEST_PASSWORD_REALM_MIGRATION @"abaenglish"

#define TEST_USER_REALM_MIGRATION_PREMIUM @"migration.realm.premium@test.com"
#define TEST_PASSWORD_REALM_MIGRATION_PREMIUM @"abaenglish"

#define WS_TIMEOUT 10000

// Important HTTP response codes
#define CODE_CONNECTION_ERROR -1005 // just like AFNerworking connection lost
#define CODE_LANGUAGE_ERROR 900
#define CODE_GENERIC_ERROR 901
#define CODE_WRONG_LOGIN_PASS_ERROR 902
#define CODE_EMAIL_EXIST_ERROR 444
#define CODE_HANDLED_SERVER_SEGMENT_ERROR_400 400
#define CODE_UNHANDLED_SERVER_SEGMENT_ERROR_500 500

//#define AppStoreURL @"https://itunes.apple.com/us/app/aba-english-aprender-ingles/id952651403?ls=1&mt=8"

#define AppStoreURL @"https://itunes.apple.com/us/app/aba-english-aprender-ingles/id859243872?ls=1&mt=8"

#define AppShareTwIos [[NSURL alloc] initWithString:@"https://app.adjust.com/xai1hs"]

#define AppSjareFbIos [[NSURL alloc] initWithString:@"https://app.adjust.com/ltrrj2"]

// detect test target
#define isTargetTest [[[NSProcessInfo processInfo] environment] objectForKey:@"TEST"]

#define OptionsHasValue(options, value) (((options) & (value)) == (value))

#endif
