//
//  SpeakViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SpeakViewController.h"
#import "AudioController.h"
#import "Resources.h"
#import "Utils.h"
#import "UIViewController+ABA.h"
#import "SpeakDialogCell.h"
#import "ABAAlert.h"
#import "ABASpeak.h"
#import "ABASpeakPhrase.h"
#import "ABASpeakDialogPhrase.h"
#import "ABASpeakDialog.h"
#import "SectionController.h"
#import "SpeakController.h"
#import "PhrasesController.h"
#import "NSAttributedString+Height.h"
#import "ABAActionButtonView.h"
#import "ABATeacherTipView.h"
#import "ABARole.h"
#import "ImageHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LegacyTrackingManager.h"
#import "DataController.h"
#import "PureLayout.h"
#import "APIManager.h"
#import "ABA-Swift.h"
#import <Crashlytics/Crashlytics.h>

static NSString *cellDialogSpeak = @"cellDialogTeacher";
static NSString *cellDialogSpeak_iPad = @"cellDialogTeacher_iPad";
static NSString *cellDialogPhase = @"cellDialogPhase";
static NSString *cellDialogTranslate = @"cellDialogTranslate";
static NSString *cellSampleSpeak = @"cellSampleSpeak";
static NSString *cellFooter = @"cellFooter";

static float minTeacherDialogHeight = 72.0f;
static float minTeacherDialogHeight_iPad = 160.0f;
static float separationHeight = 5.0f;
static NSString *separation = @"Separation";

@interface SpeakViewController () <UITableViewDataSource, UITableViewDelegate,  AudioControllerDelegate>

@property (nonatomic, strong) AudioController *audioController;
@property (nonatomic, strong) SectionController *sectionController;
@property (nonatomic, strong) SpeakController *speakController;
@property (nonatomic, strong) PhrasesController *phrasesController;

@property NSMutableArray *dataSource;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) ABAControlView *controlView;

@property ABAAlert *abaAlert;
@property ABATeacherTipView *abaTeacher;
@property UITapGestureRecognizer *singleTap;

@property (nonatomic, strong) ABASpeakPhrase *selectedPhrase;
@property (nonatomic, strong) ABASpeakDialog *selectedDialog;

@property BOOL selectionDisabled;

@property BOOL sectionCompleted;

@end

@implementation SpeakViewController
@dynamic section;

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    [self setupConstants];
    
    _audioController = [AudioController new];
    _audioController.audioControllerDelegate = self;
    _speakController = [[SpeakController alloc] init];
    _sectionController = [[SectionController alloc] init];
    _phrasesController = [PhrasesController new];

    [self addBackButtonNavBar];
    
    _currentStatus = kABASpeakListen;
    
    [self configTitle];
	
    if ([_speakController isSectionCompleted:self.section]) {
        _selectedPhrase = nil;
        _selectedDialog = nil;
    }
    else {
        _selectedPhrase = [self getCurrentPhrase];
        _selectedDialog = [self getCurrentDialog];
    }
    if (_selectedDialog.dialog.count == [_speakController getTotalPhraseCompletedDialogForSection:_selectedDialog]) {
        _selectedDialog = nil;
    }

    _tableView.hidden = YES;
    
    [self createDataSource];
    
    if ([_speakController isSectionCompleted:self.section]) {
        _sectionCompleted = YES;
    }
	
    [DataController saveProgressActionForSection:self.section andUnit:[self.section getUnitFromSection] andPhrases:nil errorText:nil isListen:NO isHelp: NO completionBlock:^(NSError *error, id object) {
        
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"speaksection"];
	
	[self updateContentInset];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupControlView];
    
    if (![self.view.subviews containsObject:_abaTeacher] && [[_speakController getPercentageForSection:self.section] isEqualToString:@"0%"])
    {
        _abaTeacher = [[ABATeacherTipView alloc]
                       initTeacherWithText:STRTRAD(@"sectioSpeakTeacherKey", @"Vas a escuchar una conversación en inglés, para habituarte tu oido al día a día")  withImage:@"teacher" withShadow:YES];
        
        [self.view addSubview:_abaTeacher];
        [_abaTeacher setConstraints];
        
        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTeacherView)];
        [self.view addGestureRecognizer:_singleTap];
    }
    
    [self.view layoutIfNeeded];
    
    // Checking microphone status
    __weak typeof(self) weakSelf = self;
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
         if (!granted)
         {
             weakSelf.abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"noMicrophoneTextKey", @"Imposible grabar sonido :-(")];
             [weakSelf.abaAlert showWithOffset:44 andDissmisAfter:2000]; // showing error 2000 seconds (trying to show the error permanently)
             weakSelf.controlView.alpha = 0.5;
             weakSelf.view.userInteractionEnabled = NO;
         }
     }];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationWillResignActiveNotification
                                               object: nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(showCompletedAlertIfNeeded)
												 name:@"showCompletedAlertIfNeeded"
											   object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _audioController.audioControllerDelegate = nil;
    
    if ([_audioController.recorder isRecording]) {
        [_audioController stopRecording];
    }
    
    if ([_audioController.player isPlaying]) {
        [[_audioController player] stop];
    }
    
    if (_abaAlert) {
        [_abaAlert dismiss];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.tableView reloadData];
    });
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
}

#pragma mark - Private methods

- (void)updateContentInset
{
	[_tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, _controlView.frame.size.height, 0)];
	
	if ([self.view.subviews containsObject:_abaTeacher]) {
		
		_tableView.hidden = NO;
		
		[_tableView setContentInset:UIEdgeInsetsMake(_tableView.frame.size.height, 0, - _controlView.frame.size.height, 0)];
		_tableView.contentOffset = CGPointMake(0, -_tableView.frame.size.height);
		
		[UIView animateWithDuration:0.8
							  delay:0.0
			 usingSpringWithDamping:0.6
			  initialSpringVelocity:0.7
							options:UIViewAnimationOptionCurveEaseIn
						 animations:^{
							 
							 _tableView.contentOffset = CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]);
							 [_tableView setContentInset:UIEdgeInsetsMake(_tableView.frame.size.height - _controlView.frame.size.height -[self calculateHeightFirstCell], 0, _controlView.frame.size.height, 0)];
							 
						 }
						 completion:NULL];
		
	}
	else {
		[_tableView setContentInset:UIEdgeInsetsMake(_tableView.frame.size.height - _controlView.frame.size.height -[self calculateHeightFirstCell], 0, _controlView.frame.size.height, 0)];
		
		[_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:NO];
		
		_tableView.hidden = NO;
	}
}

- (void)createDataSource {
    
    _dataSource = [[NSMutableArray alloc] init];
    
    for(ABASpeakDialog *dialog in self.section.content) {
        
        [_dataSource addObject:dialog];

        if ([_speakController isDialogCompleted:dialog]) {
            [_dataSource addObject:separation];
        }
        else {
            break;
        }
    }
    
    [_tableView reloadData];
    
}

- (float)calculateHeightFirstCell {
    
    ABASpeakDialog *dialog = [self.section.content objectAtIndex:0];
    ABASpeakDialogPhrase *cellDialogPhrase = [dialog.dialog objectAtIndex:0];
    
    UILabel *virtualLabel = [[UILabel alloc] init];
    
    [self configDialogLabel:virtualLabel withPhrase:cellDialogPhrase];
    
    CGFloat leftDistanceLabel, rightDistanceLabel, minDistance, virtualHeight;
    leftDistanceLabel = 75;
    rightDistanceLabel = 10;
	minDistance = IS_IPAD?minTeacherDialogHeight_iPad:minTeacherDialogHeight;
    
    virtualLabel.numberOfLines = 0;
    virtualLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_tableView.frame.size.width -leftDistanceLabel -rightDistanceLabel, 9999);
    
    CGSize size = [virtualLabel sizeThatFits:maximumLabelSize];
    virtualHeight = size.height +12; // 12 son las constraints top & bottom
    
    if (minDistance > virtualHeight) {
        return minDistance;
    }
    
    return virtualHeight;
}

- (float)calculateTopOffset:(BOOL)firstDisplay {
    
    float cellsHeight = 0.0;
    BOOL exit = '\0';
    
    for(ABASpeakDialog *dialog in _dataSource) {
        
        if ([dialog isKindOfClass:[NSString class]]) {
            cellsHeight += separationHeight;
        }
        else {
            BOOL avatar = YES;
            for (ABASpeakDialogPhrase *dialogPhrase in dialog.dialog) {
                
                if (avatar) {
                    avatar = NO;
                    UILabel *virtualLabel = [[UILabel alloc] init];
                    [self configDialogLabel:virtualLabel withPhrase:dialogPhrase];
                    cellsHeight += [self calculateHeightDialogAvatarLabel:virtualLabel withPhrase:dialogPhrase];
                }
                else {
                    UILabel *virtualLabel = [[UILabel alloc] init];
                    [self configDialogLabel:virtualLabel withPhrase:dialogPhrase];
                    cellsHeight += [self calculateHeightDialogNormalLabel:virtualLabel withPhrase:dialogPhrase];
                }
                
                if ([dialogPhrase isEqualToObject:(ABASpeakDialogPhrase*)_selectedPhrase] && !firstDisplay) {
                    exit = YES;
                }
            }
            
            if ([self haveTranslation:dialog]) {
                UILabel *virtualLabel = [[UILabel alloc] init];
                [self configDialogTranslateLabel:virtualLabel withPhrase:[dialog.dialog objectAtIndex:dialog.dialog.count-1]];
                cellsHeight += [self calculateHeightDialogTranslationLabel:virtualLabel withPhrase:[dialog.dialog objectAtIndex:dialog.dialog.count-1]];
            }
            
            if (exit) {
                break;
            }
            
            if (firstDisplay) {
                break;
            }
            
            for (ABASpeakPhrase *samplePhrase in dialog.sample) {
                
                UILabel *virtualLabel = [[UILabel alloc] init];
                [self configSampleLabel:virtualLabel withPhrase:samplePhrase];
                cellsHeight += [self calculateHeightSampleLabel:virtualLabel withPhrase:samplePhrase];
                
                if ([samplePhrase isEqualToObject:_selectedPhrase]) {
                    exit = YES;
                    break;
                }
            }

            if (exit) {
                break;
            }
        }
    }
    
    return -(_tableView.frame.size.height - _controlView.frame.size.height -cellsHeight);
}

- (void)hideTeacherView
{
	[self.view removeGestureRecognizer:_singleTap];
	
	if (_abaTeacher.alpha > 0.0) {
		[UIView animateWithDuration:0.5
							  delay:0.0f
							options:UIViewAnimationOptionCurveEaseInOut
						 animations:^{
							 _abaTeacher.alpha = 0.0;
						 }completion:nil];
	}
}

- (void)configTitle
{
    [self addTitleNavbarSection:STRTRAD(@"sectionSpeakTitleKey", @"Habla") andSubtitle:[_speakController getPercentageForSection:self.section]];
    [self addBackButtonNavBar];
}

- (void)setupControlView
{
	_controlView = [[ABAControlView alloc] initWithSectionType:kABASpeakSectionType];
	_controlView.audioController = _audioController;
	[self.view addSubview:_controlView];
	
	[_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABASpeakSectionType];

	CGFloat controlHeight = MAX(self.view.frame.size.width, self.view.frame.size.height) * (IS_IPAD?0.17f:0.24f);
	
	[_controlView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
	[_controlView autoSetDimension:ALDimensionHeight toSize:controlHeight];
	[_controlView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
	[_controlView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
	[_controlView setNeedsLayout];
	[_controlView setNeedsUpdateConstraints];
	[_controlView layoutIfNeeded];
	
	_controlView.delegate = self;
	_controlView.actionButton.delegate = self;
	_audioController.pulseViewController = _controlView.actionButton.pulseViewController;
	
	[_controlView.actionButton setAccessibilityLabel:@"actionButton"];

    [_controlView showAuxiliaryButtons:NO];
}

-(BOOL) isFirstDialog {
    
    ABASpeakDialog *firstDialog = [_dataSource objectAtIndex:0];
    ABASpeakDialogPhrase *firstPhrase = firstDialog.dialog[0];
    
    if ([_speakController getTotalPhraseCompletedDialogForSection:firstDialog] != firstDialog.dialog.count) {
        if ([firstPhrase isEqualToObject:(ABASpeakDialogPhrase*)_selectedPhrase]) {
            return YES;
        }
    }
    
    return NO;
}

-(NSIndexPath *) getCurrentSamplePhraseIndexPath{
    
    NSUInteger section = -1;
    NSUInteger row = -1;
   
    for(ABASpeakDialog *dialog in self.section.content) {
        if (![_speakController isDialogCompleted:dialog]) {
            section = [self.section.content indexOfObject:dialog];
            
            for (ABASpeakDialogPhrase *phrase in dialog.dialog) {
                if (!phrase.done) {
                    row = [dialog.dialog indexOfObject:phrase];
                    return [NSIndexPath indexPathForItem:row
                                               inSection:section];
                }
            }
            
            NSUInteger sampleRow = [dialog.dialog count];
            if ([self haveTranslation:dialog])
                sampleRow++;
            
            for (ABASpeakPhrase *phrase in dialog.sample) {
                if (!phrase.done) {
                    row = [dialog.sample indexOfObject:phrase];
                    return [NSIndexPath indexPathForItem:row
                                               inSection:section];
                }
            }
            break;
        }
    }
    return nil;
}

-(ABASpeakDialog*) getCurrentDialog {
    if (_selectedDialog != nil)
        return _selectedDialog;
    
    return [_speakController getCurrentDialogFromSection:self.section];
}

-(ABASpeakPhrase*) getCurrentPhrase {
    
    ABASpeakPhrase *currentPhrase = [_speakController getCurrentPhraseFromSection:self.section];
    [[Crashlytics sharedInstance] setObjectValue:currentPhrase.text forKey:@"Current phrase"];
    [[LegacyTrackingManager sharedManager] trackInteraction];
    return currentPhrase;
}

-(ABASpeakPhrase*) getCurrentSamplePhrase {
    if (_selectedPhrase != nil)
        return _selectedPhrase;
    
    return [_speakController getCurrentSamplePhraseFromSection:self.section];
}

-(ABASpeakPhrase*) getCurrentSampleToPlay {
    
    ABASpeakPhrase* phraseToPlay = _selectedPhrase;
    
    if ([phraseToPlay isKindOfClass:[ABASpeakDialogPhrase class]]) {
        return phraseToPlay;
    }
    
    if ([phraseToPlay.subPhrases count] > 0) {
        for(ABASpeakPhrase* subPhrase in phraseToPlay.subPhrases) {
            if (!subPhrase.done && ![subPhrase.text isEqualToString:@""]) {
                return subPhrase;
            }
        }
        
            for (ABASpeakPhrase *subPhrase in phraseToPlay.subPhrases) {
                if (subPhrase.audioFile != nil && ![subPhrase.audioFile isEqualToString:@""]) {
                    return subPhrase;
                }
            }
            
            return phraseToPlay.subPhrases[0];
    }
    
    return phraseToPlay;
}

- (BOOL)arrayPhraseCurrentFromCurrentDialogContainsObject:(id)object
{
    for (ABASpeakPhrase *phrase in _selectedDialog.dialog)
    {
        if ([phrase isEqualToObject:object])
        {
            return YES;
        }
    }

    ABASpeakDialogPhrase *translatePhrase = [_selectedDialog.dialog lastObject];
    if (translatePhrase.translation != nil && translatePhrase.translation.length > 0)
    {
        if ([translatePhrase isEqualToObject:object])
        {
            return YES;
        }
    }

    return NO;
}

#pragma mark TableView custom methods

- (void)configDialogLabel:(UILabel *) cellTextLabel withPhrase:(ABASpeakDialogPhrase *) cellDialogPhrase {
    
    NSMutableAttributedString *mutAttStr  = [[NSMutableAttributedString alloc] init];
    NSString *text = cellDialogPhrase.text;
    
    float spacing = 7.0f;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineSpacing = spacing;
    
    
    NSMutableAttributedString * strAtt = [[NSMutableAttributedString alloc] initWithString:text];
    
    // NSLog(@"cellDialogPhrase: %@", cellDialogPhrase.text);
    
    if ([cellDialogPhrase isEqualToObject:(ABASpeakDialogPhrase*)_selectedPhrase]) {
        
        [strAtt addAttribute:NSFontAttributeName
					   value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                       range:[text rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_BLUE_SPEAK_TEXT
                       range:[text rangeOfString:text]];
        
    }
    if ([cellDialogPhrase.done boolValue]) {
        
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                       range:[text rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_REG_MAIL_BUT
                       range:[text rangeOfString:text]];
    }
    if (![cellDialogPhrase isEqualToObject:(ABASpeakDialogPhrase*)_selectedPhrase] && ![cellDialogPhrase.done boolValue]) {
        
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:16]
                       range:[text rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_BLUE_SPEAK_TEXT
                       range:[text rangeOfString:text]];
        
    }
    if ([self.section.progress integerValue] >= 100) {
        
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                       range:[text rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_REG_MAIL_BUT
                       range:[text rangeOfString:text]];
    }
    
    [mutAttStr appendAttributedString:strAtt];
    
    [cellTextLabel setAttributedText:mutAttStr.copy];
    cellTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
}

- (void)configDialogTranslateLabel:(UILabel *) cellTextLabel withPhrase:(ABASpeakDialogPhrase *) cellDialogPhrase {
    
    [cellTextLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:14]];
    [cellTextLabel setTextColor:ABA_COLOR_BLUE_SPEAK_TEXT];
    cellTextLabel.text = cellDialogPhrase.translation;
    
}

- (void)configSampleLabel:(UILabel *) cellTextLabel withPhrase:(ABASpeakPhrase *) cellSamplePhrase {
    
    if (cellSamplePhrase.subPhrases.count > 0) {
        
        NSMutableAttributedString *mutAttStr  = [[NSMutableAttributedString alloc] init];
        BOOL subPhraseCurrent = YES;
        for (ABASpeakPhrase *subPhrase in cellSamplePhrase.subPhrases) {
            
            NSString *firstStr,*str;
            
            NSString *text = subPhrase.text;
            NSString *translation = subPhrase.translation;
            if (translation == nil || [translation isEqualToString:@""]) {
                firstStr = text;
            }
            else if ((text == nil || [text isEqualToString:@""]) && [mutAttStr.string isEqualToString:@""]) {
                firstStr = [NSString stringWithFormat:@"%@", translation];
            }
            else if (text == nil || [text isEqualToString:@""]) {
                firstStr = [NSString stringWithFormat:@" %@", translation];
            }
            else {
                firstStr = [NSString stringWithFormat:@"%@ %@", text, translation];
            }
            
            str = firstStr;
            if (![mutAttStr.string isEqualToString:@""]) {
                str = [NSString stringWithFormat:@" %@", firstStr];
            }
            
            float spacing = 7.0f;
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.alignment = NSTextAlignmentLeft;
            paragraphStyle.lineSpacing = spacing;
            
            NSMutableAttributedString * strAtt = [[NSMutableAttributedString alloc] initWithString:str];
            
            if ([cellSamplePhrase isEqualToObject:_selectedPhrase]) {
                
                if ([subPhrase.done boolValue]) {
                    
                    [strAtt addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                                   range:[str rangeOfString:text]];
                    [strAtt addAttribute:NSForegroundColorAttributeName
                                   value:ABA_COLOR_REG_MAIL_BUT
                                   range:[str rangeOfString:text]];
                    
                    if (translation != nil && ![translation isEqualToString:@""]) {
                        NSString *translationWithSpace = [NSString stringWithFormat:@" %@", translation];
                        [strAtt addAttribute:NSFontAttributeName
                                       value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                                       range:[str rangeOfString:translationWithSpace]];
                        [strAtt addAttribute:NSForegroundColorAttributeName
                                       value:ABA_COLOR_BLUE_SPEAK_TEXT
                                       range:[str rangeOfString:translationWithSpace]];
                    }
                }
                else {
                    if (subPhraseCurrent && ![subPhrase.text isEqualToString:@""]) {
                        subPhraseCurrent = NO;
                        
                        [strAtt addAttribute:NSFontAttributeName
                                       value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                                       range:[str rangeOfString:text]];
                        [strAtt addAttribute:NSForegroundColorAttributeName
                                       value:ABA_COLOR_BLUE_SPEAK_TEXT
                                       range:[str rangeOfString:text]];
                    }
                    else {
                        [strAtt addAttribute:NSFontAttributeName
                                       value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:16]
                                       range:[str rangeOfString:text]];
                        [strAtt addAttribute:NSForegroundColorAttributeName
                                       value:ABA_COLOR_BLUE_SPEAK_TEXT
                                       range:[str rangeOfString:text]];
                    }
                    if (translation != nil && ![translation isEqualToString:@""]) {
                        NSString *translationWithSpace = [NSString stringWithFormat:@" %@", translation];
                        [strAtt addAttribute:NSFontAttributeName
                                       value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                                       range:[str rangeOfString:translationWithSpace]];
                        [strAtt addAttribute:NSForegroundColorAttributeName
                                       value:ABA_COLOR_BLUE_SPEAK_TEXT
                                       range:[str rangeOfString:translationWithSpace]];
                        
                    }
                }
                
            }
            if ([subPhrase.done boolValue]) {
                
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                               range:[str rangeOfString:text]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_REG_MAIL_BUT
                               range:[str rangeOfString:text]];
                
                if (translation != nil && ![translation isEqualToString:@""]) {
                    NSString *translationWithSpace = [NSString stringWithFormat:@" %@", translation];
                    [strAtt addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                                   range:[str rangeOfString:translationWithSpace]];
                    [strAtt addAttribute:NSForegroundColorAttributeName
                                   value:ABA_COLOR_BLUE_SPEAK_TEXT
                                   range:[str rangeOfString:translationWithSpace]];
                }
            }
            if (![cellSamplePhrase isEqualToObject:_selectedPhrase] && ![subPhrase.done boolValue]) {
                
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:16]
                               range:[str rangeOfString:text]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_BLUE_SPEAK_TEXT
                               range:[str rangeOfString:text]];
                
                if (translation != nil && ![translation isEqualToString:@""]) {
                    NSString *translationWithSpace = [NSString stringWithFormat:@" %@", translation];
                    if([mutAttStr.string isEqualToString:@""]) {
                        translationWithSpace = translation;
                    }
                    [strAtt addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                                   range:[str rangeOfString:translationWithSpace]];
                    [strAtt addAttribute:NSForegroundColorAttributeName
                                   value:ABA_COLOR_BLUE_SPEAK_TEXT
                                   range:[str rangeOfString:translationWithSpace]];
                }
                
            }
            if ([self.section.progress integerValue] >= 100) {
                
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                               range:[str rangeOfString:text]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_REG_MAIL_BUT
                               range:[str rangeOfString:text]];
                
                if (translation != nil && ![translation isEqualToString:@""]) {
                    NSString *translationWithSpace = [NSString stringWithFormat:@" %@", translation];
                    [strAtt addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                                   range:[str rangeOfString:translationWithSpace]];
                    [strAtt addAttribute:NSForegroundColorAttributeName
                                   value:ABA_COLOR_BLUE_SPEAK_TEXT
                                   range:[str rangeOfString:translationWithSpace]];
                }
            }
            
            [mutAttStr appendAttributedString:strAtt];
        }
        
        [cellTextLabel setAttributedText:mutAttStr.copy];
        cellTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    else {
        
        NSMutableAttributedString *mutAttStr  = [[NSMutableAttributedString alloc] init];
        NSString *str;
        NSString *text = cellSamplePhrase.text;
        NSString *translation = cellSamplePhrase.translation;
        if (translation == nil || [translation isEqualToString:@""]) {
            str = [NSString stringWithFormat:@" %@", text];
        }
        else {
            str = [NSString stringWithFormat:@"%@\n%@", text, translation];
        }
        
        float spacing = 7.0f;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = NSTextAlignmentCenter;
        paragraphStyle.lineSpacing = spacing;
        
        NSMutableAttributedString * strAtt = [[NSMutableAttributedString alloc] initWithString:str];
        
        if ([cellSamplePhrase isEqualToObject:_selectedPhrase]) {
            
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                           range:[str rangeOfString:text]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_BLUE_SPEAK_TEXT
                           range:[str rangeOfString:text]];
            
            if (translation != nil && ![translation isEqualToString:@""]) {
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                               range:[str rangeOfString:translation]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_BLUE_SPEAK_TEXT
                               range:[str rangeOfString:translation]];
            }
            
        }
        if ([cellSamplePhrase.done boolValue]) {
            
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                           range:[str rangeOfString:text]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_REG_MAIL_BUT
                           range:[str rangeOfString:text]];
            
            if (translation != nil && ![translation isEqualToString:@""]) {
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                               range:[str rangeOfString:translation]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_BLUE_SPEAK_TEXT
                               range:[str rangeOfString:translation]];
            }
            
        }
        if (![cellSamplePhrase isEqualToObject:_selectedPhrase] && ![cellSamplePhrase.done boolValue]) {
            
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:16]
                           range:[str rangeOfString:text]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_BLUE_SPEAK_TEXT
                           range:[str rangeOfString:text]];
            
            if (translation != nil && ![translation isEqualToString:@""]) {
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                               range:[str rangeOfString:translation]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_BLUE_SPEAK_TEXT
                               range:[str rangeOfString:translation]];
            }
            
        }
        if ([self.section.progress integerValue] >= 100) {
            
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                           range:[str rangeOfString:text]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_REG_MAIL_BUT
                           range:[str rangeOfString:text]];
            
            if (translation != nil && ![translation isEqualToString:@""]) {
                [strAtt addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:14]
                               range:[str rangeOfString:translation]];
                [strAtt addAttribute:NSForegroundColorAttributeName
                               value:ABA_COLOR_BLUE_SPEAK_TEXT
                               range:[str rangeOfString:translation]];
            }
        }
        
        [mutAttStr appendAttributedString:strAtt];
        
        [cellTextLabel setAttributedText:mutAttStr.copy];
        cellTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
}

-(BOOL) haveTranslation: (ABASpeakDialog*) dialog {
    
    if ([dialog.dialog count] > 0) {
        ABASpeakDialogPhrase *phrase = [dialog.dialog lastObject];
        
        if (phrase.translation != nil && phrase.translation.length > 0) {
            return YES;
        }
    }
    return NO;
}

-(kABASpeakDialogCellType) cellTypeForIndexPath: (NSIndexPath*) indexPath {

    if ([[_dataSource objectAtIndex:indexPath.section] isKindOfClass:[NSString class]]) {
        return kABASpeakDialogSeparation;
    }
        
    ABASpeakDialog *dialog = [_dataSource objectAtIndex:indexPath.section];
    
    
    if (indexPath.row <= ([dialog.dialog count] > 0 ? [dialog.dialog count] - 1 : 0))
    {
        if (indexPath.row == 0)
        {
            return kABASpeakDialogAvatarCell;
        }
        else
        {
            return kABASpeakDialogNormalCell;
        }
    }
    
    if ([self haveTranslation:dialog] && indexPath.row == [dialog.dialog count]) {
        return kABASpeakDialogTranslationCell;
    }
    
    return kABASpeakDialogSample;
}

-(id) objectAtIndexPath: (NSIndexPath*) indexPath {

    ABASpeakDialog *dialog = [_dataSource objectAtIndex:indexPath.section];
    
    if (indexPath.row <= ([dialog.dialog count] - 1)) {
        return [dialog.dialog objectAtIndex:indexPath.row
                ];
    }
    
    if ([self haveTranslation:dialog] && indexPath.row == [dialog.dialog count]) {
        ABASpeakDialogPhrase *phrase = [dialog.dialog lastObject];
        return phrase;
    }
    
    NSUInteger priorRows = [dialog.dialog count];
    if ([self haveTranslation:dialog])
        priorRows++;
    
    NSUInteger sampleIndex = indexPath.row - priorRows;
    
    return [dialog.sample objectAtIndex:sampleIndex];
}

- (float)calculateHeightDialogAvatarLabel:(UILabel *) virtualLabel withPhrase:(ABASpeakDialogPhrase *) cellDialogPhrase {
    
    CGFloat leftDistanceLabel, rightDistanceLabel, minDistance, virtualHeight;
    leftDistanceLabel = 75;
    rightDistanceLabel = 10;
	minDistance = IS_IPAD?minTeacherDialogHeight_iPad:minTeacherDialogHeight;
	
    virtualLabel.numberOfLines = 0;
    virtualLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_tableView.frame.size.width -leftDistanceLabel -rightDistanceLabel, 9999);
    
    CGSize size = [virtualLabel sizeThatFits:maximumLabelSize];
    virtualHeight = size.height +12; // 12 son las constraints top & bottom
    
    if (minDistance > virtualHeight) {
        return minDistance;
    }
    
    return virtualHeight;
}

- (float)calculateHeightDialogNormalLabel:(UILabel *) virtualLabel withPhrase:(ABASpeakDialogPhrase *) cellDialogPhrase {
    
    CGFloat leftDistanceLabel, rightDistanceLabel, virtualHeight;
    leftDistanceLabel = 15;
    rightDistanceLabel = 10;
    
    virtualLabel.numberOfLines = 0;
    virtualLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_tableView.frame.size.width -leftDistanceLabel -rightDistanceLabel, 9999);
    
    CGSize size = [virtualLabel sizeThatFits:maximumLabelSize];
    virtualHeight = size.height;
    
    return virtualHeight;
}

- (float)calculateHeightDialogTranslationLabel:(UILabel *) virtualLabel withPhrase:(ABASpeakDialogPhrase *) cellDialogPhrase {
    
    virtualLabel.numberOfLines = 0;
    virtualLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_tableView.frame.size.width -25, 9999); //25 son las constraints right & left
    
    CGSize size = [virtualLabel sizeThatFits:maximumLabelSize];
    
    return size.height +10; // 10 constraint bottom
}

- (float)calculateHeightSampleLabel:(UILabel *) virtualLabel withPhrase:(ABASpeakPhrase *) cellDialogPhrase {
    
    virtualLabel.numberOfLines = 0;
    virtualLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_tableView.frame.size.width -25, 9999); //25 son las constraints right & left
    
    CGSize size = [virtualLabel sizeThatFits:maximumLabelSize];
    
    return size.height +20; // 20 son las constraints top & bottom
}


#pragma mark UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return _dataSource.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if ([[_dataSource objectAtIndex:section] isKindOfClass:[NSString class]]) {
        return 1;
    }
    
    ABASpeakDialog *dialog = [_dataSource objectAtIndex:section];
    
    NSUInteger rows = [dialog.dialog count];
    
    if ([self haveTranslation:dialog])
        rows++;
    
    rows += [dialog.sample count];
    
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    kABASpeakDialogCellType cellType = [self cellTypeForIndexPath:indexPath];
    
    switch (cellType) {
        case kABASpeakDialogAvatarCell: {
            //Dialog
            ABASpeakDialogPhrase *cellDialogPhrase = [self objectAtIndexPath:indexPath];
            
            UILabel *virtualLabel = [[UILabel alloc] init];
            
            [self configDialogLabel:virtualLabel withPhrase:cellDialogPhrase];
            
            return [self calculateHeightDialogAvatarLabel:virtualLabel withPhrase:cellDialogPhrase];
            
            break;
        }
           
        case kABASpeakDialogNormalCell: {
            //Dialog Normal
            ABASpeakDialogPhrase *cellDialogPhrase = [self objectAtIndexPath:indexPath];
            
            UILabel *virtualLabel = [[UILabel alloc] init];
            
            [self configDialogLabel:virtualLabel withPhrase:cellDialogPhrase];
            
            return [self calculateHeightDialogNormalLabel:virtualLabel withPhrase:cellDialogPhrase];
            
            break;
        }
            
        case kABASpeakDialogTranslationCell: {
            //Dialog Translation
            ABASpeakDialogPhrase *cellDialogTranslatePhrase = [self objectAtIndexPath:indexPath];
            
            UILabel *virtualLabel = [[UILabel alloc] init];
            
            [self configDialogTranslateLabel:virtualLabel withPhrase:cellDialogTranslatePhrase];
            
            return [self calculateHeightDialogTranslationLabel:virtualLabel withPhrase:cellDialogTranslatePhrase];

            break;
        }
            
        case kABASpeakDialogSample: {
            //Sample
            ABASpeakPhrase *cellSamplePhrase = [self objectAtIndexPath: indexPath];
            
            UILabel *virtualLabel = [[UILabel alloc] init];
            
            [self configSampleLabel:virtualLabel withPhrase:cellSamplePhrase];
            
            return [self calculateHeightSampleLabel:virtualLabel withPhrase:cellSamplePhrase];

            break;
        }
        
        case kABASpeakDialogSeparation:{
            return separationHeight;
        }
        default:
            return 0;
            break;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    UILabel * cellTextLabel;
    
    kABASpeakDialogCellType cellType = [self cellTypeForIndexPath:indexPath];
    
    switch (cellType) {
        case kABASpeakDialogAvatarCell: {
            //Dialog Avatar
            ABASpeakDialogPhrase *cellDialogPhrase = [self objectAtIndexPath:indexPath];
            
			cell = [tableView dequeueReusableCellWithIdentifier:IS_IPAD?cellDialogSpeak_iPad:cellDialogSpeak];
            cellTextLabel = (UILabel*)[cell viewWithTag:20];
            
            [self configDialogLabel:cellTextLabel withPhrase:cellDialogPhrase];
            
            if (cellDialogPhrase.role != nil) {
                UIImageView * cellRoleImage = (UIImageView*)[cell viewWithTag:21];
                
                for(ABARole *role in self.section.unit.roles)
                {
                    if ([role.name isEqualToString:cellDialogPhrase.role]) {
                        
                        cellRoleImage.layer.cornerRadius = cellRoleImage.frame.size.width/2;
                        
                        NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:role.imageUrl];
                        NSURL *imageUrl = [NSURL URLWithString:urlString];
                        
                        [cellRoleImage sd_setImageWithURL:imageUrl];
                    }
                }
            }
            if ([self arrayPhraseCurrentFromCurrentDialogContainsObject:cellDialogPhrase]) {
                cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
            }
            else {
                cell.backgroundColor = [UIColor clearColor];
            }
            
            return cell;
            
            break;

        }
        case kABASpeakDialogNormalCell: {
            //Dialog Normal
            ABASpeakDialogPhrase *cellDialogPhrase = [self objectAtIndexPath:indexPath];
            
            cell = [tableView dequeueReusableCellWithIdentifier:cellDialogPhase];
            cellTextLabel = (UILabel*)[cell viewWithTag:40];

            
            [self configDialogLabel:cellTextLabel withPhrase:cellDialogPhrase];

            if ([self arrayPhraseCurrentFromCurrentDialogContainsObject:cellDialogPhrase]) {
                cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
            }
            else {
                cell.backgroundColor = [UIColor clearColor];
            }
            
            return cell;

            break;
        }
        case kABASpeakDialogTranslationCell: {
            // Translation
            cell = [tableView dequeueReusableCellWithIdentifier:cellDialogTranslate];
            
            cellTextLabel = (UILabel*)[cell viewWithTag:30];
            
            ABASpeakDialogPhrase *cellDialogPhrase = [self objectAtIndexPath:indexPath];
            
            [self configDialogTranslateLabel:cellTextLabel withPhrase:cellDialogPhrase];
            
            if ([self arrayPhraseCurrentFromCurrentDialogContainsObject:cellDialogPhrase]) {
                cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
            }
            else {
                cell.backgroundColor = [UIColor clearColor];
            }
            
            return cell;
            
            break;
        }
        case kABASpeakDialogSample: {
            // Sample
            cell = [tableView dequeueReusableCellWithIdentifier:cellSampleSpeak];
            
            cellTextLabel = (UILabel*)[cell viewWithTag:10];
            
            ABASpeakPhrase *cellSamplePhrase = [self objectAtIndexPath:indexPath];
            
            [self configSampleLabel:cellTextLabel withPhrase:cellSamplePhrase];
            
            if ([_selectedPhrase isEqualToObject:cellSamplePhrase]) {
                cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
            }
            else {
                cell.backgroundColor = [UIColor clearColor];
            }
            
            return cell;
            
            break;
        }
        case kABASpeakDialogSeparation:{
            // Separation
            cell = [tableView dequeueReusableCellWithIdentifier:cellFooter];
            
            if (indexPath.section == 0) {
                ABASpeakDialog *cellNextDialog = [_dataSource objectAtIndex:indexPath.section+1];
                
                if ([_selectedDialog isEqualToObject:cellNextDialog]) {
                    cell.backgroundColor = [UIColor whiteColor];
                }
                else {
                    cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
                }
            }
            else if (indexPath.section == _dataSource.count-1) {
                ABASpeakDialog *cellPreviewDialog = [_dataSource objectAtIndex:indexPath.section-1];
                
                if ([_selectedDialog isEqualToObject:cellPreviewDialog]) {
                    cell.backgroundColor = [UIColor whiteColor];
                }
                else {
                    cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
                }
            }
            else {
                ABASpeakDialog *cellPreviewDialog = [_dataSource objectAtIndex:indexPath.section-1];
                ABASpeakDialog *cellNextDialog = [_dataSource objectAtIndex:indexPath.section+1];
                
                if ([_selectedDialog isEqualToObject:cellPreviewDialog] ||  [_selectedDialog isEqualToObject:cellNextDialog]) {
                    cell.backgroundColor = [UIColor whiteColor];
                }
                else {
                    cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
                }
            }
            return cell;
        }
            
        default:
            return nil;
            break;
    }
    
    return nil;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_selectionDisabled) {
        return nil;
    }
    return indexPath;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self hideTeacherView];
    
    kABASpeakDialogCellType cellType = [self cellTypeForIndexPath:indexPath];
    
    NSInteger dialogSection = indexPath.section;
    
    switch (cellType) {
        case kABASpeakDialogAvatarCell:
        case kABASpeakDialogNormalCell:{
            //Dialog Avatar
            ABASpeakDialogPhrase *speakDialogPhraseSelected = [self objectAtIndexPath:indexPath];
            _selectedPhrase = (ABASpeakPhrase*)speakDialogPhraseSelected;
            _selectedDialog = [_dataSource objectAtIndex:dialogSection];
            
            _currentStatus = kABASpeakListen;
            [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABASpeakSectionType];
            [_controlView showAuxiliaryButtons:NO];
            [_tableView reloadData];
            [_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
            [self listenButtonTapped];

            break;
        }
        case kABASpeakDialogTranslationCell: {
            // Translation
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            break;
        }
        case kABASpeakDialogSample: {
            // Sample
            ABASpeakPhrase *speakSamplePhraseSelected = [self objectAtIndexPath:indexPath];
            _selectedPhrase = speakSamplePhraseSelected;
            _selectedDialog = nil;
            
            _currentStatus = kABASpeakListen;
            [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABASpeakSectionType];
            [_controlView showAuxiliaryButtons:NO];
            [_tableView reloadData];
            [_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
            [self listenButtonTapped];
            break;
        }
        default:
            break;
    }
}

#pragma mark ActionButton delegates

- (void)listenButtonTapped
{
    if (_selectedPhrase != nil) {
        [_controlView.actionButton removeButtonTapRecognizer];
        
        [self hideTeacherView];
        
        _selectionDisabled = YES;

        _audioController.pulseViewController = _controlView.actionButton.pulseViewController;

        [_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
        
        [_audioController playAudioFile:[self getCurrentSampleToPlay].audioFile withUnit: self.section.unit forRec:NO];
    }
}

- (void)stopRecordButtonTapped
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [weakSelf.controlView.actionButton removeButtonTapRecognizer];
        [weakSelf.controlView.actionButton stopRecordingAnimation];
        [weakSelf.controlView hideRecordingTime];
        
        weakSelf.currentStatus = kABASpeakCompareRecording;
        
        [weakSelf.controlView.actionButton switchToType:kABAActionButtonTypeCompare forSection:kABASpeakSectionType];
    });
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [weakSelf.audioController stopRecording];
    });
}

- (void)continueButtonTapped
{
    [_controlView.actionButton removeButtonTapRecognizer];
    [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABASpeakSectionType];
    [_controlView showAuxiliaryButtons:NO];
    
    _currentStatus = kABASpeakListen;
    
    if (_sectionCompleted) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([_speakController isSectionCompleted:self.section]) {
        
        __weak typeof(self) weakSelf = self;
        [_speakController setCompletedSection:self.section completionBlock:^(NSError *error, id object) {
            if (!error)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [weakSelf showCompletedAlert];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                });
            }
            else
            {
                // alarm!
            }
         }];
    }
    else if ([_speakController isDialogCompleted:[_dataSource lastObject]])
    {
        [_dataSource addObject:separation];
        _selectedDialog = nil;
        [_dataSource addObject:[self getCurrentDialog]];
        _selectedPhrase = [self getCurrentPhrase];
        _selectedDialog = [self getCurrentDialog];
        [_tableView reloadData];
        [_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
        [self configTitle];
        [self listenButtonTapped];
    }
    else {
        _selectedDialog = nil;
        _selectedPhrase = [self getCurrentPhrase];
        _selectedDialog = [self getCurrentDialog];
        if (_selectedDialog.dialog.count == [_speakController getTotalPhraseCompletedDialogForSection:_selectedDialog]) {
            _selectedDialog = nil;
        }
        [_tableView reloadData];
        [_tableView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
        [self configTitle];
        [self listenButtonTapped];
    }

}

- (void)compareButtonTapped
{
    [self startComparing];
}

- (void)prepareForComparing
{
    _currentStatus = kABASpeakCompareRecording;
    [_controlView.actionButton switchToType:kABAActionButtonTypeCompare forSection:kABASpeakSectionType];
    _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
}

- (void)startComparing
{
    [self prepareForComparing];
    //play my recording
    [_audioController playAudioFile:[self getCurrentSampleToPlay].audioFile withUnit: self.section.unit forRec:YES];
}

- (void)playOriginal
{
    _audioController.pulseViewController = _controlView.actionButton.pulseViewController;

    [_audioController playAudioFile:[self getCurrentSampleToPlay].audioFile withUnit: self.section.unit forRec:NO];
}

- (void)markPhraseAsDone {
    
    __weak typeof(self) weakSelf = self;
    
    [_phrasesController setPhrasesDone:@[[self getCurrentSampleToPlay]] forSection:self.section sendProgressUpdate:YES  completionBlock:^(NSError *error, id object) {
        
         if (!error)
         {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 weakSelf.selectionDisabled = NO;
                 [weakSelf configTitle];
                 [weakSelf.tableView reloadData];
                 
                 [weakSelf.controlView.actionButton switchToType:kABAActionButtonTypeContinue forSection:kABASpeakSectionType];
                 [weakSelf.controlView.actionButton addButtonTapRecognizer];
                 [weakSelf.controlView showAuxiliaryButtons:YES];
                 
                 NSLog(@"PROGRESS: %f - %f",
                       [weakSelf.speakController getTotalElementsForSection:weakSelf.section],
                       [weakSelf.speakController getElementsCompletedForSection:weakSelf.section]
                       );
             });
             
#ifdef SHEPHERD_DEBUG
             if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                     [weakSelf continueButtonTapped];
                 });
             }
#endif
         }
         else
         {
             // alarm!
         }
     }];
}


#pragma mark - ABAControlViewDelegate

- (void)controlViewRedoButtonTapped {
    
    _currentStatus = kABASpeakListen;
    [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABASpeakSectionType];
    [self listenButtonTapped];
    [_controlView showAuxiliaryButtons:NO];
}

- (void)controlViewCompareButtonTapped {
    
    _currentStatus = kABASpeakCompareRecording;
    [_controlView.actionButton switchToType:kABAActionButtonTypeCompare forSection:kABASpeakSectionType];
    [self startComparing];
    [_controlView showAuxiliaryButtons:NO];
}

#pragma mark - AudioControllerDelegate

- (void)audioStarted
{
    if (self.currentStatus == kABASpeakBeginRecordSound)
    {
        self.currentStatus = kABASpeakRec;
        
        _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
        
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf.audioController recordAudioFile:[self getCurrentSampleToPlay].audioFile withUnit: self.section.unit];
            
#ifdef SHEPHERD_DEBUG
            if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [weakSelf stopRecordButtonTapped];
                });
            }
#endif
        });
    }

}

- (void)audioFinished
{
    switch (_currentStatus)
    {
        case kABASpeakListen:
        {
            _audioController.pulseViewController = nil;

            __weak typeof(self) weakSelf = self;
            ABASpeakPhrase * phrase = [self getCurrentSampleToPlay];
            [_phrasesController setPhrasesListened:@[phrase] forSection:self.section sendProgressUpdate:NO completionBlock:^(NSError *error, id object) {
                weakSelf.currentStatus = kABASpeakBeginRecordSound;
                [weakSelf.audioController playSound:@"beep.mp3"];
            }];
            
            // Cooladata tracking
            [CoolaDataTrackingManager trackListenedToAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaSpeak exerciseId:phrase.idPhrase];
            
            break;
        }
            
        case kABASpeakCompareRecording: {

            _currentStatus = kABASpeakCompare;
            _audioController.pulseViewController = nil;
            
            __weak typeof(self) weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [weakSelf playOriginal];
            });

            break;
        }
            
        case kABASpeakCompare: {
            
            _audioController.pulseViewController = nil;
            _currentStatus = kABASpeakContinue;

            [self markPhraseAsDone];
            
            // Cooladata tracking
            ABASpeakPhrase * phrase = [self getCurrentSampleToPlay];
            [CoolaDataTrackingManager trackComparedAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaSpeak exerciseId:phrase.idPhrase];
            break;
        }
			
        default:
        {
            NSLog(@"Unexpected status");
            break;
        }
    }
    
}

- (void)audioDidGetError:(NSError *)error
{
	[self audioFinished];
}

- (void)recordingDidGetError:(NSError *)error
{
	[self recordingFinished];
}

- (void)recordingStarted
{
    [self.controlView.actionButton switchToType:kABAActionButtonTypeRecord forSection:kABASpeakSectionType];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^
    {
        [_controlView.actionButton addButtonTapRecognizer];
    });

    [_controlView showRecordingTime];
}

- (void)recordingFinished
{
    [_controlView hideRecordingTime];
    [_controlView.actionButton stopRecordingAnimation];

    _audioController.pulseViewController = nil;
    [_audioController.recorderPulseTimer invalidate];
    _audioController.recorderPulseTimer = nil;

    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        [self prepareForComparing];
    }
    else
    {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf startComparing];
        });
    }
    
    // Cooladata tracking
    ABASpeakPhrase * phrase = [self getCurrentSampleToPlay];
    [CoolaDataTrackingManager trackRecordedAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaSpeak exerciseId:phrase.idPhrase];
}

#pragma mark - Private methods (from notifications)

- (void)handleEnteredBackground
{
    if ([[_audioController player] isPlaying])
    {
        [[_audioController player] stop];

        [_controlView.actionButton removeButtonAnimations];
        [_controlView.actionButton addButtonTapRecognizer];

        _audioController.pulseViewController = nil;
    }
    else if ([[_audioController recorder] isRecording])
    {
        [_audioController stopRecording];

        [_controlView.actionButton stopRecordingAnimation];
        [_controlView hideRecordingTime];
        [_controlView.actionButton removeButtonAnimations];
        [_controlView.actionButton addButtonTapRecognizer];

        _audioController.pulseViewController = nil;
    }
}

- (void)showCompletedAlertIfNeeded
{
    if (!_sectionCompleted && [_speakController isSectionCompleted:self.section])
    {
        [self showCompletedAlert];
    }
}

- (void)showCompletedAlert
{
    [self.delegate showAlertCompletedUnit:kABASpeakSectionType];
}

@end
