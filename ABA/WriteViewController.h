//
//  WriteViewController.h
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "Definitions.h"
#import "AudioController.h"
#import "ABAActionButtonView.h"
#import "SectionProtocol.h"
#import <PHFComposeBarView/PHFComposeBarView.h>
#import "ABAWrite.h"

@class ABAPhrase;

@interface WriteViewController : SectionBaseViewController <AudioControllerDelegate, ABAActionButtonViewDelegate, PHFComposeBarViewDelegate>

@property (strong, nonatomic) ABAWrite *section;

@property (strong, nonatomic) ABAActionButtonView *actionButton;

@property kABAWriteStatus currentStatus;
@property (weak) id <SectionProtocol> delegate;

- (void)textViewDidChange:(UITextView *)textView;

@end
