//
//  ABAAlertCompletedUnit.h
//  ABA
//
//  Created by Marc Güell Segarra on 19/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Definitions.h"

@interface ABAAlertCompletedUnit : UIView

-(id) initWithFrame:(CGRect)frame andSectionType:(kABASectionTypes)type;

@end
