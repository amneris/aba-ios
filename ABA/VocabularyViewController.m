//
//  VocabularyViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 11/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "VocabularyViewController.h"
#import "UIViewController+ABA.h"
#import "Resources.h"
#import "Utils.h"
#import "ABASection+Methods.h"
#import "ABAVocabulary.h"
#import "ABAPhrase.h"
#import "ABAVocabularyPhrase.h"
#import "VocabularyCell.h"
#import "AudioController.h"
#import "ABAAlert.h"
#import "VocabularyController.h"
#import "PhrasesController.h"
#import "ABATeacherTipView.h"
#import "ImageHelper.h"
#import "DataController.h"
#import "PureLayout.h"
#import "APIManager.h"
#import "ABA-Swift.h"
#import <Crashlytics/Crashlytics.h>
#import "LegacyTrackingManager.h"

static NSString *cellVocabulary = @"VocabularyCell";
static NSString *cellVocabulary_iPad = @"VocabularyCell_iPad";
static float minHeighCollectionCell = 62.0f;
static float minHeighCollectionCell_iPad = 100.0f;

@interface VocabularyViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, AudioControllerDelegate>

@property (nonatomic, strong) AudioController* audioController;
@property (nonatomic, strong) SectionController* sectionController;
@property (nonatomic, strong) PhrasesController *phrasesController;

@property VocabularyController *vocabularyController;

@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *progressTitle;
@property (weak, nonatomic) IBOutlet UILabel *progressSubTitle;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property NSMutableArray *dataSource;

@property UIView *firstView;

@property (strong, nonatomic) ABAControlView *controlView;

@property ABAAlert *abaAlert;

@property BOOL selectionDisabled;

@property ABATeacherTipView *abaTeacher;
@property UITapGestureRecognizer *singleTap;

@property BOOL sectionCompleted;

@end

@implementation VocabularyViewController
@dynamic section;

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _currentStatus = kABAVocabularyListen;
    
    _vocabularyController = [[VocabularyController alloc] init];
    _sectionController = [[SectionController alloc] init];
    _phrasesController = [PhrasesController new];
    
    _audioController = [AudioController new];
    _audioController.audioControllerDelegate = self;
    _audioController.sectionType = kABAVocabularySectionType;
    
    [self addBackButtonNavBar];
    [self setupView];
    [self configProgressTitle];
    [self createDataSource];
    
    _collectionView.hidden = YES;
    
    [DataController saveProgressActionForSection:self.section andUnit:[self.section getUnitFromSection] andPhrases:nil errorText:nil isListen:NO isHelp: NO completionBlock:^(NSError *error, id object) {
        
    }];
	
	if([_vocabularyController isSectionCompleted:self.section]) {
		_sectionCompleted = YES;
	}
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	
    [self setupControlView];
    [self setupTeacherView];
    
    // Checking microphone status
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted)
     {
         if (!granted)
         {
             _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"noMicrophoneTextKey", @"Imposible grabar sonido :-(")];
             [_abaAlert showWithOffset:44 andDissmisAfter:2000]; // showing error 2000 seconds (trying to show the error permanently)
             self.controlView.alpha = 0.5;
             self.view.userInteractionEnabled = NO;
         }
     }];
    
    [self.view layoutIfNeeded];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];    
    [_collectionView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, _controlView.frame.size.height, 0)];
    
    if([self.view.subviews containsObject:_abaTeacher]) {
        
        _collectionView.hidden = NO;
        
        [_collectionView setContentInset:UIEdgeInsetsMake(_collectionView.frame.size.height, 0, - _controlView.frame.size.height, 0)];
        _collectionView.contentOffset = CGPointMake(0, -_collectionView.frame.size.height);
        
        [UIView animateWithDuration:0.8
                              delay:0.0
             usingSpringWithDamping:0.6
              initialSpringVelocity:0.7
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             _collectionView.contentOffset = CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]);
                             [_collectionView setContentInset:UIEdgeInsetsMake(_collectionView.frame.size.height - _controlView.frame.size.height -[self calculateHeightFirstCell], 0, _controlView.frame.size.height, 0)];
                             
                         }
                         completion:NULL];
        
    }
    else {
        
        [_collectionView setContentInset:UIEdgeInsetsMake(_collectionView.frame.size.height - _controlView.frame.size.height -[self calculateHeightFirstCell], 0, _controlView.frame.size.height, 0)];
        
        [_collectionView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:NO];
        
        _collectionView.hidden = NO;
        
    }
    
    [[LegacyTrackingManager sharedManager] trackPageView:@"vocabularysection"];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationWillResignActiveNotification
                                               object: nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(showCompletedAlertIfNeeded)
												 name:@"showCompletedAlertIfNeeded"
											   object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _audioController.audioControllerDelegate = nil;
    
    if ([_audioController.recorder isRecording]) {
        [_audioController stopRecording];
    }
    
    if ([_audioController.player isPlaying]) {
        [[_audioController player] stop];
    }
    if(_abaAlert) {
        [_abaAlert dismiss];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [_collectionView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
}

#pragma mark - Private methods

-(void) setupControlView
{
	_controlView = [[ABAControlView alloc] initWithSectionType:kABAVocabularySectionType];
	_controlView.audioController = _audioController;
	[self.view addSubview:_controlView];
	
	[_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAVocabularySectionType];
	
	CGFloat controlHeight = MAX(self.view.frame.size.width, self.view.frame.size.height) * (IS_IPAD?0.17f:0.24f);
	
	[_controlView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:self.view];
	[_controlView autoSetDimension:ALDimensionHeight toSize:controlHeight];
	[_controlView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self.view];
	[_controlView autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.view];
	[_controlView setNeedsLayout];
	[_controlView setNeedsUpdateConstraints];
	[_controlView layoutIfNeeded];
	
	_controlView.delegate = self;
	_controlView.actionButton.delegate = self;
	_audioController.pulseViewController = _controlView.actionButton.pulseViewController;
	
	[_controlView.actionButton setAccessibilityLabel:@"actionButton"];
	
	[_controlView showAuxiliaryButtons:NO];
}

-(void)handleEnteredBackground
{
    if([[_audioController player] isPlaying])
    {
        [[_audioController player] stop];
        
        [_controlView.actionButton removeButtonAnimations];
        
        [_controlView.actionButton addButtonTapRecognizer];
        
        _audioController.pulseViewController = nil;
    }
    else if([[_audioController recorder] isRecording])
    {
        [_audioController stopRecording];
        [_controlView.actionButton stopRecordingAnimation];
        [_controlView hideRecordingTime];
        
        [_controlView.actionButton removeButtonAnimations];
        
        [_controlView.actionButton addButtonTapRecognizer];
        
        _audioController.pulseViewController = nil;
    }
}


-(float) calculateHeightFirstCell {
    
    ABAVocabularyPhrase *cellVocabularyPhrase = [_dataSource objectAtIndex:0];
    
    UILabel *virtualLabel = [[UILabel alloc] init];
    
    [self configLabel:virtualLabel withPhrase:cellVocabularyPhrase];
    
    float cellHeight = [self calculateHeightDialogLabel:virtualLabel withPhrase:cellVocabularyPhrase];
    
    return cellHeight;
}

-(BOOL) isFirstDialog {
    
    ABAVocabularyPhrase *firstPhrase = [_dataSource objectAtIndex:0];
    
    if([firstPhrase isEqualToObject: _currentPhrase]) {
        return YES;
    }
    
    return NO;
}

-(float) calculateTopOffset:(BOOL)firstDisplay {
    
    float cellsHeight = 0.0;
    
    for(ABAVocabularyPhrase *phrase in _dataSource) {
        
        UILabel *virtualLabel = [[UILabel alloc] init];
        
        [self configLabel:virtualLabel withPhrase:phrase];
        
        cellsHeight += [self calculateHeightDialogLabel:virtualLabel withPhrase:phrase];
        
        if([phrase isEqualToObject: _currentPhrase]) {
            break;
        }
        
    }
    
    return -(_collectionView.frame.size.height - _controlView.frame.size.height -cellsHeight);
}

-(void) createDataSource {
    
    _dataSource = [[NSMutableArray alloc] init];
    
    for (ABAVocabularyPhrase *vocabularyPhrase in self.section.content) {
        [_dataSource addObject:vocabularyPhrase];
    }
    
    if([_vocabularyController isSectionCompleted:self.section]) {
        _currentPhrase = nil;
    }
    else {
        _currentPhrase = [self getCurrentPhrase];
    }
    
    [_collectionView reloadData];
}

-(void)setupView {
    
    _progressView.backgroundColor = ABA_COLOR_BLUE_MENU_UNIT;
    
    _progressSubTitle.text = STRTRAD(@"sectioVocabularyProgressKey", @"palabras aprendidas en esta unidad");
    _progressSubTitle.textColor = ABA_COLOR_DARKGREY;
	[_progressSubTitle setFont:[UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?18:14]];
}

-(void)setupTeacherView
{
    if(![self.view.subviews containsObject:_abaTeacher] && [[_vocabularyController getPercentageForSection:self.section] isEqualToString:@"0%"])
    {
        _abaTeacher = [[ABATeacherTipView alloc] initTeacherWithText:@"" withImage:@"teacher" withShadow:YES];

        UIColor *color = ABA_COLOR_REG_BACKGROUND;
        _abaTeacher.backgroundColor = [color colorWithAlphaComponent:1.0f];

        NSString *str = STRTRAD(@"sectioVocabularyTeacherKey", @"Es el momento de pensar en la gramática");
        float spacing = 5.0f;
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = spacing;
        NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
        [mutAttStr addAttribute:NSFontAttributeName
                          value:[UIFont fontWithName:ABA_MUSEO_SANS_300 size:IS_IPAD?18:14]
                          range:[str rangeOfString:str]];
        [mutAttStr addAttribute:NSForegroundColorAttributeName
                          value:ABA_COLOR_DARKGREY
                          range:[str rangeOfString:str]];
        [mutAttStr addAttribute:NSParagraphStyleAttributeName
                          value:paragraphStyle
                          range:[str rangeOfString:str]];
        
        [_abaTeacher setAttributedText:mutAttStr.copy];
        
		[self.view addSubview:_abaTeacher];
		[_abaTeacher setConstraintsAttributedText];

        _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTeacherView)];
        [self.view addGestureRecognizer:_singleTap];
    }
}

-(void)hideTeacherView
{
    [self.view removeGestureRecognizer:_singleTap];
    
    [UIView animateWithDuration:0.3
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
		 _abaTeacher.alpha = 0.0f;
	 }
                     completion:^(BOOL finished)
     {
         [_abaTeacher removeFromSuperview];
     }];
}

-(void) configProgressTitle {
    
    [self addTitleNavbarSection:STRTRAD(@"sectionVocabularyTitleKey", @"Vocabulario") andSubtitle:[_vocabularyController getPercentageForSection:self.section]];

    // Title label
    NSString *str0 = [NSString stringWithFormat:@"%ld",(long)[_vocabularyController getTotalPhraseDoneForVocabularySection:self.section]];
    NSString *str1 = STRTRAD(@"sectionVocabularyTitledeKey", @"de");
    NSString *str2 = [NSString stringWithFormat:@"%lu",(unsigned long)self.section.content.count];
    if ([self.section.progress integerValue] >= 100) {
        str0 = str2;
    }
    NSString *str = [NSString stringWithFormat:@"%@ %@ %@", str0, str1, str2];
    
    NSMutableAttributedString *mutAttStr = [[NSMutableAttributedString alloc] initWithString:str];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SLAB_900 size:IS_IPAD?24:16]
                      range:[str rangeOfString:str0]];
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:ABA_COLOR_DARKGREY
                      range:[str rangeOfString:str0]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SLAB_500 size:IS_IPAD?18:14]
                      range:[str rangeOfString:str1]];
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:ABA_COLOR_DARKGREY
                      range:[str rangeOfString:str1]];
    
    [mutAttStr addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:ABA_MUSEO_SLAB_900 size:IS_IPAD?24:16]
                      range:[str rangeOfString:str2 options:NSBackwardsSearch]];
    [mutAttStr addAttribute:NSForegroundColorAttributeName
                      value:[UIColor whiteColor]
                      range:[str rangeOfString:str2 options:NSBackwardsSearch]];
    
    [_progressTitle setAttributedText:mutAttStr.copy];
    [self addBackButtonNavBar];
}

-(ABAVocabularyPhrase *) getCurrentPhrase {
    
    for(ABAVocabularyPhrase *phrase in self.section.content) {
        
        if(![phrase.done boolValue]) {
            
            [[Crashlytics sharedInstance] setObjectValue:phrase.text forKey:@"Current phrase"];
            [[LegacyTrackingManager sharedManager] trackInteraction];
            return phrase;
        }
    }
    
    return nil;
}

-(void)showCompletedAlertIfNeeded {
    if(!_sectionCompleted && [_vocabularyController isSectionCompleted:self.section]) {
        [self.delegate showAlertCompletedUnit:kABAVocabularySectionType];
    }
}

#pragma mark - UICollectionView custom methods

-(float) calculateHeightDialogLabel:(UILabel *) virtualLabel withPhrase:(ABAVocabularyPhrase *) cellVocabularyPhrase {
    
    CGFloat leftDistanceLabel, rightDistanceLabel, virtualHeight;
    leftDistanceLabel = 14;
    rightDistanceLabel = 30;
    
    virtualLabel.numberOfLines = 0;
    virtualLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(_collectionView.frame.size.width -leftDistanceLabel -rightDistanceLabel, 9999);
    
    CGSize size = [virtualLabel sizeThatFits:maximumLabelSize];
    virtualHeight = size.height;
    
	if(virtualHeight < IS_IPAD?minHeighCollectionCell_iPad:minHeighCollectionCell)
		virtualHeight = IS_IPAD?minHeighCollectionCell_iPad:minHeighCollectionCell;
    
    return virtualHeight;
}

-(void) configLabel:(UILabel *) cellTextLabel withPhrase:(ABAVocabularyPhrase *) cellVocabularyPhrase {
    
    NSMutableAttributedString *mutAttStr  = [[NSMutableAttributedString alloc] init];
    NSString *str;
    NSString *text = cellVocabularyPhrase.text;
    NSString *translation = [NSString stringWithFormat:@"%@ %@",cellVocabularyPhrase.wordType, cellVocabularyPhrase.translation];
    if(translation == nil || [translation isEqualToString:@""]) {
        str = [NSString stringWithFormat:@" %@", text];
    }
    else {
        str = [NSString stringWithFormat:@"%@\n%@", text, translation];
    }
    
    float spacing = 7.0f;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    paragraphStyle.lineSpacing = spacing;
    
    NSMutableAttributedString * strAtt = [[NSMutableAttributedString alloc] initWithString:str];
    
    if([cellVocabularyPhrase isEqualToObject: _currentPhrase]) {
	
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:18]
                       range:[str rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_DARKGREY
                       range:[str rangeOfString:text]];
        
        if(translation != nil && ![translation isEqualToString:@""]) {
            
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                           range:[str rangeOfString:translation]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_DARKGREY
                           range:[str rangeOfString:translation]];
        }
        
    }
    if([cellVocabularyPhrase.done boolValue]) {
        
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:18]
                       range:[str rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_REG_MAIL_BUT
                       range:[str rangeOfString:text]];
        
        if(translation != nil && ![translation isEqualToString:@""]) {
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                           range:[str rangeOfString:translation]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_DARKGREY
                           range:[str rangeOfString:translation]];
        }
        
    }
    if(![cellVocabularyPhrase isEqualToObject:_currentPhrase] && ![cellVocabularyPhrase.done boolValue]) {
        
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:18]
                       range:[str rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_DARKGREY
                       range:[str rangeOfString:text]];
        
        if(translation != nil && ![translation isEqualToString:@""]) {
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                           range:[str rangeOfString:translation]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_DARKGREY
                           range:[str rangeOfString:translation]];
        }
        
    }
    if ([self.section.progress integerValue] >= 100) {
        [strAtt addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:18]
                       range:[str rangeOfString:text]];
        [strAtt addAttribute:NSForegroundColorAttributeName
                       value:ABA_COLOR_REG_MAIL_BUT
                       range:[str rangeOfString:text]];
        
        if(translation != nil && ![translation isEqualToString:@""]) {
            [strAtt addAttribute:NSFontAttributeName
                           value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]
                           range:[str rangeOfString:translation]];
            [strAtt addAttribute:NSForegroundColorAttributeName
                           value:ABA_COLOR_DARKGREY
                           range:[str rangeOfString:translation]];
        }
    }
    
    [mutAttStr appendAttributedString:strAtt];
    
    [cellTextLabel setAttributedText:mutAttStr.copy];
    cellTextLabel.lineBreakMode = NSLineBreakByTruncatingTail;
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return _dataSource.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    return 1;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABAVocabularyPhrase *cellVocabularyPhrase = [_dataSource objectAtIndex:indexPath.row];
    
    UILabel *virtualLabel = [[UILabel alloc] init];
    
    [self configLabel:virtualLabel withPhrase:cellVocabularyPhrase];
    
    float cellHeight = [self calculateHeightDialogLabel:virtualLabel withPhrase:cellVocabularyPhrase];

    return CGSizeMake(_collectionView.frame.size.width, cellHeight);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
	VocabularyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IS_IPAD?cellVocabulary_iPad:cellVocabulary forIndexPath:indexPath];
    
    ABAVocabularyPhrase *cellVocabularyPhrase = [_dataSource objectAtIndex:indexPath.row];
    
    [self configLabel:cell.txtLabel withPhrase:cellVocabularyPhrase];
    
    if([cellVocabularyPhrase isEqualToObject: _currentPhrase]) {
        cell.backgroundColor = ABA_COLOR_REG_BACKGROUND;
    }
    else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return  cell;
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ABAVocabularyPhrase *vocabularyPhraseSelected = [_dataSource objectAtIndex:indexPath.row];
    _currentPhrase = vocabularyPhraseSelected;
    _currentStatus = kABAVocabularyListen;
    [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAVocabularySectionType];
    [_controlView showAuxiliaryButtons:NO];
    [_collectionView reloadData];
    [_collectionView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
    [self listenButtonTapped];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(_selectionDisabled) {
        return NO;
    }
    return YES;
}

#pragma mark ActionButton delegates

-(void) listenButtonTapped
{
    if(_currentPhrase != nil) {
        [_controlView.actionButton removeButtonTapRecognizer];
        
        [self hideTeacherView];
        
        _selectionDisabled = YES;
        
        _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
        
        [_collectionView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
        
        [_audioController playAudioFile:_currentPhrase.audioFile withUnit: self.section.unit forRec:NO];
    }
}

-(void) stopRecordButtonTapped {

	dispatch_async(dispatch_get_main_queue(), ^
				   {
                       [_controlView.actionButton removeButtonTapRecognizer];

					   [_controlView.actionButton stopRecordingAnimation];
					   
					   [_controlView hideRecordingTime];
					   
					   self.currentStatus = kABASpeakCompareRecording;
					   
					   [_controlView.actionButton switchToType:kABAActionButtonTypeCompare forSection:kABAVocabularySectionType];
				   });
	
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1*NSEC_PER_SEC), dispatch_get_main_queue(), ^
				   {
					   [_audioController stopRecording];
				   });
}

-(void) continueButtonTapped {
    
    [_controlView.actionButton removeButtonTapRecognizer];
    [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAVocabularySectionType];
    [_controlView showAuxiliaryButtons:NO];
    _currentStatus = kABAVocabularyListen;
    
    if (_sectionCompleted) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if([_vocabularyController isSectionCompleted:self.section]) {

        __weak typeof(self) weakSelf = self;
        
        [_vocabularyController setCompletedSection:self.section
                                   completionBlock:^(NSError *error, id object)
         {
             if(!error)
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^
                                {
                                    [weakSelf.delegate showAlertCompletedUnit:kABAVocabularySectionType];
                                    [weakSelf.navigationController popViewControllerAnimated:YES];
                                });
             }
             else
             {
                 // alarm!
             }
         }];

    }
    else {

        _currentPhrase = [self getCurrentPhrase];
        [self configProgressTitle];
        [_collectionView reloadData];
        [_collectionView setContentOffset:CGPointMake(0, [self calculateTopOffset:[self isFirstDialog]]) animated:YES];
        [self listenButtonTapped];
        
    }
}

-(void)compareButtonTapped
{
    [self startComparing];
}

-(void) prepareForComparing {
    
    _currentStatus = kABAVocabularyCompareRecording;
    
    [_controlView.actionButton switchToType:kABAActionButtonTypeCompare forSection:kABAVocabularySectionType];
    _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
}

-(void) startComparing {
    [self prepareForComparing];
    
    //play my recording
    [_audioController playAudioFile:_currentPhrase.audioFile withUnit: self.section.unit forRec:YES];
}

-(void) playOriginal {
    
    _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
    
    [_audioController playAudioFile:_currentPhrase.audioFile withUnit: self.section.unit forRec:NO];
}

- (void)controlViewRedoButtonTapped {
    
    _currentStatus = kABAVocabularyListen;
    [_controlView.actionButton switchToType:kABAActionButtonTypeListen forSection:kABAVocabularySectionType];
    [self listenButtonTapped];
    [_controlView showAuxiliaryButtons:NO];
}

- (void)controlViewCompareButtonTapped {
    
    _currentStatus = kABAVocabularyCompareRecording;
    [_controlView.actionButton switchToType:kABAActionButtonTypeCompare forSection:kABAVocabularySectionType];
    [self startComparing];
    [_controlView showAuxiliaryButtons:NO];
}

#pragma mark AudioControllerDelegate

-(void) audioStarted
{
    if(_currentStatus == kABAVocabularyBeginRecordSound)
    {
        self.currentStatus = kABAVocabularyRec;
        
        _audioController.pulseViewController = _controlView.actionButton.pulseViewController;
        
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1*NSEC_PER_SEC), dispatch_get_main_queue(), ^ {
            [weakSelf.audioController recordAudioFile:_currentPhrase.audioFile withUnit:weakSelf.section.unit];

#ifdef SHEPHERD_DEBUG
            if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [weakSelf stopRecordButtonTapped];
                });
            }
#endif

        });
    }
}

- (void)audioFinished
{
    switch (_currentStatus)
    {
    case kABAVocabularyListen:
    {
        _audioController.pulseViewController = nil;

        __weak typeof(self) weakSelf = self;

        [_phrasesController setPhrasesListened:@[ _currentPhrase ] forSection:self.section sendProgressUpdate:NO completionBlock:^(NSError *error, id object) {
            weakSelf.currentStatus = kABAVocabularyBeginRecordSound;
            [weakSelf.audioController playSound:@"beep.mp3"];
        }];

        // Cooladata tracking
        [CoolaDataTrackingManager trackListenedToAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaVocabulary exerciseId:_currentPhrase.audioFile];

        break;
    }

    case kABAVocabularyCompareRecording:
    {
        _currentStatus = kABAVocabularyCompare;
        _audioController.pulseViewController = nil;

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self playOriginal];
        });

        break;
    }

    case kABAVocabularyCompare:
    {
        _audioController.pulseViewController = nil;
        _currentStatus = kABAVocabularyContinue;

        // Cooladata tracking
        [CoolaDataTrackingManager trackComparedAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaVocabulary exerciseId:_currentPhrase.audioFile];
        
        [self markPhraseAsDone];
        break;
    }

    default:
        NSLog(@"Unexpected status");
        break;
    }
}

-(void) markPhraseAsDone {
    
    __weak typeof(self) weakSelf = self;
    
    [_phrasesController setPhrasesDone:@[_currentPhrase] forSection:self.section sendProgressUpdate: YES completionBlock:^(NSError *error, id object)
     {
         if(!error)
         {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^ {
                 weakSelf.selectionDisabled = NO;
                 [weakSelf configProgressTitle];
                 [weakSelf.collectionView reloadData];
                 [weakSelf.controlView.actionButton switchToType:kABAActionButtonTypeContinue forSection:kABAVocabularySectionType];
                 [weakSelf.controlView.actionButton addButtonTapRecognizer];
                 [weakSelf.controlView showAuxiliaryButtons:YES];

#ifdef SHEPHERD_DEBUG
                 if ([ABAShepherdAutomatorPlugin isAutomationEnabled]) {
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [weakSelf continueButtonTapped];
                     });
                 }
#endif
             });
         }
         else
         {
             // alarm!
         }
     }];
}

-(void) audioDidGetError:(NSError *)error
{
	[self audioFinished];
	
	//    _audioController.pulseViewController = nil;
	//
	//	[_controlView.actionButton addButtonTapRecognizer];
	//
	//    if (_abaAlert)
	//    {
	//        [_abaAlert dismiss];
	//    }
	//    _abaAlert = [[ABAAlert alloc] initAlertWithText:error.localizedDescription];
	//    [_abaAlert showWithOffset:44];
}

- (void)recordingDidGetError:(NSError *)error
{
	[self recordingFinished];
	
	//    _audioController.pulseViewController = nil;
	//    [_controlView hideRecordingTime];
	//
	//	[_controlView.actionButton addButtonTapRecognizer];
	//
	//    if (_abaAlert)
	//    {
	//        [_abaAlert dismiss];
	//    }
	//    _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"recordingInterpretErrorKey", @"Imposible grabar sonido :-(")];
	//    [_abaAlert showWithOffset:44];
}

-(void) recordingStarted
{
    [_controlView.actionButton switchToType:kABAActionButtonTypeRecord forSection:kABAVocabularySectionType];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^
    {
        [_controlView.actionButton addButtonTapRecognizer];
    });
   
    [_controlView showRecordingTime];
}

- (void)recordingFinished
{
    [_controlView hideRecordingTime];
    [_controlView.actionButton stopRecordingAnimation];

    _audioController.pulseViewController = nil;
    [_audioController.recorderPulseTimer invalidate];
    _audioController.recorderPulseTimer = nil;

    UIApplicationState state = [[UIApplication sharedApplication] applicationState];

    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        [self prepareForComparing];
    }
    else
    {
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf startComparing];
        });
    }

    // Cooladata tracking
    [CoolaDataTrackingManager trackRecordedAudio:[UserController currentUser].idUser levelId:self.section.unit.level.idLevel unitId:self.section.unit.idUnitString sectionType:CooladataSectionTypeAbaVocabulary exerciseId:_currentPhrase.audioFile];
}

@end
