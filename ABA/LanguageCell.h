//
//  LanguageCell.h
//  ABA
//
//  Created by Jordi Mele on 12/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLanguageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *languageImage;


@end
