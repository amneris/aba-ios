//
//  FormValidator.swift
//  ABA
//
//  Created by MBP13 Jesus on 03/10/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol FormValidator {
    
    func parameterValidation(_ email: String, pass: String) -> LoginParametersValidation
    func parameterValidation(_ name: String, email: String, pass: String) -> LoginParametersValidation
    
    func validateName(_ name: String?)
    func validateEmail(_ email: String?)
    func validatePassword(_ password: String?)
    
    func isNameValid(_ name: String?) -> Bool
    func isEmailValid(_ email: String?) -> Bool
    func isPasswordValid(_ password: String?) -> Bool
}

extension FormValidator {
    
    func parameterValidation(_ email: String, pass: String) -> LoginParametersValidation {
        if MDTUtils.isTextEmpty(email) {
            return .EmptyEmail
        } else if !MDTUtils.isEmailValid(email) {
            return .WrongEmailFormat
        }
        else if MDTUtils.isTextEmpty(pass) {
            return .EmptyPassword
        }
        return .Success
    }
    
    func parameterValidation(_ name: String, email: String, pass: String) -> LoginParametersValidation {
        if !isNameValid(name) {
            return .EmptyName
        }
        else if MDTUtils.isTextEmpty(email) {
            return .EmptyEmail
        }
        else if !isEmailValid(email) {
            return .WrongEmailFormat
        }
        else if MDTUtils.isTextEmpty(pass) {
            return .EmptyPassword
        }
        else if !isPasswordValid(pass) {
            return .WrongPasswordFormat
        }
        
        return .Success
    }
    
    func isNameValid(_ name: String?) -> Bool {
        return !MDTUtils.isNameEmpty(name)
    }
    
    func isEmailValid(_ email: String?) -> Bool {
        return !MDTUtils.isTextEmpty(email) && MDTUtils.isEmailValid(email)
    }
    
    func isPasswordValid(_ password: String?) -> Bool {
        return (!MDTUtils.isTextEmpty(password) && MDTUtils.isCorrectPassword(password))
    }
}
