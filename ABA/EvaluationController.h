//
//  EvaluationController.h
//  ABA
//
//  Created by Ivan Ferrera on 27/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "SectionController.h"

@class ABAEvaluation, ABAEvaluationQuestion, ABAEvaluationOption;

@interface EvaluationController : SectionController

/**
 *  This method, checks if the answer is correct or not
 *
 *  @param question the question answered
 *  @param option   the option selected
 */
+ (void)evaluationQuestionAnsweredWithOption:(ABAEvaluationOption *)option completionBlock:(CompletionBlock)completionBlock;

/**
 *  This method returns an integer from 0-10 with the correct answers from an evaluation
 *
 *  @param evaluation the evaluation object
 *
 *  @return 0-10
 */
+ (NSInteger)getEvaluationCorrectAnswers:(ABAEvaluation *)evaluation;

/**
 *  This method returns the answers of the evaluation marked by the user
 *
 *  @param evaluation the evaluation object
 *
 *  @return NSOrderedSet of ABAEvaluationOption
 */
+ (NSOrderedSet *)getEvaluationAnswers:(ABAEvaluation *)evaluation;

/**
 *  This method reloads the answers of the evaluation, setting all to not-answered.
 *
 *  @param evaluation the evaluation to reload
 */
+ (void)reloadEvaluationAnswers:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock;

+ (void)reloadWrongAnswers:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock;

- (void)endEvaluationWithOK:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock;

- (void)endEvaluationWithKO:(ABAEvaluation *)evaluation completionBlock:(CompletionBlock)completionBlock;

@end
