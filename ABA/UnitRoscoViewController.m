//
//  UnitViewController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "Notifications.h"

#import "UnitRoscoViewController.h"
#import "VideoViewController.h"
#import "SpeakViewController.h"
#import "WriteViewController.h"
#import "InterpretViewController.h"
#import "ExercicesViewController.h"
#import "VocabularyViewController.h"
#import "EvaluationViewController.h"
#import "EvaluationQuestionViewController.h"
#import "ABACustomSectionView.h"
#import "ABASpeakDialog.h"
#import "ABAEvaluationQuestion.h"
#import "ABAUnit.h"
#import "Utils.h"
#import "UIViewController+ABA.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import "InitialPlansViewController.h"
#import "ABAAlert.h"
#import "ABAAlertCompletedUnit.h"
#import "ABATeacherTipView.h"
#import "LegacyTrackingManager.h"
#import "ImageHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AudioController.h"
#import "ExercisesController.h"
#import "InterpretController.h"
#import "SpeakController.h"
#import "VocabularyController.h"
#import "WriteController.h"
#import "LevelUnitController.h"
#import "ABARealmLevel.h"
#import "FilmController.h"
#import "DownloadController.h"
#import "APIManager.h"
#import "ABAPopup.h"
#import "EvaluationController.h"
#import "LGAlertView.h"
#import "ABAExperiment.h"
#import "ExperimentRunner.h"
#import "ABARoscoController.h"
#import "ABAFilm.h"
#import "ABASection+Methods.h"
@import SVProgressHUD;
#import "ABA-Swift.h"

// Analytics
#import "MixpanelTrackingManager.h"
#import <Crashlytics/Crashlytics.h>


@interface UnitRoscoViewController () <RoscoControllerDelegate, SectionProtocol>

@property(weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property(weak, nonatomic) IBOutlet UIView *containerView;

@property ABAAlert *abaAlert;
@property ABAAlert *abaInfoAlert;
@property BOOL lockInfoAlert;

@property ABAAlertCompletedUnit *completedAlert;

@property BOOL firstAnimationPassed;
@property BOOL mustShowCompletedAlert;

@property(nonatomic, strong) ABATeacherTipView *abaTeacher;

@property(nonatomic, strong) ABAPopup *abaPopup;
@property(nonatomic, strong) UIButton *downloadButton;

// controllers
@property(strong, nonatomic) DownloadController *downloadController;
@property(strong, nonatomic) ABARoscoController *roscoController;

@property(nonatomic, strong) ABAUnit *unit;
@end

@implementation UnitRoscoViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        _firstAnimationPassed = NO;
        _mustShowCompletedAlert = NO;
    }

    return self;
}

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([UserController isUserPremium])
    {
        [self addDownloadButton];
        [self toggleDownloadButton:[DownloadController shouldDownloadOfflineModeForUnit:_unit]];
    }

    // Hack for mantain infoAlert TODO: Find better approach to get HUD id or etc
    _lockInfoAlert = NO;

    [self addBackButtonNavBar];

    [self addTitleNavbar:[NSString stringWithFormat:@"%@ %@", STRTRAD(@"unitMenuKey", @"Unidad"), _unit.idUnit] andSubtitle:_unit.title andProgress:[_unit getPercentage]];

    [self setupBackgroundImage];

    _roscoController = [ABARoscoController roscoControllerWithUnit:_unit containerView:_containerView];
    [_roscoController setDelegate:self];
    CLS_LOG(@"UnitViewController: entering unit %@", _unit.idUnit);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [[LegacyTrackingManager sharedManager] trackPageView:@"sectionlist"];

    if (!_firstAnimationPassed)
    {
        [_roscoController loadSectionDataAnimated:YES];
        _firstAnimationPassed = YES;
    }
    else
    {
        [_roscoController loadSectionDataAnimated:NO];
    }
}


-(void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(progressHUDDidDisappear:)
                                                 name:SVProgressHUDDidDisappearNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unitProgressUpdated) name:kUnitViewProgressUpdatedNotification object:nil];

    [self setupTeacherView];

    BOOL isLandscape = NO;
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight)
    {
        isLandscape = YES;
    }

    [_roscoController udpateConstraintsIsLandscape:isLandscape shouldHideMenuView:YES];

    if ([_containerView.motionEffects count] == 0 || _containerView.motionEffects == nil)
    {
        [self addMotionForce];
    }

    [self updateTitleWithProgress];

    if (_mustShowCompletedAlert)
    {
        [self showCompletedAlert];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [SVProgressHUD dismiss];

    if (_abaAlert)
    {
        [_abaAlert dismiss];
    }

    if (_abaInfoAlert)
    {
        [_abaInfoAlert dismiss];
    }

    if (_completedAlert)
    {
        [_completedAlert removeFromSuperview];
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self name:SVProgressHUDDidDisappearNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUnitViewProgressUpdatedNotification object:nil];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    BOOL isLandscape = NO;
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        isLandscape = YES;
    }
    [_roscoController udpateConstraintsIsLandscape:isLandscape shouldHideMenuView:NO];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [_roscoController setupCenterView];
}

#pragma mark - Public methods

- (void)setUnit:(ABAUnit *)unit
{
    _unit = unit;
}

#pragma mark - Private methods

-(void)addDownloadButton
{
    _downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _downloadButton.frame = CGRectMake(0,0,40,40);
    [_downloadButton setTintColor:[UIColor whiteColor]];

    UIImage *image = [UIImage imageNamed:@"icon_download"];
    [_downloadButton addTarget:self action:@selector(downloadOfflineMode) forControlEvents:UIControlEventTouchUpInside];

    [_downloadButton setBackgroundImage:image forState:UIControlStateNormal];
    UIBarButtonItem *dButton = [[UIBarButtonItem alloc] initWithCustomView:_downloadButton];
    self.navigationItem.rightBarButtonItem = dButton;
}

-(void)toggleDownloadButton:(BOOL)status
{
    UIImage *image = nil;

    if (status)
    {
        image = [UIImage imageNamed:@"icon_download"];
        [_downloadButton setBackgroundImage:image forState:UIControlStateNormal];
        [_downloadButton addTarget:self action:@selector(downloadOfflineMode) forControlEvents:UIControlEventTouchUpInside];
        [[Crashlytics sharedInstance] setObjectValue:@"NO" forKey:@"Unit downloaded"];
    }
    else
    {
        image = [UIImage imageNamed:@"icon_downloaded"];
        [_downloadButton setBackgroundImage:image forState:UIControlStateNormal];
        [_downloadButton setUserInteractionEnabled:NO];
        [[Crashlytics sharedInstance] setObjectValue:@"YES" forKey:@"Unit downloaded"];
    }
}

- (void)downloadOfflineMode
{
    if ([[APIManager sharedManager] getReachabilityStatus] == AFNetworkReachabilityStatusNotReachable)
    {
        [self showConnectionAlert];
        return;
    }
    __weak typeof(self) weakSelf = self;

    __block LGAlertView *alertView = [LGAlertView alertViewWithProgressViewStyleWithTitle:nil
                                                                                  message:STRTRAD(@"offlineDownloadingFileKey", @"message")
                                                                        progressLabelText:@"0 %"
                                                                             buttonTitles:nil
                                                                        cancelButtonTitle:STRTRAD(@"alertSubscriptionKOButton", @"Cancel")
                                                                   destructiveButtonTitle:nil];
    [alertView setDidDismissHandler:^(LGAlertView *alert)
    {
        [weakSelf.downloadController cancelDownload];
     }];
    
    [alertView setProgress:0.0f progressLabelText:@"0 %"];
    [alertView showAnimated:YES completionHandler:^{}];

    alertView.cancelOnTouch = NO;

    if (_downloadController)
    {
        [_downloadController cancelDownload];
        _downloadController = nil;
    }

    _downloadController = [DownloadController controllerWithUnit:_unit
                                        andDownloadProgressBlock:^(NSError *error, float progress, BOOL didFinish) {
                                            
                                            if (error)
                                            {
                                                CLS_LOG(@"UnitViewController: Error downloading unit %@", _unit.idUnit);
                                                NSLog(@"error: %@ %@", error, error.localizedDescription);

                                                [alertView dismissAnimated:YES completionHandler:nil];
                                                
                                                if (didFinish)
                                                {
                                                    [weakSelf showAlertWithKey:@"offlineDownloadStoppedKey" andText:@"Download stopped"];
                                                }
                                                else
                                                {
                                                    [weakSelf showAlertWithKey:@"offlineDownloadErrorKey" andText:@"Download error"];

                                                    [[MixpanelTrackingManager sharedManager] trackDownloadError:self.unit];
                                                }

                                                [weakSelf.roscoController loadSectionDataAnimated:NO];
                                                [weakSelf updateImages];
                                            }
                                            else if (didFinish)
                                            {
                                                [alertView dismissAnimated:YES completionHandler:^{}];

                                                _firstAnimationPassed = NO;

                                                [weakSelf.roscoController loadSectionDataAnimated:YES];
                                                [weakSelf updateImages];
                                                [weakSelf toggleDownloadButton:NO];

                                                [[MixpanelTrackingManager sharedManager] trackDownloadFinish:self.unit];
                                            }
                                            else
                                            {
                                                float currentProgress = progress / 100;
                                                [alertView setProgress:currentProgress progressLabelText:[NSString stringWithFormat:@"%ld %%", (long)progress]];
                                            }
                                        }];

    [_downloadController startDownload];
    [[MixpanelTrackingManager sharedManager] trackDownloadStarted:self.unit];
}

-(void)addMotionForce
{
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-15);
    verticalMotionEffect.maximumRelativeValue = @(15);

    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-15);
    horizontalMotionEffect.maximumRelativeValue = @(15);

    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[ horizontalMotionEffect, verticalMotionEffect ];

    // Add both effects to your view
    [_containerView addMotionEffect:group];
}

-(void)openEvaluationSection
{
    BOOL restartEvaluation = NO;

    for (ABAEvaluationQuestion *question in self.unit.sectionEvaluation.content)
    {
        if (question.answered != nil)
        {
            restartEvaluation = YES;
            break;
        }
    }

    if (restartEvaluation)
    {
        EvaluationQuestionViewController *questionViewController = [[UIStoryboard storyboardWithName:@"Evaluation" bundle:nil] instantiateViewControllerWithIdentifier:@"EvaluationQuestionViewController"];
        [questionViewController setSection:self.unit.sectionEvaluation];
        [self.navigationController pushViewController:questionViewController animated:YES];
    }
    else
    {
        [EvaluationController reloadEvaluationAnswers:self.unit.sectionEvaluation
                                      completionBlock:^(NSError *error, id object) {
                                          EvaluationViewController *evaluationViewController = [[UIStoryboard storyboardWithName:@"Evaluation" bundle:nil] instantiateInitialViewController];
                                          [evaluationViewController setSection:self.unit.sectionEvaluation];
                                          [self.navigationController pushViewController:evaluationViewController animated:YES];
                                      }];
    }

}

- (void)showAlertWithKey:(NSString *)key andText:(NSString *)text
{
    _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(key, text)];
    [_abaAlert showWithOffset:44];
}

- (void)showAlertCompletedUnit:(kABASectionTypes)type
{
    CGRect initialFrame = CGRectMake(0, 0, self.view.frame.size.width, 100.0f);

    _completedAlert = [[ABAAlertCompletedUnit alloc] initWithFrame:initialFrame andSectionType:type];

    _mustShowCompletedAlert = YES;

    if (type == kABAInterpretSectionType)
    {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:kDontShowRateApp])
        {
            _abaPopup = [ABAPopup abaPopupWithType:kABAPopupTypeRate];
            [_abaPopup showInView:self.view];
        }
    }
}

- (void)showCompletedAlert
{
    [self.view addSubview:_completedAlert];

    CGRect newFrame = CGRectMake(0, -300, _completedAlert.frame.size.width, _completedAlert.frame.size.height);

    [UIView animateWithDuration:1.0
        delay:4.5
        options:UIViewAnimationOptionCurveEaseOut
        animations:^{
            [_completedAlert setFrame:newFrame];
        }
        completion:^(BOOL finished) {
            if (finished)
            {
                [_completedAlert removeFromSuperview];
                _mustShowCompletedAlert = NO;
            }
        }];
}

#pragma mark - Private methods (Notifications)

- (void)unitProgressUpdated
{
    [_roscoController refreshProgressUnit];
}

- (void)progressHUDDidDisappear:(NSNotification *)notification
{
    if (_abaInfoAlert)
    {
        [_abaInfoAlert dismiss];
    }

    _lockInfoAlert = YES;
}

#pragma mark - UI

- (CGFloat)getTeacherViewHeight
{
    NSString *teacherTextKey = [NSString stringWithFormat:@"unitTeacherText%@", _unit.idUnit];
    ABATeacherTipView *getHeightForAbaTeacher = [[ABATeacherTipView alloc] initTeacherWithText:STRTRAD(teacherTextKey, @"teacherTextKey") withImage:@"teacher" withShadow:NO];
    return getHeightForAbaTeacher.frame.size.height;
}

- (void)setupBackgroundImage
{
    NSString *urlString = [ImageHelper change3xIfNeededWithUrlString:_unit.filmImageInactiveUrl];
    NSURL *imageUrl = [NSURL URLWithString:urlString];
    
    [_backgroundImage sd_setImageWithPreviousCachedImageWithURL:imageUrl placeholderImage:nil options:SDWebImageRetryFailed progress:nil completed:nil];
}

- (void)setupTeacherView
{
    if ([self.view.subviews containsObject:_abaTeacher])
    {
        [_abaTeacher removeFromSuperview];
    }

    NSString *teacherTextKey = [NSString stringWithFormat:@"unitTeacherText%@", _unit.idUnit];
    teacherTextKey = STRTRAD(teacherTextKey, @"teacherTextKey");

    kABASectionTypes currentSectionType = [ABAUnit getCurrentSectionForUnit:_unit];
    NSString *currentSectionName = [ABASection localizedNameforType:currentSectionType];

   if ([_unit.progress integerValue] > 0 && ![_unit.completed boolValue])
    {
        if(currentSectionType == kABASpeakSectionType)
        {
            teacherTextKey = [NSString stringWithFormat:@"%@", STRTRAD(@"unitTeacherFilmContinueKey", @"Ahora puedes continuar con la sección Habla o volver a ver el ABA Film")];
        }
        else if (currentSectionType == kABAAssessmentSectionType)
        {
            teacherTextKey = [NSString stringWithFormat:@"%@", STRTRAD(@"unitContinueSectionAssesment", @"¡Enhorabuena! ahora estas listo para realizar la prueba final de esta unidad.")];
        }
        else
        {
            teacherTextKey = [NSString stringWithFormat:@"%@ %@", STRTRAD(@"unitContinueSectionTeacherText", @"Continúa con la sección"), currentSectionName];
        }
    }

    else if ([_unit.completed boolValue])
    {
        NSString *completedText = [NSString stringWithFormat:@"unitCompletedTeacherText%@", _unit.idUnit];
        teacherTextKey = STRTRAD(completedText, @"teacherCompleteTextKey");
    }

    else if (_newRegister)
    {
        _newRegister = NO;
        ABARealmUser *user = UserController.currentUser;

        teacherTextKey = [NSString stringWithFormat:STRTRAD(@"unitTeacherNewRegister", @"Te estoy mostrando la primera unidad del nivel seleccionado, empieza aquí ....."), user.teacherName];
    }

    _abaTeacher = [[ABATeacherTipView alloc] initTeacherWithText:teacherTextKey withImage:@"teacher" withShadow:NO];

    // setting up constrains
    if ([self.view.subviews containsObject:_completedAlert])
    {
        [self.view insertSubview:_abaTeacher belowSubview:_completedAlert];
    }
    else
    {
        [self.view addSubview:_abaTeacher];
    }
    
    [_abaTeacher setConstraints];
}


#pragma mark - RoscoControllerDelegate

- (void)didSelectSectionWithType:(kABASectionTypes)type
{
    switch (type)
    {
        case kABAFilmSectionType:
        {
            VideoViewController *videoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
            [videoViewController setSection:(ABASection *)self.unit.sectionFilm];
            [videoViewController setCurrentSection:kABAFilmSectionType];
            [videoViewController setDelegate:self];
            
            [[Crashlytics sharedInstance] setObjectValue:@"Film" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaFilm];
            
            [self.navigationController pushViewController:videoViewController animated:YES];
            
            break;
        }
        case kABASpeakSectionType:
        {
            SpeakViewController *speakViewController = [[UIStoryboard storyboardWithName:@"Speak" bundle:nil] instantiateInitialViewController];
            [speakViewController setSection:self.unit.sectionSpeak];
            [speakViewController setDelegate:self];
            
            [[Crashlytics sharedInstance] setObjectValue:@"Speak" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaSpeak];
            
            [self.navigationController pushViewController:speakViewController animated:YES];
            
            break;
        }
        case kABAWriteSectionType:
        {
            WriteViewController *writeViewController = [[UIStoryboard storyboardWithName:@"Write" bundle:nil] instantiateInitialViewController];
            [writeViewController setSection:(ABAWrite *)self.unit.sectionWrite];
            [writeViewController setDelegate:self];

            [[Crashlytics sharedInstance] setObjectValue:@"Write" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaWrite];

            [self.navigationController pushViewController:writeViewController animated:YES];
            
            break;
        }
        case kABAInterpretSectionType:
        {
            InterpretViewController *interpretViewController = [[UIStoryboard storyboardWithName:@"Interpret" bundle:nil] instantiateInitialViewController];
            [interpretViewController setSection:self.unit.sectionInterpret];
            [interpretViewController setDelegate:self];

            [[Crashlytics sharedInstance] setObjectValue:@"Interpret" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaInterpret];

            [self.navigationController pushViewController:interpretViewController animated:YES];
            
            break;
        }
        case kABAVideoClassSectionType:
        {
            VideoViewController *videoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoViewController"];
            [videoViewController setSection:(ABASection *)self.unit.sectionVideoClass];
            [videoViewController setCurrentSection:kABAVideoClassSectionType];
            [videoViewController setDelegate:self];
            
            [[Crashlytics sharedInstance] setObjectValue:@"Videoclass" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaVideoClass];

            [self.navigationController pushViewController:videoViewController animated:YES];
            
            break;
        }
        case kABAExercisesSectionType:
        {
            ExercicesViewController *exercicesViewController = [[UIStoryboard storyboardWithName:@"Exercices" bundle:nil] instantiateInitialViewController];
            [exercicesViewController setSection:self.unit.sectionExercises];
            [exercicesViewController setDelegate:self];
            
            [[Crashlytics sharedInstance] setObjectValue:@"Exercises" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaExercises];

            [self.navigationController pushViewController:exercicesViewController animated:YES];
            
            break;
        }
        case kABAVocabularySectionType:
        {
            VocabularyViewController *vocabularyViewController = [[UIStoryboard storyboardWithName:@"Vocabulary" bundle:nil] instantiateInitialViewController];
            [vocabularyViewController setSection:self.unit.sectionVocabulary];
            [vocabularyViewController setDelegate:self];
            
            [[Crashlytics sharedInstance] setObjectValue:@"Vocabulary" forKey:@"Entered section"];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaVocabulary];

            [self.navigationController pushViewController:vocabularyViewController animated:YES];
            
            break;
        }
            
        case kABAAssessmentSectionType:
        {
            [self openEvaluationSection];
            [CoolaDataTrackingManager trackEnteredSection:[UserController currentUser].idUser levelId:self.unit.level.idLevel unitId:self.unit.idUnitString sectionType:CooladataSectionTypeAbaEvaluation];
            
            break;
        }
            
        default:
            break;
    }
};

- (void)showPlans
{
    InitialPlansViewController *plansViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"InitialPlansViewController"];
    plansViewController.backButton = YES;
    [self.navigationController pushViewController:plansViewController animated:YES];
}

-(void)showConnectionAlert
{
    [self showAlertWithKey:@"getAllSectionsForUnitErrorKey" andText:@"¡Ups! Ha sido imposible cargar el contenido de las secciones. Por favor, revisa tu conexión a Internet."];
}

- (void)showLockMessage
{
    kABASectionTypes currentSectionType = [ABAUnit getCurrentSectionForUnit:_unit];
    NSString *currentSectionName = [ABASection localizedNameforType:currentSectionType];

    _abaInfoAlert = [[ABAAlert alloc] initAlertHelpWithText: [NSString stringWithFormat:@"%@ %@", STRTRAD(@"sectionLockMessageAlert", @"Debes realizar primero la sección"), currentSectionName]];
    [_abaInfoAlert showWithOffset:44 andDissmisAfter:1.0f];
}

- (void)showAssesmentLockAlert
{
    _abaAlert = [[ABAAlert alloc] initAlertWithText:STRTRAD(@"cannotEnterAssesmentKey", @"\nTodavía no puedes realizar la prueba final de esta unidad porque no has terminado las siete secciones anteriores. \n")];
    [_abaAlert showWithOffset:44 andDissmisAfter:1.0f];
}

-(void)updateTitleWithProgress
{
    [self addTitleNavbar:[NSString stringWithFormat:@"%@ %@", STRTRAD(@"unitMenuKey", @"Unidad"), _unit.idUnit] andSubtitle:_unit.title andProgress:[_unit getPercentage]];
}

-(void)updateImages
{
    [self setupBackgroundImage];
    [self setupTeacherView];
}

- (void)showLoadingProgress
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if ([SVProgressHUD isVisible] && !_lockInfoAlert)
        {
            _abaInfoAlert = [[ABAAlert alloc] initAlertHelpWithText:STRTRAD(@"ProgressDialogMessage", @"Estamos actualizando tu progreso. Espera unos segundos.")];
            [_abaInfoAlert showWithOffset:44 andDissmisAfter:120.0];
        }
    });
}

-(void)hideLoadingProgress
{
    [SVProgressHUD dismiss];
}

@end
