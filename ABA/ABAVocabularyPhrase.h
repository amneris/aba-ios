//
//  ABAVocabularyPhrase.h
//  ABA
//
//  Created by Jordi Mele on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "ABAPhrase.h"


@interface ABAVocabularyPhrase : ABAPhrase

@property NSString* wordType;

@end

RLM_ARRAY_TYPE(ABAVocabularyPhrase)