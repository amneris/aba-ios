//
//  Tracker.swift
//  ABA
//
//  Created by MBP13 Jesus on 30/06/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

class Tracker: NSObject {
    
    static let sharedInstance = Tracker()
}

extension Tracker: LoginTracker {
    
    func trackLoginWithEmailAndPassword() {

        MixpanelTrackingManager.shared().trackLoginSuperproperties()
        MixpanelTrackingManager.shared().trackLoginEvent(RecordType.registerTypeMail)
        MixpanelTrackingManager.shared().trackRegistrationFunnel(with: RecordType.registerTypeMail, with: RegistrationFunnelActionType.login)
        FabricTrackingManager.shared().trackLoginEvent()
        LegacyTrackingManager.shared().trackLogin()

        if let user = UserDataModelDAO.getCurrentUser() { // User could be null the first time using VIPER login
            CoolaDataTrackingManager.trackLoginEvent(loginOfflineFlag: false, userId: user.idUser)
        }
    }

    func trackLoginWithToken() {

        // Login event
        if let user = UserDataModelDAO.getCurrentUser() { // User could be null the first time using VIPER login
            CoolaDataTrackingManager.trackLoginEvent(loginOfflineFlag: false, userId: user.idUser)
        }

        // Superproperties
        MixpanelTrackingManager.shared().trackLoginSuperproperties()
        LegacyTrackingManager.shared().trackLogin()
    }

    func trackLoginOffline() {

        // Superproperties
        MixpanelTrackingManager.shared().trackLoginSuperproperties()

        // offline events
        MixpanelTrackingManager.shared().trackLoginOfflineEvent()
        FabricTrackingManager.shared().trackLoginOfflineEvent()

        if let user = UserDataModelDAO.getCurrentUser() { // User could be null the first time using VIPER login
            CoolaDataTrackingManager.trackLoginEvent(loginOfflineFlag: true, userId: user.idUser)
        }

        // Login events
        LegacyTrackingManager.shared().trackLogin()
    }

}

extension Tracker: RegisterTracker {
    
    func trackRegister(_ email: String, name: String) {

        LegacyTrackingManager.shared().trackRegister()
        MixpanelTrackingManager.shared().trackRegister(RecordType.registerTypeMail)
        MixpanelTrackingManager.shared().trackRegistrationFunnel(with: RecordType.registerTypeMail, with: RegistrationFunnelActionType.register)
        FabricTrackingManager.shared().trackRegister(RecordType.registerTypeMail)

        if let user = UserDataModelDAO.getCurrentUser() {
            CoolaDataTrackingManager.trackUserRegistered(user.idUser)
        }

        Crashlytics.sharedInstance().setUserName(name)
        Crashlytics.sharedInstance().setUserEmail(email)
    }
}

extension Tracker: FacebookTracker {

    func trackAccessWithFacebook(_ email: String, name: String, newUser: Bool) {

        if newUser {
            LegacyTrackingManager.shared().trackRegister()
            MixpanelTrackingManager.shared().trackRegister(RecordType.registerTypeFacebook)
            MixpanelTrackingManager.shared().trackRegistrationFunnel(with: RecordType.registerTypeFacebook, with: RegistrationFunnelActionType.register)
            FabricTrackingManager.shared().trackRegister(RecordType.registerTypeFacebook)

            if let user = UserDataModelDAO.getCurrentUser() {
                CoolaDataTrackingManager.trackUserRegistered(user.idUser)
            }
        }
        else {
            LegacyTrackingManager.shared().trackLogin()
            MixpanelTrackingManager.shared().trackLoginSuperproperties()
            MixpanelTrackingManager.shared().trackLoginEvent(RecordType.registerTypeFacebook)
            MixpanelTrackingManager.shared().trackRegistrationFunnel(with: RecordType.registerTypeFacebook, with: RegistrationFunnelActionType.login)
            FabricTrackingManager.shared().trackFacebookLoginEvent()

            if let user = UserDataModelDAO.getCurrentUser() {
                CoolaDataTrackingManager.trackUserRegistered(user.idUser)
            }
        }

        Crashlytics.sharedInstance().setUserName(name)
        Crashlytics.sharedInstance().setUserEmail(email)
    }
}
