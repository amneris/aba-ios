//
//  ABAVocabulary.m
//  ABA
//
//  Created by Jordi Mele on 26/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAVocabulary.h"
#import "ABAUnit.h"


@implementation ABAVocabulary

-(BOOL)hasContent
{
    if (self.content)
    {
        return YES;
    }
    return NO;
}

- (ABAUnit *)unit
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionVocabulary == %@", self];
    RLMResults *results = [ABAUnit objectsInRealm:realm withPredicate:predicate];
    return [results firstObject];
}

@end
