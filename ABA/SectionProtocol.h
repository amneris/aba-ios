//
//  SectionProtocol.h
//  ABA
//
//  Created by Marc Guell on 18/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Definitions.h"

@protocol SectionProtocol <NSObject>

- (void)showAlertCompletedUnit: (kABASectionTypes)type;

@end
