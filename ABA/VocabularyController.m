//
//  VocabularyController.m
//  ABA
//
//  Created by Jordi Mele on 2/12/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "VocabularyController.h"
#import "ABAVocabulary.h"
#import "ABAVocabularyPhrase.h"
#import "PhrasesController.h"

@implementation VocabularyController

-(NSInteger) getTotalPhraseDoneForVocabularySection:(ABAVocabulary *)vocabulary {
    
    NSInteger total = 0;
    
    for(ABAVocabularyPhrase *phrase in vocabulary.content) {
        
        if([phrase.done boolValue]) {
            total ++;
        }
    }
    
    return total;
}

-(NSInteger) getTotalPhraseCompletedForVocabularySection:(ABAVocabulary *)vocabulary {
    
    NSInteger total = 0;
    
    for(ABAVocabularyPhrase *phrase in vocabulary.content) {
        
        if([phrase.listened boolValue])
            total++;
		
        if([phrase.done boolValue]) {
            total++;
        }
    }
    
    return total;
}

-(RLMArray*) getPhrasesDataSource:(ABAVocabulary*) vocabulary {
    return vocabulary.content;
}

-(CGFloat) getTotalElementsForSection: (ABASection*) section {
    ABAVocabulary *vocabuary = (ABAVocabulary*) section;
    return vocabuary.content.count*2;
}

-(CGFloat) getElementsCompletedForSection: (ABASection*) section {
    ABAVocabulary *vocabuary = (ABAVocabulary*) section;
    return [self getTotalPhraseCompletedForVocabularySection:vocabuary];
}

-(BOOL) isSectionCompleted: (ABASection*) section {
    ABAVocabulary *vocabuary = (ABAVocabulary*) section;
    if([self getTotalElementsForSection:vocabuary] == [self getTotalPhraseCompletedForVocabularySection:vocabuary]) {
        return YES;
    }
    return NO;
}

-(ABAPhrase*) phraseWithID: (NSString*) audioID andPage: (NSString*) page onVocabulary: (ABAVocabulary*) vocabulary {
    
    for(ABAPhrase* phrase in [self getPhrasesDataSource:vocabulary]) {
        if([phrase.audioFile isEqualToString:audioID] && [phrase.page isEqualToString:page])
            return phrase;
    }
    
    return nil;
}

- (void)syncCompletedActions:(NSArray *)actions withSection:(ABASection *)section
{
    PhrasesController *phrasesController = [PhrasesController new];
    NSMutableArray *listenedPhrasesArray = @[].mutableCopy;
    NSMutableArray *donePhrasesArray = @[].mutableCopy;

    for (NSDictionary *action in actions)
    {
        ABAPhrase *phrase = [self phraseWithID:[action objectForKey:@"Audio"]
                                       andPage:[action objectForKey:@"Page"]
                                  onVocabulary:(ABAVocabulary *)section];

        if (phrase == nil)
        {
            NSLog(@"ACTION NOT FOUND: %@", action);
            continue;
        }
        if ([[action objectForKey:@"Action"] isEqualToString:@"LISTENED"])
        {

            if ([phrase.listened boolValue] == YES)
            {
                continue;
            }
            [listenedPhrasesArray addObject:phrase];
        }
        else
        {
            if ([phrase.done boolValue] == YES)
            {
                continue;
            }
            [donePhrasesArray addObject:phrase];
        }
    }

    if (listenedPhrasesArray.count)
    {
        [phrasesController setPhrasesListened:listenedPhrasesArray.copy forSection:section sendProgressUpdate:NO completionBlock:^(NSError *error, id object) {

        }];
    }

    if (donePhrasesArray.count)
    {
        [phrasesController setPhrasesDone:donePhrasesArray.copy
                               forSection:section
                       sendProgressUpdate:NO
                          completionBlock:^(NSError *error, id object) {

                              if (!error) {
                                  if ([self isSectionCompleted:section] && (section.completed == nil || section.completed == [NSNumber numberWithBool:NO]))
                                  {
                                      [self setCompletedSection:section completionBlock:^(NSError *error, id object) {
                                          
                                      }];
                                  }
                              }
                          }];
    }
}

-(NSArray *)getAllAudioIDsForVocabularySection: (ABAVocabulary *)vocabulary
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(ABAVocabularyPhrase *phrase in vocabulary.content)
    {
        [array addObject:phrase.audioFile];
    }
    
    return array;
}

-(void) completeAllSection:(ABAVocabulary *)vocabulary {
    
    PhrasesController *phrasesController = [PhrasesController new];
    NSInteger count = 0;
    for(ABAVocabularyPhrase *phrase in vocabulary.content) {
        count++;
        [phrasesController setPhrasesDone:@[phrase] forSection:vocabulary sendProgressUpdate: YES completionBlock:^(NSError *error, id object) {
            NSLog(@"PRHASE DONE");
            if(count == vocabulary.content.count) {
                
                [self setCompletedSection:vocabulary
                          completionBlock:^(NSError *error, id object) {
                              NSLog(@"SECTION DONE");
                          }];
            }
            
        }];
        
    }
    
}

-(void) sendAllPhrasesDoneForVocabularySection:(ABAVocabulary *)vocabulary completionBlock:(CompletionBlock)block {
    
    __weak typeof(self) weakSelf = self;
    
    __block NSInteger numberOfPhrasesCompleted = 0;
    
    PhrasesController *phrasesController = [PhrasesController new];
    
    for(ABAVocabularyPhrase *phrase in vocabulary.content) {

        [phrasesController setPhrasesDone:@[phrase] forSection:vocabulary sendProgressUpdate:NO  completionBlock:^(NSError *error, id object) {
            
            if(!error) {
                numberOfPhrasesCompleted++;
                
                if(numberOfPhrasesCompleted == vocabulary.content.count) {
                    [weakSelf setCompletedSection:vocabulary completionBlock:^(NSError *error, id object) {
                        if(!error) {
                            block(nil,vocabulary);
                            NSLog(@"Finished");
                        }
                        else {
                            block(error,nil);
                            NSLog(@"Error");
                        }
                        
                    }];
                }
            }
            else {
                block(error,nil);
                NSLog(@"Error");
            }
            
        }];
        
    }
}

-(ABAVocabularyPhrase*) getRandomPhraseForVocabularySection:(ABAVocabulary *)vocabulary {
    
    int newUnitIndex = arc4random_uniform((int)[vocabulary.content count]);
    
    return [vocabulary.content objectAtIndex:newUnitIndex];
}

@end
