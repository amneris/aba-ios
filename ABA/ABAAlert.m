//
//  ABAAlert.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "ABAAlert.h"
#import "Resources.h"
#import "Utils.h"
#import "PureLayout.h"
#import "LegacyTrackingManager.h"

@interface ABAAlert ()

@property (strong, nonatomic) NSString *titleText, *firstButtonText, *secondButtonText;

@property (assign) BOOL withButtons, twoButtons, correctBackground, helpBackground;
@property (copy) ButtonBlock buttonBlock;

@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *firstButton;
@property (weak, nonatomic) IBOutlet UIButton *secondButton;
@property (weak, nonatomic) IBOutlet UIImageView *infoButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelLeftSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelRightSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelBottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleViewBottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTopSpaceConstraint;

@end

@implementation ABAAlert

- (id)initAlertWithText:(NSString *)text
{
    self = [super initWithNibName:@"ABAAlert" bundle:nil];
    if (self)
    {
        _titleText = text;
        _withButtons = NO;
        _twoButtons = NO;
    }
    return  self;
}

- (id)initAlertHelpWithText:(NSString *)text
{
    self = [super initWithNibName:@"ABAAlertHelp" bundle:nil];
    if (self)
    {
        _titleText = text;
        _withButtons = NO;
        _twoButtons = NO;
        _helpBackground = YES;
    }
    return  self;
    
}

- (id)initAlertEmailWithText:(NSString *)text withCorrect:(BOOL)correct
{
    self = [super initWithNibName:@"ABAAlertWithImage" bundle:nil];
    if (self)
    {
        _titleText = text;
        _correctBackground = correct;
        _withButtons = NO;
        _twoButtons = YES;
    }
    return  self;
}

- (id)initAlertWithText:(NSString *)text withCorrect:(BOOL)correct
{
    self = [super initWithNibName:@"ABAAlert" bundle:nil];
    if (self)
    {
        _titleText = text;
        _correctBackground = correct;
        _withButtons = NO;
        _twoButtons = NO;
    }
    return  self;
}

- (id)initAlertWithText:(NSString *)text withButtonText:(NSString *)buttonText buttonBlock:(ButtonBlock)buttonBlock
{
    self = [super initWithNibName:@"ABAAlertWithButton" bundle:nil];
    if (self)
    {
        _titleText = text;
        _firstButtonText = buttonText;
        _buttonBlock = buttonBlock;
        
        _withButtons = YES;
        _twoButtons = NO;
    }
    return  self;
    
}

- (id)initAlertWithText:(NSString *)text firstButtonText:(NSString *)firstButtonText secondButtonText:(NSString *)secondButtonText buttonBlock:(ButtonBlock)buttonBlock
{
    self = [super initWithNibName:@"ABAAlertWithButtons" bundle:nil];
    if (self)
    {
        _titleText = text;
        _firstButtonText = firstButtonText;
        _secondButtonText = secondButtonText;
        _buttonBlock = buttonBlock;
        
        _withButtons = YES;
        _twoButtons = YES;
        
    }
    return  self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_titleLabel setText:_titleText];
    _titleLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16];
    
    if (_correctBackground)
    {
        _alertView.backgroundColor = ABA_COLOR_REG_MAIL_BUT;
    }
    else
    {
        _alertView.backgroundColor = ABA_COLOR_RED_ERROR;
    }
    
    if (_helpBackground)
    {
        _alertView.backgroundColor = ABA_COLOR_YELLOW;
    }
    
    [_firstButton setTitle:_firstButtonText forState:UIControlStateNormal];
    [_firstButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    _firstButton.backgroundColor = ABA_COLOR_DARKGREY;
    
    [_secondButton setTitle:_secondButtonText forState:UIControlStateNormal];
    [_secondButton.titleLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?18:14]];
    _secondButton.backgroundColor = ABA_COLOR_DARKGREY;
    
    _infoButton.backgroundColor = [UIColor whiteColor];
    _infoButton.layer.cornerRadius = _infoButton.frame.size.width/2;
    
}

- (void)show
{
    [self showWithOffset:0];
}

- (void)showWithOffset:(CGFloat)offset andDissmisAfter:(CGFloat)delay
{
    // Report
    [[LegacyTrackingManager sharedManager] trackAlertErrorWithText:_titleText andDelay:delay withButtonText:_firstButtonText withButtonText:_secondButtonText];
    //xt:_titleText andDelay:delay withButtons:_titleText withTwoButtons:_twoButtons];

    // Search for top most controller of de view hicherachy
    UIViewController *topController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }
    
    [topController addChildViewController:self];
    
    
    // Check Title Label size
    //    CGFloat labelWidth = [UIScreen mainScreen].bounds.size.width - _titleLabelLeftSpaceConstraint.constant - _titleLabelRightSpaceConstraint.constant;
    CGFloat labelWidth = [UIScreen mainScreen].bounds.size.width - 8 - ((_withButtons&&!_twoButtons)?93:8);
    
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = [UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?20:16];
    gettingSizeLabel.text = _titleText;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(labelWidth, 9999);
    CGSize size = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    
    //    CGFloat viewHeight = size.height>34?:34 + _titleLabelTopSpaceConstraint.constant + _titleLabelBottomSpaceConstraint.constant + _titleViewBottomSpaceConstraint.constant;
    CGFloat viewHeight = MAX(size.height, 34) + (IS_IPAD?26:16);
    if (_twoButtons) viewHeight += 50;
    
    // Set view frame and alpha
    //    [self.view setFrame:CGRectMake(0, offset, [UIScreen mainScreen].bounds.size.width, viewHeight)];
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [topController.view addSubview:self.view];
    
    [self.view autoSetDimension:ALDimensionHeight toSize:viewHeight];
    [self.view autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:topController.view];
    [self.view autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:topController.view];
    [self.view autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:topController.view withOffset:offset];
    
    [self.view setAlpha:0.0];
    
    // Fade animation
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        
        [weakSelf.view setAlpha:1.0];
        
    } completion:^(BOOL finished) {
        
        if (!weakSelf.withButtons) {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.25 animations:^{
                    
                    [weakSelf.view setAlpha:0.0];
                    
                }completion:^(BOOL finished) {
                    
                    [weakSelf.view removeFromSuperview];
                    [weakSelf removeFromParentViewController];
                    
                }];
                
            });
        }
    }];
    
}

- (void)showWithOffset:(CGFloat)offset
{
    [self showWithOffset:offset andDissmisAfter:2];
}

- (void)dismiss
{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        
        [weakSelf.view setAlpha:0.0];
        
    } completion:^(BOOL finished) {
        
        [weakSelf.view removeFromSuperview];
        [weakSelf removeFromParentViewController];
        
    }];
    
}

- (IBAction)defaultAction:(id)sender
{
    _buttonBlock(self, [sender tag]);
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        
        [weakSelf.view setAlpha:0.0];
        
    }completion:^(BOOL finished) {
        
        [weakSelf.view removeFromSuperview];
        [weakSelf removeFromParentViewController];
        
    }];
}

@end
