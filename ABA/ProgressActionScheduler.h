//
//  ProgressActionController.h
//  ABA
//
//  Created by MBP13 Jesus on 16/03/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kProgressActionLimit 1000

@interface ProgressActionScheduler : NSObject

+ (id)sharedManager;

- (void)startScheduling;
- (void)pauseScheduling;
- (void)resumeScheduling;

@end
