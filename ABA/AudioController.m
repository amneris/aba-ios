//
//  AudioController.m
//  ABA
//
//  Created by Oriol Vilaró on 10/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import "AudioController.h"
#import "ABAPulseViewController.h"
#import "ABAUnit.h"
#import "APIManager.h"
#import "ABAError.h"
#import "Utils.h"
#include <sys/param.h>
#include <sys/mount.h>
#import "MixpanelTrackingManager.h"

#define kAudioBaseUrl @"http://static.abaenglish.com/audio/course/units/unit%@/audio/%@.mp3"
#define kMaxRecordTime 30.0

@implementation AudioController

#pragma mark - Lifecycle

- (void)dealloc
{
    _recorder.delegate = nil;
    _player.delegate = nil;
}

#pragma mark - Public methods

- (void)playAudioFile:(NSString *)audioID withUnit:(ABAUnit *)unit forRec:(BOOL)rec
{
	__weak typeof(self) weakSelf = self;
	
    if (![self checkIfAudioExists:audioID andUnit:unit forRec:rec])
    {
        [[APIManager sharedManager] downloadFile:[self getAudioFileURL:audioID unit:unit] toFileName:[self getLocalAudioPath:audioID andUnit:unit forRec:rec] completionBlock:^(NSError *error, id object) {
            
            if (!error)
            {
				[weakSelf playAudioFromURL:[NSURL fileURLWithPath:(NSString *)object] forRec:rec withAudioId:audioID andUnit:unit];
            }
            else
            {
				[weakSelf checkDownloadOrSpaceError:error withAudioId:audioID andUnit:unit withTag:@"001"];
			}
        }];
    }
    else
    {
		[self playAudioFromURL:[NSURL fileURLWithPath:[self getLocalAudioPath:audioID andUnit:unit forRec:rec]] forRec:rec withAudioId:audioID andUnit:unit];
    }
}

- (void)recordAudioFile:(NSString *)audioID withUnit:(ABAUnit *)unit
{
    // Already recording or playing
    if ([_recorder isRecording])
    {
        [_audioControllerDelegate recordingDidGetError:[ABAError errorWithKey:STRTRAD(@"audioPlayerAlreadyPlayingErrorKey", @"audioPlayerAlreadyPlayingErrorKey")]];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    
    // Check for microphone granted access
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted)
     {
         if (!granted)
         {
             NSError *error = [ABAError errorWithKey:@"Imposible acceder al micrófono"];
             [_audioControllerDelegate recordingDidGetError:error];
         }
         else
         {
             // Setup audio session
             AVAudioSession *session = [AVAudioSession sharedInstance];
             [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
             
             NSDictionary *recordSettings = [NSDictionary
                                             dictionaryWithObjectsAndKeys:
                                             [NSNumber numberWithInt:AVAudioQualityMin],
                                             AVEncoderAudioQualityKey,
                                             [NSNumber numberWithInt:16],
                                             AVEncoderBitRateKey,
                                             [NSNumber numberWithInt:2],
                                             AVNumberOfChannelsKey,
                                             [NSNumber numberWithFloat:44100.0],
                                             AVSampleRateKey,
                                             nil];
             
             NSString *path = [weakSelf getLocalAudioPath:audioID andUnit:unit forRec:YES];
             
             NSURL *soundFileURL = [NSURL fileURLWithPath:path];
             NSError *error = nil;
             
             _recorder = [[AVAudioRecorder alloc] initWithURL:soundFileURL settings:recordSettings error:&error];
             _recorder.delegate = weakSelf;
             
             if (error)
             {
                 [_audioControllerDelegate recordingDidGetError:error];
                 
                 [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
             }
             else
             {
                 [[AVAudioSession sharedInstance] setActive:YES error:nil];
                 
                 [_recorder prepareToRecord];
                 _recorder.meteringEnabled = YES;
                 
                 _recorderPulseTimer = [NSTimer scheduledTimerWithTimeInterval:0.10 target:weakSelf selector:@selector(updateRecorderPulseMeter:) userInfo:nil repeats:YES];
                 
                 [_recorder record];
                 [_audioControllerDelegate recordingStarted];
                 
                 _recorderMaxSecondsTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxRecordTime target:weakSelf selector:@selector(stopRecording) userInfo:nil repeats:NO];
             }
         }
     }];
}

- (void)stopRecording
{
    [_recorderMaxSecondsTimer invalidate];
    _recorderMaxSecondsTimer = nil;
    
    if (_recorder == nil)
    {
        NSError *error = [ABAError errorWithKey:@"stopRecording recorder is nil flag"];
        [_audioControllerDelegate recordingDidGetError:error];
    }
    else
    {
        [_recorder stop];
    }
}

- (BOOL)checkIfAudioExists:(NSString *)audioID andUnit:(ABAUnit *)unit forRec:(BOOL)rec
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self getLocalAudioPath:audioID andUnit:unit forRec:rec]];
}

- (void)downloadAudios:(NSArray *)audioArray withUnit:(ABAUnit *)unit completionBlock:(ErrorBlock)block {
	
	__block int totalAudios = (int)[audioArray count];
	__block int downloadedAudios = 0;
	__block bool errorHasOcurred = NO;
	
	for(NSString *audio in audioArray)
	{
		if (![self checkIfAudioExists:audio andUnit:unit forRec:NO])
		{
			[[APIManager sharedManager] downloadFile:[self getAudioFileURL:audio unit:unit] toFileName:[self getLocalAudioPath:audio andUnit:unit forRec:NO] completionBlock:^(NSError *error, id object) {
                
				 if (!error)
                 {
					 downloadedAudios++;
					 
					 if (downloadedAudios == totalAudios)
                     {
						 block(nil);
					 }
				 }
				 else {
					 if(!errorHasOcurred) {
						 errorHasOcurred = YES;
						 block(error);
					 }
				 }
			 }];
		}
		else
        {
			downloadedAudios++;
			
			if (downloadedAudios == totalAudios) {
				block(nil);
			}
		}
	}
}

- (void)playSound:(NSString *)audioFile
{
    // Already recording or playing
    if ([_recorder isRecording] || [_player isPlaying])
    {
        return;
    }
    
    NSError *error = nil;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], audioFile]];
    
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"mp3" error:&error];
    [_player setVolume:1.0];
    
    if (!error)
    {
        _player.delegate = self;
        
        [_player prepareToPlay];
        [_player play];
        [_player setMeteringEnabled:YES];
        [_audioControllerDelegate audioStarted];
    }
}

- (BOOL)audiosDownloaded:(NSArray *)audioArray withUnit:(ABAUnit *)unit
{
    for(NSString *audio in audioArray)
    {
        if (![self checkIfAudioExists:audio andUnit: unit forRec:NO])
        {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [_playerPulseTimer invalidate];
    _playerPulseTimer = nil;
    _player = nil;
    
    if (flag)
    {
        [_audioControllerDelegate audioFinished];
		
		if (_currentUnit && _currentAudioId) {
			_currentAudioId = nil;
			_currentUnit = nil;
		}
    }
    else
    {
		// At this point, the error means bad audio file. This will try to delete file, redownload one more time and play it
		if (!_redownloadRetryCounter && (_currentAudioId && _currentUnit))
        {
			__weak typeof(self) weakSelf = self;
			
			_redownloadRetryCounter = [NSNumber numberWithInt:1];
			[[NSFileManager defaultManager] removeItemAtPath:[self getLocalAudioPath:_currentAudioId andUnit:_currentUnit forRec:NO] error:nil];
			
			[[APIManager sharedManager] downloadFile:[self getAudioFileURL:_currentAudioId unit:_currentUnit] toFileName:[self getLocalAudioPath:_currentAudioId andUnit:_currentUnit forRec:NO] completionBlock:^(NSError *error, id object) {
                
				 if (!error)
				 {
					 [weakSelf playAudioFromURL:[NSURL fileURLWithPath:(NSString *)object] forRec:NO withAudioId:_currentAudioId andUnit:_currentUnit];
				 }
				 else
				 {
					 weakSelf.redownloadRetryCounter = nil;
					 [weakSelf checkDownloadOrSpaceError:error withAudioId:_currentAudioId andUnit:_currentUnit withTag:@"008"];
					 
					 weakSelf.currentAudioId = nil;
					 weakSelf.currentUnit = nil;
				 }
			 }];
		}
		else
        {
			// At this point, we have already retried to download the audio again, but
			// it's impossible to play it though (maybe network/cdn problems, or space)
			
			_redownloadRetryCounter = nil;
			[_audioControllerDelegate audioDidGetError:[ABAError errorWithKey:STRTRAD(@"audioPlayerDownloadErrorKey", @"audioPlayerDownloadErrorKey")]];
			_currentAudioId = nil;
			_currentUnit = nil;
		}
    }
}

#pragma mark - AVAudioRecorderDelegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
    _recorder = nil;
    
    if (flag)
    {
        [_audioControllerDelegate recordingFinished];
    }
    else
    {
        NSError *error = [ABAError errorWithKey:@"audioRecorderDidFinishRecording unsuccessful flag"];
        [_audioControllerDelegate recordingDidGetError:error];
    }
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    [_audioControllerDelegate recordingDidGetError:error];
}

#pragma mark - Private methods

- (void)checkDownloadOrSpaceError:(NSError *)error withAudioId:(NSString *)audioId andUnit:(ABAUnit *)unit withTag:(NSString *)tag{
	
	NSError *pError;
	
	if (error.code == NSFileWriteOutOfSpaceError)
    {
		pError = [ABAError errorWithKey:STRTRAD(@"audioPlayerNotEnoughSpaceErrorKey", @"audioPlayerNotEnoughSpaceErrorKey")];
	}
	else
    {
		pError = [ABAError errorWithKey:STRTRAD(@"audioPlayerDownloadErrorKey", @"audioPlayerDownloadErrorKey")];
	}
	
    [_audioControllerDelegate audioDidGetError:pError];
    
}

- (float)getFreeSpace
{
	long long freeSpace = 0.0f;
	NSError *error = nil;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
	
	if (dictionary)
    {
		NSNumber *fileSystemFreeSizeInBytes = [dictionary objectForKey: NSFileSystemFreeSize];
		freeSpace = [fileSystemFreeSizeInBytes longLongValue];
	}
	else {
  //Handle error
	}
	
	return freeSpace;
}

- (void)updatePlayerPulseMeter:(NSTimer *)timer
{
    [_player updateMeters];
    
    //    float power = [_player averagePowerForChannel:0];
    //    float peak = [_player peakPowerForChannel:0];
    
    if (_pulseViewController != nil) {
        //        if (power > -15.0) {
        [_pulseViewController createPulse];
        //        }
        
    } else {
        [_playerPulseTimer invalidate];
        _playerPulseTimer = nil;
    }
}

- (void)updateRecorderPulseMeter:(NSTimer *)timer
{
    [_recorder updateMeters];
    
    //    float power = [_recorder averagePowerForChannel:0];
    
    if (_pulseViewController != nil) {
        //        if (power > -15.0) {
        [_pulseViewController createPulse];
        //        }
        
    } else {
        [_recorderPulseTimer invalidate];
        _recorderPulseTimer = nil;
    }
}

- (void)playAudioFromURL:(NSURL *)url forRec:(BOOL)rec withAudioId:(NSString *)audioId andUnit:(ABAUnit *)unit
{
    // Already recording or playing
    if ([_recorder isRecording] || [_player isPlaying])
    {
        NSLog(@"ALREADY PLAY AUDIO ERROR:%@",url);
        
        NSError *error = [ABAError errorWithKey:STRTRAD(@"audioPlayerAlreadyPlayingErrorKey", @"audioPlayerAlreadyPlayingErrorKey")];
        [_audioControllerDelegate audioDidGetError:error];
        
        return;
    }
    
    NSError *error = nil;
    
    //#warning TESTING PURPOSES!
    //	url = [NSURL URLWithString:@"https://www.dropbox.com/s/zbz1fgsj2hw3mmi/audio.mp3?dl=0"];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    
    if (rec)
    {
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"caf" error:&error];
        [_player setVolume:1.0];
    }
    else
    {
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"mp3" error:&error];
        [_player setVolume:1.0];
        _currentAudioId = audioId;
        _currentUnit = unit;
    }
    
    if (!error)
    {
        // NSLog(@"PLAY AUDIO OK:%@",url);
        
        _audioType = kAudioControllerTypeAudio;
        
        [_audioControllerDelegate audioStarted];
        _player.delegate = self;
        
        [_player prepareToPlay];
        [_player play];
        [_player setMeteringEnabled:YES];
        
        _playerPulseTimer = [NSTimer scheduledTimerWithTimeInterval:0.10 target:self selector:@selector(updatePlayerPulseMeter:) userInfo:nil repeats:YES];
    }
    else
    {
        [self checkDownloadOrSpaceError:error withAudioId:audioId andUnit:unit withTag:@"007"];
    }
}

- (NSURL *)getAudioFileURL:(NSString *)audioID unit:(ABAUnit *)unit
{
    NSString *idUnit = [NSString stringWithFormat:@"%03ld", (long)[unit.idUnit integerValue]];
    NSURL *audioURL = [NSURL URLWithString:[NSString stringWithFormat:kAudioBaseUrl, idUnit, audioID]];
    return audioURL;
}

- (NSString *)getLocalAudioPath:(NSString *)audioID andUnit:(ABAUnit *)unit forRec:(BOOL)rec
{
    NSString *fileName = [NSString stringWithFormat:@"%@.mp3", audioID];
    if (rec)
    {
        fileName = [NSString stringWithFormat:@"%@.caf", audioID];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *unitFolder = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"unit%@", unit.idUnit]];
    
    BOOL isDir = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:unitFolder isDirectory:&isDir])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:unitFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *path = [unitFolder stringByAppendingPathComponent:fileName];
    return path;
}

@end
