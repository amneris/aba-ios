//
//  WelcomeViewController.swift
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 06/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import UIKit
import BWWalkthrough
import Device
import BonMot

class WelcomeViewController: BWWalkthroughViewController {
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var welcomePageControl: UIPageControl!
    
    var welcomePage0: WelcomePageViewController!
    var welcomePage1: WelcomePageViewController!
    var welcomePage2: WelcomePageViewController!
    var welcomePage3: WelcomePageViewController!

    @IBOutlet weak var buttonSizeConstraint: NSLayoutConstraint!
    let buttonRegisterWidth: CGFloat = Device.isLargerThanScreenSize(Size.screen5_5Inch) ? 380 : 260
    let buttonRegisterCorenerRadius: CGFloat = 2

    let startStoryboard = { return UIStoryboard(name: "Start", bundle: Bundle.main) }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollview.bounces = false
        self.pageControl = welcomePageControl

        welcomePage0 = startStoryboard.instantiateViewController(withIdentifier: "welcomePage0") as! WelcomePageViewController
        welcomePage1 = startStoryboard.instantiateViewController(withIdentifier: "welcomePage1") as! WelcomePageViewController
        welcomePage2 = startStoryboard.instantiateViewController(withIdentifier: "welcomePage2") as! WelcomePageViewController
        welcomePage3 = startStoryboard.instantiateViewController(withIdentifier: "welcomePage3") as! WelcomePageViewController

        setupPage0(welcomePage0)
        setupPage1(welcomePage1)
        setupPage2(welcomePage2)
        setupPage3(welcomePage3)

        setupRegisterButton()
        setupLoginButton()
    }

    @IBAction func goToRegister(_ sender: AnyObject) {
        let registerFormViewController = RegisterConfigurator.createRegisterViperStack()
        self.navigationController?.pushViewController(registerFormViewController, animated: true)
    }

    @IBAction func goToLogin(_ sender: AnyObject) {
        let loginViewController = LoginConfigurator.createLoginViperStack()        
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }

    func setupRegisterButton() {
        registerButton.backgroundColor = RegisterFunnelConstants.colorAquaBlue
        registerButton.layer.cornerRadius = buttonRegisterCorenerRadius
        let string = String.localize("startNowButtonKey", comment: "empieza ya").uppercased()

        registerButton.setAttributedTitle(string.styled(with: TextAttributedStyles.buttonStyle), for: UIControlState())
        buttonSizeConstraint.constant = buttonRegisterWidth
        view.setNeedsUpdateConstraints()
    }

    func setupLoginButton() {
        let string1 = String.localize("funnelABtestLoginTextButton", comment: "Are you already a user?")
        let string2 = String.localize("funnelABtestLoginJoinTextButton", comment: "Log In")
        let attributedString = NSAttributedString.composed(of: [
            string1.styled(with: TextAttributedStyles.leftPartWellcomeStyle),
            Special.space,
            string2.styled(with: TextAttributedStyles.rightPartWellcomeStyle)
        ])
        
        logInButton.setAttributedTitle(attributedString, for: UIControlState())
    }

    func setupPage0(_ page: WelcomePageViewController) {
        addViewController(page)

        let line0 = String.localize("startTitle1Key", comment: "aprende")
        let line1 = String.localize("startTitle2Key", comment: "ingles online")
        let line2 = String.localize("startTitle3Key", comment: "para la vida real")
        let concatString = "\(line0)\n\(line1)\n<blue>\(line2)</blue>"
        page.titleTextString = concatString
    }

    func setupPage1(_ page: WelcomePageViewController) {
        addViewController(page)

        let line0 = String.localize("tuto1Title1Key", comment: "comprueba la")
        let line1 = String.localize("tuto1Title2Key", comment: "eficacia del")
        let line2 = String.localize("tuto1Title3Key", comment: "metodo natural")
        let descriptionString = String.localize("tuto1DescKey", comment: "aprenderas ingles como en la vida.... que mejor funciona!")
        let concatString = "\(line0)\n\(line1)\n<blue>\(line2)</blue>"
        page.titleTextString = concatString
        page.descriptionTextString = descriptionString
    }

    func setupPage2(_ page: WelcomePageViewController) {
        addViewController(page)

        let line0 = String.localize("tuto2Title1Key", comment: "el placer de")
        let line1 = String.localize("tuto2Title2Key", comment: "ver peliculas")
        let line2 = String.localize("tuto2Title3Key", comment: "en ingles")
        let descriptionString = String.localize("tuto2DescKey", comment: "en cada capitulo, trajaras.... conversaciones reales")
        let concatString = "\(line0)\n\(line1)\n<blue>\(line2)</blue>"
        page.titleTextString = concatString
        page.descriptionTextString = descriptionString
    }

    func setupPage3(_ page: WelcomePageViewController) {
        addViewController(page)
        let line0 = String.localize("tuto3Title1Key", comment: "habla")
        let line1 = String.localize("tuto3Title2Key", comment: "tuto3Title2Key")
        let line2 = String.localize("tuto3Title3Key", comment: "tuto3Title3Key")
        let descriptionString = String.localize("tuto3DescKey", comment: "saca todo el partido.... practicando ingles...")
        let concatString = "\(line0)\n\(line1)\n<blue>\(line2)</blue>"
        page.titleTextString = concatString
        page.descriptionTextString = descriptionString
    }

    override var shouldAutorotate: Bool {
        get {
            return Device.isLargerThanScreenSize(.screen5_5Inch)
        }
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if Device.type() == .iPad {
                return .all
            }
            else {
                return .portrait
            }
        }
    }
}
