//
//  EvaluationResultViewController.h
//  ABA
//
//  Created by Ivan Ferrera on 27/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@class ABAEvaluation;

@interface EvaluationResultViewController : UIViewController

@property (nonatomic, strong) ABAEvaluation * evaluation;

@end
