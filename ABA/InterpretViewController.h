//
//  InterpretSelectionViewController.h
//  ABA
//
//  Created by Marc Guell on 24/11/14.
//  Copyright (c) 2014 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionBaseViewController.h"
#import "SectionProtocol.h"
#import "ABAInterpret.h"

@class ABASection;

@interface InterpretViewController : SectionBaseViewController

@property (strong, nonatomic) ABAInterpret *section;
@property (weak) id <SectionProtocol> delegate;

@end
