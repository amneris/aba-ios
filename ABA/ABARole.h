//
//  ABARole.h
//  ABA
//
//  Created by Marc Güell Segarra on 12/1/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface ABARole : RLMObject

@property NSString * imageUrl;
@property NSString * imageBigUrl;
@property NSString * name;

@end

RLM_ARRAY_TYPE(ABARole)
