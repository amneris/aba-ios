//
//  ExperimentRunner.m
//  ABA
//
//  Created by Oriol Vilaró on 6/10/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ExperimentRunner.h"
#import "Utils.h"
#import "ABAExperiment.h"
#import "Mixpanel/MPTweakInline.h"

@implementation ExperimentRunner

+ (void)runExperiment:(NSString *)experimentIdentifier withBaseline:(VariationBlock)baselineBlock andVariationBlockDict:(NSDictionary *)variationBlockDict
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kExperimentsKey];
    NSArray *experimentsArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    for (ABAExperiment *experiment in experimentsArray)
    {
        if ([experiment.experimentIdentifier isEqualToString:experimentIdentifier]) // search for current variation
        {
            VariationBlock variationBlock = [variationBlockDict objectForKey:experiment.experimentVariationIdentifier];
            if (!variationBlock)
            {
                // if we do not have variation block for current variation, do baseline
                NSLog(@"Experiment %@ without %@ block variation, run baseline", experimentIdentifier, experiment.experimentVariationIdentifier);
                baselineBlock();
                return;
            }
            
            NSLog(@"Experiment %@ run %@ variation", experimentIdentifier, experiment.experimentVariationIdentifier);
            variationBlock();
            return;
        }
    }
    
    // we do not have any experiment, run baseline
    NSLog(@"Experiment %@ without variations, run baseline", experimentIdentifier);
    baselineBlock();
}

+ (void)saveExperimentWithExperimentProfile:(NSArray *)profilesArray {
    NSMutableArray *mutArray = @[].mutableCopy;
    
    for (NSDictionary *experimentDict in profilesArray)
    {
        [experimentDict enumerateKeysAndObjectsUsingBlock:^(id _Nonnull key, id _Nonnull obj, BOOL *_Nonnull stop) {
            ABAExperiment *exp = [ABAExperiment new];
            exp.experimentIdentifier = key;
            exp.experimentVariationIdentifier = [obj objectForKey:@"variation"];
            exp.userExperimentVariationId = [obj objectForKey:@"type"];
            
            [mutArray addObject:exp];
        }];
    }
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:mutArray.copy];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kExperimentsKey];
}

#pragma mark - Private methods

+ (BOOL)isFirstHalfMinute {
    NSDate * currentDate = [[NSDate alloc] init];
    NSInteger comp = [[NSCalendar currentCalendar] component:NSCalendarUnitSecond fromDate: currentDate];
    return comp < 30;
}

@end
