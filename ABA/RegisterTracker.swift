//
//  RegisterTracker.swift
//  ABA
//
//  Created by MBP13 Jesus on 08/09/16.
//  Copyright © 2016 ABA English. All rights reserved.
//

import Foundation

protocol RegisterTracker: FacebookTracker {
    func trackRegister(_ email: String, name: String)
    func trackAccessWithFacebook(_ email: String, name: String, newUser: Bool)
}
