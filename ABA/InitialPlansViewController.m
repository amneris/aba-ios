//
//  InitialPlansViewController.m
//  ABA
//
//  Created by Jordi Melé on 6/2/15.
//  Copyright (c) 2015 mOddity. All rights reserved.
//

#import "InitialPlansViewController.h"
#import "UIViewController+ABA.h"
#import "Resources.h"
#import "Utils.h"
#import "PlansViewController.h"
#import "UserController.h"
#import "ABARealmUser.h"
#import "SDVersion.h"
#import "TrackingDefinitions.h"

#import "ABA-Swift.h"

@interface InitialPlansViewController ()

@property (weak, nonatomic) IBOutlet UILabel *helloTitle;
@property (weak, nonatomic) IBOutlet UILabel *subtitle1;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *buttonSubtitle;
@property (weak, nonatomic) IBOutlet UIButton *goPlansButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textTopConstraint;

@end

@implementation InitialPlansViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addBackButtonWithTitleBlue:@"ABA Premium"];
    
    [_goPlansButton.layer setCornerRadius:3.0f];
    _goPlansButton.clipsToBounds = YES;
    
    ABARealmUser *currentUser = [UserController currentUser];
    
    NSString *helloString = STRTRAD(@"helloInitialPlansKey", @"¡Hola");
    
    [_helloTitle setText:[NSString stringWithFormat:@"%@ %@!", helloString, currentUser.name]];
    
    NSString *subtitle1FirstString = STRTRAD(@"subtitle1InitialPlansKey", @"El contenido al que quieres acceder es exclusivo para los alumnos de");
    NSString *subtitle1PremiumString = STRTRAD(@"subtitle1BoldInitialPlansKey", @"ABA Premium");

    NSString *subtitle1String = [NSString stringWithFormat:@"%@ %@", subtitle1FirstString, subtitle1PremiumString];
    
    NSMutableAttributedString *subtitle1AttrString = [[NSMutableAttributedString alloc] initWithString:subtitle1String];
    
    [subtitle1AttrString addAttribute:NSFontAttributeName
								value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?36:19]
                      range:[subtitle1String rangeOfString:subtitle1FirstString]];

    [subtitle1AttrString addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?36:19]
                                range:[subtitle1String rangeOfString:subtitle1PremiumString]];

    _subtitle1.attributedText = [subtitle1AttrString mutableCopy];

    switch([SDVersion deviceSize])
    {
        case Screen4inch:
        {
            [_textLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:14]];
            break;
        }
        case Screen4Dot7inch:
        {
            [_textLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:16]];
            break;
        }
        case Screen5Dot5inch:
        {
            [_textLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:18]];
            break;
        }
        case Screen3Dot5inch:
        {
            _textTopConstraint.constant = -40;
			[_goPlansButton.titleLabel setFont:[UIFont fontWithName:ABA_RALEWAY_BOLD size:18]];
			break;
        }
		default: {
			if(IS_IPAD) {
				[_textLabel setFont:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:24]];
				[_goPlansButton.titleLabel setFont:[UIFont fontWithName:ABA_RALEWAY_BOLD size:24]];
				_textTopConstraint.constant = _textTopConstraint.constant * 6;
			}
			break;
		}
    }
    
    NSString *subtitleButtonFirstString = STRTRAD(@"subtitleButtonFirstInitialPlansKey", @"Desbloquea ABA Premium");
    NSString *subtitleButtonSecondString = STRTRAD(@"subtitleButtonSecondInitialPlansKey", @"y disfruta de todos los contenidos");
    
    NSString *subtitleButtonString = [NSString stringWithFormat:@"%@\n%@", subtitleButtonFirstString, subtitleButtonSecondString];
    
    NSMutableAttributedString *subtitleButtonAttrString = [[NSMutableAttributedString alloc] initWithString:subtitleButtonString];
    
    [subtitleButtonAttrString addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:ABA_MUSEO_SANS_900 size:IS_IPAD?24:16]
                                range:[subtitleButtonString rangeOfString:subtitleButtonFirstString]];
    
    [subtitleButtonAttrString addAttribute:NSForegroundColorAttributeName
                                     value:ABA_COLOR_DARKGREY
                                     range:[subtitleButtonString rangeOfString:subtitleButtonFirstString]];
    [subtitleButtonAttrString addAttribute:NSFontAttributeName
									 value:[UIFont fontWithName:ABA_MUSEO_SANS_500 size:IS_IPAD?24:16]
                                range:[subtitleButtonString rangeOfString:subtitleButtonSecondString]];
    
    [subtitleButtonAttrString addAttribute:NSForegroundColorAttributeName
                                     value:ABA_COLOR_BLUE_MENU_UNIT
                                     range:[subtitleButtonString rangeOfString:subtitleButtonSecondString]];

    _buttonSubtitle.attributedText = [subtitleButtonAttrString mutableCopy];
    
    NSString *textString = STRTRAD(@"textInitialPlansKey", @"Como ABA Free, puedes probar la primera unidad completa de cada nivel y ver todas las videoclases de gramática de todos los niveles\nPara disfrutar de los contenidos de forma ilimitada y obtener los certificados de American & British Academy pásate a Premium");
    
    [_textLabel setText:textString];

    NSString *buttonString = STRTRAD(@"buttonLabelPlansKey", @"Ver precios");
    [_goPlansButton setTitle:buttonString forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)goPlansButtonAction:(id)sender
{
    PlansViewController *plansViewController = [PlansConfigurator createPlansViperStackWithType:kPlansViewControllerOriginUnlockContent shouldShowBackButton:YES];
    [self.navigationController pushViewController:plansViewController animated:YES];
    
}
@end
