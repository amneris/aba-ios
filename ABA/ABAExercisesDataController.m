//
//  ABAExercisesDataController.m
//  ABA
//
//  Created by Oleksandr Gnatyshyn on 17/11/15.
//  Copyright © 2015 ABA English. All rights reserved.
//

#import "ABAExercisesDataController.h"
#import "ABAExercises.h"
#import "ABAUnit.h"

@implementation ABAExercisesDataController

+ (void)parseABAExercisesSection:(NSArray *)abaExercisesDataArray onUnit:(ABAUnit *)unit completionBlock:(CompletionBlock)block
{
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSError *error;
    
    __block ABAExercises *exercises = unit.sectionExercises;
    
    [realm transactionWithBlock:^{
        
        //
        // We create ABAExercises object
        //
        if (!exercises)
        {
            exercises = [[ABAExercises alloc] init];
            unit.sectionExercises = exercises;
            exercises.completed = [NSNumber numberWithInt:0];
        }
        
        //
        //  We iterate in data array
        //
        for (NSDictionary *abaExerciseGroupData in abaExercisesDataArray)
        {
            //NSLog(@"abaExerciseGroupData: %@", abaExerciseGroupData);
            
            ABAExercisesGroup *exerciseGroup = [[ABAExercisesGroup alloc] init];
            exerciseGroup.completed = [NSNumber numberWithInt:0];
            [exercises.exercisesGroups addObject:exerciseGroup];
            
            // Capturamos el título del ejercicio
            NSArray *titleArray = abaExerciseGroupData[@"title"];
            
            if ([titleArray count] > 0)
            {
                NSDictionary *titleDict = [titleArray objectAtIndex:0];
                
                if (![titleDict[@"translation"] isEqual:[NSNull null]] &&
                    titleDict[@"translation"] != nil)
                {
                    exerciseGroup.title = titleDict[@"translation"];
                }
            }
            
            NSArray *exercisesQuestions = abaExerciseGroupData[@"exercise"];
            
            for (NSArray *abaExerciseQuestionData in exercisesQuestions)
            {
                ABAExercisesQuestion *exerciseQuestion = [[ABAExercisesQuestion alloc] init];
                exerciseQuestion.completed = [NSNumber numberWithInt:0];
                [exerciseGroup.questions addObject:exerciseQuestion];
                
                //NSLog(@"abaExerciseQuestionData: %@", abaExerciseQuestionData);
                
                for (id object in abaExerciseQuestionData)
                {
                    if ([object isKindOfClass:[NSArray class]]) // Exercises entry
                    {
                        NSArray *abaExerciseQuestionArray = (NSArray *)object;
                        
                        //NSLog(@"abaExerciseQuestionArray: %@", abaExerciseQuestionArray);
                        
                        //							 NSInteger phraseIndex = 0;
                        
                        for (NSDictionary *objectDict in abaExerciseQuestionArray)
                        {
                            if (objectDict[@"translation"] != nil)
                            {
                                exerciseQuestion.exerciseTranslation = objectDict[@"translation"];
                            }
                            else if (objectDict[@"text"] != nil &&
                                     objectDict[@"audio"] == nil)
                            {
                                ABAExercisesPhrase *exercisePhrase = [[ABAExercisesPhrase alloc] init];
                                [exerciseQuestion.phrases addObject:exercisePhrase];
                                
                                exercisePhrase.text = [NSString stringWithFormat:@"%@: ", objectDict[@"text"]];
                                exercisePhrase.blank = @"";
                            }
                            else
                            {
                                ABAExercisesPhrase *exercisePhrase = [[ABAExercisesPhrase alloc] init];
                                [exerciseQuestion.phrases addObject:exercisePhrase];
                                
                                if (![objectDict[@"blank"] isEqual:[NSNull null]] && ![objectDict[@"blank"] isEqualToString:@""])
                                {
                                    exercisePhrase.idPhrase = [NSString stringWithFormat:@"%@_%@", objectDict[@"audio"], objectDict[@"posAudio"]];
                                }
                                else
                                {
                                    exercisePhrase.idPhrase = [NSString stringWithFormat:@"%@", objectDict[@"audio"]];
                                }
                                
                                exercisePhrase.text = objectDict[@"text"];
                                exercisePhrase.audioFile = objectDict[@"audio"];
                                exercisePhrase.blank = objectDict[@"blank"];
                                exercisePhrase.done = [NSNumber numberWithInt:0];
                                exercisePhrase.page = objectDict[@"page"];
                            }
                        }
                    }
                    else if ([object isKindOfClass:[NSDictionary class]]) // Translation entry
                    {
                        NSDictionary *objectDict = (NSDictionary *)object;
                        
                        if (![objectDict[@"translation"] isEqual:[NSNull null]])
                        {
                            exerciseQuestion.exerciseTranslation = objectDict[@"translation"];
                        }
                        else
                        {
                            exerciseQuestion.exerciseTranslation = @"";
                        }
                    }
                }
            }
        }
        
    } error:&error];
    
    if (!error)
    {
        block(nil, exercises);
    }
    else
    {
        block(error, nil);
    }
}

@end
